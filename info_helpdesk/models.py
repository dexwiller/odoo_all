# -*- coding: utf-8 -*-
import os
import time
import base64

from openerp import models, fields, api

class kilavuz(models.Model):
    _name = 'info_helpdesk.kilavuz'

    name = fields.Char(string="Kılavuz Adı", required=True)
    dosya = fields.Binary(string='Dosya', required=True)
    dosyaadi = fields.Char(string='Dosya Adı')
    proje = fields.Char(string='Proje')
    
    @api.multi
    def klavuzizle(self):
        self.ensure_one()
        view_id = self.env['ir.ui.view'].search([('name','=','info_helpdesk_klavuz.form2')])[0].id
        res = {
            'type' : 'ir.actions.act_window',
            'res_model' : 'info_helpdesk.kilavuz',
            'res_id' : self.id,
            'target' : 'current',
            'view_mode' : 'form',
            'view_id' : view_id,
        }
        return res
    @api.multi
    def get_fileb64(self):
        self.ensure_one()
        return {'b64':str(self.dosya)}
        
class sss(models.Model):
    _name = 'info_helpdesk.sss'

    soru = fields.Text(string="Soru", required=True)
    cevap = fields.Text(string="Cevap", required=True)
    sorukisa  = fields.Char(string="Soru", compute="kisatext")
    cevapkisa  = fields.Char(string="Cevap", compute="kisatext")
    linkler  = fields.Char(string="Linkler", compute="kisatext")
    proje = fields.Char(string='Proje')

    video_rel = fields.Many2many('info_helpdesk.video', relation='info_helpdesk_sss_video_rel', string="Video Linkleri")
    kilavuz_rel = fields.Many2many('info_helpdesk.kilavuz', relation='info_helpdesk_sss_kilavuz_rel', string="Kılavuz Linkleri")

    @api.one
    def kisatext(self):
        getsoru = self.soru
        getcevap = self.cevap
        self.sorukisa = ucnokta(getsoru, 100, u'... DEVAMI')
        self.cevapkisa = ucnokta(getcevap, 100, u'... DEVAMI')


class video_baslik(models.Model):
    _name = 'info_helpdesk.video_header'

    @api.model
    def _get_dynamic_domian(self):
        print([('id', 'in', (1, 2, 3, 5, 20))])
        return [('id', 'in', (1, 2, 3, 5, 20))]

    name = fields.Char(string="Kategori", required=True)
    vids = fields.Many2many('info_helpdesk.video', string='Videolar')  # domain=_get_dynamic_domian)
    files = fields.Many2many('info_helpdesk.kilavuz', string='Dosyalar')
    level = fields.Selection(selection=[(1, 'Kullanıcı Videoları'),
                                        (2, 'Açık Teknik Servis Videoları'),
                                        (3, 'İnfoteks(Gizli) Teknik Servis Videoları')], string='Video Yetki Seviyesi',
                             required=True,default="1")
    proje = fields.Char(string='Proje')

    @api.onchange('vids')
    def onchange_vids(self):
        print('Fired Onchange Vids ', self.vids)
        return {'domain':{'vids':[('id','in',[2,3])]}}


class video(models.Model):
    _name = 'info_helpdesk.video'

    name = fields.Char(string="Video Adı", required=True)
    dosya = fields.Binary(string='Video', required=True)
    dosyaadi = fields.Char(string='Dosya Adı')
    path = fields.Char(string='path')
    dosya2 = fields.Binary(string='Ek Dosya')
    dosyaadi2 = fields.Char(string='Ek Dosya Adı')
    proje = fields.Char(string='Proje')

    @api.model
    def create(self, value):

        dosya_base64 = value.get("dosya")
        uzanti = value.get("dosyaadi").split(".")[-1]
        videoname = time.time()
        value["path"] = str(videoname) + "." + uzanti
        dosyaByte = base64.b64decode(dosya_base64)
        dir_path = os.path.dirname(os.path.realpath(__file__)) + "/uploads/" + str(videoname) + "." + uzanti
        videodosya = open(dir_path, "wb")
        videodosya.write(dosyaByte)
        videodosya.close()

        res = super(video, self).create(value)
        return res

    @api.multi
    def write(self, value):
        self.ensure_one()

        if value.get("dosya") != None :
            silinecekdosya = self.path
            os.remove(os.path.dirname(os.path.realpath(__file__)) + "/uploads/" + silinecekdosya)
            dosya_base64 = value.get("dosya")
            uzanti = value.get("dosyaadi").split(".")[-1]
            videoname = time.time()
            value["path"] = str(videoname) + "." + uzanti
            dosyaByte = base64.b64decode(dosya_base64)
            dir_path = os.path.dirname(os.path.realpath(__file__)) + "/uploads/" + str(videoname) + "." + uzanti
            videodosya = open(dir_path, "wb")
            videodosya.write(dosyaByte)
            videodosya.close()

        res = super(video, self).write(value)
        return res

    @api.multi
    def videoizle(self):
        self.ensure_one()
        view_id = self.env['ir.ui.view'].search([('name','=','info_helpdesk_video.form2')])[0].id
        others  = "select info_helpdesk_video_id from info_helpdesk_video_info_helpdesk_video_header_rel where info_helpdesk_video_header_id = ( select info_helpdesk_video_header_id from info_helpdesk_video_info_helpdesk_video_header_rel where  info_helpdesk_video_id = %s) order by info_helpdesk_video_id asc"%self.id
        self.env.cr.execute( others )
        ot      = self.env.cr.fetchall()
        ids     = list(map( lambda x:x[0],ot))
        
        res = {
            'type' : 'ir.actions.act_window',
            'res_model' : 'info_helpdesk.video',
            'res_id' : self.id,
            'target' : 'new',
            'view_mode' : 'form',
            'view_id' : view_id,
            'context' : {'playlist':ids}
        }
        return res
    @api.multi
    def videoindir(self):
        self.ensure_one()
        server_adres = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        final_url = server_adres  + "/web/content?model=info_helpdesk.video&field=dosya&id=%s&download=true&filename_field=dosyaadi"%self.id
        res = {
            'type':'ir.actions.act_url',
            'target':'current',
            'url':final_url
        }
        
        return res
    
    @api.model
    def get_url_header(self,**kw):
        paths = []
        names = []
        if kw.get('playlist'):    
            ps    = self.browse( kw.get('playlist'))
            for p in ps:
                paths.append( p.path )
                names.append( p.name )
        param  = self.env.get('ir.config_parameter').get_param( "video_server_adres")
        record = self.browse( kw.get('active_id'))
        
        return dict( url     = param,
                    playlist = paths,
                    names    = names,
                    path     = record.path)

def ucnokta(metin, limit, devam):
    yeni_limit = limit - len(devam)
    metinolustur = ''

    if(len(metin) > limit):
        yenimetin = metin[0:yeni_limit]
        metinparcalari = yenimetin.split(' ')
        metinparcalari.pop()

        for x in metinparcalari:
            metinolustur += ' '
            metinolustur += x

        metinolustur += devam
    else:
        metinolustur = metin

    return metinolustur