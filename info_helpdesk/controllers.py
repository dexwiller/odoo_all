# -*- coding: utf-8 -*-
from openerp import http
import os

class videocontent(http.Controller):

    @http.route('/info_helpdesk/uploads/<d>', type='http', auth='public')
    def export_xls_view(self, d):
        videoObj  = http.request.env['info_helpdesk.video'].sudo().browse([int(d)])
        filename = videoObj.path
        dir_path = os.path.dirname(os.path.realpath(__file__)) + "/uploads/" + filename
        videodosya = open(dir_path, "rb")
        fileBytes = videodosya.read();
        videodosya.close()
        return http.request.make_response(
           fileBytes,
           headers=[
               ('Content-Disposition', 'attachment; filename="%s"'
                % videoObj.dosyaadi),
                ('Content-Type', 'application/octet-stream')
            ],
        )