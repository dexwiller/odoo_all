# -*- coding: utf-8 -*-
# Part of Odoo. See COPYRIGHT & LICENSE files for full copyright and licensing details.

from odoo import models, api, fields


class GlobalSearchConfigBatch(models.Model):
    _name = 'global.search.config.batch'
    _rec_name = 'model_id'
    _description = 'Global Search Batch Configuration'

    template_id = fields.Many2one('global.search.config.template', 'Template', domian="[('id', in, [])]")
    searches_ids = fields.One2many('global.search.config', 'batch_id', 'User(s)', required=False, readonly=True)
    customized = fields.Boolean('Customized')
    model_id = fields.Many2one('ir.model', 'Model', required=True, )
    model_name = fields.Char(related='model_id.model', string='Model Name', readonly=True, store=True)
    field_ids = fields.Many2many('ir.model.fields', string='Fields', readonly=True)
    search_field_ids = fields.Char('Search Fields')

    _sql_constraints = [
        ('uniq_model',
         "UNIQUE(model_id)",
         "The model must be unique."),
    ]

    @api.multi
    def write(self, vals):
        '''Override to manage customized boolean'''
        if 'customized' not in vals and ((vals.get('user_id') and len(vals.keys())>1) or not vals.get('user_id')):
            vals['customized'] = True
        if 'template_id' in vals and not vals.get('model_id', False):
            vals['model_id'] = self.env['global.search.config.template'].search([('id','=',vals.get('template_id'))]).model_id.id
        if 'search_field_ids' in vals:
            domain_list = eval(vals['search_field_ids'] or '[]')
            search_fields = []
            model_id = 'model_id' in vals and vals['model_id'] or self.model_id.id
            for rec in domain_list:
                if type(rec) is list:
                    if rec[0].find('.') != -1:
                        search_fields.append(rec[0].split('.', 1)[0])
                    else:
                        search_fields.append(rec[0])
            if search_fields:
                field_ids = self.env['ir.model.fields'].search([('model_id', '=', model_id), ('name', 'in', search_fields)])
                vals['field_ids'] = field_ids and [(6, 0, field_ids.ids)]
            else:
                vals['field_ids'] = [(6, 0, [])]
        res = super(GlobalSearchConfigBatch, self).write(vals)
        return res

    @api.model
    def create(self, vals):
        '''Override check the values'''
        if 'template_id' in vals and not vals.get('model_id', False):
            vals['model_id'] = self.env['global.search.config.template'].search([('id','=',vals.get('template_id'))]).model_id.id
        if 'search_field_ids' in vals and vals.get('search_field_ids', False):
            domain_list = eval(vals['search_field_ids'])
            search_fields = []
            model_id = 'model_id' in vals and vals['model_id']
            for rec in domain_list:
                if type(rec) is list:
                    if rec[0].find('.') != -1:
                        search_fields.append(rec[0].split('.', 1)[0])
                    else:
                        search_fields.append(rec[0])
            if search_fields:
                field_ids = self.env['ir.model.fields'].search([('model_id', '=', model_id), ('name', 'in', search_fields)])
                vals['field_ids'] = field_ids and [(6, 0, field_ids.ids)]
        return super(GlobalSearchConfigBatch, self).create(vals)

    @api.onchange('template_id')
    def _onchange_template_id(self):
        '''To set fields as per template selection.'''
        for rec in self:
            rec.search_field_ids = rec.template_id.search_field_ids
            rec.field_ids = [(6, 0, rec.template_id.field_ids.ids)]
            rec.model_id = rec.template_id.model_id.id
            rec.customized = False

    @api.onchange('model_id')
    def _onchange_model_id(self):
        if self.template_id:
            self._onchange_template_id()
        else:
            self.field_ids = [(6, 0, [])]
            self.search_field_ids = '[]'

    @api.multi
    def apply_changes_in_searches(self):
        """ Its calling for effects on searches."""
        for rec in self.searches_ids:
            if not rec.customized:
                rec.set_values_template_batch(rec.batch_id)
        return True
