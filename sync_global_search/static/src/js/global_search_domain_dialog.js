odoo.define("sync_global_search.GlobalDomainSelectorDialog", function (require) {
"use strict";

    var core = require("web.core");
    var Dialog = require("web.Dialog");
    var GlobalDomainSelector = require("sync_global_search.GlobalSearchDomainSelector");

    var _t = core._t;

    var GlobalDomainSelectorDialog =  Dialog.extend({
        init: function (parent, model, domain, options) {
            this.model = model;
            this.options = _.extend({
                readonly: true,
                debugMode: false,
            }, options || {});

            var buttons;
            if (this.options.readonly) {
                buttons = [
                    {text: _t("Close"), close: true},
                ];
            } else {
                buttons = [
                    {text: _t("Save"), classes: "btn-primary", close: true, click: function () {
                        this.trigger_up("domain_selected", {domain: this.domainSelector.getDomain()});
                    }},
                    {text: _t("Discard"), close: true},
                ];
            }

            this._super(parent, _.extend({}, {
                title: _t("Domain"),
                buttons: buttons,
            }, options || {}));

            this.domainSelector = new GlobalDomainSelector(this, model, domain, options);
        },
        start: function () {
            var self = this;
            this.opened().then(function () {
                self.$el.css('overflow', 'visible').closest('.modal-dialog').css('height', 'auto');
            });
            return $.when(
                this._super.apply(this, arguments),
                this.domainSelector.appendTo(this.$el)
            );
        },
    });
    return GlobalDomainSelectorDialog;
});