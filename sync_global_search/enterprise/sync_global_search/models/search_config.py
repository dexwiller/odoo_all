# -*- coding: utf-8 -*-
# Part of Odoo. See COPYRIGHT & LICENSE files for full copyright and licensing details.

from odoo import models, api, fields


class GlobalSearchConfigTemplate(models.Model):
    _name = 'global.search.config.template'
    _rec_name = 'model_id'
    _description = 'Global Search Configuration Template'

    @api.multi
    def _search_model(self):
        '''Returns model list which are accesible for login user'''
        models = self.env['ir.model'].search([('state', '!=', 'manual'), ('transient', '=', False)])
        access_model = self.env['ir.model.access']
        model_ids = [model.id for model in models if access_model.sudo(user=self.env.user.id).check(model.model, 'read', raise_exception=False)]
        return [('id', 'in', model_ids)]

    model_id = fields.Many2one('ir.model', 'Model', domain=_search_model, required=True)
    model_name = fields.Char(related='model_id.model', string='Model Name', readonly=True, store=True)
    field_ids = fields.Many2many('ir.model.fields', string='Fields', readonly=True)
    search_field_ids = fields.Char('Search Fields')

    _sql_constraints = [
        ('uniq_model',
         "UNIQUE(model_id)",
         "The Model must be unique."),
    ]

    @api.onchange('model_id')
    def _onchange_model_id(self):
        self.field_ids = [(6, 0, [])]
        self.search_field_ids = '[]'

    @api.multi
    def apply_changes_in_searches(self):
        """ Its calling for effects on searches."""
        searches = self.env['global.search.config'].search([('batch_id', '=', False), ('template_id', 'in', self.ids)])
        for rec in searches:
            if not rec.customized:
                rec.set_values_template_batch(rec.template_id)
        return True

    @api.multi
    def write(self, vals):
        if 'search_field_ids' in vals:
            domain_list = eval(vals['search_field_ids'] or '[]')
            search_fields = []
            model_id = 'model_id' in vals and vals['model_id'] or self.model_id.id
            for rec in domain_list:
                if type(rec) is list:
                    if rec[0].find('.') != -1:
                        search_fields.append(rec[0].split('.', 1)[0])
                    else:
                        search_fields.append(rec[0])
            if search_fields:
                field_ids = self.env['ir.model.fields'].search([('model_id', '=', model_id), ('name', 'in', search_fields)])
                vals['field_ids'] = field_ids and [(6, 0, field_ids.ids)]
            else:
                vals['field_ids'] = [(6, 0, [])]
        return super(GlobalSearchConfigTemplate, self).write(vals)

    @api.model
    def create(self, vals):
        if 'search_field_ids' in vals and vals.get('search_field_ids', False):
            domain_list = eval(vals['search_field_ids'])
            search_fields = []
            model_id = 'model_id' in vals and vals['model_id']
            for rec in domain_list:
                if type(rec) is list:
                    if rec[0].find('.') != -1:
                        search_fields.append(rec[0].split('.', 1)[0])
                    else:
                        search_fields.append(rec[0])
            if search_fields:
                field_ids = self.env['ir.model.fields'].search([('model_id', '=', model_id), ('name', 'in', search_fields)])
                vals['field_ids'] = field_ids and [(6, 0, field_ids.ids)]
        return super(GlobalSearchConfigTemplate, self).create(vals)


class GlobalSearchConfig(models.Model):
    _name = 'global.search.config'
    _rec_name = 'model_id'
    _description = 'Global Search Configuration'

    template_id = fields.Many2one('global.search.config.template', 'Template', domian="[('id', in, [])]")
    batch_id = fields.Many2one('global.search.config.batch', string="Batch")
    user_id = fields.Many2one('res.users', 'User', default=lambda self: self.env.user, copy=False)
    customized = fields.Boolean('Customized')
    model_id = fields.Many2one('ir.model', 'Model', required=True, )
    model_name = fields.Char(related='model_id.model', string='Model Name', readonly=True, store=True)
    field_ids = fields.Many2many('ir.model.fields', string='Fields', readonly=True)
    search_field_ids = fields.Char('Search Fields')

    _sql_constraints = [
        ('uniq_template_user',
         "UNIQUE(model_id, user_id)",
         "The Model must be unique per user."),
    ]

    @api.multi
    def write(self, vals):
        '''Override to manage customized boolean'''
        if 'customized' not in vals and ((vals.get('user_id') and len(vals.keys())>1) or not vals.get('user_id')):
            vals['customized'] = True
        if 'template_id' in vals and not vals.get('model_id', False):
            vals['model_id'] = self.env['global.search.config.template'].search([('id','=',vals.get('template_id'))]).model_id.id
        if 'search_field_ids' in vals:
            domain_list = eval(vals['search_field_ids'] or '[]')
            search_fields = []
            model_id = 'model_id' in vals and vals['model_id'] or self.model_id.id
            for rec in domain_list:
                if type(rec) is list:
                    if rec[0].find('.') != -1:
                        search_fields.append(rec[0].split('.', 1)[0])
                    else:
                        search_fields.append(rec[0])
            if search_fields:
                field_ids = self.env['ir.model.fields'].search([('model_id', '=', model_id), ('name', 'in', search_fields)])
                vals['field_ids'] = field_ids and [(6, 0, field_ids.ids)]
            else:
                vals['field_ids'] = [(6, 0, [])]
        return super(GlobalSearchConfig, self).write(vals)

    @api.model
    def create(self, vals):
        '''Override check the values'''
        if 'template_id' in vals and not vals.get('model_id', False):
            vals['model_id'] = self.env['global.search.config.template'].search([('id','=',vals.get('template_id'))]).model_id.id
        if 'search_field_ids' in vals and vals.get('search_field_ids', False):
            domain_list = eval(vals['search_field_ids'])
            search_fields = []
            model_id = 'model_id' in vals and vals['model_id']
            for rec in domain_list:
                if type(rec) is list:
                    if rec[0].find('.') != -1:
                        search_fields.append(rec[0].split('.', 1)[0])
                    else:
                        search_fields.append(rec[0])
            if search_fields:
                field_ids = self.env['ir.model.fields'].search([('model_id', '=', model_id), ('name', 'in', search_fields)])
                vals['field_ids'] = field_ids and [(6, 0, field_ids.ids)]
        return super(GlobalSearchConfig, self).create(vals)

    @api.onchange('user_id')
    def _onchange_user_id(self):
        '''Returns domain to filter template whose model are accessible for selected user.'''
        dom = {'template_id': [('id', 'in', [])]}
        if self.user_id:
            models = self.env['ir.model'].search([('state', '!=', 'manual'), ('transient', '=', False)])
            access_model = self.env['ir.model.access']
            model_ids = [model.id for model in models if access_model.sudo(user=self.user_id.id).check(model.model, 'read', raise_exception=False)]
            dom['template_id'] = [('model_id', 'in', model_ids)]
            dom['model_id'] = [('id', 'in', model_ids)]
        return {'domain': dom}

    @api.onchange('template_id')
    def _onchange_template_id(self):
        '''To set fields as per template selection.'''
        for rec in self:
            rec.set_values_template_batch(rec.template_id)

    @api.onchange('model_id')
    def _onchange_model_id(self):
        if self.template_id:
            self._onchange_template_id()
        else:
            self.field_ids = [(6, 0, [])]
            self.search_field_ids = '[]'

    @api.multi
    def set_values_template_batch(self, template_batch_id):
        for rec in self:
            rec.search_field_ids = template_batch_id.search_field_ids
            rec.field_ids = [(6, 0, template_batch_id.field_ids.ids)]
            rec.model_id = template_batch_id.model_id.id
            rec.customized = False

    @api.multi
    def set_default_template_batch(self):
        '''Set default button.
        To set fields as per template or batch selection. '''
        for rec in self:
            rec.set_values_template_batch(rec.batch_id or rec.template_id)
