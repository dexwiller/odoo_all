# -*- coding: utf-8 -*-
# Part of Odoo. See COPYRIGHT & LICENSE files for full copyright and licensing details.

import odoo
from odoo import http, _
from odoo.addons.web.controllers.main import *
from odoo.http import request
import numbers


class GlobalSearch(http.Controller):

    @http.route('/globalsearch/model_fields', type='json', auth="public")
    def search_model_fields(self, **kwargs):
        '''This function prepares values for autocomplete 'Search for <model:name>:'
        it returns Models whose template is assigned to current login user.
        '''
        GS = request.env['global.search.config'].sudo().search([('user_id', '=', request.env.user.id)])
        result = dict([(gs.model_id.name, gs.model_id.model) for gs in GS if len(gs.field_ids) > 0])
        return result

    @http.route('/globalsearch/search_data', type='json', auth="public")
    def search_data(self, **kwargs):
        '''This function returns data for partucular model's search expand.'''
        search_string = kwargs['search_string']
        GS = request.env['global.search.config'].sudo().search([('user_id', '=', request.env.user.id), ('model_id.model', '=', kwargs['model'])], limit=1)
        try:
            if len(GS.field_ids) > 0:
                fields = dict([(field.name, (field.field_description, field.relation)) for field in GS.field_ids])
                dom = eval(GS.search_field_ids)
                search_fields = []
                child_fields = []
                search_domain = []
                for domain in dom:
                    if type(domain) is list:
                        if domain[0].find('.') != -1:
                            field = domain[0].split('.', 1)
                            search_fields.append(field[0])
                            child_fields.append((fields[field[0]][1], field[-1]))
                        else:
                            search_fields.append(domain[0])
                        domain[1] = 'ilike'
                        domain[2] = search_string
                        domain = tuple(domain)
                    search_domain.append(domain)

                search_fields.append('display_name')
                datas = request.env[kwargs['model']].search_read(search_domain, list(set(search_fields)))
                for data in datas:
                    for f,v in list(data.items()):
                        if f == 'display_name':
                            continue
                        if type(v) in [list, tuple]:
                            if data[f]:
                                if type(v) is list:
                                    records = request.env[fields[f][1]].browse(data[f])
                                    x2m_data = records.name_get()
                                    x2m_v = ', '.join([d[1] for d in x2m_data if search_string.lower() in d[1].lower()])
                                elif type(v) is tuple:
                                    x2m_data = [data[f]]
                                    records = request.env[fields[f][1]].browse(data[f][0])
                                    x2m_v = data[f][1].lower()
                                if x2m_v:
                                    data[fields[f][0]] = x2m_v
                                for child_field in child_fields:
                                    x2m_v = ""
                                    if child_field[0] == fields[f][1]:
                                        for d in records:
                                            field_list = child_field[1].rsplit('.', 2)
                                            if len(field_list) > 1:
                                                current_model = eval("d.mapped('" + ".".join(field_list[:-1]) + "')._name")
                                                current_field_type = request.env[current_model]._fields[field_list[-1]].type
                                                field_name = eval("d.mapped('" + child_field[1] + "')")

                                                if current_field_type in ['char', 'float', 'datetime', 'date', 'text', 'integer']:
                                                    for list_rec in field_name:
                                                        list_rec = str(list_rec)
                                                        if search_string.lower() in list_rec.lower():
                                                            x2m_v = x2m_v and ', '.join([x2m_v, list_rec]) or list_rec
                                                elif current_field_type in ['one2many', 'many2many', 'many2one']:
                                                    field_name = eval("d.mapped('" + child_field[1] + "').name_get()")
                                                    x2m_v = set([name[1] for name in field_name if search_string.lower() in name[1].lower()])
                                                    x2m_v = ', '.join(list(x2m_v))
                                                elif current_field_type == 'boolean':
                                                    for list_rec in field_name:
                                                        list_rec = str(list_rec)
                                                        if search_string.lower() == list_rec.lower():
                                                            x2m_v = x2m_v and ', '.join([x2m_v, list_rec]) or list_rec
                                                elif current_field_type == 'selection':
                                                    for item in field_name:
                                                        selection = dict(request.env[current_model]._fields[field_list[-1]]._description_selection(request.env)).get(item)
                                                        if search_string.lower() in selection.lower():
                                                            x2m_v = x2m_v and ', '.join([x2m_v, selection]) or selection

                                            elif len(field_list) == 1:
                                                current_model = eval("d._name")
                                                current_field_type = request.env[current_model]._fields[child_field[1]].type
                                                field_name = str(eval("d." + child_field[1]))

                                                if current_field_type in ['char', 'float', 'datetime', 'date', 'text', 'integer']:
                                                    if field_name and search_string.lower() in field_name.lower():
                                                        x2m_v = x2m_v and ', '.join([x2m_v, field_name]) or field_name
                                                elif current_field_type in ['one2many', 'many2many', 'many2one']:
                                                    field_name = eval("d." + child_field[1] + ".name_get()")
                                                    x2m_v = ', '.join([name[1] for name in field_name if search_string.lower() in name[1].lower()] + [x2m_v])
                                                elif current_field_type == 'boolean':
                                                    if field_name and search_string.lower() == field_name.lower():
                                                        x2m_v = x2m_v and ', '.join([x2m_v, field_name]) or field_name
                                                elif current_field_type == 'selection':
                                                    selection = dict(request.env[current_model]._fields[child_field[1]]._description_selection(request.env)).get(field_name)
                                                    if search_string.lower() in selection.lower():
                                                        x2m_v = x2m_v and ', '.join([x2m_v, selection]) or selection

                                            # for label Generation
                                            label_field_list = child_field[1].split('.')
                                            field_count = 1
                                            previous_field_label = ""
                                            for rec in label_field_list:
                                                previous_model = eval("d.mapped('" + ".".join(label_field_list[:field_count-1]) + "')._name") or fields[f][1]
                                                previous_field_label = ' > '.join([previous_field_label, request.env[previous_model]._fields[rec].get_description(request.env).get('string')])
                                                field_count += 1
                                        field_label = ''.join([fields[f][0], previous_field_label])
                                        if x2m_v:
                                            data[field_label] = x2m_v
                        else:
                            if isinstance(data[f], numbers.Number) and type(data[f]) is not bool:
                                data[f] = str(data[f])
                            if f in fields:
                                if data[f] and search_string.lower() in str(data[f]).lower():
                                    data[fields[f][0]] = data[f]
                        if f != 'id':
                            del data[f]
                        else:
                            data['id'] = v
                        if f in fields:
                            if data.get(fields[f][0]) and kwargs['search_string'].lower() not in str(data[fields[f][0]]).lower():
                                del data[fields[f][0]]
                    data['model'] = kwargs['model']
                return datas or [{'label': '(no result)'}]
            return [{'label': '(no result)'}]
        except KeyError as e:
            raise UserError(_("Incorrect configuration, please check it.\n\n%s not found") % (e))
