odoo.define('search_tree_custom.custom_search', function (require) {
    "use strict";
    var rpc = require('web.rpc');
    var model = require('web.BasicModel');
    var core = require('web.core');
    require('web.dom_ready');
    var widgetRegistry = require('web.widget_registry');
    var Widget = require('web.Widget');
    var QWeb = core.qweb;

    var registry = require('web.field_registry');
    widgetRegistry.add('custom_search', Widget.extend({
        template: 'search_tree_custom.custom_search',
        events: {
            'click .search_button': '_onClickAttachSearchButton',
        },
        init: function(parent,model,domain) {
            this._super.apply(this, arguments);
            this.attrs = domain.attrs;
            this.model = model;

        },
        xmlDependencies: ['/search_tree_custom/static/xml/templates.xml'],
        _onClickAttachSearchButton: function (event) {
            event.stopPropagation();
            var self = this;
            var search_value = $('#search_input').val().toUpperCase();

            console.log( self )
            /*for (var key in self.__parentedParent.__parentedParent) {
                  console.log( key )
                  console.log( self.__parentedParent.__parentedParent.model )
                }
            */
            self.__parentedParent.state.data[self.attrs.field_name].data = self.__parentedParent.state.data[self.attrs.field_name].data.slice(0,1);
            //console.log( self.__parentedParent.state.data[self.attrs.field_name].data );
            if (self.__parentedParent.state.data[self.attrs.field_name].data.length > 0){
                console.log( this.attrs.method_name );
                rpc.query({
                        model: 'base.formsearch',
                        method: this.attrs.method_name,
                        args:   [search_value,this.attrs],
                        kwargs: self.__parentedParent.state
                }).then(function(result) {

                        console.log( result );

                        self.__parentedParent.state.data[self.attrs.field_name].res_ids = result.data.res_ids;
                        self.__parentedParent.state.data[self.attrs.field_name].count  = result.data.count;

                        var dset = [];

                        for (var i=0;i<result.data.data.length;i++){
                            var d = result.data.data[i];
                            var dataPoint = self.__parentedParent.__parentedParent.model._makeDataPoint( d );
                            dset.push( dataPoint );
                        }

                        console.log( dset );

                        self.__parentedParent.state.data[self.attrs.field_name].data = dset;
                        self.__parentedParent._render();
                        //self.__parentedParent.__parentedParent.renderPager();

                  });

            }
            else{
                self.__parentedParent._render();
            }
        },
    }));

});