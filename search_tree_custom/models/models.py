# -*- coding: utf-8 -*-

from odoo import models, fields, api
from copy import copy
class formsearch( models.Model ):
	_name = 'base.formsearch'

	@api.model
	def do_one2many_search(self,search_value,attrs,**kw):
		field_name 			= attrs['field_name']
		active_id 			= kw.get('res_id')
		model_name 			= kw.get('model')
		search_model_name 	= attrs['search_model_name']
		relation_field_name = attrs['relation_field_name']
		model = self.env.get(model_name)

		relational_model = model._fields[field_name].comodel_name

		search_model = self.env.get(relational_model)
		domain_base = ['&', (relation_field_name, '=', int(active_id))]
		domain = []

		for name, field in search_model._fields.items():
			if not field.related:
				if isinstance(field, (fields.Char)):
					if field.store and not field.company_dependent:
						domain.append((name, 'ilike', search_value))
				if isinstance(field, (fields.Many2one)):
					if field.comodel_name in search_model_name:
						sub_model = self.env.get(field.comodel_name)
						for s_name, s_field in sub_model._fields.items():
							if not s_field.related:
								if isinstance(s_field, (fields.Char)):
									if s_field.store and not s_field.company_dependent:
										domain.append(('%s.%s' % (name, s_name), 'ilike', search_value))
								if isinstance(s_field, (fields.Many2one)):
									if s_field.comodel_name in search_model_name:
										sub_sub_model = self.env.get(s_field.comodel_name)
										for ss_name, ss_field in sub_sub_model._fields.items():
											if not ss_field.related:
												if isinstance(ss_field, (fields.Char)):
													if ss_field.store and not ss_field.company_dependent:
														domain.append(
															('%s.%s.%s' % (name, s_name, ss_name), 'ilike', search_value))

		ors_count = len(domain) - 1
		ors = list('|' * ors_count)
		domain = domain_base + ors + domain

		print (domain)

		fdata = kw.get('data').get(field_name)
		st_obj = fdata.get('data')[0]
		ffields = fdata.get('fields').keys()
		if 'id' not in ffields:
			ffields.append('id')
		ids = search_model.search_read(domain, fields=ffields)
		fdataset = []
		for d in ids[:fdata.get('limit')]:
			fobj = copy(st_obj)
			fobj['data'] = d
			fobj['modelName'] = fobj['model']
			fobj['res_id'] = d.get('id')
			fdataset.append(fobj)
		fdata['data'] = fdataset
		fdata['count'] = len(ids)
		fdata['res_ids'] = [x.get('id') for x in ids]

		print (fdata)

		return {'data': fdata}

	@api.model
	def do_many2many_search(self,search_value,attrs,**kw):

		field_name 			= attrs['field_name']
		active_id 			= kw.get('res_id')
		model_name 			= kw.get('model')
		search_model_name 	= attrs['search_model_name']

		model = self.env.get( model_name )
		relational_model       =  model._fields[field_name].relation
		relational_base_column =  model._fields[field_name].column1
		relational_column      =  model._fields[field_name].column2
		self.env.cr.execute("""select %s from %s where %s = %s"""%( relational_column,
																		  relational_model,
																		  relational_base_column,
																		  int(active_id)))
		result         = self.env.cr.fetchall()
		relational_ids = list(map(lambda x:x[0],result))
		domain_base   = ['&',('id','in',relational_ids)]
		domain        = []
		search_model= self.env.get(model._fields[field_name].comodel_name)
		for name, field in search_model._fields.items():
			if not field.related:
				if isinstance(field, (fields.Char)):
					if field.store and not field.company_dependent:
						domain.append( (name,'ilike',search_value))
				if isinstance(field, (fields.Many2one)):
					if field.comodel_name in search_model_name:
						sub_model = self.env.get(field.comodel_name)
						for s_name,s_field in sub_model._fields.items():
							if not s_field.related:
								if isinstance(s_field, (fields.Char)):
									if s_field.store and not s_field.company_dependent:
										domain.append( ('%s.%s'%(name,s_name),'ilike',search_value) )
								if isinstance(s_field, (fields.Many2one)):
									if s_field.comodel_name in search_model_name:
										sub_sub_model = self.env.get(s_field.comodel_name)
										for ss_name,ss_field in sub_sub_model._fields.items():
											if not ss_field.related:
												if isinstance(ss_field,(fields.Char)):
													if ss_field.store and not ss_field.company_dependent:
														domain.append(('%s.%s.%s'%(name,s_name,ss_name),'ilike',search_value))
		ors_count = len(domain) - 1
		ors = list('|'*ors_count)
		domain = domain_base + ors + domain

		fdata 		 = kw.get('data').get(field_name)
		st_obj       = fdata.get('data')[0]
		ffields      = fdata.get('fields').keys()
		if 'id' not in ffields:
			ffields.append('id')
		ids    		= search_model.search_read(domain, fields=ffields)
		fdataset    = []
		for d in ids[:fdata.get('limit')]:
			fobj              = copy( st_obj )
			fobj['data']      = d
			fobj['modelName'] = fobj['model']
			fobj['res_id']    = d.get('id')
			fdataset.append( fobj )
		fdata['data'] 	 = fdataset
		fdata['count']   = len(ids)
		fdata['res_ids'] = [x.get('id') for x in ids]

		return {'data': fdata}

