# -*- coding: utf-8 -*-
{
    'name': "Customized Search",
    'summary': """The solution to make your calls easier.""",
    'description': """
Custom Search
===============
You can search your searches in all columns in the table.
    """,
    'author': "Infoteks Teknology",
    'website': "http://www.infoteks.com.tr",
    'category': 'Project',
    'version': '0.1',
    'depends': ['base'],
    'data': [
        # 'security/ir.model.access.csv',
        'static/xml/assets.xml',
        'views/views.xml',
    ],
    'demo': [
        'demo/demo.xml',
    ],
}