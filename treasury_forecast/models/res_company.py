# Copyright 2018 Giacomo Grasso <giacomo.grasso.82@gmail.com>
# Odoo Proprietary License v1.0 see LICENSE file

from odoo import api, fields, models, _


class ResCompany(models.Model):
    _inherit = "res.company"

    fc_css_dict = fields.Text(
        string='Dictionary with colours',
        default=lambda self: self.default_fc_css())

    def default_fc_css(self):
        css = """{
            '': '',
            'BNK': '#FFFFFF',
            'FBK': '#D4EFDF',
            'FPL': '#FAFAD2',
            'DFT': '#D7DBDD',
        }"""
        return css
