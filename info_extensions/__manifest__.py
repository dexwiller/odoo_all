# -*- coding: utf-8 -*-
{
    'name': "İnfoteks Eklentiler",

    'summary': """İnfoteks openerp eklentileri ve küçük modüller""",

    'description': """
        Openerp yapısı için hazırlanan tüm eklentiler ve küçük modüller bu başlık altında toplanmaktadır.
    """,

    'author': "infoteks arge",
    'website': "http://www.infoteks.com.tr",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','stock', 'account','info_helpdesk','search_tree_custom'],

    # always loaded
    'data': [
        'security/groups.xml',
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/assets.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'qweb':[
        'static/xml/one2many_check.xml',
    ],

    'installable': True,
    'application': True,
    'auto_install': False,

}