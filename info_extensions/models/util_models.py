# -*- coding: utf-8 -*-

from odoo import models, fields, api


class logging(models.Model):
    '''
    classdocs
    '''
    _name = 'info_extensions.logging'

    serviceName = fields.Char(string='serviceName')
    operationName = fields.Char(string='operationName')
    requestTime = fields.Datetime(string='requestTime')
    responseTime = fields.Datetime(string='responseTime')
    requestData = fields.Char(string='requestData')
    responseData = fields.Char(string='responseData')
    write_date = fields.Datetime(string="WriteDate", required=False, )
    _order = 'id desc'