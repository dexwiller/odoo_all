# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions


class res_partner(models.Model):
    _inherit = 'res.partner'
    _order = 'name asc'

    firma_unvani = fields.Char(string='Firma Ünvanı')
    taxAgent = fields.Many2one('info_extensions.vergidaireleri', ondelete='restrict', string='Vergi Dairesi', size=50,
                               select=True)
    taxAgentCity = fields.Many2one('info_extensions.iller', string="Vergi Dairesi İli", related='taxAgent.il_id')

    taxNumber = fields.Char(string='Vergi Numarasi', size=10)

    city_combo = fields.Many2one('info_extensions.iller', ondelete='restrict', string="İller", required=True)
    ilce = fields.Many2one('info_extensions.ilceler', ondelete='restrict', string="İlçe", required=True)
    plaka = fields.Char(string="Plaka Kodu", related='city_combo.plaka')

    street = fields.Char('Firma Adresi', required=True)
    tckn = fields.Char(string='Kimlik Numarasi', size=11)
    mersisNumber = fields.Char(string='Mersis Numarasi', size=16, select=True)

    firmaKodu = fields.Char(string='Firma Kodu', size=10, select=True)
    # plaka = fields.Char(string="Plaka Kodu", related='city_combo.plaka')
    sahis_sirketi = fields.Boolean(string="Şahıs Şirketi")
    sermaye_sirketi = fields.Boolean(string="Sermaye Şirketi")
    ortaklik = fields.Boolean(string="Ortaklık ve Diğer")

    yetkili_ismi = fields.Char(string='Yetkili İsmi')

    is_company = fields.Boolean(string='Is a Company', default=True)
    sube_tanim = fields.Char(string='İlçe Şube Tanımı')
    balance_count = fields.Float(compute='_get_balance_monetry')
    # arrear_count                    = fields.Float(compute='_get_arrear_monetry')
    extre_alt_journallar_dahil = fields.Selection(
        selection=[(1, 'Sadece Kendi Defterlerimi Dahil Et'), (2, 'Ana Bayi Defterlerini Dahil Et'),
                   (3, 'Ana Bayi ve Alt Bayi Defterleri Birlikte')], string="Ekstre Yöntemi")
    type = fields.Selection([('contact', 'Contact'), ('invoice', 'Invoice address'), ('delivery', 'Shipping address'),
                             ('other', 'Other address')], string='Address Type', default='other',
                            help="Used to select automatically the right address according to the context in sales and purchases documents.")
    company_type = fields.Selection(string='Company Type', selection=[('person', 'Individual'), ('company', 'Company')],
                                    compute='_compute_company_type', readonly=False, default="company")

    adres = fields.Char(string="Merkez Adresi", compute="merkezadresi")
    ust_firma = fields.Char(string="Üst birim", compute="ustfirma")
    country_code_rel = fields.Char(related='country_id.code')
    street2 = fields.Char()
    child_ids_ = fields.Many2many(comodel_name='res.partner', relation='res_partner_frenchise', column1='partner_id',
                                  column2='frenchise_id', string='Franchise',
                                  domain=['&', ('active', '=', True), ('is_company', '=', True)])
    contacts = fields.One2many('info_extensions.partner_contact', 'partner')
    sube_contacts = fields.One2many('info_extensions.partner_contact', related='parent_id.contacts')
    sube_sayisi = fields.Char(string="Şube Sayısı", compute="subesayisi")
    frenchise_sayisi = fields.Char(string="Frenchise Sayısı", compute="frenchisesayisi")
    firma_oznitelik = fields.Integer(compute='get_firma_oznitelik')
    state = fields.Selection(selection=[('predraft', 'Üye İş Yeri Girişi'),
                                        ('draft', 'Başvuru Aşamasında - Eksik Bilgileri Tamamlayınız.'),
                                        ('sended', 'E-Posta Gönderildi, Belgeler Bekleniyor.'),
                                        ('waiting_confirm', 'Onay Bekliyor'),
                                        ('turkpara_onay_bekleniyor', 'Merkez Onayı Bekleniyor'),
                                        ('confirmed', 'Onaylandı'),
                                        ('missing_parameters', 'Eksik Evrak'),
                                        ('cancel', 'İptal Edildi'),
                                        ('declined', 'Reddedildi'),
                                        ('document_change', 'Evraklar Değişime Açık'),
                                        ('islak_bekleniyor', 'Islak İmzalı Evraklar Bekleniyor'),
                                        ('turkpara_ws_error', 'Servis Gönderimi Başarısız'),
                                        ('musteri_istemiyor', 'Müşteri İstemiyor'),
                                        ('banka_posu', 'Banka Posu Sorunu'),
                                        ('aktiflerle_calismayacak', 'Aktif Bankalarla Çalışmak İstemiyor'),
                                        ('bankaya_iletildi', 'İlgili Bankaya İletildi'),
                                        ('infoteks_alacak', 'İnfoteks Yazarkasa İstiyor'),
                                        ('dusunecekmis', 'Düşünmek İstiyor'),
                                        ('istemiyor_poslu', 'İstemiyor (Aktif Bankası Var)'),
                                        ('istemiyor_possuz', 'İstemiyor (Aktif Bankası Yok)'),
                                        ('bankayla_anlasamadi', 'Banka İle Anlaşamadı'),
                                        ('slip_alinacak', 'Slip Görüntüsü Alınacak'),
                                        ('giris', 'Girişi Yapıldı'),
                                        ('muhasebe', 'Muhasebe Arşiv'),
                                        ],
                             default='predraft')
    mail_kargo_gonderildi_mi = fields.Integer()  # 1:mail, 2:kargo send 3:kargo received

    @api.multi
    def get_firma_oznitelik(self):
        for s in self:

            if s.state == 'sended' or s.mail_kargo_gonderildi_mi == 1:
                s.firma_oznitelik = 9
            elif s.mail_kargo_gonderildi_mi == 2:
                s.firma_oznitelik = 3
            elif s.mail_kargo_gonderildi_mi == 3:
                s.firma_oznitelik = 1

    @api.one
    def _get_arrear_monetry(self):
        arrear = 0

        partner_ids = [self.id]

        partner_ids.extend(list(map(lambda x: x.id, self.company_id)))

        invoices = self.env['account.invoice'].search(
            [('type', '=', 'out_invoice'), ('partner_id', 'in', partner_ids), ('state', '=', 'open')])
        final_aml_ids = []

        for i in invoices:
            arrear += i.residual

        journal_ids = self.env['account.journal'].search(
            [('type', 'in', ('cash', 'bank')), ('company_id', '=', self.env.user.company_id.id)]).ids
        domain = [('partner_id', '=', self._find_accounting_partner(self).id), ('reconciled', '=', False),
                  ('amount_residual', '!=', 0.0), ('journal_id', 'in', journal_ids)]

        domain.extend([('credit', '>', 0), ('debit', '=', 0)])

        lines = self.env['account.move.line'].search(domain)

        currency_id = self.currency_id

        if len(lines) != 0:
            for line in lines:
                # get the outstanding residual value in invoice currency
                if line.currency_id and line.currency_id == self.env.user.company_id.currency_id:
                    amount_to_show = abs(line.amount_residual_currency)
                else:
                    amount_to_show = line.company_id.currency_id.with_context(date=line.date).compute(
                        abs(line.amount_residual), self.env.user.company_id.currency_id)
                if float_is_zero(amount_to_show, precision_rounding=self.env.user.company_id.currency_id.rounding):
                    continue
                arrear -= amount_to_show

        ##print "------------> ",  arrear

        self.arrear_count = arrear

    @api.one
    def _get_balance_monetry(self):
        balance = 0
        domain = []
        if self.id == 1:
            self.balance_count = 0.0
            return
        if self.id == self.env.user.company_id.partner_id.id:
            # kendine bakiyo, ust journallari islet.
            # print '1 : ',  self.id
            domain = [('company_id', '=', self.env.user.company_id.parent_id.sudo().id)]

        else:
            # yallah tazyik.
            ##print '2 : ',self.id
            domain = [('company_id', '=', self.env.user.company_id.id)]
        journal_ids = self.env['account.journal'].sudo().search(domain)
        ##print journal_ids
        data = {}
        data['ids'] = [self.id]
        data['model'] = u'res.partner'
        data['form'] = {}
        data['form']['used_context'] = dict({'strict_range': False,
                                             'state': u'all',
                                             'date_to': False,
                                             'journal_ids': journal_ids.ids,
                                             'date_from': False,
                                             }, lang=self.env.context.get('lang') or 'en_US')

        data['form'].update({'reconciled': True,
                             'id': self.id,
                             'target_move': u'all',
                             'amount_currency': False,
                             'custom_partner_ids': [self.id],
                             'journal_ids': journal_ids.ids,
                             'date_to': False,
                             'date_from': False, }
                            )
        data['computed'] = {}
        data['computed']['move_state'] = ['draft', 'posted']
        data['computed']['ACCOUNT_TYPE'] = ['payable', 'receivable']
        self.env.cr.execute("""
          SELECT a.id
          FROM account_account a
          WHERE a.internal_type IN %s
          AND NOT a.deprecated""", (tuple(data['computed']['ACCOUNT_TYPE']),))
        data['computed']['account_ids'] = [a for (a,) in self.env.cr.fetchall()]

        statement_obj = self.env['report.account_customer_statement.report_partnerledger'].sudo()
        # balance       = statement_obj._sum_partner( data, self, 'debit - credit')
        balance = 0
        self.balance_count = balance

    @api.model
    def default_get(self, fields_list):
        res = super(res_partner, self).default_get(fields_list)

        res['image'] = self._get_default_image('', True, self._context.get('active_id'))
        if self._context.get('sube_ekle') == 1:
            parent = self.browse(int(self._context.get('active_id')))

            res['parent_id'] = parent.id
            res['firma_unvani'] = parent.firma_unvani
            res['name'] = parent.name
            res['category_id'] = list(map(lambda x: (4, x.id), parent.category_id))
            res['website'] = parent.website
            res['taxAgent'] = parent.taxAgent.id
            res['sahis_sirketi'] = parent.sahis_sirketi
            res['sermaye_sirketi'] = parent.sermaye_sirketi
            res['ortaklik'] = parent.ortaklik
            res['taxNumber'] = parent.taxNumber
            res['tckn'] = parent.tckn
            #res['slip_top'] = parent.slip_top
            #res['slip_bottom'] = parent.slip_bottom
            res['adres'] = parent.adres
            res['customer'] = True
            res['is_company'] = True

        if self._context.get('franc_ekle') == 1:
            parent = self.browse(int(self._context.get('active_id')))

            res['parent_id'] = parent.id
            res['category_id'] = list(map(lambda x: (4, x.id), parent.category_id))
            res['website'] = parent.website
            res['sahis_sirketi'] = parent.sahis_sirketi
            res['sermaye_sirketi'] = parent.sermaye_sirketi
            res['ortaklik'] = parent.ortaklik
            res['is_company'] = True

        return res

    @api.one
    def ustfirma(self):
        get_ust = len(self.parent_id)
        get_subeler = len(self.child_ids)

        if get_ust > 0:
            self.ust_firma = u"ŞUBE"
        elif get_subeler > 0:
            self.ust_firma = u"GENEL MERKEZ"
        else:
            self.ust_firma = ""

    @api.depends('parent_id')
    @api.one
    def issube(self):
        get_ust = len(self.parent_id)

        if get_ust > 0:
            self.is_sube = True
        else:
            self.is_sube = False

    @api.one
    def subesayisi(self):
        if not self.parent_id:
            get_subeler = len(
                self.child_ids)

            if get_subeler > 0:
                self.sube_sayisi = u"Şube : " + str(get_subeler)
            else:
                self.sube_sayisi = u"Şubesi Yok"
        else:
            self.sube_sayisi = hasattr(self, 'sube_kodu') and getattr(self, 'sube_kodu') or ''

    @api.one
    def frenchisesayisi(self):
        get_frenchiselar = len(list(filter(lambda x: x if x.is_company else None, self.child_ids_)))

        if get_frenchiselar > 0:
            self.frenchise_sayisi = u"Frenchise : " + str(get_frenchiselar)
        else:
            self.frenchise_sayisi = u"Frenchise Yok"

    @api.multi
    def merkezadresi(self):
        for s in self:
            get_adres = ""
            get_street = s.parent_id.street
            get_street2 = s.parent_id.street2
            get_plaka = s.parent_id.plaka
            get_zip = s.parent_id.zip
            get_ilce = s.parent_id.ilce.name
            get_il = s.parent_id.city_combo.name
            get_ulke = s.parent_id.country_id.name

            if get_street:
                get_adres += get_street

            if get_street2:
                get_adres += " " + get_street2

            if get_plaka and get_zip:
                get_adres += " " + get_plaka + get_zip

            if get_ilce:
                get_adres += " " + get_ilce

            if get_il:
                get_adres += " " + get_il

            if get_ulke:
                get_adres += " " + get_ulke

            s.adres = get_adres
            '''self.street + " " + self.street2 + " " + self.plaka + self.zip + " " + self.ilce.name + " " + self.city_combo.name + " " + self.country_id.name'''

    @api.multi
    def bir_sube_ekle_action(self):

        context = {'sube_ekle': 1}
        context.update({'active_id': self.id})
        if self.esnaf:
            context.update({'default_esnaf': True})
        if self.kart_musterisi:
            context.update({'default_kart_musterisi': True})
        # print context
        act_dict = dict(
            type="ir.actions.act_window",
            name="Bir Şube Ekle",
            src_model="res.partner",
            res_model="res.partner",
            view_mode="form",
            view_id=self.env.ref("info_extensions.bir_sube_ekle_form").id,
            target="new",
            context=context,
            key2="client_action_multi")

        # print act_dict

        return act_dict

    @api.multi
    def bir_franchise_ekle_act(self):

        context = {'franc_ekle': 1}
        if self.esnaf:
            context.update({'default_esnaf': True})
        if self.kart_musterisi:
            context.update({'default_kart_musterisi': True})
        # print context
        act_dict = dict(
            type="ir.actions.act_window",
            name="Bir Franchise Ekle",
            src_model="res.partner",
            res_model="res.partner",
            view_mode="form",
            view_id=self.env.ref("info_extensions.bir_bayi_ekle_form").id,
            target="new",
            context=context,
            key2="client_action_multi")

        # print act_dict

        return act_dict

    @api.one
    def res_partner_sube_kaydet(self):
        pass

    '''
   @api.multi
   @api.onchange('city_combo','ilce','sube_tanim')
   def onchange_name_params( self ):
      self.ensure_one()
      if self._context.get( 'sube_ekle') == 1 and self.parent_id:
         if len(self.city_combo) > 0 and len(self.ilce) >0 and self.sube_tanim:
            self.name = '%s %s %s %s'% (self.parent_id.name, self.city_combo.name, self.ilce.name, self.sube_tanim )
   '''

    @api.model
    def create(self, vals):

        '''
       if vals.get('parent_id') and self._context.get('sube_ekle') == 1:
          parent    = self.browse(vals.get('parent_id'))
          city      = self.env['info_extensions.iller'].browse(vals.get('city_combo'))
          prov      = self.env['info_extensions.ilceler'].browse(vals.get('ilce'))
          new_name  = '%s %s %s %s '% (parent.name, city.name, prov.name,vals.get('sube_tanim')  )
          vals['name'] = new_name
       '''
        res = super(res_partner, self).create(vals)
        if vals.get('parent_id') and self._context.get('franc_ekle') == 1:
            parent = self.browse(vals.get('parent_id'))

            parent.child_ids_ = [(4,res.id)]
            res.parent_id = False
        '''
       if not res.parent_id and res.customer and (res.sahis_sirketi or res.sermaye_sirketi or res.ortaklik):
           if not res.contacts:
               raise exceptions.ValidationError(u"Devam Etmeden önce %s için Lütfen En Az Bir Adet İletişim Numarası Giriniz."%res.name)
       '''
        return res

    @api.multi
    def write(self, vals):

        ##print vals

        for s in self:
            if s.parent_id and self._context.get('sube_ekle') == 1:
                '''
            parent      = s.parent_id
            city        = s.city_combo
            prov        = s.ilce
            sube_tanim  = s.sube_tanim

            if vals.get('parent_id'):
               parent    = self.browse(vals.get('parent_id'))
            if vals.get('city_combo'):
               city      = self.env['info_extensions.iller'].browse(vals.get('city_combo'))
            if vals.get('ilce'):
               prov      = self.env['info_extensions.ilceler'].browse(vals.get('ilce'))
            if vals.get('sube_tanim'):
               sube_tanim = vals.get('sube_tanim')
            #new_name  = '%s %s %s %s '% (parent.name, city.name, prov.name,sube_tanim )
            #vals['name'] = new_name
            '''
        res = super(res_partner, self).write(vals)
        '''
      if not self.parent_id and self.customer and (self.sahis_sirketi or self.sermaye_sirketi or self.ortaklik):
           if not self.contacts and self.taxNumber != '1111111111' and not vals.get('servis_firmalar'):
               raise exceptions.ValidationError(u"Devam Etmeden önce %s için Lütfen En Az Bir Adet İletişim Numarası Giriniz."%self.name)
      '''
        return res

    @api.multi
    def musTedDetayEkstraYazdir(self, selectedOption, return_data=False):
        self.ensure_one()
        domain = []
        if int(selectedOption) == 1:
            domain = [('company_id', '=', self.env.user.company_id.id)]
        elif int(selectedOption) == 2:
            domain = ['|', ('company_id', '=', self.env.user.company_id.id),
                      ('company_id', 'in', self.env.user.company_id.child_ids.ids), ]
        elif int(selectedOption) == 3:
            domain = []

        # print 1

        journal_ids = self.env['account.journal'].search(domain)

        journal_ids = list(map(lambda x: (4, x.id), journal_ids))

        # print 2

        dictDetay = {
            'target_move': 'all',
            'amount_currency': True,
            'result_selection': 'customer_supplier',
            'reconciled': True,
            'journal_ids': journal_ids
        }
        raporDetay = self.env['account.report.partner.ledger.statement'].create(dictDetay)
        # print 3
        data = {}
        data['ids'] = [self.id]
        data['model'] = u'res.partner'
        data['form'] = raporDetay.read(['date_from', 'date_to', 'journal_ids', 'target_move'])[0]
        used_context = raporDetay._build_contexts(data)
        data['form']['used_context'] = dict(used_context, lang=self.env.context.get('lang') or 'en_US')
        # print 4
        data = raporDetay.pre_print_report(data)
        # print 5

        # print 'DÜZGÜN   : ',journal_ids

        data['form'].update({'reconciled': raporDetay.reconciled,
                             'amount_currency': False,
                             'custom_partner_ids': [self.id],
                             'journal_ids': journal_ids}
                            )
        # print 6
        if return_data:
            return data

        return self.env['report'].get_action(raporDetay, 'account_customer_statement.report_partnerledger', data=data)


class Iller(models.Model):
    _name = 'info_extensions.iller'

    name = fields.Char(string="İl Adı", required=True)
    plaka = fields.Char(string="Plaka Kodu", size=4, required=True)
    country = fields.Many2one('res.country', ondelete='restrict', string="Ülke", required=True)

    ilceler_rel = fields.One2many('info_extensions.ilceler', 'il_id', "İlçeler", required=True)
    vergidaireleri_rel = fields.One2many('info_extensions.vergidaireleri', 'il_id', "İlçeler", required=True)

    @api.one
    @api.constrains('name', 'plaka')  # server side kontrol
    def _check_name_and_plate_spaceless(self):
        if len(self.name.strip()) == 0 or len(self.plaka.strip()) == 0:
            raise exceptions.ValidationError(" İl ve Plaka Bilgileri Boştan Farklı Olmalıdır.")

    _sql_constraints = [
        ('name_unique',
         'UNIQUE(name)',
         "İl Zaten Eklenmiş!"),
        ('plaka_unique',
         'UNIQUE(plaka)',
         "Plaka Zaten Eklenmiş!")
    ]


class Ilceler(models.Model):
    _name = 'info_extensions.ilceler'

    name = fields.Char(string="İlçe Adı", required=True)
    il_id = fields.Many2one('info_extensions.iller', ondelete='restrict', string="İller", required=True)
    ilce_kodu = fields.Integer(string="İlçe Kodu")

    @api.one
    @api.constrains('name')  # server side kontrol
    def _check_name_spaceless(self):
        if len(self.name.strip()) == 0:
            raise exceptions.ValidationError(" İlçe Boştan Farklı Olmalıdır.")


class VergiDaireleri(models.Model):
    _name = 'info_extensions.vergidaireleri'
    name = fields.Char(string="Vergi Dairesi Adı", required=True)
    il_id = fields.Many2one('info_extensions.iller',
                            ondelete='restrict', string="İller", required=True)

    vergi_dairesi_kodu = fields.Char(string='Vergi Dairesi Kodu')

    @api.one
    @api.constrains('name')  # server side kontrol
    def _check_name_spaceless(self):
        if len(self.name.strip()) == 0:
            raise exceptions.ValidationError(" Vergi Dairesi Adı Boştan Farklı Olmalıdır.")


class res_partner_contact(models.Model):
    _name = 'info_extensions.partner_contact'

    name = fields.Char(string='İletişim Kurulacak Kişi Adı', required=True)
    gorev = fields.Char(string='Görevi')
    eposta = fields.Char(string='E-Posta')
    tel_no = fields.Char(string='Tel No', size=14, required=True)
    partner = fields.Many2one('res.partner', string='Partner')

    @api.onchange('tel_no')
    def onchange_phone(self):
        res = {}
        if self.tel_no and len(self.tel_no) > 0:
            phone = [str(s) for s in self.tel_no.split() if s.isdigit()]
            if len(phone) > 0:
                phone = "".join(phone)
                if phone[0] == "0":
                    phone = phone[1:]
                if len(phone) != 10:
                    self.tel_no = False
                    res = {'warning': {
                        'title': 'Tel No Hatalı',
                        'message': u'Lütfen Geçerli Bir GSM No Giriniz. '}
                    }
                else:
                    phone = phone[0:3] + '-' + phone[3:6] + ' ' + phone[6:8] + ' ' + phone[8:10]
                    self.tel_no = phone

            else:
                self.tel_no = False
                res = {'warning': {
                    'title': 'Tel No Hatalı',
                    'message': u'Lütfen Geçerli Bir GSM No Giriniz. '}
                }
        return res


class stock_production_lots(models.Model):
    _inherit = 'stock.production.lot'

    sold_to = fields.Many2one('res.partner', string='Satış Yapılan Firma')
    company_id = fields.Many2one('res.company', string="Bayi",
                                 default=lambda self: self.env.user.company_id.id)
'''
class res_company(models.Model):
    _inherit = 'res.company'

    active = fields.Boolean(related='partner_id.active')
'''