# See LICENSE file for full copyright and licensing details.

{
    'name': 'Project Status Report Project Budget Timesheet',
    'version': '12.0.1.0.0',
    'author': 'Serpent Consulting Services Pvt. Ltd.',
    'license': 'AGPL-3',
    'description': """

Project Status Report
============================

This application allows you to print Project Status Report from projects.
    """,
    'sequence': 1,
    'category': 'Project Management',
    'website': 'http://www.serpentcs.com',
    'summary': 'Project Status Report',
    'depends': ['project', 'hr_timesheet'],
    'images': ['static/description/project_status.jpg'],
    'data': [
         'views/hr_employee_extended_view.xml',
         'views/project_status_view.xml',
         'wizard/project_status_report_wiz_view.xml'
    ],
    'installable': True,
    'price': 30,
    'currency': 'EUR',
}
