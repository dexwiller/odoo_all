# See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models
import xlsxwriter
from odoo import tools
import datetime
import base64
import time
import random
import math
import tempfile
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as DATETIME_FORMAT


class ProjectFileDataWiz(models.TransientModel):
    _name = 'project.file.data.wiz'

    name = fields.Char('File Name')
    file_download = fields.Binary('File to Download')
    project_status_id = fields.Many2one('project.status.report.wiz',
                                        "Project Status")


class ProjectStatusReportWiz(models.TransientModel):
    _name = 'project.status.report.wiz'

    @api.model
    def _get_end_date(self):
        today = datetime.date.today()
        last_days = [31, 30, 29, 28, 27]
        for i in last_days:
            try:
                end = datetime.datetime(today.year, today.month, i)
            except ValueError:
                continue
            else:
                return datetime.datetime.strftime(end.date(), DATETIME_FORMAT)
        return None

    date_start = fields.Datetime("Start Date", default=lambda *a:
                                 time.strftime('%Y-%m-01 %H:%M:%S'))
    date_end = fields.Datetime("End Date", default=_get_end_date)
    file_data = fields.One2many('project.file.data.wiz', 'project_status_id',
                                "Files to Download", readonly=True)
    state = fields.Selection([('generate', 'Generate'),
                              ('download', 'Download')],
                             default='generate', string='State')

    @api.multi
    def update_sheet(self):
        project_obj = self.env['project.project']
        task_obj = self.env['project.task']
        emp_obj = self.env['hr.employee']
        file_data_wiz_obj = self.env['project.file.data.wiz']
        dir_temp = tempfile.gettempdir()
        for project in project_obj.browse(self._context.get('active_ids')):
            project_name = project.name
            if '/' in project_name:
                index = project_name.index('/')
                lst_name = list(project_name)
                lst_name[index] = '-'
                project_name = "".join(lst_name)
            file_path = str(project_name) + '.xlsx'
            workbook = xlsxwriter.Workbook(dir_temp + '/' + file_path)
            worksheet_plan = workbook.add_worksheet("Project Plan and Gantt")
            worksheet_plan.hide_gridlines(2)
            worksheet_notes = workbook.add_worksheet("Notes")

            clr_lst = ['green', '#9932CC', '#00008B', '#400000',
                       '#B40431', '#FF4500', '#A0522D']
            task_stage_list = [x.name for x in project.type_ids]
            task_state_dict = {}
            for stage in task_stage_list:
                task_state_dict.update({stage: {'color': random.choice(clr_lst)
                                                }
                                        })

            # set size of column
            worksheet_plan.set_column('A:B', 15)
            worksheet_plan.set_column('D:D', 18)
            worksheet_notes.set_column('A:A', 25)
            worksheet_notes.set_column('B:B', 15)
            worksheet_notes.set_column('F:F', 15)
            worksheet_notes.set_column('C:E', 12)
            worksheet_notes.set_column('G:H', 15)

            # Add format
            format_big = workbook.add_format()
            format_big.set_bold()  # bold font
            format_big.set_font_size(12)  # set font size
            format_big.set_font_name('Calibri')  # set Font style
            # Add general_format
            general_format = workbook.add_format()
            general_format.set_font_size(12)  # set font size
            general_format.set_font_name('Calibri')  # set Font style
            worksheet_plan.write('A2', 'Project Name:', format_big)
            worksheet_plan.write('A3', 'Report Date', general_format)
            worksheet_plan.write('A4', 'Project Status', general_format)
            worksheet_plan.write('A5', 'Completed', general_format)
            worksheet_plan.write('B2', project.name, format_big)
            worksheet_plan.write('B3', project.date or '',
                                 general_format)
            # Add state_format
            state_format = workbook.add_format()
            state_format.set_bold()  # bold font
            state_format.set_font_size(12)  # set font size
            state_format.set_font_name('Calibri')  # set Font style
            state_name = str(project.state)
            if project.state == 'close':
                state_name = 'Complete'
                state_format.set_font_color('green')
            elif project.state == 'initiating':
                state_name = 'Initiating'
                state_format.set_font_color('#9932CC')
            elif project.state == 'planning':
                state_name = 'Planning'
                state_format.set_font_color('#00008B')
            elif project.state == 'open':
                state_name = 'Executing'
                state_format.set_font_color('#FF4500')
            elif project.state == 'pending':
                state_name = 'Pending'
                state_format.set_font_color('red')
            elif project.state == 'cancelled':
                state_name = 'Cancelled'
                state_format.set_font_color('#A0522D')
            worksheet_plan.write('B4', state_name, state_format)

            # Add table_header_format
            table_header_format = workbook.add_format()
            table_header_format.set_bold()
            table_header_format.set_border()  # Add cell border
            table_header_format.set_font_name('Calibri')  # set Font style
            table_header_format.set_font_color('white')
            table_header_format.set_bg_color('#008080')
            worksheet_plan.write('A22', 'Tasks', table_header_format)
            worksheet_plan.write('B22', 'Assigned To', table_header_format)
            worksheet_plan.write('C22', 'Priority', table_header_format)
            worksheet_plan.write('D22', 'Status', table_header_format)

            # Add task_name_format
            task_name_format = workbook.add_format()
            task_name_format.set_font_size(8)  # set font size
            task_name_format.set_font_name('Calibri')  # set Font style
            task_name_format.set_border()  # Add cell border
            # Add assigned_to_format
            assigned_to_format = workbook.add_format()
            assigned_to_format.set_font_size(10)  # set font size
            assigned_to_format.set_border()  # Add cell border
            assigned_to_format.set_font_name('Calibri')  # set Font style
            task_name_col = 0
            task_assign_col = 1
            task_priority_col = 2
            task_status_col = 3
            task_row = 22

            # Notes worksheet variables
            task_planned_budget = task_actual_budget = 0
            total_dur_hours = 0
            total_working_hours = 0
            bar_chart_height = 90
            notes_task_name_col = 0
            notes_task_assign_col = 1
            notes_task_start_date_col = 2
            notes_task_end_date_col = 3
            notes_task_days_col = 4
            notes_task_status_col = 5
            bug = change_req = development = meeting = rnd = 0

            # Add task_state_format
            task_table_notes_format = workbook.add_format()
            task_table_notes_format.set_border()  # Add cell border

            worksheet_notes.write('A2', "Percentage of Tasks Complete",
                                  format_big)
            state_row = 2
            for s_name in task_stage_list:
                worksheet_notes.write(state_row, 0, s_name,
                                      task_table_notes_format)
                state_row += 1
            if state_row < 7:
                state_row = 7

            worksheet_notes.write(state_row + 2, 0, "Tasks",
                                  table_header_format)
            worksheet_notes.write(state_row + 2, 1, "Assigned To",
                                  table_header_format)
            worksheet_notes.write(state_row + 2, 2, "Start",
                                  table_header_format)
            worksheet_notes.write(state_row + 2, 3, "End", table_header_format)
            worksheet_notes.write(state_row + 2, 4, "Days",
                                  table_header_format)
            worksheet_notes.write(state_row + 2, 5, "Status",
                                  table_header_format)
            task_row_notes = state_row + 3

            task_list = task_obj.search([('project_id', '=', project.id),
                                         ('date_start', '>=',
                                          self.date_start)],
                                        order='date_start')

            for task in task_list:
                dur_days = 0
                dur_hours = 0
                if task.date_start:
                    date_start = task.date_start
                    deadline = False
                    if not task.date_deadline:
                        plan_hours = task.planned_hours
                        deadline = (
                            date_start + datetime.timedelta(
                                hours=plan_hours)).date()
                        delta = (date_start +
                                 datetime.timedelta(hours=task.planned_hours) -
                                 date_start)
                        d_hours = math.floor((delta.total_seconds()) / 3600)
                        dur_hours = math.floor((delta.total_seconds()) / 3600)
                        dur_days = math.ceil(d_hours / 8)
                        d_deadline = (date_start +
                                      datetime.timedelta(days=dur_days))
                    elif task.date_deadline:
                        dt_dead = task.date_deadline
                        plan_hours = task.planned_hours
                        d_deadline = dt_dead
                        deadline = dt_dead
                        delta = deadline - date_start.date()
                        days_to_hours = delta.days * 8
                        diff_btw_two_times = (delta.seconds) / 3600
                        d_hours = days_to_hours + diff_btw_two_times
                        dur_delta = (date_start +
                                     datetime.timedelta(hours=plan_hours) -
                                     date_start)
                        dur_hours = math.floor((dur_delta.total_seconds()) /
                                               3600)
                        dur_days = math.ceil(d_hours / 8)

                    if dur_days == 0:
                        dur_days = 1
                    if dur_hours == 0:
                        dur_hours = 1
                    if (deadline and
                            deadline > self.date_end.date()):
                        continue

                    priority = ''
                    if task.priority == '1':
                        priority = '★'
                    elif task.priority == '2':
                        priority = '★★'
                    worksheet_plan.write(task_row, task_name_col,
                                         task.name, task_name_format)
                    worksheet_plan.write(task_row, task_assign_col,
                                         task.user_id and
                                         task.user_id.name or '-',
                                         assigned_to_format)
                    worksheet_plan.write(task_row, task_priority_col,
                                         tools.ustr(priority),
                                         task_name_format)
                    # Add task_state_format_notes
                    task_state_format_notes = workbook.add_format()
                    task_state_format_notes.set_bold()  # bold font
                    task_state_format_notes.set_font_size(12)  # set font size
                    task_state_format_notes.set_border()  # Add cell border
                    # set Font style
                    task_state_format_notes.set_font_name('Calibri')
                    if task.stage_id.name == "Done":
                        task_state_format_notes.set_font_color('green')
                    elif task.stage_id.name == 'Analysis':
                        task_state_format_notes.set_font_color('#9932CC')
                    elif task.stage_id.name == 'Specification':
                        task_state_format_notes.set_font_color('#00008B')
                    elif task.stage_id.name == 'Design':
                        task_state_format_notes.set_font_color('#B404AE')
                    elif task.stage_id.name == 'Development':
                        task_state_format_notes.set_font_color('#400000')
                    elif task.stage_id.name == 'Testing':
                        task_state_format_notes.set_font_color('#FF4500')
                    elif task.stage_id.name == 'Cancelled':
                        task_state_format_notes.set_font_color('#A0522D')
                    worksheet_plan.write(task_row, task_status_col,
                                         task.stage_id.name,
                                         task_state_format_notes)
                    task_row += 1

                    # Notes worksheet data
                    working_hours = task.effective_hours
                    employee_rec = emp_obj.search([('user_id', '=',
                                                    task.user_id.id)])
                    if employee_rec:
                        price = (employee_rec[0].product_id and
                                 employee_rec[0].product_id.lst_price or 0)
                        task_planned_budget += price * dur_hours
                        task_actual_budget += price * working_hours
                    total_dur_hours += dur_hours
                    total_working_hours += working_hours
                    d_for = 'dd/mm/yy'
                    date_format = workbook.add_format({'num_format': d_for})
                    date_format.set_border()  # Add cell border

                    worksheet_notes.write(task_row_notes, notes_task_name_col,
                                          task.name, task_table_notes_format)
                    worksheet_notes.write(task_row_notes,
                                          notes_task_assign_col,
                                          task.user_id.name,
                                          task_table_notes_format)
                    worksheet_notes.write(task_row_notes,
                                          notes_task_start_date_col,
                                          date_start, date_format)
                    worksheet_notes.write(task_row_notes,
                                          notes_task_end_date_col,
                                          d_deadline, date_format)
                    worksheet_notes.write(task_row_notes, notes_task_days_col,
                                          dur_days,
                                          task_table_notes_format)

                    # Add task_state_format
                    task_state_format = workbook.add_format()
                    task_state_format.set_bold()  # bold font
                    task_state_format.set_font_size(12)  # set font size
                    task_state_format.set_font_name('Calibri')  # set Fontstyle
                    task_state_format.set_border()  # Add cell border

                    if task.stage_id.name in task_state_dict:
                        t_name = task.stage_id.name
                        sclr = task_state_dict.get(t_name).get('color')
                        task_state_format.set_font_color(sclr)
                        if task_state_dict.get(t_name).get('count'):
                            c_state = task_state_dict.get(t_name).get('count')
                            task_state_dict.get(t_name).update({
                                'count': c_state + 1
                            })
                        else:
                            task_state_dict.get(t_name).update({'count': 1})

                        worksheet_notes.write(task_row_notes,
                                              notes_task_status_col,
                                              task.stage_id.name,
                                              task_state_format)
                    task_row_notes += 1
                    bar_chart_height += 18.90

            state_row_count = 2
            for s_name in task_stage_list:
                if task_state_dict.get(s_name).get('count'):
                    temp_count = task_state_dict.get(s_name).get('count')
                    worksheet_notes.write(state_row_count, 1,
                                          temp_count,
                                          task_table_notes_format)
                else:
                    worksheet_notes.write(state_row_count, 1, 0,
                                          task_table_notes_format)
                state_row_count += 1

            worksheet_notes.write('D2', "Budget", format_big)
            worksheet_notes.write('D3', "Planned", task_table_notes_format)
            worksheet_notes.write('E3', task_planned_budget,
                                  task_table_notes_format)
            worksheet_notes.write('D4', "Actual", task_table_notes_format)
            worksheet_notes.write('E4', task_actual_budget,
                                  task_table_notes_format)

            worksheet_notes.write('D6', "Time Duration(Hours)", format_big)
            worksheet_notes.write('D7', "Planned", task_table_notes_format)
            worksheet_notes.write('E7', total_dur_hours,
                                  task_table_notes_format)
            worksheet_notes.write('D8', "Actual", task_table_notes_format)
            worksheet_notes.write('E8', total_working_hours,
                                  task_table_notes_format)

            worksheet_notes.write('G2', "Pending Items", format_big)
            worksheet_notes.write('G3', "Development", task_table_notes_format)
            worksheet_notes.write('H3', development, task_table_notes_format)
            worksheet_notes.write('G4', "Change Requests",
                                  task_table_notes_format)
            worksheet_notes.write('H4', change_req, task_table_notes_format)
            worksheet_notes.write('G5', "Bugs", task_table_notes_format)
            worksheet_notes.write('H5', bug, task_table_notes_format)
            worksheet_notes.write('G6', "Meeting", task_table_notes_format)
            worksheet_notes.write('H6', meeting, task_table_notes_format)
            worksheet_notes.write('G7', "Research & Development",
                                  task_table_notes_format)
            worksheet_notes.write('H7', rnd, task_table_notes_format)

            # Adding bar chart
            bar_chart = workbook.add_chart({'type': 'bar',
                                            'subtype': 'stacked'})
            bar_chart.set_size({'width': 900, 'height': bar_chart_height})
            bar_chart.set_legend({'none': True})
            bar_chart.set_title({'none': True})
            bar_chart.set_y_axis({'reverse': True})
            till_bar_cat = ':$A$' + str(task_row_notes)
            till_bar_val = ':$C$' + str(task_row_notes)
            bar_graph_name1 = '=Notes!$C$' + str(state_row + 3)
            bar_graph_cat1 = '=Notes!$A$' + str(state_row + 4) + till_bar_cat
            bar_graph_val1 = '=Notes!$C$' + str(state_row + 4) + till_bar_val
            bar_chart.add_series({
                'name': bar_graph_name1,
                'categories': bar_graph_cat1,
                'values': bar_graph_val1,
                'fill': {'none': True},
                'line': {'none': True},
            })
            till_bar_cat2 = ':$A$' + str(task_row_notes)
            till_bar_val2 = ':$E$' + str(task_row_notes)
            bar_graph_name2 = '=Notes!$E$' + str(state_row + 3)
            bar_graph_cat2 = '=Notes!$A$' + str(state_row + 4) + till_bar_cat2
            bar_graph_val2 = '=Notes!$E$' + str(state_row + 4) + till_bar_val2
            bar_chart.add_series({
                'name': bar_graph_name2,
                'categories': bar_graph_cat2,
                'values': bar_graph_val2,
                'fill': {'color': 'green'},
            })
            # Insert the chart into the worksheet (with an offset).
            worksheet_plan.insert_chart('E20', bar_chart,
                                        {'x_offset': 10, 'y_offset': 5})

            # Adding pie chart
            pie_chart = workbook.add_chart({'type': 'pie'})
            pie_chart.set_title({'name': 'Overall Task Status'})
            pie_chart.set_size({'width': 350, 'height': 220})
            pie_chart_cat = '=Notes!$A$3:$A$' + str(state_row)
            pie_chart_val = '=Notes!$B$3:$B$' + str(state_row)
            pie_chart.add_series({
                'categories': pie_chart_cat,
                'values': pie_chart_val,
            })
            # Insert the chart into the worksheet (with an offset).
            worksheet_plan.insert_chart('A7', pie_chart,
                                        {'x_offset': 25, 'y_offset': 10})

            # Adding bar chart
            budget_bar_chart = workbook.add_chart({'type': 'bar'})
            budget_bar_chart.set_size({'width': 350, 'height': 250})
            budget_bar_chart.set_title({'name': 'Budget'})
            budget_bar_chart.set_legend({'none': True})
            budget_bar_chart.add_series({
                'categories': '=Notes!$D$3:$D$4',
                'values': '=Notes!$E$3:$E$4',
            })
            # Insert the chart into the worksheet (with an offset).
            worksheet_plan.insert_chart('E6', budget_bar_chart,
                                        {'x_offset': 30, 'y_offset': 25})

            # Adding bar chart
            budget_bar_chart = workbook.add_chart({'type': 'bar'})
            budget_bar_chart.set_size({'width': 350, 'height': 250})
            budget_bar_chart.set_title({'name': 'Budget'})
            budget_bar_chart.set_legend({'none': True})
            budget_bar_chart.add_series({
                'categories': '=Notes!$D$3:$D$4',
                'values': '=Notes!$E$3:$E$4',
            })
            # Insert the chart into the worksheet (with an offset).
            worksheet_plan.insert_chart('E6', budget_bar_chart,
                                        {'x_offset': 30, 'y_offset': 25})

            # Adding column chart
            column_chart = workbook.add_chart({'type': 'column'})
            column_chart.set_title({'name': 'Pending Items'})
            column_chart.set_size({'width': 800, 'height': 250})
            column_chart.set_legend({'none': True})
            column_chart.add_series({
                'categories': '=Notes!$G$2:$G$7',
                'values': '=Notes!$H$2:$H$7',
            })
            # Insert the chart into the worksheet (with an offset).
            worksheet_plan.insert_chart('R6', column_chart,
                                        {'x_offset': 10, 'y_offset': 25})

            # Adding column chart
            bar_chart_time = workbook.add_chart({'type': 'bar'})
#            column_chart.set_y_axis({'visible': False})
            bar_chart_time.set_title({'name': 'Time Duration(Hours)'})
            bar_chart_time.set_size({'width': 400, 'height': 250})
            bar_chart_time.set_legend({'none': True})
            bar_chart_time.add_series({
                'categories': '=Notes!$D$7:$D$8',
                'values': '=Notes!$E$7:$E$8',
            })
            # Insert the chart into the worksheet (with an offset).
            worksheet_plan.insert_chart('K6', bar_chart_time,
                                        {'x_offset': 10, 'y_offset': 25})

            workbook.close()
            pro_file = base64.b64encode(open(dir_temp + '/' + file_path,
                                             'rb').read())
            file_data_wiz_obj.create({'name': file_path,
                                      'file_download': pro_file,
                                      'project_status_id': self.id})
        self.write({'state': 'download'})
        return {
            'res_id': self.id,
            'name': 'Files to Download',
            'view_type': 'form',
            "view_mode": 'form',
            'res_model': 'project.status.report.wiz',
            'type': 'ir.actions.act_window',
            'target': 'new',
        }
