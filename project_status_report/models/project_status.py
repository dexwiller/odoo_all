# See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class Project(models.Model):
    _inherit = "project.project"
    _description = "Project"

    state = fields.Selection([
        ('template', 'Template'),
        ('draft', 'New'),
        ('initiating', 'Initiating'),
        ('planning', 'Planning'),
        ('open', 'Executing'),
        ('pending', 'Pending'),
        ('close', 'Closing'),
        ('cancelled', 'Cancelled')
    ], string='Status', default='initiating',
        required=True, copy=False)

    @api.multi
    def set_cancel(self):
        self.state = 'cancelled'

    @api.multi
    def set_pending(self):
        self.state = 'pending'

    @api.multi
    def set_plan(self):
        self.state = 'planning'

    @api.multi
    def set_open(self):
        self.state = 'open'

    @api.multi
    def set_close(self):
        self.state = 'close'

    @api.multi
    def set_draft(self):
        self.state = 'draft'
