# -*- coding: utf-8 -*-
{
    "name": "Tasks Daily Reminder",
    "version": "12.0.1.2.1",
    "category": "Project",
    "author": "Odoo Tools",
    "website": "https://odootools.com/apps/12.0/tasks-daily-reminder-244",
    "license": "Other proprietary",
    "application": True,
    "installable": True,
    "auto_install": False,
    "depends": [
        "project"
    ],
    "data": [
        "views/project_task_type.xml",
        "data/cron.xml",
        "data/notification_task_data.xml"
    ],
    "summary": "The tool to notify users of assigned to them topical tasks",
    "description": """
    This is the tool to remind responsible users of tasks which deadline is today or which deadline has already passed

    Configure your own single-list reminders for any Odoo records using <a href="https://apps.odoo.com/apps/modules/12.0/total_notify/">the tool All-In-One Lists Reminders</a>
    Some email clients / browser might partially spoil the table appearance
    The tool gathers all tasks which should be done urgently: today or in the Past
    The reminder is sent <strong>for each assigned user</strong> if he/she has such tasks 
    Notification is done as a <strong>single</strong> email with all tasks combined in the table. Each line has a link for an instant access. No dozens useless emails
    Only up-to-date tasks are included into the reminder. Choose project stages on which tasks should be taken into account
    A reminder is sent once per day, but you can change it. Look at the 'Configuration' tab
    You may also modify email appearance and set. Look at the 'Configuration' tab
    All tasks are combined in a single to-do list
    Choose stages for notifications: remind only of topical tasks
""",
    "images": [
        "static/description/main.png"
    ],
    "price": "28.0",
    "currency": "EUR",
}