# -*- coding: utf-8 -*-
from odoo import http,exceptions
from odoo.addons.web.controllers.main import serialize_exception
from odoo.modules import get_resource_path
from odoo import SUPERUSER_ID
import functools
from datetime import datetime, date
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import werkzeug, json
import traceback,urllib,re
import base64
import json
import pytz
import xlsxwriter
from io import StringIO
from Crypto.Cipher import AES
import base64
import os
import xlsxwriter, re
from io import StringIO
BLOCK_SIZE = 16
PADDING = '{'
pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING
EncodeAES = lambda c, s: base64.b64encode(c.encrypt(pad(s)))
DecodeAES = lambda c, e: str(c.decrypt(base64.b64decode(e)), 'utf-8').rstrip(PADDING)
#secret = os.urandom(BLOCK_SIZE)
a = 'PTHK%GT&+Y386^0*'
cipher = AES.new(a)
def compute_curent_time( context=None ):
    date_now = datetime.now()
    
    if context and context.get('tz',False):
       
        time_zone=context['tz']
        tz = pytz.timezone(time_zone)
        tzoffset = tz.utcoffset(date_now)
        date_now = date_now + tzoffset
    return date_now.strftime('%d/%m/%Y %H:%M:%S')

class dummy:
     pass
class HtmlFieldResponse():
    error = ""
    return_data = ""
    history_data = ""
class InfoKart(http.Controller):
    def _process_html_input_group(self, field, field_data, values):
       """Validation for input_groups and preps for insertion into database"""
       html_response = HtmlFieldResponse()
       html_response.error = ""

       input_group_obj = json.loads(field_data)
       
       all_inserts = []
       for row in input_group_obj:
           _logger.error(row)
           valid_row = True

           #Only allow fields in the sub field list
           for sub_field in field.setting_input_group_sub_fields:
               if sub_field.name not in row:
                   valid_row = False

               if sub_field.ttype == "binary":
                   row[str(sub_field.name) + "_filename"] = values[row[sub_field.name]].filename
                   row[sub_field.name] = base64.encodestring(values[row[sub_field.name]].read())

           if valid_row:
               all_inserts.append((0, 0, row))

       html_response.return_data = all_inserts
       html_response.history_data = all_inserts

       return html_response

    def _process_html_textbox(self, field, field_data, values):
       """Validation for textbox and preps for insertion into database"""
       html_response = HtmlFieldResponse()
       html_response.error = ""

       #Required Check
       if field.setting_general_required is True and field_data == "":
           html_response.error = "Zorunlu Alan"

       html_response.return_data = field_data
       html_response.history_data = field_data

       return html_response

    def _process_html_checkbox_group(self, field, field_data, values):
       """Validation for checkbox group and preps for insertion into database"""
       html_response = HtmlFieldResponse()
       html_response.error = ""

       create_list = []
       
       _logger.error(request.httprequest.form.getlist(field.html_name))
       
       for checkbox_value in request.httprequest.form.getlist(field.html_name):
       
           #Awkward hack to deal for javascript posting...
           if "," in checkbox_value:
               for checkbox_subvalue in checkbox_value.split(","):
                   create_list.append((4, int(checkbox_subvalue) ))                
           else:
               create_list.append((4, int(checkbox_value) ))

       html_response.return_data = create_list
       html_response.history_data = create_list

       return html_response

    def _process_html_date_picker(self, field, field_data, values):
       """Validation for date picker textbox and preps for insertion into database"""
       html_response = HtmlFieldResponse()
       html_response.error = ""

       #Required Check
       if field.setting_general_required is True and field_data == "":
           html_response.error = "Zorunlu Alan"

       my_date = datetime.strptime(field_data, '%Y-%m-%d')

       html_response.return_data = my_date.strftime(DEFAULT_SERVER_DATE_FORMAT)
       html_response.history_data = my_date.strftime(DEFAULT_SERVER_DATE_FORMAT)

       return html_response

    def _process_html_datetime_picker(self, field, field_data, values):
       """Validation for datetime picker textbox and preps for insertion into database"""
       html_response = HtmlFieldResponse()
       html_response.error = ""

       #Required Check
       if field.setting_general_required is True and field_data == "":
           html_response.error = "Zorunlu Alan"

       html_response.return_data = field_data
       html_response.history_data = field_data

       return html_response

    def _process_html_checkbox_boolean(self, field, field_data, values):
       """Validation for Checkboxes(Boolean) and preps for insertion into database"""
       html_response = HtmlFieldResponse()
       html_response.error = ""

       #Required Check
       if field.setting_general_required is True and field_data == "":
           html_response.error = "Zorunlu Alan"

       html_response.return_data = field_data
       html_response.history_data = field_data

       return html_response

    def _process_html_dropbox_m2o(self, field, field_data, values):
       """Validation for Dropbox(m2o) and preps for insertion into database"""
       html_response = HtmlFieldResponse()
       html_response.error = ""

       #Required Check
       if field.setting_general_required is True and field_data == "":
           html_response.error = "Zorunlu Alan"

       html_response.return_data = field_data
       html_response.history_data = field_data

       return html_response

    def _process_html_textarea(self, field, field_data, values):
       html_response = HtmlFieldResponse()
       html_response.error = ""

       #Required Check
       if field.setting_general_required is True and field_data == "":
           html_response.error = "Zorunlu Alan"

       html_response.return_data = field_data
       html_response.history_data = field_data

       return html_response

    def _process_html_radio_group_selection(self, field, field_data, values):
       html_response = HtmlFieldResponse()
       html_response.error = ""

       #Required Check
       if field.setting_general_required is True and field_data == "":
           html_response.error = "Zorunlu Alan"

       html_response.return_data = field_data
       html_response.history_data = field_data

       return html_response

    def _process_html_file_select(self, field, field_data, values):
       html_response = HtmlFieldResponse()
       html_response.error = ""

       #Required Check
       if field.setting_general_required is True and field_data == "":
           html_response.error = "Zorunlu Alan"
       
       base64_string = base64.encodestring(field_data.read())
               
       html_response.return_data = base64_string
       html_response.history_data = base64_string

       return html_response
   
    def _process_html_dropbox(self, field, field_data, values):
       """Validation for dropbox and preps for insertion into database"""
       html_response = HtmlFieldResponse()
       html_response.error = ""

       #Required Check
       if field.setting_general_required is True and field_data == "":
           html_response.error = "Zorunlu Alan"

       if field.field_id.ttype == "selection":

           #ensure that the user isn't trying to inject data that is not in the selection
           selection_list = dict(request.env[field.field_id.model_id.model]._fields[field.field_id.name].selection)

           for selection_value, selection_label in selection_list.items():
               if field_data == selection_value:
                   html_response.error = ""
                   html_response.return_data = field_data
                   html_response.history_data = field_data

                   return html_response

           html_response.error = "Geçerli Bir Değer Seçiniz."
           html_response.return_data = ""
           html_response.history_data = ""

           return html_response
       elif field.field_id.ttype == "many2one":

           html_response.error = ""
           html_response.return_data = int(field_data)
           html_response.history_data = field_data

           return html_response

    def process_form(self, kwargs,new_rec = False):
       values = {}
       for field_name, field_value in kwargs.items():
               values[field_name] = field_value
       #print values

       if values['my_pie'] != "3.14":
           return "You touched my pie!!!"

       #Check if this form still exists
       if http.request.env['html.form'].sudo().search_count([('id', '=', int(values['form_id']))]) == 0:
           return "The form no longer exists"

       entity_form = http.request.env['html.form'].sudo().browse(int(values['form_id']))

       ref_url = ""
       if 'Referer' in http.request.httprequest.headers:
           ref_url = http.request.httprequest.headers['Referer']

       #Captcha Check
       if entity_form.captcha:

           #Redirect them back if they didn't answer the captcha
           if 'g-recaptcha-response' not in values:
               return werkzeug.utils.redirect(ref_url)

           payload = {'secret': str(entity_form.captcha_secret_key), 'response': str(values['g-recaptcha-response'])}
           response_json = requests.post("https://www.google.com/recaptcha/api/siteverify", data=payload)

           if response_json.json()['success'] is not True:
               return werkzeug.utils.redirect(ref_url)

       secure_values = {}
       history_values = {}
       return_errors = []
       insert_data_dict = []
       form_error = False

       #populate an array which has ONLY the fields that are in the form (prevent injection)
       for fi in entity_form.fields_ids:
           
           #Required field check
           if fi.setting_general_required and fi.html_name not in values:
               return_item = {"html_name": fi.html_name, "error_messsage": "Bu alan zorunludur!"}
               return_errors.append(return_item)
               form_error = True

           if fi.html_name in values:
               method = '_process_html_%s' % (fi.field_type.html_type,)
               
               action = getattr(self, method, None)

               if fi.field_type.html_type == "file_select":
                   #Also insert the filename
                   filename_field = str(fi.field_id.name) + "_filename"
                   secure_values[filename_field] = values[fi.html_name].filename

               if not action:
                   raise NotImplementedError('Method %r is not implemented on %r object.' % (method, self))

               field_valid = HtmlFieldResponse()

               field_valid = action(fi, values[fi.html_name], values)

               if field_valid.error == "":
                   secure_values[fi.field_id.name] = field_valid.return_data
                   insert_data_dict.append({'field_id': fi.field_id.id, 'insert_value': field_valid.history_data})
               else:
                   return_item = {"html_name": fi.html_name, "error_messsage": field_valid.error}
                   return_errors.append(return_item)
                   form_error = True

       if form_error:
           return json.JSONEncoder().encode({'status': 'error', 'errors': return_errors})
       else:
           new_history = http.request.env['html.form.history'].sudo().create({'ref_url': ref_url, 'html_id': entity_form.id})

           for insert_field in insert_data_dict:
               new_history.insert_data.sudo().create({'html_id': new_history.id, 'field_id': insert_field['field_id'], 'insert_value': insert_field['insert_value']})

           #default values
           for df in entity_form.defaults_values:
               if df.field_id.ttype == "many2many":
                   secure_values[df.field_id.name] = [(4, request.env[df.field_id.relation].search([('name', '=', df.default_value)])[0].id)]
               else:
                   if df.default_value == "user_id":
                       secure_values[df.field_id.name] = request.env.user.id
                   elif df.default_value == "partner_id":
                       secure_values[df.field_id.name] = request.env.user.partner_id.id
                   else:
                       if df.field_id.ttype == "many2one":
                           secure_values[df.field_id.name] = int(df.default_value)
                       else:
                           secure_values[df.field_id.name] = df.default_value

               new_history.insert_data.sudo().create({'html_id': new_history.id, 'field_id': df.field_id.id, 'insert_value': df.default_value})

           try:
              if new_rec:
                   new_record = http.request.env[entity_form.model_id.model].sudo().create(secure_values)
                   new_history.record_id = new_record.id
           except Exception as e:
               _logger.error(str(e))
               return "Failed to insert record<br/>\n" + str(e)
           #Execute all the server actions
           for sa in entity_form.submit_action:

               method = '_html_action_%s' % (sa.setting_name,)
               action = getattr(self, method, None)

               if not action:
                   raise NotImplementedError('Method %r is not implemented on %r object.' % (method, self))

               #Call the submit action, passing the action settings and the history object
               action(sa, new_history, values)

           if 'is_ajax_post' in values:
               return json.JSONEncoder().encode({'status': 'success', 'redirect_url': entity_form.return_url})
           else:
               return werkzeug.utils.redirect(entity_form.return_url)

    @http.route("/yemek-karti/yemekmatik-karta-para-yukle/<t>",type='http', auth='public',cors='*',website=True)
    @serialize_exception
    def load_para_yukle(self,t,**kw):
        values = {
            'p_source':t
        }
        page = 'ym_forms.para_yukle'
        return http.request.render(page, values)
    
    @http.route("/yemek-karti/iliskili-kart-form/<d>/<t>",type='http', auth='public',website=True)
    @serialize_exception
    def get_iliskili_page(self,d,t,**kw):
         seri_no = d
         return http.request.render('ym_forms.kart_iliskilendir', {'k_id':seri_no,'p_source':t})
    
    @http.route("/yemek-karti/iliskili-kart-docs/<d>/<p>/<t>",type='http', auth='public',website=True)
    @serialize_exception
    def get_iliskili_docs(self,d,p,t,**kw):
        p_id    = p
        seri_no = d
        p_id_d  = DecodeAES(cipher, urllib.unquote(p_id.replace("AaBaA","%2F")))
         
        s_dict = {'nufus_state'    : 'Evrak Bulunamadı',
                   'adres_state'    : 'Evrak Bulunamadı',
                   'sozlesme_state' : 'Evrak Bulunamadı',
                   'kvkk_state'     : 'Evrak Bulunamadı'}
        try: 
            partner = http.request.env['res.partner'].sudo().browse([int(p_id_d)])
        except:
            partner = False
        if partner:
            for e in partner.uye_is_yeri_evrak:
                if s_dict.get (e.evrak_id.web_name + '_state'):
                    s_dict[e.evrak_id.web_name + '_state'] = filter(lambda x:x[1] if x[0] == e.state else None,e._fields['state'].selection)[0][1]
        
        s_dict.update( {'k_id':seri_no,'p_id':p_id,'p_source':t} )
        
        #print s_dict
         
        return http.request.render('ym_forms.kart_kisisellestir', s_dict)
    
    @http.route("/yemek-karti/kart-hata/<c>/<d>/",type='http', auth='public',website=True)
    @serialize_exception
    def get_hata_page(self,c,d,**kw):
         if int(d) == 1:
              hatamesaji = 'Girilen Kart No Sistemimizde Bulunamadı'
         if int(d) == 2:
              hatamesaji = 'Lütfen Geçerli Bir Kart No Giriniz'
         if int(d) == 3:
              kart = http.request.env.get('stock.production.lot').sudo().search([('guid','=',c)])
              hatamesaji = kart.ws_last_message
         if int(d) == 4:
            hatamesaji = 'Ödeme Başarısız - Girilen Verileri Kontrol Ediniz.'
         return http.request.render('ym_forms.kart_error', {'hatamesaji':hatamesaji})
    
    def create_bireysel_musteri(self,kw,kart,response):
        
        p = http.request.env['res.partner'].sudo()
        chack_p_by_tckn = p.search([('tckn','=',kw.get('tckn'))],order='id desc',limit=1)
        if chack_p_by_tckn:
            chack_p_by_tckn.turkpara_basvuru_id = response.Basvuru_ID
            self.kart_params_up( kart, kw,chack_p_by_tckn )
            return 1,chack_p_by_tckn.id
        
        d = dict(name=kw.get('ad') + ' ' + kw.get('soyad'),
                 street = kw.get('adres'),
                 city_combo = kw.get('il'),
                 ilce       = kw.get('ilce'),
                 tckn       = kw.get('tckn'),
                 email      = kw.get('email'),
                 gsm_no     = kw.get('cep_tel'),
                 dogum_tar  = kw.get('dogum_tar'),
                 turkpara_basvuru_id = response.Basvuru_ID
                 )
        
        context = {'default_customer': 1,
                       'default_dealer': 0,
                       'default_esnaf':0,
                       'default_bireysel_musteri':1,
                       'default_is_company':0,
                       'default_state':'predraft',
                       'default_order':'id asc',
                  }
        try:
            part = p.with_context(context).create(d)
            p1   = 1,part.id
            self.kart_params_up( kart, kw,part )
        except exceptions.ValidationError:
            p1 = traceback.format_exc()
            s  = p1.split("ValidationError")
            if len(s) > 0:
                 p1 = 0,s[1].replace("\\","").replace('(','').replace('u0131','ı').replace('xe7','ç').replace('u015f','ş').replace('u015e','Ş').replace('u"',"").replace('")','')
        return p1
    
    @serialize_exception
    def kart_params_up(self,kart,kw,part):
        
        kart.sudo().write({'bireysel_partner_id':part.id,
                           'ad_soyad'           :kw.get('ad') + ' ' + kw.get('soyad'),
                           'tckn'               :kw.get('tckn'),
                           'cep_tel'            :kw.get('cep_tel'),
                           'email'              :kw.get('email'),
                           'adres'              :kw.get('adres'),
                           'il'                 :kw.get('il'),
                           'ilce'               :kw.get('ilce'),
                           'dogum_tar'          :kw.get('dogum_tar'),
                           'lot_state'          :'sold2'})
        
    def isValidTCID(self,value):
        value = str(value)
        if not len(value) == 11:
            return False
        if not value.isdigit():
            return False
        if int(value[0]) == 0:
            return False
        digits = [int(d) for d in str(value)]
        if not sum(digits[:10]) % 10 == digits[10]:
            return False
        if not (((7 * sum(digits[:9][-1::-2])) - sum(digits[:9][-2::-2])) % 10) == digits[9]:
            return False
        return True
    
    def check_email(self,email):
        if not re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', email):
            return False
        return True
    
    
    def calculate_age(self,born):
        born = datetime.strptime( born, '%Y-%m-%d')
        today = date.today()
        return today.year - born.year - ((today.month, today.day) < (born.month, born.day))
    
   
    def tel_conf(self,tel):
        phone = [str(s) for s in tel.split() if s.isdigit()]
        #print phone
        if phone[0][0] == "0":
            phone = [phone[0][1:]]
        if len( phone )> 0:
            phone = "".join( phone )
            if len( phone) != 10:
                return 1
            else:
                if phone[0] != '5':
                    return 2
                if not phone.isdigit():
                    return 3
        else:
            return 4
        return 5
    
    @http.route("/yemek-karti/kart-step-1",type='http', auth='none',cors='*')
    @serialize_exception
    def get_kart_step_1(self,**kw):
         #val  = self.process_form(kw)
        source = kw.get('p_source')
        return_errors = []
        if not kw.get("gun") or kw.get("gun") == "00":
            return_item = {"html_name": "gun", "error_messsage": "Gün Seçiniz."}
            return_errors.append( return_item )
        if not kw.get("ay") or kw.get("ay") == "00":
            return_item = {"html_name": "ay", "error_messsage": "Ay Seçiniz."}
            return_errors.append( return_item )
        if not kw.get("yil") or kw.get("yil") == "00":
            return_item = {"html_name": "yil", "error_messsage": "Yıl Seçiniz."}
            return_errors.append( return_item )
        
        kw['dogum_tar'] = kw.get("yil") + "-" + kw.get("ay") + "-" + kw.get("gun")
        
        if not kw.get("ad"):
            return_item = {"html_name": "ad", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append( return_item )
        if not  kw.get("soyad"):
            return_item = {"html_name": "soyad", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append( return_item )
        if not kw.get("tckn"):
            return_item = {"html_name": "tckn", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append( return_item )
        else:
            v = self.isValidTCID(kw.get('tckn'))
            if not v:
                return_item = {"html_name": "tckn", "error_messsage": "Lütfen Geçerli Bir T.C. Kimlik No Giriniz."}
                return_errors.append( return_item )
        if not kw.get("adres"):
            return_item = {"html_name": "adres", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append( return_item )
        if not kw.get("il"):
            return_item = {"html_name": "il", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append( return_item )
        if not kw.get("ilce"):
            return_item = {"html_name": "ilce", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append( return_item )
        if not kw.get("dogum_tar"):
            return_item = {"html_name": "yil", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append( return_item )
        else:
            if self.calculate_age(kw.get("dogum_tar") ) < 18:
                return_item = {"html_name": "yil", "error_messsage": "18 Yaşından Küçükler Kart Kişiselleştiremez."}
                return_errors.append( return_item )
        
        if not kw.get("cep_tel"):
            return_item = {"html_name": "cep_tel", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append( return_item )
        else:
            if kw.get("cep_tel")[0] == "0":
                kw["cep_tel"] = kw.get("cep_tel")[1:]
            #print kw["cep_tel"]
            c = self.tel_conf(  kw.get("cep_tel") )
                
            if c == 1:
                return_item = {"html_name": "cep_tel", "error_messsage": "Cep Telefonu Numarası 10 Hane Olmalıdır ve Başında 0 Olmamalıdır."}
                return_errors.append( return_item )
            elif c == 2:
                return_item = {"html_name": "cep_tel", "error_messsage": "Cep Telefonu Numarası 5 ile Başlamalıdır."}
                return_errors.append( return_item )
            elif c == 3:
                return_item = {"html_name": "cep_tel", "error_messsage": "Cep Telefonu Numarası Sadece Rakamlardan Oluşmalıdır."}
                return_errors.append( return_item )
            elif c == 4:
                return_item = {"html_name": "cep_tel", "error_messsage": "Lütfen Geçerli bir Cep Telefonu Numarası Giriniz."}
                return_errors.append( return_item )
        
        if not kw.get("email"):
            return_item = {"html_name": "email", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append( return_item )
        else:
            if not self.check_email(  kw.get("email") ):
                return_item = {"html_name": "email", "error_messsage": "Lütfen Geçerli E-Posta Adresi Giriniz."}
                return_errors.append( return_item )
        
        if return_errors:
            return json.JSONEncoder().encode({'status': 'error', 'errors': return_errors}) 
         
        guid = kw.get('k_id')
        if guid:
            kart    = http.request.env.get('stock.production.lot').sudo().search([('guid','=',guid)])
            if kart:
                ############## Burayı Aç ###############
                response = Kisisellestirme(kart,kw)
                ########################################
                #response = dummy()
                #response.Sonuc = 1
                #response.Basvuru_ID = 0
                if int(response.Sonuc) != 1 and not hasattr (response,'Basvuru_ID'):
                    kart.sudo().write({'ws_last_message': response.Sonuc_Str.replace('_',' ')})
                    return json.JSONEncoder().encode({'status': 'success', 'redirect_url': '/yemek-karti/kart-hata/%s/3'%kart.guid})
                else:
                     partner_id = self.create_bireysel_musteri( kw,kart,response )
                     if partner_id[0] != 1:
                          kart.ws_last_message = partner_id[1]
                          return json.JSONEncoder().encode({'status': 'success', 'redirect_url': '/yemek-karti/kart-hata/%s/3'%kart.guid})
                     return json.JSONEncoder().encode({'status': 'success', 'redirect_url': '/yemek-karti/iliskili-kart-docs/%s/%s/%s'%(kart.guid, urllib.quote(EncodeAES( cipher, str(partner_id[1])),safe="").replace('%2F','AaBaA'),source)})
            else:
                return json.JSONEncoder().encode({'status': 'success', 'redirect_url': '/yemek-karti/kart-hata/g/1'})
        else: 
            return json.JSONEncoder().encode({'status': 'success', 'redirect_url': '/yemek-karti/kart-hata/g/2'})
    @http.route("/yemek-karti/kart-step-2",type='http', auth='none',cors='*')
    @serialize_exception
    def get_kart_step_2(self,**kw):
        
        k_id = kw.get("k_id")
        p_id = kw.get("p_id")
        
        rd = True
        dos_dict = {}
        if kw.get("nufus_fotokopi"):
            rd = False
            dos_dict['nufus'] = (base64.b64encode((kw.get("nufus_fotokopi").stream.read())),kw.get("nufus_fotokopi").filename)
        if kw.get("adres"):
            rd = False
            dos_dict['adres'] = (base64.b64encode((kw.get("adres").stream.read())),kw.get("adres").filename)
        if kw.get("troy_sozlesme"):
            rd = False
            dos_dict['troy'] = (base64.b64encode((kw.get("troy_sozlesme").stream.read())),kw.get("troy_sozlesme").filename)
        if kw.get("kvkk_sozlesme"):
            rd = False
            dos_dict['kvkk'] = (base64.b64encode((kw.get("kvkk_sozlesme").stream.read())),kw.get("kvkk_sozlesme").filename)
    
        if not rd:
            p_id    = p_id.replace("AaBaA","%2F")
            p_id    = DecodeAES(cipher, urllib.unquote(p_id))
            
            partner = http.request.env['res.partner'].sudo().browse( [int(p_id)] )

            for e in partner.uye_is_yeri_evrak:
                if dos_dict.get(e.evrak_id.web_name ):
                    new_doc_dict = {'doc':dos_dict.get(e.evrak_id.web_name )[0],'doc_name':dos_dict.get(e.evrak_id.web_name )[1]}
                    e.write ({'docs':[(0,0,new_doc_dict)]})
            
            doc_yolla(partner)
        if kw.get('p_source') == 'p':     
            return json.JSONEncoder().encode({'status': 'success', 'redirect_url': '/yemek-karti/kart-ode/%s/%s'%(k_id,p_id)})
        elif kw.get('p_source') == 'a':
            return json.JSONEncoder().encode({'status': 'success', 'redirect_url': '/' })
            
            
    @http.route("/yemek-karti/kart-ode/<d>/<p>",type='http', auth='public',website=True)
    @serialize_exception
    def get_kart_ode(self,d,p,**kw):
         p_id    = p
         seri_no = d
         kart    = http.request.env['stock.production.lot'].sudo().search([('guid','=',seri_no)])
         
         return http.request.render('ym_forms.kart_ode', {'k_id':seri_no,'p_id':p_id,'yukleme_yapilacak':kart.name})
    
    @http.route('/yemek-karti/tl_oranlar/',type='http', auth="public")
    @serialize_exception
    def get_dynamic_tl_oranlar(self,**kw):
         res = tl_oranlar_tek(http.request)
         #{'kredi_kart_marka': Axess, 'T9': 14.7200, 'T6': 10.3000, 'T3': 5.8900, '_rowOrder': u'5', 'T1': 2.5900, 'T12': 19.1400, '_id': u'T'}
         
         html = """
         <div style="text-align:center; > 
              <div class="modal-header">
                   <h4>Banka / Ön Ödemeli / Kredi Kartı ile<br> TL Yükleme Komisyon Oranları</h4>
              </div>  
              <div class="modal-body">
                  <div class="col-sm-12" style="text-align:left">
                      <ul>
                        <li style="font-weight: bold;">Tek Çekim (400 TL' ye kadar): <span style="font-weight: normal;float: right;">%%0</span></li>
                        <li style="font-weight: bold;">Tek Çekim (400 TL üzeri): <span style="font-weight: normal;float: right;">%%%(T1)s</span></li>
                        <li style="font-weight: bold;">3 Taksit: <span style="font-weight: normal;float: right;">%%%(T3)s</span></li>
                        <li style="font-weight: bold;">6 Taksit: <span style="font-weight: normal;float: right;">%%%(T6)s</span></li>
                        <li style="font-weight: bold;">9 Taksit: <span style="font-weight: normal;float: right;">%%%(T9)s</span></li>
                        <li style="font-weight: bold;">12 Taksit: <span style="font-weight: normal;float: right;">%%%(T12)s</span></li>
                      </ul>
                   </div>
              </div>    
              <div class="col-md-12" style="text-align:center">
                   <p><br>Sadece kredi kartı ile taksitli TL yükleme işlemi yapılabilmektedir.</p>
              </div>
  
         </div>
          
         """%(res)
         return html
    @http.route('/yemek-karti/dynamic_tutar/<t>/<k>/<g>/<tk>',type='http', auth="public")
    @serialize_exception
    def get_dynamic_tutar(self,t,k,g,tk,**kw):
         
         kart_no = http.request.env['stock.production.lot'].sudo().search([('guid','=',g)])
         g       = kart_no.seri_no_computed 
         t       = t.replace(",",".") 
         res     = taksit_info(http.request,t,k,g,tk)
         
         if not res:
            html = """
            <div class="row" id="ode_f" style="color:red">
                 Kredi Kartı Bilgileri Hatalı.
             </div>
            """
         else:
            res.update({'raw_tutar':round(float(t),2)})
            
            if res.get('Column1'):
              c_splited = res.get('Column1').split('|')
              
              res['Oran']           = c_splited[4].replace(',','.')
              res['Komisyon_Tutar'] = c_splited[5].replace(',','.')
            
            if res.get("Oran"):
              res.update({'Oran':float(res['Oran'])})
            else:
              res.update({'Oran':0.0})
            if res.get('Komisyon_Tutar'):
              total = float(t) + float( res['Komisyon_Tutar'])
              res.update({'Komisyon_Tutar':round(float( res['Komisyon_Tutar'] ),2)})
            else:
              total = float(t)
              res.update({'Komisyon_Tutar':0.0})
            res.update({'total':round(total,2)})
            #{'Taksit_Oran': 6|8,94|1031|0, 'Taksit': 6, 'Taksit_Str': 6 Taksit [%8,94], '_rowOrder': u'0', 'SanalPOS_ID': 1031, 'Komisyon_Tutar': 4.4700, '_id': u'T', 'Oran': 8.9400}
            html = """
            <div class="row" id="ode_f">
                 <div class="hff col-md-3 hff_textbox form-group" data-form-type="textbox" data-field-id="31">
                   <label class="control-label" for="ttr">Yükleme Tutarı</label>
                   <input type="text" 
                   class="form-control"  name="ttr" readonly="readonly" value="%(raw_tutar)s TL"/>
                 </div>
                  <div class="hff col-md-3 hff_textbox form-group" data-form-type="textbox" data-field-id="31">
                   <label class="control-label " for="kom_oran">Komisyon Oranı</label>
                   <input type="text" 
                   class="form-control"  name="kom_oran" readonly="readonly" value="%%%(Oran)s"/>
                 </div>
                  <div class="hff col-md-3 hff_textbox form-group" data-form-type="textbox" data-field-id="31">
                   <label class="control-label " for="kom_tut">Komisyon Tutarı</label>
                   <input type="text" 
                   class="form-control"  name="kom_tut" readonly="readonly" value="%(Komisyon_Tutar)s TL"/>
                 </div>
                  <div class="hff col-md-3 hff_textbox form-group" data-form-type="textbox" data-field-id="31">
                   <label class="control-label " for="total_tut">Toplam Tutar</label>
                   <input type="text" 
                   class="form-control"  name="total_tut" readonly="readonly" value="%(total)s TL"/>
                 </div>
             </div>
            """%( res )
         return html
    @http.route('/yemek-karti/kart-ode-2',type='http', auth='none',cors='*')
    @serialize_exception
    def get_kk_payment(self, **kw):
        #{'kom_oran': u'%12.96', 'ad': u'Niyazi Yasin', 'is_ajax_post': u'Yes', 'ttr': u'60.7 TL',
        #'p_id': u'WXy943CAaBaA9OAaBaAdIAuTSMRi328QNCQMkmXbyuq4FtPyhLU=',
        #'total_tut': u'68.57 TL', 'kom_tut': u'7.87 TL', 'tutar': u'60,705', 'tk': u'9', 'k_id': u'0cdee236-0d58-42e0-899d-f907812c684e',
        #'serial_number': u'5222 0407 9284 6013'}
        
        return_errors = []
        if not kw.get("ad"):
            return_item = {"html_name": "ad", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append( return_item )
        if not  kw.get("serial_number"):
            return_item = {"html_name": "serial_number", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append( return_item )
        if not kw.get("k_id"):
            return_item = {"html_name": "k_id", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append( return_item )
        if not kw.get("ay"):
            return_item = {"html_name": "ay", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append( return_item )
        if not kw.get("yil"):
            return_item = {"html_name": "yil", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append( return_item )
        if not kw.get("cvc"):
            return_item = {"html_name": "cvc", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append( return_item )
        if not kw.get("tk"):
            return_item = {"html_name": "tk", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append( return_item )
        if not kw.get("tutar"):
            return_item = {"html_name": "tutar", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append( return_item )
            
        if return_errors:
            return json.JSONEncoder().encode({'status': 'error', 'errors': return_errors}) 
        
        #obj,Troy_KN,KK_AdSoyad,KK_No,KK_Ay,KK_Yil,KK_CVV,Taksit,Tutar
        kart_no  = http.request.env['stock.production.lot'].sudo().search([('guid','=',kw.get("k_id"))])
        g        = kart_no.seri_no_computed.replace(" ",'')
        response = TL_Yukle_KK(obj          = http.request,
                               Troy_KN      = g,
                               KK_AdSoyad   = kw.get("ad"),
                               KK_No        = kw.get("serial_number").replace(" ",''),
                               KK_Ay        = kw.get("ay"),
                               KK_Yil       = kw.get("yil"),
                               KK_CVV       = kw.get("cvc"),
                               Taksit       = kw.get("tk"),
                               Tutar        = kw.get("tutar").replace(",",'.')
                               )
        if response:
            return json.JSONEncoder().encode({'status': 'success', 'redirect_url': response,'return_type':'modal'})
        else:
            return json.JSONEncoder().encode({'status': 'success', 'redirect_url': '/yemek-karti/kart-hata/g/4'})    
    @http.route('/troy_sozlesme/', type='http', auth="none", cors="*",csrf=False)
    @serialize_exception
    def troy_sozlesme_sablon(self, **kw):
       imgname = 'troy_sozlesme'
       imgext = '.pdf'
       placeholder = functools.partial(get_resource_path, 'ym_forms', 'static')
       
       uid = None
       if not uid:
           uid = SUPERUSER_ID
       response = http.send_file(placeholder(imgname + imgext))
       
       return response

    def from_data_custom(self, fields, rows, float_colored = False):
        fp         = StringIO()
        workbook   = xlsxwriter.Workbook(fp)
        worksheet  = workbook.add_worksheet()
        bold       = workbook.add_format({'bold': True,'text_wrap': True})
        base_style = workbook.add_format({'text_wrap': True})
        format_2 = workbook.add_format({'bold': True,'text_wrap': True})
        format_2.set_align('center')
        format_2.set_align('vcenter')
        format_2.set_bg_color("#EB560A")
        format_2.set_font_color("white")
        
        datetime_style = workbook.add_format({'text_wrap': True, 'num_format':'DD-MM-YYYY HH:mm:SS'})
        date_style     = workbook.add_format({'text_wrap': True, 'num_format':'DD-MM-YYYY'})
        date_style.set_align('vcenter')
        datetime_style.set_align('vcenter')
        base_style.set_align('vcenter')
        
        int_stye = workbook.add_format({'text_wrap': False})
        int_stye.set_align('vcenter')
        if float_colored:
            int_stye.set_bg_color(float_colored[0])
            int_stye.set_font_color(float_colored[1])
        for i, fieldname in enumerate(fields):
            worksheet.write(0, i, fieldname,format_2)
            #worksheet.set_column(0,i,50)
        for row_index, row in enumerate(rows):
            #print row_index + 1
            height_setted = False
            for cell_index, cell_value in enumerate(row):
                cell_style = base_style

                if not height_setted:
                    worksheet.set_row(row_index + 1,64)
                worksheet.set_column(row_index + 1,0,30)
                if isinstance(cell_value, basestring):
                    cell_value = re.sub("\r", " ", cell_value)
                elif isinstance(cell_value, datetime):
                    cell_style = datetime_style
                elif isinstance(cell_value, date):
                    cell_style = date_style
                elif isinstance(cell_value,float):
                    cell_style = int_stye
                
                worksheet.write(row_index + 1, cell_index, cell_value, cell_style)
        workbook.close()
        fp.seek(0)
        data = fp.read()
        fp.close()
        return data

    @http.route('/excel_c/<c>/<d>/<a>/<r>/', type='http', auth='user',csrf_token=False)
    def export_xls_view(self, c, d,a,r,**kw):
       
        if a > 0:
            dataset = getattr (http.request.env[c].browse( int(d) ),a)
        else:
            dataset =  http.request.env[c].search(d)
        r = r.replace('\'',"\"")
        r = json.loads( r )
        
        columns_headers = []
        rows_p         = []
        for dict_ in r:
            columns_headers.append(dict_[1])
            rows_p.append(dict_[0])
        rows=[]
        
        for data in dataset:
            r_child = []
            for r_c in rows_p:
                if r_c != 'empty':
                    r_child.append( getattr(data,r_c) or ' ')
                else:
                    r_child.append(0.00)
            rows.append( r_child )

        excel   = self.from_data_custom(columns_headers, rows,float_colored=["#08C5E7","white"])
        filename = compute_curent_time() + c
        if filename.find('xls') > 0:
            filename = filename.replace('xls','xlsx')
        else:
            filename += '.xlsx'

        return http.request.make_response(
            excel,
            headers=[
                ('Content-Disposition', 'attachment; filename="%s"'
                 %filename),
                ('Content-Type', "application/vnd.ms-excel")
            ],
        )
    @http.route('/excel_yuk_har/<c>/<t>', type='http', auth='user',csrf_token=False)
    def export_xls_yuk_har_view(self, c, t,**kw):
        if t == 'yuk':
            rapor = http.request.env['ym_forms.yuk_har'].browse([int(c)]).yuk_rapor
            excel = base64.b64decode( rapor )
            filename = 'Yükleme Raporu - ' + compute_curent_time() + c
            if filename.find('xls') > 0:
                filename = filename.replace('xls','xlsx')
            else:
                filename += '.xlsx'
        if t == 'har':
            rapor = http.request.env['ym_forms.yuk_har'].browse([int(c)]).har_rapor
            excel = base64.b64decode( rapor )
            filename = 'Harcama Raporu - ' + compute_curent_time() + c
            if filename.find('xls') > 0:
                filename = filename.replace('xls','xlsx')
            else:
                filename += '.xlsx'
        return http.request.make_response(
            excel,
            headers=[
                ('Content-Disposition', 'attachment; filename="%s"'
                 %filename),
                ('Content-Type', "application/vnd.ms-excel")
            ],
        )    