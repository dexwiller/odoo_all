# See LICENSE file for full copyright and licensing details.
{
    "name": "Tour Travel Management System",
    "version": "12.0.1.0.0",
    'sequence': 1,
    "author": "Serpent Consulting Services Pvt. Ltd.",
    'maintainer': 'Serpent Consulting Services Pvt. Ltd.',
    "website": "http://www.serpentcs.com",
    'description': """
    Tour Travel Package Design
    Hospitality
    Travel agency
    Tour agency
    Tours and travel
    tour package design
    visa tourist hotel booking
    ticket booking
    hotel booking
    package booking
    visa management
    tourist guide management
    transport management
    tourism industry
    travel planning
    route plan
    group tour packages
    online package design
    online hotel booking
    online tour travel management
    """,
    'summary': """
    Tour Travel Package Design
    """,
    'license': 'LGPL-3',
    'depends': ['tour_travel_package_design', 'web_widget_date_validation',
                'website_tour_agency'],
    'images': ['static/src/img/tour-pkg.jpg'],
    'category': 'Extra Tools',
    'installable': True,
    'price': 1,
    'currency': 'EUR',
}
