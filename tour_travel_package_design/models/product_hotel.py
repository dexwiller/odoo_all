
from odoo import fields, models, api

class Hoteltags(models.Model):
    _name = "hotel.tag"
    _description = "Hotel tags"

    name = fields.Char('Hotel Tag')


class ProductAsHotelTemplate(models.Model):
    _inherit = 'product.template'

    @api.depends('list_price')
    def _currency_convert(self):
        """
        Convert the currency to related company's currency.

         ----------------------------------------------------
        @param self : object pointer
        """
        date_today = fields.Date.context_today(self)
        for rec in self:
            if rec.company_id:
                company = rec.company_id
            else:
                company = self.env.user.company_id
            company_currency = rec.company_id.currency_id
            currency_id = self.env['res.currency'].browse(3)
            rec.converted_amount = company_currency._convert(
                rec.list_price, currency_id, company, date_today)

    converted_amount = fields.Float(compute='_currency_convert',
                                    string='Amount Converted', store=True)
    hotel_tag = fields.Many2one('hotel.tag', 'Hotel Tag',
                                help="This Tag will be display on hotel.")
    rating = fields.Selection([('0', 'None'),
                               ('1', 'Bad'),
                               ('2', 'Average'),
                               ('3', 'Good'),
                               ('4', 'Very Good'),
                               ('5', 'Excellent')], 'Product rating',
                              index=True)
    street = fields.Char('Street')
    street2 = fields.Char('Street2')
    zip = fields.Char('Zip', change_default=True)
    city = fields.Char('City')
    state_id = fields.Many2one("res.country.state", string='State')
    country_id = fields.Many2one('res.country', string='Country')
    hotel_ok = fields.Boolean(
        'Is Hotel ?', default=False)
