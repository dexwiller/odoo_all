#  -*- coding: utf-8 -*-
#  Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import qweb_package_report
from . import invoice_package_report
from . import qweb_po_package_report
from . import booking_ref_room_report
from . import rfq_quote_report
