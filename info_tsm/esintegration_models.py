# -*- coding: utf-8 -*-

from openerp import models, fields, api

class ESFirma(models.Model):
    _name = 'info_tsm.harici_firmalar'
    
    @api.model
    def _get_default_licence(self):
        import random, string
        x = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(16)).upper()
        return x[0:4] + '-' + x[4:8] + '-' + x[8:12] +'-'+ x[12:16]
    musteri     = fields.Many2one('res.partner',string='Müşteri ( Cari )' )
    name        = fields.Char('Firma Adı')
    kurum_kodu  = fields.Char('Kurum Kodu')
    kurum_token = fields.Char('Kurum Token')
    lisans_no   = fields.Char('Lisans No',  default = _get_default_licence)
    
    

class okcFisBilgisi( models.Model):
    _name = 'info_tsm.harici_sistemler_okc_fis_bilgisi'

    kurum_kodu          = fields.Char('Kurum Kodu')
    kurum_token         = fields.Char('Kurum Token')
    es_cihaz_seri_no    = fields.Char('es Cihaz Seri No')
    satis_tipi          = fields.Char('Satış Tipi')
    es_islem_id         = fields.Char('es İşlem ID')
    kalem_verisi        = fields.Char('Kalem Verisi')

class es_logs ( models.Model ):

    _name='info_tsm.es_logs'

    serviceName   = fields.Char(string='serviceName')
    operationName = fields.Char(string='operationName')
    requestTime   = fields.Datetime(string='requestTime')
    responseTime  = fields.Datetime(string='responseTime')
    requestData   = fields.Char(string='requestData')
    responseData  = fields.Char(string='responseData')
    write_date    = fields.Datetime(string="WriteDate", required=False, )
    baslangic_tarihi           = fields.Date( string ='Başlangıç Tarihi', compute = '_tarih_araligi')
    bitis_tarihi               = fields.Date( string ='Bitiş Tarihi', compute = '_tarih_araligi')

    @api.multi
    def _tarih_araligi( self ):
         return lambda *a,**k:{}
    _order = 'id desc'
