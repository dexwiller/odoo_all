# -*- coding: utf-8 -*-
from openerp import models, fields, api, exceptions
from gib_message_formatter import gib_formatter, gibSocket, gib_parameters
from parameter_models import terminal_parametre_history, uygulama_parametreleri
import hashlib,json
from collections import Counter
from parameter_models import bip_tipleri
from check_rules import check
class VersionChangeWizard(models.TransientModel):
	_name          = 'info_tsm.version_change_wizard'
	
	source_version = fields.Many2one('info_tsm.program_version',string="Cihazlardan Kaldırılacak Versiyon : ",domain=[('group_id','!=',False)])
	program_id     = fields.Many2one(related='source_version.program_id')
	dest_version   = fields.Many2one('info_tsm.program_version',string="Cihazlara Eklenecek Versiyon : ")
	
	@api.multi
	def open_confirm(self):
		return dict(
					type      = 'ir.actions.act_window',
					name      = "Şifre Giriniz",
					res_model = "info_tsm.change_version_confirm",
					src_model = "info_tsm.version_change_wizard",
					view_type = 'form',
					view_mode = 'form',
					target    = "new",
				)

	@api.multi
	def change_vers(self):
		print 'Burda'
		self.ensure_one()
		grsss = self.source_version.group_id
		for g in grsss:
			print g.id
			g.write ({'prog_version':[(3,self.source_version.id)]})
			g.write ({'prog_version':[(4,self.dest_version.id)]})		
			
			history_obj    = self.env['info_tsm.program_version_history'].sudo()
			for lot_id in g.terminal_id:
				history_obj.create({'version_id':self.source_version.id,
									'islem_tipi':'delete',
									'lot_id':lot_id.id})
				history_obj.create({'version_id':self.dest_version.id,
								'islem_tipi':'add',
								'lot_id':lot_id.id})
			
class change_version_confirm ( models.TransientModel ):
	_name = 'info_tsm.change_version_confirm'

	passwd = fields.Char(string='Şifre Giriniz :')
	
	@api.multi
	def sub(self,**kw):
		
		print self._context
		print kw
		print self.passwd
		
		v = self.env['info_tsm.version_change_wizard'].browse([ kw.get('active_id')])
		print v
		if self.passwd == 'manager':
			print 'Pass Doğru'
			v.change_vers()
		else:
			print 'Pass Yanlış'
			raise exceptions.ValidationError(u"Hatalı Şifre Girdiniz. Tekrar Deneyiniz.")
		self.passwd = ""
		
		return v.dest_version.id
	
class KurWizard(models.TransientModel):
	_name          = 'info_tsm.kur_wizard'
	terminal_id    = fields.Many2one('stock.production.lot',string="Terminal",domain=[('lot_state','=','sold'),('terminal_id','!=',False)])
	sira_no        = fields.Char(string='Kur Sıra No')

	@api.multi
	def kur_send_gib(self):

		kur_objs = self.env['info_tsm.kur_bilgileri'].browse(self._context.get('active_ids'))
		gib_formatter_obj         = gib_formatter()
		s                         = gibSocket()
		tanimli_para_birimi_adedi = gib_formatter_obj.eksik_byte_tamamlamaca ( str(len( kur_objs )), 2)

		obj_p = gib_parameters( self.sira_no )

		message_header = '''6000010000FF8812'''
		message_part_1 = obj_p.message_part_1

		param_save_date       = obj_p.save_date
		terminal_id           = self.terminal_id[0].terminal_id
		if not terminal_id:
			raise exceptions.ValidationError('GİB Terminal ID Bulunamadı, Seçilen erminalin satışının yapıldığından eminseniz, Terminalden İlk Kayıt Mesajı Gönderiniz.')
		message_part_2_header = 'DF40'
		message_part_2        = 'DFC00108%sDFC00705%s' %( terminal_id , param_save_date )
		kur_header            = 'DFC009'
		kur_bilgileri         = tanimli_para_birimi_adedi

		for obj in kur_objs:
			para_birimi_kodu       = gib_formatter_obj.eksik_byte_tamamlamaca( obj.para_birimi_kodu, 4)
			para_birimi_kisaltmasi = gib_formatter_obj.eksik_byte_tamamlamaca( gib_formatter_obj.str_to_ascii (obj.para_birimi_kisaltmasi), 3, 'ascii')
			para_birimi_isareti    = gib_formatter_obj.eksik_byte_tamamlamaca( gib_formatter_obj.str_to_ascii (obj.para_birimi_isareti), 3, 'ascii')
			isaret_yonu            = gib_formatter_obj.eksik_byte_tamamlamaca( gib_formatter_obj.str_to_ascii (obj.isaret_yonu), 1, 'ascii')
			binlik_ayraci          = gib_formatter_obj.eksik_byte_tamamlamaca( gib_formatter_obj.str_to_ascii (obj.binlik_ayraci), 1, 'ascii')
			kurus_ayraci           = gib_formatter_obj.eksik_byte_tamamlamaca( gib_formatter_obj.str_to_ascii (obj.kurus_ayraci), 1, 'ascii')
			kurus_basamak_adedi    = gib_formatter_obj.eksik_byte_tamamlamaca( obj.kurus_basamak_adedi, 2)
			kur_cevrimi            = gib_formatter_obj.eksik_byte_tamamlamaca( int(obj.kur_cevrimi), 12)

			kur_bilgileri +=        para_birimi_kodu +\
									para_birimi_kisaltmasi +\
									para_birimi_isareti +\
									isaret_yonu +\
									binlik_ayraci +\
									kurus_ayraci +\
									kurus_basamak_adedi +\
									kur_cevrimi

		uzunluk_kur_len        = gib_formatter_obj.uzunluk_lrc_final_send_data_hesaplamaca( kur_bilgileri , True )
		kur_final_str          = gib_formatter_obj.uzunluk_prefix_formatlamaca(uzunluk_kur_len[1],  kur_header )  + kur_bilgileri

		message_part_2         = message_part_2  + kur_final_str

		uzunluk_message_part_2 = gib_formatter_obj.uzunluk_lrc_final_send_data_hesaplamaca( message_part_2 , True )
		message_part_2         = gib_formatter_obj.uzunluk_prefix_formatlamaca(uzunluk_message_part_2[1],  message_part_2_header ) + message_part_2

		hash_sha256            = hashlib.sha256()
		hash_sha256.update     ( kur_bilgileri )
		hash_kur               = hash_sha256.hexdigest()
		kur_sign_data          = hash_kur + terminal_id + obj_p.sira_no + obj_p.imza_saati
		kur_sign               = obj_p.sign_data ( obj_p.p_key_loc + 'Sgibsign.key',kur_sign_data  )
		message_footer         = 'DF42820106DFC20A820100' + kur_sign
		final_message          = message_part_1 + message_part_2 + message_footer

		uzunluk_message        = gib_formatter_obj.uzunluk_lrc_final_send_data_hesaplamaca ( final_message, True  )

		message                = gib_formatter_obj.uzunluk_prefix_formatlamaca(uzunluk_message[1],  message_header ) + final_message

		send_message           = gib_formatter_obj.uzunluk_lrc_final_send_data_hesaplamaca( message )

		s.connect(gib_formatter.GIB_SERVER_IP, gib_formatter.GIB_SERVER_PORT)
		s.gibsend( send_message )
		donen =  s.gibreceive()

		s.close()

		return  [{'warning': {
					'title': 'Hi!',
					'message':  'Your data were saved successfully',
					}
				}]
class grup_OKC_wizard(models.TransientModel):

	@api.model
	def get_domain ( self ):

		param_prod = self.env['ir.config_parameter'].get_param('enviroment',default='development')
		domain     =  []

		if param_prod == 'production':
			domain = [('name','ilike','OF')]

		return domain

	_name    = 'info_tsm.grup_okc_wizard'
	okc_list = fields.Many2many('stock.production.lot', string='YÖKC Terminaller',domain=get_domain)

	def _default_grup(self):
		return self.env['info_tsm.parametre_grup'].browse(self._context.get('active_id'))

	grup_id = fields.Many2one('info_tsm.parametre_grup',
		string="Grup", required=True, default=_default_grup)

	firma               = fields.Many2one('res.partner', string='Müşteri', domain=[('active','=',True),
																				   ('is_company','=',True),
																				   ('satilan_lots','!=',False)])

	@api.one
	def okc_terminal_grup_set( self ):
		okc_to_add = []
		for okc in self.okc_list:
			#history first
			history_dict = dict(terminal_id  = okc.id,
								parametre    = self.grup_id.parametre.id,
								indirildi_mi = False)
			self.env['info_tsm.parametre_history'].sudo().create( history_dict )

			okc_to_add.append((4,okc.id))
		self.grup_id.terminal_id = okc_to_add
class urun_grup_OKC_wizard(models.TransientModel):
	_name    = 'info_tsm.urun_grup_okc_wizard'

	@api.model
	def get_domain ( self ):

		param_prod = self.env['ir.config_parameter'].get_param('enviroment',default='development')
		domain     =  []

		if param_prod == 'production':
			domain = [('name','ilike','OF')]

		return domain

	okc_list = fields.Many2many('stock.production.lot', string='YÖKC Terminaller',domain=get_domain)

	def _default_grup(self):
		return self.env['info_tsm.okc_urun_grup'].browse(self._context.get('active_id'))

	def _default_firma(self):
		return self.env['info_tsm.okc_urun_grup'].browse(self._context.get('active_id')).firma


	grup_id = fields.Many2one('info_tsm.okc_urun_grup',
		string="Grup", required=True, default=_default_grup)

	firma               = fields.Many2one('res.partner', string='Müşteri', domain=[('active','=',True),
																				   ('is_company','=',True),
																				   ('satilan_lots','!=',False)],
										  default = _default_firma)

	@api.one
	def okc_terminal_grup_set_urun( self ):
		#print self.okc_list
		for okc in self.okc_list:
			#print self.grup_id,okc.id

			okc.urun_params = self.grup_id.id
class grup_PROG_wizard(models.TransientModel):
	_name    = 'info_tsm.grup_prog_wizard'
	version_list = fields.Many2one('info_tsm.program_version', string='Versiyonlar')

	def _default_grup(self):

		return self.env['info_tsm.parametre_grup'].browse(self._context.get('active_id'))

	@api.model
	def _ekli_programlar(self):
		ekli_id_list = []
		ekliwqnames =  map ( lambda x:x.program_id.programtype.wqname, self._default_grup().prog_version )
		#print ekliwqnames
		for version in self._default_grup().prog_version:
			wqname = version.program_id.programtype.wqname
			if wqname in ekliwqnames:
				ekli_id_list.append( version.program_id.id)


		if self._context.has_key('tip') and self._context['tip'] == 'guncelle':
			domain = [('id', 'in', ekli_id_list)]
		else:
			domain = [('id', 'not in', ekli_id_list)]

		return domain

	grup_id = fields.Many2one('info_tsm.parametre_grup',
		string="Grup", required=True, default=_default_grup)

	program               = fields.Many2one('info_tsm.programs', string='Program',domain=_ekli_programlar)

	@api.multi
	def prog_grup_set( self ):
		if self._context.has_key('tip') and self._context['tip'] == 'ekle':
			for grup in self.grup_id:
				grup.prog_version |= self.version_list
		else:
			for grup in self.grup_id:
				for vers in grup.prog_version:
					if vers.program_id.id == self.version_list.program_id.id:
						grup.prog_version = [(3,vers.id)]
						grup.prog_version = [(4,self.version_list.id)]
class grup_ilk_iletisim_wizard(models.TransientModel):
	_name    = 'info_tsm.grup_ilk_iletisim_wizard'
	def _default_grup(self):

		return self.env['info_tsm.parametre_grup'].browse(self._context.get('active_id'))

	grup_id = fields.Many2one('info_tsm.parametre_grup',
		string="Grup", required=True, default=_default_grup)

	ilk_iletisim_list = fields.Many2one('info_tsm.ilk_iletisim_params', string='İlk İletişim Parametresi')
	@api.one
	def ilk_iletisim_grup_set( self ):
		for p in self.ilk_iletisim_list:
			p.uygulama_params = self.grup_id
class grup_haberlesme_wizard(models.TransientModel):
	_name    = 'info_tsm.grup_haberlesme_wizard'
	def _default_grup(self):

		return self.env['info_tsm.parametre_grup'].browse(self._context.get('active_id'))

	grup_id = fields.Many2one('info_tsm.parametre_grup',
		string="Grup", required=True, default=_default_grup)

	haberlesme_list = fields.Many2one('info_tsm.haberlesme', string='Haberleşme Parametresi')
	@api.one
	def haberlesme_grup_set( self ):
		for h in self.haberlesme_list:
			h.uygulama_params = self.grup_id
class Program_Version_OKC_Update_Wizard( models.TransientModel):
	
	def get_domain_vers_ids( self ):
		#print self._context
		
		return [('id','in',self._context.get( 'vers_ids')),
				('isactive','=',True)]
	
	_name    = 'info_tsm.vers_okc_up_wiz'
	versiyon_list    = fields.Many2one ('info_tsm.program_version',string="Program Versionları",domain=get_domain_vers_ids)
	
	@api.one
	def okc_terminal_grup_set( self ):
		self.ensure_one()
		
		parent_view            = self.env['info_tsm.program_version'].browse([self._context.get('active_id')])
		parent_view.update_v_f = self.versiyon_list.id		
class Program_Version_OKC_wizard( models.Model):
	_name    = 'info_tsm.vers_okc_wiz'

	@api.model
	def default_get(self,field_list):
		#print 'aaaa',field_list
		res = super( Program_Version_OKC_wizard ,self ).default_get( field_list )
		res['history_count'] = 0
		if self._context.has_key('ids'):
			device_ids = self._context.get ('ids')
		elif self._context.get('active_ids'):
			device_ids = self._context.get('active_ids')
		if device_ids:
			res['okc_list'] =  device_ids
			grups  = self.env['info_tsm.parametre_grup'].search([('terminal_id','in',device_ids)])
			if len(device_ids) == 1:
				if len( grups ) > 0:
					vers_list =  grups[0].prog_version
					for v in vers_list:
						v.to_delete = False
						v.update_v_f = False
					final_prog_list = []
					res['versiyon_list'] = map( lambda x:x.id,filter( lambda x:x if x.program_id.programtype.kod in self._context.get('types') else None, vers_list))
					res['history']       = self.env['info_tsm.program_version_history'].search([('lot_id','in',device_ids),
																								('version_id.program_id.programtype.kod','in',self._context.get('types'))],order='id desc').ids
					res['history_count'] = len( res['history'])
					acquirer_history = self.env['mrp.repair'].sudo().search([('lot_id','in',device_ids),
																			 ('ws_acquirerId','!=',False)],order = 'id desc')

					acquirer_histories = []
					for h in acquirer_history:

						history_dict = dict(banka_adi      = h.ws_acquirerId.name,
											islem_tipi = h.isEmriKodu,
											lot_id     = device_ids[0])
						new_ah       = self.env['info_tsm.acquirer_history'].create(  history_dict )
						acquirer_histories.append( new_ah.id )
					res['acquirer_history'] = acquirer_histories


			elif len( device_ids) > 1:
				#ortaklari bul
				ortak_list 		= []
				final_list 		= []
				#final_prog_list = []
				for g in grups:
					ortak_list.extend( map( lambda x:x.id,filter( lambda x:x if x.program_id.programtype.kod in self._context.get('types') else None, g.prog_version)) )
				ortaks = Counter( ortak_list )
				for k, v in ortaks.iteritems():
					if v  == len( device_ids ):
						final_list.append( k )
						#final_prog_list.append( self.env['info_tsm.program_version'].browse([ k ]).program_id.id)
				res['versiyon_list'] = final_list

		return res

	@api.model
	def _compute_program_id_domain_default(self):
		types = self._context.get('types')#okc,mh,appmgr

		#print types

		final_vers_ids = []
		domain = []
		for t in types:
			prog     = self.env['info_tsm.programs'].search([('programtype.kod','=',t)])
			prog_ids = map(lambda x:x.id, prog )
			if self.env.user.company_id.id == 1:
				for p in prog_ids:
					last_2_vers = self.env['info_tsm.program_version'].search([('program_id','=',p),('isactive','=',True)],order='major desc, minor desc, build desc, revision desc')
					final_vers_ids.extend( map(lambda x:x.id ,last_2_vers))
			else:
				for p in prog_ids:
					last_2_vers = self.env['info_tsm.program_version'].search([('program_id','=',p),
																		   ('listelere_ekle','=',True),
																		   ('isactive','=',True)],
					order='major desc, minor desc, build desc, revision desc',limit=1)
					final_vers_ids.extend( map(lambda x:x.id ,last_2_vers))
		prog_ids = []
		for v in self.versiyon_list:
			prog_ids.append( v.program_id.id)

		return json.dumps(
				[('id', 'in', final_vers_ids),
				 ('program_id', 'not in',prog_ids ),
				 ('isactive','=',True)]
			)
	
	@api.model
	def get_cihaz_domain(self):

		domain = [(1,'=',2)]
		if self._context.has_key('ids'):
			domain = [('sold_to','=',self.env['stock.production.lot'].browse ( [self._context.get( 'ids' )[0]]).sold_to.id)]
		elif self._context.has_key('active_ids'):
			domain = [('sold_to','=',self.env['stock.production.lot'].browse ( [self._context.get( 'active_ids' )[0]]).sold_to.id)]

		return domain

	okc_list         = fields.Many2many ('stock.production.lot', string='YÖKC Terminaller',domain=get_cihaz_domain)
	versiyon_list    = fields.Many2many ('info_tsm.program_version',string="Program Versionları")
	history          = fields.Many2many ('info_tsm.program_version_history',string='Versiyon Tarihçesi')
	history_count    = fields.Integer()
	acquirer_history = fields.Many2many('info_tsm.acquirer_history')

	program_id_domain = fields.Char(
		compute="_compute_program_id_domain",
		readonly=True,
		store=False,
		default = _compute_program_id_domain_default
	)

	@api.multi
	@api.depends('versiyon_list')
	def _compute_program_id_domain(self):
		types = self._context.get('types')#okc,mh,appmgr
		final_vers_ids = []
		domain = []
		for t in types:
			prog     = self.env['info_tsm.programs'].search([('programtype.kod','=',t)])
			prog_ids = map(lambda x:x.id, prog )
			if self.env.user.company_id.id == 1:
				for p in prog_ids:
					last_2_vers = self.env['info_tsm.program_version'].search([('program_id','=',p),('isactive','=',True)],order='major desc, minor desc, build desc, revision desc')
					final_vers_ids.extend( map(lambda x:x.id ,last_2_vers))
			else:
				for p in prog_ids:
					last_2_vers = self.env['info_tsm.program_version'].search([('program_id','=',p),
																		   ('listelere_ekle','=',True),
																		   ('isactive','=',True)],
					order='major desc, minor desc, build desc, revision desc',limit=1)
					final_vers_ids.extend( map(lambda x:x.id ,last_2_vers))

		prog_ids = []
		for v in self.versiyon_list:

			prog_ids.append( v.program_id.id)
		for rec in self:
			rec.program_id_domain = json.dumps(
				[('id', 'in', final_vers_ids),
				 ('program_id', 'not in',prog_ids ),
				 ('isactive','=',True)]
			)

	firma           = fields.Many2one('res.partner', string='Müşteri', domain=[('active',      '=', True),
																			   ('is_company',  '=', True),
																			   ('satilan_lots','!=',False)
																			   ]
									)
	@api.multi
	def okc_terminal_grup_set( self,**kwgars ):
		
		#print self._context
		if not kwgars.get( 'types'):
			exceptions.ValidationError('Kayıt Yapılamadı, Tekrar Deneyiniz.')
		history_obj = self.env['info_tsm.program_version_history'].sudo()
		for lot_id in self.okc_list:
			#print 'Lot ID:', lot_id
			if lot_id.uygulama_params and not lot_id.uygulama_params.default_param and not lot_id.uygulama_params.test_default_param:
				#onceden grubu var, her bir uygulama icin bu grupta kayit var mi?
				#print 'AAAAAAAAAAAAAAAAAAAAAAAA'
				for v in self.versiyon_list:
					#print 'Versionlar :',v.id  , v.update_v_f 
					olan_vers_ids = map( lambda t:(3,t.id),filter( lambda x:x if x.program_id.id == v.program_id.id else None, lot_id.uygulama_params.prog_version))#olan uygulamanin eski versiyonunu bulur !!!dokunma!!!
					if len( olan_vers_ids ) > 0:
						if v.id != olan_vers_ids[0][1]:
							lot_id.uygulama_params.prog_version = olan_vers_ids #olan uygulamanin eski versiyonunu siler !!!dokunma!!!
							history_obj.create({'version_id':olan_vers_ids[0][1],
										'islem_tipi':'delete',
										'lot_id':lot_id.id})
							#print  'Değişen Version  : ', v.update_v_f.id or v.id
							lot_id.uygulama_params.prog_version = [(4,v.update_v_f.id or v.id)]#olan uygulamanin yeni versiyonunu ekler.
							history_obj.create({'version_id':v.update_v_f.id or v.id,
										'islem_tipi':'add',
										'lot_id':lot_id.id})
							v.update_v_f = False
						else:
							if v.update_v_f:
								lot_id.uygulama_params.prog_version = [(3,v.id)]
								lot_id.uygulama_params.prog_version = [(4,v.update_v_f.id)]
								history_obj.create({'version_id':v.id,
										'islem_tipi':'delete',
										'lot_id':lot_id.id})
								history_obj.create({'version_id':v.update_v_f.id,
										'islem_tipi':'add',
										'lot_id':lot_id.id})
								v.update_v_f = False
								
								
					else:
						#print  'Değişen Version  : ', v.update_v_f.id or v.id
						lot_id.uygulama_params.prog_version = [(4,v.update_v_f.id or v.id)]#olan uygulamanin yeni versiyonunu ekler.
						history_obj.create({'version_id':v.update_v_f.id or v.id,
										'islem_tipi':'add',
										'lot_id':lot_id.id})
						v.update_v_f = False
					
					#print 'Silinecek : ', v.id,v.name, v.to_delete
					types = kwgars.get('types')
					if not types:
						types = self.env.context.get('types')
					if v.program_id.programtype.kod in types and v.to_delete:
						lot_id.uygulama_params.prog_version = [(3,v.id)]
						history_obj.create({'version_id':v.id,
											'islem_tipi':'delete',
											'lot_id':lot_id.id})
						v.to_delete  = False
					
					
				'''
				for v_on in lot_id.uygulama_params.prog_version:
					if v_on.program_id.programtype.kod in kwgars.get('types' ):
						olan_prog = v_on.program_id.id
						if not olan_prog in map( lambda x:x.program_id.id, self.versiyon_list):
							lot_id.uygulama_params.prog_version = [(3,v_on.id)]
							history_obj.create({'version_id':v_on.id,
												'islem_tipi':'delete',
												'lot_id':lot_id.id})
				'''			
			else:
				#create uyg params with default parameters.
				
				company_id = lot_id.sudo().company_id.id
				default_uyg_default_get = self.with_context({'types':['01','03','07','08'],'ids':self.okc_list.ids}).default_get(self._fields)
				#print default_uyg_default_get
				default_uyg_ids = default_uyg_default_get.get('versiyon_list')
				ver_types       = self.versiyon_list.mapped('program_id').mapped('programtype').mapped('kod')
				if default_uyg_ids:
					for u_id in default_uyg_ids:
						if self.env['info_tsm.program_version'].browse( u_id ).program_id.programtype.kod not in ver_types:
							self.versiyon_list = [(4,u_id)]
				
				#print self.versiyon_list
				
				new_params = {'name':lot_id.name,
						  'terminal_id':[(4,lot_id.id),],
						  'prog_version':map( lambda x:(4,x.id) if not x.update_v_f else (4,x.update_v_f.id), self.versiyon_list ),
						  'otomatik':True,
						  'company_id':company_id}

				self.env['info_tsm.parametre_grup'].create( new_params )
				types = kwgars.get('types')
				if not types:
					types = self.env.context.get('types')
				for v in self.versiyon_list:
					if v.program_id.programtype.kod in types and v.to_delete == True:
						lot_id.uygulama_params.prog_version = [(3,v.id)]
					history_obj.create({'version_id':v.update_v_f.id or v.id,
										'islem_tipi':'add',
										'lot_id':lot_id.id})
					v.update_v_f = False
					v.to_delete  = False
		
		return {'return':True, 'type':'ir.actions.act_window_close' }
	@api.model
	def create( self,vals):
		flags = {}
		#print vals
		if vals.get('versiyon_list'):
			versions = []
			for vrs in vals.get('versiyon_list'):
				if len(vrs) > 2:
					versions.extend (self.env['info_tsm.program_version'].browse( vrs [2]))
				else:
					versions.extend (self.env['info_tsm.program_version'].browse( vrs [1]))
			#print 'Versions : ', versions
			for v in versions:
				flags[v] = [v.update_v_f, v.to_delete ]
		
		res = super(Program_Version_OKC_wizard,self).create( vals )
		for k,v in flags.iteritems():
			
			k.update_v_f = v[0]
			k.to_delete  = v[1]
		
		return res		
class ServisOzelikWizard( models.TransientModel):
	_name = 'info_tsm.okc_servis_ozellik'
	
	@api.model
	def default_get(self, fields_list ):
		res = super(ServisOzelikWizard,self).default_get( fields_list )
		lot_id   =  self.env['stock.production.lot'].browse (self._context.get('active_id'))
		res['lot_id']     = self._context.get('active_id')
		res['satis_bayi'] = lot_id.company_id.id
		last_repair = self.env['mrp.repair'].sudo().search([('lot_id','=',lot_id.id)],order='id desc')
		son_kurulum_bayi = False
		
		if lot_id.set_edilen_servis_bayi:
			res['set_servis'] = lot_id.set_edilen_servis_bayi.id
		
		if last_repair:
			son_kurulum_bayi = last_repair[-1].servis_bayi.id
		elif lot_id.set_edilen_servis_bayi:
			son_kurulum_bayi = lot_id.set_edilen_servis_bayi.id
		if not son_kurulum_bayi:
			if lot_id.company_id.partner_id.profil.profil_tipi > 1:
				son_kurulum_bayi =  lot_id.company_id.id
			else:
				son_kurulum_bayi =  lot_id.company_id.partner_id.kurulum_bayi.id
		
		res['son_kurulum_bayi'] = son_kurulum_bayi
		
		return res
		
	lot_id             = fields.Many2one('stock.production.lot',string='Seri No')
	satis_bayi         = fields.Many2one('res.company',related='lot_id.company_id',string='Satış Bayi')
	son_kurulum_bayi   = fields.Many2one('res.company',string='Kurulum Bayi')
	set_servis         = fields.Many2one('res.company',domain=[('partner_id.profil.profil_tipi','>',1)])
	
	@api.multi
	def okc_terminal_servis_set( self ):
		
		#print 'aaaa'
		
		self.ensure_one()
		if not self.set_servis:
			raise exceptions.ValidationError('Önce Atanacak Servisi Seçiniz.')
		
		#print self.lot_id
		#print self.set_servis.id
		
		self.lot_id.set_edilen_servis_bayi = self.set_servis.id	
class Params_OKC_wizard( models.TransientModel):

	_name    = 'info_tsm.params_okc_wiz'
	@api.model
	def default_get(self,field_list):
		res = super( Params_OKC_wizard ,self ).default_get( field_list )

		if self._context.has_key('ids'):
			device_ids = self._context.get ('ids')
		elif self._context.get('active_ids'):
			device_ids = self._context.get('active_ids')
		if device_ids:
			res['okc_list'] = device_ids
			if len(device_ids) == 1:
				paramset = {}
				grup = self.env['stock.production.lot'].browse( device_ids ).uygulama_params
				if grup and not grup.test_default_param and not grup.default_param:
					parameters = grup.parametre
					haberlesme = grup.haberlesme
				else:
					parameters = self.env['info_tsm.uygulama_parametreleri'].search([('default','=',True)])[-1]
					haberlesme = self.env['info_tsm.haberlesme'].search([('default','=',True)])[-1]
				if self._context.has_key('hide_infoteks_fields') and not self._context['hide_infoteks_fields']:
					paramset = {'appissilent'			:parameters.appissilent,
									'gprsopenmode'		:parameters.gprsopenmode,
									'foreignsales'		:parameters.foreignsales,
									'flightmode'		:parameters.flightmode,
									'sendsalestsm'		:parameters.sendsalestsm,
									'seperatedproc'		:parameters.seperatedproc,
									'statereptype'		:parameters.statereptype.id,
									'staterepperiod'	:parameters.staterepperiod,
									"sendsalestsmtime"	:parameters.sendsalestsmtime,
									"stocksyncmode"		:parameters.stocksyncmode,
									"stocksynctime"		:parameters.stocksynctime,
									"prmupdatetype"		:parameters.prmupdatetype.id,
									"verialmaipadresi"	:parameters.verialmaipadresi,
									"verilmaportno"		:parameters.verilmaportno,
									"externalcomstatus"	:parameters.externalcomstatus,
									"salestypeallowedit":parameters.salestypeallowedit
							}
				elif self._context.has_key('hide_bayi_fields') and not self._context['hide_bayi_fields']:
					paramset = {'canfingerlogin'		:parameters.canfingerlogin,
									'quicksalemode'		:parameters.quicksalemode,
									'partialpaydisabled':parameters.partialpaydisabled
									}
				elif self._context.has_key('hide_senkron_fields') and not self._context['hide_senkron_fields']:
					paramset = { 	'haberlesme'    :haberlesme.id,
									'loginrequire'	:parameters.loginrequire,
									'acssuspend'	:parameters.acssuspend.id,
									'btssuspend'	:parameters.btssuspend.id,
									'acsleep'		:parameters.acsleep.id,
									'btsleep'		:parameters.btsleep.id,
									'left_button'	:parameters.left_button.id,
									'enddaytype'	:parameters.enddaytype.ids,
									'enddaytime'	:parameters.enddaytime,
									'keybeep'		:parameters.keybeep,
									'keybeeptype'	:parameters.keybeeptype,
									"drawerkey"		:parameters.drawerkey,
									"drawerkeypword":parameters.drawerkeypword,
									"right_button"	:parameters.right_button.id,
									"printzslip"    :parameters.printzslip,
									}
						
				res.update ( paramset )
		return res

	@api.model
	def _get_default_sleep( self ):
		return self.env['info_tsm.p_sleep_suspend'].search([('sleep_default', '=', True)]).id
	@api.model
	def _get_default_suspend( self ):
		return self.env['info_tsm.p_sleep_suspend'].search([('suspend_default', '=', True)]).id

	@api.model
	def get_cihaz_domain(self):

		domain = [(1,'=',2)]
		if self._context.has_key('ids'):
			domain = [('sold_to','=',self.env['stock.production.lot'].browse ( [self._context.get( 'ids' )[0]]).sold_to.id)]
		elif self._context.has_key('active_ids'):
			domain = [('sold_to','=',self.env['stock.production.lot'].browse ( [self._context.get( 'active_ids' )[0]]).sold_to.id)]

		return domain

	_defaults = {
					'keybeep':True,
					'keybeeptype':1
				}
	okc_list            = fields.Many2many ('stock.production.lot', string='YÖKC Terminaller',domain=get_cihaz_domain)
	param_id            = fields.Many2one('info_tsm.uygulama_parametreleri')
	appissilent         = fields.Boolean(string='Arka Planda Çalış?')
	haberlesme          = fields.Many2one('info_tsm.haberlesme',string='Haberleşme Bilgileri')
	loginrequire        = fields.Boolean(string='Login Zorunlu mu?')
	canremovesalesline  = fields.Boolean(string='Satış Satırını Kaldır?')
	canfingerlogin      = fields.Boolean(string='Parmak İzi Kullan?')
	gprsopenmode        = fields.Boolean(string='Manuel GPRS Kullan?')
	foreignsales        = fields.Boolean(string='Uluslar Arası Satışlar?')
	flightmode          = fields.Boolean(string='Uçuş Modu?')
	selectedsalesscreen = fields.Many2one('info_tsm.p_satis_ekran_tipleri',string='Seçili Satış Ekranı')
	enddaytype          = fields.Many2many('info_tsm.p_gun_sonu_tipleri',string='Gün Sonu Tipi')
	enddaytime          = fields.Float(string='Gün Sonu Zamanı')
	statereptype        = fields.Many2one('info_tsm.p_okc_durum_raporlama',string='Durum Cevap Tipi')
	staterepperiod      = fields.Integer(string='Durum Cevap Gönderme Periyodu')
	sendsalestsm        = fields.Boolean(string='Satış TSM Gönderme')
	sendsalestsmtime    = fields.Integer(string='Satış TSM Gönderme Zamanı')
	stocksyncmode       = fields.Boolean(string='Stok Eşitleme Modu')
	stocksynctime       = fields.Integer(string='Stok Eşitleme Zamanı')
	left_button         = fields.Many2one('info_tsm.p_okc_left_button' , string='Sol Buton')
	right_button        = fields.Many2one('info_tsm.p_okc_right_button', string='Sağ Buton')
	prmupdatetype       = fields.Many2one('info_tsm.p_prm_update_type', string='Prm Güncelleme Tipi')
	printzslip          = fields.Boolean(string='Z raporu Dijital olarak alınsın mı?')
	acsleep             = fields.Many2one('info_tsm.p_sleep_suspend', string='Ac Güçte Uyku Periyodu', default=_get_default_sleep)
	acssuspend          = fields.Many2one('info_tsm.p_sleep_suspend', string='Güç Modunda Uykuya Geçme Süresi', default=_get_default_suspend)
	btsleep             = fields.Many2one('info_tsm.p_sleep_suspend', string='Pilde Uyku Periyodu', default=_get_default_sleep)
	btssuspend          = fields.Many2one('info_tsm.p_sleep_suspend', string='Batarya Modunda Uykuya Geçme Süresi', default=_get_default_suspend)
	verialmaipadresi    = fields.Char( string="Veri Alma IP Adresi");
	verilmaportno       = fields.Integer( string="Veri Alma Port No");
	resetgibparams      = fields.Boolean(string="Gib Parametrelerini Resetle?")
	gibcurrencymode     = fields.Boolean(string="Döviz Tutarı Gönderilsin mi?")
	rj45boardrev        = fields.Boolean(string="Yeni RJ45 Board?")
	modem3g             = fields.Boolean(string="3G Modem Kullan?")
	keybeep             = fields.Boolean(string="Tuş Sesi Aktif")
	keybeeptype         = fields.Selection(string="Tuş Sesi Tipi", selection=bip_tipleri)
	drawerkey           = fields.Boolean(string="Çekmece Tuşu")
	drawerkeypword      = fields.Boolean(string="Şifreli Çekmece Açılışı?")
	externalcomstatus   = fields.Boolean(string="Harici Cihaz Eşleştirmesi Açık mı?")
	quicksalemode       = fields.Boolean(string="Hızlı Satış Modu Aktif")
	salestypeallowedit  = fields.Selection( selection = [(1, 'TSM + Cihaz'),
														 (2, 'Sadece TSM'),
														 (3, 'Sadece Cihaz')
														 ],
											string="Cihazdan Satış ve Tahsilat Tipleri Değiştirilebilir mi?")
	partialpaydisabled  = fields.Boolean( string='Parçalı Ödemeyi Engelle')
	seperatedproc    = fields.Boolean(string='Ayrık İşlem İzni')

	@api.one
	@api.constrains('verialmaipadresi')
	def _check_ip_address(self):

		##print self.haberlesme_tip_adi
		ch_obj = {'verialmaipadresi':'Veri Alma IP Adresi'}
		foo = check( exceptions, self )
		foo.check_ip_no( ch_obj )

	@api.multi
	def	okc_parameter_grup_set( self ):
		#once parametreyi kaydet, her basista 0 dan olmayacak
		#parametresi varsa grubu var cool!!!
		#so master decision shoud be set by group_id, individual for every lot id.
		
		
		if self._context.has_key('hide_infoteks_fields') and not self._context['hide_infoteks_fields']:
			#infoteks butonuna basilmis.
			paramset = {'appissilent':self.appissilent,
						'gprsopenmode':self.gprsopenmode,
						'foreignsales':self.foreignsales,
						'flightmode':self.flightmode,
						'sendsalestsm':self.sendsalestsm,
						'seperatedproc':self.seperatedproc,
						'statereptype':self.statereptype.id,
						'staterepperiod':self.staterepperiod,
						"sendsalestsmtime":self.sendsalestsmtime,
						"stocksyncmode":self.stocksyncmode,
						"stocksynctime":self.stocksynctime,
						"prmupdatetype":self.prmupdatetype.id,
						"verialmaipadresi":self.verialmaipadresi,
						"verilmaportno":self.verilmaportno,
						"externalcomstatus":self.externalcomstatus,
						"salestypeallowedit":self.salestypeallowedit
						}
		elif self._context.has_key('hide_bayi_fields') and not self._context['hide_bayi_fields']:
			paramset = {'canfingerlogin':self.canfingerlogin,
						'quicksalemode':self.quicksalemode,
						'partialpaydisabled':self.partialpaydisabled
						}
		elif self._context.has_key('hide_senkron_fields') and not self._context['hide_senkron_fields']:

			#print self.enddaytype


			paramset = {
						'loginrequire'	:self.loginrequire,
						'acssuspend'	:self.acssuspend.id,
						'btssuspend'	:self.btssuspend.id,
						'acsleep'		:self.acsleep.id,
						'btsleep'		:self.btsleep.id,
						'left_button'	:self.left_button.id,
						'enddaytime'	:self.enddaytime,
						'keybeep'		:self.keybeep,
						'keybeeptype'	:self.keybeeptype,
						"drawerkey"		:self.drawerkey,
						"drawerkeypword":self.drawerkeypword,
						"right_button"	:self.right_button.id,
						"printzslip"	:self.printzslip
						}
			
			if self.enddaytype:
				
				paramset['enddaytype']=[(5,),(4,self.enddaytype.ids)]
			else:
				paramset['enddaytype']=[(5,)]
		else:
			paramset = {}

		for lot_id in self.okc_list:
			paramset['name'] = lot_id.name
			if lot_id.uygulama_params and not lot_id.uygulama_params.default_param and not lot_id.uygulama_params.test_default_param:
				#onceden grubu var,
				new_params = lot_id.uygulama_params.parametre
				if self.haberlesme:
					lot_id.uygulama_params.write({'haberlesme':self.haberlesme.id})
				if not new_params.default:
					new_params.write ( paramset )
				else:
					#dont mess with the default params....
					new_params         				 = new_params.sudo().copy({'company_id':lot_id.company_id.id})
					new_params.default 				 = False
					new_params.write( paramset )
					lot_id.uygulama_params.parametre = new_params.id
			else:
				#create uyg params with default parameters.
				new_params 				= self.env['info_tsm.uygulama_parametreleri']
				paramset['company_id'] 	= lot_id.company_id.id
				new_params 				= new_params.sudo().create( paramset )
				haberlesme  			= self.haberlesme
				if not haberlesme:
					haberlesme = self.env['info_tsm.haberlesme'].search([('default','=',True)])[-1]
				new_grups  				= {'name':lot_id.name,
											'terminal_id':[(4,lot_id.id),],
											'parametre':new_params.id,
											'otomatik':True,
											'company_id':lot_id.company_id.id,
											'haberlesme':haberlesme.id
										}
				
				final_vers_ids = []
				domain = []
				types  = ['01','03','07','08']
				for t in types:
					prog     = self.env['info_tsm.programs'].search([('programtype.kod','=',t)])
					prog_ids = map(lambda x:x.id, prog )
					for p in prog_ids:
						last_1_vers = self.env['info_tsm.program_version'].search([('program_id','=',p),
							('listelere_ekle','=',True)],order='major desc, minor desc, build desc, revision desc',limit=1)
						final_vers_ids.extend( map(lambda x:(4,x.id) ,last_1_vers))
					
				
				new_grups ['prog_version'] = final_vers_ids
				
				self.env['info_tsm.parametre_grup'].create( new_grups )
				

		return {}
