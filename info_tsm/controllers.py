# -*- coding: utf-8 -*-
from openerp import http
from lxml import etree
from xml.etree import ElementTree as ET
import werkzeug.utils
from openerp import api,exceptions
import datetime
from collections import OrderedDict
from pysimplesoap.server import SoapDispatcher
import random
try:
    from urllib.request import urlopen
except ImportError:
    from urllib2 import urlopen

from gib_message_formatter import gib_formatter, gibSocket, gib_parameters
try:
    import json
except ImportError:
    import simplejson as json
import sys
import  time
import datetime
from openerp.http import request
from openerp.addons.web.controllers.main import ExcelExport
from check_rules import compute_curent_time
from parameter_models import birim_tipleri_map
class DownloadTSMLog( http.Controller):
    pass
import xlwt
import xlsxwriter
import re
from cStringIO import StringIO
from io import BytesIO
from PIL import Image
import base64
from utils import generate_new_pass
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT

class ExcelExportView(ExcelExport):
    def __getattribute__(self, name):
        if name == 'fmt':
            raise AttributeError()
        return super(ExcelExportView, self).__getattribute__(name)

    def from_data_custom(self, fields, rows,model):
        fp         = StringIO()
        workbook   = xlsxwriter.Workbook(fp)
        worksheet  = workbook.add_worksheet()
        bold       = workbook.add_format({'bold': True,'text_wrap': True})
        base_style = workbook.add_format({'text_wrap': True})
        format_2 = workbook.add_format({'bold': True,'text_wrap': True})
        format_2.set_align('center')
        format_2.set_align('vcenter')
        datetime_style = workbook.add_format({'text_wrap': True, 'num_format':'DD-MM-YYYY HH:mm:SS'})
        date_style     = workbook.add_format({'text_wrap': True, 'num_format':'DD-MM-YYYY'})
        date_style.set_align('vcenter')
        datetime_style.set_align('vcenter')
        base_style.set_align('vcenter')
        for i, fieldname in enumerate(fields):
            worksheet.write(0, i, fieldname,format_2)
            #worksheet.set_column(0,i,50)
        for row_index, row in enumerate(rows):
            #print row_index + 1
            height_setted = False
            for cell_index, cell_value in enumerate(row):
                cell_style = base_style

                if isinstance(cell_value, unicode ) and cell_value.startswith('http'):
                    try:
                        id_splitted     = cell_value.split("&id=")
                        id_             = id_splitted[1]
                        field_splitted  = id_splitted[0].split("&field=")
                        field_name      = field_splitted[1]

                        base64str = getattr (request.env[model].sudo().browse( int(id_)),field_name)
                        image_string = StringIO(base64.b64decode( base64str ))
                        image = Image.open(image_string)
                        image_data = BytesIO(base64.b64decode( base64str ))


                        width, height = image.size
                        worksheet.set_row(row_index + 1,height)
                        height_setted = True
                        worksheet.set_column(row_index + 1,0,30)

                        worksheet.insert_image(row_index +1,0,'image'+str(row_index+1),
                                   {'image_data': image_data,
                                    'positioning': 1,
                                    'x_offset': 15,
                                    'y_offset': 10} )
                    except:
                        worksheet.write(row_index + 1, cell_index, cell_value, cell_style)
                else:
                    if not height_setted:
                        worksheet.set_row(row_index + 1,64)
                    worksheet.set_column(row_index + 1,0,30)
                    if isinstance(cell_value, basestring):
                        cell_value = re.sub("\r", " ", cell_value)
                    elif isinstance(cell_value, datetime.datetime):
                        cell_style = datetime_style
                    elif isinstance(cell_value, datetime.date):
                        cell_style = date_style

                    worksheet.write(row_index + 1, cell_index, cell_value, cell_style)



        workbook.close()
        fp.seek(0)
        data = fp.read()
        fp.close()
        return data

    @http.route('/web/export/xls_view', type='http', auth='user',csrf_token=False)
    def export_xls_view(self, data, token):
        data = json.loads(data)
        model = data.get('model', [])
        columns_headers = data.get('headers', [])
        rows = data.get('rows', [])
        #print rows
        excel = self.from_data_custom(columns_headers, rows, model)
        filename = compute_curent_time() + str(self.filename(model))
        if filename.find('xls') > 0:
            filename = filename.replace('xls','xlsx')
        else:
            filename += '.xlsx'

        return request.make_response(
            excel,
            headers=[
                ('Content-Disposition', 'attachment; filename="%s"'
                 %filename),
                ('Content-Type', self.content_type)
            ],
            cookies={'fileToken': token}
        )

class TsmWebService(http.Controller):

    @http.route('/kdintegration', auth='public',csrf=False)
    def service_index_kd(self, **kw):
        
        def get_partner_by_token( kurum_kodu, token ):
            harici_firma_obj = http.request.env['info_tsm.harici_firmalar'].sudo().search([('kurum_kodu','=',kurum_kodu),('kurum_token','=',token)])
            if len(harici_firma_obj) == 1:
                harici_firma_obj = harici_firma_obj[0].musteri.id
            else:
                harici_firma_obj = None
            return harici_firma_obj

        def get_cihaz_kontrol ( partner, seri_no):
            cihaz_seri_no_kontrol = http.request.env['stock.production.lot'].sudo().search([('name','=',seri_no),('sold_to','=',partner)])
            if len( cihaz_seri_no_kontrol ) == 1:
                return cihaz_seri_no_kontrol[0].id
            return None
        
        def kdGetLicenseInfo(lisansNo):
            domain = [('lisans_no','=',lisansNo)]
            
            harici_firma_obj = http.request.env['info_tsm.harici_firmalar'].sudo().search( domain )
            if len(harici_firma_obj) == 1:
                return {'kdGetLicenseInfoCevap': {
                                                 'cevapKodu':'000',
                                                 'cevapAciklama':'Islem Basarili',
                                                 'kurumKodu':harici_firma_obj[0].kurum_kodu,
                                                 'kurumToken':harici_firma_obj[0].kurum_token
                                                 }}
            else:
                return {'kdGetLicenseInfoCevap': {
                                                 'cevapKodu':'001',
                                                 'cevapAciklama':'Geçersiz Lisans No',
                                                 }}
            
        
        def sozlesme_kontrol( partner,check_list,cihaz_id=False ):
            domain = [('firma','=', partner)]
            if cihaz_id:
                domain.appned (('name','=',cihaz_id))
            search_list = domain
            for l in check_list:
                search_list.append ( ( l,'=', True ) )
                search_list.append ( ( str(l)+'_bas', '<=', time.strftime('%Y-%m-%d') ))
                search_list.append ( ( str(l)+'_bit', '>=', time.strftime('%Y-%m-%d') ))
            #print search_list
            guncel_sozlesme = http.request.env['info_tsm.katma_deger_sozlesme'].sudo().search(search_list,order='id desc')
            sozlesme = None
            if len(guncel_sozlesme) > 0 and cihaz_id:
                sozlesme = guncel_sozlesme[0]
            elif len(guncel_sozlesme) > 0 and  not cihaz_id:
                sozlesme =  guncel_sozlesme 
            return sozlesme

        def kdSalesReportData (kurumKodu,kurumToken,OKCSeriNo,reportDateStart,reportDateEnd, reportZNo ):
            #print kurumKodu,kurumToken
            partner = get_partner_by_token (kurumKodu,kurumToken)
            if not partner:
                return {'kdSalesReportDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'001',
                                                 'cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'
                                                 }}
            cihaz_id = get_cihaz_kontrol( partner, OKCSeriNo )
            if not cihaz_id:
                return {'kdSalesReportDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'004',
                                                 'cevapAciklama':'Tanimsiz YNOKC'
                                                 }}
            sozlesme = sozlesme_kontrol( partner, ['z_raporu','fis_raporu'],cihaz_id)
            if not sozlesme:
                return {'kdSalesReportDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'003',
                                                 'cevapAciklama':'Tanimsiz Harici Sistem'
                                                 }}


            search_list = [('name','=', cihaz_id)]
            if reportDateStart:
                reportDateStart = datetime.datetime.strptime(reportDateStart, '%Y%m%d').strftime('%Y-%m-%d')
                search_list.append( ('rap_bas','>=', reportDateStart ))
            if reportDateEnd  :
                reportDateEnd   = datetime.datetime.strptime(reportDateEnd, '%Y%m%d').strftime('%Y-%m-%d')
                search_list.append( ('rap_bas','<=', reportDateEnd ))
            if reportZNo:
                search_list.append (('z_rapor_no','=',reportZNo))

            satis_raporlari   = http.request.env['info_tsm.satis_raporu'].sudo().search( search_list )
            satis_raporu_list = []

            for s in satis_raporlari:
                satis = {}

                satis['salesLine'] = dict(prdName       = s.urun_aciklama,
                     prdUnitType    = birim_tipleri_map[ s.birim_tipi ].decode('utf-8'),
                     prdSaleAmount  = ('%f' % s.miktar).rstrip('0').rstrip('.'),
                     prdVatRate     = s.kdv_dilimi,
                     salesTotalVAT  = ('%f' % s.total_kdv).rstrip('0').rstrip('.'),
                     salesTotalCost = ('%f' % s.toplam_tutar).rstrip('0').rstrip('.'),
                     salesReportZNo = s.z_rapor_no
                )
                satis_raporu_list.append ( satis )

            satis_raporlari   = http.request.env['info_tsm.satis_raporu_eslesmeyen'].sudo().search( search_list )

            for s in satis_raporlari:
                satis = {}

                satis['salesLine'] = dict(prdName       = s.urun_aciklama,
                     prdUnitType    = birim_tipleri_map[ s.birim_tipi ].decode('utf-8'),
                     prdSaleAmount  = ('%f' % s.miktar).rstrip('0').rstrip('.'),
                     prdVatRate     = s.kdv_dilimi ,
                     salesTotalVAT  = ('%f' % s.total_kdv).rstrip('0').rstrip('.'),
                     salesTotalCost = ('%f' % s.toplam_tutar).rstrip('0').rstrip('.'),
                     salesReportZNo = s.z_rapor_no
                )
                satis_raporu_list.append ( satis )

            return {'kdSalesReportDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'000',
                                                 'cevapAciklama':'islem Basarili',
                                                 'reportData': satis_raporu_list }}
        
        def kdSalesParamGrupData (kurumKodu,kurumToken ):
            #print kurumKodu,kurumToken
            partner = get_partner_by_token (kurumKodu,kurumToken)
            print partner
            if not partner:
                return {'kdSalesReportDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'001',
                                                 'cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'
                                                 }}
            cihaz_ids   = http.request.env['stock.production.lot'].sudo().search([('sold_to','=',partner),('lot_state','=','sold')])
            param_grups = list(set(map(lambda x:x.urun_params,cihaz_ids)))
            
            uni_kdv     = []
            uni_gr      = []
            uni_pr      = []
            kdv_map     = {1:'0',2:'100',3:'800',4:'1800'} 
            for p in param_grups:
                print p
                for k in p.ozel_kdv:
                    kdv_d = {}
                    kdv_d['vatLine'] = dict(name=k.ozel_kdv_rel.name,
                                              vat =kdv_map [k.ozel_kdv_rel.kdv_orani])
                    
                    if kdv_d not in uni_kdv:
                        uni_kdv.append( kdv_d )
                for gr in p.ozel_urun_gruplari:
                    
                    gr_d ={}
                    gr_d['groupLine'] = dict(name = gr.ozel_urun_rel.name,
                                             vat  = kdv_map [gr.ozel_kdv_rel.kdv_orani] )
                        
                    if gr_d not in uni_gr:
                        uni_gr.append( gr_d )
                for u in p.urun:
                    
                    ur_d ={}
                    ur_d['productLine'] = dict( name    = u.urun_rel.name,
                                             #vat     = kdv_map [u.urun_grup_id.ozel_kdv_rel.kdv_orani],
                                             amount  = u.tutar,
                                             barcode = u.barkod_no)
                        
                    if ur_d not in uni_pr:
                        uni_pr.append( ur_d )

            return {'kdSalesReportDataCevap': {  'cevapKodu':'000',
                                                 'cevapAciklama':'islem Basarili',
                                                 'vatData': uni_kdv,
                                                 'groupData': uni_gr,
                                                 'productData' : uni_pr
                                            }
                    }
        
        def kdContractInfo (kurumKodu,kurumToken,OKCSeriNo ):
            #print kurumKodu,kurumToken
            partner = get_partner_by_token (kurumKodu,kurumToken)
            if not partner:
                return {'kdContractInfoCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'001',
                                                 'cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'
                                                 }}
            cihaz_id = get_cihaz_kontrol( partner, OKCSeriNo )
            if not cihaz_id:
                return {'kdContractInfoCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'004',
                                                 'cevapAciklama':'Tanimsiz YNOKC'
                                                 }}

            sozlesme = sozlesme_kontrol( partner, [],cihaz_id)
            if not sozlesme:
                return {'kdSalesReportDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'003',
                                                 'cevapAciklama':'Tanimsiz Harici Sistem'
                                                 }}
            z_raporu_bas = ''
            if sozlesme.z_raporu_bas:
                z_raporu_bas = datetime.datetime.strptime(sozlesme.z_raporu_bas, '%Y-%m-%d').strftime('%Y%m%d')

            z_raporu_bit = ''
            if sozlesme.z_raporu_bit:
                z_raporu_bit = datetime.datetime.strptime(sozlesme.z_raporu_bit, '%Y-%m-%d').strftime('%Y%m%d')

            fis_raporu_bas = ''
            if sozlesme.fis_raporu_bas:
                fis_raporu_bas = datetime.datetime.strptime(sozlesme.fis_raporu_bas, '%Y-%m-%d').strftime('%Y%m%d')

            fis_raporu_bit = ''
            if sozlesme.fis_raporu_bit:
                fis_raporu_bit = datetime.datetime.strptime(sozlesme.fis_raporu_bit, '%Y-%m-%d').strftime('%Y%m%d')

            olay_raporu_bas = ''
            if sozlesme.olay_raporu_bas:
                olay_raporu_bas = datetime.datetime.strptime(sozlesme.olay_raporu_bas, '%Y-%m-%d').strftime('%Y%m%d')

            olay_raporu_bit = ''
            if sozlesme.olay_raporu_bit:
                olay_raporu_bit = datetime.datetime.strptime(sozlesme.olay_raporu_bit, '%Y-%m-%d').strftime('%Y%m%d')

            istat_raporu_bas = ''
            if sozlesme.istat_raporu_bas:
                istat_raporu_bas = datetime.datetime.strptime(sozlesme.istat_raporu_bas, '%Y-%m-%d').strftime('%Y%m%d')

            istat_raporu_bit = ''
            if sozlesme.istat_raporu_bit:
                istat_raporu_bit = datetime.datetime.strptime(sozlesme.istat_raporu_bit, '%Y-%m-%d').strftime('%Y%m%d')

            sozlesme_dict = dict(zReportActive = sozlesme.z_raporu,

                                 zReportStart  = z_raporu_bas,
                                 zReportEnd    = z_raporu_bit,

                                 salesActive = sozlesme.fis_raporu,
                                 salesStart  = fis_raporu_bas,
                                 salesEnd    = fis_raporu_bit,

                                 eventsActive = sozlesme.olay_raporu,
                                 eventsStart  = olay_raporu_bas,
                                 eventsEnd    = olay_raporu_bit,

                                 statisticActive = sozlesme.istat_raporu,
                                 statisticStart  = istat_raporu_bas,
                                 statisticEnd    = istat_raporu_bit,
                                 )
            return  {'kdContractInfoCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'000',
                                                 'cevapAciklama':'Islem Basarili',
                                                 'contractInfo' : sozlesme_dict
                                                 }}
        
        def kdAcquirerInfo(kurumKodu,kurumToken):
            partner = get_partner_by_token (kurumKodu,kurumToken)
            if not partner:
                return {'kdAcquirerInfoCevap': {'cevapKodu':'001',
                                                 'cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'
                                                 }}
            acquirer_list = http.request.env['info_extensions.acquirer'].sudo().search([])
            a_list = []
            for a in acquirer_list:
                acquirer_dict = {}
                acquirer_dict['acquirerInfo'] = dict(bkmId = a.bkm_id,
                                                     name   = a.name)
                
                a_list.append( acquirer_dict )
            return {'kdAcquirerInfoCevap': {'cevapKodu':'000',
                                            'cevapAciklama':'islem Basarili',
                                            'acquirerData': a_list }}
            
        def kdSalesReceiptData ( kurumKodu,kurumToken,OKCSeriNo,reportDateStart,reportDateEnd, reportZNo ):
            #print kurumKodu,kurumToken
            partner = get_partner_by_token (kurumKodu,kurumToken)
            if not partner:
                return {'kdSalesReceiptDataCevap':{'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'001',
                                                 'cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'
                                                 }}
            
            cihaz_id = get_cihaz_kontrol( partner, OKCSeriNo )
            if not cihaz_id:
                return {'kdSalesReceiptDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'004',
                                                 'cevapAciklama':'Tanimsiz YNOKC'
                                                 }}
            
            sozlesme = sozlesme_kontrol( partner, ['fis_raporu'],cihaz_id)
            if not sozlesme:
                return {'kdSalesReceiptDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'003',
                                                 'cevapAciklama':'Tanimsiz Harici Sistem'
                                                 }}


            search_list = [('terminal_serial','=', OKCSeriNo),('status','!=','iptal')]
            if reportDateStart:
                reportDateStart = datetime.datetime.strptime(reportDateStart, '%Y%m%d').strftime('%Y-%m-%d')
                search_list.append( ('islem_tarihi','>=', reportDateStart ))
            if reportDateEnd  :
                reportDateEnd   = datetime.datetime.strptime(reportDateEnd, '%Y%m%d').strftime('%Y-%m-%d')
                search_list.append( ('islem_tarihi','<=', reportDateEnd ))
            if reportZNo:
                search_list.append (('z_sayac_no','=',reportZNo))

            fis_raporlari   = http.request.env['info_tsm.fis_data'].sudo().search( search_list )
            fis_raporu_list = []
            for s in fis_raporlari:
                #print s.id
                fis_dict = {}
                kalems = []
                banks  = []
                fis_kalems              =  http.request.env['info_tsm.fis_kalem_data'].sudo().search([('fis_id','=',s.id)] )
                
                for f in fis_kalems:
                    kalem_dict = {}
                    kalem_dict['salesLine'] = dict( salesLineName      = f.isim,
                                                    salesLineAmount    = ('%f' % f.miktar).rstrip('0').rstrip('.'),
                                                    salesLineIncAmount = ('%f' % f.artirim_tutari).rstrip('0').rstrip('.'),
                                                    salesLineDecAmount = ('%f' % f.indirim_tutari).rstrip('0').rstrip('.'),
                                                    salesLineVAT       = ('%f' % f.kdv_orani).rstrip('0').rstrip('.'),
                                                    salesLineTotCost   = ('%f' % f.tutar).rstrip('0').rstrip('.'),
                                                )
                    kalems.append  ( kalem_dict )
                
                fis_banks              =  http.request.env['info_tsm.fis_bank_info'].sudo().search([('fis_id','=',s.id)] )
                
                for f in fis_banks:
                    kalem_dict = {}
                    
                    kalem_dict['bankLine'] = dict(  acquirer_id     = f.acquirer_id,
                                                    terminal_no     = f.terminal_no,
                                                    account_no      = f.account_no,
                                                    receipt_no      = f.receipt_no,
                                                    kk_number       = f.kk_number,
                                                    batch_no        = f.batch_no,
                                                    onay_no         = f.onay_no,
                                                    amount          = f.amount
                                                )
                    banks.append  ( kalem_dict )
                    
                islem_saati  = s.islem_saati
                islem_tarihi = datetime.datetime.strptime(islem_saati, '%Y-%m-%d %H:%M:%S').strftime('%Y%m%d%H%M%S')
                fatura_no    = s.fatura_no
                fatura_ratihi = s.fatura_tarihi
                
                fis_dict['receiptLine'] = dict(
                     
                     salesTime       = islem_tarihi,
                     salesType          = s.fis_tipi,
                     salesFaturaNo      = fatura_no,
                     salesFaturaTarihi  = fatura_ratihi,
                     salesReceiptNo     = s.fis_sira_no,
                     salesZNo           = s.z_sayac_no,
                     salesEKUNo         = s.eku_no ,
                     salesReceiptType   = s.status,
                     salesTotalVAT      = ('%f' % s.toplam_kdv_tutari).rstrip('0').rstrip('.'),
                     salesTotalCost     = ('%f' % s.toplam_tutar).rstrip('0').rstrip('.'),
                     salesCashAmount    = ('%f' % s.nakit_odenen_tutar).rstrip('0').rstrip('.'),
                     salesKKAmount      = ('%f' % s.kredi_karti_odenen_tutar).rstrip('0').rstrip('.'),
                     salesOtherAmount   = ('%f' % s.diger_odenen_tutar).rstrip('0').rstrip('.'),
                     salesCurrAmount    = ('%f' % s.doviz_odenen_tutar).rstrip('0').rstrip('.'),
                     salesLines         = kalems,
                     bankLines          = banks
                )

                fis_raporu_list.append ( fis_dict )


            return {'kdSalesReceiptDataCevap': { 'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'000',
                                                 'cevapAciklama':'islem Basarili',
                                                 'salesData': fis_raporu_list }}

        def kdZReportData ( kurumKodu,kurumToken,OKCSeriNo,reportDateStart,reportDateEnd, reportZNo ):
            #print kurumKodu,kurumToken
            partner = get_partner_by_token (kurumKodu,kurumToken)
            if not partner:
                return {'kdZReportDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'001',
                                                 'cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'
                                                 }}
            cihaz_id = get_cihaz_kontrol( partner, OKCSeriNo )
            if not cihaz_id:
                return {'kdZReportDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'004',
                                                 'cevapAciklama':'Tanimsiz YNOKC'
                                                 }}
            sozlesme = sozlesme_kontrol( partner, ['z_raporu'],cihaz_id)
            if not sozlesme:
                return {'kdZReportDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'003',
                                                 'cevapAciklama':'Tanimsiz Harici Sistem'
                                                 }}


            search_list = [('terminal_serial','=', OKCSeriNo)]
            if reportDateStart:
                reportDateStart = datetime.datetime.strptime(reportDateStart, '%Y%m%d').strftime('%Y-%m-%d')
                search_list.append( ('islem_tarihi','>=', reportDateStart ))
            if reportDateEnd  :
                reportDateEnd   = datetime.datetime.strptime(reportDateEnd, '%Y%m%d').strftime('%Y-%m-%d')
                search_list.append( ('islem_tarihi','<=', reportDateEnd ))
            if reportZNo:
                search_list.append (('z_sayac_no','=',reportZNo))

            z_raporlari   = http.request.env['info_tsm.z_raporu_data'].sudo().search( search_list )

            z_raporu_list = []

            for z in z_raporlari:

                islem_saati  = z.islem_saati
                islem_tarihi = datetime.datetime.strptime(islem_saati, '%Y-%m-%d %H:%M:%S').strftime('%Y%m%d%H%M%S')
                z_dict = {}
                z_kisim_tutars = dict(  kisim_toplam_tutari1 = z.kisim_toplam_tutari1,
                                        kisim_toplam_tutari2 = z.kisim_toplam_tutari2,
                                        kisim_toplam_tutari3 = z.kisim_toplam_tutari3,
                                        kisim_toplam_tutari4 = z.kisim_toplam_tutari4,
                                        kisim_toplam_tutari5 = z.kisim_toplam_tutari5,
                                        kisim_toplam_tutari6 = z.kisim_toplam_tutari6,
                                        kisim_toplam_tutari7 = z.kisim_toplam_tutari7,
                                        kisim_toplam_tutari8 = z.kisim_toplam_tutari8,
                                        kisim_toplam_tutari9 = z.kisim_toplam_tutari9,
                                        kisim_toplam_tutari10 = z.kisim_toplam_tutari10,
                                        kisim_toplam_tutari11 = z.kisim_toplam_tutari11,
                                        kisim_toplam_tutari12 = z.kisim_toplam_tutari12
                                      )
                z_kisim_names  = dict(  kisim_adi1 = z.kisim_adi1,
                                        kisim_adi2 = z.kisim_adi2,
                                        kisim_adi3 = z.kisim_adi3,
                                        kisim_adi4 = z.kisim_adi4,
                                        kisim_adi5 = z.kisim_adi5,
                                        kisim_adi6 = z.kisim_adi6,
                                        kisim_adi7 = z.kisim_adi7,
                                        kisim_adi8 = z.kisim_adi8,
                                        kisim_adi9 = z.kisim_adi9,
                                        kisim_adi10 = z.kisim_adi10,
                                        kisim_adi11 = z.kisim_adi11,
                                        kisim_adi12 = z.kisim_adi12
                                      )
                z_kisim_adets  = dict(  kisim_toplam_adedi1 = z.kisim_toplam_adedi1,
                                        kisim_toplam_adedi2 = z.kisim_toplam_adedi2,
                                        kisim_toplam_adedi3 = z.kisim_toplam_adedi3,
                                        kisim_toplam_adedi4 = z.kisim_toplam_adedi4,
                                        kisim_toplam_adedi5 = z.kisim_toplam_adedi5,
                                        kisim_toplam_adedi6 = z.kisim_toplam_adedi6,
                                        kisim_toplam_adedi7 = z.kisim_toplam_adedi7,
                                        kisim_toplam_adedi8 = z.kisim_toplam_adedi8,
                                        kisim_toplam_adedi9 = z.kisim_toplam_adedi9,
                                        kisim_toplam_adedi10 = z.kisim_toplam_adedi10,
                                        kisim_toplam_adedi11 = z.kisim_toplam_adedi11,
                                        kisim_toplam_adedi12 = z.kisim_toplam_adedi12
                                      )
                z_cur_tutar    = dict(  doviz_toplam_tutar1 = z.doviz_toplam_tutar1,
                                    doviz_toplam_tutar2 = z.doviz_toplam_tutar2,
                                    doviz_toplam_tutar3 = z.doviz_toplam_tutar3,
                                    doviz_toplam_tutar4 = z.doviz_toplam_tutar4,
                                    doviz_toplam_tutar5 = z.doviz_toplam_tutar5,
                                    doviz_toplam_tutar6 = z.doviz_toplam_tutar6,

                                      )
                z_cur_names    = dict(  doviz_adi1 = z.doviz_adi1,
                                        doviz_adi2 = z.doviz_adi2,
                                        doviz_adi3 = z.doviz_adi3,
                                        doviz_adi4 = z.doviz_adi4,
                                        doviz_adi5 = z.doviz_adi5,
                                        doviz_adi6 = z.doviz_adi6,

                                      )
                z_cur_cevrim   = dict(  doviz_tl_karsiligi1 = z.doviz_tl_karsiligi1,
                                        doviz_tl_karsiligi2 = z.doviz_tl_karsiligi2,
                                        doviz_tl_karsiligi3 = z.doviz_tl_karsiligi3,
                                        doviz_tl_karsiligi4 = z.doviz_tl_karsiligi4,
                                        doviz_tl_karsiligi5 = z.doviz_tl_karsiligi5,
                                        doviz_tl_karsiligi6 = z.doviz_tl_karsiligi6,

                                      )
                z_ver_tutar    = dict(  vergi_toplam_tutari1 = z.vergi_toplam_tutari1,
                                    vergi_toplam_tutari2 = z.vergi_toplam_tutari2,
                                    vergi_toplam_tutari3 = z.vergi_toplam_tutari3,
                                    vergi_toplam_tutari4 = z.vergi_toplam_tutari4,
                                    vergi_toplam_tutari5 = z.vergi_toplam_tutari5,
                                    vergi_toplam_tutari6 = z.vergi_toplam_tutari6,
                                    vergi_toplam_tutari7 = z.vergi_toplam_tutari7,
                                    vergi_toplam_tutari8 = z.vergi_toplam_tutari8,

                                      )
                z_ver_names    = dict(  vergi_orani1 = z.vergi_orani1,
                                        vergi_orani2 = z.vergi_orani2,
                                        vergi_orani3 = z.vergi_orani3,
                                        vergi_orani4 = z.vergi_orani4,
                                        vergi_orani5 = z.vergi_orani5,
                                        vergi_orani6 = z.vergi_orani6,
                                        vergi_orani7 = z.vergi_orani7,
                                        vergi_orani8 = z.vergi_orani8,

                                      )
                z_ver_kdv   = dict(     vergi_toplam_kdv1 = z.vergi_toplam_kdv1,
                                        vergi_toplam_kdv2 = z.vergi_toplam_kdv2,
                                        vergi_toplam_kdv3 = z.vergi_toplam_kdv3,
                                        vergi_toplam_kdv4 = z.vergi_toplam_kdv4,
                                        vergi_toplam_kdv5 = z.vergi_toplam_kdv5,
                                        vergi_toplam_kdv6 = z.vergi_toplam_kdv6,
                                        vergi_toplam_kdv7 = z.vergi_toplam_kdv7,
                                        vergi_toplam_kdv8 = z.vergi_toplam_kdv8,

                                      )

                z_dict['zReportLine'] = dict(zReportTime                = islem_tarihi,
                                             zReportReceiptNo           = z.fis_sira_no,
                                             zReportNo                  = z.z_sayac_no,
                                             zReportEKUNo               = z.eku_no,
                                             zReportIncNum              = z.artirim_toplam_adedi,
                                             zReportDecNum              = z.indirim_toplam_adedi,
                                             zReportFixNum              = z.hata_duzelt_toplam_adedi,
                                             zReportFiscalRecNum        = z.mali_fis_adedi,
                                             zReportUnfiscalRecNum      = z.mali_olmayan_fis_adedi,
                                             zReportCustRecNum          = z.musteri_fis_adedi,
                                             zReportCancelRecNum        = z.satis_fisi_iptal_adedi,
                                             zReportBillRecNum          = z.faturali_satis_adedi,
                                             zReportRestTrnNum          = z.yemek_islem_sayisi,
                                             zReportParkTrnNum          = z.otopark_giris_islem_sayisi,
                                             zReportKisimCost           = z_kisim_tutars,
                                             zReportKisimName           = z_kisim_names,
                                             zReportKisimNum            = z_kisim_adets,
                                             zReportIncCost             = z.artirim_toplam_tutari,
                                             zReportDecCost             = z.indirim_toplam_tutari,
                                             zReportCashAmount          = z.nakit_toplam_tutar,
                                             zReportOtherAmount         = z.diger_odeme_toplam_tutar,
                                             zReportKKAmount            = z.kredi_karti_toplam_tutar,
                                             zReportCurrAmount          = z_cur_tutar,
                                             zReportCurrName            = z_cur_names,
                                             zReportCurrToTL            = z_cur_cevrim,
                                             zReportCancelAmount        = z.satis_iptal_toplam_tutar,
                                             zReportFixAmount           = z.hata_duzelt_toplam_tutar,
                                             zReportBillAmount          = z.faturali_satis_toplam_tutar,
                                             zReportRestAmount          = z.yemek_islem_toplam_tutari,
                                             zReportTechNum             = z.servis_mudehale_sayisi,
                                             zReportVATAmount           = z_ver_tutar,
                                             zReportVATRate             = z_ver_names,
                                             zReportVATCost             = z_ver_kdv,
                                             zReportDailyTotAmount      = z.gunluk_toplam_tutar,
                                             zReportDailyVATAmount      = z.gunluk_toplam_kdv,
                                             zReportAccTotAmount        = z.kumule_toplam_tutar,
                                             zReportAccVATAmount        = z.kumule_toplam_kdv
                                             )

                z_raporu_list.append( z_dict )
            #print len(z_raporu_list)
            return {'kdZReportDataCevap': { 'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'000',
                                                 'cevapAciklama':'islem Basarili',
                                                 'zRepData': z_raporu_list }}

        def kdEventsData (kurumKodu,kurumToken,OKCSeriNo,reportDateStart,reportDateEnd ):
            #print kurumKodu,kurumToken
            partner = get_partner_by_token (kurumKodu,kurumToken)
            if not partner:
                return {'kdEventsDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'001',
                                                 'cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'
                                                 }}
            cihaz_id = get_cihaz_kontrol( partner, OKCSeriNo )
            if not cihaz_id:
                return {'kdEventsDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'004',
                                                 'cevapAciklama':'Tanimsiz YNOKC'
                                                 }}
            sozlesme = sozlesme_kontrol( partner, ['olay_raporu'],cihaz_id)
            if not sozlesme:
                return {'kdEventsDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'003',
                                                 'cevapAciklama':'Tanimsiz Harici Sistem'
                                                 }}


            search_list = [('terminal_serial','=', OKCSeriNo)]
            if reportDateStart:
                reportDateStart = datetime.datetime.strptime(reportDateStart, '%Y%m%d').strftime('%Y-%m-%d')
                search_list.append( ('olay_tarihi','>=', reportDateStart ))
            if reportDateEnd  :
                reportDateEnd   = datetime.datetime.strptime(reportDateEnd, '%Y%m%d').strftime('%Y-%m-%d')
                search_list.append( ('olay_tarihi','<=', reportDateEnd ))

            olay_raporlari   = http.request.env['info_tsm.olay_kaydi_data'].sudo().search( search_list )

            olay_raporu_list = []

            for o in olay_raporlari:
                islem_saati  = o.olay_saati
                islem_tarihi = datetime.datetime.strptime(islem_saati, '%Y-%m-%d %H:%M:%S').strftime('%Y%m%d%H%M%S')
                olay_dict = {}
                olay_dict['eventsLine'] = dict(
                    eventsSource   = o.olay_kaynagi,
                    eventsCrtLevel = o.olay_kritiklik_seviyesi,
                    eventsType     = o.olay_tipi,
                    eventsTime     = islem_tarihi,
                    eventsUserName = o.kullanici_adi,
                    eventsTermState= o.yazar_kasa_konumu,
                )

                olay_raporu_list.append( olay_dict )

            return {'kdEventsDataCevap': { 'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'000',
                                                 'cevapAciklama':'islem Basarili',
                                                 'eventsData': olay_raporu_list }}
        def kdStatisticsData (kurumKodu,kurumToken,OKCSeriNo,reportDateStart,reportDateEnd ):
            #print kurumKodu,kurumToken
            partner = get_partner_by_token (kurumKodu,kurumToken)
            if not partner:
                return {'kdStatisticsDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'001',
                                                 'cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'
                                                 }}
            cihaz_id = get_cihaz_kontrol( partner, OKCSeriNo )
            if not cihaz_id:
                return {'kdStatisticsDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'004',
                                                 'cevapAciklama':'Tanimsiz YNOKC'
                                                 }}
            sozlesme = sozlesme_kontrol( partner, ['istat_raporu'],cihaz_id)
            if not sozlesme:
                return {'kdStatisticsDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'003',
                                                 'cevapAciklama':'Tanimsiz Harici Sistem'
                                                 }}


            search_list = [('terminal_serial','=', OKCSeriNo)]
            if reportDateStart:
                reportDateStart = datetime.datetime.strptime(reportDateStart, '%Y%m%d').strftime('%Y-%m-%d')
                search_list.append( ('save_date','>=', reportDateStart ))
            if reportDateEnd  :
                reportDateEnd   = datetime.datetime.strptime(reportDateEnd, '%Y%m%d').strftime('%Y-%m-%d')
                search_list.append( ('save_date','<=', reportDateEnd ))

            istat_raporlari   = http.request.env['info_tsm.istatistik_data'].sudo().search( search_list )

            istat_raporu_list = []

            for i in istat_raporlari:

                stat_dict = {}
                stat_dict['statLine'] = dict(
                    statRespNum  = i.toplam_yanit_alan_islem,
                    statFailNum  = i.toplam_olumsuz_yanit_alan_islem,
                    statEthConn  = i.toplam_ethernet_baglanti,
                    statDialConn = i.toplam_dialup_baglanti,
                    statGPRSConn = i.toplam_gprs_baglanti,
                    statGSMConn  = i.toplam_gsm_baglanti,
                    statEthFail  = i.toplam_ethernet_basarisiz_baglanti,
                    statDialFail = i.toplam_dialup_basarisiz_baglanti,
                    statGPRSFail = i.toplam_gprs_basarisiz_baglanti,
                    statGSMFail  = i.toplam_gsm_basarisiz_baglanti,

                )

                istat_raporu_list.append( stat_dict )

            return {'kdStatisticsDataCevap': { 'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'000',
                                                 'cevapAciklama':'islem Basarili',
                                                 'statData': istat_raporu_list }}
        
        def kdSalesReceiptAllData( kurumKodu,kurumToken,reportDateStart,reportDateEnd ):
            #print kurumKodu,kurumToken
            partner = get_partner_by_token (kurumKodu,kurumToken)
            if not partner:
                return {'kdSalesReceiptDataAllCevap':{
                                                 'cevapKodu':'001',
                                                 'cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'
                                                 }}
            
            sozlesme = sozlesme_kontrol( partner, ['fis_raporu'],)
            if not sozlesme:
                return {'kdSalesReceiptDataAllCevap': {
                                                 'cevapKodu':'003',
                                                 'cevapAciklama':'Tanimsiz Harici Sistem'
                                                 }}

            search_list = [('terminal_serial','in',map(lambda x:x.name.name,sozlesme) ),('status','!=','iptal'),('logo_etc','=',False)]
            if reportDateStart:
                reportDateStart = datetime.datetime.strptime(reportDateStart, '%Y%m%d').strftime('%Y-%m-%d')
                search_list.append( ('islem_tarihi','>=', reportDateStart ))
            if reportDateEnd  :
                reportDateEnd   = datetime.datetime.strptime(reportDateEnd, '%Y%m%d').strftime('%Y-%m-%d')
                search_list.append( ('islem_tarihi','<=', reportDateEnd ))
            

            fis_raporlari   = http.request.env['info_tsm.fis_data'].sudo().search( search_list )
            fis_raporu_list = []
            for s in fis_raporlari:
                #print s.id
                fis_dict = {}
                kalems = []
                banks  = []
                fis_kalems              =  http.request.env['info_tsm.fis_kalem_data'].sudo().search([('fis_id','=',s.id)] )
                
                for f in fis_kalems:
                    kalem_dict = {}
                    kalem_dict['salesLine'] = dict( salesLineName      = f.isim,
                                                    salesLineAmount    = ('%f' % f.miktar).rstrip('0').rstrip('.'),
                                                    salesLineIncAmount = ('%f' % f.artirim_tutari).rstrip('0').rstrip('.'),
                                                    salesLineDecAmount = ('%f' % f.indirim_tutari).rstrip('0').rstrip('.'),
                                                    salesLineVAT       = ('%f' % f.kdv_orani).rstrip('0').rstrip('.'),
                                                    salesLineTotCost   = ('%f' % f.tutar).rstrip('0').rstrip('.'),
                                                )
                    kalems.append  ( kalem_dict )
                
                fis_banks              =  http.request.env['info_tsm.fis_bank_info'].sudo().search([('fis_id','=',s.id)] )
                
                for f in fis_banks:
                    kalem_dict = {}
                    
                    kalem_dict['bankLine'] = dict(  acquirer_id     = f.acquirer_id,
                                                    terminal_no     = f.terminal_no,
                                                    account_no      = f.account_no,
                                                    receipt_no      = f.receipt_no,
                                                    kk_number       = f.kk_number,
                                                    batch_no        = f.batch_no,
                                                    onay_no         = f.onay_no,
                                                    amount          = f.amount
                                                )
                    banks.append  ( kalem_dict )
                    
                islem_saati  = s.islem_saati
                islem_tarihi = datetime.datetime.strptime(islem_saati, '%Y-%m-%d %H:%M:%S').strftime('%Y%m%d%H%M%S')
                fatura_no    = s.fatura_no
                fatura_ratihi = s.fatura_tarihi
                seri_no       = s.terminal_serial
                
                fis_dict['receiptLine'] = dict(
                     salesServerId      = s.id,
                     salesTime          = islem_tarihi,
                     salesType          = s.fis_tipi,
                     salesFaturaNo      = fatura_no,
                     salesSerialNo      = seri_no,
                     salesFaturaTarihi  = fatura_ratihi,
                     salesReceiptNo     = s.fis_sira_no,
                     salesZNo           = s.z_sayac_no,
                     salesEKUNo         = s.eku_no ,
                     salesReceiptType   = s.status,
                     salesTotalVAT      = ('%f' % s.toplam_kdv_tutari).rstrip('0').rstrip('.'),
                     salesTotalCost     = ('%f' % s.toplam_tutar).rstrip('0').rstrip('.'),
                     salesCashAmount    = ('%f' % s.nakit_odenen_tutar).rstrip('0').rstrip('.'),
                     salesKKAmount      = ('%f' % s.kredi_karti_odenen_tutar).rstrip('0').rstrip('.'),
                     salesOtherAmount   = ('%f' % s.diger_odenen_tutar).rstrip('0').rstrip('.'),
                     salesCurrAmount    = ('%f' % s.doviz_odenen_tutar).rstrip('0').rstrip('.'),
                     salesLines         = kalems,
                     bankLines          = banks
                )

                fis_raporu_list.append ( fis_dict )


            return {'kdSalesReceiptDataAllCevap': { 
                                                 'cevapKodu':'000',
                                                 'cevapAciklama':'islem Basarili',
                                                 'salesData': fis_raporu_list }}
        
        def kdUpdateReceiptLogoStatus( kurumKodu,kurumToken,serverId ):
            #print kurumKodu,kurumToken
            
            partner = get_partner_by_token (kurumKodu,kurumToken)
            if not partner:
                return {'kdUpdateReceiptLogoStatusCevap':{
                                                 'cevapKodu':'001',
                                                 'cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'
                                                 }}
           
            serials = map(lambda x:int(x),serverId.split(','))
            try:
                http.request.env["info_tsm.fis_data"].sudo().browse( serials ).write({'logo_etc':True})
            except exceptions.MissingError:
                return {'kdUpdateReceiptLogoStatusCevap':{
                                                 'cevapKodu':'001',
                                                 'cevapAciklama':'Listede Gecersiz veya Silinmiş Server Id '
                                                 }}
            return {'kdUpdateReceiptLogoStatusCevap':{
                                                 'cevapKodu':'000',
                                                 'cevapAciklama':'islem Basarili'
                                                 }}
        def kdgetSozlesmeStatus( kurumKodu,kurumToken ):
            
            partner = get_partner_by_token (kurumKodu,kurumToken)
            if not partner:
                return {'kdgetSozlesmeStatusCevap':{
                                                 'cevapKodu':'001',
                                                 'cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'
                                                 }}
            
            soz          = sozlesme_kontrol(partner,['fis_raporu'])
            if not soz:
                soz = []
            registered   = map(lambda x:{'terminalSerial':{'name':x.name.name,'sozlesmeBas':x.fis_raporu_bas,'sozlesmeBit':x.fis_raporu_bit}},soz)
            
            unregistered = map (lambda x:{'terminalSerial':{'name':x.name}},http.request.env["stock.production.lot"].sudo().search([('sold_to','=',partner),
                                                                                   ('lot_state','=','sold'),
                                                                                   ('name','not in',map(lambda x:x.name.name,soz))]))
        
            
            
            return {'kdgetSozlesmeStatusCevap':{
                                                 'cevapKodu':'000',
                                                 'cevapAciklama':'islem Basarili',
                                                 'registeredOKC':registered,
                                                 'unregisteredOKC':unregistered
                                                 }}  
            
        param = http.request.env["ir.config_parameter"]

        dispatcher = SoapDispatcher(
            'soap_service',
            location = param.get_param( "bkm.ws_location", default='http://localhost:8069') + '/kdintegration/',
            action = param.get_param( "bkm.ws_location", default='http://localhost:8069') + '/kdintegration/',
            #namespace = "http://esintegration.infoteks.com.tr/",
            #prefix='tns',
            ns = None)
        # register func
        dispatcher.register_function('kdgetSozlesmeStatus', kdgetSozlesmeStatus,
            returns=OrderedDict([('kdgetSozlesmeStatusCevap', OrderedDict(
                                                [('cevapKodu', str), ('cevapAciklama', str),
                                                ('registeredOKC',OrderedDict([('name',str),('sozlesmeBas',str),('sozlesmeBit',str)])),
                                                ('unregisteredOKC',OrderedDict([('name',str),]))]
                                                )
                                )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str)])
        )
        dispatcher.register_function('kdUpdateReceiptLogoStatus', kdUpdateReceiptLogoStatus,
            returns=OrderedDict([('kdUpdateReceiptLogoStatusCevap', OrderedDict(
                                                [('cevapKodu', str), ('cevapAciklama', str)])
                                    )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str),('serverId',str)])
        )
        dispatcher.register_function('kdGetLicenseInfo', kdGetLicenseInfo,
            returns=OrderedDict([('kdGetLicenseInfoCevap', OrderedDict(
                                                [('cevapKodu', str), ('cevapAciklama', str),('kurumKodu', str), ('kurumToken', str)])
                                    )]),
            args=OrderedDict([('lisansNo',str)])
        )
        dispatcher.register_function('kdSalesReportData', kdSalesReportData,
            returns=OrderedDict([('kdSalesReportDataCevap', OrderedDict(
                                                [('OKCSeriNo', str),('cevapKodu', str), ('cevapAciklama', str),('reportData',
                                                                      OrderedDict([('prdName',str),
                                                                                    ('prdUnitType',str),
                                                                                    ('prdSaleAmount',str),
                                                                                    ('prdVatRate',str),
                                                                                    ('salesTotalVAT',str),
                                                                                    ('salesTotalCost',str),
                                                                                    ('salesReportZNo',str)]))])
                                    )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str),('OKCSeriNo',str),('reportDateStart',str),('reportDateEnd',str),('reportZNo',str)])
        )
        dispatcher.register_function('kdAcquirerInfo', kdAcquirerInfo,
            returns=OrderedDict([('kdAcquirerInfoCevap', OrderedDict(
                                                [('cevapKodu', str), ('cevapAciklama', str),('acquirerData',
                                                                      OrderedDict([('bkmId',str),
                                                                                    ('name',str),
                                                                                    ]))])
                                    )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str)])
        )
        dispatcher.register_function('kdContractInfo', kdContractInfo,
            returns=OrderedDict([('kdContractInfoCevap', OrderedDict(
                    [('OKCSeriNo', str),('cevapKodu', str), ('cevapAciklama', str),
                        ('contractInfo',
                            OrderedDict([('zReportActive',str),
                                        ('zReportStart',str),
                                        ('zReportEnd',str),
                                        ('salesActive',str),
                                        ('salesStart',str),
                                        ('salesEnd',str),
                                        ('eventsActive',str),
                                        ('eventsStart',str),
                                        ('eventsEnd',str),
                                        ('statisticActive',str),
                                        ('statisticStart',str),
                                        ('statisticEnd',str)
                                        ]
                            )
                        )
                    ]
                )
            )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str),('OKCSeriNo',str)])
        )

        dispatcher.register_function('kdSalesReceiptData', kdSalesReceiptData,
            returns=OrderedDict([('kdSalesReceiptDataCevap', OrderedDict(
                                                [('OKCSeriNo', str),('cevapKodu', str), ('cevapAciklama', str),('salesData',
                                                                      OrderedDict([('salesTime',str),
                                                                                    ('salesType',str),
                                                                                    ('salesReceiptType',str),
                                                                                    ('salesReceiptNo',str),
                                                                                    ('salesZNo',str),
                                                                                    ('salesEKUNo',str),
                                                                                    ('salesTotalVAT',str),
                                                                                    ('salesTotalCost',str),
                                                                                    ('salesCashAmount',str),
                                                                                    ('salesKKAmount',str),
                                                                                    ('salesOtherAmount',str),
                                                                                    ('salesCurrAmount',str),
                                                                                    ('salesLines',OrderedDict([ ('salesLineName',str),
                                                                                                                ('salesLineAmount',float),
                                                                                                                ('salesLineIncAmount',float),
                                                                                                                ('salesLineDecAmount',float),
                                                                                                                ('salesLineVAT',float),
                                                                                                                ('salesLineTotCost',float),
                                                                                                            ])
                                                                                    ),
                                                                                    ('bankLines',OrderedDict([ ('acquirer_id',str),
                                                                                                                ('terminal_no',str),
                                                                                                                ('account_no',str),
                                                                                                                ('receipt_no',str),
                                                                                                                ('kk_number',str),
                                                                                                                ('batch_no',str),
                                                                                                                ('onay_no',str),
                                                                                                                ('amount',float),
                                                                                                            ])
                                                                                    )
                                                                                ]))
                                                ])
                                    )]),
            
            args=OrderedDict([('kurumKodu',str),('kurumToken',str),('OKCSeriNo',str),('reportDateStart',str),('reportDateEnd',str),('reportZNo',str)])
        )
        
        dispatcher.register_function('kdSalesReceiptAllData', kdSalesReceiptAllData,
            returns=OrderedDict([('kdSalesReceiptDataAllCevap', OrderedDict(
                                                [('cevapKodu', str), ('cevapAciklama', str),('salesData',
                                                                      OrderedDict([ ('salesServerId',str),
                                                                                    ('salesTime',str),
                                                                                    ('salesType',str),
                                                                                    ('salesSerialNo',str),
                                                                                    ('salesReceiptType',str),
                                                                                    ('salesReceiptNo',str),
                                                                                    ('salesZNo',str),
                                                                                    ('salesEKUNo',str),
                                                                                    ('salesTotalVAT',str),
                                                                                    ('salesTotalCost',str),
                                                                                    ('salesCashAmount',str),
                                                                                    ('salesKKAmount',str),
                                                                                    ('salesOtherAmount',str),
                                                                                    ('salesCurrAmount',str),
                                                                                    ('salesLines',OrderedDict([ ('salesLineName',str),
                                                                                                                ('salesLineAmount',float),
                                                                                                                ('salesLineIncAmount',float),
                                                                                                                ('salesLineDecAmount',float),
                                                                                                                ('salesLineVAT',float),
                                                                                                                ('salesLineTotCost',float),
                                                                                                            ])
                                                                                    ),
                                                                                    ('bankLines',OrderedDict([ ('acquirer_id',str),
                                                                                                                ('terminal_no',str),
                                                                                                                ('account_no',str),
                                                                                                                ('receipt_no',str),
                                                                                                                ('kk_number',str),
                                                                                                                ('batch_no',str),
                                                                                                                ('onay_no',str),
                                                                                                                ('amount',float),
                                                                                                            ])
                                                                                    )
                                                                                ]))
                                                ])
                                    )]),
            
            args=OrderedDict([('kurumKodu',str),('kurumToken',str),('reportDateStart',str),('reportDateEnd',str)])
        )
        
        dispatcher.register_function('kdZReportData', kdZReportData,
            returns=OrderedDict([('kdZReportDataCevap', OrderedDict(
                                                [('OKCSeriNo', str),('cevapKodu', str), ('cevapAciklama', str),('zRepData',
                                                                      OrderedDict([('zReportTime',str),
                                                                                    ('zReportReceiptNo',str),
                                                                                    ('zReportNo',str),
                                                                                    ('zReportEKUNo',str),
                                                                                    ('zReportIncNum',str),
                                                                                    ('zReportDecNum',str),
                                                                                    ('zReportFixNum',str),
                                                                                    ('zReportFiscalRecNum',str),
                                                                                    ('zReportUnfiscalRecNum',str),
                                                                                    ('zReportCustRecNum',str),
                                                                                    ('zReportCancelRecNum',str),

                                                                                    ('zReportBillRecNum',str),
                                                                                    ('zReportRestTrnNum',str),
                                                                                    ('zReportParkTrnNum',str),
                                                                                    ('zReportKisimCost',OrderedDict([
                                                                                        ('kisim_toplam_tutari1',str),
                                                                                        ('kisim_toplam_tutari2',str),
                                                                                        ('kisim_toplam_tutari3',str),
                                                                                        ('kisim_toplam_tutari4',str),
                                                                                        ('kisim_toplam_tutari5',str),
                                                                                        ('kisim_toplam_tutari6',str),
                                                                                        ('kisim_toplam_tutari7',str),
                                                                                        ('kisim_toplam_tutari8',str),
                                                                                        ('kisim_toplam_tutari9',str),
                                                                                        ('kisim_toplam_tutari10',str),
                                                                                        ('kisim_toplam_tutari11',str),
                                                                                        ('kisim_toplam_tutari12',str),
                                                                                        ])),
                                                                                    ('zReportKisimName',OrderedDict([
                                                                                        ('kisim_adi1',str),
                                                                                        ('kisim_adi2',str),
                                                                                        ('kisim_adi3',str),
                                                                                        ('kisim_adi4',str),
                                                                                        ('kisim_adi5',str),
                                                                                        ('kisim_adi6',str),
                                                                                        ('kisim_adi7',str),
                                                                                        ('kisim_adi8',str),
                                                                                        ('kisim_adi9',str),
                                                                                        ('kisim_adi10',str),
                                                                                        ('kisim_adi11',str),
                                                                                        ('kisim_adi12',str),
                                                                                        ])),
                                                                                    ('zReportKisimNum',OrderedDict([
                                                                                        ('kisim_toplam_adedi1',str),
                                                                                        ('kisim_toplam_adedi2',str),
                                                                                        ('kisim_toplam_adedi3',str),
                                                                                        ('kisim_toplam_adedi4',str),
                                                                                        ('kisim_toplam_adedi5',str),
                                                                                        ('kisim_toplam_adedi6',str),
                                                                                        ('kisim_toplam_adedi7',str),
                                                                                        ('kisim_toplam_adedi8',str),
                                                                                        ('kisim_toplam_adedi9',str),
                                                                                        ('kisim_toplam_adedi10',str),
                                                                                        ('kisim_toplam_adedi11',str),
                                                                                        ('kisim_toplam_adedi12',str),
                                                                                        ])),
                                                                                    ('zReportIncCost',str),
                                                                                    ('zReportDecCost',str),
                                                                                    ('zReportCashAmount',str),
                                                                                    ('zReportOtherAmount',str),
                                                                                    ('zReportKKAmount',str),

                                                                                    ('zReportCurrAmount',OrderedDict([
                                                                                        ('doviz_toplam_tutar1',str),
                                                                                        ('doviz_toplam_tutar2',str),
                                                                                        ('doviz_toplam_tutar3',str),
                                                                                        ('doviz_toplam_tutar4',str),
                                                                                        ('doviz_toplam_tutar5',str),
                                                                                        ('doviz_toplam_tutar6',str)
                                                                                        ])),
                                                                                    ('zReportCurrName',OrderedDict([
                                                                                        ('doviz_adi1',str),
                                                                                        ('doviz_adi2',str),
                                                                                        ('doviz_adi3',str),
                                                                                        ('doviz_adi4',str),
                                                                                        ('doviz_adi5',str),
                                                                                        ('doviz_adi6',str)
                                                                                        ])),
                                                                                    ('zReportCurrToTL',OrderedDict([
                                                                                        ('doviz_tl_karsiligi1',str),
                                                                                        ('doviz_tl_karsiligi2',str),
                                                                                        ('doviz_tl_karsiligi3',str),
                                                                                        ('doviz_tl_karsiligi4',str),
                                                                                        ('doviz_tl_karsiligi5',str),
                                                                                        ('doviz_tl_karsiligi6',str)
                                                                                        ])),
                                                                                    ('zReportCancelAmount',str),
                                                                                    ('zReportFixAmount',str),
                                                                                    ('zReportBillAmount',str),
                                                                                    ('zReportRestAmount',str),
                                                                                    ('zReportTechNum',str),
                                                                                    ('zReportVATAmount',OrderedDict([
                                                                                        ('vergi_toplam_tutari1',str),
                                                                                        ('vergi_toplam_tutari2',str),
                                                                                        ('vergi_toplam_tutari3',str),
                                                                                        ('vergi_toplam_tutari4',str),
                                                                                        ('vergi_toplam_tutari5',str),
                                                                                        ('vergi_toplam_tutari6',str),
                                                                                        ('vergi_toplam_tutari7',str),
                                                                                        ('vergi_toplam_tutari8',str)
                                                                                        ])),
                                                                                    ('zReportVATRate',OrderedDict([
                                                                                        ('vergi_orani1',str),
                                                                                        ('vergi_orani2',str),
                                                                                        ('vergi_orani3',str),
                                                                                        ('vergi_orani4',str),
                                                                                        ('vergi_orani5',str),
                                                                                        ('vergi_orani6',str),
                                                                                        ('vergi_orani7',str),
                                                                                        ('vergi_orani8',str)
                                                                                        ])),
                                                                                    ('zReportVATCost',OrderedDict([
                                                                                        ('vergi_toplam_kdv1',str),
                                                                                        ('vergi_toplam_kdv2',str),
                                                                                        ('vergi_toplam_kdv3',str),
                                                                                        ('vergi_toplam_kdv4',str),
                                                                                        ('vergi_toplam_kdv5',str),
                                                                                        ('vergi_toplam_kdv6',str),
                                                                                        ('vergi_toplam_kdv7',str),
                                                                                        ('vergi_toplam_kdv8',str)
                                                                                        ])),

                                                                                    ('zReportDailyTotAmount',str),
                                                                                    ('zReportDailyVATAmount',str),
                                                                                    ('zReportAccTotAmount',str),
                                                                                    ('zReportAccVATAmount',str)
                                                                                    ]))])
                                    )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str),('OKCSeriNo',str),('reportDateStart',str),('reportDateEnd',str),('reportZNo',str)])
        )

        dispatcher.register_function('kdEventsData', kdEventsData,
            returns=OrderedDict([('kdEventsDataCevap', OrderedDict(
                                                [('OKCSeriNo', str),('cevapKodu', str), ('cevapAciklama', str),('eventsData',
                                                                      OrderedDict([('eventsSource',str),
                                                                                    ('eventsCrtLevel',str),
                                                                                    ('eventsType',str),
                                                                                    ('eventsTime',str),
                                                                                    ('eventsUserName',str),
                                                                                    ('eventsTermState ',str)]))])
                                    )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str),('OKCSeriNo',str),('reportDateStart',str),('reportDateEnd',str)])
        )

        dispatcher.register_function('kdStatisticsData', kdStatisticsData,
            returns=OrderedDict([('kdStatisticsDataCevap', OrderedDict(
                                                [('OKCSeriNo', str),('cevapKodu', str), ('cevapAciklama', str),('statData',
                                                                      OrderedDict([('statRespNum',str),
                                                                                    ('statFailNum',str),
                                                                                    ('statEthConn',str),
                                                                                    ('statDialConn',str),
                                                                                    ('statGPRSConn',str),
                                                                                    ('statGSMConn ',str),
                                                                                    ('statEthFail',str),
                                                                                    ('statDialFail',str),
                                                                                    ('statGPRSFail',str),
                                                                                    ('statGSMFail',str)]))])
                                    )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str),('OKCSeriNo',str),('reportDateStart',str),('reportDateEnd',str)])
        )
        dispatcher.register_function('kdSalesParamGrupData', kdSalesParamGrupData,
            returns=OrderedDict([('kdSalesParamGrupDataCevap', OrderedDict(
                                                [('cevapKodu', str), ('cevapAciklama', str),
                                                                     ('vatData',
                                                                      OrderedDict([('name',str),
                                                                                    ('vat',str)])),
                                                                      ('groupData',
                                                                      OrderedDict([('name',str),
                                                                                    ('vat',str)])),
                                                                      ('productData',
                                                                      OrderedDict([('name',str),
                                                                                    ('vat',str),
                                                                                    ('amount',float),
                                                                                    ('barcode',str)]))
                                                                      ])
                                    )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str)])
        )
        if http.request.httprequest.method == "POST":


            time_request = datetime.datetime.today()
            response = http.request.make_response(dispatcher.dispatch(http.request.httprequest.data))
            time_response = datetime.datetime.today()
            logging_var={'serviceName':'katma_deger_entegrasyonu','operationName':'',
                         'requestTime':time_request,'responseTime':time_response,
                         'requestData':http.request.httprequest.data,'responseData':response.data
                         }
            logging_obj = http.request.env['info_tsm.es_logs']
            logging_obj.sudo().create(logging_var)

        else:
            response = http.request.make_response(dispatcher.wsdl())

        response.headers['Content-Type'] = 'text/xml'
        return response

    @http.route('/esintegration', auth='public',csrf=False)
    def service_index(self, **kw):

        def get_partner( isyeriTCKN, isyeriVergiNo ):
            partner_obj=http.request.env['res.partner']
            partner_sahis = []
            partner_ser   = []

            if isyeriVergiNo != None:
                partner_ser=partner_obj.sudo().search([('taxNumber','=',isyeriVergiNo)])
            if isyeriTCKN != None:
                partner_sahis=partner_obj.sudo().search([('tckn','=',isyeriTCKN)])

            if len(partner_sahis)>0:
                partner =  map( lambda x:x.id, partner_sahis )
            elif len(partner_ser)>0:
                partner =  map( lambda x:x.id, partner_ser )
            else:
                partner = None

            return partner
        def get_NextUniqueIslemIdByCihaz ( cihaz_obj, esislemid ):
            term_kodu = cihaz_obj.name
            term_id   = cihaz_obj.id
            params    = gib_parameters( term_id )
            gib_formatter_obj         = gib_formatter()
            s                         = gibSocket(tls=True)
            parameters         = dict (term_kodu     = gib_formatter_obj.str_to_ascii(term_kodu , hex_=True ),
                                       islem_sira_no = '000001',
                                       term_date     = params.date,
                                       term_time     = params.time_,
                                       esislemid     = gib_formatter_obj.eksik_byte_tamamlamaca ( esislemid, 20 ))
            header             = '''6000010000%(term_kodu)sFF8B7639DF0225DF82040C%(term_kodu)sDF820803%(islem_sira_no)sDF820903%(term_date)sDF820A03%(term_time)sDF550FDFf0180A%(esislemid)s''' %( parameters )
            #print header
            final_message      =  gib_formatter_obj.uzunluk_lrc_final_send_data_hesaplamaca( header )
            s.connect(gib_formatter.TSM_SERVER_IP, gib_formatter.TSM_SERVER_PORT )
            donen_es_islem_no = None
            unique_id         = None
            try:
                s.gibsend( final_message.decode( 'hex' ) )
                #time.sleep(7)
                donen             =  s.gibreceive()
                donen_es_islem_no = donen[:20]
                unique_id         = donen[20:100]
                #print donen_es_islem_no , unique_id
                if str(donen_es_islem_no) == str(parameters[ 'esislemid' ]) :
                    #print 'Eşleştieaaaaa'
                    return unique_id
                else: raise 'Control Error Es Islem Id'
            except:
                e = sys.exc_info()
                #print e
            return False

        def get_cihaz_byES( esCihazId ):
            cihaz_obj = http.request.env["stock.production.lot"]
            cihaz     = cihaz_obj.sudo().search([('harici_cihaz_no','=',esCihazId)])
            if len(cihaz) == 1:
                return cihaz[0]
            return False
        def check_auth(kurumKodu,kurumToken):

            firma_obj = http.request.env["info_tsm.harici_firmalar"]
            firma = firma_obj.sudo().search([('kurum_kodu','=',kurumKodu),('kurum_token','=',kurumToken)])
            if len(firma) == 1:
                return firma[0]
            return False

        def okcESIntegrate(kurumKodu,kurumToken,esCihazSeriNo,masterOKCSeriNo,isyeriTCKN,isyeriVergiNo):
            #print kurumKodu, kurumToken,esCihazSeriNo,masterOKCSeriNo,isyeriTCKN,isyeriVergiNo
            if not check_auth(kurumKodu,kurumToken):
                return {'okcESIntegrateResult': {'okcESIntegrateCevap:masterOKCSeriNo':masterOKCSeriNo,
                                                 'okcESIntegrateCevap:cevapKodu':'001',
                                                 'okcESIntegrateCevap:cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'}}

            partner_id_list= get_partner(isyeriTCKN, isyeriVergiNo )
            #print partner_id_list
            if not partner_id_list:
                return {'okcESIntegrateResult': {'okcESIntegrateCevap:masterOKCSeriNo':masterOKCSeriNo,
                                                 'okcESIntegrateCevap:cevapKodu':'001',
                                                 'okcESIntegrateCevap:cevapAciklama':'TCKN/VKN Hatali'}}


            lot_ids=http.request.env['stock.production.lot'].sudo().search([('name','=',masterOKCSeriNo)])
            if len(lot_ids) == 0:
                return {'okcESIntegrateResult':  {'okcESIntegrateCevap:masterOKCSeriNo':masterOKCSeriNo,
                                                 'okcESIntegrateCevap:cevapKodu':'001',
                                                 'okcESIntegrateCevap:cevapAciklama':'Cihaz Tanimli Degil'}}

            lot_id = lot_ids[0]

            if lot_id.lot_state == 'sold' and lot_id.sold_to.id in partner_id_list:
                lot_id.harici_cihaz_no = esCihazSeriNo
                return {'okcESIntegrateResult': {'okcESIntegrateCevap:masterOKCSeriNo':masterOKCSeriNo,
                                                 'okcESIntegrateCevap:cevapKodu':'000',
                                                 'seriNoDogrulamaCevap:cevapAciklama':'Cihaz Firmaya Ait, Eslesme Kaydi Basarili'}}
            else:
                return {'okcESIntegrateResult': {'okcESIntegrateCevap:masterOKCSeriNo':masterOKCSeriNo,
                                                 'okcESIntegrateCevap:cevapKodu':'001',
                                                 'okcESIntegrateCevap:cevapAciklama':'TCKN/VKN Hatali'}}

        def okcIslemNo(kurumKodu,kurumToken,esCihazSeriNo,esIslemID):
            #print kurumKodu,kurumToken,esCihazSeriNo,esIslemID
            if not check_auth(kurumKodu,kurumToken):
                return {'okcIslemNoResult': {'okcIslemNoCevap:esCihazSeriNo':esCihazSeriNo,
                                                 'okcIslemNoCevap:cevapKodu':'001',
                                                 'okcIslemNoCevap:cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'}}

            cihaz_control = get_cihaz_byES(esCihazSeriNo )
            #print cihaz_control
            if not cihaz_control:
                return {'okcIslemNoResult': {'okcIslemNoCevap:esCihazSeriNo':esCihazSeriNo,
                                                 'okcIslemNoCevap:cevapKodu':'003',
                                                 'okcIslemNoCevap:cevapAciklama':'Eslesme Yapilmamis!!!'}}


            unique_islem_id = get_NextUniqueIslemIdByCihaz( cihaz_control, esIslemID )
            if  not unique_islem_id:
                return {'okcIslemNoResult':  {'okcIslemNoCevap:esCihazSeriNo':esCihazSeriNo,
                                                 'okcIslemNoCevap:cevapKodu':'001',
                                                 'okcIslemNoCevap:cevapAciklama':'Unique Key Bulunamadi'}}


            return {'okcIslemNoResult': {'okcIslemNoCevap:esCihazSeriNo':esCihazSeriNo,
                                                 'okcIslemNoCevap:cevapKodu':'000',
                                                 'okcIslemNoCevap:cevapAciklama':'islem Basarili',
                                                 'okcIslemNoCevap:esIslemID':esIslemID,
                                                 'okcIslemNoCevap:okcUniqueID':str(unique_islem_id)}}

        def okcFisBilgisi(kurumKodu,kurumToken,esCihazSeriNo,satisTipi,esIslemID,kalemVerisi):
            #print kurumKodu,kurumToken,esCihazSeriNo,esIslemID
            if not check_auth(kurumKodu,kurumToken):
                return {'okcFisBilgisiResult': {'okcFisBilgisiCevap:esCihazSeriNo':esCihazSeriNo,
                                                 'okcFisBilgisiCevap:cevapKodu':'001',
                                                 'okcFisBilgisiCevap:cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'}}

            cihaz_control = get_cihaz_byES(esCihazSeriNo )
            #print cihaz_control
            if not cihaz_control:
                return {'okcFisBilgisiResult': {'okcFisBilgisiCevap:esCihazSeriNo':esCihazSeriNo,
                                                 'okcFisBilgisiCevap:cevapKodu':'003',
                                                 'okcFisBilgisiCevap:cevapAciklama':'Eslesme Yapilmamis!!!'}}


            data={'kurum_kodu':kurumKodu,
                  'kurum_token':kurumToken,
                  'es_cihaz_seri_no':esCihazSeriNo,
                  'satis_tipi':satisTipi,
                  'es_islem_id':esIslemID,
                  'kalem_verisi':kalemVerisi
                  }
            new_fis = http.request.env['info_tsm.harici_sistemler_okc_fis_bilgisi'].sudo().create(data)

            return {'okcFisBilgisiResult': {'okcFisBilgisiCevap:esCihazSeriNo':esCihazSeriNo,
                                                 'okcFisBilgisiCevap:cevapKodu':'000',
                                                 'okcFisBilgisiCevap:cevapAciklama':'islem Basarili',
                                                 'okcFisBilgisiCevap:esIslemID':esIslemID
                                                 }}

        def okcFisOdemeDurumSorgulama ( kurumKodu,kurumToken,esIslemID):
            #print kurumKodu,kurumToken,esIslemID
            esCihazSeriNo = 1914
            if not check_auth(kurumKodu,kurumToken):
                return {'okcFisOdemeDurumSorgulamaResult': {'okcFisOdemeDurumSorgulamaCevap:esCihazSeriNo':esCihazSeriNo,
                                                 'okcFisOdemeDurumSorgulamaCevap:cevapKodu':'001',
                                                 'okcFisOdemeDurumSorgulamaCevap:cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'}}


            return {'okcFisOdemeDurumSorgulamaResult': {'okcFisOdemeDurumSorgulamaCevap:esCihazSeriNo':esCihazSeriNo,
                                                 'okcFisOdemeDurumSorgulamaCevap:cevapKodu':'000',
                                                 'okcFisOdemeDurumSorgulamaCevap:cevapAciklama':'islem basarili (Sabit Demo Veri)',
                                                 'okcFisOdemeDurumSorgulamaCevap:fisOdemeDurumu':'00',
                                                 'okcFisOdemeDurumSorgulamaCevap:satisTipi':'00',
                                                 'okcFisOdemeDurumSorgulamaCevap:nakitOdemeMiktar':'35.25',
                                                 'okcFisOdemeDurumSorgulamaCevap:kkOdemeMiktar':'00',
                                                 'okcFisOdemeDurumSorgulamaCevap:cariOdemeMiktar':'00'
                                                 }}


        param = http.request.env["ir.config_parameter"]

        dispatcher = SoapDispatcher(
            'soap_service',
            location = param.get_param( "bkm.ws_location", default='http://localhost:8069') + '/esintegration/',
            action = param.get_param( "bkm.ws_location", default='http://localhost:8069') + '/esintegration/',
            #namespace = "http://esintegration.infoteks.com.tr/",
            #prefix='tns',
            ns = False)

        # register func

        dispatcher.register_function('okcESIntegrate', okcESIntegrate,
            returns=OrderedDict([('okcESIntegrateResult', OrderedDict(
                                                [('masterOKCSeriNo', str),('cevapKodu', str), ('cevapAciklama', str)])
                                    )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str),('esCihazSeriNo',str),('masterOKCSeriNo',str),('isyeriTCKN',str),('isyeriVergiNo',str)])
                                     )

        dispatcher.register_function('okcIslemNo', okcIslemNo,
            returns=OrderedDict([('okcIslemNoResult', OrderedDict(
                                                [('esCihazSeriNo', str),('cevapKodu', str), ('cevapAciklama', str), ('esIslemID', str), ('okcUniqueID', str)])
                                    )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str),('esCihazSeriNo',str),('esIslemID',str)])
                                     )

        dispatcher.register_function('okcFisBilgisi', okcFisBilgisi,
            returns=OrderedDict([('okcFisBilgisiResult', OrderedDict(
                                                [('esCihazSeriNo', str),('cevapKodu', str), ('cevapAciklama', str), ('esIslemID', str)])
                                    )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str),('esCihazSeriNo',str),('satisTipi',str),('esIslemID',str),('kalemVerisi',str)])
                                     )

        dispatcher.register_function('okcFisOdemeDurumSorgulama', okcFisOdemeDurumSorgulama,
            returns=OrderedDict([('okcFisOdemeDurumSorgulamaResult', OrderedDict(
                                                [('esCihazSeriNo', str),('cevapKodu', str), ('cevapAciklama', str), ('fisOdemeDurumu', str), ('satisTipi', str),
                                                    ('nakitOdemeMiktar', str),('kkOdemeMiktar', str), ('cariOdemeMiktar', str)])
                                    )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str),('esIslemID',str)])
                                     )

        if http.request.httprequest.method == "POST":


            time_request = datetime.datetime.today()
            response = http.request.make_response(dispatcher.dispatch(http.request.httprequest.data))
            time_response = datetime.datetime.today()
            logging_var={'serviceName':'es_harici_sistemler','operationName':'',
                         'requestTime':time_request,'responseTime':time_response,
                         'requestData':http.request.httprequest.data,'responseData':response.data
                         }
            logging_obj = http.request.env['info_tsm.es_logs']
            logging_obj.sudo().create(logging_var)

        else:
            response = http.request.make_response(dispatcher.wsdl())

        response.headers['Content-Type'] = 'text/xml'
        return response
    @http.route('/servisinfo', auth='public',csrf=False)
    def service_index_service_user_info(self, **kw):


        def check_auth(user,passMd5):

            user_obj = http.request.env["res.partner.personel"]
            user_obj = user_obj.sudo().search([('teknik_servis_elemani','=',True),('teknik_servis_kullanici_kodu','=',user),
                ('teknik_servis_pass','=',passMd5),'|', ('teknik_servis_bas_tar', '=', False), ('teknik_servis_bas_tar', '<=', time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)),
                            '|', ('teknik_servis_bit_tar', '=', False), ('teknik_servis_bit_tar', '>=', time.strftime(DEFAULT_SERVER_DATETIME_FORMAT))])
            if len(user_obj) == 1:
                return user_obj[0]
            return False
        def check_auth_for_reset(user,tckn,email):
            user_obj = http.request.env["res.partner.personel"]
            user_obj = user_obj.sudo().search([('teknik_servis_elemani','=',True),('teknik_servis_kullanici_kodu','=',user),('tckn','=',tckn),
                ('email','=',email),'|', ('teknik_servis_bas_tar', '=', False), ('teknik_servis_bas_tar', '<=', time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)),
                            '|', ('teknik_servis_bit_tar', '=', False), ('teknik_servis_bit_tar', '>=', time.strftime(DEFAULT_SERVER_DATETIME_FORMAT))])
            if len(user_obj) == 1:
                return user_obj[0]
            return False

        def getCityDistirctId(plaka = None,ilce_kod = None):
            res = {}
            if plaka:
                il_obj = http.request.env["info_extensions.iller"].sudo().search([('plaka','=',plaka)])
                if len(il_obj) > 0:
                    res['il'] = il_obj[0].id
            if ilce_kod:
                ilce_obj = http.request.env["info_extensions.ilceler"].sudo().search([('ilce_kodu','=',ilce_kod)])
                if len(ilce_obj) > 0:
                    res['ilce'] = ilce_obj[0].id
            return res

        def okcGetUserInfo(userCode,userPass):
            #print userCode,userPass
            user  = check_auth(userCode,userPass)
            if not user:
                return {'okcGetUserInfoResult': {'userStatus':False}}
            else:
                #print 'response gidiyor'
                
                return {'okcGetUserInfoResult': {'userStatus':True,
                                                 'userCode':userCode,
                                                 'userDispName':user.name,
                                                 'userFirmName':user.parent_id.name,
                                                 'userFirmAddress':user.street,
                                                 'userAddressDistrictCode':user.ilce.ilce_kodu,
                                                 'userAddresscityCode':user.city_combo.plaka,
                                                 'userMail':user.email,
                                                 'userPhone':user.phone,
                                                 'userAuthDateStart':user.teknik_servis_bas_tar,
                                                 'userAuthDateEnd':user.teknik_servis_bit_tar,
                                                 'userCanScrap':user.hurdaya_ayirabilir,
                                                 'userCanTransfer':user.devir_yapabilir,}}

        def okcUserPassChange(userCode,userPass,
                              newPass=None,newAddressText=None,
                              newAddressDistrictCode=None,
                              newAddresscityCode=None,
                              newPhone=None,newMail=None):

            user  = check_auth(userCode,userPass)
            if not user:
                return {'okcUserPassChangeResult': {'userStatus':False}}
            res = getCityDistirctId( newAddresscityCode,newAddressDistrictCode)

            if newPass:
                user.teknik_servis_pass           = newPass
                user.teknik_servis_first_pass_raw = False
            if newAddressText:
                user.use_parent_address = False
                user.street = newAddressText
            if newAddressDistrictCode:
                if res.has_key('ilce'):
                    user.ilce = res['ilce']
            if newAddresscityCode:
                if res.has_key('il'):
                    user.city_combo = res['il']
            if newPhone:
                user.phone = newPhone
            if newMail:
                user.email = newMail

            return {'okcUserPassChangeResult': {'userStatus':True,
                                                 'userCode':userCode,
                                                 'userDispName':user.name,
                                                 'userFirmName':user.parent_id.name,
                                                 'userFirmAddress':user.street,
                                                 'userAddressDistrictCode':user.ilce.ilce_kodu,
                                                 'userAddresscityCode':user.city_combo.plaka,
                                                 'userMail':user.email,
                                                 'userPhone':user.phone,
                                                 'userAuthDateStart':user.teknik_servis_bas_tar,
                                                 'userAuthDateEnd':user.teknik_servis_bit_tar,
                                                 'userCanScrap':user.hurdaya_ayirabilir,
                                                 'userCanTransfer':user.devir_yapabilir,}}

        def okcUserPassReset(userCode,userTCKN,userMail):

            user = check_auth_for_reset( userCode,userTCKN,userMail )
            if not user:
                return {'okcUserPassResetResult': {'userStatus':False}}

            new_pass, new_pass_md5            = generate_new_pass()
            user.teknik_servis_pass           = new_pass_md5
            user.teknik_servis_first_pass_raw = new_pass
            user.send_mail()
            return {'okcUserPassResetResult': {'userStatus':True}}



        param = http.request.env["ir.config_parameter"]

        dispatcher = SoapDispatcher(
            'soap_service',
            location = param.get_param( "bkm.ws_location", default='http://localhost:8069') + '/servisinfo/',
            action = param.get_param( "bkm.ws_location", default='http://localhost:8069') + '/servisinfo/',
            namespace = 'servisinfo',
            prefix='okcGetUserInfoResult',
            ns = 'servisinfo')

        # register func

        dispatcher.register_function('okcGetUserInfo', okcGetUserInfo,
            returns=OrderedDict([('okcGetUserInfoResult', OrderedDict(
                                                [('userStatus', bool),('userCode', str),('userDispName', str),('userFirmName',str),
                                                    ('userFirmAddress',str),
                                                    ('userAddressDistrictCode',str),
                                                    ('userAddresscityCode',str),('userMail',str),('userPhone',str),
                                                    ('userAuthDateStart',str),('userAuthDateEnd',str),
                                                    ('userCanScrap',bool),('userCanTransfer',bool)])
                                    )]),
            args=OrderedDict([('userCode',str),('userPass',str)])
                                     )

        dispatcher.register_function('okcUserPassChange', okcUserPassChange,
            returns=OrderedDict([('okcUserPassChangeResult', OrderedDict(
                                                [('userStatus', bool),('userCode', str),('userDispName', str),('userFirmName',str),
                                                    ('userFirmAddress',str), ('userAddressDistrictCode',str),
                                                    ('userAddresscityCode',str),('userMail',str),('userPhone',str),
                                                    ('userAuthDateStart',str),('userAuthDateEnd',str),
                                                    ('userCanScrap',bool),('userCanTransfer',bool)])
                                    )]),
            args=OrderedDict([('userCode',str),('userPass',str),('newPass',str),
                                ('newAddressText',str),
                                ('newAddressDistrictCode',str),
                                ('newAddresscityCode',str),
                                ('newPhone',str),
                                ('newMail',str)])
                                     )

        dispatcher.register_function('okcUserPassReset', okcUserPassReset,
            returns=OrderedDict([('okcUserPassResetResult', OrderedDict(
                                                [('userStatus', bool),])
                                    )]),
            args=OrderedDict([('userCode',str),('userTCKN',str),('userMail',str)])
                                     )

        if http.request.httprequest.method == "POST":


            time_request = datetime.datetime.today()
            response = http.request.make_response(dispatcher.dispatch(http.request.httprequest.data))
            time_response = datetime.datetime.today()
            logging_var={'serviceName':'es_harici_sistemler','operationName':'',
                         'requestTime':time_request,'responseTime':time_response,
                         'requestData':http.request.httprequest.data,'responseData':response.data
                         }
            logging_obj = http.request.env['info_tsm.es_logs']
            logging_obj.sudo().create(logging_var)

        else:
            response = http.request.make_response(dispatcher.wsdl())

        response.headers['Content-Type'] = 'text/xml'
        return response
