# -*- coding: utf-8 -*-
{
    'name': "TSM",

    'summary': """
        TSM Sunucu Rapor ve Yönetim Arayüzleri""",

    'description': """
        YÖKC Projesine bağlı çalışan cihaz verilerini görüntüleme ve yönetme modülü.
    """,

    'author': "İnfoteks Technology",
    'website': "http://www.infoteks.com.tr",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'TSM',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','stock','info_extensions','mrp_repair','currency_rate_update','web_widget_timepicker'],

    # always loaded

    'data': [
        'security/tsm_group_manager.xml',
        'security/tsm_multi_company_rules.xml',
        'templates.xml',
        'views/graphs.xml',
        'views/lists.xml',
        'views/actions.xml',
        'views/forms.xml',
        'views/sol_menu.xml',
        'views/garanti.xml',
        'security/ir.model.access.csv',
        'views/searches.xml',
        'views/wizards.xml',
        'views/kanbans.xml',
        'views/asset.xml',
        'views/res_partner_teknik_servis.xml',
        'views/email_template.xml',
        'views/report_hurda.xml',
        
        #'data/info_tsm.haberlesme_tipleri.csv',
        #'data/info_tsm.p_gun_sonu_tipleri.csv',
        #'data/info_tsm.p_okc_durum_raporlama.csv',
        #'data/info_tsm.p_okc_left_button.csv',
        #'data/info_tsm.p_okc_right_button.csv',
        #'data/info_tsm.p_prm_update_type.csv',
        #'data/info_tsm.p_satis_ekran_tipleri.csv',
        #'data/info_tsm.p_sleep_suspend.csv',
        'data/automated_actions.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
    "qweb": ['static/src/xml/header.xml',],

    'installable': True,
    'application': True,
    'auto_install': False,
}