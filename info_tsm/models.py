# -*- coding: utf-8 -*-

from openerp import models, fields, api


class tsm_okc_log(models.Model):
     _name = 'info_tsm.okc_log'
     _order ='id desc'
     #data                       = fields.Text(string='OKC Data',required=False)
     terminal_serial            = fields.Char(string='Terminal Seri No', size=255)
     terminal_id                = fields.Many2one('stock.production.lot', string='Terminal')
     firma_id                   = fields.Many2one('res.partner', string='Firma')
     tpdu                       = fields.Char(string='TPDU', size=255)
     message_type               = fields.Char(string='Mesaj Tipi', size=255)
     job_group                  = fields.Char(string='İşlem Grubu', size=255)
     job_group_terminal_serial  = fields.Char(string='İşlem Grubu Terminal Seri No', size=255)
     order_no                   = fields.Char(string='İşlem Sıra No', size=255)
     terminal_date              = fields.Date(string='Terminal Tarihi', required=True)
     terminal_hour              = fields.Datetime(string='Terminal Saati', required=True)
     status                     = fields.Char(string='Durum', size=64)
     status_detail              = fields.Char(string='Durum Detay', size=255)
     source                     = fields.Integer( string='Kaynak')
     son_baglanti_tipi          = fields.Char(string="Son Bağlantı Tipi",default="M2M")

class tsm_error_log(models.Model):
     _name = 'info_tsm.error_data'
     _order ='id desc'

     data                       = fields.Text(string='OKC Hata Verisi',required=False)
     terminal_id                = fields.Many2one('stock.production.lot', string='Terminal')
     firma_id                   = fields.Many2one('res.partner', string='Firma')
     error_detail               = fields.Char(string='Hata Detayı', size=255, required=False)
     date                       = fields.Datetime( string='Tarih')
     source                     = fields.Integer( String='Kaynak')

class tsm_success_log(models.Model):
     _name = 'info_tsm.success_data'
     _order ='id desc'

     data                       = fields.Text(string='OKC Başarı Verisi',required=False)
     terminal_serial            = fields.Char(string='Terminal Seri No', size=255, required=False)
     terminal_id                = fields.Many2one('stock.production.lot', string='Terminal')
     firma_id                   = fields.Many2one('res.partner', string='Firma')
     message_type               = fields.Char(string='Mesaj Tipi', size=255, required=False)
     job_group                  = fields.Char(string='İşlem Grubu', size=255, required=False)
     job_group_terminal_serial  = fields.Char(string='İşlem Grubu Terminal Seri No', size=255, required=False)
     order_no                   = fields.Char(string='Sipariş No', size=255, required=False)
     status                     = fields.Char(string='Durum', size=64, required=False)
     date                       = fields.Datetime( string='Tarih')
     source                     = fields.Integer( String='Kaynak')



class tsm_olay_tipleri(models.Model):
    _name           = 'info_tsm.olay_kaydi_param'

    name            = fields.Char(string='Olay Tip Adı', required=True)
    kod             = fields.Integer(string='Olay Tipi', required=True)
    olay_kategori   = fields.Many2one('info_tsm.olay_kategori_kayitlari', string='Olay Tip Kategorisi', required=True)

    _sql_constraints    = [
        ('olay_tipi_kod_unique', 'unique(kod)', 'Bu Koda Sahip Başka Bir Olay Tipi Bulunmakta...'),
        ]

class tsm_olay_kategori_kayitlari(models.Model):
     _name     = 'info_tsm.olay_kategori_kayitlari'

     name      = fields.Char(string='Olay Kategori Adı', required=True)
     kategori_kod       = fields.Integer(string='Olay Tip Kodu',  required=True)
     olay_tipleri = fields.One2many('info_tsm.olay_kaydi_param', 'olay_kategori', string='Olay Tipleri')

     _sql_constraints = [
          ('olay_kategori_kod_unique', 'unique(kategori_kod)', 'Bu Koda Sahip Başka Bir Olay Kategorisi Bulunmakta...'),
     ]

class tsm_olay_kaydi(models.Model):
     _name = 'info_tsm.olay_kaydi_data'
     _order ='id desc'

     olay_kaynagi               = fields.Integer(string='Olay Kaynaği',required=False)
     terminal_serial            = fields.Char(string='Terminal Seri No', size=255, required=False, index=True)
     terminal_id                = fields.Many2one('stock.production.lot', string='Terminal')
     firma_id                   = fields.Many2one('res.partner', string='Firma',)
     olay_kritiklik_seviyesi    = fields.Integer(string='Olay Kritiklik Seviyesi',required=False)
     olay_tipi                  = fields.Many2one('info_tsm.olay_kaydi_param',string='Olay Tipi',required=False)
     kullanici_adi              = fields.Char(string='Kullanıcı Adı', size=255, required=False)
     olay_tarihi                = fields.Date( string='Olay Tarihi',required=False)
     olay_saati                 = fields.Datetime( string='Olay Saati',required=False)
     yazar_kasa_konumu          = fields.Integer(string='Konum Verisi', required=False)
     save_date                  = fields.Datetime( string='Kayıt Tarihi',required=False)
     source                     = fields.Integer( String='Kaynak')
     baslangic_tarihi           = fields.Date( string ='Başlangıç Tarihi', compute = '_tarih_araligi')
     bitis_tarihi               = fields.Date( string ='Bitiş Tarihi', compute = '_tarih_araligi')

     @api.multi
     def _tarih_araligi( self ):
          return lambda *a,**k:{}

class tsm_fis_data(models.Model):
     _name = 'info_tsm.fis_data'
     _order ='id desc'
     fis_tipi                   = fields.Selection(string='Fiş/Fatura', selection=[('fis','Fiş'),('fatura','Fatura')])
     fatura_no                  = fields.Char(string='Fatura No')
     fatura_tarihi              = fields.Date(string='Fatura Tarihi')                                                                     
     terminal_serial            = fields.Char(string='Terminal Seri No', size=255, required=False, index=True)
     terminal_id                = fields.Many2one('stock.production.lot', string='Terminal')
     firma_id                   = fields.Many2one('res.partner', string='Firma',)
     islem_tarihi               = fields.Date( string='İşlem Tarihi',required=False)
     islem_saati                = fields.Datetime( string='İşlem Saati',required=False)
     fis_sira_no                = fields.Integer(string='Fiş Sıra No',required=False, index=True)
     z_sayac_no                 = fields.Integer(string='Z Sayaç No',required=False)
     eku_no                     = fields.Integer(string='Ekü No',required=False)
     fis_blok_sira_no           = fields.Integer(string='Fiş Blok Sıra No',required=False)
     fis_blok_adedi             = fields.Integer(string='Fiş Blok Adet',required=False)
     toplam_kdv_tutari          = fields.Float(string='Toplam KDV Tutarı',required=False)
     toplam_tutar               = fields.Float(string='Toplam KDV Tutar',required=False)
     nakit_odenen_tutar         = fields.Float(string='Nakit Ödenen Tutar',required=False)
     kredi_karti_odenen_tutar   = fields.Float(string='Kredi Kartı Ödenen Tutar',required=False)
     diger_odenen_tutar         = fields.Float(string='Diğer Ödenen Tutar',required=False)
     doviz_odenen_tutar         = fields.Float(string='Döviz Ödenen Tutar',required=False)
     satis_kalem_adedi          = fields.Integer(string='Satış Kalem Adet',required=False)
     status                     = fields.Char(string='Durum', size=64, required=False)
     save_date                  = fields.Datetime( string='Kayıt Tarihi',required=False)
     source                     = fields.Integer( String='Kaynak')
     
     logo_etc                   = fields.Boolean(String="Logoya Aktarıldı")  

     fis_kalems = fields.One2many('info_tsm.fis_kalem_data', 'fis_id', string="Fiş Kalemleri")
     fis_banks  = fields.One2many('info_tsm.fis_bank_info', 'fis_id', string="Fiş Banka Bilgileri")

     baslangic_tarihi           = fields.Date( string ='Başlangıç Tarihi', compute = '_tarih_araligi')
     bitis_tarihi               = fields.Date( string ='Bitiş Tarihi', compute = '_tarih_araligi')

     @api.multi
     def _tarih_araligi( self ):
          return lambda *a,**k:{}

class tsm_fis_bank_info(models.Model):
    _name = 'info_tsm.fis_bank_info'
    
    fis_id      = fields.Many2one('info_tsm.fis_data', string="Fiş", required=False)
    acquirer_id = fields.Char('Bkm ID')
    terminal_no = fields.Char('Terminal No')
    account_no  = fields.Char('Hesap iş Yeri No')
    receipt_no  = fields.Char('Makbuz No')
    kk_number   = fields.Char('Kredi Kart No')
    batch_no    = fields.Char('Batch Numarası')
    onay_no     = fields.Char('Onay Numarası')
    amount      = fields.Float('Tutar')
class tsm_fis_kalem_data(models.Model):
     _name = 'info_tsm.fis_kalem_data'
     _order ='id desc'

     fis_id                     = fields.Many2one('info_tsm.fis_data', string="Fiş", required=False)
     isim                       = fields.Char(string='İsim', size=255, required=False)
     miktar                     = fields.Float(string='Miktar',required=False)
     indirim_tutari             = fields.Float(string='İndirim Tutarı',required=False)
     artirim_tutari             = fields.Float(string='Artırım Tutarı',required=False)
     kdv_orani                  = fields.Integer( string='KDV Oranı',required=False)
     tutar                      = fields.Float( string='Tutar',required=False)
     source                     = fields.Integer( String='Kaynak')
     

class tsm_istatistik_data( models.Model):
     _name = 'info_tsm.istatistik_data'
     _order ='id desc'

     terminal_serial                    = fields.Char(string='Terminal Seri No', size=255, required=False, index=True)
     terminal_id                        = fields.Many2one('stock.production.lot', string='Terminal')
     firma_id                           = fields.Many2one('res.partner', string='Firma', )
     toplam_yanit_alan_islem            = fields.Integer(string='Toplam Yanıt Alan İşlem', required=False)
     toplam_olumsuz_yanit_alan_islem    = fields.Integer(string='Toplam Olumsuz Yanıt Alan İşlem', required=False)

     toplam_ethernet_baglanti           = fields.Integer(string='Toplam Ethernet Bağlantı', required=False)
     toplam_dialup_baglanti             = fields.Integer(string='Toplam Dial-Up Bağlantı', required=False)
     toplam_gprs_baglanti               = fields.Integer(string='Toplam GPRS Bağlantı', required=False)
     toplam_gsm_baglanti                = fields.Integer(string='Toplam GSM Bağlantı', required=False)

     toplam_ethernet_basarisiz_baglanti = fields.Integer(string='Toplam Başarısız Ethernet Bağlantı', required=False)
     toplam_dialup_basarisiz_baglanti   = fields.Integer(string='Toplam Başarısız Dial-Up Bağlantı', required=False)
     toplam_gprs_basarisiz_baglanti     = fields.Integer(string='Toplam Başarısız GPRS Bağlantı', required=False)
     toplam_gsm_basarisiz_baglanti      = fields.Integer(string='Toplam Başarısız GSM Bağlantı', required=False)

     son_istatistik_gonderme_tarihi     = fields.Date( string='Son İstatistik Gönderme Tarihi',required=False)
     son_istatistik_gonderme_saati      = fields.Datetime( string='Son İstatistik Gönderme Saati',required=False)

     son_yanit_alan_islem               = fields.Integer(string='Son Yanıt Alınan İşlem', required=False)
     son_olumsuz_yanit_alan_islem       = fields.Integer(string='Son Olumsuz Yanıt Alınan İşlem', required=False)

     son_ethernet_baglanti              = fields.Integer(string='Son Ethernet Bağlantı', required=False)
     son_dialup_baglanti                = fields.Integer(string='Son Dial-Up Bağlantı', required=False)
     son_gprs_baglanti                  = fields.Integer(string='Son GPRS Bağlantı', required=False)
     son_gsm_baglanti                   = fields.Integer(string='Son GSM Bağlantı', required=False)

     son_ethernet_basarisiz_baglanti    = fields.Integer(string='Son Başarısız Ethernet Bağlantı', required=False)
     son_dialup_basarisiz_baglanti      = fields.Integer(string='Son Başarısız Dial-Up Bağlantı', required=False)
     son_gprs_basarisiz_baglanti        = fields.Integer(string='Son Başarısız GPRS Bağlantı', required=False)
     son_gsm_basarisiz_baglanti         = fields.Integer(string='Son Başarısız GSM Bağlantı', required=False)

     save_date                          = fields.Datetime( string='Kayıt Tarihi',required=False)
     source                             = fields.Integer( String='Kaynak')


     baslangic_tarihi           = fields.Date( string ='Başlangıç Tarihi', compute = '_tarih_araligi')
     bitis_tarihi               = fields.Date( string ='Bitiş Tarihi', compute = '_tarih_araligi')

     @api.multi
     def _tarih_araligi( self ):
          return lambda *a,**k:{}

class tsm_harici_baglanti( models.Model ):

     _name = 'info_tsm.harici_baglanti'
     _order = 'id desc'

     terminal_serial               = fields.Char(string='Terminal Serial', size=255, required=False, index=True)
     terminal_id                   = fields.Many2one('stock.production.lot', string='Terminal')
     firma_id                      = fields.Many2one('res.partner', string='Firma',store = True)
     islem_tarihi                  = fields.Date(string='Islem Tarihi',  required=False)
     islem_saati                   = fields.Datetime(string='Islem Saati',  required=False)
     ex_vendor_name                = fields.Char(string='Harici Servis Sağlayıcı', required=False, index=True)
     ex_model                      = fields.Char(string='Harici Model', required=False, index=True)
     ex_serial_num                 = fields.Char(string='Harici Cihaz Seri No', required=False, index=True)
     in_vendor_name                = fields.Char(string='ECR Servis Sağlayıcı', required=False, index=True)
     in_model                      = fields.Char(string='ECR Model', required=False, index=True)
     in_serial_num                 = fields.Char(string='ECR Cihaz Seri No', required=False, index=True)
     istek_mesaj_harici_rast_sayi  = fields.Char(string='İstek Mesaj Rast.', required=False, index=True)
     cevap_mesaj_harici_rast_sayi  = fields.Char(string='Cevap Mesaj Rast.', required=False, index=True)
     source                        = fields.Integer( String='Kaynak')


     baslangic_tarihi           = fields.Date( string ='Başlangıç Tarihi', compute = '_tarih_araligi')
     bitis_tarihi               = fields.Date( string ='Bitiş Tarihi', compute = '_tarih_araligi')

     @api.multi
     def _tarih_araligi( self ):
          return lambda *a,**k:{}

class tsm_z_raporu_data( models.Model):

     _name = 'info_tsm.z_raporu_data'
     _order ='id desc'

     terminal_serial            = fields.Char(string='Terminal Serial', size=255, required=False, index=True)
     terminal_id                = fields.Many2one('stock.production.lot', string='Terminal')
     firma_id                   = fields.Many2one('res.partner', string='Firma',store = True)
     islem_tarihi               = fields.Date(string='Islem Tarihi',  required=False)
     islem_saati                = fields.Datetime(string='Islem Saati',  required=False)
     fis_sira_no                = fields.Integer(string='Fis Sira No',  required=False, index=True)
     z_sayac_no                 = fields.Integer(string='Z Sayac No',  required=False)
     eku_no                     = fields.Integer(string='Eku No',  required=False)
     artirim_toplam_adedi       = fields.Integer(string='Artirim Toplam Adedi',  required=False)
     indirim_toplam_adedi       = fields.Integer(string='Indirim Toplam Adedi',  required=False)
     hata_duzelt_toplam_adedi   = fields.Integer(string='Hata Duzelt Toplam Adedi',  required=False)
     mali_fis_adedi             = fields.Integer(string='Mali Fis Adedi',  required=False)
     mali_olmayan_fis_adedi     = fields.Integer(string='Mali Olmayan Fis Adedi',  required=False)
     musteri_fis_adedi          = fields.Integer(string='Musteri Fis Adedi',  required=False)
     satis_fisi_iptal_adedi     = fields.Integer(string='Satis Fisi Iptal Adedi',  required=False)
     faturali_satis_adedi       = fields.Integer(string='Faturali Satis Adedi',  required=False)
     yemek_islem_sayisi         = fields.Integer(string='Yemek Islem Sayisi',  required=False)
     otopark_giris_islem_sayisi = fields.Integer(string='Otopark Giris Islem Sayisi',  required=False)
     kisim_toplam_tutari1       = fields.Float(string='Kisim Toplam Tutari1',  required=False)
     kisim_toplam_tutari2       = fields.Float(string='Kisim Toplam Tutari2',  required=False)
     kisim_toplam_tutari3       = fields.Float(string='Kisim Toplam Tutari3',  required=False)
     kisim_toplam_tutari4       = fields.Float(string='Kisim Toplam Tutari4',  required=False)
     kisim_toplam_tutari5       = fields.Float(string='Kisim Toplam Tutari5',  required=False)
     kisim_toplam_tutari6       = fields.Float(string='Kisim Toplam Tutari6',  required=False)
     kisim_toplam_tutari7       = fields.Float(string='Kisim Toplam Tutari7',  required=False)
     kisim_toplam_tutari8       = fields.Float(string='Kisim Toplam Tutari8',  required=False)
     kisim_toplam_tutari9       = fields.Float(string='Kisim Toplam Tutari9',  required=False)
     kisim_toplam_tutari10      = fields.Float(string='Kisim Toplam Tutari10',  required=False)
     kisim_toplam_tutari11      = fields.Float(string='Kisim Toplam Tutari11',  required=False)
     kisim_toplam_tutari12      = fields.Float(string='Kisim Toplam Tutari12',  required=False)
     kisim_adi1                 = fields.Char(string='Kisim Adi1', size=255, required=False)
     kisim_adi2                 = fields.Char(string='Kisim Adi2', size=255, required=False)
     kisim_adi3                 = fields.Char(string='Kisim Adi3', size=255, required=False)
     kisim_adi4                 = fields.Char(string='Kisim Adi4', size=255, required=False)
     kisim_adi5                 = fields.Char(string='Kisim Adi5', size=255, required=False)
     kisim_adi6                 = fields.Char(string='Kisim Adi6', size=255, required=False)
     kisim_adi7                 = fields.Char(string='Kisim Adi7', size=255, required=False)
     kisim_adi8                 = fields.Char(string='Kisim Adi8', size=255, required=False)
     kisim_adi9                 = fields.Char(string='Kisim Adi9', size=255, required=False)
     kisim_adi10                = fields.Char(string='Kisim Adi10', size=255, required=False)
     kisim_adi11                = fields.Char(string='Kisim Adi11', size=255, required=False)
     kisim_adi12                = fields.Char(string='Kisim Adi12', size=255, required=False)
     kisim_toplam_adedi1        = fields.Float(string='Kisim Toplam Adedi1',  required=False)
     kisim_toplam_adedi2        = fields.Float(string='Kisim Toplam Adedi2',  required=False)
     kisim_toplam_adedi3        = fields.Float(string='Kisim Toplam Adedi3',  required=False)
     kisim_toplam_adedi4        = fields.Float(string='Kisim Toplam Adedi4',  required=False)
     kisim_toplam_adedi5        = fields.Float(string='Kisim Toplam Adedi5',  required=False)
     kisim_toplam_adedi6        = fields.Float(string='Kisim Toplam Adedi6',  required=False)
     kisim_toplam_adedi7        = fields.Float(string='Kisim Toplam Adedi7',  required=False)
     kisim_toplam_adedi8        = fields.Float(string='Kisim Toplam Adedi8',  required=False)
     kisim_toplam_adedi9        = fields.Float(string='Kisim Toplam Adedi9',  required=False)
     kisim_toplam_adedi10       = fields.Float(string='Kisim Toplam Adedi10',  required=False)
     kisim_toplam_adedi11       = fields.Float(string='Kisim Toplam Adedi11',  required=False)
     kisim_toplam_adedi12       = fields.Float(string='Kisim Toplam Adedi12',  required=False)
     artirim_toplam_tutari      = fields.Float(string='Artirim Toplam Tutari',  required=False)
     indirim_toplam_tutari      = fields.Float(string='Indirim Toplam Tutari',  required=False)
     nakit_toplam_tutar         = fields.Float(string='Nakit Toplam Tutar',  required=False)
     diger_odeme_toplam_tutar   = fields.Float(string='Diger Odeme Toplam Tutar',  required=False)
     kredi_karti_toplam_tutar   = fields.Float(string='Kredi Karti Toplam Tutar',  required=False)
     doviz_toplam_tutar1        = fields.Float(string='Doviz Toplam Tutar1',  required=False)
     doviz_toplam_tutar2        = fields.Float(string='Doviz Toplam Tutar2',  required=False)
     doviz_toplam_tutar3        = fields.Float(string='Doviz Toplam Tutar3',  required=False)
     doviz_toplam_tutar4        = fields.Float(string='Doviz Toplam Tutar4',  required=False)
     doviz_toplam_tutar5        = fields.Float(string='Doviz Toplam Tutar5',  required=False)
     doviz_toplam_tutar6        = fields.Float(string='Doviz Toplam Tutar6',  required=False)
     doviz_adi1                 = fields.Char(string='Doviz Adi1', size=255, required=False)
     doviz_adi2                 = fields.Char(string='Doviz Adi2', size=255, required=False)
     doviz_adi3                 = fields.Char(string='Doviz Adi3', size=255, required=False)
     doviz_adi4                 = fields.Char(string='Doviz Adi4', size=255, required=False)
     doviz_adi5                 = fields.Char(string='Doviz Adi5', size=255, required=False)
     doviz_adi6                 = fields.Char(string='Doviz Adi6', size=255, required=False)
     doviz_tl_karsiligi1        = fields.Float(string='Doviz Tl Karsiligi1',  required=False)
     doviz_tl_karsiligi2        = fields.Float(string='Doviz Tl Karsiligi2',  required=False)
     doviz_tl_karsiligi3        = fields.Float(string='Doviz Tl Karsiligi3',  required=False)
     doviz_tl_karsiligi4        = fields.Float(string='Doviz Tl Karsiligi4',  required=False)
     doviz_tl_karsiligi5        = fields.Float(string='Doviz Tl Karsiligi5',  required=False)
     doviz_tl_karsiligi6        = fields.Float(string='Doviz Tl Karsiligi6',  required=False)
     satis_iptal_toplam_tutar   = fields.Float(string='Satis Iptal Toplam Tutar',  required=False)
     hata_duzelt_toplam_tutar   = fields.Float(string='Hata Duzelt Toplam Tutar',  required=False)
     faturali_satis_toplam_tutar = fields.Float(string='Faturali Satis Toplam Tutar',  required=False)
     yemek_islem_toplam_tutari   = fields.Float(string='Yemek Islem Toplam Tutari',  required=False)
     servis_mudehale_sayisi      = fields.Integer(string='Servis Mudehale Sayisi',  required=False)
     vergi_toplam_tutari1        = fields.Float(string='Vergi Toplam Tutari1',  required=False)
     vergi_toplam_tutari2        = fields.Float(string='Vergi Toplam Tutari2',  required=False)
     vergi_toplam_tutari3        = fields.Float(string='Vergi Toplam Tutari3',  required=False)
     vergi_toplam_tutari4        = fields.Float(string='Vergi Toplam Tutari4',  required=False)
     vergi_toplam_tutari5        = fields.Float(string='Vergi Toplam Tutari5',  required=False)
     vergi_toplam_tutari6        = fields.Float(string='Vergi Toplam Tutari6',  required=False)
     vergi_toplam_tutari7        = fields.Float(string='Vergi Toplam Tutari7',  required=False)
     vergi_toplam_tutari8        = fields.Float(string='Vergi Toplam Tutari8',  required=False)
     vergi_orani1                = fields.Integer(string='Vergi Orani1',  required=False)
     vergi_orani2                = fields.Integer(string='Vergi Orani2',  required=False)
     vergi_orani3                = fields.Integer(string='Vergi Orani3',  required=False)
     vergi_orani4                = fields.Integer(string='Vergi Orani4',  required=False)
     vergi_orani5                = fields.Integer(string='Vergi Orani5',  required=False)
     vergi_orani6                = fields.Integer(string='Vergi Orani6',  required=False)
     vergi_orani7                = fields.Integer(string='Vergi Orani7',  required=False)
     vergi_orani8                = fields.Integer(string='Vergi Orani8',  required=False)
     vergi_toplam_kdv1           = fields.Float(string='Vergi Toplam Kdv1',  required=False)
     vergi_toplam_kdv2           = fields.Float(string='Vergi Toplam Kdv2',  required=False)
     vergi_toplam_kdv3           = fields.Float(string='Vergi Toplam Kdv3',  required=False)
     vergi_toplam_kdv4           = fields.Float(string='Vergi Toplam Kdv4',  required=False)
     vergi_toplam_kdv5           = fields.Float(string='Vergi Toplam Kdv5',  required=False)
     vergi_toplam_kdv6           = fields.Float(string='Vergi Toplam Kdv6',  required=False)
     vergi_toplam_kdv7           = fields.Float(string='Vergi Toplam Kdv7',  required=False)
     vergi_toplam_kdv8           = fields.Float(string='Vergi Toplam Kdv8',  required=False)
     gunluk_toplam_tutar         = fields.Float(string='Gunluk Toplam Tutar',  required=False)
     gunluk_toplam_kdv           = fields.Float(string='Gunluk Toplam Kdv',  required=False)
     kumule_toplam_tutar         = fields.Float(string='Kumule Toplam Tutar',  required=False)
     kumule_toplam_kdv           = fields.Float(string='Kumule Toplam Kdv',  required=False)
     yazilim_dogrulama_degeri    = fields.Char(string='Yazilim Dogrulama Degeri', size=255, required=False)
     save_date                   = fields.Datetime(string='Save Date',  required=False)
     source                      = fields.Integer( String='Kaynak')

     baslangic_tarihi           = fields.Date( string ='Başlangıç Tarihi', compute = '_tarih_araligi')
     bitis_tarihi               = fields.Date( string ='Bitiş Tarihi', compute = '_tarih_araligi')

     total_0000        = fields.Float( string='%0 Toplam')
     total_0100        = fields.Float( string='%1 Toplam')
     total_0800        = fields.Float( string='%8 Toplam')
     total_1800        = fields.Float( string='%18 Toplam')

     kdv_0000        = fields.Float( string='%0 KDV')
     kdv_0100        = fields.Float( string='%1 KDV')
     kdv_0800        = fields.Float( string='%8 KDV')
     kdv_1800        = fields.Float( string='%18 KDV')


     @api.multi
     def _tarih_araligi( self ):
           return lambda *a,**k:{}

class bank_data ( models.Model ):
     _name = 'info_tsm.bank_data'
     program     = fields.Many2one ( 'info_tsm.program_type', string='Program Tipi')
     bank_data_header   = fields.Char(string='Bank Data Başlığı')
     seri_no            = fields.Char(string='Cihaz Seri No')
     eft_pos_seri       = fields.Char(string='EFT/POS Seri No')
     yazilim_surum_no   = fields.Char(string='Yazılım Sürüm No')
     lisans_no          = fields.Char(string='Lisans No')
     islem_tipi         = fields.Char(string='İşlem Tipi')
     islem_tarihi       = fields.Datetime(string='İşlem Tarihi')
     eku_no             = fields.Char(string='Ekü No')
     z_sayac_no         = fields.Char(string='Z Sayaç No')
     fis_sira_no         = fields.Char(string='Fiş Sıra No')