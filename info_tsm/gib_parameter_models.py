# -*- coding: utf-8 -*-
from openerp import models, fields, api, exceptions
from openerp import http
from check_rules import check
from utils import create_file_chunk, check_file_and_create_line , global_params,send_files_to_tsm, gib_file_formatter, list_files_from_tsm, get_file_parse,contains
from gib_message_formatter import gib_formatter, gibSocket, gib_parameters
import base64
from datetime import datetime
from dateutil.relativedelta import relativedelta
import time
import logging
_logger = logging.getLogger(__name__)

class GIB_STATUSES:
    KULLANIM      = 0
    AKTIF         = 1
    DEVIR         = 2
    GECICI_KAPAT  = 3
    HURDA         = 4


class Dummy:
    pass
class gib_muatabakat(models.Model):
    _name = 'gib_mutabakat'
    
    durum = fields.Char()
    tarih = fields.Date()
    seri_no = fields.Char()
    vkn     = fields.Char()
    active = fields.Integer()
class eshs_kurumlari(models.Model):
    _name = 'info_tsm.gib_eshs_firmalari'

    name = fields.Char('ESHS Firma Adı', required=True)
    kod  = fields.Char('ESHS Firma Kodu', required=True)

class gib_yoknc_status_codes( models.Model):

    _name = 'info_tsm.gib_yoknc_status_codes'

    durum_kodu = fields.Integer( string = 'Durum Kodu',required=True)
    name = fields.Char( string = 'Durum Açıklaması',required=True)
class kayit_listesi_dosya_satirlari  (models.Model):

    _name = 'info_tsm.kayit_listesi_dosya_satirlari'

    name                = fields.Many2one('stock.production.lot', string='Cihaz Seri No',required=True, domain=([('kayit_satiri','=',False)]))

    #uretici_firma       = fields.Char(string='Üretici Firma' , required = True, size=30, default=global_params.URETICI_FIRMA_ADI, readonly=True)
    tsm_firma_kodu      = fields.Char(string='TSM Firma Kodu', required = True, size=4, default=global_params.TSM_FIRMA_KODU, readonly=True)
    #cihaz_marka         = fields.Char(string='Cihaz Marka'   , required = True, size=15, default=global_params.CIHAZ_MARKA, readonly=True)
    #cihaz_model         = fields.Char(string='Cihaz Model'   , required = True, size=15, default= global_params.CIHAZ_MODEL, readonly=True)
    #yokc_public_key     = fields.Char(string='YÖKC Public Key', size=512, default='ynokc_public_key')
    #sertifika_seri_no   = fields.Char(string='Sertifika Seri No', size=30, required = True)
    #sertifik_gecerlilik_baslangic = fields.Datetime(string='Sertifika Geçerlilik Başlangıç', required = True)
    #sertifik_gecerlilik_bitis     = fields.Datetime(string='Sertifika Geçerlilik Bitiş',  required = True)
    cihaz_muhurleme_tarihi        = fields.Datetime(string='Cihaz Mühürleme Tarihi',  required=True)
    #baglanti_tipi                 = fields.Char(string='Bağlantı Tipi (1:TK1, 2:TK2)', size=2, required = True,default=1)
    yazilim_hash_degeri           = fields.Char(string='Yazılım Hash Değeri', size=64)
    kayit_dosyasi                 = fields.Many2one( 'info_tsm.gib_files', string="Kayıtlı Olduğu Dosya")
    durum                         = fields.Integer()#-1:sorunlu kayit var, 0 yeni, 1 dosyay aeklendi,2 sorunsuz
    color                         = fields.Integer()
    dosya_satir_no                = fields.Integer()
    hata_kodu                     = fields.Char(string='Hata Kodu')
    hata_aciklama                 = fields.Char(string='Hata Açıklama')
    source                        = fields.Char(string='Kaynak')

    '''
    @api.constrains('cihaz_muhurleme_tarihi')
    def _check_muhurleme(self):
        if self.cihaz_muhurleme_tarihi:
            if self.sertifik_gecerlilik_baslangic:
                if self.sertifik_gecerlilik_baslangic > self.cihaz_muhurleme_tarihi:
                    raise exceptions.ValidationError ("Cihaz Serfitika Geçerlilik Başlangıç Tarihi, Mühürleme Tarihinden Büyük Olamaz " )

    '''
    @api.onchange('name')
    @api.depends('name')
    def name_on_change(self):
        if self.name:
            #self.yokc_public_key                   = self.name.public_key
            #if self.name.sertifika_seri_no:
            #    self.sertifika_seri_no             = self.name.sertifika_seri_no
            #else:
            #    self.sertifika_seri_no             = self.name.name
            #self.sertifik_gecerlilik_baslangic = self.name.sertifika_baslangic
            #self.sertifik_gecerlilik_bitis     = self.name.sertifika_bitis
            self.yazilim_hash_degeri           = self.name.yazilim_hash

    '''
    @api.onchange('sertifik_gecerlilik_baslangic')
    @api.depends( 'sertifik_gecerlilik_baslangic')
    def sertifika_baslangic_onchange(self):
        if self.sertifik_gecerlilik_baslangic:
            onbir_yil    = relativedelta(years=11)
            #onbir_ay  = relativedelta(months=11)
            uc_gun    = relativedelta(days=3)
            dt = datetime.fromtimestamp(time.mktime(time.strptime(self.sertifik_gecerlilik_baslangic, '%Y-%m-%d')))

            self.sertifik_gecerlilik_bitis = dt + onbir_yil  - uc_gun

    '''
    @api.multi
    def unlink(self):
        for id_ in self:
            if id_.kayit_dosyasi:
                raise exceptions.ValidationError(' Bu Dosya Satırı Bir Dosyaya Eklenmiş ve Değiştirilemez. Sistem Yöneticinizle Görüşün.')

        return super(kayit_listesi_dosya_satirlari, self).unlink()

    _sql_constraints = [('serial_no_uniq','unique (name)','Bu Seri Numarası Zaten Eklenmiş')]
class gib_cevap_files( models.Model ):
    _name = 'info_tsm.gib_cevap_files'

    name         = fields.Char(  string='Dosya Adı')
    file_        = fields.Binary(string='Dosya')
    related_file = fields.Many2one('info_tsm.gib_files', string='İlişkili Dosya' )
class gib_files( models.Model):

    _name = 'info_tsm.gib_files'

    name         = fields.Char(  string='Dosya Adı')
    file_        = fields.Binary(string='Dosya')
    tip          = fields.Char(  string='Dosya Tipi')
    model_name   = fields.Char(  string='Gib Dosya Kodu')
    date         = fields.Date(  string='Kayıt Tarihi')
    sended       = fields.Integer( string='Gönderildi mi?')#-1:sorunlu kayit var, 0 yeni, 1 gonderildi,2 sorunsuz
    color        = fields.Integer()
    sira_no      = fields.Char( string='Dosya Sıra No')

    kayit_satirlari      = fields.One2many('info_tsm.kayit_listesi_dosya_satirlari','kayit_dosyasi' )
    aktivasyon_satirlari = fields.One2many('info_tsm.ynokc_aktivasyon_listesi_dosya_satirlari','kayit_dosyasi' )
    publickey_satirlari  = fields.One2many('info_tsm.ynokc_public_key_update','kayit_dosyasi' )
    yetkili_satirlari    = fields.One2many('info_tsm.ynokc_yetkili_servis','kayit_dosyasi' )

    path                 = fields.Char(string='Dosya Yolu')

    def get_files_list( self, model_name=None):
        params      = Dummy()
        params.params_host      = self.env.get('ir.config_parameter').get_param('info_tsm.send_gib_files_host')
        params.params_port      = self.env.get('ir.config_parameter').get_param('info_tsm.send_gib_files_port')
        params.params_tls       = self.env.get('ir.config_parameter').get_param('info_tsm.send_gib_files_tls')
        params.params_user_name = self.env.get('ir.config_parameter').get_param('info_tsm.send_gib_files_user_name')
        params.params_password  = self.env.get('ir.config_parameter').get_param('info_tsm.send_gib_files_pass')

        result_list = list_files_from_tsm ( params )
        if result_list == 1:
            saved_file_name_list = self.env.get('info_tsm.gib_cevap_files').search([])
            pool_pack         = Dummy()
            pool_pack.cr      = self._cr
            pool_pack.uid     = self.env.user.id
            pool_pack.context = {}
            pool_pack.pool    = self.env
            return_parse      = get_file_parse(saved_files_objects, pool_pack )
            return {1:'Dosyalar Başarıyla Alındı ve Listeler Güncellendi. '}
        else:
            return {1:'Dosyalar Gib\'den Alınamadı !!! \n %s' %result_list}

    def set_update_failed_file_row_status( self, file_obj,cr,exclude_list=[] ):
        satir_obj = {}

        kayit   = []
        aktif   = []
        public  = []
        yetkili = []
        for file_o in file_obj:

            if file_o.id not in exclude_list:
                file_o.sended = 1
                kayit.extend  (file_o.kayit_satirlari)
                aktif.extend  (file_o.aktivasyon_satirlari)
                public.extend (file_o.publickey_satirlari)
                yetkili.extend  (file_o.yetkili_satirlari)
        satir_obj['tsm_kayit_listesi_dosya_satirlari']             = kayit
        satir_obj['tsm_ynokc_aktivasyon_listesi_dosya_satirlari']  = aktif
        satir_obj['tsm_ynokc_public_key_update']                   = aktif
        satir_obj['tsm_ynokc_yetkili_servis']                      = yetkili
        #print satir_obj
        for k,v in satir_obj.iteritems() :
            for s in v:
                sql = 'update %s set durum=1 where id = %s' %( k, s.id )

                cr.execute(sql)
                cr.commit()

    def send_gib_files_daily(self):
        model_names=['info_tsm.kayit_listesi_dosya_satirlari','info_tsm.ynokc_public_key_update','info_tsm.ynokc_aktivasyon_listesi_dosya_satirlari','info_tsm.ynokc_yetkili_servis']
        for name in model_names:
            self.create_file_and_send_ftp( name )

    def create_file_and_send_ftp(self, model_name):
        #print model_name
        #gelen model name ine göre, herhangi bir dosya içine atılmamış satırlardan mesaj oluşturur ve gönderir.
        #gun icerisinde gonderilen dosya no'sunu da hesaplayalım.
        file_obj = None
        type_    = 0
        if model_name == 'info_tsm.gib_files':
            type_ = 1
            file_objs   = self.search([('sended','=', False),
                                       ('model_name','!=','info_tsm.yoknc_gib_uyari_dosya_satirlari')
                                      ])
        else:
            type_ = 2
            #print model_name
            if model_name == 'info_tsm.ynokc_aktivasyon_listesi_dosya_satirlari':

                resultset_ids = self.env.get( model_name ).search([('durum','in',[-1,0,'0','-1']),('name.name','not like','OF6011'),
                                                                   '|',('foreign_sale','!=',1),('foreign_sale','=',False)],
                                                           order='statu_degisim_tarihi')
            elif model_name == 'info_tsm.ynokc_yetkili_servis' :
                resultset_ids = self.env.get( model_name ).search([('durum','in',[-1,0,'0','-1'])])
            else:
                resultset_ids = self.env.get( model_name ).search([('durum','in',[-1,0,'0','-1']),
                                                                   ('name.name','not like','OF6011')])

            objects       = resultset_ids

            file_countby_date_and_time = self.search([('model_name','=', model_name),
                                                      ('date','=',time.strftime("%Y-%m-%d"))
                                                    ])
            file_number = len( file_countby_date_and_time ) + 1

            result = check_file_and_create_line( objects, model_name, 'odoo', file_number  )
            if result:
                new_id = self.create( result )
                dosya_satir_no = 1
                for o in objects:
                    o.durum          = 1 #durum guncellemesi önemli. aç
                    o.dosya_satir_no = dosya_satir_no
                    o.kayit_dosyasi  = new_id.id

                    dosya_satir_no  += 1

                file_obj    = new_id
            else:
                return {1:'Dosyaya Eklenmemiş Satır Kaydı Bulunamadı'}
        if file_obj:

            #print 'File Seeeeeeeend--------------->' , file_obj.id

            params      = Dummy()
            params.params_host      = self.env.get('ir.config_parameter').get_param('info_tsm.send_gib_files_host')
            params.params_port      = self.env.get('ir.config_parameter').get_param('info_tsm.send_gib_files_port')
            params.params_tls       = self.env.get('ir.config_parameter').get_param('info_tsm.send_gib_files_tls')
            params.params_user_name = self.env.get('ir.config_parameter').get_param('info_tsm.send_gib_files_user_name')
            params.params_password  = self.env.get('ir.config_parameter').get_param('info_tsm.send_gib_files_pass')
            
            #print params.params_host, params.params_port , params.params_tls ,params.params_user_name,params.params_password

            result_send  = send_files_to_tsm ( params, file_obj) #Göderimi bura yapıyor, aç
            #result_send = 1
            if result_send == 1:
                if model_name != 'info_tsm.gib_files':
                    file_obj.sended = 1
                    return {1:'Dosya Başarıyla Oluşuruldu ve Gib Dosyaları Listesine Eklendi ve GİB\'e Gönderildi. '}
                else:
                    self.set_update_failed_file_row_status( file_obj,cr )
                    return {1:'Daha Önce Gönderilmemiş Dosyalar Gib\' e gönderildi.'}

            else:
                if model_name != 'info_tsm.gib_files':
                    return {1:'Dosya Başarıyla Oluşuruldu ve Gib Dosyaları Listesine Eklendi ANCAK GİB\'e GÖNDERİLİRKEN BİR HATA OLUŞTU! %s' % result_send}
                else:
                    self.set_update_failed_file_row_status ( file_obj,cr,result_send )
                    return {1:'Dosyalar Gib\'e Gönderilemedi !!! \n %s' %result_send}
        else:
            return {1:"Gönderilmemiş Dosya Bulunamadı !!!"}

    @api.multi
    def unlink(self):
        for id_ in self:
            if id_.sended:
                raise exceptions.ValidationError(' Bu Dosya Gönerilmiş ve Değiştirilemez. Sistem Yöneticinizle Görüşün.')

        return super(gib_files, self).unlink()
    
class stock_production_lot(models.Model):
    _inherit = 'stock.production.lot'
    kayit_satiri      = fields.One2many('info_tsm.kayit_listesi_dosya_satirlari', 'name' )
    public_key_satiri = fields.One2many('info_tsm.ynokc_public_key_update', 'name' )
    aktivasyon_satiri = fields.One2many('info_tsm.ynokc_aktivasyon_listesi_dosya_satirlari', 'name' )

    public_key               = fields.Char(string = 'YNÖKC Public Key')
    sertifika_seri_no        = fields.Char(string = 'YNÖKC Sertifika Seri No')
    sertifika_baslangic      = fields.Date(string = 'Sertifika Başlanıç Tarihi')
    sertifika_bitis          = fields.Date(string = 'Sertifika Bitiş Tarihi')
    harici_cihaz_no          = fields.Char ( string = 'Harici Cihaz No')

class info_extensions_hakedis(models.Model):
    _inherit = 'info_extensions.hakedis'
    prim_ac         = fields.Many2one('info_tsm.ynokc_aktivasyon_listesi_dosya_satirlari')

class ynokc_aktivasyon_listesi_dosya_satirlari (models.Model ):
    _name = 'info_tsm.ynokc_aktivasyon_listesi_dosya_satirlari'

    def _get_default_status(self):
        return self.env['info_tsm.gib_yoknc_status_codes'].browse(1)

    name                        = fields.Many2one('stock.production.lot', string='Cihaz Seri No',required=True)
    mukellef                    = fields.Many2one('res.partner',string='Mükkelef Adı/Ünvanı' , required = True,ondelete='restrict',size=50)
    mukellef_tckn               = fields.Char(string='TCKN',related='mukellef.tckn')
    mukellef_vkn                = fields.Char(string='VKN',related='mukellef.taxNumber')
    parent_mukellef             = fields.Many2one('res.partner',string='Bayi Satışı')
    vkn                         = fields.Char(required = True,string="Vergi Kimlik No")
    #vergi_dairesi               = fields.Many2one('info_extensions.vergidaireleri',ondelete='restrict',string='Vergi Dairesi', size=50, store=True , required = True)
    #vergi_dairesi_kodu          = fields.Char(related='vergi_dairesi.vergi_dairesi_kodu', store=True, required = True)
    #faaliyet_konusu             = fields.Many2one('info_extensions.nace_kodlari',string='Faaliyet Konusu',ondelete='restrict',  store=True)
    #gib_isyeri_grup_kodu        = fields.Integer(string = 'GİB İşyeri Grup Kodu',size=4)
    ynokc_il                    = fields.Many2one('info_extensions.iller',ondelete='restrict',string='YNÖKC İl', size=50, select=True )
    yoknc_il_kodu               = fields.Char(related='ynokc_il.plaka', store=True, size=3, required = True)
    yoknc_ilce_kodu             = fields.Many2one('info_extensions.ilceler',ondelete='restrict',string='YNÖKC İlçe', size=50, select=True)
    yoknc_adres                 = fields.Text(  size=50, required = True, string="YNÖKC Adres")
    durum_bilgisi               = fields.Many2one('info_tsm.gib_yoknc_status_codes', size=1, required = True, string = "Durum", domain=[('durum_kodu','in',[1,2,3,4])])#default=_get_default_status

    satici                      = fields.Many2one('res.partner',string='Satıcı Adı / Ünvanı',  size=30)
    satici_vkn                  = fields.Char(string='Satıcı VKN', size=10)
    fatura_tarihi               = fields.Date(string='Fatura Tarihi', size=8)
    fatura_seri                 = fields.Char(string='Fatura Seri No', size=30)

    islem_yapan_ad                    = fields.Many2one('res.partner',string='İşlem Yapan Ad Soyad', required = True, size=30,
                                                        domain = ([('is_company','=',False),('teknik_servis_elemani','=',True)]))
    #islem_yapan_firma                 = fields.Many2one('res.partner',string='İşlem Yapan Firma', required = True, size=30)
    islem_yapan_tckn              = fields.Char(string='İşlem Yapan TCKN', required=True, size=11)
    #islem_yapan_firma_il              = fields.Many2one('info_extensions.iller',ondelete='restrict',string='İşlem Yapan Firma İl', size=50, select=True , required = True)
    #islem_yapan_firma_il_kodu         = fields.Char(related='islem_yapan_firma_il.plaka', store=True, size=3, required = True, string='İşlem Yapan Firma İl Kodu')

    statu_degisim_tarihi          = fields.Datetime(string='Statü Değişim Tarihi',  default=datetime.now())

    kayit_dosyasi                 = fields.Many2one( 'info_tsm.gib_files', string="Kayıtlı OLduğu Dosya")
    durum                         = fields.Integer()#-1:sorunlu kayit var, 0 yeni, 1 dosyay aeklendi,2 sorunsuz
    color                         = fields.Integer()
    dosya_satir_no                = fields.Integer()
    hata_kodu                     = fields.Char(string='Hata Kodu')
    hata_aciklama                 = fields.Char(string='Hata Açıklama')
    source                        = fields.Char(string='Kaynak')
    aktif                         = fields.Boolean()
    foreign_sale                  = fields.Integer(string='Yurtdışı Satış',default=0)
    
    prim_executed                 = fields.Boolean(default=False)
    
    @api.onchange ('mukellef')
    @api.depends('mukellef')
    def _mukellef_on_change(self):
        self.vkn              = self.mukellef.taxNumber
        self.vergi_dairesi    = self.mukellef.taxAgent
        #self.faaliyet_konusu  = self.mukellef.nace_kodu
        self.parent_mukellef  = self.mukellef.parent_id
        self.ynokc_il         = self.mukellef.city_combo.id
        self.yoknc_ilce_kodu   = self.mukellef.ilce.id
        address_format =  "%(street)s\n%(street2)s\n%(ilce_name_str)s\n%(city)s %(state_code)s %(zip)s\n%(country_name)s"
        args = {
            'state_code': self.mukellef.state_id.code or '',
            'state_name': self.mukellef.state_id.name or '',
            'ilce_name_str':self.mukellef.ilce.name or '',
            'country_code': self.mukellef.country_id.code or '',
            'country_name': self.mukellef.country_id.name or '',
            'company_name': self.mukellef.parent_name or '',
        }
        for field in self.mukellef._address_fields():
            args[field] = getattr(self.name.sold_to, field) or ''

        adress_str             = address_format % args
        self.yoknc_adres       = adress_str

    @api.onchange('name')
    @api.depends('name')
    def _name_on_change(self):
        if self.name:
            self.mukellef         = self.name.sold_to
            self.vkn              = self.mukellef.taxNumber
            self.vergi_dairesi    = self.mukellef.taxAgent
            #self.faaliyet_konusu  = self.mukellef.nace_kodu
            self.parent_mukellef  = self.mukellef.parent_id
            sale_ids        = []
            invoices_origin = []
            ops = self.env['stock.pack.operation.lot'].search([('lot_id','=',self.name.id)])
            
            
                
            for op in ops:
                sale_ids.append( op.operation_id.picking_id.sale_id )
                invoices_origin.append( op.operation_id.picking_id.origin)

            sale_id = None
            sale_ids.reverse()
            invoices_origin.reverse()
            origin = None
            if len( sale_ids) > 0:
                sale_id = sale_ids[0]
            if len ( invoices_origin ) > 0:
                origin = invoices_origin[0]
            if sale_id:
                self.fatura_seri = sale_id.client_order_ref
            invoice =  self.env['account.invoice'].search([('origin','=', origin)])
            if len(invoice) > 0:
                invoice = invoice[0]
                self.fatura_tarihi = invoice.date_invoice
                if not self.fatura_seri or str(self.fatura_seri).startswith('SO'):
                    self.fatura_seri   = invoice.reference
                if str( self.fatura_seri).startswith('SO'):
                    self.fatura_seri = invoice.name

            self.ynokc_il          = self.name.sold_to.city_combo.id
            self.yoknc_ilce_kodu   = self.name.sold_to.ilce.id

            address_format =  "%(street)s\n%(street2)s\n%(ilce_name_str)s\n%(city)s %(state_code)s %(zip)s\n%(country_name)s"
            args = {
                'state_code': self.name.sold_to.state_id.code or '',
                'state_name': self.name.sold_to.state_id.name or '',
                'ilce_name_str':self.name.sold_to.ilce.name or '',
                'country_code': self.name.sold_to.country_id.code or '',
                'country_name': self.name.sold_to.country_id.name or '',
                'company_name': self.name.sold_to.parent_name or '',
            }
            for field in self.name.sold_to._address_fields():
                args[field] = getattr(self.name.sold_to, field) or ''

            adress_str             = address_format % args
            self.yoknc_adres       = adress_str
            if self.parent_mukellef:
                self.satici            = self.parent_mukellef
                self.satici_vkn        = self.satici.taxNumber
            else:
                self.satici = self.env['res.partner'].browse(1)
                self.satici_vkn        = self.satici.taxNumber
        ##print (dir (self.name.pack_operation.picking_id))

    @api.onchange('durum_bilgisi')
    def get_islem_yapan (self ):
        if self.durum_bilgisi and self.name:
            islem_yapan = self.env['info_tsm.ynokc_islem_bilgisi'].search([('lot_id','=',self.name.id),
                                                        ('islem_id','=',self.durum_bilgisi.id)])
            if len(islem_yapan) > 0:
                islem_yapan = islem_yapan[0]
                self.islem_yapan_ad = islem_yapan.islem_yapan.id

    @api.onchange('satici')
    def _satici_on_change( self ):
        self.satici_vkn      = self.satici.taxNumber

    @api.onchange('islem_yapan_ad')
    def _islem_yapan_ad_on_change ( self ):
        #self.islem_yapan_firma = self.islem_yapan_ad.parent_id.id
        self.islem_yapan_tckn  = self.islem_yapan_ad.tckn
        #self.islem_yapan_firma_il = self.islem_yapan_firma.city_combo.id

    @api.multi
    def unlink(self):
        for id_ in self:
            if id_.kayit_dosyasi:
                raise exceptions.ValidationError(' Bu Dosya Satırı Bir Dosyaya Eklenmiş ve Değiştirilemez. Sistem Yöneticinizle Görüşün.')

        return super(ynokc_aktivasyon_listesi_dosya_satirlari, self).unlink()

    @api.model
    def create(self, values ):
        values["aktif"] = False

        devam, status = self.check_uygunluk ( values,'create' )
        if not devam:
            raise exceptions.ValidationError ( status )

        res = super( ynokc_aktivasyon_listesi_dosya_satirlari, self ).create( values )
        return res

    @api.multi
    def write( self, values ):
        devam, status = self.check_uygunluk ( values,'write' )
        if not devam:
            raise exceptions.ValidationError ( status )

        res = super( ynokc_aktivasyon_listesi_dosya_satirlari, self ).write( values )
        return res

    def check_uygunluk ( self, values, method):
        durum = None
        if method == 'create':
            onceki_kayitlar = self.env['info_tsm.ynokc_aktivasyon_listesi_dosya_satirlari'].search([('name','=',values['name'])],order='id desc' )
        elif method == 'write':
            self.ensure_one()
            onceki_kayitlar = self.env['info_tsm.ynokc_aktivasyon_listesi_dosya_satirlari'].search([('name','=',self.name.id)],order='id desc' )

        if values.has_key( 'durum_bilgisi'):
            durum           = self.env['info_tsm.gib_yoknc_status_codes'].browse( [values ['durum_bilgisi'],])
            if len(filter ( lambda x:x.durum_bilgisi.durum_kodu == GIB_STATUSES.HURDA, onceki_kayitlar )) > 0:
                return False, 'Cihaz Daha Önce Hurdaya Ayrılmış'
            if durum.durum_kodu == GIB_STATUSES.KULLANIM and len( onceki_kayitlar ) > 0:
                return False, 'Cihaz Daha Önce Kullanıma Alınmış'
            if len( onceki_kayitlar )>0:
                son_durum = onceki_kayitlar[0]
                if son_durum.durum_bilgisi.durum_kodu == GIB_STATUSES.GECICI_KAPAT and  durum.durum_kodu not in [ GIB_STATUSES.AKTIF, GIB_STATUSES.DEVIR]:
                    return False,'Geçici Kapamadan Sonra Sadece Devir veya Aktivasyon Yapılabilir.'
                if durum.durum_kodu == GIB_STATUSES.AKTIF and son_durum.durum_bilgisi.durum_kodu not in [GIB_STATUSES.GECICI_KAPAT]:
                    return False,'Sadece Geçici Kapatma veya Kurulumdan Sonra aktivasyon Yapılabilir'
        return True  ,''

class ynokc_islem_bilgisi ( models.Model ):
    _name = 'info_tsm.ynokc_islem_bilgisi'

    lot_id      = fields.Many2one('stock.production.lot', string='Cihaz Seri No')
    islem_id    = fields.Many2one('info_tsm.gib_yoknc_status_codes', string='İşlem No')
    islem_yapan = fields.Many2one('res.partner', string='İşlem Yapan Kişi')
    kamera      = fields.Boolean(string='Kamera Var mı?')
    barkod      = fields.Boolean(string='Barkod Okuyucu Var mı?')
    parmak      = fields.Boolean(string='Parmak İzi Okuyucu Var mı?')
    imei        = fields.Char( string = 'Imei No')
    m2m         = fields.Char( string = 'M2m Kod')

    devir_fatura_tarihi    = fields.Date(string = 'Devir Fatura Tarihi')
    devir_fatura_seri_sira = fields.Char( string = 'Devir Fatura Seri - Sıra No')
    devreden               = fields.Char( string='Devreden')
    devralan               = fields.Char( string = 'Devralan')
    foreign_sale           = fields.Integer(string='Yurtdışı Satış')

    satici_id              = fields.Many2one('res.partner', string='Satıcı')
class ynokc_public_key_update( models.Model ):

    _name = 'info_tsm.ynokc_public_key_update'

    @api.model
    def _get_kamu_sm(self):

        kamu_sm = self.env['info_tsm.gib_eshs_firmalari'].search([('kod','=','KMSM')])
        if len(kamu_sm ) > 0:
            kamu_sm = kamu_sm[0]
        else:
            kamu_sm = self.env['info_tsm.gib_eshs_firmalari']

        return kamu_sm

    name  = fields.Many2one('stock.production.lot', string='Cihaz Seri No',required=True)
    yokc_public_key     = fields.Char(string='YÖKC Public Key', size=512, required = True)
    sertifika_seri_no   = fields.Char(string='Sertifika Seri No', size=30, required = True)
    sertifik_gecerlilik_baslangic = fields.Datetime(string='Sertifika Geçerlilik Başlangıç', required = True)
    sertifik_gecerlilik_bitis     = fields.Datetime(string='Sertifika Geçerlilik Bitiş', required = True)
    kayit_dosyasi                 = fields.Many2one( 'info_tsm.gib_files', string="Kayıtlı Olduğu Dosya")
    durum                         = fields.Integer()#-1:sorunlu kayit var, 0 yeni, 1 dosyay aeklendi,2 sorunsuz
    color                         = fields.Integer()
    dosya_satir_no                = fields.Integer()
    hata_kodu                     = fields.Char(string='Hata Kodu')
    hata_aciklama                 = fields.Char(string='Hata Açıklama')
    source                        = fields.Char(string='Kaynak')
    eshs_kodu                     = fields.Many2one('info_tsm.gib_eshs_firmalari', default=_get_kamu_sm)


    @api.onchange('name')
    @api.depends('name')
    def name_on_change(self):
        if self.name:
            self.yokc_public_key               = self.name.public_key
            self.sertifika_seri_no             = self.name.sertifika_seri_no
            self.sertifik_gecerlilik_baslangic = self.name.sertifika_baslangic
            self.sertifik_gecerlilik_bitis     = self.name.sertifika_bitis

    '''
    @api.onchange('sertifik_gecerlilik_baslangic')
    @api.depends( 'sertifik_gecerlilik_baslangic')
    def sertifika_baslangic_onchange(self):
        if self.sertifik_gecerlilik_baslangic:
            onbir_yil = relativedelta(years=11)
            dt = datetime.fromtimestamp(time.mktime(time.strptime(self.sertifik_gecerlilik_baslangic, '%Y-%m-%d')))

            self.sertifik_gecerlilik_bitis = dt + onbir_yil
    '''


    @api.multi
    def unlink(self):
        for id_ in self:
            if id_.kayit_dosyasi:
                raise exceptions.ValidationError(' Bu Dosya Satırı Bir Dosyaya Eklenmiş ve Değiştirilemez. Sistem Yöneticinizle Görüşün.')

        return super(ynokc_public_key_update, self).unlink()

    _sql_constraints = [('serial_no_uniq','unique (name)','Bu Seri Numarası Zaten Eklenmiş')]
class ynokc_yetkili_servis ( models.Model ):

    _name = 'info_tsm.ynokc_yetkili_servis'
    
    yetkili_ad                    = fields.Many2one('res.partner',string='Yetkili Kişi Ad Soyad', required = True, size=30,
                                                        domain = ([('is_company','=',False),('teknik_servis_elemani','=',True)]))
    yetkili_firma                 = fields.Many2one('res.partner',string='Yetkili Firma', required = True, size=30)
    yetkili_firma_vkn             = fields.Char(related='yetkili_firma.taxNumber')
    yetkili_tckn                  = fields.Char(string='Yetkili Kişi TCKN', required=True, size=11)
    yetkili_firma_il              = fields.Many2one('info_extensions.iller',ondelete='restrict',string='Yetkili Firma İl', size=50, select=True , required = True)
    yetkili_firma_il_kodu         = fields.Char(related='yetkili_firma_il.plaka', store=True, size=3, required = True, string='Yetkili Firma İl Kodu')
    yetki_gecerlilik_baslangic        = fields.Date(string='Yetki Başlangıç', size=8, required = True)
    yetki_gecerlilik_bitis            = fields.Date(string='Yetki Bitiş', size=8, required = True)
    durum_bilgisi                     = fields.Integer(size=1, required = True, string = "Durum",default=1)
    kayit_dosyasi                     = fields.Many2one( 'info_tsm.gib_files', string="Kayıtlı Olduğu Dosya")
    durum                         = fields.Integer()#-1:sorunlu kayit var, 0 yeni, 1 dosyay aeklendi,2 sorunsuz
    color                         = fields.Integer()
    dosya_satir_no                = fields.Integer()
    hata_kodu                     = fields.Char(string='Hata Kodu')
    hata_aciklama                 = fields.Char(string='Hata Açıklama')
    source                        = fields.Char(string='Kaynak')


    @api.multi
    def unlink(self):
        for id_ in self:
            if id_.kayit_dosyasi:
                raise exceptions.ValidationError(' Bu Dosya Satırı Bir Dosyaya Eklenmiş ve Değiştirilemez. Sistem Yöneticinizle Görüşün.')

        return super(ynokc_yetkili_servis, self).unlink()

    @api.onchange('yetkili_ad')
    def _yetkili_ad_on_change ( self ):
        self.yetkili_firma = self.yetkili_ad.parent_id.id
        self.yetkili_tckn  = self.yetkili_ad.tckn
        self.yetkili_firma_il = self.yetkili_ad.city_combo.id
        self.yetki_gecerlilik_baslangic = self.yetkili_ad.teknik_servis_bas_tar
        self.yetki_gecerlilik_bitis = self.yetkili_ad.teknik_servis_bit_tar

    _sql_constraints = [('tckn_no_uniq','unique (yetkili_tckn)','Bu Kişi Zaten Eklenmiş')]
class yoknc_gib_uyari_dosya_satirlari ( models.Model ):
    _name = 'info_tsm.yoknc_gib_uyari_dosya_satirlari'

    name          = fields.Many2one('stock.production.lot', string='Cihaz Seri No')
    tarih         = fields.Date(string='Bildirim Tarihi')
    onem_derecesi = fields.Char('Önem Derecesi')
    kategori      = fields.Char('Kategori')
    alt_kategori  = fields.Char('Alt Kategori')
    kaynak        = fields.Char('Kaynak')
    aciklama      = fields.Char('Açıklama')
    aksiyon       = fields.Char('Aksiyon')
    kayit_dosyasi    = fields.Many2one( 'info_tsm.gib_files', string="Kayıtlı Olduğu Dosya")
    color            = fields.Integer()
class res_config_settings(models.TransientModel):

    _inherit='base.config.settings'

    send_gib_files_by_time=fields.Char(string='TSM GİB Dosyaları Otomatik Gönderilme Saati')
    send_gib_files_host   = fields.Char(string='GİB FTP IP Adresi')
    send_gib_files_port   = fields.Char(string='GİB FTP Port')
    send_gib_files_tls    = fields.Boolean(string='GİB FTP TLS mi?')
    send_gib_files_user_name = fields.Char(string='GİB FTP Kullanıcı Adı')
    send_gib_files_pass = fields.Char(string='GİB FTP Şifre')
    @api.multi
    def get_default_send_gib_files_by_time(self,fields):

        param = self.env["ir.config_parameter"]

        return {
                'send_gib_files_by_time': param.get_param( "info_tsm.send_gib_files_by_time", default='2320'),
                'send_gib_files_host'   : param.get_param( "info_tsm.send_gib_files_host", default=None),
                'send_gib_files_port'   : param.get_param( "info_tsm.send_gib_files_port", default='21'),
                'send_gib_files_tls'    : param.get_param( "info_tsm.send_gib_files_tls", default=True),
                'send_gib_files_user_name'    : param.get_param( "info_tsm.send_gib_files_user_name", default=None),
                'send_gib_files_pass'    : param.get_param( "info_tsm.send_gib_files_pass", default=None),
                }

    @api.one
    def set_send_gib_files_by_time(self):
        param = self.env["ir.config_parameter"]

        if self.send_gib_files_by_time:
            param.set_param('info_tsm.send_gib_files_by_time',self.send_gib_files_by_time)

        if self.send_gib_files_host:
            param.set_param('info_tsm.send_gib_files_host',self.send_gib_files_host)

        if self.send_gib_files_port:
            param.set_param('info_tsm.send_gib_files_port',self.send_gib_files_port)

        if self.send_gib_files_tls:
            param.set_param('info_tsm.send_gib_files_tls',self.send_gib_files_tls)

        if self.send_gib_files_user_name:
            param.set_param('info_tsm.send_gib_files_user_name',self.send_gib_files_user_name)

        if self.send_gib_files_pass:
            param.set_param('info_tsm.send_gib_files_pass',self.send_gib_files_pass)

class account_invoice(models.Model):
    _inherit = 'account.invoice'


    @api.model
    def listele(self):
        #print u'aaa'
        pass


    #fonksiyon adı fatura doğrula olsun
    @api.model
    def account_cron(self):

        aktif_durum_kod = self.env['info_tsm.gib_yoknc_status_codes'].search([('durum_kodu', '=', 1)])

        if len(aktif_durum_kod) != 0:
            aktif_durum_kod = aktif_durum_kod[0]

            taslak_faturalar = self.search([('type', '=', 'out_invoice'), ('state', '=', 'draft')])
            taslak_faturalar = filter(lambda x:x if len(x.lot_ids) > 0 else None,taslak_faturalar)

            for fatura in taslak_faturalar:

                urunler =  fatura.lot_ids

                continue_ = True

                for urun in urunler:
                    aktif_urun = self.env['info_tsm.ynokc_aktivasyon_listesi_dosya_satirlari'].search([('name', '=', urun.id), ('durum_bilgisi', '=', aktif_durum_kod.id)])

                    if len(aktif_urun) == 0:
                        continue_ = False
                        break

                if continue_ == True:

                    fatura.name = aktif_urun[-1].fatura_seri
                    fatura.date_invoice = aktif_urun[-1].fatura_tarihi

                    try:
                        fatura.signal_workflow('invoice_open')
                    except:
                        pass