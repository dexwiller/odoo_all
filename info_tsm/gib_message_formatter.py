from binascii import unhexlify, hexlify
import socket, hashlib
from datetime import datetime
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA256
import ssl
p_key_loc = '/var/lib/tsm_node/'
#p_key_loc = '/home/deniz/tsm_node/'
class gib_formatter:
    
    GIB_SERVER_IP   = 'localhost'
    GIB_SERVER_PORT = 6969
    
    TSM_SERVER_IP   = 'localhost'
    TSM_SERVER_PORT = 8001
    
    def  eksik_byte_tamamlamaca ( self,data, length, type_ =None): 
        sifir_str = ""
        if data == False:
            data = ''
        if not type_:
            data = str(data) ;
            sifir_sayisi = length - len(data)
            sifir_str = '0'* sifir_sayisi;
        elif type_ == 'ascii':
            length = length * 2;
            sifir_sayisi = length - len(data);
            if sifir_sayisi % 2 == 0:
                final_sifir_sayisi = sifir_sayisi / 2
                sifir_str = '20'*final_sifir_sayisi
            else:
                final_sifir_sayisi = (sifir_sayisi -1 )/2
                sifir_str = '20'*final_sifir_sayisi
                sifir_str += '0';
            
        data = sifir_str + data;
        return data;

    def uzunluk_lrc_final_send_data_hesaplamaca( self, send_data, return_only_lrc_uzunluk=False,uzunluk_byte_len=4 ):
        lrc_result = 0;
        for i in range (0, len(send_data ) +1, 2):
           data_part = send_data[i:i+2]
           if data_part:
                lrc_result = lrc_result ^ int(send_data [i:i+2], 16)
        lrc_result = hex( lrc_result ).upper()[2:]
        lrc_result = self.eksik_byte_tamamlamaca( lrc_result, 2)
       
        uzunluk = self.eksik_byte_tamamlamaca ( hex(len(send_data)/ 2).upper()[2:], uzunluk_byte_len )
        final_send_data = uzunluk + send_data + lrc_result
        if return_only_lrc_uzunluk:
            
            return [lrc_result, uzunluk]
        return final_send_data;
    def str_to_ascii ( self, str_, hex_=False ):
        final_ascii = ""
        for s in str_:
            buff = ord(s)
            if hex_ :
                buff = hex( buff )[2:]
            final_ascii += str(buff)
        
        return final_ascii
    
    def uzunluk_prefix_formatlamaca(self, uzunluk, prefix ):
        
        uzunluk = int(uzunluk,16)
        if int(uzunluk) < 128:
            prefix += hex(uzunluk)[2:].upper()
        elif int(uzunluk) > 255:
            
            prefix += '82'
            hex_uzunluk =  hex(uzunluk)[2:].upper()
            if len(hex_uzunluk)%2 != 0:
                hex_uzunluk = '0' + hex_uzunluk
            prefix += hex_uzunluk
        else:
            prefix += '81'
            prefix += hex(uzunluk)[2:].upper()
       
        return prefix
      
class gib_parameters:
    def __init__(self, id_ ):
        gib_formatter_obj = gib_formatter()
        self.id           = id_
        sira_no           = gib_formatter_obj.eksik_byte_tamamlamaca( self.id, 6 )
        now               = datetime.now()
        date              = str(now.year)[2:] + gib_formatter_obj.eksik_byte_tamamlamaca (now.month,2) + gib_formatter_obj.eksik_byte_tamamlamaca (now.day,2)
        time_             = gib_formatter_obj.eksik_byte_tamamlamaca (now.hour,2) + gib_formatter_obj.eksik_byte_tamamlamaca (now.minute,2) + gib_formatter_obj.eksik_byte_tamamlamaca (now.second,2)
        
        self.date                  = date
        self.time_                 = time_
        self.p_key_loc             = p_key_loc 
        self.message_part_1        = '''DF0215DF820803%sDF821003%sDF821103%s'''%( sira_no, date, time_)
        self.save_date             = date + time_[: len(time_)-2]
        self.sira_no               = sira_no
        self.imza_saati            = '20' + date + time_
        self.shaPad                = '3031300D060960864801650304020105000420' 
    
    def sign_data(self,private_key_loc, data):

        key = open(private_key_loc, "r").read()
        rsakey = RSA.importKey(key)
        signer = PKCS1_v1_5.new(rsakey)
        digest = SHA256.new()
        digest.update( data )
        sign = signer.sign( digest )
        
        return sign.encode('hex')
    
    def prvEncrypt(self, private_key_loc, data ):
        key = open(private_key_loc, "r").read()
        key = RSA.importKey(key)
        
        s    = hashlib.sha256()
        s.update( data )
        s_hash = s.hexdigest()
        data = self.shaPad + s_hash
        enc_data = key.encrypt(data, 256)
        ##print encode
        return enc_data[0].encode('hex')
 
class gibSocket:
    def __init__(self, sock=None, tls=None):
        self.tls = tls
        if sock is None:
            self.sock = socket.socket(
                socket.AF_INET, socket.SOCK_STREAM)
            if tls:
                self.sock  = ssl.wrap_socket(self.sock,
                       ssl_version=ssl.PROTOCOL_TLSv1_2)
        else:
            self.sock = sock

    def connect(self, host, port):
        self.sock.connect((host, port))
        self.sock.settimeout(10);
    def close(self):
        self.sock.close()
    def gibsend(self, msg):
        totalsent = 0
        while totalsent < len( msg ):
            sent = self.sock.send(msg[totalsent:])
            if sent == 0:
                raise RuntimeError("socket connection broken")
            totalsent = totalsent + sent

    def gibreceive(self):
        chunks = []
        bytes_recd = 0
        while bytes_recd < 128:
            chunk = self.sock.recv(min(128 - bytes_recd, 1024))
            if not chunk:
                break
            chunks.append(chunk)
            bytes_recd = bytes_recd + len(chunk)
        return ''.join(chunks)
    