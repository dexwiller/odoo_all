# -*- coding: utf-8 -*-
from openerp import tools
import openerp.addons.decimal_precision as dp
from openerp import models, fields, api, exceptions
class tsm_yeni_cihaz_report(models.Model):
    _name = "info_tsm.yeni_vers_cihaz"
    _description = "Yeni Vers. Cihazlar"
    _auto = False
    cihaz_seri_no = fields.Char(string='Stok Seri No')
    firma         = fields.Char(string='Kullanan Firma')
    _order        = 'cihaz_seri_no asc'

    @api.model
    def init(self):
        tools.drop_view_if_exists(self.env.cr,'info_tsm_yeni_vers_cihaz')
        self.env.cr.execute("""
            create or replace view info_tsm_yeni_vers_cihaz as (
                select a.id as id,a.name as cihaz_seri_no, (case when b.name is null then 'Satış Yapılmamış' else b.name end) as firma from
				(select id,name,sold_to from stock_production_lot where id in
				( select distinct( terminal_id) from info_tsm_okc_log )
				order by name asc)
				as a left join res_partner b on b.id = a.sold_to);
				""")
