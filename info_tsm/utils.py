# -*- coding: utf-8 -*-
import base64
import hashlib
import os
import time
import base64
import ssl
from ftplib import FTP, FTP_TLS
from tempfile import NamedTemporaryFile
from datetime import datetime
import socket
import subprocess
import glob
glob_folder_path = '/var/lib/odoo/addons/10.0/info_tsm/gib_files/'
out_folder_path  = glob_folder_path + 'OUT'
import pytz,string,random
from Crypto.PublicKey import RSA

def f7(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]	
def generate_new_pass():
    raw_pass = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(6))
    md5      = hashlib.md5()
    md5.update( raw_pass )

    return raw_pass, md5.hexdigest()
class global_params:

    URETICI_FIRMA_ADI = 'İNFOTEKS BİL. ELK. TELE. MED. REK.  İTH. İHR. SAN.'.decode('utf-8')
    CIHAZ_MARKA       = 'Fusions'.decode('utf-8')
    CIHAZ_MODEL       = '410G'.decode('utf-8')
    TSM_FIRMA_KODU    = 'O001'
    FIRMA_KODU        = 'OF'
    YAZILIM_HASH      = '197a9e8b61780abb15f0d99bdfd603eed4ca52284335ed799f2356f04d6920f6276a7b2be70169276729dd0b7b03f386c26f1cf1345407b0d60177a78b8dd99150bff808a2b187574d4517b16b2a644d361e2332730810ee4674a235440794f0dfc0fc238a5596be3c2471d7704bd7d580f228246b801b4f23ff1b7ff7c9fcbff320bbc0881a0bb268e20dd3483ae13595e461f6fd7012b99de6f55a08390e2347a0b8ddd7736d9bb40e95c12956860a5113134ae473e7f67b7681d14639d7077c88413f575a8100d38af3da16815fae9c3b0026cfad6d7691721d6e677d4ef1ae2d2b2ddbb602e199e42c0084e6570c0f96c86ea2ecadd5e6cecfefffde25ce'.decode('hex')
class file_name_keys:
    keys_dict = dict(
        Kayit     = 'info_tsm.kayit_listesi_dosya_satirlari',
        Durum     = 'info_tsm.ynokc_aktivasyon_listesi_dosya_satirlari',
        Sertifika = 'info_tsm.ynokc_public_key_update',
        Yetkili   = 'info_tsm.ynokc_yetkili_servis',
        Uyari     = 'info_tsm.yoknc_gib_uyari_dosya_satirlari'
    )
    OF_list       = [ keys_dict[ 'Kayit' ], keys_dict[ 'Durum' ], keys_dict[ 'Sertifika' ]]
    TC_list       = [ keys_dict[ 'Yetkili' ] ]
    UY_list       = [ keys_dict['Uyari'] ]
def contains(list, filter):
    for x in list:
        if filter(x):
            return True
    return False
def create_file_chunk( data, chunkSize ):
    hash_ = hashlib.sha256()
    data_b64  = base64.b64decode( data )
    hash_.update ( data_b64 )
    data  = data_b64.encode('hex')

    bytes        = len( data )
    chunks       = []
    if bytes > chunkSize:
        for i in range(0, bytes+1, chunkSize):
            chunks.append (data[i:i+ chunkSize])
    else:
        chunks.append ( data )

    hexdi = hash_.hexdigest()

    return  chunks, hexdi
def folder_check():
    now = datetime.now()
    year  = str(now.year)
    month = str(now.month)
    if len(month) == 1:
        month = '0' + month
    day   = str(now.day)
    if len(day) == 1:
        day = '0' + day
    global_path = glob_folder_path
    if not os.path.exists( global_path + year ):
        os.mkdir( global_path + year )
    global_path = global_path + year + '/'
    if not os.path.exists( global_path + month ):
        os.mkdir( global_path + month )
    global_path = global_path + month + '/'
    if not os.path.exists( global_path + day):
        os.mkdir( global_path + day)
    global_path = global_path + day + '/'

    return global_path
def gib_file_formatter( value, target_size, type_='string' ):

    if type_ == 'string':
        if not value:
            value = ''
        value = value.replace('\n','')
        value = value +  '#'
    if type_ == 'date':
        if value:
            value = value.replace('-','') + '#'
        else:
            value = '#'
    if type_ == 'time':
        if value:
            value = value.replace(':','')+ '#'
        else:
            value = '#'
    if type_ == 'numeric':
        if not value:
            value = ''
        value = str( value )
        value = value.replace('\n','')+ '#'
    if type_ == 'sha256':
        m = hashlib.sha256()
        m.update( value )
        value = m.hexdigest() + '#'
    if type_ == 'il_kodu':
        if not value:
            value = 0
        value = str( value )
        if len(value) < 3:
            zero = 3-len( value )
            value = '0'*zero + value
        value = value.replace('\n','')+ '#'
    '''
    if len( value ) > target_size:
            value = value[:target_size]
    '''
    return value.upper()
def get_time_formatted(time):
    #print time

    try:
        date_time = datetime.strptime(time, '%Y-%m-%d %H:%M:%S')
        tz = pytz.timezone('Europe/Istanbul')
        tzoffset = tz.utcoffset(date_time)
        date_time = date_time + tzoffset

        return datetime.strftime( date_time,'%Y-%m-%d %H:%M:%S')
    except:
        date_time = datetime.strptime(time, '%Y-%m-%d')
        tz = pytz.timezone('Europe/Istanbul')
        tzoffset = tz.utcoffset(date_time)
        date_time = date_time + tzoffset

        return datetime.strftime( date_time,'%Y-%m-%d')
def check_file_and_create_line(objects, model_name, source, file_sira_no ):
    tsm_firma_kodu   = global_params.TSM_FIRMA_KODU
    uretici_firma    = global_params.URETICI_FIRMA_ADI
    cihaz_marka      = global_params.CIHAZ_MARKA
    cihaz_model      = global_params.CIHAZ_MODEL
    firma_kodu       = global_params.FIRMA_KODU

    path = folder_check()
    if model_name   == 'info_tsm.kayit_listesi_dosya_satirlari':

        dosya_kodu       = '0001'
        tip              = 'Kayıt Listesi Dosyası'
        lines = []
        dosya_adi_header = 'YNOKC_Kayit'
        for object_ in objects:
            object_name                     = object_.name.name.split('OF')

            if len (object_name) > 10:
                seri_no_ = 'OF' + object_name[1][2:]
            else:
                seri_no_                         = object_.name.name
            #yokc_public_key                 = object_.yokc_public_key
            #sertifika_seri_no               = object_.sertifika_seri_no
            #sertifik_gecerlilik_baslangic   = object_.sertifik_gecerlilik_baslangic
            #sertifik_gecerlilik_bitis       = object_.sertifik_gecerlilik_bitis
            cihaz_muhurleme_tarihi          = get_time_formatted (object_.cihaz_muhurleme_tarihi)
            #baglanti_tipi                   = object_.baglanti_tipi
            yazilim_hash_degeri_db          = object_.yazilim_hash_degeri

            '''
            if yokc_public_key == 'ynokc_public_key':
                line_id = object_.name.name.rsplit('000')[-1]
                f_size  = 512 - len( line_id )
                yokc_public_key = 'F'*f_size + str( line_id )
            '''
            #uretici_firma     = gib_file_formatter( uretici_firma, 30 )
            tsm_firma_kodu_f    = gib_file_formatter( tsm_firma_kodu, 4 )
            #cihaz_marka       = gib_file_formatter( cihaz_marka, 15)
            #cihaz_model       = gib_file_formatter( cihaz_model, 15)
            firma_kodu_f        = gib_file_formatter( firma_kodu,4)
            seri_no           = gib_file_formatter( seri_no_, 12)
            #yokc_public_key   = gib_file_formatter( yokc_public_key,512)
            #sertifika_seri_no = gib_file_formatter( sertifika_seri_no, 30)

            #sertifik_gecerlilik_baslangic = gib_file_formatter (sertifik_gecerlilik_baslangic, 8, 'date')
            #sertifik_gecerlilik_bitis     = gib_file_formatter (sertifik_gecerlilik_bitis, 8, 'date')
            #print cihaz_muhurleme_tarihi

            cihaz_muhurleme_tarihi_formatted        = gib_file_formatter (cihaz_muhurleme_tarihi[:10], 8, 'date')
            cihaz_muhurleme_saati                    = gib_file_formatter (cihaz_muhurleme_tarihi[11:16], 4, 'time')
            #baglanti_tipi                 = gib_file_formatter( baglanti_tipi, 2)
            if yazilim_hash_degeri_db:
                #print '*****************************YAzılım Hash DB den GEldi ************************************'
                yazilim_hash_degeri           = gib_file_formatter( yazilim_hash_degeri_db, 64  )
            else:
                #print '*****************************YAzılım Hash SABIT ************************************'
                yazilim_hash_degeri           = gib_file_formatter(  global_params.YAZILIM_HASH, 64, 'sha256' )

            lines.append   ('D'  + tsm_firma_kodu_f  + firma_kodu_f + seri_no     + cihaz_muhurleme_tarihi_formatted + cihaz_muhurleme_saati  + yazilim_hash_degeri.replace('#','') + '\n' )
    elif model_name == 'info_tsm.ynokc_aktivasyon_listesi_dosya_satirlari':
        dosya_kodu       = '0002'
        tip              = 'Aktivasyon Listesi Dosyası'
        lines = []
        dosya_adi_header = 'YNOKC_Durum'
        #print objects
        #objects.sort(key = lambda x: x.statu_degisim_tarihi)
        #print objects
        for object_ in objects:
            mukellef           = gib_file_formatter (object_.mukellef.name, 140)
            vkn                = gib_file_formatter (object_.vkn, 10, 'numeric')
            #vergi_dairesi_kodu = gib_file_formatter (object_.vergi_dairesi_kodu, 6 )
            #faaliyet_konusu    = gib_file_formatter (object_.faaliyet_konusu.kod, 8)
            #gib_isyeri_grup_kodu = gib_file_formatter ( object_.gib_isyeri_grup_kodu , 4, 'numeric')
            yoknc_il_kodu        = gib_file_formatter ( object_.yoknc_il_kodu , 3, 'il_kodu')#dokümanda alfanümerik ama padding soldan 0
            yoknc_ilce_kodu      = gib_file_formatter ( object_.yoknc_ilce_kodu.name , 20)
            yoknc_adres          = gib_file_formatter ( object_.yoknc_adres , 300)
            durum_bilgisi        = gib_file_formatter ( object_.durum_bilgisi.durum_kodu ,1, 'numeric')

            if int(object_.durum_bilgisi.durum_kodu) == 3 or int(object_.durum_bilgisi.durum_kodu) == 4:


                satici               = gib_file_formatter ( '' ,1000)
                satici_vkn           = gib_file_formatter ( '' ,10,'numeric')
                fatura_tarihi        = gib_file_formatter ( '',8,'date')
                fatura_seri_no       = gib_file_formatter ( '',30,'numeric')#dokümanda alfanümerik ama padding soldan 0
            else:
                satici               = gib_file_formatter ( object_.satici.name ,1000)
                satici_vkn           = gib_file_formatter ( object_.satici_vkn ,10,'numeric')

                if object_.fatura_tarihi:
                    tarih_formatted = get_time_formatted( object_.fatura_tarihi )
                    fatura_tarihi        = gib_file_formatter ( tarih_formatted[:10],8,'date')
                else:
                    fatura_tarihi        = gib_file_formatter ( '',8,'date')
                fatura_seri_no       = gib_file_formatter ( object_.fatura_seri,30,'numeric')
            firma_kodu_               = gib_file_formatter( firma_kodu,4)
            #print firma_kodu_

            object_name                     = object_.name.name.split('OF')
            if len (object_.name.name) > 10:
                seri_no_ = 'OF' + object_name[1][2:]
            else:
                seri_no_                         = object_.name.name

            seri_no              = gib_file_formatter( seri_no_, 12)
            #print seri_no
            #islem_yapan_ad       = gib_file_formatter( object_.islem_yapan_ad.name, 30)
            #islem_yapan_firma    = gib_file_formatter( object_.islem_yapan_firma.name, 30)
            islem_yapan_tckn     = gib_file_formatter( object_.islem_yapan_tckn, 11, 'numeric')
            #islem_yapan_firma_il_kodu   = gib_file_formatter ( object_.islem_yapan_firma_il_kodu , 3, 'numeric')#dokümanda alfanümerik ama padding soldan 0
            statu_degisim_tarihi_formatted = get_time_formatted( object_.statu_degisim_tarihi )
            statu_degisim_tarihi  = gib_file_formatter (statu_degisim_tarihi_formatted[:10],8,'date')
            statu_degisim_saati  = gib_file_formatter ( statu_degisim_tarihi_formatted[11:16],4,'time')

            if int(object_.durum_bilgisi.durum_kodu) == 1 or int(object_.durum_bilgisi.durum_kodu) == 2:
                lines.append ('D' + mukellef + vkn  + yoknc_il_kodu + yoknc_ilce_kodu + yoknc_adres + durum_bilgisi + satici + satici_vkn + fatura_tarihi + fatura_seri_no + firma_kodu_ + seri_no   + islem_yapan_tckn  + statu_degisim_tarihi + statu_degisim_saati.replace('#','')+ '\n')
            elif int(object_.durum_bilgisi.durum_kodu) == 3 or int(object_.durum_bilgisi.durum_kodu) == 4 or int( object_.durum_kodu) == 5:
                lines.append ('D' +'#####'+ durum_bilgisi +'####'+  firma_kodu_ + seri_no   + islem_yapan_tckn  + statu_degisim_tarihi + statu_degisim_saati.replace('#','')+ '\n')
    elif model_name == 'info_tsm.ynokc_public_key_update':

        dosya_kodu       = '0003'
        tip              = 'Public Key Güncelleme Dosyası'
        lines = []
        dosya_adi_header = 'YNOKC_Sertifika'

        for object_ in objects:
            object_name                     = object_.name.name.split('OF')
            if len (object_.name.name) > 10:
                seri_no_ = 'OF' + object_name[1][2:]
            else:
                seri_no_                         = object_.name.name

            yokc_public_key                 = object_.yokc_public_key
            if yokc_public_key.startswith('--'):
                
                pubkey          = RSA.importKey(yokc_public_key.replace('KEY-----','KEY-----\n').replace('-----END','\n-----END'))
                yokc_public_key = hex(pubkey.n).rstrip("L").lstrip("0x") or "0"
            print yokc_public_key
            sertifika_seri_no               = object_.sertifika_seri_no
            sertifik_gecerlilik_baslangic   = get_time_formatted (object_.sertifik_gecerlilik_baslangic)
            sertifik_gecerlilik_bitis       = get_time_formatted (object_.sertifik_gecerlilik_bitis)
            eshs_kodu                       = object_.eshs_kodu.kod
            if not eshs_kodu:
                eshs_kodu = 'KMSM'
            if yokc_public_key == 'ynokc_public_key':
                line_id = object_.name.name.rsplit('000')[-1]
                f_size  = 512 - len( line_id )
                yokc_public_key = 'F'*f_size + str( line_id )

            seri_no           = gib_file_formatter( seri_no_, 12)
            yokc_public_key   = gib_file_formatter( yokc_public_key,512)
            
            sertifika_seri_no = gib_file_formatter( sertifika_seri_no, 30)
            sertifik_gecerlilik_baslangic_f = gib_file_formatter (sertifik_gecerlilik_baslangic[:10], 8, 'date')
            sertifik_gecerlilik_baslangic_s = gib_file_formatter (sertifik_gecerlilik_baslangic[11:16], 4, 'time')
            sertifik_gecerlilik_bitis_f     = gib_file_formatter (sertifik_gecerlilik_bitis[:10], 8, 'date')
            sertifik_gecerlilik_bitis_s     = gib_file_formatter (sertifik_gecerlilik_bitis[11:16], 4, 'time')
            eshs_kodu                       = gib_file_formatter (eshs_kodu,4)
            lines.append   ('D' + seri_no + yokc_public_key + sertifika_seri_no + sertifik_gecerlilik_baslangic_f + sertifik_gecerlilik_baslangic_s + sertifik_gecerlilik_bitis_f + sertifik_gecerlilik_bitis_s+ eshs_kodu.replace('#','') +'\n' )
    elif model_name == 'info_tsm.ynokc_yetkili_servis':
        dosya_kodu       = '0005'
        tip              = 'Servis / Yetkili Kişi Listesi Dosyası'
        lines = []
        dosya_adi_header = 'YNOKC_Yetkili'
        for object_ in objects:

            yetkili_ad              = gib_file_formatter( object_.yetkili_ad.name, 30)
            yetkili_firma           = gib_file_formatter( object_.yetkili_firma.name, 30)
            yetkili_firma_vkn       = gib_file_formatter( object_.yetkili_firma.taxNumber, 30)
            yetkili_tckn            = gib_file_formatter( object_.yetkili_tckn, 11, 'numeric')
            yetkili_firma_il_kodu   = gib_file_formatter ( object_.yetkili_firma_il_kodu , 3, 'il_kodu')
            yetki_gecerlilik_baslangic = gib_file_formatter (get_time_formatted(object_.yetki_gecerlilik_baslangic), 8, 'date')
            yetki_gecerlilik_bitis     = gib_file_formatter (get_time_formatted(object_.yetki_gecerlilik_bitis), 8, 'date')
            durum_bilgisi              = gib_file_formatter ( object_.durum_bilgisi ,1, 'numeric')

            lines.append ('D'  +yetkili_firma + yetkili_ad +yetkili_firma_vkn+ yetkili_tckn + yetkili_firma_il_kodu + yetki_gecerlilik_baslangic + yetki_gecerlilik_bitis + durum_bilgisi.replace('#','') + '\n')

    #bak bakalim bu turden bugün baska dosya olusmus mu

    #print lines

    #if lines !=[]:
    firma_kodu               = global_params.FIRMA_KODU
    file_sira_no = str( file_sira_no )
    if len( file_sira_no)%2 == 1:
        file_sira_no = '0' + file_sira_no
    now = datetime.now()
    year  = str(now.year)
    month = str(now.month)
    if len(month) == 1:
        month = '0' + month
    day   = str(now.day)
    if len(day) == 1:
        day = '0' + day
    dosya_adi  = '%s_%s_%s_%s_%s.txt'%( dosya_adi_header, tsm_firma_kodu, firma_kodu, year+month+day ,file_sira_no)#gunde bir dosya atacagiz. o yuzden sira no 1
    file_path = path + '/' + dosya_adi
    header    = 'H%s'%dosya_kodu + year+month+day + '%sI\n'%file_sira_no
    footer    = 'F%s'%len(lines)
    try:
        f = open( file_path, 'w')
        f.write(header)
        lines = map( lambda x:x.encode('utf8'),lines)
        f.writelines( lines )
        f.write( footer )
        f.close()
    except:
        return 0
    encoded = ""
    with open(file_path) as f:
        encoded = base64.b64encode(f.read())
    file_dict = dict (name  = dosya_adi,
                      file_ =  encoded,
                      tip   = tip,
                      model_name = model_name,
                      date       = year+'-'+month+'-'+day,
                      path       = file_path,
                      sira_no    = file_sira_no
                      )

    return file_dict
    '''
    else:
        return None
    '''
class IMPLICIT_FTP_TLS(FTP_TLS):
    def __init__(self, host='', user='', passwd='', acct='', keyfile=None,
        certfile=None, timeout=60):
        FTP_TLS.__init__(self, host, user, passwd, acct, keyfile, certfile, timeout)

    def connect(self, host='', port=0, timeout=-999):

        if host != '':
            self.host = host
        if port > 0:
            self.port = port
        if timeout != -999:
            self.timeout = timeout
        try:
            self.sock = socket.create_connection((self.host, self.port), self.timeout)
            self.af = self.sock.family
            self.sock = ssl.wrap_socket(self.sock, self.keyfile, self.certfile, ssl_version=ssl.PROTOCOL_TLSv1)
            self.file = self.sock.makefile('rb')
            self.welcome = self.getresp()
        except Exception as e:
            print (e)
        return self.welcome

    def ntransfercmd(self, cmd, rest=None):
        conn, size = FTP_TLS.ntransfercmd(self, cmd, rest)
        if self._prot_p:
            conn = ssl.wrap_socket(conn, self.keyfile, self.certfile)
        return conn, size
def list_files_from_tsm( config_params_obj ):
    messages = []
    if config_params_obj.params_tls:

        try:
            #ftps = FTP_TLS( timeout=30 )
            #messages.append (ftps.connect( str(config_params_obj.params_host), str(config_params_obj.params_port) , timeout = 30))
            #messages.append (ftps.set_pasv(0))
            #messages.append (ftps.login  ( str(config_params_obj.params_user_name), str(config_params_obj.params_password)))
            #messages.append (ftps.prot_p() )
            pass
        except:
            return messages
    else:

        ftps = FTP()
        messages.append (ftps.connect( str(config_params_obj.params_host), str(config_params_obj.params_port) ))
        messages.append (ftps.set_pasv(0))
        messages.append ( ftps.login  ( str(config_params_obj.params_user_name), str(config_params_obj.params_password)))

    except_list = []

    sub_messages = ['lftp','-c']
    try:
        if not config_params_obj.params_tls:
            pass
        else:
            plain_message = ''' open -e "set ftps:initial-prot ""; set ftp:ssl-force true; set ftp:ssl-protect-data true; set ssl:verify-certificate false; mirror  /OUT %s" -u "%s","%s" ftps://%s:%s '''
            plain_message = plain_message %(out_folder_path,
                                            config_params_obj.params_user_name,
                                            config_params_obj.params_password,
                                            config_params_obj.params_host,
                                            config_params_obj.params_port)

            sub_messages.append ( plain_message )
            #print sub_messages
            prod = subprocess.Popen( sub_messages,stdout = subprocess.PIPE, stderr = subprocess.PIPE  )
            out, err = prod.communicate()

            if err:
                except_list.append( err )
    except:
        except_list.append( 'Hata ')
    if  except_list == []:
        return 1
    else:
        except_list.extend( messages )
        return except_list
def file_received  ( file_, pool_pack ):
    file_type = None
    real_file = None

    try:
        file_name_= file_.split('/')[-1]
        file_name_splitted_l1 = file_name_.split('.')[0].split('_')
        #print file_name_splitted_l1
        file_type = file_name_keys.keys_dict[ file_name_splitted_l1[2].strip()]
        tarih     = file_name_splitted_l1[5]
        tarih_y   = tarih[:4]
        tarih_m   = tarih[4:6]
        tarih_d   = tarih[6:]
        tarih_t   = (tarih_y  , tarih_m, tarih_d )
        sira      = file_name_splitted_l1[6]
        #print sira
        if file_type not in file_name_keys.UY_list:
            #print sira, '%s-%s-%s'%( tarih_y,tarih_m,tarih_d),file_type
            resultset  = pool_pack.pool.get( 'info_tsm.gib_files' ).search( pool_pack.cr, pool_pack.uid, [( 'sira_no','=', sira ),
                                                                                          ('date','=','%s-%s-%s'%( tarih_y,tarih_m,tarih_d)),
                                                                                          ('model_name','=',file_type)])
            #print resultset
            if len( resultset ) > 0:
                file_id = resultset[0]
            else:
                #print ('Gelen Dosyanın Karşılığı Bulunamadı %s'.decode('latin5') % file_)
                file_id = -1
                raise Exception('No result Set')

        if not file_type:
            raise Exception('file_type is null')
    except :
        #print('Gelen Dosya İsim Formati Yanlis veya Karsiligi Yok %s'.decode('latin5') % file_)
        return

    file_o = open( file_ , 'r')
    lines = file_o.readlines()
    now = datetime.now()
    year  = str(now.year)
    month = str(now.month)
    if len(month) == 1:
        month = '0' + month
    day   = str(now.day)
    if len(day) == 1:
        day = '0' + day
    if file_type in file_name_keys.UY_list:
        tip               = 'Gib YNOKC Uyarı Dosyası'
    if file_type in file_name_keys.TC_list:
        tip = 'Gib Cevap Servis / Yetkili Kişi Listesi Dosyası'
    else:
        if file_type == file_name_keys.keys_dict[ 'Kayit' ]:
            tip = 'Gib Cevap Kayıt Listesi Dosyası'
        elif file_type == file_name_keys.keys_dict['Durum']:
            tip = 'Gib Cevap Aktivasyon Listesi Dosyası'
        elif file_type == file_name_keys.keys_dict['Sertifika']:
            tip = 'Gib Cevap Public Key Güncelleme Dosyası'
    encoded           = base64.b64encode(file_o.read())
    file_insert_dict  = dict( name       = file_name_,
                              file_      = encoded,
                              tip        = tip,
                              model_name = file_type,
                              day        =  year+'-'+month+'-'+day)

    pool_pack.pool.get('info_tsm.gib_files').create(pool_pack.cr, pool_pack.uid, file_insert_dict, context=pool_pack.context)


    if len(lines) >= 2:
        if  len(lines) > 2:
            lines     = lines[1:]
            if file_type not in file_name_keys.UY_list:
                result           =  pool_pack.pool.get( 'info_tsm.gib_files').browse(pool_pack.cr, pool_pack.uid, file_id, context=pool_pack.context )
                result[0].sended = -1
                hata_dict        = {}
                where_f          = None
                kirmii_id        = []
                for line in lines:
                    #line = line.decode('utf-8')
                    if line[0] == 'D':
                        line = line[1:]
                        hata_line_splitted = line.split('#')
                        if file_type in file_name_keys.OF_list:

                            hatali_satir_no = hata_line_splitted[0]
                            hatali_of_kodu  = hata_line_splitted[1]
                            hatali_of_kodu  = hatali_of_kodu[:2] + '00' + hatali_of_kodu[2:]
                            hatali_of_kodu  = hatali_of_kodu.strip()
                            hata_kodu       = hata_line_splitted[2]
                            hata_aciklama   = hata_line_splitted[3].strip()

                            where_f          =  [( 'dosya_satir_no','=', hatali_satir_no ),
                                                 ('kayit_dosyasi','=',file_id)]


                        if where_f:

                            line_to_update_ids = pool_pack.pool.get( file_type ).search(pool_pack.cr, pool_pack.uid, where_f )
                            line_to_update     = pool_pack.pool.get( file_type ).browse( pool_pack.cr, pool_pack.uid, line_to_update_ids, context = pool_pack.context)
                            if len(line_to_update) > 0 :
                                line_to_update               = line_to_update[0]
                                line_to_update.source        = 'ftp'
                                line_to_update.durum         = -1
                                line_to_update.hata_kodu     = hata_kodu
                                line_to_update.hata_aciklama = hata_aciklama
                                kirmii_id.append( line_to_update.id )
                            else:
                                print (" Dosya Satırları Parse Edilemedi file = %s INNER"  % file_ )

                        else:
                            print (" Dosya Satırları Parse Edilemedi file = %s OUTHER" % file_ )

                    if kirmii_id:
                        yesiller     = pool_pack.pool.get( file_type ).search( pool_pack.cr, pool_pack.uid, [('id','not in', kirmii_id),('kayit_dosyasi','=',file_id)])
                        yesiller     = pool_pack.pool.get( file_type ).browse( pool_pack.cr, pool_pack.uid, yesiller, context = pool_pack.context)
                        for yesil in yesiller:
                            yesil.source        = 'ftp'
                            yesil.durum         = 2
                            yesil.hata_kodu     = '---DUZELTILMIS HATA--- %s'.encode('utf-8') %yesil.hata_kodu
                            yesil.hata_aciklama = '---DUZELTILMIS HATA--- %s'.encode('utf-8') %yesil.hata_aciklama

                    #print  'sorunlu kayit var'
            else:
                #uyari dosyasi gelmiş, parse edecez.
                #print 'Uyari Dosyasi'
                for line in lines:
                    if line[0] == 'D':
                        line = line[1:]
                        uyari_line_splitted = line.split('#')

                        tsm_kodu      = uyari_line_splitted[0].strip()
                        of_kodu       = uyari_line_splitted[1].strip()
                        tarih         = uyari_line_splitted[2].strip()
                        onem          = uyari_line_splitted[3].strip()
                        kategori      = uyari_line_splitted[4].strip()
                        alt_kategori  = uyari_line_splitted[5].strip()
                        kaynak        = uyari_line_splitted[6].strip()
                        aciklama      = uyari_line_splitted[7].strip()
                        aksiyon       = uyari_line_splitted[9].strip()

                        ##print onem, kategori , alt_kategori, kaynak , aciklama, aksiyon
                        lot_id = pool_pack.pool.get('stock.production.lot').search( pool_pack.cr, pool_pack.uid, [( 'name','=', of_kodu )])

                        if len( lot_id ) > 0:
                                lot_id         = lot_id[0]
                                line_          = dict (name             = lot_id,
                                                       tarih            = tarih[:4] + '-' + tarih[4:6] + '-' + tarih[6:],
                                                       onem_derecesi    = onem,
                                                       kategori         = kategori,
                                                       alt_kategori     = alt_kategori,
                                                       kaynak           = kaynak,
                                                       aciklama         = aciklama,
                                                       aksiyon          = aksiyon,
                                                       kayit_dosyasi    = file_id
                                                      )
                                pool_pack.pool.get(file_type).create ( pool_pack.cr, pool_pack.uid, line_ , context = pool_pack.context)

                        else:
                            logging.error(" Dosya Satırları Parse Edilemedi file = %s " .decode('latin5') % file_ )

        else:
            if file_type not in file_name_keys.UY_list:
                resultset  = pool_pack.pool.get( 'info_tsm.gib_files' ).search( pool_pack.cr, pool_pack.uid, [( 'sira_no','=', sira ),
                                                                                          ('date','=','%s-%s-%s'%( tarih_y,tarih_m,tarih_d)),
                                                                                          ('model_name','=',file_type)])

                if len( resultset ) > 0:
                    file_ = pool_pack.pool.get( 'info_tsm.gib_files' ).browse( pool_pack.cr, pool_pack.uid, resultset, context=pool_pack.context )
                    file_.sended = 2

                    lines   = pool_pack.pool.get( file_type ).search( pool_pack.cr, pool_pack.uid, [( 'kayit_dosyasi','=', file_.id )])
                    lines   = pool_pack.pool.get( file_type ).browse( pool_pack.cr, pool_pack.uid, lines, context=pool_pack.context )
                    for l in lines:
                        l.durum = 2
                        l.hata_kodu     = u'---DÜZELTİLMİŞ HATA--- %s' %l.hata_kodu
                        l.hata_aciklama = u'---DÜZELTİLMİŞ HATA--- %s' %l.hata_aciklama

                #print 'tum kayitlar ok'
    else:
        logging.error('Gelen Dosya İçerik Formati Yanlis %s'.decode('latin5') % file_)
        #print 'dosya bozuk'
def get_file_parse ( saved_files_list, pool_pack ):
    saved_name_list = map(lambda x:x.name, saved_files_list )

    for eachfile in glob.iglob(out_folder_path + '/*'):
        for e in glob.iglob (eachfile + '/*'):
            eachfile_name = e.split('/')[-1]
            #print e
            if eachfile_name not in saved_files_list :
                file_received ( e , pool_pack )
def send_files_to_tsm( config_params_obj,file_records_list=[] ):
    #sub threadde mi çalıştırsak ki??
    ##print config_params_obj.params_tls, config_params_obj.params_host, config_params_obj.params_port, config_params_obj.params_user_name, config_params_obj.params_password
    messages = []
    '''
    if config_params_obj.params_tls:

        try:
            #ftps = FTP_TLS( timeout=30 )
            #messages.append (ftps.connect( str(config_params_obj.params_host), str(config_params_obj.params_port) , timeout = 30))
            #messages.append (ftps.set_pasv(0))
            #messages.append (ftps.login  ( str(config_params_obj.params_user_name), str(config_params_obj.params_password)))
            #messages.append (ftps.prot_p() )
            pass
        except:
            return messages
    else:

        ftps = FTP()
        messages.append (ftps.connect( str(config_params_obj.params_host), str(config_params_obj.params_port) ))
        messages.append (ftps.set_pasv(0))
        messages.append ( ftps.login  ( str(config_params_obj.params_user_name), str(config_params_obj.params_password)))
    '''
    except_list = []
    for file_obj in file_records_list:
        sub_messages = ['lftp','-c']
        #try:
        path =file_obj.path
        if os.path.exists( path ):
            #if not config_params_obj.params_tls:
            #    messages.append (ftps.storbinary('STOR %s'%file_obj.name, open(f_name,'rb') ,1024 ))
            #else:
            plain_message = ''' open -e "set ftps:initial-prot ""; set ftp:ssl-force true; set ftp:ssl-protect-data true; set ssl:verify-certificate false; put -O IN/ %s;" -u "%s","%s" ftps://%s:%s '''
            plain_message = plain_message %(
                                            path,
                                            config_params_obj.params_user_name,
                                            config_params_obj.params_password,
                                            config_params_obj.params_host,
                                            config_params_obj.params_port)
            sub_messages.append ( plain_message )
            print sub_messages
            prod = subprocess.Popen( sub_messages,stdout = subprocess.PIPE, stderr = subprocess.PIPE  )
            out, err = prod.communicate()
            #print sub_messages
            if err:
                messages.append( err )
                except_list.append( file_obj.id )
            #except:
            #    except_list.append( file_obj.id )

    if except_list == []:
        return 1
    else:
        except_list .extend( messages )
        #print except_list
        return except_list
