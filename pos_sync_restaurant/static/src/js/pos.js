odoo.define('pos_sync_restaurant', function(require){
    var module = require('point_of_sale.models');
    var models = module.PosModel.prototype.models;
    var screens = require('point_of_sale.screens');
    var gui = require('point_of_sale.gui');
    var floors = require('pos_restaurant.floors');

    for(var i=0; i<models.length; i++){
        var model=models[i];
        if(model.model === 'restaurant.floor'){
            model.domain = function(self)
            { 
                return [['pos_config_id','in',self.config.id]];
            }
        }
    }
    gui.Gui.prototype.screen_classes.filter(function(el) {
        return el.name === 'splitbill';
    })[0].widget.include({
        pay: function(order,neworder,splitlines){
            this._super(order,neworder,splitlines);
            this.pos.sync_session.send({'action':'update_order','order':neworder.export_as_JSON()});
        }
    });
    var PosModelSuper = module.PosModel;
    module.PosModel = module.PosModel.extend({

        update_the_order: function(sync_order){
            var self = this;
            var order = PosModelSuper.prototype.update_the_order.apply(this, arguments);
            if (sync_order.table_id) {
                order.table = self.tables_by_id[sync_order.table_id];
                order.customer_count = sync_order.customer_count;
                order.computeChanges();
                if(this.gui.screen_instances){
                   if(this.gui.screen_instances['floors'] != undefined){
                    this.gui.screen_instances['floors'].replace();
                    this.gui.screen_instances.products.action_buttons.guests.renderElement();
                // this.gui.screen_instances.products.action_buttons.submit_order.renderElement();
            }
            }
        }
            return order;
        },
        set_table: function(table) {
            var self = this;
            if (table && this.order_to_transfer_to_different_table) {
                this.order_to_transfer_to_different_table.table = table;
                setTimeout(function(){
                    self.sync_session.send({'action':'update_order','order':self.get_order().export_as_JSON()});
                }, 100);
                this.order_to_transfer_to_different_table = null;
                this.set_table(table);
            } else {
                PosModelSuper.prototype.set_table.apply(this, arguments);
            }
        }
    });
    var OrderSuper = module.Order;
    module.Order = module.Order.extend({
            set_customer_count: function (count) {
                var self = this;
                OrderSuper.prototype.set_customer_count.apply(this, arguments);
                setTimeout(function(){
                    self.pos.sync_session.send({'action':'update_order','order':self.export_as_JSON()});
                }, 1000);
                
            },
            hasSkippedChanges: function() {
                if(this.orderlines){
                    var orderlines = this.get_orderlines();
                    if(orderlines != null){
                        for (var i = 0; i < orderlines.length; i++) {
                            if (orderlines[i].mp_skip) {
                                return true;
                            }
                        }
                    }
                }
                return false;
            },
        });

    module.Orderline = module.Orderline.extend({
        get_line_diff_hash: function(){
            if(this.extra_id != undefined && this.get_note()){
                return this.extra_id + '|' + this.get_note();
            }
            else if(this.extra_id != undefined){
                return '' + this.extra_id;
            }
            else if (this.get_note()) {
                return this.id + '|' + this.get_note();
            } else {
                return '' + this.id;
            }
        },
    });

    var OrderlineSuper = module.Orderline;
    module.Orderline = module.Orderline.extend({
        get_line_diff_hash: function(){
            var res = OrderlineSuper.prototype.get_line_diff_hash.apply(this, arguments);
            res = res.split('|');
            res[0] = this.uid;
            res = res.join('|');
            return res;
        },
    });
    screens.OrderWidget.include({
        update_summary: function(){
            var order = this.pos.get_order();
            if (!order){
                return;
            }
            this._super();
        },
        remove_orderline: function(order_line){
            if (this.pos.get_order() && this.pos.get_order().get_orderlines().length === 0){
                this._super(order_line);
            }
            else {
                if(order_line.node != undefined){
                    order_line.node.parentNode.removeChild(order_line.node);
                }
            }
        }
    });
});
