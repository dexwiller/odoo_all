# -*- coding: utf-8 -*-
from openerp import models, fields, api, exceptions, SUPERUSER_ID
from datetime import datetime
from parameter_models import birim_tipleri,fonk_tus1,fonk_tus2
from pysimplesoap.client import SoapClient, SimpleXMLElement
import xml.etree.ElementTree as ET


altin_kod_map = {
	'B'  : 601,
	'18' : 602,
	'14' : 603,
	'C'  : 604,
	'Y'  : 605,
	'T'  : 606,
	'G'  : 607,
	'A'  : 608,
	'R'  : 609,
	'H'  : 610,
	'GA' : 611,
	'EC' : 612,
	'EY' : 613,
	'ET' : 614,
	'EG' : 615
}
class ozel_kur_group_rel (models.Model):

    _name = 'info_tsm.ozel_kur_group_rel'
    def _get_next_sq_num ( self ):
        added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_kur'])
        return len( added_context ) + 1

    button_no           = fields.Integer(string='Ökc Buton No', default = _get_next_sq_num)
    ozel_kur_rel        = fields.Many2one('info_tsm.kur_bilgileri', string='Özel Kur Oranları', required=True, )
    grup_id             = fields.Many2one('info_tsm.okc_urun_grup', string='Ökc Ürün Grup')
    kur_cevrimi         = fields.Float( string = "Kur Çevrimi", size=6, digits=(2,4))
    webden_guncelle     = fields.Boolean( string = "Web'den Anlık Güncelle", default=True)
    source              = fields.Integer(string="Kaynak")

    @api.onchange( 'ozel_kur_rel')
    @api.multi
    def ozel_urun_rel_onchange(self):
        print self.grup_id.name
        self.ensure_one()
        added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_kur'])

        dynamic_name_list = []
        #print self._context['pack_kur']
        for eklenmis in added_context:
            if eklenmis [2]:
                dynamic_name_list.append( self.env['info_tsm.kur_bilgileri'].browse( eklenmis[2]['ozel_kur_rel'] )[0].name )
            else:
                dynamic_name_list.append (self.env['info_tsm.ozel_kur_group_rel'].browse( eklenmis[1] )[0].ozel_kur_rel.name)


        domain =   {'ozel_kur_rel':[('name','not in',dynamic_name_list),('gib_test','=',0)]}
        res = {'domain':domain}
        #onceki kaydi bulalım
        prev_ozel_kur_rel = False
        if self._context.get('grup_id'):
            db_ozel_kur_rel_by_group              = self.env['info_tsm.ozel_kur_group_rel'].search( [('grup_id','=',self._context['grup_id'])] )
            db_ozel_kur_rel                       = filter( lambda x:x if x.id == self._origin.id else None, db_ozel_kur_rel_by_group)
    
            if db_ozel_kur_rel and db_ozel_kur_rel[0].ozel_kur_rel:
                prev_ozel_kur_rel = db_ozel_kur_rel[0].ozel_kur_rel.id
    
            
            if self.ozel_kur_rel.name in map(lambda x:x.ozel_kur_rel.name,db_ozel_kur_rel_by_group):
                self.ozel_kur_rel = prev_ozel_kur_rel
                res = {'ozel_kur_rel': prev_ozel_kur_rel,
                        'warning': {
                            'title': 'Zaten Eklenmiş',
                            'message': u'Değiştirilmek İstenen Kayıt Cihazda Zaten Mevcuttur. '},
                        'domain':domain
    
                    }
        return res

    @api.onchange('button_no')
    def button_no_onchange( self ):

        dynamic_name_list = []

        added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_kur'])
        for eklenmis in added_context:
            if eklenmis [2]:
                dynamic_name_list.append( self.env['info_tsm.kur_bilgileri'].browse( eklenmis[2]['ozel_kur_rel'] )[0].name )
            else:
                dynamic_name_list.append (self.env['info_tsm.ozel_kur_group_rel'].browse( eklenmis[1] )[0].ozel_kur_rel.name)


        return dict( domain = {'ozel_kur_rel':[('name','not in',dynamic_name_list),('gib_test','=',0)]})
    
    @api.multi
    def unlink(self ):
        
        for s in self:
            if s.grup_id.sudo().firma.taxNumber == '1111111111' and self.env.user.id != s.create_uid.id:
                raise exceptions.ValidationError('Test Modundan Kayıt Silemezsiniz. ')
        
        return super(ozel_kur_group_rel, self ).unlink()
    
    
    @api.model
    def process_wsdl_scheduler_queue(self):

        try:
            namespace = "http://data.altinkaynak.com/"
            ns        = "data"
            client = SoapClient(wsdl="http://data.altinkaynak.com/DataService.asmx?WSDL",trace=False,namespace='uhttp://data.altinkaynak.com/',soap_ns='soapenv')
            header = SimpleXMLElement('<Headers/>', namespace=namespace, prefix=ns)
            authheader = header.add_child("AuthHeader")
            authheader['xmlns:'+ns] = namespace
            authheader.marshall('data:Username','AltinkaynakWebServis',ns=ns)
            authheader.marshall('data:Password','AltinkaynakWebServis',ns=ns)
            client['AuthHeader'] = authheader

        except:
            raise exceptions.ValidationError(u'ALTINKAYNAK Servis Hatası  Hata: Servis istemcisi yaratılamadı')

        def filterbyvalue(seq, value):
            for el in seq:
                ##print el.ozel_kur_rel.para_birimi_kisaltmasi, value
                if str(el.ozel_kur_rel.para_birimi_kisaltmasi).strip()== str(value).strip():
                    ##print el
                    yield el

        def filterbyvalueAltin(seq, value):
            for el in seq:
                ##print el.ozel_kur_rel.para_birimi_kisaltmasi, value
                if str(el.ozel_altin_rel.kod).strip()== str(value).strip():
                    ##print el
                    yield el

        time_request = datetime.today()
        response_currency = client.GetCurrency()
        response_gold     = client.GetGold()
        time_response = datetime.today()
        ##print response_currency
        ##print response_gold
        result_currency =  ET.fromstring(response_currency['GetCurrencyResult'].encode('utf-8'))
        result_gold     =  ET.fromstring(response_gold['GetGoldResult'].encode('utf-8'))

        guncellenecek_kurlar = self.env['info_tsm.ozel_kur_group_rel'].search([('webden_guncelle', '=', True)])
        rate_obj   = self.env.get('res.currency.rate')
        cur_obj    = self.env.get('res.currency')
        for res_cur in result_currency:
            para_birimi_kod      =  res_cur[0].text
            '''
            #print res_cur[0].text
            #print res_cur[1].text
            #print res_cur[2].text
            #print res_cur[3].text
            #print res_cur[4].text
            '''

            satis_fiyati         =  res_cur[3].text
            update_generator     =  filterbyvalue( guncellenecek_kurlar, para_birimi_kod )
            for el in update_generator:
                if not el.grup_id.kur_altin_update_service or el.grup_id.kur_altin_update_service == 'altinkaynak':
                    el.kur_cevrimi = float (satis_fiyati)
                else:
                    service    = el.grup_id.kur_altin_update_service
                    cur_id     = cur_obj.sudo().search([('name','=',para_birimi_kod)])
                    if len(cur_id)  > 0:
                        cur_id = cur_id[0]
                    else:
                        #print 'Güncelleme Başarısız. Para Birimi Eşleşmedi'
                        continue

                    last_rate = rate_obj.sudo().search([('currency_id','=',cur_id),
                                                          ('source','=',int(service) )],limit=1,order='id desc')

                    el.kur_cevrimi = 1.0 / float( last_rate.rate )
                    #print el.kur_cevrimi

        guncellenecek_altin = self.env['info_tsm.ozel_altin_group_rel'].search([('webden_guncelle', '=', True)])

        for res_gold in result_gold:
            if altin_kod_map.has_key(res_gold[0].text):
                altin_kodu           =  altin_kod_map [res_gold[0].text]
                alis_fiyati          =  res_gold[2].text
                satis_fiyati         =  res_gold[3].text

                update_generator     =  filterbyvalueAltin( guncellenecek_altin, altin_kodu )
                for el in update_generator:
                    el.alis_fiyati = alis_fiyati
                    el.satis_fiyati = satis_fiyati

class ozel_altin_group_rel (models.Model):

    def _get_next_sq_num ( self ):
        added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_altin'])
        return len( added_context ) + 1

    _name = 'info_tsm.ozel_altin_group_rel'

    button_no        = fields.Integer(string='Ökc Buton No', default = _get_next_sq_num)
    ozel_altin_rel  = fields.Many2one('info_tsm.altin_kodlari', string='Altın Çeşidi', required=True,)
    grup_id         = fields.Many2one('info_tsm.okc_urun_grup', string='Ökc Ürün Grup')
    alis_fiyati     = fields.Float( string = "Alış Fiyatı", size=6, digits=(2,4))
    satis_fiyati    = fields.Float( string = "Satış Fiyatı", size=6, digits=(2,4))
    webden_guncelle = fields.Boolean( string = "Web'den Anlık Güncelle",default=True)
    source          = fields.Integer(string="Kaynak")
    
    @api.onchange( 'ozel_altin_rel')
    @api.multi
    def ozel_urun_rel_onchange(self):
        self.ensure_one()
        #onceki kaydi bulalım
        added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_altin'])
        dynamic_id_list = []

        for eklenmis in added_context:
            if eklenmis [2]:
                dynamic_id_list.append( self.env['info_tsm.altin_kodlari'].browse( eklenmis[2]['ozel_altin_rel'] )[0].id )
            else:
                dynamic_id_list.append (self.env['info_tsm.ozel_altin_group_rel'].browse( eklenmis[1] )[0].ozel_altin_rel.id)


        domain = {'ozel_altin_rel':[('id','not in',dynamic_id_list)]}
        res = {'domain':domain}
        
        if self._context.get('grup_id'):
            prev_ozel_altin_rel = False
            db_ozel_altin_rel_by_group              = self.env['info_tsm.ozel_altin_group_rel'].search( [('grup_id','=',self._context['grup_id'])] )
            db_ozel_altin_rel                       = filter( lambda x:x if x.id == self._origin.id else None, db_ozel_altin_rel_by_group)

            if db_ozel_altin_rel and db_ozel_altin_rel[0].ozel_altin_rel:
                prev_ozel_altin_rel = db_ozel_altin_rel[0].ozel_altin_rel.id

        

            if self.ozel_altin_rel.name in map(lambda x:x.ozel_altin_rel.name,db_ozel_altin_rel_by_group):
                self.ozel_altin_rel = prev_ozel_altin_rel
                res = {'ozel_altin_rel': prev_ozel_altin_rel,
                        'warning': {
                        'title': 'Zaten Eklenmiş',
                        'message': u'Değiştirilmek İstenen Kayıt Cihazda Zaten Mevcuttur. '},
                        'domain':domain
                    }
        return res
    @api.multi
    def unlink(self ):
        
        for s in self:
            if s.grup_id.sudo().firma.taxNumber == '1111111111' and self.env.user.id != s.create_uid.id:
                raise exceptions.ValidationError('Test Modundan Kayıt Silemezsiniz. ')
        
        return super(ozel_altin_group_rel, self ).unlink()
    
    @api.onchange('button_no')
    def button_no_onchange( self ):
        added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_altin'])
        dynamic_id_list = []
        #print self._context['pack_altin']
        for eklenmis in added_context:
            if eklenmis [2]:
                dynamic_id_list.append( self.env['info_tsm.altin_kodlari'].browse( eklenmis[2]['ozel_altin_rel'] )[0].id )
            else:
                dynamic_id_list.append (self.env['info_tsm.ozel_altin_group_rel'].browse( eklenmis[1] )[0].ozel_altin_rel.id)


        return dict( domain = {'ozel_altin_rel':[('id','not in',dynamic_id_list)]})

class ozel_kdv_group_rel (models.Model):

    _name = 'info_tsm.ozel_kdv_group_rel'
    def _get_next_sq_num ( self ):
        if self._context.get('pack_kdv'):
            added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_kdv'])
            return len( added_context ) + 1
        return 1
    _order           = 'button_no_v asc'
    ozel_kdv_rel     = fields.Many2one('info_tsm.ozel_kdv_tablosu', string='Özel KDV Grubu', required=True)
    grup_id          = fields.Many2one('info_tsm.okc_urun_grup', string='Ökc Ürün Grup')
    button_no        = fields.Integer(string='Ökc Buton No', required=True,default = _get_next_sq_num)
    button_no_v      = fields.Integer(string='Ökc Buton No', default = _get_next_sq_num)
    max_satis_limiti = fields.Float( string='Satış Tutar Limiti', size=6,digits=(4,2))
    birim_tipi       = fields.Selection(related='ozel_kdv_rel.unit_type')
    
    @api.onchange('button_no_v')
    def seauence_onchange(self):
        print 'Sequence Onchange Event....'
    
    @api.onchange( 'ozel_kdv_rel')
    @api.multi
    def ozel_urun_rel_onchange(self):
        self.ensure_one()
        #onceki kaydi bulalım
        dynamic_id_list = []
        added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_kdv'])

        for eklenmis in added_context:
            if eklenmis [2]:
                dynamic_id_list.append( eklenmis[2]['ozel_kdv_rel'])
            else:
                dynamic_id_list.append (self.env['info_tsm.ozel_kdv_group_rel'].browse( eklenmis[1] )[0].ozel_kdv_rel.id)


        domain = {'ozel_kdv_rel':[('id','not in',dynamic_id_list)]}
        res = {'domain':domain}
        
        prev_ozel_kdv_rel = False
        if self._context.get('grup_id'):
            db_ozel_kdv_rel_by_group              = self.env['info_tsm.ozel_kdv_group_rel'].search( [('grup_id','=',self._context['grup_id'])] )
            print db_ozel_kdv_rel_by_group
            db_ozel_kdv_rel                       = filter( lambda x:x if x.id == self._origin.id else None, db_ozel_kdv_rel_by_group)

            if db_ozel_kdv_rel and db_ozel_kdv_rel[0].ozel_kdv_rel:
                prev_ozel_kdv_rel = db_ozel_kdv_rel[0].ozel_kdv_rel.id

            if self.ozel_kdv_rel.name in map(lambda x:x.ozel_kdv_rel.name,db_ozel_kdv_rel_by_group):
                self.ozel_kdv_rel = prev_ozel_kdv_rel
                res = {'ozel_kdv_rel': prev_ozel_kdv_rel,
                       'warning': {
                            'title': 'Zaten Eklenmiş',
                            'message': u'Değiştirilmek İstenen Kayıt Cihazda Zaten Mevcuttur. '},
                        'domain':domain
                    }
            
            print res
        return res


    @api.onchange('button_no')
    def button_no_onchange( self ):
        dynamic_id_list = []
        added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_kdv'])
        if len(added_context) > 11:
            raise exceptions.ValidationError(u"En Fazla 12 Tane Özel KDV Eklenebilir.")

        for eklenmis in added_context:
            #print eklenmis
            if eklenmis [2]:
                dynamic_id_list.append( eklenmis[2]['ozel_kdv_rel'])
            else:
                dynamic_id_list.append (self.env['info_tsm.ozel_kdv_group_rel'].browse( eklenmis[1] )[0].ozel_kdv_rel.id)
        return dict( domain = {'ozel_kdv_rel':[('id','not in',dynamic_id_list)]})
    @api.model
    def create( self, vals):
        #vals['button_no_v'] = vals.get('button_no')
        if vals.get('button_no_v'): vals['button_no'] = vals.get('button_no_v')
        return super (ozel_kdv_group_rel,self).create( vals )
        
    @api.multi
    def write(self,vals):
        if vals.get('button_no_v'): vals['button_no'] = vals.get('button_no_v')
        print vals
        for s in self:
            if vals.get('ozel_kdv_rel') and int(vals.get('ozel_kdv_rel')) != s.ozel_kdv_rel.id:
                
                rfere_urun_grup = self.env['info_tsm.ozel_urun_grup_group_rel'].search([('grup_id','=',s.grup_id.id),
                                                                                    ('ozel_kdv_rel','=',s.ozel_kdv_rel.id),
                                                                                    ('sil_ekle_disabled','=',False)])
                print 'write : ',rfere_urun_grup, s.grup_id
                if len(rfere_urun_grup) > 0:
                    raise exceptions.ValidationError('Bu KDV Departmanını Kullanan Ürün Grubu Bulunmakta, Lütfen Önce Ürün Gruplarını Düzenleyiniz.')
        res = super(ozel_kdv_group_rel,self).write( vals)
        return res 
    
    @api.multi
    def unlink(self):
        for s in self:
            if s.grup_id.sudo().firma.taxNumber == '1111111111' and self.env.user.id != s.create_uid.id:
                raise exceptions.ValidationError('Test Modundan Kayıt Silemezsiniz. ')
            silinen_dict = {'grup_id':s.grup_id.id,
                            'ozel_kdv_rel':s.ozel_kdv_rel.id}
            self.env['info_tsm.silinen_kisimlar'].create( silinen_dict )
            
            rfere_urun_grup = self.env['info_tsm.ozel_urun_grup_group_rel'].search([('grup_id','=',s.grup_id.id),
                                                                                    ('ozel_kdv_rel','=',s.ozel_kdv_rel.id),
                                                                                    ('sil_ekle_disabled','=',False)])
            
            print 'unlink: ',rfere_urun_grup , s.grup_id
            
            if len(rfere_urun_grup) > 0:
                raise exceptions.ValidationError('Bu KDV Departmanını Kullanan Ürün Grubu Bulunmakta, Lütfen Önce Ürün Gruplarını Düzenleyiniz.')
            
        return super( ozel_kdv_group_rel,self).unlink()

    _sql_constraints = [
    ('rel_unique', 'unique(grup_id, ozel_kdv_rel)', 'Bu Grubun İlgili Butonuna Zaten KDV Eklenmiş'),
    ]

class ozel_urun_grup_group_rel (models.Model):

    _name = 'info_tsm.ozel_urun_grup_group_rel'


    def _get_next_sq_num ( self ):
        added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_ozel_urun'])
        return len( added_context ) + 1

    ozel_urun_rel  = fields.Many2one('info_tsm.okc_ozel_urun_grubu_tablosu', string='Özel Ürün Grubu', required=True,domain=[('name','!=','Diğer')] )
    grup_id        = fields.Many2one('info_tsm.okc_urun_grup', string='Ökc Ürün Grup')
    button_no      = fields.Integer(string='Ökc Grup Id',  default=_get_next_sq_num)
    ozel_kdv_rel   = fields.Many2one('info_tsm.ozel_kdv_tablosu', string='Özel KDV Grubu (Kısım)', required=True)
    change         = fields.Boolean (string='Değişti mi')
    state          = fields.Integer(string='Silinme Durumu')
    color          = fields.Integer()
    sil_ekle_disabled  = fields.Boolean('Silinebilir/Eklenebilir')
    
    @api.multi
    def unlink(self ):
        
        for s in self:
            self.search([('grup_id','=',s.grup_id.id),('change','=',False)]).write({'change':True})
            if s.grup_id.sudo().firma.taxNumber == '1111111111' and self.env.user.id != s.create_uid.id:
                raise exceptions.ValidationError('Test Modundan Kayıt Silemezsiniz. ')
            
            urun_rels   = self.env['info_tsm.urun_grup_group_rel'].search([('grup_id','=',s.grup_id.id),('urun_grup_id','=',s.ozel_urun_rel.id)])
            diger_grubu = self.env['info_tsm.ozel_urun_grup_group_rel'].search([('grup_id','=',s.grup_id.id),('sil_ekle_disabled','=',True)])

            for urun in urun_rels:

                urun.prev_urun_grup_id = s.ozel_urun_rel.id
                urun.urun_grup_id      = diger_grubu.ozel_urun_rel.id
                urun.change            = True

        return super(ozel_urun_grup_group_rel, self ).unlink()
    
    @api.onchange( 'ozel_urun_rel')
    @api.multi
    def ozel_urun_rel_onchange(self):
        self.ensure_one()
        #onceki kaydi bulalım
        dynamic_id_list         = []
        dynamic_id_kisim_list   = []
        added_context_kisim= filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_kdv'])

        for kisim in added_context_kisim:
            if kisim[2]:
                dynamic_id_kisim_list.append( kisim[2]['ozel_kdv_rel'])
            else:
                dynamic_id_kisim_list.append (self.env['info_tsm.ozel_kdv_group_rel'].browse( kisim[1] )[0].ozel_kdv_rel.id)

        added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_ozel_urun'])
        
        if len(added_context) > 18:
            raise exceptions.ValidationError(u"En Fazla 18 Tane Özel Grup Eklenebilir.")
        
        
        for eklenmis in added_context:
            
            if eklenmis [2]:
                dynamic_id_list.append( eklenmis[2]['ozel_urun_rel'])
            else:
                dynamic_id_list.append (self.env['info_tsm.ozel_urun_grup_group_rel'].browse( eklenmis[1] )[0].ozel_urun_rel.id)
        domain = {'ozel_urun_rel':[('id','not in',dynamic_id_list),('name','!=','Diğer')],
                  'ozel_kdv_rel':[('id','in',dynamic_id_kisim_list)]}

        res = {'domain':domain}
        prev_ozel_urun_rel = False
        if self._context.get('grup_id'):
            db_ozel_urun_rel_by_group             = self.env['info_tsm.ozel_urun_grup_group_rel'].search( [('grup_id','=',self._context['grup_id'])] )
            db_ozel_urun_rel                      = filter( lambda x:x if x.id == self._origin.id else None, db_ozel_urun_rel_by_group)
    
            if db_ozel_urun_rel and db_ozel_urun_rel[0].ozel_urun_rel:
                prev_ozel_urun_rel = db_ozel_urun_rel[0].ozel_urun_rel.id
    
            if self.ozel_urun_rel.id in map(lambda x:x.ozel_urun_rel.id,db_ozel_urun_rel_by_group):
                self.ozel_urun_rel = prev_ozel_urun_rel
                res = {'ozel_urun_rel': prev_ozel_urun_rel,
                           'warning': {
                            'title': 'Zaten Eklenmiş',
                            'message': u'Değiştirilmek İstenen Kayıt Cihazda Zaten Mevcuttur. '},
                        'domain':domain
                    }
        return res

    @api.onchange( 'ozel_kdv_rel')
    @api.multi
    def ozel_urun_grup_id_onchange(self):
        self.ensure_one()
        #onceki kaydi bulalım
        prev_urun_rel = False
        db_urun_rel_by_group              = self.env['info_tsm.ozel_kdv_group_rel'].search( [('grup_id','=',self._context['grup_id'])] )
        db_urun_rel                       = filter( lambda x:x if x.id == self._origin.id else None, db_urun_rel_by_group)

        if db_urun_rel and db_urun_rel[0].ozel_kdv_rel:
            prev_urun_rel = db_urun_rel[0].ozel_kdv_rel.id

        dynamic_id_list         = []
        dynamic_id_kisim_list   = []
        added_context_kisim= filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_kdv'])

        for kisim in added_context_kisim:
            if kisim[2]:
                dynamic_id_kisim_list.append( kisim[2]['ozel_kdv_rel'])
            else:
                dynamic_id_kisim_list.append (self.env['info_tsm.ozel_kdv_group_rel'].browse( kisim[1] )[0].ozel_kdv_rel.id)

        added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_ozel_urun'])

        for eklenmis in added_context:
            if eklenmis [2]:
                dynamic_id_list.append( eklenmis[2]['ozel_urun_rel'])
            else:
                dynamic_id_list.append (self.env['info_tsm.ozel_urun_grup_group_rel'].browse( eklenmis[1] )[0].ozel_urun_rel.id)
        domain = {'ozel_urun_rel':[('id','not in',dynamic_id_list),('name','!=','Diğer')],
                  'ozel_kdv_rel':[('id','in',dynamic_id_kisim_list)]}

        res = {'domain':domain}

        if self.ozel_kdv_rel and self.ozel_kdv_rel.id not in dynamic_id_kisim_list:
            self.ozel_kdv_rel = prev_urun_rel
            res = {'urun_grup_id': prev_urun_rel,
                       'warning': {
                        'title': 'Zaten Eklenmiş',
                        'message': u'Değiştirilmek İstenen Kayıt Cihazda Mevcut Değildir. Lütfen Ekleyiniz.. '},
                       'domain':domain
                }
        return res

    @api.onchange('button_no')
    def button_no_onchange( self ):
        #print 'domain' , self._context
        dynamic_id_kisim_list   = []
        added_context_kisim= filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_kdv'])

        for kisim in added_context_kisim:
            if kisim[2]:
                dynamic_id_kisim_list.append( kisim[2]['ozel_kdv_rel'])
            else:
                dynamic_id_kisim_list.append (self.env['info_tsm.ozel_kdv_group_rel'].browse( kisim[1] )[0].ozel_kdv_rel.id)

        added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_ozel_urun'])
        dynamic_id_list = []
        for eklenmis in added_context:
            if eklenmis [2]:
                dynamic_id_list.append( eklenmis[2]['ozel_urun_rel'])
            else:
                dynamic_id_list.append (self.env['info_tsm.ozel_urun_grup_group_rel'].browse( eklenmis[1] )[0].ozel_urun_rel.id)
        return dict( domain = {'ozel_urun_rel':[('id','not in',dynamic_id_list),('name','!=','Diğer')],
                               'ozel_kdv_rel':[('id','in',dynamic_id_kisim_list)]})

    @api.multi
    def disable(self):

        for id_ in self:
            urun_rels   = self.env['info_tsm.urun_grup_group_rel'].search([('grup_id','=',id_.grup_id.id),('urun_grup_id','=',id_.ozel_urun_rel.id)])
            diger_grubu = self.env['info_tsm.ozel_urun_grup_group_rel'].search([('grup_id','=',id_.grup_id.id),('sil_ekle_disabled','=',True)])

            for urun in urun_rels:

                urun.prev_urun_grup_id = id_.ozel_urun_rel.id
                urun.urun_grup_id = diger_grubu.ozel_urun_rel.id
                urun.change = True
            id_.state = 1
            id_.change = True
            
            
        return True
    @api.multi
    def enable(self ):
        for id_ in self:
            urun_rels = self.env['info_tsm.urun_grup_group_rel'].search([('grup_id','=',id_.grup_id.id),('prev_urun_grup_id','=',id_.ozel_urun_rel.id)])
            for urun in urun_rels:
                urun.urun_grup_id = urun.prev_urun_grup_id.id
                urun.change = True
            id_.state = 0
            id_.change = True

        return True
    @api.model
    def create(self,values ):
        res = super(ozel_urun_grup_group_rel,self ).create( values )
        for senkron in res.grup_id.senkron:
            senkron.grup_senkron = False
        return res
    @api.multi
    def write(self,values ):
        res = super(ozel_urun_grup_group_rel,self ).write( values )
        for s in self:
            for senkron in s.grup_id.senkron:
                senkron.grup_senkron = False
        return res

    _sql_constraints = [
    ('rel_unique', 'unique(grup_id, ozel_urun_rel)', 'Bu Ürün Grubu Zaten Eklenmiş'),
    ]

class okc_urun_rel (models.Model):


    _name = 'info_tsm.urun_grup_group_rel'

    def _get_next_sq_num ( self ):
        added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_urun'])
        return len( added_context ) + 1


    line_number   = fields.Integer(string='Satır No', default=_get_next_sq_num )
    urun_rel      = fields.Many2one('info_tsm.urun_listesi', string='Ürün', required=True)
    grup_id       = fields.Many2one('info_tsm.okc_urun_grup', string='Ökc Ürün Grup')
    prev_urun_grup_id  = fields.Many2one('info_tsm.okc_ozel_urun_grubu_tablosu', string='Prev Özel Ürün Grubu')
    urun_grup_id  = fields.Many2one('info_tsm.okc_ozel_urun_grubu_tablosu',string="Özel Ürün Grubu",required =True)
    plu_no        = fields.Char(string='PLU No',size=8)
    barkod_no     = fields.Char(related='urun_rel.barkod_no',string='Barkod No')
    birim_tipi    = fields.Selection( selection=birim_tipleri, string = 'Birim Tipi', required=True)
    tutar         = fields.Float(string="Toplam Satış Tutarı", digits=(4,2), size=6, required=True )
    change        = fields.Boolean (string='Değişti mi')
    state         = fields.Integer(string='Silinme Durumu')
    
    
    @api.onchange( 'urun_rel')
    @api.multi
    def ozel_urun_rel_onchange(self):
        self.ensure_one()
        #onceki kaydi bulalım
        added_context_grup = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_ozel_urun'])
        dynamic_id_list = []
        for grup in added_context_grup:
            if grup[2]:
                dynamic_id_list.append( grup[2]['ozel_urun_rel'])
            else:
                dynamic_id_list.append (self.env['info_tsm.ozel_urun_grup_group_rel'].browse( grup[1] )[0].ozel_urun_rel.id)

        added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_urun'])
        dynamic_id_urun_list = []
        for eklenmis in added_context:
            if eklenmis [2]:
                dynamic_id_urun_list.append( eklenmis[2]['urun_rel'])
            else:
                dynamic_id_urun_list.append (self.env['info_tsm.urun_grup_group_rel'].browse( eklenmis[1] )[0].urun_rel.id)


        domain = {'urun_grup_id':[('id','in',dynamic_id_list)],
                               'urun_rel':[('id','not in',dynamic_id_urun_list)]}

        res = {'domain':domain}
        
        prev_urun_rel = False
        if self._context.get('grup_id'):
            db_urun_rel_by_group              = self.env['info_tsm.urun_grup_group_rel'].search( [('grup_id','=',self._context['grup_id'])] )
            db_urun_rel                       = filter( lambda x:x if x.id == self._origin.id else None, db_urun_rel_by_group)
    
            if db_urun_rel and db_urun_rel[0].urun_rel:
                prev_urun_rel = db_urun_rel[0].urun_rel.id
    
            if self.urun_rel.name in map(lambda x:x.urun_rel.name,db_urun_rel_by_group):
                self.urun_rel = prev_urun_rel
                res = {'urun_rel': prev_urun_rel,
                           'warning': {
                            'title': 'Zaten Eklenmiş',
                            'message': u'Değiştirilmek İstenen Kayıt Cihazda Zaten Mevcuttur. '},
                        'domain':domain
                    }
        return res
    
    @api.multi
    def unlink(self ):
        
        for s in self:
            self.search([('grup_id','=',s.grup_id.id),('change','=',False)]).write({'change':True})
            if s.grup_id.sudo().firma.taxNumber == '1111111111' and self.env.user.id != s.create_uid.id:
                raise exceptions.ValidationError('Test Modundan Kayıt Silemezsiniz. ')

        return super(okc_urun_rel, self ).unlink()

    @api.onchange( 'urun_grup_id')
    @api.multi
    def ozel_urun_grup_id_onchange(self):
        self.ensure_one()
        #onceki kaydi bulalım
        prev_urun_rel = False
        db_urun_rel_by_group              = self.env['info_tsm.urun_grup_group_rel'].search( [('grup_id','=',self._context['grup_id'])] )
        db_urun_rel                       = filter( lambda x:x if x.id == self._origin.id else None, db_urun_rel_by_group)
        added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_ozel_urun'])
        dynamic_id_list = []
        for grup in added_context:
            if grup[2]:
                dynamic_id_list.append( grup[2]['ozel_urun_rel'])
            else:
                dynamic_id_list.append (self.env['info_tsm.ozel_urun_grup_group_rel'].browse( grup[1] )[0].ozel_urun_rel.id)

        if db_urun_rel and db_urun_rel[0].urun_grup_id:
            prev_urun_rel = db_urun_rel[0].urun_grup_id.id

        added_context_urun = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_urun'])
        dynamic_id_urun_list = []
        for eklenmis in added_context_urun:
            if eklenmis [2]:
                dynamic_id_urun_list.append( eklenmis[2]['urun_rel'])
            else:
                dynamic_id_urun_list.append (self.env['info_tsm.urun_grup_group_rel'].browse( eklenmis[1] )[0].urun_rel.id)


        domain = {'urun_grup_id':[('id','in',dynamic_id_list)],
                               'urun_rel':[('id','not in',dynamic_id_urun_list)]}

        res = {'domain':domain}

        if self.urun_grup_id and self.urun_grup_id.id not in dynamic_id_list:
            self.urun_grup_id = prev_urun_rel
            res = {'urun_grup_id': prev_urun_rel,
                       'warning': {
                        'title': 'Zaten Eklenmiş',
                        'message': u'Değiştirilmek İstenen Kayıt Cihazda Mevcut Değildir. Lütfen Ekleyiniz.. '},
                       'domain':domain
                }
        return res

    @api.onchange('plu_no')
    def plu_no_onchange_control(self):
        res = {}
        if self.plu_no:
            prev_plu_no = False
            db_urun_rel_by_group              = self.env['info_tsm.urun_grup_group_rel'].search( [('grup_id','=',self._context['grup_id'])] )
            db_urun_rel                       = filter( lambda x:x if x.id == self._origin.id else None, db_urun_rel_by_group)
            if db_urun_rel and db_urun_rel[0].urun_grup_id:
                prev_plu_no = db_urun_rel[0].plu_no

            if not self.plu_no.isdigit():

                self.plu_no = prev_plu_no
                res = {'plu_no': prev_plu_no,
                       'warning': {
                        'title': 'Nümerik Lütfen',
                        'message': u'PLU No alanına nümerik karakter ekleyiniz.'}
                }
                return res

            added_context_urun = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_urun'])

            #bak bakalim yunik mi
            #print filter(lambda x:x if x.plu_no == self.plu_no else None, db_urun_rel_by_group)
            if len(filter( lambda x:x if int(x.plu_no) == int(self.plu_no) else None, db_urun_rel_by_group)) > 0:
                self.plu_no = prev_plu_no
                res = {'plu_no': prev_plu_no,
                       'warning': {
                        'title': 'Tekil Lütfen',
                        'message': u'PLU No Zaten Eklenmiş.'}
                }
                return res
            dynamic_id_urun_list = []
            for eklenmis in added_context_urun:
                if eklenmis [2]:
                    dynamic_id_urun_list.append( eklenmis[2]['plu_no'])
                else:
                    dynamic_id_urun_list.append (self.env['info_tsm.urun_grup_group_rel'].browse( eklenmis[1] )[0].plu_no)

            if self.plu_no in dynamic_id_urun_list:
                self.plu_no = prev_plu_no
                res = {'plu_no': prev_plu_no,
                       'warning': {
                        'title': 'Tekil Lütfen',
                        'message': u'PLU No Zaten Eklenmiş.'}
                }
                return res

        return res
    '''
    @api.onchange('tutar')
    def tutar_onchange_control(self):
        res = {}
        if self.tutar:
            prev_tutar = False
            db_urun_rel_by_group              = self.env['info_tsm.urun_grup_group_rel'].search( [('grup_id','=',self._context['grup_id'])] )
            db_urun_rel                       = filter( lambda x:x if x.id == self._origin.id else None, db_urun_rel_by_group)
            if db_urun_rel and db_urun_rel[0].urun_grup_id:
                prev_tutar = db_urun_rel[0].tutar


            added_context_urun = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_urun'])
            #print added_context_urun
            dynamic_id_urun_list = []
            for eklenmis in added_context_urun:
                if eklenmis [2]:
                    dynamic_id_urun_list.append(float(eklenmis[2]['tutar']))
                else:
                    dynamic_id_urun_list.append (float(self.env['info_tsm.urun_grup_group_rel'].browse( eklenmis[1] )[0].tutar))
            #print dynamic_id_urun_list, 'tutar'
            if float(0) in dynamic_id_urun_list:
                self.tutar = prev_tutar
                res = {'tutar': prev_tutar,
                       'warning': {
                        'title': 'Tutar Hatası',
                        'message': u'Tutar 0 olamaz'}
                }
                return res

        return res
    '''
    @api.constrains('tutar')
    def _tutar_coltrol(self, ):
        if float(self.tutar) == float(0):
            raise exceptions.ValidationError(u'Girilen Ürünlerden Birinin Satış Tutarı 0 !!! Tutar 0 olamaz, Lütfen Düzeltin.')

    @api.onchange('line_number')
    def line_number_onchange( self ):
        ##print 'domain' , self._context
        added_context_grup = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_ozel_urun'])
        added_context_urun = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_urun'])
        dynamic_id_list = []
        for grup in added_context_grup:
            if grup[2]:
                dynamic_id_list.append( grup[2]['ozel_urun_rel'])
            else:
                dynamic_id_list.append (self.env['info_tsm.ozel_urun_grup_group_rel'].browse( grup[1] )[0].ozel_urun_rel.id)


        dynamic_id_urun_list = []
        for eklenmis in added_context_urun:
            if eklenmis [2]:
                dynamic_id_urun_list.append( eklenmis[2]['urun_rel'])
            else:
                dynamic_id_urun_list.append (self.env['info_tsm.urun_grup_group_rel'].browse( eklenmis[1] )[0].urun_rel.id)


        dynamic_id_fyat_list = []
        for eklenmis in added_context_urun:
            if eklenmis [2]:
                dynamic_id_fyat_list.append( float(eklenmis[2]['tutar']))
            else:
                dynamic_id_fyat_list.append (float(self.env['info_tsm.urun_grup_group_rel'].browse( eklenmis[1] )[0].tutar))

        #print dynamic_id_fyat_list, 'button'

        if float(0) in dynamic_id_fyat_list:

            res = {
                   'warning': {
                   'title': 'Tutar Hatası',
                   'message': u'Tutar 0 olamaz. Lütfen Düzeltin.'}
            }
            return res




        return dict( domain = {'urun_grup_id':[('id','in',dynamic_id_list)],
                               'urun_rel':[('id','not in',dynamic_id_urun_list)]})

    _sql_constraints = [
    ('rel_unique', 'unique(grup_id, urun_rel)', 'Ürün Zaten Eklenmiş'),
    ('rel_plu', 'unique(grup_id,plu_no)', 'Plu No Zaten Mevcut')
    ]

    @api.multi
    def disable(self):
        for id_ in self:
            id_.state = 1
            id_.change = True

        return True
    @api.multi
    def enable(self ):
        for id_ in self:
            id_.state = 0
            id_.change = True

        return True

    @api.model
    def create(self,values ):
        res = super(okc_urun_rel,self ).create( values )
        self.search([('grup_id','=',res.grup_id.id),('change','=',False)]).write({'change':True})
        for senkron in res.grup_id.senkron:
            senkron.senkron = False
        return res
    @api.multi
    def write(self,values ):
        res = super(okc_urun_rel,self ).write( values )
        for s in self:
            self.search([('grup_id','=',s.grup_id.id),('id','!=',s.id),('change','=',False)]).write({'change':True})
            for senkron in s.grup_id.senkron:
                senkron.senkron = False
        return res

    '''
    @api.model
    def create(self, values):
        if values.has_key('tutar'):
            if int(values ['tutar']) == 0 :
                res = {
                   'warning': {
                   'title': 'Tutar Hatası',
                   'message': u' ürün Tutarı 0 olamaz. Lütfen Düzeltin.'}
                }
                return res
    '''

class fatura_tahsilat_kurum_group_rel(models.Model):

    _name = 'info_tsm.fatura_tahsilat_kurum_group_rel'
    def _get_next_sq_num ( self ):
        #print self._context['pack_tahsilat_kurum']
        added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_tahsilat_kurum'])
        return len( added_context ) + 1

    tahsilat_kurum_rel      = fields.Many2one('info_tsm.fatura_tahsilat_kurum_tipleri', string='Tahsilat Tipi', required=True)
    grup_id                 = fields.Many2one('info_tsm.okc_urun_grup', string='Fatura Tahsilat Kurumları')
    button_no               = fields.Integer(string='Ökc Buton No', required=True,default = _get_next_sq_num)
    kurum_kodu              = fields.Integer(string="Kurum Kodu", required=True)
    kurum_adi               = fields.Char(string = "Kurum Adı")
    
    @api.multi
    def unlink(self ):
        
        for s in self:
            if s.grup_id.sudo().firma.taxNumber == '1111111111' and self.env.user.id != s.create_uid.id:
                raise exceptions.ValidationError('Test Modundan Kayıt Silemezsiniz. ')

        return super(fatura_tahsilat_kurum_group_rel, self ).unlink()

class tahsilat_tipi_group_rel (models.Model):

    _name = 'info_tsm.tahsilat_tipi_group_rel'
    def _get_next_sq_num ( self ):
        #print self._context['pack_tahsilat']
        added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_tahsilat'])
        return len( added_context ) + 1

    tahsilat_rel      = fields.Many2one('info_tsm.tahsilat_tipleri', string='Tahsilat Tipi', required=True)
    grup_id           = fields.Many2one('info_tsm.okc_urun_grup', string='Tahsilat Satış Grup')
    button_no         = fields.Integer(string='Ökc Buton No', required=True,default = _get_next_sq_num)

    @api.onchange( 'tahsilat_rel')
    @api.multi
    def ozel_tahsilat_rel_onchange(self):
        self.ensure_one()
        #onceki kaydi bulalım
        dynamic_id_list = []
        added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_tahsilat'])

        for eklenmis in added_context:
            if eklenmis [2]:
                dynamic_id_list.append( eklenmis[2]['tahsilat_rel'])
            else:
                dynamic_id_list.append (self.env['info_tsm.tahsilat_tipi_group_rel'].browse( eklenmis[1] )[0].tahsilat_rel.id)


        domain = {'tahsilat_rel':[('id','not in',dynamic_id_list)]}

        res = {'domain':domain}
        
        prev_tahsilat_rel = False
        if self._context.get('grup_id'):
            db_tahsilat_rel_by_group              = self.env['info_tsm.tahsilat_tipi_group_rel'].search( [('grup_id','=',self._context['grup_id'])] )
            db_tahsilat_rel                       = filter( lambda x:x if x.id == self._origin.id else None, db_tahsilat_rel_by_group)
    
            if db_tahsilat_rel and db_tahsilat_rel[0].tahsilat_rel:
                prev_tahsilat_rel = db_tahsilat_rel[0].tahsilat_rel.id
    
            
            if self.tahsilat_rel.name in map(lambda x:x.tahsilat_rel.name,db_tahsilat_rel_by_group):
                self.tahsilat_rel = prev_tahsilat_rel
                res = {'tahsilat_rel': prev_tahsilat_rel,
                           'warning': {
                           'title': 'Zaten Eklenmiş',
                           'message': u'Değiştirilmek İstenen Kayıt Cihazda Zaten Mevcuttur. '},
                        'domain':domain
                    }
        return res

    @api.onchange('button_no')
    def button_no_onchange( self ):

        dynamic_id_list = []
        added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_tahsilat'])
        if len(added_context) > 19:
            raise exceptions.ValidationError(u"En Fazla 20 Tane Tahsilat Tipi Eklenebilir.")
        for eklenmis in added_context:
            if eklenmis [2]:
                dynamic_id_list.append( eklenmis[2]['tahsilat_rel'])
            else:
                dynamic_id_list.append (self.env['info_tsm.tahsilat_tipi_group_rel'].browse( eklenmis[1] )[0].tahsilat_rel.id)

        return dict( domain = {'tahsilat_rel':[('id','not in',dynamic_id_list)]})

    def list_button_no_arrange( self, cr, uid, context=None,datasets=[],ids = []):
        #print ids
        button_no = 1
        for i in datasets:
            obj = self.browse(cr, uid, i['id'], context=context)
            obj.button_no = button_no
            button_no += 1

        return {'result':1}
    @api.multi
    def unlink(self ):
        
        for s in self:
            if s.grup_id.sudo().firma.taxNumber == '1111111111' and self.env.user.id != s.create_uid.id:
                raise exceptions.ValidationError('Test Modundan Kayıt Silemezsiniz. ')

        return super(tahsilat_tipi_group_rel, self ).unlink()

class satis_tipi_group_rel (models.Model):

    _name = 'info_tsm.satis_tipi_group_rel'
    def _get_next_sq_num ( self ):
        added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_satis'])
        return len( added_context ) + 1

    #satis_rel        = fields.Many2one('info_tsm.satis_tipleri', string='Satış Tipi')
    grup_id          = fields.Many2one('info_tsm.okc_urun_grup', string='Tahsilat Satış Grup')
    button_no                   = fields.Integer(string='Ökc Buton No', required=True,default = _get_next_sq_num)
    satis_tipi_islem_rel        = fields.Many2one('info_tsm.satis_tipi_islem_kodu_tipleri', string="Satış Tipi İşlem Kodu", required=True)

    @api.onchange( 'satis_tipi_islem_rel')
    @api.multi
    def ozel_satis_rel_onchange_(self):
        self.ensure_one()
        #onceki kaydi bulalım
        dynamic_id_list = []
        added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_satis'])
        #print added_context
        for eklenmis in added_context:
            if eklenmis [2]:
                dynamic_id_list.append( eklenmis[2]['satis_tipi_islem_rel'])
            else:
                dynamic_id_list.append (self.env['info_tsm.satis_tipi_group_rel'].browse( eklenmis[1] )[0].satis_tipi_islem_rel.id)

        domain = {'satis_tipi_islem_rel':[('id','not in',dynamic_id_list)]}
        #print domain,  db_satis_rel_by_group
        res = {'domain':domain}
        
        prev_satis_rel = False
        if self._context.get('grup_id'):
            db_satis_rel_by_group              = self.env['info_tsm.satis_tipi_group_rel'].search( [('grup_id','=',self._context['grup_id'])] )
            #print '*************'
            #print db_satis_rel_by_group, self._context['grup_id']
            #print '*************'
    
    
            db_satis_rel                       = filter( lambda x:x if x.id == self._origin.id else None, db_satis_rel_by_group)
    
            if db_satis_rel and db_satis_rel[0].satis_tipi_islem_rel:
                prev_satis_rel = db_satis_rel[0].satis_tipi_islem_rel.id
    
            if self.satis_tipi_islem_rel.name in map(lambda x:x.satis_tipi_islem_rel.name,db_satis_rel_by_group):
                self.satis_tipi_islem_rel = prev_satis_rel
                res = {'satis_tipi_islem_rel': prev_satis_rel,
                           'warning': {
                           'title': 'Zaten Eklenmiş',
                           'message': u'Değiştirilmek İstenen Kayıt Cihazda Zaten Mevcuttur. '},
                        'domain':domain
                    }
        return res

    @api.onchange('button_no')
    def button_no_onchange( self ):

        dynamic_id_list = []
        added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_satis'])
        if len(added_context) > 19:
            raise exceptions.ValidationError(u"En Fazla 20 Tane Satış Tipi Eklenebilir.")
        for eklenmis in added_context:
            if eklenmis [2]:
                dynamic_id_list.append( eklenmis[2]['satis_tipi_islem_rel'])
            else:
                dynamic_id_list.append (self.env['info_tsm.satis_tipi_group_rel'].browse( eklenmis[1] )[0].satis_tipi_islem_rel.id)

        return dict( domain = {'satis_tipi_islem_rel':[('id','not in',dynamic_id_list)]})

    def list_button_no_arrange( self, cr, uid, context=None,datasets=[],ids = []):
        #print ids
        button_no = 1
        for i in datasets:
            obj = self.browse(cr, uid, i['id'], context=context)
            obj.button_no = button_no
            button_no += 1

        return {'result':1}
    
    @api.multi
    def unlink(self ):
        
        for s in self:
            if s.grup_id.sudo().firma.taxNumber == '1111111111' and self.env.user.id != s.create_uid.id:
                raise exceptions.ValidationError('Test Modundan Kayıt Silemezsiniz. ')

        return super(satis_tipi_group_rel, self ).unlink()

class fonsiyon_tus_rel1(models.Model):
    
    _name = 'info_tsm.fonksiyon_tuslari1'
    _order           = 'button_no_v asc'
    
    def _get_next_sq_num ( self ):
        if self._context.get('pack_fonk1'):
            added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_fonk1'])
            return len( added_context ) + 1
        return 1
    
    name             = fields.Selection(selection=fonk_tus1, string='Buton Adı')
    grup_id          = fields.Many2one('info_tsm.okc_urun_grup', string='Ökc Ürün Grup')
    button_no        = fields.Integer(string='Sıra No', required=True,default = _get_next_sq_num)
    button_no_v      = fields.Integer(string='Ökc Buton No', default = _get_next_sq_num)
    flag             = fields.Boolean()
    
    @api.model
    def create( self, vals):
        #vals['button_no_v'] = vals.get('button_no')
        
        print 'Create Vals : ',vals
        
        if vals.get('button_no_v'): vals['button_no'] = vals.get('button_no_v')
        return super (fonsiyon_tus_rel1,self).create( vals )
        
    @api.multi
    def write(self,vals):
        
        if vals.get('button_no_v'): vals['button_no'] = vals.get('button_no_v')
        res = super(fonsiyon_tus_rel1,self).write( vals)
        if not vals.get('flag'):
            i = 1
            for s in self:
                others = self.search([('grup_id','=',s.grup_id.id)],order='button_no_v')
                for o in others:
                    o.write({'button_no':i,
                            'flag':True})
                    i += 1
        
        return res
    
class fonsiyon_tus_rel2(models.Model):
    
    _name = 'info_tsm.fonksiyon_tuslari2'
    _order           = 'button_no_v2 asc'
    
    def _get_next_sq_num ( self ):
        console.log( self._context)
        if self._context.get('pack_fonk2'):
            print 'burda'
            added_context = filter( lambda x:x if x[0] == 4 or x[0] == 0 else None,self._context['pack_fonk2'])
            return len( added_context ) + 1
        return 1
    
    name2             = fields.Selection(selection=fonk_tus2, string='Buton Adı')
    grup_id2          = fields.Many2one('info_tsm.okc_urun_grup', string='Ökc Ürün Grup')
    button_no2        = fields.Integer(string='Sıra No', required=True,default = _get_next_sq_num)
    button_no_v2      = fields.Integer(string='Ökc Buton No', default = _get_next_sq_num)
    flag              = fields.Boolean()
    
    @api.model
    def create( self, vals):
        #vals['button_no_v'] = vals.get('button_no')
        if vals.get('button_no_v2'): vals['button_no2'] = vals.get('button_no_v2')
        return super (fonsiyon_tus_rel2,self).create( vals )
        
    @api.multi
    def write(self,vals):
        
        if vals.get('button_no_v2'): vals['button_no2'] = vals.get('button_no_v2')
        res = super(fonsiyon_tus_rel2,self).write( vals)
        print vals
        if not vals.get('flag'):
            i = 1
            for s in self:
                others = self.search([('grup_id2','=',s.grup_id2.id)],order='button_no_v2')
                for o in others:
                    o.write({'button_no2':i,
                            'flag':True})
                    i += 1
        
        return res
    