# -*- coding: utf-8 -*-
__author__ = 'deniz'
import pytz
from datetime import datetime

def compute_curent_time( context=None ):
    date_now = datetime.now()
    
    if context and context.get('tz',False):
       
        time_zone=context['tz']
        tz = pytz.timezone(time_zone)
        tzoffset = tz.utcoffset(date_now)
        date_now = date_now + tzoffset

    return date_now.strftime('%d/%m/%Y %H:%M:%S')
class check:
    def __init__(self, exceptions,  model_class_attrs=None ):

        self.model_class_attrs     = model_class_attrs
        self.exceptions            = exceptions

    def check_unique_bos(self,checked_field_obj,length = 0, len_check=True):

        sr_ids = self.model_class_attrs.search([])

        for key, string in checked_field_obj.iteritems():

            attr   = getattr(self.model_class_attrs, key)
            if attr:
                if type( attr ) != unicode and type( attr ) != str:
                    attr = unicode( attr )

                if len_check:
                    if length == 0:
                        if len( attr.strip() ) == length:

                            raise self.exceptions.ValidationError("%s Boş Olamaz!!" % string)
                    else:
                        if len( attr.strip() ) != length:
                            raise self.exceptions.ValidationError("%s , %s Uzunluğunda olmalıdır." % (string, length))


                lst = map( lambda x:getattr(x,key).lower() if x.id != self.model_class_attrs.id else None, sr_ids)
                if attr and attr.lower() in  lst:
                    raise self.exceptions.ValidationError("%s %s Zaten Eklenmiş!" % (attr.encode('utf-8'),string))

    def check_ip_no ( self, checked_field_obj):
        for key, string in checked_field_obj.iteritems():

            attr = getattr(self.model_class_attrs, key)
            if attr:
                if attr.count('.') != 3:
                    raise self.exceptions.ValidationError("%s Ip Adres Formatına Uymuyor!" % string)
                if not attr.replace('.','').isdigit():
                    raise self.exceptions.ValidationError("%s Ip Adresi Nümerik Olmalıdır!" % string)