# -*- coding: utf-8 -*-
{
    'name': "Bayi",

    'summary': """İnfoteks ve müşterilerine ait bayi modülü""",

    'description': """
        İnfoteks ana bayileri, müşterileri, müşterilerinin bayi ve franchaselarının kullanımı için hazırlanmış bayi modülüdür.
    """,

    'author': "İnfoteks adına Deniz Yıldız",
    'website': "http://www.infoteks.com.tr",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Plugins',
    'version': '10.1',

    # any module necessary for this one to work correctly
    'depends': ['base','hr','account','info_extensions','crm','mail','contacts','sales_team','calendar'],

    # always loaded
    'data': [
        'views/assets.xml',
        'security/groups.xml',
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/tree.xml',
        'views/wizards.xml',
        'views/actions.xml',
        'views/forms.xml',
        'views/menu.xml',
        'views/sequences.xml',
        'views/report_makbuz.xml',
        'views/report_sozlesme.xml',
        'views/search.xml',
        'views/login.xml',
        'views/kanban.xml',
        'views/il_ilce_vergidaire.xml',
        'views/report_invoice.xml',
        #'views/automated_actions.xml',
        'views/report_sozlesme_sablon.xml',
        'views/uye_isyeri_mail.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    "qweb": [
        'static/src/xml/info_bayi_temp.xml'],
    'installable': True,
    'application': True,
    'auto_install': False,
}