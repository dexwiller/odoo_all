# -*- coding: utf-8 -*-
{
    'name': "Yardım Masası",
    'summary': """İnfoteks projeleri için yardım modülü""",
    'description': """
İnfoteks yardım
===============
İnfoteks projeleri hakkında metin ve videolarla eğitim ve sık sorulan sorular oluşturabilirsiniz
    """,
    'author': "infoteks",
    'website': "https://www.infoteks.com",
    'category': 'Proje',
    'version': '0.1',
    'depends': ['base'],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/assets.xml',
        'views/actions.xml',
        'views/menu.xml',
        'views/form.xml',
        'views/search.xml',
        'views/tree.xml',
    ],
    'demo': [
        'demo.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
