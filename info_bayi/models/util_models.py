# -*- coding: utf-8 -*-

from odoo import models, fields, api
import heapq
path  = '/var/lib/odoo/addons/10.0/'
path  = '/home/deniz/odoo12/odoo/addons/'
def merge_no_duplicates(*iterables):
    last = object()

    for val in heapq.merge(*iterables):
        if val != last:
            last = val
            yield val

class ir_sequence(models.Model):
    _inherit = 'ir.sequence'
class info_extensions_vergidaireleri(models.Model):
	_inherit = 'info_extensions.vergidaireleri'
class info_extensions_iller(models.Model):
	_inherit = 'info_extensions.iller'
class info_extensions_ilceler(models.Model):
	_inherit = 'info_extensions.ilceler'
class info_extensions_partner_contact(models.Model):
	_inherit = 'info_extensions.partner_contact'
class info_bayi_hr_employee(models.Model):
	_inherit = 'hr.employee'
	@api.model
	def _get_default_company_id(self):
		return self.env.user.company_id
	company_id = fields.Many2one('res.company', default=_get_default_company_id)


class info_bayi_m_tercih_istemiyor(models.Model):
	_name = 'info_bayi.musteri_istemiyor_states'

	name = fields.Char('Durum Açıklama', required=True)
	state = fields.Selection(selection=[('dusunecekmis', 'Düşünmek İstiyor'),
										('istemiyor', 'Müşteri İstemiyor'), ],
							 string="Durum", required=True)

class info_bayi_res_users(models.Model):
	_inherit = 'res.users'

class PickingType(models.Model):
	_inherit = "stock.picking.type"
	@api.model
	def _get_default_company_id(self):
		return self.env.user.company_id
	company_id = fields.Many2one('res.company', default=_get_default_company_id)
class ok_confirm( models.TransientModel):
	_name = 'info_bayi.ok_confirm'
	
	@api.model
	def default_get ( self,field_list ):
		res = super( ok_confirm, self).default_get( field_list )
		res['active_id'] = self._context['active_id']
		res['model']     = self._context['active_model']
		res['func' ]     = self._context['active_function']
		res['conf_mes']  = self._context['active_mes']
		
		return res
	
	active_id         = fields.Integer()
	model             = fields.Char()
	func              = fields.Char()
	conf_mes          = fields.Char()
	
	@api.multi
	def confirm_and_proceed( self ):
		self.ensure_one()
		m = self.env[self.model].browse(self.active_id)
		return getattr(m,self.func)(from_wizard=True)