# -*- coding: utf-8 -*-
from odoo import models, fields, api
class mailmail(models.Model):
	_inherit = 'mail.mail'
	@api.model
	def create( self,values ):
		if not values.get('state') or values.get('state') == u'draft':
			values['state'] = u'outgoing'

		return super( mailmail, self).create( values )

class mail_tracking_value(models.Model):
	_inherit = 'mail.tracking.value'