# -*- coding: utf-8 -*-
from odoo import models, fields, api,exceptions

class PhoneCalls(models.Model):
    _name = 'info_bayi.phone_calls'
    
    @api.model
    def default_get(self,field_list):
        field_list = super(PhoneCalls, self).default_get(field_list)
        return field_list
        
    
    type_   = fields.Selection(selection=[('out','Giden Aramalar'),('in','Gelen Aramalar')], string='Arama Tipi')
    number  = fields.Char(string='Telefon Numarası')
    person  = fields.Char(string='Konuşulan Kişi')
    sum_    = fields.Text(string='Konuşma Özeti')
    pos_id  = fields.Many2one('info_bayi.turkpara_banka_rel')
    kart_id = fields.Many2one('stock.production.lot')
    
    @api.onchange('number')
    def onchange_mobile(self):
        
        res = {}
        if self.number and len( self.number) > 0:
            phone = [str(s) for s in self.number.split() if s.isdigit()]
            if len( phone )> 0:
                phone = "".join( phone )
                if phone[0] == "0":
                    phone = phone[1:]
                if len( phone) != 10:
                    self.number = False
                    res = {'warning': {
                    'title': 'Telefon No',
                    'message': u'Lütfen Geçerli Bir Telefon No Giriniz. '}
                    }
                else:
                    phone = phone[0:3] + '-' + phone[3:6] + ' ' + phone[6:8] + ' ' + phone[8:10]
                    self.number = phone
            else:
                self.number = False
                res = {'warning': {
                  'title': 'Telefon No',
                  'message': u'Lütfen Geçerli Bir Telefon No Giriniz. '}
                }
        return res

class PosMarkalari(models.Model):
    _name = 'info_bayi.pos_markalari'
    
    name = fields.Char(string='Cihaz Marka', required=True)

class YemekKartlari(models.Model):
    _name = 'info_bayi.yemek_kartlari'
    
    yemek_karti_adi = fields.Char('Yemek Kartı')
    pos_id          = fields.Many2one('info_bayi.turkpara_banka_rel')

class container( models.Model):
    
    _name = 'info_bayi.container'
    
    partner_id  = fields.Many2one('res.partner')

class encokharcama( models.Model):
    
    _name  = 'info_bayi.en_cok_harcama'
    
    pos_id          = fields.Many2one('info_bayi.turkpara_banka_rel')
    kart_no         = fields.Char('Kart No')
    kart_sahibi     = fields.Char('Kart Sahibi')
    firma           = fields.Char('Kart Firması')  
    tutar           = fields.Float('Toplam Tutar')

class webforms( models.Model ):
    
    _name = 'info_bayi.webforms'
    
    adi_soyadi = fields.Char('Adı Soyadı' ,required=True)
    firma      = fields.Char('Firma',required=True)
    e_posta    = fields.Char('E-Posta',required=True)
    tel        = fields.Char('Telefon',required=True)
    sehir      = fields.Many2one('info_extensions.iller',string='Şehir',required=True)