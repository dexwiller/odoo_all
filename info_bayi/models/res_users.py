# -*- coding: utf-8 -*-
from odoo import models, fields, api,exceptions

class res_users( models.Model ):

	_inherit = 'res.users'
	ekip_uye_id = fields.Many2many('res.users',relation='res_users_ekip_users_rel',column1='res_user_id',column2='ekip_user_id')
	
	@api.model
	def create( self,values):
		res = super( res_users, self).create( values )
		
		if res.partner_id.sube_kodu == False:
			res.partner_id.customer          = False
			res.partner_id.is_company        = False
		sqls = "update res_partner set create_uid = %s where id = %s"%(res.id, res.partner_id.id )
		self.env.cr.execute( sqls )
		##print 'aaaaaaaaaaaaaaaa'
		##print res.partner_id

		return res
class res_groups(models.Model):
	_inherit = 'res.groups'
	
	uye_is_yeri_default  = fields.Boolean('Üye İş Yeri Varsayılan Yetki')
	kurumsal_default     = fields.Boolean('Kurumsal Müşteri Varsayılan Yetki')
	is_ortagi_default    = fields.Boolean('İş Ortağı Varsayılan Yetki')
	sube_default         = fields.Boolean("Şube Kullanıcısı Varsayılan Yetki")