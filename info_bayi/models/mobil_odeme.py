# -*- coding: utf-8 -*-
from odoo import models, fields, api,exceptions

class mobil_odemeler( models.Model):
    _name = 'info_kart.mobil_odeme'
    
    lot_id      = fields.Many2one('stock.production.lot',string='Ödeme Yapılan Kart')
    tutar       = fields.Float(string='Ödeme Tutarı')
    islem_id    = fields.Integer(string='İşlem Id')#sql const konulacak!
    cuzdan_kodu = fields.Char(string='Cüzdan Kodu')
    status      = fields.Selection(selection=[('done','Başarılı'),('fail','Başarısız'),('cancel','Iptal'),('cancelFail','Iptal')],string='Servis Durumu')
    status_str  = fields.Char(string='Servis Açıklama')
    aciklama    = fields.Char(string='Açıklama')
    dekont_id   = fields.Char(string='Dekont Id') #sql const konulacak!
    cari_id     = fields.Char(string='Cari Id') #sql const konulacak!
    partner_id  = fields.Many2one('res.partner',string='Ödeme Yapılan Partner')