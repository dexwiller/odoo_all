# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions
from webserviceopt import pos_islemleri_2, Cuzdan
from copy import copy


class cuzdan(models.Model):
    _name = 'info_kart.cuzdan'
    _order = 'sequence'

    sequence = fields.Integer('Sıra No')
    name = fields.Char('Cüzdan Adı')
    kod = fields.Char('Cüzdan Kodu')
    serbest = fields.Boolean()
    uyari = fields.Text('Yükleme Uyarısı')
    mcc_acik = fields.Boolean("MCC Kodu İle İşlem Yapılıyor mu")

    @api.model
    def get_cuzdan_full(self, **kw):

        result = Cuzdan(self)
        cuzdan = self.search([])
        kods = [int(x.kod) for x in cuzdan]

        for k, v in result.items():
            if int(k) not in kods:
                serbest = False
                if int(k) == 1000:
                    serbest = True
                new_cr = {'name': v,
                          'kod': k,
                          'serbest': serbest}
                o = self.sudo().create(new_cr)

        return {1: 1}


class stock_production_lot_cuzdan_rel(models.Model):
    _name = 'info_kart.lot_cuzdan_rel'

    lot_id = fields.Many2one('stock.production.lot', string='Kart no')
    cuzdan = fields.Many2one('info_kart.cuzdan', string='Cüzdan')
    bakiye = fields.Float(string='Bakiye')
    bakiye_b = fields.Float(string='Bloke Tutar')
    curr = fields.Many2one('res.currency',
                           default=lambda self: self.env['res.currency'].search([('name', '=', 'TRY')], limit=1))


class aktifBanka(models.Model):
    _name = 'info_bayi.turkpara_aktif_banka'

    name = fields.Many2one('info_extensions.acquirer', string='Aktif Banka Adı')
    email = fields.Char('İletişim E-Posta')


class turkParaPosEmirleri(models.Model):
    _name = 'info_bayi.turkpara_is_emirleri'
    _order = "id desc "

    @api.model
    def _get_domain(self):
        domain = [('esnaf', '=', True),
                  ('state', 'in', ('islak_bekleniyor', 'confirmed', 'giris', 'muhasebe')),
                  ('is_company', '=', True),
                  ('turkpara_e_para_hesap', '!=', False)]
        eklenenemer = self.search([])
        p_ids = list(map(lambda x:x.partner_id.id, eklenenemer))
        final_ids = self.env['res.partner'].search(domain).ids
        sube_ids = self.env['res.partner'].search([('parent_id', 'in', final_ids)]).ids

        final_ids.extend(sube_ids)

        domain = [('id', 'in', final_ids)]
        domain.append(('id', 'not in', p_ids))
        return domain

    partner_id = fields.Many2one('res.partner', string='Üye İş Yeri / Esnaf', domain=_get_domain)
    partner_dummy = fields.Char("Kayıtlı Olmayan İş Yeri / Esnaf")

    sermaye_sirketi = fields.Boolean(related='partner_id.sermaye_sirketi')
    sahis_sirketi = fields.Boolean(related='partner_id.sahis_sirketi')
    ortaklik = fields.Boolean(related='partner_id.ortaklik')

    adres = fields.Char(related='partner_id.street')
    adres2 = fields.Char(related='partner_id.street2')

    ilce = fields.Many2one(related='partner_id.ilce')
    il = fields.Many2one(related='partner_id.city_combo')

    vergi_no = fields.Char(related='partner_id.taxNumber')
    tckn = fields.Char(related='partner_id.tckn')

    banka_rels = fields.One2many('info_bayi.turkpara_banka_rel', 'isemri', string='Tanımlı / Tanımlanacak Poslar')
    state = fields.Selection(string='Durumu', selection=[('draft', 'Yeni'),
                                                         ('confirmed', 'Türkpara Onaylandı'),
                                                         ('silindi', 'Türkpara Tarafında Silindi'),
                                                         ('yasaklandi', 'Pos Yasaklı'),
                                                         ('yuksek_oncelik', 'Pos Yüksek Öncelikli'),
                                                         ('error', 'Türkpara WS Hatası')], compute='_get_state')
    supurme = fields.Selection(selection=[('v', 'Var'), ('y', 'Yok')], string='Banka Süpürme Talimatı', default='y',
                               compute='_get_supurme')
    state_text = fields.Text(string='Pos Durumları', compute='_get_pos_text')

    tumunu_yolla = fields.Boolean(string='Kaydettikten Hemen Sonra Tüm Posları Otomatik Olarak Gönder', default=False)
    company_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id)

    cuzdanlar = fields.One2many('info_bayi.terminal_cuzdan_rel', 'is_emri', string='Cüzdanlar')

    @api.model
    def create(self, values):
        if values.get('partner_id') and len(self.search([('partner_id', '=', values.get('partner_id'))])) > 0:
            raise exceptions.ValidationError(
                'Aynı Üye İşyeri İçin İkinci Bir İş Emri Grubu Oluşturamazsınız. Mevcut olan pos iş emirlerinde düzenleme yapınız.')
        res = super(turkParaPosEmirleri, self).create(values)
        if res.tumunu_yolla:
            res.tumunu_gonder()

        return res

    @api.multi
    def _get_pos_text(self):

        for s in self:
            text = ''
            for b in s.banka_rels:
                opt = b._fields['state'].selection
                text += b.banka.name.name + ' : ' + \
                        list(filter(lambda x: x[1] if x[0] == b.state else None, b._fields['state'].selection))[0][
                            1] + ' - Süpürme Talimatı :' + list(
                    filter(lambda x: x[1] if x[0] == b.supurme_durumu else None,
                           b._fields['supurme_durumu'].selection))[0][1] + '\n'
            s.state_text = text

    @api.multi
    def _get_supurme(self):
        supurme = 'y'
        for s in self:
            f = len(list(filter(lambda x: x if x.supurme_durumu == 'y' else None, s.banka_rels)))
            if f > 0:
                supurme = 'y'
            else:
                supurme = 'v'
            s.supurme = supurme

    @api.multi
    def _get_state(self):
        state = 'draft'
        for s in self:
            f = len(list(
                filter(lambda x: x if x.state in ('confirmed', 'guncellendi', 'aktif_edildi') else None, s.banka_rels)))
            if f > 0:
                state = 'confirmed'
            else:
                state = 'silindi'
            s.state = state

    @api.multi
    def unlink(self):
        for s in self:
            if s.state not in ('draft', 'error'):
                raise exceptions.ValidationError('Türk Para Tarafına Gönderilmiş Bir İş Emrini Silemezsiniz.')

        return super(turkParaPosEmirleri, self).unlink()

    @api.multi
    def tumunu_gonder(self):
        for s in self:
            s.banka_rels.send_cuzdan()
            s.banka_rels.send()


class turkParaBankaRel(models.Model):
    _name = 'info_bayi.turkpara_banka_rel'

    def _get_next_islem_id(self):
        s_no = self.env['ir.sequence'].sudo().next_by_code('info_bayi.pos_islem')
        return s_no

    banka = fields.Many2one('info_bayi.turkpara_aktif_banka', string='Pos Banka', required=True)
    isemri = fields.Many2one('info_bayi.turkpara_is_emirleri', string='İlgili İş Emri')
    terminal_no = fields.Char(string='Pos Terminal No', required=True)
    uye_isyeri_no = fields.Char(string='Üye İş Yeri No', required=True)
    islem_tipi = fields.Selection(selection=[("1", "Normal Pos"),
                                             ("12", "Pos Cüzdan Yasakla (Tüm MCC Açık Cüzdanlar)"),
                                             ("3", "Pos Sil ")], string='İşlem Tipi', required=True, default="1")
    pid = fields.Char(string='PID')
    tid = fields.Char(string='TID')
    state = fields.Selection(string='Durumu', selection=[('draft', 'Yeni Kayıt'),
                                                         ('sended', 'Sunucuya Gönderildi'),
                                                         ('confirmed', 'Pos Aktif'),
                                                         ('guncellendi', 'Pos Aktif - Güncellendi'),
                                                         ('yasaklandi', 'Pos Yasaklı'),
                                                         ('yuksek_oncelik', 'Pos Yüksek Öncelikli'),
                                                         ('silindi', 'Pos Silindi'),
                                                         ('aktif_edildi', 'Pos Aktif'),
                                                         ('error', 'Sunucu Hatası')], default="draft")
    ws_hata_mesaji = fields.Char(string='TürkPara WS Hatası')

    partner_id = fields.Many2one(related='isemri.partner_id', store=True)

    pos_markasi = fields.Many2one('info_bayi.pos_markalari', string='Yazarkasa Cihaz Markası')

    son_gun_sonu = fields.Float(string='Son Gün Sonu Tutarı')

    sticker_seysi = fields.Selection(
        selection=[('g', 'Gönderilmedi'), ('k', 'Kargolandı'), ('m', 'Müşteri Teslim Aldı'),
                   ('i', 'İş yerinin Camına Yapıştırıldı')], string='Sticker Durumu')

    supurme_durumu = fields.Selection(selection=[('v', 'Var'), ('y', 'Yok')], string='Banka Süpürme Talimatı',
                                      default='y')

    islem_id = fields.Float(string="İslem ID")

    pos_goruntu_name = fields.Char()
    pos_goruntu = fields.Binary('Pos Slip Görüntüsü')
    pos_slip_durumu = fields.Selection(selection=[('1', 'Slip Forma Eklendi'),
                                                  ('2', 'Whatsapp İle Gönderildi'),
                                                  ('3', 'E-posta İle Gönderildi'),
                                                  ('4', 'Henüz Gönderilmedi')],
                                       string='Pos Slip Görüntüsü Gönderim Metodu')

    red_aciklamasi = fields.Text('İptal Açıklaması')
    son_proc_id = fields.Integer()

    cuzdanlar = fields.One2many('info_bayi.terminal_cuzdan_rel', 'terminal', string='Cüzdanlar')
    renkler = fields.Char(compute='_get_color_by_wallet')
    cuzdanlar_html = fields.Html(compute='get_dynamic_cuzdan_by_pos')

    @api.multi
    def get_dynamic_cuzdan_by_pos(self):

        for s in self:
            html = '''<div class="divTable">
                            <div class="divTableBody">
                                <div class="divTableRow">
                                    <div class="divTableHead">Cüzdan</div>
                                    <div class="divTableHead">İndirim</div>
                                    <div class="divTableHead">Durum</div>
                                </div>'''
            for c in s.cuzdanlar:
                print(c._fields['state'].selection)
                c_s = list(filter(lambda x: x[1] if x[0] == c.state else None, c._fields['state'].selection))
                print(c_s)
                if c_s:
                    c_state = c_s[0][1]
                else:
                    c_state = ""

                if c.cuzdan.kod != '1000':
                    html += '<div class="divTableRow">'
                    html += '<div class="divTableCell">' + c.cuzdan.name.split(' ')[0].split('-')[0].split('(')[
                        0] + '</div>'
                    html += '<div class="divTableCell">%' + str(c.indirim_) + '</div>'
                    html += '<div class="divTableCell">' + c_state + '</div>'
                    html += '</div>'
            html += '</div></div>'
            s.cuzdanlar_html = html

    @api.constrains('terminal_no', 'uye_isyeri_no')
    def check_unique_terminal_uyeisyeri(self):
        v = self.search([('terminal_no', '=', self.terminal_no),
                         ('uye_isyeri_no', '=', self.uye_isyeri_no)])
        for i in v:
            if i and i.id != self.id:

                raise exceptions.ValidationError(
                    'Bu Üye İşyeri no ve Terminal No Daha Önce Eklenmiş. Eklenen Cari: %s' % (
                                i.partner_id.name or i.isemri.partner_dummy))

    @api.multi
    def _get_color_by_wallet(self):
        for s in self:
            yasak = []
            aktif = []
            error = []

            for c in s.cuzdanlar:
                if c.state == 'yasaklandi':
                    yasak.append(c.id)
                elif c.state in ('confirmed', 'yuksek_oncelik', 'sended', 'guncellendi', 'aktif_edildi'):
                    aktif.append(c.id)
                elif c.state == 'error':
                    error.append(c.id)
                    break

            if error:
                s.renkler = "red"
            else:
                if yasak and aktif:
                    s.renkler = 'orange'
                elif yasak and not aktif:
                    s.renkler = 'purple'
                elif aktif and not yasak:
                    s.renkler = 'green'

    @api.multi
    def send_cuzdan(self):
        for s in self:
            s.cuzdanlar.gonder()
        msj = 'İşlem Gerçekleştirildi, Pos Listesini ve Cüzdan Durumlarını Kontrol Edebilirsiniz.'
        r = "/web/payresult?inner_message=1&success=%s&msj=%s" % (1, msj)
        return {
            'type': 'ir.actions.act_url',
            'url': r,
            'target': 'popup'
        }

    @api.multi
    def sube_degistir(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'name': 'Şube Değiştir',
            'view_mode': 'form',
            'view_type': 'form',
            'res_model': 'info_bayi.is_emri_sube_degistir',
            'target': "new"
        }

    @api.multi
    def pos_iptal(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'name': 'İptal Sebebi',
            'view_mode': 'form',
            'view_type': 'form',
            'res_model': 'info_bayi.sube_form_iptal',
            'target': "new"
        }

    @api.multi
    def kaydet(self):
        return True

    @api.model
    def create(self, v):
        if v.get('pos_goruntu'):
            v['pos_slip_durumu'] = "1"
        cuzdanlar = self.env['info_kart.cuzdan'].search([('kod', '=', '1084')])

        if v.get('islem_tipi') == "12":
            cuzdanlar = self.env['info_kart.cuzdan'].search([('mcc_acik', '=', True)])
            c_ids = [(0, False, {'cuzdan': x.id, 'islem_tipi': "12"}) for x in cuzdanlar]
        else:
            c_ids = [(0, False, {'cuzdan': x.id}) for x in cuzdanlar]
        v['cuzdanlar'] = c_ids
        return super(turkParaBankaRel, self).create(v)

    @api.multi
    def gelen_aramalar(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'name': 'Gelen Aramalar',
            'view_mode': 'tree,form',
            'view_type': 'form',
            'res_model': 'info_bayi.phone_calls',
            'context': {'default_type_': 'in', 'default_pos_id': self.id},
            'flags': {'action_buttons': True},
            'domain': "[('type_','=','in'),('pos_id','=',%s)]" % self.id,
            'target': "new"
        }

    @api.multi
    def giden_aramalar(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'name': 'Giden Aramalar',
            'view_mode': 'tree,form',
            'view_type': 'form',
            'res_model': 'info_bayi.phone_calls',
            'context': {'default_type_': 'out', 'default_pos_id': self.id},
            'flags': {'action_buttons': True},
            'domain': "[('type_','=','out'),('pos_id','=',%s)]" % self.id,
            'target': "new"
        }

    @api.multi
    def pos_report(self):
        self.ensure_one()
        action = {
            'name': 'Pos Hareketleri',
            'type': 'ir.actions.act_window',
            'res_model': 'info_bayi.get_rapor_wizard',
            'src_model': 'info_bayi.turkpara_banka_rel',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
            'context': {'default_uye_is_yeri_no': self.uye_isyeri_no,
                        'default_terminal_no': self.terminal_no,
                        'active_id': self.isemri.sudo().partner_id.id,
                        'get_by_all': False}
        }
        return action

    @api.multi
    def send(self):
        for s in self:
            if s.islem_tipi == "12":
                cuz = [x for x in s.cuzdanlar if x.cuzdan.mcc_acik]
                if cuz:
                    for c in cuz:
                        c.islem_tipi = s.islem_tipi
                        pos_islemleri_2(c)
                    s.state = 'guncellendi'
                else:
                    raise exceptions.UserError(
                        "Pos'a Ekli MCC Kodu Açık Cüzdan Bulunamadı, Lütfen Cüzdan Ayarlarını Yapılandırınız.")
            else:
                pos_islemleri_2(s, obj_type='pos')

        msj = 'İşlem Gerçekleştirildi, Pos Listesini ve Cüzdan Durumlarını Kontrol Edebilirsiniz.'
        r = "/web/payresult?inner_message=1&success=%s&msj=%s" % (1, msj)
        return {
            'type': 'ir.actions.act_url',
            'url': r,
            'target': 'popup'
        }

    @api.multi
    def unlink2(self):
        self.unlink()

    @api.multi
    def cuzdan_opts(self):
        self.ensure_one()
        action = {
            'name': 'Cüzdan Özellikleri',
            'type': 'ir.actions.act_window',
            'res_model': 'info_bayi.terminal_cuzdan_rel',
            'src_model': 'info_bayi.turkpara_banka_rel',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'target': 'new',
            'domain': [('terminal', '=', self.id)],
            'context': {'default_terminal': self.id, 'odoo_bug': True},
            'flags': {'action_buttons': True, 'terminate_parent': True},
        }
        return action

    @api.multi
    def get_cuzdan_view(self):
        self.ensure_one()
        action = {
            'name': 'Cüzdan Özellikleri',
            'type': 'ir.actions.act_window',
            'res_model': 'info_bayi.terminal_cuzdan_rel',
            'src_model': 'info_bayi.turkpara_banka_rel',
            'view_type': 'tree',
            'view_mode': 'tree',
            'view_id': self.env.ref('info_bayi.info_bayi_turkpara_terminal_cuzdan_tree2').id,
            'target': 'new',
            'domain': [('terminal', '=', self.id)],
            'context': {'default_terminal': self.id, 'odoo_bug': True, 'odoo_bug_bitmiyor': True},
        }
        return action


class Terminal_Cuzdan_Rel(models.Model):
    _name = 'info_bayi.terminal_cuzdan_rel'
    _order = 'sequence'

    def _get_other_cuzdans_def(self):
        if self._context.get('is_emri'):
            others = list(map(lambda x: (4, x.id), self.search([('is_emri', '=', self._context.get('is_emri'))])))
        else:
            others = list(
                map(lambda x: (4, x.id), self.search([('terminal', '=', self._context.get('default_terminal'))])))
        return others

    def get_cuzdan_domain(self):
        if self._context.get('params') and self._context.get('params').get('model') == 'info_bayi.turkpara_is_emirleri':
            # active modele gore de ayrilablir
            others = list(
                map(lambda x: x.cuzdan.id, self.search([('is_emri', '=', self._context.get('params').get('id'))])))
        elif self._context.get('default_terminal'):
            others = list(
                map(lambda x: x.cuzdan.id, self.search([('terminal', '=', self._context.get('default_terminal'))])))
        else:
            others = []

        return [('id', 'not in', others), ('serbest', '=', False)]

    cuzdan = fields.Many2one('info_kart.cuzdan', string='Cüzdan', required=True, domain=get_cuzdan_domain)
    terminal = fields.Many2one('info_bayi.turkpara_banka_rel')
    sequence = fields.Integer()
    indirim_ = fields.Float(string='İndirim Oranı')
    state = fields.Selection(selection=[('draft', 'Yeni Kayıt'),
                                        ('sended', 'Sunucuya Gönderildi'),
                                        ('confirmed', 'Cüzdan Aktif'),
                                        ('guncellendi', 'Cüzdan Aktif - Güncellendi'),
                                        ('yasaklandi', 'Cüzdan Yasaklı'),
                                        ('yuksek_oncelik', 'Cüzdan Yüksek Öncelikli'),
                                        ('silindi', 'Cüzdan Silindi'),
                                        ('aktif_edildi', 'Cüzdan Aktif'),
                                        ('error', 'Sunucu Hatası')], default="draft")

    cid = fields.Float(string='CID')
    islem_tipi = fields.Selection(selection=[
        ("2", "Güncelle"),
        ("3", "Sil"),
        ("12", "Pos Cüzdan Yasakla"),
        ("13", "Pos Cüzdan Yüksek Öncelik")], string='İşlem Tipi', required=True, default="2")

    other_cuzdan_ids = fields.One2many('info_bayi.terminal_cuzdan_rel', 'id', compute='_get_other_cuzdans',
                                       default=_get_other_cuzdans_def)

    is_emri = fields.Many2one('info_bayi.turkpara_is_emirleri')
    ws_hata_mesaji = fields.Char('Servis Hata Mesajı')
    master_id = fields.Many2one('info_bayi.terminal_cuzdan_rel')
    pupils = fields.One2many('info_bayi.terminal_cuzdan_rel', 'master_id')
    ac = fields.Text('Açıklama')

    @api.onchange('cuzdan')
    def onchange_cuzdan(self):
        cuzdan_ids = []
        if self._context.get('ekli_cuzdanlar'):
            for c in self._context.get('ekli_cuzdanlar'):
                if c[1]:
                    cuzdan_ids.append(self.env['info_bayi.terminal_cuzdan_rel'].sudo().browse(c[1]).cuzdan.id)
                else:
                    cuzdan_ids.append(c[2]['cuzdan'])

            final_ids = list(set(cuzdan_ids))

            return {'domain': {'cuzdan': [('id', 'not in', final_ids), ('serbest', '=', False)]}}

    @api.multi
    def confirm_and_proceed(self):
        self.ensure_one()

        action = {
            'name': 'Cüzdan Özellikleri',
            'type': 'ir.actions.act_window',
            'res_model': 'info_bayi.terminal_cuzdan_rel',
            'src_model': 'info_bayi.turkpara_banka_rel',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'target': 'new',
            'domain': [('terminal', '=', self.terminal.id)],
            'context': {'default_terminal': self.terminal.id, 'odoo_bug': True},
            'flags': {'action_buttons': True, 'terminate_parent': True},
        }
        # return action

    @api.multi
    def send_pupils(self):
        self.ensure_one()
        if self.pupils:
            for p in pupils:
                p.gonder()
        else:
            raise exceptions(
                "Terminaller İçerisinde Cüzdan Bulunamadı, Lütfen Terminaller İçerisinden Tek Tek Gönderim Yapınız.")

    @api.multi
    def gonder(self):
        for s in self:

            if s.state == 'draft':
                pos_islemleri_2(s, islem_tipi=1, obj_type='cuzdan_rel')
            else:
                pos_islemleri_2(s, obj_type='cuzdan_rel')

    @api.model
    def create(self, values):
        if not values.get('terminal') and values.get('is_emri'):
            obj = super(Terminal_Cuzdan_Rel, self).create(values)
            terminaller = self.env['info_bayi.turkpara_is_emirleri'].browse([values.get('is_emri')]).banka_rels
            for t in terminaller:
                if t.state in ('confirmed', 'sended', 'guncellendi'):
                    c_id = values.get('cuzdan')
                    if c_id not in [x.cuzdan.id for x in t.cuzdanlar]:
                        values_c = copy(values)
                        values_c['is_emri'] = False
                        values_c['terminal'] = t.id
                        values_c['master_id'] = obj.id
                        self.create(values_c)
        else:
            obj = super(Terminal_Cuzdan_Rel, self).create(values)
        obj.gonder()
        return obj

    @api.multi
    def write(self, values):
        prev_cuzdan = self.cuzdan.id

        res = super(Terminal_Cuzdan_Rel, self).write(values)
        if values.get('islem_tipi') or values.get('indirim_') or values.get('sequence'):
            if self.is_emri:
                terminaller = self.is_emri.banka_rels
                for t in terminaller:
                    if t.state in ('confirmed', 'sended', 'guncellendi'):

                        for c in t.cuzdanlar:
                            if c.cuzdan.id == self.cuzdan.id:
                                values_c = copy(values)
                                values_c['is_emri'] = False
                                c.write(values_c)
                        if int(self.islem_tipi) != 3:
                            if self.cuzdan.id not in [x.cuzdan.id for x in t.cuzdanlar]:
                                c = next((x for x in t.cuzdanlar if x.cuzdan.id == prev_cuzdan), None)
                                if c:
                                    values_c = copy(values)
                                    values_c['is_emri'] = False
                                    c.write(values_c)
                                else:
                                    self.copy({'is_emri': False, 'terminal': t.id})

        return res

    @api.multi
    def renew(self):
        self.ensure_one()
        self.state = 'draft'
        self.gonder()
        # state guncellendi, tekrar yolla
        self.gonder()

    @api.multi
    def _get_other_cuzdans(self):

        for s in self:
            if self._context.get('is_emri'):
                others = list(map(lambda x: (4, x.id), self.search([('is_emri', '=', self._context.get('is_emri'))])))
            else:
                others = list(
                    map(lambda x: (4, x.id), self.search([('terminal', '=', s.terminal.id), ('id', '!=', s.id)])))
            s.other_cuzdan_ids = others




