from odoo import models,_
from odoo.exceptions import UserError

class CronJob(models.Model):
    _name = 'info_restaurant_timer.cron_job'

    def cron_send_demo_email(self):

        demo_values = self.env["info_restaurant_options.config"].sudo().search([])

        if demo_values:
            template = self.env.ref('info_restaurant_timer.mail_demo_template')
            template.send_mail(demo_values[-1].id,
                               raise_exception="Mail gönderilemedi Parametreleri ve Mail adresini Kontrol Ediniz.",
                               force_send=True)
        else:
            raise UserError(_('Kayıt bulunamadı.'))

        return True
