# -*- coding: utf-8 -*-

from odoo import models, fields, api
oran_range = 6.01
import xml.etree.ElementTree as etree
class Kampanya(models.Model):
    _name = 'info_kampanya.kampanya'
    
    @api.model
    def _get_kom_kdv_selection(self):
        sel = []
        for i in range(int(oran_range * 100)):
            str_i = str(i)
            if len(str_i) < 3:
                str_i = '0'*(3-len(str_i)) + str_i
            sel.append((str_i[:1] + '.' + str_i[1:],str_i[:1] + ',' + str_i[1:]))
        
        return sel
    
    name            = fields.Char(string='Avantaj Cüzdan Adı')
    kod             = fields.Char(string='Avantaj Cüzdan Kodu')
    poslar          = fields.Many2many('info_bayi.turkpara_banka_rel', string='Tanımlı Poslar', ondelete='restrict',
                                       domain=[('state','in',('confirmed','aktif_edildi'))])
    uye_is_yeri     = fields.Many2one(related='poslar.partner_id')
    kartlar         = fields.Many2many('stock.production.lot',ondelete='restrict',string='Tanımlı Kartlar')
    bas_tar         = fields.Date(string='Başlangıç Tarihi',required=True )
    bit_tar         = fields.Date(string='Bitiş Tarihi',required=True )
    state           = fields.Selection(selection=[('draft','Yeni'),('waiting_confirm','Onay Bekliyor'),('confirmed','Onaylandı'),('cancel','İptal Edildi')])
    
    urunler         = fields.Many2many('info_kampanya.urun',string='Avantaj Cüzdanında Satılacak Ürünleri Seçiniz')
    fatura          = fields.Selection(selection=[('evet','Evet'),('hayir','Hayır')],string='Üye İş Yeri Yemekmatiğe Fatura Kesecek mi?',required=True)
    belge           = fields.Selection(selection=[('dekont','Dekont'),('fatura','Fatura')],string='Yemekmatik A.Ş.\'nin Kart Firmasına Düzenleyeceği Resmi Belgeyi Seçiniz.',required=True)
    desc            = fields.Char(string='Faturaya Ürün Açıklaması Olarak Ne Yazılacak?',required=True)
    kdv             = fields.Selection(selection=[('0','%0'),('1','%1'),('8','%8'),('18','%18')],required=True,string='Kesilecek Faturanın KDV Oranını Seçiniz.')

    komisyon         = fields.Selection(selection=[('evet','Evet'),('hayir','Hayır')],string='Üye İş Yerinden Hizmet Komisyonu Alınacak mı?',required=True)
    kdv_durumu       = fields.Selection(selection=[('evet','Dahil'),('hayir','Hariç')],string='KDV Durumu',required=True)
    kom_kdv_orani    = fields.Selection(selection=_get_kom_kdv_selection, string='Hizmet Komisyon Oranı',default='6.00')
    
    kurallar         = fields.Many2many('info_kampanya.kural',string='Avantaj Cüzdanına Katılım Kurallarını Giriniz')
    avantaj_ismi     = fields.Char(string="Avantaj Cüzdanı Sloganı")
    cuzdan           = fields.Many2one('info_kart.cuzdan',domain=[('serbest','=',False)])
    
    kart_sayisi        = fields.Integer(compute='_get_stats_count')
    uye_is_yeri_sayisi = fields.Integer(compute='_get_stats_count')
    
    @api.multi
    def _get_stats_count(self):
        for s in self:
            s.kart_sayisi = len(s.kartlar)
            s.uye_is_yeri_sayisi = len(set([x.partner_id.id  for x in s.poslar]))

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        
        res = super(Kampanya, self).fields_view_get(view_id=view_id, view_type=view_type,toolbar=toolbar, submenu=submenu)
        
        doc = etree.XML(res['arch'])
        if view_type in ('form','kanban','tree') and self._context.get('disable_create'):
            doc.set("create", 'false')
            res['arch'] = etree.tostring(doc)
        
        return res
    
    @api.multi
    def go_to_waiting_confirm(self):
        self.ensure_one()
        self.state = 'waiting_confirm'
    
    @api.multi
    def go_to_confirm(self):
        self.ensure_one()
        action={
            'name':'Avantaj Cüzdanını Onayla',
            'type':'ir.actions.act_window',
            'res_model':'info_kampanya.kampanya_onay',
            'src_model':'info_kampanya.kampanya',
            'view_type':'form',
            'view_mode':'form',
            'target':'new'
        }
        return action
    @api.multi
    def go_to_cancel(self):
        self.ensure_one()
        self.state = 'cancel'
        
    @api.multi
    def go_to_draft(self):
        self.ensure_one()
        self.state = 'draft'
        
class KampanyaUrun(models.Model):
    _name = 'info_kampanya.urun'
    
    name = fields.Char('Ürün Adı', required=True)
    kdv  = fields.Selection(selection=[('0','%0'),('1','%1'),('8','%8'),('18','%18')],required=True,string='KDV')
    
    @api.model
    def name_get(self):
        res = []
        for record in self:
            _name = record.name + u' - ' + kdv
            res.append((record['id'],_name ))
        return res

class KampanyaKural(models.Model):
    _name = 'info_kampanya.kural'
    
    name  = fields.Char('Kural Adı', required=True)
    kod   = fields.Char('Kural Kodu')
    
    
    