# -*- coding: utf-8 -*-

from openerp import models, fields, api, exceptions
import json
import requests,datetime,calendar
from collections import OrderedDict
from ..util.util import batarya_selection,renk_selection
from .webserviceopt import Rapor,RaporBundles,pos_islemleri,pos_islemleri_2

urun_map_mobil = {#barkod,kamera,parmak
    'SPMR':[False,False,False],
    'BSMR':[True,False,False],
    'KSMR':[False,True,False],
    'PPMR':[True,True,False],
    'UPMR':[True,True,True],
    'PSMR':[False,False,True],
    'PBMR':[True,False,True],
    'PKMR':[False,True,True],
}

urun_map_sabit={
    'SPSR':[False,False,False],
    'BSSR':[True,False,False],
    'KSSR':[False,True,False],
    'PPSR':[True,True,False],
    'UPSR':[True,True,True],
    'PSSR':[False,False,True],
    'PBSR':[True,False,True],
    'PKSR':[False,True,True]
}

class getRaporParams(models.Model):
    _name='info_bayi.get_rapor_wizard'
    
    def _get_default_st_date(self):
         now = datetime.datetime.now()
         m   = str(now.month)
         if len(m) == 1:
            m = '0' + m
         return str(now.year) + '-' + m + '-' + '01'
    def _get_default_sp_date(self):
         now = datetime.datetime.now()
         m   = now.month
         d   = calendar.monthrange(now.year,m)[1]
         m   = str(m)
         if len(m) == 1:
            m = '0' + m
         return str(now.year) + '-' + m + '-' + str(d)
    
    def _get_default_partner(self):
        p_id = self._context.get('active_id')
        if not p_id:
            p_id = self.env.user.sudo().company_id.partner_id.id
        return p_id
    
    def _get_default_extre( self ):
        extre_dict = get_extre(self,False)
        return extre_dict
    
    str_date        = fields.Date(string='Başlangıç Tarihi',default=_get_default_st_date)
    stp_date        = fields.Date(string='Bitiş Tarihi',default=_get_default_sp_date)
    partner_id      = fields.Many2one('res.partner', string='Üye İş Yeri',default=_get_default_partner)
    etxre_lines     = fields.One2many('info_bayi.rapor','get_rapor_wizard',string="Rapor Satırları",domain=[('kapanacak','!=',True)])
    etxre_lines_kap = fields.One2many('info_bayi.rapor','get_rapor_wizard',string="Rapor Satırları",domain=[('kapanacak','=',True)])
    rapor_tipi      = fields.Selection(selection=[("1","Pos İşlemleri")],string="Rapor Tipi",default="1")
    uye_is_yeri_no  = fields.Char()
    terminal_no     = fields.Char()
    
    @api.multi
    def get_rapor(self):
        
        if self.sudo().partner_id.turkpara_e_para_hesap or self.sudo().partner_id.parent_id.turkpara_e_para_hesap:
            #print self._context.get('bundle')
            if self._context.get('bundle'):
                RaporBundles(self,self._context.get('bundle'))
                state = True
            else:
                state,response = Rapor(self,get_by_all=self._context.get('get_by_all'))
        else:
            raise exceptions.ValidationError("E- Hesap Numarası Buluanmadı, Üye İş Yeri Başvurusunun Tamamlandığından Emin Olunuz")
        if state:
            action = {
                'name': 'Pos Hareketleri',
                'type': 'ir.actions.act_window',
                'res_model': 'info_bayi.get_rapor_wizard',
                'view_type': 'form',
                'view_mode': 'form',
                'res_id':self.id,
                'target': 'new',
                'context': {'get_by_all': True}
            }
            return action

        else:
            msj = 'Pos Üzerinde İşlem Bulunamadı. : %s - %s'%(response.Sonuc,response.Sonuc_Str)
            r = "/web/payresult?inner_message=1&success=%s&msj=%s"%(-1,msj)
            return {
                        'type': 'ir.actions.act_url',
                        'url': r,
                        'target': 'popup'
                    }
    @api.multi
    def get_rapor_excel(self):
        
        if self.sudo().partner_id.turkpara_e_para_hesap or self.sudo().partner_id.parent_id.turkpara_e_para_hesap:
            
            if self._context.get('bundle'):
                RaporBundles(self,self._context.get('bundle'))
                state = True
            else:
                state,response = Rapor(self,get_by_all=self._context.get('get_by_all'))
            
            f_list = [["_rowOrder","No"],
                      ["Tarih", "Tarih"],
                      ["Terminal_No","Terminal No"],
                      ["Isyeri_No","Is Yeri No"],
                      ["Pan_masked", "Kart No"],
                      ["BKM_ID", "BKM Id"],
                      ["Sube_iliskili_name","Sube Adi"],
                      ["MCC_Kodu", "Mcc Kodu"],
                      ["Tip", "Tip"],
                      ["Tutar", "Tutar"]
                      ]
           
            r = "/excel_c/info_bayi.get_rapor_wizard/%s/etxre_lines/%s"%(self.id,f_list)
            
            return {
                    'type': 'ir.actions.act_url',
                    'url': r,
                    'target': 'new'
                    }
        else:
            raise exceptions.ValidationError('Üye İş Yeri E-Hesap No ulunamadı')

class rapor(models.Model):
    _name = 'info_bayi.rapor'
    get_rapor_wizard    = fields.Many2one("info_bayi.get_rapor_wizard")
    Terminal_No         = fields.Char("Terminal No")
    Isyeri_No           = fields.Char("İş Yeri No")
    Tarih               = fields.Datetime("Tarih")
    BKM_ID              = fields.Char("BKM ID")
    Pan                 = fields.Char("Pan")
    Pan_masked          = fields.Char("Kart No")
    Sube                = fields.Char("Banka Şube")
    Sube_iliskili       = fields.Many2one("res.partner")
    Sube_iliskili_name  = fields.Char(related='Sube_iliskili.display_name') 
    Cuzdan              = fields.Char("Cüzdan")
    Tip                 = fields.Char("Tip")
    Tutar               = fields.Float("Tutar")
    curr                = fields.Many2one('res.currency',default=lambda self: self.env['res.currency'].search([('name','=','TRY')], limit=1))
    _rowOrder           = fields.Integer()
    _id                 = fields.Char()
    Uye_Isyeri          = fields.Char()
    Dekont_ID           = fields.Char()
    MCC_Kodu            = fields.Char("Mcc Kodu")
    Kart_Banka          = fields.Char("Pos Banka")
    kapanacak           = fields.Boolean(default=False)
    
class AdetReportByBayi(models.TransientModel):
    _name = 'info_bayi.adet_report_bayi'
    
    start_date = fields.Date('Başlangıç Tarihi')
    end_date   = fields.Date('Bitiş Tarihi')
    partner_id = fields.Many2many('res.partner',string='İş Ortağı Seç',domain=[('dealer','=',True)])
    
    def get_report(self):
        return {
                'type': 'ir.actions.act_url',
                'url': '/report/pdf/info_bayi.adet_report_bayi_pdf/' + str(self.id),
                'target': 'new'
            }

class dummy:
    pass

class AdetReportByBayiReport(models.AbstractModel):
    _name = 'report.info_bayi.adet_report_bayi_pdf'

    @api.model
    def render_html(self, docids,data=None):
        
        report_obj = self.env['report'].sudo()
        report = report_obj._get_report_from_name('info_bayi.adet_report_bayi_pdf')
        obj    = self.env['info_bayi.adet_report_bayi'].sudo().browse( docids )
        if len( obj ) > 1:
            raise exceptions.ValidationError("Bir Seferde Bir Rapor Basabilirsiniz. Lütfen Raporları Tek Tek Basınız")
        
        partner_ids = obj.partner_id
        if not partner_ids:
            partner_ids = self.env['res.partner'].sudo().search([('dealer','=',True),
                                                                ('company_id','!=',1),
                                                                ('is_company','=',True)])
        d_list = []
        for p in partner_ids:
            
            d         = dummy()
            d.name    = p.name
            dict_by_p = []
            p_users = self.env['res.users'].search([('company_id','=',p.company_id.id)])
            
            for u in p_users:
                adet    = len(self.env['res.partner'].sudo().search([('create_uid','=',u.id),
                                                            ('create_date','>=',obj.start_date),
                                                            ('create_date','<=',obj.end_date),
                                                            ('is_company','=',True),
                                                            ('esnaf','=',True),
                                                        ]))
                
                u2      = dummy()
                u2.name = u.name
                u2.adet = adet
                dict_by_p.append (u2)
            d.dict_by_p = dict_by_p
            d_list.append( d )
        
        docargs = {
            'doc_ids': [obj.id],
            'doc_model': report.model,
            'docs': obj,
            'd_list':d_list,
        }
        #print docargs
        
        return report_obj.render('info_bayi.adet_report_bayi_pdf', docargs)

class TeklifWizardKK(models.TransientModel):
    _name          = 'info_bayi.urun_sec_wizard_kk'

    partner_id     = fields.Many2one('res.partner',string='Üye İş Yeri / Esnaf',domain=[('is_company','=',True),('esnaf','=',True)],required=True)
    kampanya       = fields.Many2one('info_bayi.yemekmatik_kampanya',string='Kampanya', requred=True)
    tutar          = fields.Float(related = 'kampanya.tutar')

    sermaye_sirketi = fields.Boolean(related='partner_id.sermaye_sirketi')
    sahis_sirketi   = fields.Boolean(related='partner_id.sahis_sirketi')
    ortaklik        = fields.Boolean(related='partner_id.ortaklik')

    adres           = fields.Char(related='partner_id.street')
    adres2          = fields.Char(related='partner_id.street2')

    ilce            = fields.Many2one(related='partner_id.ilce')
    il              = fields.Many2one(related='partner_id.city_combo')

    vergi_no        = fields.Char(related='partner_id.taxNumber')
    tckn            = fields.Char(related='partner_id.tckn')



    @api.multi
    def kk_odeme_yap( self ):

        self.ensure_one()
        view_id = self.env['ir.ui.view'].sudo().search([('name','=','info_bayi.odeme.form.wizard')]).id
        if self.tutar > 0:
            return {
                'type': 'ir.actions.act_window',
                'name': 'Kredi Kartı Taksit Oranları',
                'res_model': 'info_bayi.odeme_yap',
                'src_model': 'info_bayi.urun_sec_wizard_kk',
                'view_mode': 'form',
                'view_type':'form',
                'views': [[view_id, "form"]],
                'target': 'new',
                'context':{'default_partner_id':self.partner_id.id,
                        'default_tutar':self.tutar,
                       'search_default_group_by_resim':1,
                       },
                }
        else:
            raise exceptions.ValidationError('Lütfen Tutar Giriniz.')


class DocWizard(models.TransientModel):
    _name = 'info_bayi.doc_wizard'

    @api.model
    def _get_default_partner(self):
        if self._context.get('active_id'):
            return self.env['res.partner'].browse([self._context.get('active_id')]).id
        else:
            raise exceptions.ValidationError('Aktif Partner Alınamadı, Tekrar Login Olarak Deneyiniz.')

    partner_id = fields.Many2one('res.partner', default=_get_default_partner)
    doc_receive_method = fields.Selection(related='partner_id.doc_receive_method',readonly=False)
    doc_katalog_no = fields.Char(related='partner_id.doc_katalog_no', size=4)
    doc_raf = fields.Char(related='partner_id.doc_raf',readonly=False, size=4)
    doc_goz = fields.Char(related='partner_id.doc_goz',readonly=False,size=4)
    doc_klasor = fields.Char(related='partner_id.doc_klasor',readonly=False,size=4)
    doc_evrak_no = fields.Char(related='partner_id.doc_evrak_no',readonly=False)
    doc_teslim_alan = fields.Char(related='partner_id.doc_teslim_alan',readonly=False)
    state = fields.Selection(related='partner_id.state',readonly=False)

    @api.multi
    def onsave(self):
        if self.partner_id.state != 'muhasebe':
            self.partner_id.state = 'giris'
        return True

    @api.multi
    def onsavetomuhasebe(self):
        obj_sequence = self.env['ir.sequence'].sudo()
        idd = obj_sequence.next_by_code('evrak.muhasebe_sequence')
        self.partner_id.doc_evrak_no = idd
        self.partner_id.state = 'muhasebe'
        return True

class sube_form_iptal(models.TransientModel):
    _name = 'info_bayi.sube_form_iptal'
    
    @api.model
    def _get_default_post(self):
        return self._context.get('active_id')
        
    pos_id = fields.Many2one('info_bayi.turkpara_banka_rel',default = _get_default_post)
    red_aciklamasi      = fields.Text()

    @api.multi
    def onsave(self):
        self.pos_id.red_aciklamasi = self.red_aciklamasi
        if self.pos_id.state != 'draft':
            self.pos_id.islem_tipi = '3'
            pos_islemleri_2(self.pos_id)
        else:
            self.pos_id.state = 'silindi'
        return True
    
class PartnerDeclineWizard(models.TransientModel):
    _name = 'info_bayi.partner_decline_wizard'

    def  get_pos_opt(self):
        ab        = self.env['info_bayi.turkpara_aktif_banka'].sudo().search([])
        selection = []
        c = 1
        for b in ab:
            t = '%s Posu için %s yetkililerini yönlendirmemizi istiyor.'%(b.name.name,b.name.name)
            selection.append((str(c),t))
            c += 1
        
        selection.append((str(c),'Aktif Bankalarımızla Çalışmak İstemiyor.'))
        selection.append((str(c+1),'Kampanyadan İnfoteks Yazarkasa almak istiyor.(2 yıl Komisyon %5) (Yazarkasa ücreti:2.350 TL kdv Dahil)'))
        return selection

    @api.model
    def _get_default_partner(self):
        if self._context.get('active_id'):
            return self.env['res.partner'].browse([self._context.get('active_id')]).id
        else:
            raise exceptions.ValidationError('Aktif Partner Alınamadı, Tekrar Login Olarak Deneyiniz.')

    partner_id = fields.Many2one('res.partner', default=_get_default_partner)
    red_aciklamasi = fields.Text(related='partner_id.red_aciklamasi',readonly=False)
    m_tercih = fields.Selection(selection=get_pos_opt, string="Müşteri Pos Tercihi")
    m_tercih_istemiyor = fields.Many2one('info_bayi.musteri_istemiyor_states', string='Müşteri Tercihi')
    
    @api.multi
    def onsave(self):
        self.partner_id.red_aciklamasi = self.red_aciklamasi
        if self._context.get('pos_sorunu'):
            pos_opt   = self.get_pos_opt()
            post_text = list(filter(lambda x:x if x[0] == self.m_tercih else None,pos_opt))[0][1] 
        
            self.partner_id.red_aciklamasi = 'Müşteri Tercihi : ' + post_text + '\n' + self.red_aciklamasi
            if self.m_tercih == pos_opt[-1][0]:
                self.partner_id.state = 'infoteks_alacak'
            elif self.m_tercih == pos_opt[-2][0]:
                self.partner_id.state='aktiflerle_calismayacak'
            else:
                self.partner_id.state = 'bankaya_iletildi'
        elif self._context.get('musteri_sorunu'):
            if self.m_tercih_istemiyor.state == 'dusunecekmis':
                self.partner_id.state = 'dusunecekmis'
            elif self.m_tercih_istemiyor.state == 'istemiyor':
                if self.partner_id.pos_goster == 'var':
                    self.partner_id.state='istemiyor_poslu'
                else:
                    self.partner_id.state='istemiyor_possuz'
            else:
                self.partner_id.state='musteri_istemiyor'
        else:
            self.partner_id.state='declined'
        return True
    
class AccountInvoiceRefund(models.TransientModel):
    _inherit =  "account.invoice.refund"
    
    def get_total_amount_core( self ):
        
        source_inv_id   = self._context.get('active_id')
        inv_obj         = self.env.get('account.invoice')
        source_inv      = inv_obj.browse( source_inv_id )
        final_balance   = source_inv.amount_total
        previous_refund = inv_obj.search([('origin','=',source_inv.number),('type','ilike','refund'),('state','!=','cancel')])
        for f in previous_refund:
            final_balance -= f.amount_total
            
        return final_balance
    
    @api.model
    def _get_total_amount_def(self) :
        return self.get_total_amount_core()
        
    amount  = fields.Float(compute='_get_total_amount', default=_get_total_amount_def)
    
    @api.multi
    def _get_total_amount(self) :
        self.ensure_one()
        self.amount = self.get_total_amount_core()
        
        #aktif id, iade girilen satış faturası id..
        #origin satıs faturası adı olan , tipi refund olan iade faturası var mı diye bak
        #tutarı karsılastır...

'''
class AccountInvoiceCancel(models.TransientModel):
    _inherit = "account.invoice.cancel"
    _description = "Cancel Account Invoices"
    
    @api.model
    def get_invoice(self):
        #print self._context.get('active_id')
        return self.env['account.invoice'].sudo().browse(self._context.get('active_id')).type
    
    
    invoice_type          = fields.Char(default=get_invoice,compute='getinvoice_cont')
    has_refund_invoice    = fields.Boolean(string="İade Faturası Var")
    refund_invoice_number = fields.Char(string="İade Fatura No")
    refund_invoice_date   = fields.Date(string="İade Fatura Tarihi")
    
    @api.multi
    def getinvoice_cont( self ):
        for s in self:
            s.invoice_type = self.env['account.invoice'].sudo().browse(self._context.get('active_id')).type
    
    @api.multi
    def invoice_cancel(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        if not self.has_refund_invoice:
            for record in self.env['account.invoice'].browse(active_ids):
                # if record.state in ('cancel', 'paid'):
                #     raise UserError(
                #         _("Selected invoice(s) cannot be cancelled as they are already in 'Cancelled' or 'Done' state."))
                if record.state not in  ['draft','proforma']:
                    record.journal_id.sudo().update_posted = True
                record.action_cancel()
                record.journal_id.sudo().update_posted = False
        else:

            for record in self.env['account.invoice'].browse(active_ids):
                mixed_context   = self._context.copy()
                mixed_context['active_id']    = record.id
                mixed_context['active_ids']   = [record.id]
                mixed_context['active_model'] = 'account.invoice'
                description =  u'İptal Edilen Satış Fatura No: ( %s-%s )'%(record.number, record.fatura_no )
                refund_dict = {'description':description}
                
                refund_inv  = self.env['account.invoice.refund'].with_context( mixed_context ).create( refund_dict )
                donen       = refund_inv.invoice_refund()
                if donen:
                    domain = donen.get('domain')
                    if domain:
                        refund_inv = self.env['account.invoice'].search( domain )
                        if refund_inv:
                            refund_inv.fatura_no    = self.refund_invoice_number
                            refund_inv.date_invoice = self.refund_invoice_date
                            refund_inv.action_invoice_open()

        return {'type': 'ir.actions.act_window_close'}
'''

class StockAdetRel(models.TransientModel):
    _name = 'info_bayi.stock_cihaz_adet_rel'
    
    product_id = fields.Many2one('product.product',string='Cihaz',domain=[('tracking','=','serial')])
    qty        = fields.Integer(string='Adetler')
    grup_id    = fields.Many2one('info_bayi.stock_cihaz_adet')
    lots       = fields.Many2many('stock.production.lot',string='Seri Nolar')
    
class StockAdet(models.TransientModel):
    _name = 'info_bayi.stock_cihaz_adet'
  
    @api.model
    def default_get(self, f_list):
        sup        = super(StockAdet, self).default_get( f_list )
        partner_id = self._context.get('active_id')
        comp       = self.env['res.company'].sudo().search([('partner_id','=',partner_id)])
        q          = "select count(id) as qty,product_id from stock_production_lot where company_id = %s and lot_state in ('draft','purchased','returned_from_customer') group by product_id"%comp.id
        rels       = []
        self.env.cr.execute( q )
        res = self.env.cr.dictfetchall()
        for r in res:
            lot_ids  = self.env['stock.production.lot'].search([('company_id','=',comp.id),
                                                                ('lot_state','in',('draft','purchased','returned_from_customer')),
                                                                ('product_id','=',r['product_id'])],order='name asc')
            lot_ids  = list(map(lambda x:(4,x.id),lot_ids))
            res_dict = {'product_id':r['product_id'],
                        'qty':r['qty'],
                        'lots':lot_ids
                        }
            new_s = self.env['info_bayi.stock_cihaz_adet_rel'].create( res_dict )
            rels.append((4,new_s.id))
            
        sup['products'] = rels
        sup['partner_id'] = partner_id
        
        return sup

    partner_id = fields.Many2one('res.partner',string='Bayi')
    products   = fields.One2many('info_bayi.stock_cihaz_adet_rel','grup_id',string='Cihaz Adetleri')
    
    @api.multi
    def excele_aktar(self):
        params      = self.env.get('ir.config_parameter').get_param('web.base.external_url')
        url = params + "/web/xlsserialnum/" + str(self.id)
        #print url
        return  {
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'self'
        }
    
class is_emri_sube_degistir_wizard(models.TransientModel):
    
    _name ='info_bayi.is_emri_sube_degistir'
    
    @api.model
    def get_dynamic_blebles(self):
        r = self.env['info_bayi.turkpara_banka_rel'].browse(  self._context.get('active_id') )
        
        domain = [('parent_id','=',-1)]
        if r:
            partner = r.partner_id
            if partner.parent_id:
                domain = ['|',('parent_id','=',partner.parent_id.id),'&',
                          ('id','=',partner.parent_id.id),
                          ('id','!=',partner.id)]
                
            else:
                domain = [('parent_id','=',partner.id)]
            
            domain.extend( [('esnaf','=',True),
                            ('state','in',('islak_bekleniyor','confirmed','giris','muhasebe')),
                            ('is_company','=',True),
                            ('turkpara_e_para_hesap','!=',False)])
                
        return domain
    
    partner_id   = fields.Many2one('res.partner',domain=get_dynamic_blebles)
    sermaye_sirketi = fields.Boolean(related='partner_id.sermaye_sirketi')
    sahis_sirketi   = fields.Boolean(related='partner_id.sahis_sirketi')
    ortaklik        = fields.Boolean(related='partner_id.ortaklik')
    
    adres           = fields.Char(related='partner_id.street')
    adres2          = fields.Char(related='partner_id.street2')
    
    ilce            = fields.Many2one(related='partner_id.ilce')
    il              = fields.Many2one(related='partner_id.city_combo')
    
    vergi_no        = fields.Char(related='partner_id.taxNumber')
    tckn            = fields.Char(related='partner_id.tckn')
    termianl_rel    = fields.Many2one('info_bayi.turkpara_banka_rel',default=lambda self:self._context.get('active_id'))
    
    terminal_no     = fields.Char(related='termianl_rel.terminal_no')
    uye_is_yeri_no  = fields.Char(related='termianl_rel.uye_isyeri_no')
    
    @api.multi
    def sube_degistir(self):
        self.ensure_one()
        i = self.env['info_bayi.turkpara_is_emirleri'].sudo().search([('partner_id','=',self.partner_id.id)])
        if i:
            self.termianl_rel.isemri = i.id
        else:
            i_dict = dict(partner_id = self.partner_id.id,
                          banka_rels = [(4,self.termianl_rel.id)])
            self.env['info_bayi.turkpara_is_emirleri'].sudo().create( i_dict )
        
        msj = 'Pos Seçilen Şubeye Taşındı...'
        r = "/web/payresult?inner_message=1&success=%s&msj=%s"%(1,msj)
        return {
                    'type': 'ir.actions.act_url',
                    'url': r,
                    'target': 'popup'
                }
