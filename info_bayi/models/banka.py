# -*- coding: utf-8 -*-

from odoo import models, fields, api
class Banka(models.Model):
    _name = 'info_extensions.acquirer'
    bkm_id = fields.Integer('BKM Banka ID')

    name = fields.Char('Banka Adı')
    name_eng = fields.Char('Banka Name Eng')
    kurum_kodu = fields.Char('Kurum Kodu')
    kurum_token = fields.Char('Kurum Token')
    odeme_sistemleri_url = fields.Char('Ödeme Sistemleri URL')
    odeme_sistemleri_usr = fields.Char('Ödeme Sistemleri Client ID')
    odeme_sistemleri_key = fields.Char('Ödeme Sistemleri Key')
    send_repair_orders_to_bkm = fields.Boolean("Onarım siparişleri BKM'ye bildirilsin mi?")
    company_id = fields.Many2one('res.company', string = 'Firma / Bayi ')
    contacts   = fields.One2many('info_extensions.banka_contact','banka')
    default_banka = fields.Boolean('Varsayılan Sanal Pos')

    EST_MERCHANT_ID = fields.Char('Banka Kodu')
    EST_3D_KEY = fields.Char('Kullanıcı Şifresi')
    EST_3D_user = fields.Char('Kullanıcı Adı')
    url = fields.Char('İstek Adresi')
    url_header = fields.Char('Post Adresi')
    api_url = fields.Char('API Adresi')


    def secimiIptalEt(self):
        seciliBanka = self.env['info_extensions.acquirer'].search([('default_banka','=',True)])

        for bank in seciliBanka:
            bank.default_banka = False


    @api.model
    def create(self, value):
        if value.get('default_banka'):
            self.secimiIptalEt()

        return super(Banka, self).create(value)

    @api.multi
    def write(self, value):
        if value.get('default_banka'):
            self.secimiIptalEt()

        return super(Banka, self).write(value)




class banka_contact ( models.Model):

   _name = 'info_extensions.banka_contact'

   name    = fields.Char(string='Banka Personel Adı')
   gorev   = fields.Char(string='Görevi')
   eposta  = fields.Char(string='E-Posta')
   tel_no  = fields.Char(string='Tel No', size=14)
   banka = fields.Many2one('info_extensions.acquirer', string='Banka')