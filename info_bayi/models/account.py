# -*- coding: utf-8 -*-
from odoo import models, fields, api,exceptions

class account(models.Model):
    _inherit = 'account.account'
class account_journal(models.Model):
    _inherit = 'account.journal'
    
    banka    = fields.Many2one('info_extensions.acquirer',string='Banka')
    
    @api.multi
    def name_get(self):
        result = []
        for j in self:
            name =j.name
            if j.banka:
                name =j.banka.name   +  '-' +  name
            
            result.append((j.id, name))
        return result
    