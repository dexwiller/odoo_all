# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions
import json
import openerp.addons.decimal_precision as dp


class account_invoice_line(models.Model):
    _inherit = 'account.invoice.line'
    parent_type = fields.Selection(related='invoice_id.type')

    @api.onchange('price_unit')
    def onchage_price_unit(self):

        res = {}
        if self.invoice_id.type == 'out_refund' or self.invoice_id.type == 'in_refund':
            self.invoice_id._compute_amount()
            if self.invoice_id.amount_untaxed > self._origin.invoice_id.amount_untaxed:
                res['warning'] = {'title': 'Hatalı Tutar',
                                  'message': 'İade Tutarı Sistemin Otomatik Belirlediği Tutardan Fazla Olamaz.'}

                self.price_unit = self._origin.price_unit
                self.invoice_id._compute_amount()
                return res


class account_invoice(models.Model):
    _inherit = 'account.invoice'

    fatura_no = fields.Char('Fatura No', size=8, readonly=True, states={'draft': [('readonly', False)]}, index=True,
                            copy=False)
    date_invoice = fields.Datetime(string='Invoice Date',
                                   readonly=True, states={'draft': [('readonly', False)]}, index=True,
                                   help="Keep empty to use the current date", copy=False)

    state = fields.Selection(selection_add=[('send_to_supp', 'Ödeme Tedarikçiye Aktarıldı'),
                                            ('waiting_cancel_confirm', 'İptal İçin Muhasebe Onayı Bekliyor.')])

    gib_onay_kodu = fields.Char(string='Gib Onay Kodu')
    cihaz_almismi = fields.Boolean(compute='_get_cihaz_almis_mi')
    seri_no_bas = fields.Boolean('Seri No\'lar Faturaya Basılsın mı?', default=True)
    imei_bas = fields.Boolean('Imei No\'lar Faturaya Basılsın mı?', default=False)
    mtm_bas = fields.Boolean('Mtm No\'lar Faturaya Basılsın mı?', default=False)
    eku_bas = fields.Boolean('Ekü No\'lar Faturaya Basılsın mı?', default=False)

    generated_origin = fields.Many2one('account.invoice', 'Otomatik Kaynak')

    lot_ids = fields.One2many('stock.production.lot', 'id', string='Serial Numbers', compute='_compute_lot_ids')
    amount_discount = fields.Float(string='Discount', digits=dp.get_precision('Account'),
                                   store=True, readonly=True, compute='_compute_amount', track_visibility='always')

    @api.one
    @api.depends('invoice_line_ids.price_subtotal', 'tax_line_ids.amount', 'currency_id', 'company_id', 'date_invoice',
                 'type', 'invoice_line_ids.discount')
    def _compute_amount(self):

        super(account_invoice, self)._compute_amount()
        self.amount_discount = sum(
            line.price_unit * line.quantity - line.price_subtotal for line in self.invoice_line_ids)

    amount_total_words = fields.Char(string='Total',
                                     store=False, readonly=True, compute='_compute_amount_total_words',
                                     track_visibility='always')

    @api.one
    @api.depends('amount_total')
    def _compute_amount_total_words(self):
        # -------------------------------------------------------------
        # ENGLISH
        # -------------------------------------------------------------

        to_10 = (u'SIFIR', u'BİR', u'İKİ', u'ÜÇ', u'DÖRT', u'BEŞ', u'ALTI',
                 u'YEDİ', u'SEKİZ', u'DOKUZ')
        tens = (u'ON', u'YİRMİ', u'OTUZ', u'KIRK', u'ELLİ', u'ALTMIŞ', u'YETMİŞ', u'SEKSEN', u'DOKSAN')

        denom = ('',
                 u'BİN', u'MİLYON', u'MİLYAR', u'TRİLYON', u'TRİLYAR',
                 u'KATRİLYON', u'KATRİLYAR')

        def _convert_nn(val):
            """convert a value < 100 to turkish.
            """
            if val < 10:
                return to_10[val]
            for (dcap, dval) in ((k, 10 + (10 * v)) for (v, k) in enumerate(tens)):
                if dval + 10 > val:
                    if val % 10:
                        return dcap + '' + to_10[val % 10]
                    return dcap

        def _convert_nnn(val):
            """
                convert a value < 1000 to turkish, special cased because it is the level that kicks
                off the < 100 special case.  The rest are more general.  This also allows you to
                get strings in the form of 'forty-five hundred' if called directly.
            """
            word = ''
            (mod, rem) = (val % 100, val // 100)

            if rem > 0:
                if rem == 1:
                    word = u'YÜZ'
                else:
                    word = to_10[rem] + u'YÜZ'
                if mod > 0:
                    word += ''
            if mod > 0:
                word += _convert_nn(mod)
            return word

        def turkish_number(val):

            if val < 10:
                return to_10[val]
            if val < 100:
                return _convert_nn(val)
            if val < 1000:
                return _convert_nnn(val)

            for (didx, dval) in ((v - 1, 1000 ** v) for v in range(len(denom))):
                if dval > val:
                    mod = 1000 ** didx
                    l = val // mod
                    r = val - (l * mod)
                    if l == 1:
                        ret = denom[didx]
                    else:
                        ret = _convert_nnn(l) + '' + denom[didx]

                    if r > 0:
                        ret = ret + '' + turkish_number(r)
                    return ret

        def amount_to_text_tr(number, currency):
            number = '%.2f' % number
            units_name = currency
            list = str(number).split('.')
            start_word = turkish_number(int(list[0]))
            end_word = turkish_number(int(list[1]))
            cents_number = int(list[1])
            ###print currency
            if currency == 'TL':
                cents_name = u'Kr.'
            else:
                cents_name = u'Cent'
            return ' '.join(list(filter(None, [start_word, units_name, end_word, cents_name])))

        self.amount_total_words = amount_to_text_tr(self.amount_total, self.currency_id.symbol)

    current_time = fields.Char(string='Current Time', compute='_compute_current_time', track_visibility='always')

    @api.one
    def _compute_current_time(self):
        import pytz
        from datetime import datetime
        date_now = datetime.today()
        if self._context.get('tz', False):
            time_zone = self._context['tz']
            tz = pytz.timezone(time_zone)
            tzoffset = tz.utcoffset(date_now)
            date_now = date_now + tzoffset
        self.current_time = date_now.strftime('%H:%M:%S')

    @api.one
    def _compute_lot_ids(self):
        picking_obj = self.env['stock.picking'].search([('origin', '=', self.origin), ('state', '!=', 'cancel')])
        lots = []
        for picks in picking_obj:
            if picks.pack_operation_exist:
                for packs in picks.pack_operation_ids:
                    if packs.product_id.tracking == 'serial':
                        for p in packs.pack_lot_ids:
                            lots.append(p.lot_id.id)

        self.lot_ids = self.env['stock.production.lot'].browse(lots)

    _order = 'id desc'

    @api.one
    def _get_cihaz_almis_mi(self):
        ###print self.lot_ids, len(self.env['res.company'].sudo().search([('partner_id','=',self.partner_id.id)]))
        if self.lot_ids and len(self.env['res.company'].sudo().search([('partner_id', '=', self.partner_id.id)])) == 0:
            self.cihaz_almismi = True

    @api.multi
    def invoice_validate(self):
        res = super(account_invoice, self).invoice_validate()
        for s in self:
            # if s.type == 'out_invoice':
            if not s.fatura_no or not s.date_invoice:
                raise exceptions.ValidationError("Lütfen Fatura Tarihi ve Numarası Giriniz.")

            aml_ids = json.loads(s.outstanding_credits_debits_widget)

            ###print 'Burada ---------------------- : ' , aml_ids

            if aml_ids:
                for ml in aml_ids.get('content'):
                    if s.state == 'open':
                        if not self.env['account.move.line'].browse([ml['id']]).reconciled:
                            s.assign_outstanding_credit(ml['id'])
                    else:
                        break
            # print s.type ,' -- : -- ',s.generated_origin
            if s.type == 'out_refund' and s.generated_origin:
                if s.generated_origin.state in ('draft', 'proforma', 'proforma2'):
                    for s_line in s.invoice_line_ids:
                        updated = list(map(lambda x: setattr(x, 'price_unit',
                                                             s_line.price_unit) if x.product_id.id == s_line.product_id.id else None,
                                           s.generated_origin.invoice_line_ids))
                    s.generated_origin._onchange_invoice_line_ids()
                    s.generated_origin.compute_taxes()
                    s.generated_origin.fatura_no = s.fatura_no
                    s.generated_origin.action_invoice_open()

        return res

    @api.multi
    def action_send_payment(self):
        self.ensure_one()
        total_payment_amount = 0.00

        for p in self.payment_ids:
            if p.state == 'posted':
                total_payment_amount += p.amount

        return {
            'type': 'ir.actions.act_window',
            'name': 'Ödeme Aktar',
            'res_model': 'account.payment',
            'src_model': 'account.invoice',
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'self',
            'context': {'default_amount': total_payment_amount,
                        'default_payment_type': 'outbound',
                        'default_partner_type': 'supplier',
                        'default_inv_sended_payment': self.id
                        },
        }
