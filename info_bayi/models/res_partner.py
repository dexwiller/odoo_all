# -*- coding: utf-8 -*-

from odoo import models, fields, api, _, exceptions
from datetime import datetime
from odoo.exceptions import UserError
from .util_models import merge_no_duplicates
oran_range = 6.01
from .webserviceopt import basvuru_kayit, doc_yolla
import xml.etree.ElementTree as etree
import barcode, time
from barcode.writer import ImageWriter
from io import StringIO,BytesIO
from base64 import b64encode, encodestring,b64decode
import qrcode
import urllib3
import functools
from odoo.modules import get_resource_path
from odoo.tools import flatten
pos_img_name = 'POS TALİMATI (KLT.FRM.03-V01-30102018)'
kvkk_name    = 'KVKK AYDINLATMA METNİ (KLT.FRM.02-V01-30102018)'
MERKEZ = 1
import re, os
import base64

class uye_is_yeri_iban(models.Model):
	_name = 'info_bayi.uye_is_yeri_iban'
	
	def _get_defalt_partner(self):
		if self._context.get('active_id'):
			return int(self._context.get('active_id'))
		return False
	banka   = fields.Many2one('info_extensions.acquirer',string='Banka',domain=[('bkm_id','>',0)])
	partner = fields.Many2one('res.partner',string='Üye İş Yeri',default = _get_defalt_partner)
	iban    = fields.Char(string='IBAN No') 

class e_mail_preview(models.TransientModel):
	_inherit = 'email_template.preview'
	
	@api.multi
	def send_mail(self):
		self.ensure_one()
		self.env['res.partner'].sudo().browse([int(self.res_id)]).belge_sablon_gonder()
		return {'type':'ir.actions.act_window_close'}

class res_partner_category(models.Model):
	_inherit = 'res.partner.category'
	
	tip      = fields.Selection(selection=[('esnaf','Üye İş Yeri'),
										   ('kart_musterisi','Kurumsal Müşteri')],string='Kategory Tipi') 

class res_partner_marka( models.Model ):
	_name = 'info_bayi.sube_marka'

	name        = fields.Char()
	partner_id  = fields.Many2one('res.partner')  

class evrak_tipleri(models.Model):
	_name = 'info_bayi.evrak_tipleri'
	
	name            = fields.Char(string='Evrak Tip Adı', required=True)
	
	sahis           = fields.Boolean(string='Şahıs Firması Evrakı')
	sermaye         = fields.Boolean(string='Sermaye Firması Evrakı')
	ortaklik        = fields.Boolean(string='Ortaklık Evrakı')
	
	cari_tipi       = fields.Selection(selection=[('esnaf','Üye İş yeri / Esnaf Evrakı'),
												  ('kurumsal','Kurumsal Müşteri Evrakı'),
												  ('bireysel','Bireysel Müşteri Evrakı'),
												  ('is_ortagi','İş Ortağı Evrakı')], string='Cari Tipi')
	ws_ile_gonder   = fields.Boolean(string='WebServis İle Gönder')
	bankaya_gonder  = fields.Boolean(string='Bankaya Gönder')
	mail_text       = fields.Char(string="E-Posta Şablon da Yazacak İfade ( Boş Bırakırsanız Belge Şablonda Yazmaz.)")
	sube_evraki     = fields.Boolean('Şubelerden Ayrıca İstenecek')
	
	web_name        = fields.Char('Web Adı (Geliştirici İçindir, Dokunmayınız, Değiştirmeyiniz.)')
class partner_evrak_rel(models.Model):
	_name = 'info_bayi.partner_evrak_rel'
	
	partner_id = fields.Many2one('res.partner',domain=[('esnaf','=',True),('customer','=',True),('is_company','=',True)])
	evrak_id   = fields.Many2one('info_bayi.evrak_tipleri',required=True,string='Evrak Adı')
	bankaya_gonder = fields.Boolean(related='evrak_id.bankaya_gonder')
	link_field = fields.Char(string='',    default = u'Dokümanları Yüklemek İçin Tıklayınız')
	link_field_ok = fields.Char(string='', default = u'Dokümanları Görüntülemek ve Düzenlemek İçin Tıklayınız')
	
	docs      = fields.One2many('info_bayi.uye_is_yeri_docs','rel_id')
	state     = fields.Selection(selection=[('yeni','Evrak Bulunamadı'),
											('yuklendi','Dosya(lar) Sisteme Yüklendi'),
											('turkpara_gonderildi','Merkeze Aktarıldı'),
											('turkpara_hata','Merkez Belge Hatası')],default='yeni',string='Durum')
	
	prev_type = fields.Char()
	
	sonradan_gonder = fields.Boolean(string='Sonradan Eklenecek')
	
	@api.multi
	def unlink(self):
		return super(partner_evrak_rel,self.sudo()).unlink()
	@api.multi
	@api.onchange('docs')
	def docs_onchange(self):
		for s in self:
			if len(s.docs) > 0:
				s.state = 'yuklendi'
			else:
				s.state = 'yeni'
			#print s.state
			
	@api.multi
	@api.onchange('evrak_id')
	def onchange_evrak_id( self ):
		dom = [('id','in',[])]
		partner = self.env['res.partner'].browse([self._context.get('active_id')])
		#print partner
		if partner:
			if partner.sahis_sirketi:
				dom = [('sahis','=',True)]
			elif partner.sermaye_sirketi:
				dom = [('sermaye','=',True)]
			elif partner.ortaklik:
				dom = [('ortaklik','=',True)]
		return {'domain':{'evrak_id':dom}}

class uye_isyeri_docs ( models.Model):
	_name = 'info_bayi.uye_is_yeri_docs'
	rel_id = fields.Many2one( 'info_bayi.partner_evrak_rel')
	bankaya_gonder = fields.Boolean(related='rel_id.bankaya_gonder')
	doc              = fields.Binary(string='Dosya')
	doc_name         = fields.Char()
	turkpara_doc_id  = fields.Char('Türkpara Belge No')
	banka            = fields.Many2one('info_bayi.turkpara_aktif_banka',string="E-Mail ile Gönderilecek Banka")
	pos_terminal_id  = fields.Char('Pos Terminal Id')
	pos_is_yeri_id  = fields.Char('Pos Üye İş Yeri No')
	
	@api.model
	def create(self,vals):
		res = super( uye_isyeri_docs, self).create( vals )
		res.rel_id.state = 'yuklendi'
		return res
	
	@api.model
	def to_file_system(self):
		if not os.path.exists('evrakstore'):
			os.mkdir('evrakstore')
		main_path = 'evrakstore'
		for d in self.search([]):
			
			#self.env.cr.execute()
			try:
				decoded = b64decode(d.doc)
			except:
				continue
			
			try:
				os.makedirs('evrakstore/'+d.create_date.split(' ')[0].replace('-','/'))
			except:
				#print 'create folder failed'
				pass
			
			f =  open('evrakstore/' + d.create_date.split(' ')[0].replace('-','/') + '/' + d.doc_name,'wb' )
			
			f.write( decoded )
			
			decoded = None
			time.sleep(0.1)
			f.close()
			
	@api.multi
	def unlink(self):
		self.ensure_one()
		try:
			others = self.rel_id.docs
			if len( others ) == 1:
				self.rel_id.state = 'yeni'
			res    = super(uye_isyeri_docs,self).unlink()
		except:
			res    = True
		return res
class marka(models.Model):
	_name = 'info_bayi.marka'
	
	@api.model
	def default_get(self,fields_list):
		#print self._context
		return super( marka,self).default_get( fields_list)
		
	partner_id = fields.Many2one('res.partner',default=lambda self:self._context.get('active_id'),required=True,string='Cari Hesap',
								 domain=[('is_company','=',True),('parent_id','=',False),('esnaf','=',True),('customer','=',True)])
	name       = fields.Char(string='Marka Adı', required=True)
	
class res_partner(models.Model):
	_inherit = 'res.partner'
	
	def cleanhtml(self,raw_html):
		cleanr = re.compile('<.*?>')
		cleantext = re.sub(cleanr, '', raw_html)
		if cleantext == 'İş Ortağı oluşturuldu':
			return ''
		return cleantext
	
	@api.model
	def _get_parsif_pos_domain(self):
		ids = []
		aktifler = self.env['info_bayi.turkpara_aktif_banka'].sudo().search([])
		ids      = self.env['info_extensions.acquirer'].sudo().search([('id','not in' ,[x.name.id for x in aktifler])]).ids
		
		return [('id','in', ids)]
	
	@api.model
	def get_current_user_def( self):
		if self.env.user.company_id.id == 1:
			return 1
		return 0

	@api.model
	def _get_default_country(self):
		res = self.env.get('res.country').search([('id', '=', '224')])
		return res and res[0] or False
	
	@api.model
	def _get_kom_kdv_selection(self):
		sel = []
		for i in range(int(oran_range * 100)):
			str_i = str(i)
			if len(str_i) < 3:
				str_i = '0'*(3-len(str_i)) + str_i
			sel.append((str_i[:1] + '.' + str_i[1:],str_i[:1] + ',' + str_i[1:]))
		
		return sel  
	
	@api.multi
	def tr_para_evrak_gonder(self):
		self.ensure_one()
		if self.esnaf:
			doc_yolla(self)
	
	@api.multi
	def basvuru_guncelle(self):
		self.ensure_one()
		if self.esnaf and self.state == 'confirmed':
			basvuru_kayit(self)
	
	@api.multi
	def default_evrak_yukle(self):
		self.ensure_one()
		if self.ortaklik:
			domain = [('ortaklik','=',True)]
			state = 'ortaklik'
		if self.sahis_sirketi:
			domain = [('sahis','=',True)]
			state = 'sahis'
		if self.sermaye_sirketi:
			domain = [('sermaye','=',True)]
			state = 'sermaye'
		donen = self.get_dws_by_state( state, domain)
		# print donen
		for d in donen:
			if d[0] == 0:
				self.env['info_bayi.partner_evrak_rel'].sudo().create(d[2])
	
	@api.model
	def get_category_domain_by_type(self):
		domain = []
		if self.env.context.get('default_esnaf'):
			domain = [('tip','=','esnaf')]
		elif self.env.context.get('default_kart_musterisi'):
			domain = [('tip','=','kart_musterisi')]
		
		return domain
	
	@api.model
	def get_marka_selection(self):
		m = [('0',u'Markasız')]
		
		if self.id:
			parent_id = self.parent_id.id
		else:
			parent_id = self._context.get('active_id')
		parent_marka = self.env['info_bayi.marka'].search([('partner_id','=',parent_id)])
		if parent_marka:
			m.extend (list(map(lambda x:(str(x.id),x.name),parent_marka)))
		return m
	
	category_id = fields.Many2many('res.partner.category', column1='partner_id',
									column2='category_id', string='Tags',  domain=get_category_domain_by_type)

	sirket_tipi_selection   = fields.Selection(selection=[('sahis','Şahıs Şirketi'),
														  ('ltd','Limited/Anonim Şirket'),
														  ('adi','Ortaklık')],string='Şirket Tipi')
	dealer                  = fields.Boolean(string='İş Ortağı')
	esnaf                   = fields.Boolean(string='Üye İşyeri/Esnaf')
	dealer_id               = fields.One2many('res.company','partner_id',string='Bayi Firması')
	main_dealer             = fields.Many2one('res.company',string='Üst Bayi',default=lambda x:x.env.user.company_id.id)
	current_user            = fields.Integer(compute='get_current_user',default=get_current_user_def )
	state                   = fields.Selection(selection=[('predraft','Üye İş Yeri Girişi'),
												  ('draft','Başvuru Aşamasında - Eksik Bilgileri Tamamlayınız.'),
												  ('sended','E-Posta Gönderildi, Belgeler Bekleniyor.'),
												  ('waiting_confirm','Onay Bekliyor'),
												  ('turkpara_onay_bekleniyor','Merkez Onayı Bekleniyor'),
												  ('confirmed','Onaylandı'),
												  ('missing_parameters','Eksik Evrak'),
												  ('cancel','İptal Edildi'),
												  ('declined','Reddedildi'),
												  ('document_change','Evraklar Değişime Açık'),
												  ('islak_bekleniyor','Islak İmzalı Evraklar Bekleniyor'),
												  ('turkpara_ws_error','Servis Gönderimi Başarısız'),
												  ('musteri_istemiyor','Müşteri İstemiyor'),
												  ('banka_posu','Banka Posu Sorunu'),
												  ('aktiflerle_calismayacak','Aktif Bankalarla Çalışmak İstemiyor'),
												  ('bankaya_iletildi','İlgili Bankaya İletildi'),
												  ('infoteks_alacak','İnfoteks Yazarkasa İstiyor'),
												  ('dusunecekmis','Düşünmek İstiyor'),
												  ('istemiyor_poslu','İstemiyor (Aktif Bankası Var)'),
												  ('istemiyor_possuz','İstemiyor (Aktif Bankası Yok)'),
												  ('bankayla_anlasamadi','Banka İle Anlaşamadı'),
												  ('slip_alinacak','Slip Görüntüsü Alınacak'),
												  ('giris','Girişi Yapıldı'),
												  ('muhasebe','Muhasebe Arşiv'),
												  ],
	default='predraft')
	parent_name             = fields.Char(string                         ='Ana Bayi Adı')
	
	red_aciklamasi          = fields.Text(string='Açıklama')
	country_id              = fields.Many2one('res.country',default=_get_default_country)
	
	file_fields             = ['sicil_gazetesi','esnaf_sanatkar','ikamet','adli','imza','faaliyet','nufus','vergi_levhasi','sozlesme']
	gsm_no                  = fields.Char( string="TürkPara'ya İletilecek GSM No", size=14 )
	gsm_no_name             = fields.Char( string="Yetkili Kişi", )

	sube_contacts_visible   = fields.Integer(compute="get_contacts_adet")
	contacts_visible        = fields.Integer(compute="get_contacts_adet")
	
	doc_receive_method      = fields.Selection(selection=[('el','Elden Teslim'),
														  ('kargo','Kargo İle'),
														  ('posta','Posta İle'),
														  ('kurye','Özel Kurye İle'),
														  ('eimza','E-İmzalı Belge'),
														  ],string='Evraklar Nasıl Geldi?')
	doc_katalog_no         = fields.Char(string='Katalog No')
	doc_raf                = fields.Char(string='Raf', size=4)
	doc_goz                = fields.Char(string='Göz', size=4)
	doc_klasor             = fields.Char(string='Klasör No', size=4)        
	doc_evrak_no           = fields.Char(string='Evrak no')
	doc_teslim_alan        = fields.Char(string='Evrak Teslim Alan')
	
	turkpara_basvuru_id    = fields.Char(string='TürkPara Başvuru ID')
	turkpara_e_para_hesap  = fields.Char(string='E-Para Hesap No')
	turkpara_ws_error      = fields.Char(string="Türkpara Servis Hatası")
	
	guid                   = fields.Char()

	pos_goster             = fields.Selection(selection=[('var','Yemekmatik Anlaşmalı Banka Posu Var'),('yok','Yemekmatik Anlaşmalı Banka Posu Yok')], string='Anlaşmalı Banka Posu Var')
	kullanilan_poslar      = fields.Many2many('info_bayi.turkpara_aktif_banka',string='Üye İş Yeri Posları')
	kullanilan_poslar2     = fields.Many2many('info_extensions.acquirer',string="Diğer Banka Posları",domain=_get_parsif_pos_domain)
	
	poslar                 = fields.One2many('info_bayi.turkpara_banka_rel','partner_id')
	
	uye_is_yeri_evrak      = fields.One2many('info_bayi.partner_evrak_rel','partner_id',string='Üye İş Yeri Evrakları')

	odemeler               = fields.One2many('info_bayi.odeme_yap','partner_id')
	b2b_id                 = fields.Integer()
	b2b_sync_info          = fields.Text()
	
	sozlesme_barcode       = fields.Binary(string="Sözleşme Barkodu")
	qr_code                = fields.Binary(string="Sözleşme QR Kod")
	sozlesme_no            = fields.Char(string="Sözleşme No")
	
	oda_sicil_no           = fields.Char(string="Oda Sicil No")
	esnaf_sicil_no         = fields.Char(string="Esnaf Sicil No")
	kurulus_yili           = fields.Integer(string="Kuruluş Yılı")
	
	kom_kdv_durumu      = fields.Selection(selection=[('dahil','KDV Dahil'),
											 ('haric','KDV Hariç')], string='KDV Durumu', default='haric')
	
	kom_kdv_orani       = fields.Selection(selection=_get_kom_kdv_selection, string='Komisyon Oranı',default='6.00')
	kom_kdv_orani_txt   = fields.Char(compute='get_com_kdv_orani_txt')
	
	ibanlar             = fields.One2many('info_bayi.uye_is_yeri_iban','partner',string='Üye İş Yeri IBAN No\'lar')
	
	gorusulen_kisi      = fields.Char(string='Görüşülen Kişi')
	gorusulen_kisi_tel  = fields.Char(string='Görüşülen Kişi Gsm')
	
	randevu_bilgisi     = fields.Char(compute='_get_randevu_bilgisi')
	firma_tipi_str      = fields.Char(compute='_get_firma_tipi_str')
	mesage_ids_text     = fields.Char(compute='_get_message_text')
	
	mail_kargo_gonderildi_mi  = fields.Integer()#1:mail, 2:kargo send 3:kargo received
	
	markalar            = fields.One2many('info_bayi.marka','partner_id',string='Markaları')
	
	sube_marka          = fields.Many2one('info_bayi.marka',string='Marka')
	sube_kodu           = fields.Char("Şube Kodu")
	
	kullanici_var       = fields.Boolean(compute='_kullanici_var')
	
	
	@api.multi
	def _kullanici_var(self):
		for s in self:
			s.kullanici_var = False
		
			if len(self.env['res.users'].sudo().search([('partner_id','=',s.id)])) > 0 :
				s.kullanici_var = True
	
	@api.multi
	def create_sube_rapor(self):
		self.ensure_one()
		bundles 	= []
		is_yeri_nos = []
		for pos in self.poslar:	
			if pos.uye_isyeri_no not in [ x[0] for x in bundles]:
				pos_bundle = [pos.uye_isyeri_no,False]
				bundles.append( pos_bundle )
		
		
		#print bundles
		action={
			'name':'Pos Hareketleri',
			'type':'ir.actions.act_window',
			'res_model':'info_bayi.get_rapor_wizard',
			'src_model':'res.partner',
			'view_type':'form',
			'view_mode':'form',
			'target':'new',
			'context':{
					   'active_id':self.id,
					   'get_by_all':False,
					   'bundle':bundles}
		}
		return action
	
	@api.multi
	def create_sube_pos( self ):
		self.ensure_one()
		is_e    = self.env['info_bayi.turkpara_is_emirleri']
		is_emri = is_e.sudo().search([('partner_id','=',self.id)])
		if not is_emri:
			is_emri_d = dict(partner_id=self.id)
			is_emri = is_e.create( is_emri_d )
		
		s_id 			=  self.env['info_bayi.turkpara_banka_rel']._get_next_islem_id()	

		r_dict = {
			'type': 'ir.actions.act_window', 
			'name': 'Yeni Pos Talebi',
			'view_mode': 'form',
			'view_type':'form',
			'res_model': 'info_bayi.turkpara_banka_rel',
			'src_model':'res.partner',
			'context': {'default_isemri':is_emri.id,'default_islem_id':s_id}, 
			'target':"new"
		}
		
		return r_dict	
	
	@api.multi
	def delete_sube_pos( self ):
		self.ensure_one()
		is_e    = self.env['info_bayi.turkpara_is_emirleri']
		is_emri = is_e.sudo().search([('partner_id','=',self.id)])
		if not is_emri:
			is_emri_d = dict(partner_id=self.id)
			is_emri = is_e.create( is_emri_d )
		
		view    = self.env.ref('info_bayi.info_bayi_sube_pos_talep_sube')
		view_id = view and view.id or False
		
		views = [
			(view_id, 'tree'),
			(False, 'form'),
		]

		r_dict = {
			'type': 'ir.actions.act_window', 
			'name': 'Yeni Pos Talebi',
			'view_mode': 'tree,form',
			'view_type':'form',
			'res_model': 'info_bayi.turkpara_banka_rel',
			'views':views,
			'src_model':'res.partner',
			'context': {'default_isemri':is_emri.id,'odoo_bug':True}, 
			'target':"new",
			'domain':[('isemri','=',is_emri.id)]
		}
		
		return r_dict	
	@api.multi
	def create_sube_user(self):
		if self.sube_kodu:
			usr      = self.env['res.users'].sudo()
			vals_user = dict(
					name  =  self.display_name,
					login =  self.sube_kodu,
					partner_id = self.id,
					active = True,
					password = '123456',
					company_id = self.company_id.id,
					company_ids = [(4,self.company_id.id)]
				)
		
			user_obj           = usr.create( vals_user )
			user_obj.groups_id = [(5,)] #tum yetkileri elinden al
			
			domain = [('sube_default','=',True)]
			
			default_groups    =  self.env['res.groups'].sudo().search( domain )
			
			g_ids = list(map(lambda x:(4,x.id),default_groups))
			user_obj.groups_id = g_ids
			
			r = "/web/payresult?inner_message=1&success=%s&msj=%s"%(1,"Kullanıcı Oluşturuldu")
			return {
						'type': 'ir.actions.act_url',
						'url': r,
						'target': 'popup'
					} 
		else:
			raise exceptions.ValidationError("Önce Şube Kodu Yazınız.")

	@api.multi
	def pos_report(self):
		action={
			'name':'Pos Hareketleri',
			'type':'ir.actions.act_window',
			'res_model':'info_bayi.get_rapor_wizard',
			'src_model':'res.partner',
			'view_type':'form',
			'view_mode':'form',
			'target':'new',
			'context':{'get_by_all':True}
		}
		return action
	
	@api.multi
	def _get_message_text(self):
		for s in self:
			t = ""
			for m in s.message_ids:
				t += self.cleanhtml(m.body) +"\n"
			s.mesage_ids_text = t
		
	@api.multi
	def _get_firma_tipi_str(self):
		for s in self:
			if s.sahis_sirketi:
				s.firma_tipi_str = 'Şahıs Şirketi'
			if s.sermaye_sirketi:
				s.firma_tipi_str = 'Sermaye Şirketi'
			if s.ortaklik:
				s.firma_tipi_str = 'Ortaklık'
	@api.multi
	def _get_randevu_bilgisi(self):
		for s in self:
			rs = self.env['calendar.event'].sudo().search([('partner_ids','child_of',s.id)])
			rb = ""
			for r in rs:
				#print r.start_date or r.start_datetime
				rb += " Tarih:%s"%(r.start_date or r.start_datetime) + "\n"
				s.randevu_bilgisi = rb
	
	@api.onchange('gorusulen_kisi_tel')
	def onchange_mobile_gor(self):
		
		res = {}
		if self.gorusulen_kisi_tel and len( self.gorusulen_kisi_tel) > 0:
			phone = [str(s) for s in self.gorusulen_kisi_tel.split() if s.isdigit()]
			if len( phone )> 0:
				phone = "".join( phone )
				if phone[0] == "0":
					phone = phone[1:]
				if len( phone) != 10:
					self.gorusulen_kisi_tel = False
					res = {'warning': {
					'title': 'Görüşülen Kişi Gsm',
					'message': u'Lütfen Geçerli Bir Görüşülen Kişi Gsm Giriniz. '}
					}
				else:
					phone = phone[0:3] + '-' + phone[3:6] + ' ' + phone[6:8] + ' ' + phone[8:10]
					self.gorusulen_kisi_tel = phone
					if phone[0] != '5':
						self.gorusulen_kisi_tel = False
						res = {'warning': {
							'title': 'Görüşülen Kişi Gsm',
							'message': u'GGörüşülen Kişi Gsm 5 ile Başlamalıdır. '}
						}
			else:
				self.gorusulen_kisi_tel = False
				res = {'warning': {
				  'title': 'Görüşülen Kişi Gsm',
				  'message': u'Lütfen Geçerli Bir Görüşülen Kişi Gsm Giriniz. '}
				}
		return res
	
	
	
	@api.onchange('city_combo')
	def on_change_city_combo(self):
		for s in self:
			if s.city_combo and s.ilce:
				s.ilce= False
	
	@api.multi
	def belge_sablon_prepare(self):
		self.ensure_one()

		report = self.env['ir.actions.report']._get_report_from_name('info_bayi.report_sozlesme')
		sozlesme, _ = report.render_qweb_pdf(self.id)
		sozlesme_attachment = self.env['ir.attachment'].sudo().create({
			'name': 'Üye İş yeri Sözleşmesi.pdf',
			'type': 'binary',
			'datas': base64.b64encode(sozlesme),
			'datas_fname': 'Üye İş Yeri Sözleşmesi.pdf',
			'res_model': 'res.partner',
			'res_id': self.id,
			'mimetype': 'application/pdf'
		})
		
		imgname = pos_img_name
		imgext = '.pdf'
		placeholder = functools.partial(get_resource_path, 'info_bayi', 'static','sozlesme')
		
		pos_talimati = placeholder( imgname + imgext )
		with open( pos_talimati, 'rb') as p:
			talimat_attachment = self.env['ir.attachment'].sudo().create({
				'name': 'Pos Talimatı.pdf',
				'type': 'binary',
				'datas': base64.encodestring(p.read()),
				'datas_fname': 'Pos Talimatı.pdf',
				'res_model': 'res.partner',
				'res_id': self.id,
				'mimetype': 'application/pdf'
			})
		
		imgname = kvkk_name
		imgext = '.pdf'
		placeholder = functools.partial(get_resource_path, 'info_bayi', 'static','sozlesme')
		
		kvkk = placeholder( imgname + imgext )
		with open( kvkk, 'rb') as k:
			kvkk_attachment = self.env['ir.attachment'].sudo().create({
				'name': 'KVKK Aydınlatma Metni.pdf',
				'type': 'binary',
				'datas': base64.encodestring(k.read()),
				'datas_fname': 'KVKK Aydınlatma Metni.pdf',
				'res_model': 'res.partner',
				'res_id': self.id,
				'mimetype': 'application/pdf'
			})
		
		email_template = self.env.ref('info_bayi.uye_isyeri_email_template')
		
		email_template.attachment_ids = [(5,)]
		email_template.attachment_ids = [(4, sozlesme_attachment.id)]
		email_template.attachment_ids = [(4, talimat_attachment.id)]
		email_template.attachment_ids = [(4, kvkk_attachment.id)]
		
	
	@api.multi
	def mail_preview(self):
		self.ensure_one()
		self.belge_sablon_prepare()
		template_id = self.env['mail.template'].search([('name','=','Uye İs Yeri e-mail template')]).id
		action={
			'name':'E-Posta Önizleme',
			'type':'ir.actions.act_window',
			'res_model':'email_template.preview',
			'src_model':'mail.template',
			'view_type':'form',
			'view_mode':'form',
			'view_id'  : self.env.ref("mail.email_template_preview_form").id,
			'target':'new',
			'context':{'template_id':template_id,'default_res_id':self.id,'hide_selection':True}
		}
		return action
	
	@api.multi
	def set_appointment(self):
		action={
			'name':'Randevu Oluştur',
			'type':'ir.actions.act_window',
			'res_model':'calendar.event',
			'src_model':'res.partner',
			'view_type':'form',
			'view_mode':'form',
			'target':'new',
			'context':{'default_name':'Üye İş Yeri Görüşmesi','default_partner_id':[(4,self.id)]}
		}
		return action

	@api.multi
	def banka_pos_preview(self):
		self.ensure_one()
		return {
			 'type' : 'ir.actions.act_url',
			 'url': '/web/pos_mail_preview/%s'%(self.id),
			 'target': 'popup',
		}
		
	@api.multi
	def belge_sablon_gonder(self):
		self.ensure_one()
		email_template = self.env.ref('info_bayi.uye_isyeri_email_template')
		mail_id   = email_template.send_mail(self.id, raise_exception="Mail gönderilemedi Parametreleri ve Mail adresini Kontrol Ediniz.", force_send=True)
		mail_obj  = self.env['mail.mail'].sudo().browse( mail_id )
		for at in email_template.attachment_ids:
			#print at, dir(at)
			at.unlink()
		if self.state in ('draft','waiting_confirm','missing_parameters','sended'):
			self.state              = 'sended'
		self.mail_kargo_gonderildi_mi = 1
		'''
		if mail_obj and mail_obj.state == 'exception':
			raise exception.UserError('Mail Gönderilemedi, Lütfen Mail Adresini Kontrol Ediniz. ')
		else:
		''' 
	@api.multi
	def bankaya_pos_gonder(self,context,return_only=False):
		self.ensure_one()
		
		mail_text = ''
		docs = list(filter(None, list(map(
			lambda x: list(filter(lambda y: y if y.banka and y.pos_is_yeri_id and y.pos_terminal_id else None, x.docs)),
			self.uye_is_yeri_evrak))))
		# docs = list(filter(None,list(map(lambda xlist(filter(lambda y:y if y.banka and y.pos_is_yeri_id and y.pos_terminal_id else None, x.docs))), self.uye_is_yeri_evrak)))
		load = {}
		for d in docs:
			c = 1;
			for i in d:
				docname = i.doc_name.split('.')[-1]
				attachment = self.env['ir.attachment'].sudo().create({
					'name': 'Pos Slip',
					'type': 'binary',
					'datas': i.doc,
					'datas_fname': 'Pos Slip Örneği-%s.%s'%(c,docname),
					'res_model': 'res.partner',
					'res_id': self.id,
					'mimetype': 'image/*'
				})
				c+=1
				if not load.get( i.banka ):
					load[ i.banka ] = {(i.pos_terminal_id,i.pos_is_yeri_id):[(4,attachment.id)]}
				else:
					if not load[i.banka].get( (i.pos_terminal_id,i.pos_is_yeri_id) ):
						load[i.banka].update({(i.pos_terminal_id,i.pos_is_yeri_id):[(4,attachment.id)]})
					else:
						load[i.banka][(i.pos_terminal_id,i.pos_is_yeri_id)].append( [(4,attachment.id)] )
						
		#print docs, 'Mail Posu'            
		email_template = self.env.ref('info_bayi.banka_pos_email_template')
		Mail = self.env['mail.mail']
		for k,v in load.items():
			email_template.write({'email_to': k.email})
			email_template.attachment_ids = [(5,)]
			counter  = 0
			pos_text = """<table border='1px solid black;2 style='padding:5px;'>
							<tr>
								<td style='padding:5px;;background:#E0E0E6;'>Pos No</td>
								<td style='padding:5px;;background:#E0E0E6;'>Üye İş Yeri No</td>
								<td style='padding:5px;;background:#E0E0E6;'>Pos Terminal Id</td>
							</tr>
						"""
			for k1,v1 in v.items():
				counter += 1
				pos_text += """<tr>
									<td style='padding:5px;'>%s</td>
									<td style='padding:5px;'>%s</td>
									<td style='padding:5px;'>%s</td>
								</tr>"""%( counter, k1[1],k1[0])
				email_template.attachment_ids = v1
			
			pos_text +="</table>"
			
			mail_id  = email_template.send_mail(self.id, raise_exception=False, force_send=False)
			mail_obj = Mail.browse([ mail_id ])
			
			mail_obj.body_html = mail_obj.body_html.replace("{{pos_text}}", pos_text).replace("{{sayi}}",str(counter))
			mail_text +=  mail_obj.body_html + "<br /><br /><br />----------------------------------------------<br /><br /><br />"
			#print 'return_only : ', return_only
			if not return_only:
				mail_obj.send( raise_exception=False )
				#print  'AAAAAAAAAA',self.mail_kargo_gonderildi_mi
				self.mail_kargo_gonderildi_mi = 1
				#print  'BBBBBBBBBB',self.mail_kargo_gonderildi_mi
			else:
				mail_obj.unlink()
			#self.env['ir.attachment'].sudo().browse( map(lambda x:x[1],v)).unlink()
		if return_only:
			return mail_text
	@api.one
	@api.depends('kom_kdv_orani')
	def get_com_kdv_orani_txt(self):
		#-------------------------------------------------------------
		#ENGLISH
		#-------------------------------------------------------------

		to_10 = ( u'SIFIR',  u'BİR',   u'İKİ',  u'ÜÇ', u'DÖRT',   u'BEŞ',   u'ALTI',
				  u'YEDİ', u'SEKİZ', u'DOKUZ' )
		tens  = ( u'ON',u'YİRMİ', u'OTUZ', u'KIRK', u'ELLİ', u'ALTMIŞ', u'YETMİŞ', u'SEKSEN', u'DOKSAN')

		denom = ( '',
				  u'BİN',     u'MİLYON',         u'MİLYAR',       u'TRİLYON',       u'TRİLYAR',
				  u'KATRİLYON',u'KATRİLYAR')


		def _convert_nn(val):
			"""convert a value < 100 to turkish.
			"""
			if val < 10:
				return to_10[val]
			for (dcap, dval) in ((k, 10 + (10 * v)) for (v, k) in enumerate(tens)):
				if dval + 10 > val:
					if val % 10:
						return dcap + '' + to_10[val % 10]
					return dcap

		def _convert_nnn(val):
			"""
				convert a value < 1000 to turkish, special cased because it is the level that kicks
				off the < 100 special case.  The rest are more general.  This also allows you to
				get strings in the form of 'forty-five hundred' if called directly.
			"""
			word = ''
			(mod, rem) = (val % 100, val // 100)

			if rem > 0:
				if rem == 1:
					word= u'YÜZ'
				else:
					word = to_10[rem] + u'YÜZ'
				if mod > 0:
					word += ''
			if mod > 0:
				word += _convert_nn(mod)
			return word

		def turkish_number(val):

			if val < 10:
				return to_10[val]
			if val < 100:
				return _convert_nn(val)
			if val < 1000:
				 return _convert_nnn(val)

			for (didx, dval) in ((v - 1, 1000 ** v) for v in range(len(denom))):
				if dval > val:
					mod = 1000 ** didx
					l = val // mod
					r = val - (l * mod)
					if l == 1:
						ret = denom[didx]
					else:
						ret = _convert_nnn(l) +'' + denom[didx]

					if r > 0:
						ret = ret + '' + turkish_number(r)
					return ret

		def amount_to_text_tr(number, currency=False):
			number = '%.2f' % number
			
			units_name = currency or ''
			list = str(number).split('.')
			start_word = turkish_number(int(list[0]))
			end_word = ''
			if int(list[1]):
				end_word = turkish_number(int(list[1]))
			
			cents_number = int(list[1])
			#print currency
			cents_name = ''
			if currency:
				if currency == 'TL':
					cents_name =  u'Kr.'
				else:
					cents_name = u'Cent'
			f_list = [start_word, units_name, end_word, cents_name]
			return ' '.join(x for x in f_list if len(x) > 0)


		self.kom_kdv_orani_txt = amount_to_text_tr(float(self.kom_kdv_orani))
			
	def get_nex_sozlesme_no(self,obj):
		s_no = self.env['ir.sequence'].sudo().next_by_code('res.partner.sozlesme')
		s_type = '1'
		if obj.kart_musterisi:
			s_type = '2'
		elif obj.bireysel_musteri:
			s_type = '3'
		plaka = obj.city_combo.plaka
		if not plaka:
			plaka = '00'
		if len(str(s_no)) < 10:
			s_no = '0'*(10-len(str(s_no))) + str(s_no)
		id_  = obj.id
		if len(str(id_)) < 7:
			id_ = '0'*(7-len(str(id_))) + str(id_)
		no = s_type + str(plaka) + str(s_no) #+ str(id_)
		return no

	def create_sozlesme_qr(self, obj):
		p_name = obj.name
		c_name = 'S. No : ' + obj.sozlesme_no
		c_id = str(obj.id)
		c_vkn_tc = ''
		if obj.taxNumber:
			c_vkn_tc += ' Vkn : ' + obj.taxNumber
		if obj.tckn:
			c_vkn_tc += ' Tckn : ' + obj.tckn

		qr_text = p_name + u'\n ' + c_name + c_id + u'\n ' + c_vkn_tc
		img = qrcode.make(qr_text)
		buff = BytesIO()
		img.save(buff)
		img_str = b64encode(buff.getvalue())

		obj.qr_code = img_str

	def create_sozlesme_barcode(self, obj):

		if not obj.sozlesme_no:
			obj.sozlesme_no = self.get_nex_sozlesme_no(obj)

		if not obj.sozlesme_barcode:
			C = barcode.get_barcode_class('ean13')
			fp = BytesIO()
			c = C(obj.sozlesme_no, writer=ImageWriter())
			c.write(fp=fp,options={'text': u'Sözleşme Seri No :', 'module_height': 8, 'text_distance': 1, 'quiet_zone': 1.5,'font_size': 9})
			b64 = b64encode(fp.getvalue())
			obj.sozlesme_barcode = b64
		if not obj.qr_code:
			self.create_sozlesme_qr(obj)
			
	@api.multi
	def reset_to_draft(self):
		self.ensure_one()
		if self.sahis_sirketi:
			dom = [('sahis','=',True)]
		if self.sermaye_sirketi:
			dom = [('sermaye','=',True)]
		if self.ortaklik:
			dom = [('ortaklik','=',True)]
			
		evrak_tipleri = len(self.env['info_bayi.evrak_tipleri'].search( dom ))
		
		evrak_list = 0
		for y in self.uye_is_yeri_evrak:
			#print y.evrak_id.name, len(y.docs)
			if len(y.docs) > 0:
				evrak_list += 1
		#rint evrak_tipleri, evrak_list
		if evrak_list == evrak_tipleri:
			state = 'waiting_confirm'
		else:
			state = 'missing_parameters'
		
		self.state = state
	
	@api.onchange('gsm_no')
	def onchange_mobile(self):
		
		res = {}
		if self.gsm_no and len( self.gsm_no) > 0:
			phone = [str(s) for s in self.gsm_no.split() if s.isdigit()]
			if len( phone )> 0:
				phone = "".join( phone )
				if phone[0] == "0":
					phone = phone[1:]
				if len( phone) != 10:
					self.gsm_no = False
					res = {'warning': {
					'title': 'GSM No',
					'message': u'Lütfen Geçerli Bir GSM No Giriniz. '}
					}
				else:
					phone = phone[0:3] + '-' + phone[3:6] + ' ' + phone[6:8] + ' ' + phone[8:10]
					self.gsm_no = phone
					if phone[0] != '5':
						self.gsm_no = False
						res = {'warning': {
							'title': 'GSM No',
							'message': u'GSM No 5 ile Başlamalıdır. '}
						}
			else:
				self.gsm_no = False
				res = {'warning': {
				  'title': 'GSM No',
				  'message': u'Lütfen Geçerli Bir GSM No Giriniz. '}
				}
		return res
	
	def get_dws_by_state(self, state, domain):
		#print self._context
		if self._context.get('sube_ekle'):
			domain.append (('sube_evraki','=',True))
		if self.kart_musterisi:
			domain.append (('cari_tipi','=','kurumsal'))
		if self.esnaf:
			domain.append (('cari_tipi','=','esnaf'))
		if self.dealer:
			domain.append(('cari_tipi','=','is_ortagi'))
		
		counter     = []
		first_node  = 0 
		for d in domain:
			if d[0] == 'cari_tipi':
				if not first_node:
					first_node = domain.index( d )
				counter.append('|')
		
		if len(counter) > 1:
			and_index = first_node - 1
			if and_index >=0:
				domain.insert(and_index,'&')
			counter.pop(0)
			for op in counter:
				domain.insert(first_node,op)
				first_node += 1

		default_evraklar = self.env['info_bayi.evrak_tipleri'].search(domain)
		
		dws = []
		for evrak in default_evraklar:
			onceden_eklenmis_evrak_list = list(filter(lambda x:x if x.evrak_id.id == evrak.id else None,self.uye_is_yeri_evrak ))
			
			if len(onceden_eklenmis_evrak_list ) > 0:
				dw = (4,onceden_eklenmis_evrak_list[0].id)
			else:
				d = {
					'evrak_id': evrak.id,
					'partner_id': self.id,
					'link_field':u"Dokümanları Yüklemek ve Görüntülemek İçin Tıklayınız",
					'link_field_ok':u"Dokümanları Görüntülemek ve Düzenlemek İçin Tıklayınız",
					'docs':False,
					'state':'yeni',
					'prev_type': state
				}
				dw = (0,0,d)
			dws.append( dw )
		return dws

	@api.onchange('sahis_sirketi')
	def _onchange_sahis_sirketi(self):
	
		res = {}
		
		if self.sahis_sirketi == True:
			self.is_company = True          
			self.taxNumber = False
			domain = [('sahis','=',True)]
			dws = self.get_dws_by_state('sahis', domain)
			res['uye_is_yeri_evrak'] = dws

			#print dws
			
			self.sermaye_sirketi = False
			self.ortaklik = False
		return {'value':res}
			
	@api.onchange('sermaye_sirketi')
	def _onchange_sermaye_sirketi(self):
		res = {}
		if self.sermaye_sirketi == True:
			self.is_company = True
			self.tckn = False
			#self.uye_is_yeri_evrak = [(5,)]
			
			domain = [('sermaye','=',True)]
			dws = self.get_dws_by_state('sermaye', domain)
			res['uye_is_yeri_evrak'] = dws
			self.sahis_sirketi = False
			self.ortaklik = False
		return {'value':res}
	
	@api.onchange('ortaklik')
	def _onchange_ortaklik(self):
		res = {}
		if self.ortaklik == True:
			self.is_company = True
			self.tckn = False
			#self.uye_is_yeri_evrak = [(5,)]
			
			domain = [('ortaklik','=',True)]
			dws = self.get_dws_by_state('ortaklik', domain)
			res['uye_is_yeri_evrak'] = dws
			self.sahis_sirketi = False
			self.sermaye_sirketi = False
			
		return {'value':res}

	@api.model
	def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
		
		res = super(res_partner, self).fields_view_get(view_id=view_id, view_type=view_type,toolbar=toolbar, submenu=submenu)
		
		doc = etree.XML(res['arch'])
		if view_type in ('form','kanban','tree') and self._context.get('disable_create'):
			doc.set("create", 'false')
			res['arch'] = etree.tostring(doc)
		
		return res

	@api.multi
	def get_contacts_adet( self ):
		for s in self:
			s.sube_contacts_visible = len(s.sube_contacts)
			s.contacts_visible      = len(s.contacts)
	
	@api.onchange('phone')
	def onchange_phone(self):
		res = {}
		if self.phone and len( self.phone) > 0:
			phone = [str(s) for s in self.phone.split() if s.isdigit()]
			if len( phone )> 0:
				phone = "".join( phone )
				if phone[0] == "0":
					phone = phone[1:]
				if len( phone) != 10:
					self.phone = False
					res = {'warning': {
							'title': 'Telefon No Hatalı',
							'message': u'Lütfen Geçerli Bir Telefon No Giriniz. '}
					}
				else:
					phone = phone[0:3] + '-' + phone[3:6] + ' ' + phone[6:8] + ' ' + phone[8:10]
					self.phone = phone
			else:
				self.phone = False
				res = {'warning': {
						'title': 'Telefon No Hatalı',
						'message': u'Lütfen Geçerli Bir Telefon No Giriniz. '}
				}
		return res  
				
	@api.multi
	def _get_cihaz_adet(self):
		for s in self:
			if s.dealer or s.id == 1:
				comp               = self.env['res.company'].sudo().search([('partner_id','=',s.id)])
				s.cihaz_adetleri_f = len(self.env['stock.production.lot'].search([('company_id','=',comp.id),
																			   ('lot_state','in',('draft','purchased','returned_from_customer'))]))
			else:
				s.cihaz_adetleri_f = 0
	
	@api.multi
	def _get_sold_adet(self):
		for s in self:
			if s.dealer or s.id == 1:
			
				picking_items      = self.env['stock.pack.operation.lot'].search([('operation_id.picking_id.picking_type_id.code',  '=','outgoing'),
																			  ('operation_id.picking_id.company_id',            '=',s.company_id.id),
																			  ('operation_id.state',                            '=','done'),
																			  ('lot_id.lot_state','=','sold')])
				s.satis_cihaz_adet = len( picking_items )
			else:
				s.satis_cihaz_adet = 0
	
	@api.multi
	def _get_iade_adet(self):
		for s in self:
			picking_items      = self.env['stock.pack.operation.lot'].sudo().search([('operation_id.picking_id.picking_type_id.code',   '=','incoming'),
																		  ('operation_id.picking_id.partner_id',            '=',s.id),
																		  ('operation_id.state',                            '=','done'),
																		  ('lot_id.lot_state','=','returned_from_customer')])
			
			s.iade_cihaz_adet = len( picking_items )

	@api.multi
	def cihaz_adetleri(self):
		return {
			'name':'Cihaz Stok Adetleri',
			'type':'ir.actions.act_window',
			'src_model':'res.partner',
			'res_model':'info_bayi.stock_cihaz_adet',
			'view_type':'form',
			'view_mode':'form',
			'target':'new'
		}
	
	#display           = fields.Boolean(store=True,compute ='_get_display_for_dealer')
	#amelelik          = fields.Boolean(compute = '_get_amelelik')
	_order = 'create_date desc'
	
	@api.multi
	def get_terminal_action( self ):
		
		action={
			'name':'TERMİNAL MÜŞTERİLERİ',
			'type':'ir.actions.act_window',
			'res_model':'res.partner',
			'view_type':'form',
			'view_mode':'kanban,tree,form',
			'views' : [[self.env.ref('info_extensions.new_partner_kanban_view').id,'kanban'],
						[self.env.ref('base.view_partner_tree').id,'tree'],
						[self.env.ref('info_bayi.terminal_res_partner_form_view').id,'form']
					],
			'filter':True,
			#'context':"{'search_default_info_bayi_company_filter':1}",
			
		}
		partners = self.search([('state','=','confirmed'),
								  ('customer','=',True),
								  ('is_company','=',True)
								  ])
		p_ids = []
		for p in partners:
			if len(p.terminaller) != 0 and p.id != 1:
					p_ids.append( p.id )
		
		p_ids.append( self.env.user.company_id.partner_id.id)
		domain = [('id','in',p_ids)]
		
		##print domain
		
		action['domain'] = domain
		
		return action
	
	@api.multi
	def get_ynokcs_action( self ):
		
		action={
			'name':'YNÖKC MÜŞTERİLERİ',
			'type':'ir.actions.act_window',
			'res_model':'res.partner',
			'view_type':'form',
			'view_mode':'kanban,tree,form',
			'views' : [[self.env.ref('info_extensions.new_partner_kanban_view').id,'kanban'],
						[self.env.ref('base.view_partner_tree').id,'tree'],
						[self.env.ref('info_bayi.tsm_ynokc_res_partner_form_view').id,'form']
					],
			'filter':True,
			#'context':"{'search_default_info_bayi_company_filter':1}",
			
		}
		partners = self.search([('state','=','confirmed'),
								  ('customer','=',True),
								  ('is_company','=',True)
								  ])
		p_ids = []
		for p in partners:
			if p.taxNumber == '1111111111':
				p_ids.append( p.id )
			else:
				if len(p.ynokcs) != 0 and p.id != 1:
					p_ids.append( p.id )
		if self.env.user.company_id.id == 1:
			p_ids.append( 1 )
		p_ids.append( self.env.user.company_id.partner_id.id)
		domain = [('id','in',p_ids)]
		
		##print domain
		
		action['domain'] = domain
		
		return action
	
	@api.multi
	def on_evrak_degistir( self ):
		self.ensure_one()
		self.state = 'document_change'
	
	@api.multi
	def on_lock( self ):
		self.ensure_one()
		self.state = 'confirmed'
	
	@api.multi
	def on_turkpara_ws_error(self):
		basvuru_kayit( self )
		#doc_yolla( self )
		
	@api.onchange('city_combo')
	def onchange_city_get_bolge_kodu(self):
		##print '******************'
		##print self.city_combo
		if self.city_combo:
			##print '######'
			count_city_dealer = self.env['res.partner'].sudo().search([('city_combo','=',self.city_combo.id)])
			next_city_code    = len( count_city_dealer ) + 1

			self.bolge_sira_no = next_city_code
	
	@api.multi
	def name_get(self):
		res = []
		for record in self:
			_name = record.name
			
			if record.parent_id:
				city  = record.city_combo.name or u''
				ilce  = record.ilce.name or u''
				tanim = record.sube_tanim or u''
				marka = record.sube_marka.name or ''
				_name = record.name + u' ' + marka + u' ' + city + u'-' + ilce + u' ' + tanim 
			
			res.append((record['id'],_name ))
		return res
	
	@api.model
	def default_get( self, fields_list ):
		res = super(res_partner, self).default_get(fields_list)

		if not res.get('property_account_payable_id'):
			payeble_account =  self.env['account.account'].search([('internal_type', '=', 'payable'),
																   ('deprecated', '=', False),
																   ('company_id','=',self.env.user.company_id.id)])
			if len(payeble_account) > 0:
				res['property_account_payable_id'] = payeble_account[0].id

		if not res.get('property_account_receivable_id'):
			payeble_account =  self.env['account.account'].search([('internal_type', '=', 'receivable'), ('deprecated', '=', False),
																   ('company_id','=',self.env.user.company_id.id)])
			if len(payeble_account) > 0:
				res['property_account_receivable_id'] = payeble_account[0].id
		
		
		if res.get("bireysel_musteri"):
		
			default_evraklar = self.env['info_bayi.evrak_tipleri'].search([('cari_tipi','=','bireysel')])
			relation_evrak   = self.env['info_bayi.partner_evrak_rel']
			dws = []
			button_no = 1
			for evrak in default_evraklar:
				dw = {
				'evrak_id': evrak.id,
				'partner_id': self.id,
				'link_field':u"Dokümanları Yüklemek ve Görüntülemek İçin Tıklayınız",
				'link_field_ok':u"Dokümanları Görüntülemek ve Düzenlemek İçin Tıklayınız",
				'docs':False,
				'state':'yeni'
				}
				dws.append([0,0,dw])
			
			res['uye_is_yeri_evrak'] = dws
		
		return res

	def get_bayi_kodu(self,plaka,bolge_sira_no):
		bolge_sira_no = str( bolge_sira_no )
		if len( bolge_sira_no ) == 1:
			bolge_sira_no = '0'+ bolge_sira_no
		return str( plaka) + str( bolge_sira_no )

	@api.one
	def get_current_user( self ):
		if self.env.user.company_id.id == 1:
			self.current_user = 1

	@api.multi
	def on_decline(self):

		self.ensure_one()
		action={
			'name':'Başvuruyu Reddet',
			'type':'ir.actions.act_window',
			'src_model':'res.partner',
			'res_model':'info_bayi.partner_decline_wizard',
			'view_type':'form',
			'view_mode':'form',
			'target':'new'
		}
		return action
	
	@api.multi
	def on_pos_error(self):

		self.ensure_one()
		action={
			'name':'Banka Pos Sorunu',
			'type':'ir.actions.act_window',
			'src_model':'res.partner',
			'res_model':'info_bayi.partner_decline_wizard',
			'view_type':'form',
			'view_mode':'form',
			'target':'new',
			'context':{'pos_sorunu':True}
		}
		return action
	
	@api.multi
	def on_customer_error(self):

		self.ensure_one()
		action={
			'name':'Müşteri Sorunu',
			'type':'ir.actions.act_window',
			'src_model':'res.partner',
			'res_model':'info_bayi.partner_decline_wizard',
			'view_type':'form',
			'view_mode':'form',
			'target':'new',
			'context':{'musteri_sorunu':True}
		}
		return action
	
	@api.multi
	def go_to_draft(self):

		self.ensure_one()
		self.state = 'draft'
		self.write({'ref':False})
	@api.one
	def on_cancel(self):
		##print 'cancelled'
		self.state= 'cancel'

	@api.one
	def on_eksik_evrak(self):
		self.state= 'missing_parameters'

	@api.one
	def on_confirm(self):
		if self.dealer or self.esnaf or self.kart_musterisi or self.bireysel_musteri:
			if not self.env['res.company'].sudo().search([('partner_id','=',self.id)]):
				curr  = self.env['res.currency']
				curr_ = curr.search([('name','=','TRY')])
				company_dict     = dict(
					name         = self.name,
					parent_id    = self.main_dealer.id,
					partner_id   = self.id,
					currency_id  = curr_.id,
				)
				if self.dealer:
					company_dict['bayi'] = True
					company_dict['infopos_bayi'] = True
				bayi_comp = self.env['res.company'].create( company_dict )
		
				self.sudo().company_id = bayi_comp.id
		
				user_        = self.env['res.users'].sudo()
				user_partner = self.env['res.partner'].sudo()
				if not self.bireysel_musteri:
					user_partner_dict = dict(
						name = self.name + u' Kullanıcı',
						email = self.email,
						is_company = False,
						company_id = bayi_comp.id,
						employee = True,
						active = True,
						customer = False
					)
					user_partner_obj = user_partner.create( user_partner_dict )
				else:
					user_partner_obj = self
				vals_user = dict(
					name  =  self.name,
					login =  self.email,
					partner_id = user_partner_obj.id,
					active = True,
					password = '123456',
					company_id = bayi_comp.id,
					company_ids = [(4,bayi_comp.id)]
				)
		
				user_obj           = user_.create( vals_user )
				user_obj.groups_id = [(5,)] #tum yetkileri elinden al
				domain = [('name','=','www')]
				if self.dealer:
					domain = [('is_ortagi_default','=',True)]
				if self.esnaf:
					domain = [('uye_is_yeri_default','=',True)]
				if self.kart_musterisi:
					domain = [('kurumsal_default','=',True)]
				default_groups    =  self.env['res.groups'].sudo().search( domain )
				
				g_ids = list(map(lambda x:(4,x.id),default_groups))
					
				
				user_obj.groups_id = g_ids
				web_site_grp = self.env['res.groups'].sudo().search( ['|',('name','=','Editor and Designer'),
																		  ('name','=','Restricted Editor')])
				if web_site_grp:
					for w in web_site_grp:
						user_obj.groups_id = [(3,w.id)]
				account = self.env['account.account'].sudo()
		
				tax     = self.env['account.tax'].sudo()
				tax_satis_dict_18 = dict(
					name='KDV %18',
					amount = 18,
					description ='KDV %18' + '(%s)'%u'Satış',
					type_tax_use = 'sale',
					company_id = bayi_comp.id,
					active     = True
				)
				tax_satis_dict_8 = dict(
					name='KDV %8',
					description ='KDV %8' + '(%s)'%u'Satış',
					amount = 8,
					type_tax_use = 'sale',
					company_id = bayi_comp.id,
					active = True,
				)
				tax_alis_dict_18 = dict(
					name='KDV %18',
					amount = 18,
					description ='KDV %18' + '(%s)'%u'Satınalma',
					type_tax_use = 'purchase',
					company_id = bayi_comp.id,
					active     = True
				)
				tax_alis_dict_8 = dict(
					name='KDV %8',
					description ='KDV %8' + '(%s)'%u'Satınalma',
					amount = 8,
					type_tax_use = 'purchase',
					company_id = bayi_comp.id,
					active = True,
				)
		
				tax_obj_18 = tax.create( tax_satis_dict_18 )
				tax_obj_8  = tax.create( tax_satis_dict_8 )
				tax_obj_alis_18 = tax.create( tax_alis_dict_18 )
				tax_obj_alis_8  = tax.create( tax_alis_dict_8 )
		
				account_master =  dict(
					name = self.name + ' Temel',
					code = self.name[:4] + ' T',
					type = 'view',
					user_type_id = self.env['account.account.type'].sudo().search([('type','=','other')])[0].id,
					company_id = bayi_comp.id,
					#currency_id = bayi_comp.currency_id.id
				)
				mas_account  = account.create( account_master )
				account_in_dict = dict(
					name = self.name + ' Gelir',
					parent_id = mas_account.id,
					code = self.name[:4] + ' IN',
					type = 'other',
					user_type_id = self.env['account.account.type'].sudo().search([('type','=','liquidity')])[0].id,
					company_id = bayi_comp.id,
					reconcile  = True,
					tax_ids = [(4,tax_obj_alis_18.id),(4,tax_obj_alis_8.id)],
					#currency_id = bayi_comp.currency_id.id
				)
				account_out_dict = dict(
					name = self.name + ' Gider',
					parent_id = mas_account.id,
					code = self.name[:4] + ' OUT',
					type = 'other',
					user_type_id = self.env['account.account.type'].sudo().search([('type','=','liquidity')])[0].id,
					company_id = bayi_comp.id,
					reconcile  = True,
					tax_ids = [(4,tax_obj_8.id),(4,tax_obj_18.id)],
					#currency_id = bayi_comp.currency_id.id
				)
				account_deb_dict = dict(
					name = self.name + u' Alacaklılar',
					parent_id = mas_account.id,
					code = self.name[:4] + 'Alacak',
					type = 'receivable',
					user_type_id = self.env['account.account.type'].sudo().search([('type','=','receivable')])[0].id,
					company_id = bayi_comp.id,
					reconcile  = True,
					tax_ids = [(4,tax_obj_alis_8.id),(4,tax_obj_alis_18.id)],
					#currency_id = bayi_comp.currency_id.id
				)
				account_cre_dict = dict(
					name = self.name + u' Borçlular',
					parent_id = mas_account.id,
					code = self.name[:4] + u' Borç',
					type = 'payable',
					user_type_id = self.env['account.account.type'].sudo().search([('type','=','payable')])[0].id,
					company_id = bayi_comp.id,
					reconcile  = True,
					tax_ids = [(4,tax_obj_8.id),(4,tax_obj_18.id)],
					#currency_id = bayi_comp.currency_id.id
				)
		
		
				in_account  = account.create( account_in_dict )
				out_account = account.create( account_out_dict )
				debt_account = account.create( account_deb_dict)
				cre_account = account.create( account_cre_dict)
		
				props      = self.env['ir.property'].sudo()
		
				pricelist_comp = self.env['product.pricelist'].sudo()
		
				sale_pricelist_comp_dict = dict (
					name = self.name[:4] + 'Sale Pricelist',
					currency_id = self.env['res.currency'].sudo().search([('name','=','TRY')])[0].id,
					active = True,
					company_id = self.id,
					type = 'sale'
				)
		
				
				products       = self.env['product.product'].sudo().search([('tracking','=','serial'),('company_id','=',False)])
				#profile_prices_obj = self.profil.products_rel
				#price_type         = self.env['product.price.type'].sudo().search([('field','=','standard_price')]).id
				for p in products:
		
					props_in_dict = dict(name = 'property_account_income_id',
								  type = 'many2one',
								  company_id = bayi_comp.id,
								  fields_id = self.env['ir.model.fields'].sudo().search([('model','=','product.template'),('name','=','property_account_income_id')])[0].id,
								  res_id = 'product.template,' + str( p.product_tmpl_id.id),
								  value_reference = 'account.account,' + str(in_account.id)
								  )
					props_out_dict = dict(name = 'property_account_expense_id',
								  type = 'many2one',
								  company_id = bayi_comp.id,
								  fields_id = self.env['ir.model.fields'].sudo().search([('model','=','product.template'),('name','=','property_account_expense_id')])[0].id,
								  res_id = 'product.template,' + str( p.product_tmpl_id.id),
								  value_reference = 'account.account,' + str(out_account.id)
								  )
		
					props.create( props_in_dict )
					props.create( props_out_dict )
		
					p.taxes_id          = [(4,tax_obj_8.id)]
					p.supplier_taxes_id = [(4,tax_obj_alis_8.id)]
		
					
				sup = self.parent_id
				
				account_journal        = self.env['account.journal'].sudo()
		
				account_journal_p_dict = dict(
					name = self.name + ' Satinalma Defteri',
					code = self.name[:3] + '-P',
					default_debit_account_id  = out_account.id,
					default_credit_account_id = out_account.id,
					type = 'purchase',
					company_id = bayi_comp.id,
					user_id    = user_obj.id,
					#currency_id = bayi_comp.currency_id.id
					)
		
				account_journal_s_dict = dict(
					name = self.name + ' Satis Defteri',
					code = self.name[:3] + '-S',
					default_debit_account_id  = in_account.id,
					default_credit_account_id = in_account.id,
					type = 'sale',
					company_id = bayi_comp.id,
					user_id    = user_obj.id,
					#currency_id = bayi_comp.currency_id.id
					)
		
				account_journal_kasa_dict = dict(
					name = u'Peşin (Kasa)',
					code = self.name[:3] + '-K',
					default_debit_account_id  = in_account.id,
					default_credit_account_id = in_account.id,
					type        = 'cash',
					company_id  = bayi_comp.id,
					user_id     = user_obj.id,
					#currency_id = bayi_comp.currency_id.id
					)
				account_journal_bank_dict = dict(
					name = u'Havale / EFT',
					code = self.name[:3] + '-B',
					default_debit_account_id  = in_account.id,
					default_credit_account_id = in_account.id,
					type = 'bank',
					company_id = bayi_comp.id,
					user_id    = user_obj.id,
					#currency_id = bayi_comp.currency_id.id
					)
				account_journal_kredikarti_dict = dict(
					name = u'Kredi Kartı',
					code =  self.name[:3] + '-KK',
					default_debit_account_id  = out_account.id,
					default_credit_account_id = out_account.id,
					type = 'bank',
					company_id = bayi_comp.id,
					user_id    = user_obj.id,
					kk = True
					#currency_id = bayi_comp.currency_id.id
					)
				account_journal_check_dict = dict(
					name = u'Çek',
					code =  self.name[:3] + '-CH',
					default_debit_account_id  = in_account.id,
					default_credit_account_id = in_account.id,
					type = 'bank',
					company_id = bayi_comp.id,
					user_id    = user_obj.id,
					check = True
					#currency_id = bayi_comp.currency_id.id
					)
		
				account_journal.create( account_journal_p_dict)
				account_journal.create( account_journal_s_dict)
				account_journal.create( account_journal_kasa_dict)
				account_journal.create( account_journal_bank_dict)
				account_journal.create( account_journal_kredikarti_dict)
				account_journal.create( account_journal_check_dict)
		
				warehouse = self.env['stock.warehouse'].search([('company_id','=',bayi_comp.id)])
				#self.so_from_po  = True
				bayi_comp.warehouse_id = warehouse.id
				bayi_comp.po_from_so = True
				bayi_comp.parent_name = self.parent_id.name
				bayi_comp.intercompany_user_id = user_obj
		
				props_in_dict = dict(name = 'property_account_receivable_id',
					type = 'many2one',
					company_id = bayi_comp.id,
					fields_id = self.env['ir.model.fields'].sudo().search([('model','=','res.partner'),('name','=','property_account_receivable_id')])[0].id,
					res_id = 'res.partner,' + str( self.main_dealer.partner_id.id),
					value_reference = 'account.account,' + str(debt_account.id)
				)
				props_in_dict2 = dict(name = 'property_account_receivable_id',
					type = 'many2one',
					company_id = bayi_comp.id,
					fields_id = self.env['ir.model.fields'].sudo().search([('model','=','res.partner'),('name','=','property_account_receivable_id')])[0].id,
					res_id = 'res.partner,' + str( self.id ),
					value_reference = 'account.account,' + str(debt_account.id)
				)
				##print "---------------->"
				##print "01 ) ", props_in_dict
				props.create( props_in_dict )
				props.sudo( user_obj.id).create( props_in_dict2 )
				
				props_out_dict = dict(name = 'property_account_payable_id',
								  type = 'many2one',
								  company_id =  bayi_comp.id,
								  fields_id = self.env['ir.model.fields'].sudo().search([('model','=','res.partner'),('name','=','property_account_payable_id')])[0].id,
								  res_id = 'res.partner,' + str(  self.main_dealer.partner_id.id),
								  value_reference = 'account.account,' + str(cre_account.id)
								  )
				props_out_dict2 = dict(name = 'property_account_payable_id',
								  type = 'many2one',
								  company_id =  bayi_comp.id,
								  fields_id = self.env['ir.model.fields'].sudo().search([('model','=','res.partner'),('name','=','property_account_payable_id')])[0].id,
								  res_id = 'res.partner,' + str(  self.id),
								  value_reference = 'account.account,' + str(cre_account.id)
								  )
				##print "02 ) ", props_out_dict
				props.create( props_out_dict )
				props.sudo( user_obj.id).create( props_out_dict2 )
		
				self_rece_account = self.env['account.account'].search([('internal_type','=','receivable'),('company_id','=',self.main_dealer.id)])
				props_in_dict = dict(name = 'property_account_receivable_id',
					type = 'many2one',
					company_id = self.main_dealer.id,
					fields_id = self.env['ir.model.fields'].sudo().search([('model','=','res.partner'),('name','=','property_account_receivable_id')])[0].id,
					res_id = 'res.partner,' + str( self.id),
					value_reference = 'account.account,' + str(self_rece_account[0].id)
				)
		
		
				##print "03 ) ", props_in_dict
				
				#props.create( props_in_dict )
		
				self_pay_account = self.env['account.account'].search([('internal_type','=','payable'),('company_id','=',self.main_dealer.id)])
		
				props_out_dict = dict(name = 'property_account_payable_id',
								  type = 'many2one',
								  company_id =  self.main_dealer.id,
								  fields_id = self.env['ir.model.fields'].sudo().search([('model','=','res.partner'),('name','=','property_account_payable_id')])[0].id,
								  res_id = 'res.partner,' + str(  self.id),
								  value_reference = 'account.account,' + str(self_pay_account[0].id)
								  )
				##print "04 ) ", props_out_dict
				#props.create( props_out_dict )
		
		
		
				props_in_dict = dict(
					name = 'property_account_receivable_id',
					type = 'many2one',
					company_id = bayi_comp.id,
					fields_id = self.env['ir.model.fields'].sudo().search([('model','=','res.partner'),('name','=','property_account_receivable_id')])[0].id,
					res_id = 'res.partner,' + str( self.id),
					value_reference = 'account.account,' + str(debt_account.id)
				)
		
				##print "1 ) ", props_in_dict
				#props.create( props_in_dict )
		
		
				props_out_dict = dict(
					name = 'property_account_payable_id',
					type = 'many2one',
					company_id =  bayi_comp.id,
					fields_id = self.env['ir.model.fields'].sudo().search([('model','=','res.partner'),('name','=','property_account_payable_id')])[0].id,
					res_id = 'res.partner,' + str(  self.id),
					value_reference = 'account.account,' + str(cre_account.id)
				)
		
				##print "2 ) ", props_out_dict
				#props.create( props_out_dict )
		
				if self.main_dealer.id != 1:
		
					props_in_dict = dict(
						name = 'property_account_receivable_id',
						type = 'many2one',
						company_id = bayi_comp.id,
						fields_id = self.env['ir.model.fields'].sudo().search([('model','=','res.partner'),('name','=','property_account_receivable_id')])[0].id,
						res_id = 'res.partner,' + str(  self.env['res.company'].sudo().browse([1]).partner_id.id ),
						value_reference = 'account.account,' + str(debt_account.id)
					)
					##print "3 ) ", props_in_dict
					props.create( props_in_dict )
		
		
					props_out_dict = dict(
						name = 'property_account_payable_id',
						type = 'many2one',
						company_id =  bayi_comp.id,
						fields_id = self.env['ir.model.fields'].sudo().search([('model','=','res.partner'),('name','=','property_account_payable_id')])[0].id,
						res_id='res.partner,' + str(self.env['res.company'].sudo().browse([1]).partner_id.id),
						value_reference = 'account.account,' + str(cre_account.id)
					)
		
					##print "4 ) ", props_out_dict
					props.create( props_out_dict )
		
					self_info_account = self.env['account.account'].search( [('internal_type', '=', 'receivable'), ('company_id', '=', 1)])
					##print self_info_account
		
					if len(self_info_account) <= 0:
						raise exceptions.ValidationError(u'İnfoteks İçin Alıcı hesabı bulunamadı. Lütfen bir satış hesabı tanımlayınız...')
		
					props_in_dict = dict(
						name = 'property_account_receivable_id',
						type = 'many2one',
						company_id = 1,
						fields_id = self.env['ir.model.fields'].sudo().search([('model','=','res.partner'),('name','=','property_account_receivable_id')])[0].id,
						res_id = 'res.partner,' + str( self.id ),
						value_reference = 'account.account,' + str(self_info_account[0].id)
					)
		
					##print "5 ) ",  props_in_dict
					#props.create( props_in_dict )
		
		
					self_info_account = self.env['account.account'].search( [('internal_type', '=', 'payable'), ('company_id', '=', 1)])
		
		
					##print self_info_account
		
					if len(self_info_account) <= 0:
						raise exceptions.ValidationError(u'İnfoteks İçin Satış hesabı bulunamadı. Lütfen bir satış hesabı tanımlayınız...')
					props_out_dict = dict(
						name = 'property_account_payable_id',
						type = 'many2one',
						company_id =  1,
						fields_id = self.env['ir.model.fields'].sudo().search([('model','=','res.partner'),('name','=','property_account_payable_id')])[0].id,
						res_id = 'res.partner,' + str(  self.id ),
						value_reference = 'account.account,' + str(self_info_account[0].id)
					)
				for route in warehouse.route_ids:
					route.company_id = bayi_comp.id
			
			if self.dealer or self.kart_musterisi or self.bireysel_musteri: 
				self.state = 'confirmed'
		
		if self.esnaf:
			#ws_bildirimi
			basvuru_kayit( self )
			#doc_yolla(self)
			#ws_bildirimi
			
		return {}
				
	@api.multi
	def on_receive_docs(self):
		self.ensure_one()
		action={
			'name':'Dosyaları Onayla',
			'type':'ir.actions.act_window',
			'src_model':'res.partner',
			'res_model':'info_bayi.doc_wizard',
			'view_type':'form',
			'view_mode':'form',
			'target':'new'
		}
		return action
			
	@api.multi
	def write( self, vals):
		print ('State Ayarı Write')
		res =  super( res_partner, self).write( vals )
		for s in self:
			c_year = datetime.now().year
			if vals.get('kurulus_yili'):
				# year = int(vals.get('kurulus_yili')[:4])
				year = vals.get('kurulus_yili')
				if year > c_year:
					raise exceptions.ValidationError('Kuruluş Yılı Bu Yıldan Büyük Olamaz.')
				elif year < 1980:
					raise exceptions.ValidationError('Kuruluş Yılı 1980 den Küçük Olamaz')
			
			if s.dealer:
				
				company_id    = self.env['res.company'].sudo().search([('partner_id','=',self.id)])
				users = self.env['res.users'].search([('company_id','=',company_id.id)])
				
			

			if s.esnaf or s.bireysel_musteri or s.dealer or s.kart_musterisi:
				if s.state in ('draft','waiting_confirm','missing_parameters','sended'):
					if not vals.get('state'):

						state    = s.state
						totals   = []
						totals_s = []
						
						dom = [('sahis','=',False),('sermaye','=',False),('ortaklik','=',False)]
						if s.sahis_sirketi:
							dom = [('sahis','=',True)]
						if s.sermaye_sirketi:
							dom = [('sermaye','=',True)]
						if s.ortaklik:
							dom = [('ortaklik','=',True)]
						if s.bireysel_musteri:
							dom.append (('cari_tipi','=','bireysel'))
						if s.kart_musterisi:
							dom.append (('cari_tipi','=','kurumsal'))
						if s.dealer:
							dom.append (('cari_tipi','=','is_ortagi'))
						if s.esnaf:
							dom.append (('cari_tipi','=','esnaf'))
						evrak_tipleri = len(self.env['info_bayi.evrak_tipleri'].search( dom ))

						evrak_list = 0
						for y in s.uye_is_yeri_evrak:
							if len(y.docs) > 0:
								evrak_list += 1
						if evrak_list >= evrak_tipleri:
							state = 'waiting_confirm'
						else:
							state = 'missing_parameters'

						if state == 'waiting_confirm':
							for y in s.uye_is_yeri_evrak:
								if len(y.docs) == 0:
									state = 'missing_parameters'
									break

						s.state = state
		
		if vals.get('main_dealer') and self.company_id.sudo().parent_id:
			self.company_id.sudo().parent_id = vals.get('main_dealer')
		return res

	@api.model
	def create( self, vals ):
		c_year = datetime.now().year
		if vals.get('kurulus_yili'):
			# year = int(vals.get('kurulus_yili')[:4])
			year = vals.get('kurulus_yili')
			if year > c_year:
				raise exceptions.ValidationError('Kuruluş Yılı Bu Yıldan Büyük Olamaz.')
			elif year < 1900:
				raise exceptions.ValidationError('Kuruluş Yılı 1980 den Küçük Olamaz')
			
		if vals.get('sirket_tipi_selection') == 'sahis':
			vals['sahis_sirketi'] = True
			
		if vals.get('sirket_tipi_selection') == 'ltd':
			vals['sermaye_sirketi'] = True
			
		if vals.get('sirket_tipi_selection') == 'adi':
			vals['ortaklik'] = True
			
		res       = super( res_partner ,self ).create( vals )
		#infoteks iliskiyi burada bas....
		evrak_donen = False
		if vals.get('sirket_tipi_selection') == 'sahis':
			evrak_donen = res._onchange_sahis_sirketi()
		if vals.get('sirket_tipi_selection') == 'ltd':
			evrak_donen =res._onchange_sermaye_sirketi()
		if vals.get('sirket_tipi_selection') == 'adi':
			evrak_donen =res._onchange_ortaklik()
		
		if evrak_donen:
			res.uye_is_yeri_evrak = evrak_donen['value']['uye_is_yeri_evrak']

		if vals.get('esnaf') or res.bireysel_musteri:
			'''
			dom = []
			
			if res.sahis_sirketi:
				dom = [('sahis','=',True)]
			if res.sermaye_sirketi :
				dom = [('sermaye','=',True)]
			if res.ortaklik :
				dom = [('ortaklik','=',True)]
			if res.bireysel_musteri:
				dom = [('bireysel','=',True)]
				
			evrak_tipleri = len(self.env['info_bayi.evrak_tipleri'].search( dom ))
			
			evrak_list = 0
			for y in res.uye_is_yeri_evrak:
				if len(y.docs) > 0:
					evrak_list += 1
			print evrak_tipleri, evrak_list
			if evrak_list == evrak_tipleri:
				state = 'waiting_confirm'
			else:
				state = 'missing_parameters'
			'''
			state = 'predraft'
			
			res.state = state
			
		elif vals.get('dealer') :
			state = 'waiting_confirm'
			dom = [('sahis','=',False),('sermaye','=',False),('ortaklik','=',False)]
			if res.sahis_sirketi:
				dom = [('sahis','=',True)]
			if res.sermaye_sirketi:
				dom = [('sermaye','=',True)]
			if res.ortaklik:
				dom = [('ortaklik','=',True)]
			if res.bireysel_musteri:
				dom.append (('cari_tipi','=','bireysel'))
			if res.kart_musterisi:
				dom.append (('cari_tipi','=','kurumsal'))
			if res.dealer:
				dom.append (('cari_tipi','=','is_ortagi'))
			if res.esnaf:
				dom.append (('cari_tipi','=','esnaf'))
			evrak_tipleri = len(self.env['info_bayi.evrak_tipleri'].search( dom ))
			
			evrak_list = 0
			for y in res.uye_is_yeri_evrak:
				if len(y.docs) > 0:
					evrak_list += 1
			if evrak_list == evrak_tipleri:
				state = 'waiting_confirm'
			else:
				state = 'missing_parameters'

			res.state = state

			if self.env.user.company_id.id != MERKEZ:
				if not res.main_dealer:
					res.main_dealer = self.env.user.sudo().company_id.id
				
			res.bayi_kodu    = self.get_bayi_kodu( res.city_combo.plaka , res.bolge_sira_no )

		else:
			res.state = 'confirmed'
			res.release_credit_hold = True
		if res.parent_id:
			package = dict(company_id = res.parent_id.sudo().company_id.id,
						   turkpara_e_para_hesap = res.parent_id.sudo().turkpara_e_para_hesap,
						   is_company = True,
						   customer = True
						   )
			res.write( package )
		
		else:   
			if res.is_company:
				props = self.env['ir.property'].sudo() 
				p     = res.sudo().company_id
				
				while p:
					debt_account = self.env['account.account'].sudo().search([('company_id','=',p.id),
																	   ('internal_type','=','receivable')], order='id',limit=1)
					cre_account  = self.env['account.account'].sudo().search([('company_id','=',p.id),
																	   ('internal_type','=','payable')], order='id', limit=1)
	
					props_in_dict = dict(
					name = 'property_account_receivable_id',
					type = 'many2one',
					company_id = p.id,
					fields_id = self.env['ir.model.fields'].sudo().search([('model','=','res.partner'),('name','=','property_account_receivable_id')])[0].id,
					res_id = 'res.partner,' + str( res.id ),
					value_reference = 'account.account,' + str(debt_account.id)
					)
					pr = props.create( props_in_dict )
					props_out_dict = dict(
						name = 'property_account_payable_id',
						type = 'many2one',
						company_id =  p.id,
						fields_id = self.env['ir.model.fields'].sudo().search([('model','=','res.partner'),('name','=','property_account_payable_id')])[0].id,
						res_id = 'res.partner,' + str( res.id ),
						value_reference = 'account.account,' + str(cre_account.id)
					)
					pr2 = props.create( props_out_dict )
	
					
					p = p.parent_id
		
		return res
	
	@api.multi
	def get_aktif_pos_action( self ):
		
		action={
			'name':'Aktif Üye İş Yerleri',
			'type':'ir.actions.act_window',
			'res_model':'res.partner',
			'view_type':'form',
			'view_mode':'kanban,tree,form',
			'views' : [[self.env.ref('info_extensions.new_partner_kanban_view').id,'kanban'],
						[self.env.ref('base.view_partner_tree').id,'tree'],
						[self.env.ref('info_bayi.aktif_res_partner_form_view').id,'form']
					],
			'filter':True,
			'context':"{'disable_create':True}",
			
		}
		
		partners = self.search([('state','in',('confirmed','islak_bekleniyor','giris','muhasebe')),
								  ('customer','=',True),
								  ('is_company','=',True),
								  ('turkpara_e_para_hesap','!=',False),
								  ])

		p_ids = []
		for p in partners:
			if len(list(filter(lambda x:x if x.state in ('confirmed','guncellendi','aktif_edildi') else None, p.poslar))) > 0 and  p.id != 1:
				p_ids.append( p.id )
			for c in p.child_ids:
				if len(list(filter(lambda x:x if x.state in ('confirmed','guncellendi','aktif_edildi') else None, c.poslar))) > 0 and  c.id != 1:
					p_ids.append( c.id )
		
		#p_ids.append( self.env.user.company_id.partner_id.id)
		domain = [('id','in',p_ids)]
		##print domain
		action['domain'] = domain
		return action
	
	@api.multi
	def get_pasif_pos_action( self ):
		
		action={
			'name':'Pasif Üye İş Yerleri',
			'type':'ir.actions.act_window',
			'res_model':'res.partner',
			'view_type':'form',
			'view_mode':'kanban,tree,form',
			'views' : [[self.env.ref('info_extensions.new_partner_kanban_view').id,'kanban'],
						[self.env.ref('base.view_partner_tree').id,'tree'],
						[self.env.ref('info_bayi.aktif_res_partner_form_view').id,'form']],
			'filter':True,
			'context':"{'disable_create':True}",
			
		}
		partners = self.search([('state','in',('confirmed','islak_bekleniyor','giris','muhasebe')),
								  ('customer','=',True),
								  ('is_company','=',True),
								  ('turkpara_e_para_hesap','!=',False),
								  ])
		p_ids = []
		
		for p in partners:
			pasif = False
			if p.id != 1:
				for p_ in p.poslar:
					pasif = True
					#print p_.partner_id.name
					if p_.state in ('confirmed','guncellendi','aktif_edildi'):
						pasif=False
						break
				#print p.name , pasif
				if pasif:
					p_ids.append( p.id )
		
		#p_ids.append( self.env.user.company_id.partner_id.id)
		domain = [('id','in',p_ids)]
		
		##print domain
		
		action['domain'] = domain
		
		
		return action
	
	@api.multi
	def fis_onizle(self):
		self.ensure_one()
		return {
			 'type' : 'ir.actions.act_url',
			 'url': '/web/fp/%s'%(self.id),
			 'target': 'popup',
		}

	@api.multi
	def check_report_excel(self):
		raise UserError(
			_('Not Implemented!'))

	@api.multi
	def sozlesme_yazdir(self):
		self.ensure_one()
		self.create_sozlesme_barcode(self)
		return {
			'type': 'ir.actions.act_url',
			'url': '/report/pdf/info_bayi.report_sozlesme/' + str(self.id),
			'target': 'new'
		}
	
	@api.multi
	def pos_talimati_yazdir(self):
		self.ensure_one()
		
		return {
			'type': 'ir.actions.act_url',
			'url': '/info_bayi/static/sozlesme/POS%20TALİMATI%20(KLT.FRM.03-V01-30102018).pdf',
			'target': 'new'
		}
	
	@api.multi
	def kvkk_yazdir(self):
		self.ensure_one()
		url = '/info_bayi/static/sozlesme/KVKK%20AYDINLATMA%20METNİ%20ÜYE%20İŞ%20YERİ%20(KLT.FRM.02-V01-30102018).pdf'
		if self.kart_musterisi:
			url = "/info_kart/static/KVKK AYDINLATMA METNİ KURUMSAL MÜŞTERİ (KLT.FRM.02-V01-30102018).pdf"
		return {
			'type': 'ir.actions.act_url',
			'url': url,
			'target': 'new'
			}

	@api.multi
	def go_to_bankayla_anlasamadi( self):
		self.ensure_one()
		self.state = 'bankayla_anlasamadi'
	
	@api.multi
	def go_to_slip_alinacak( self):
		self.ensure_one()
		self.state = 'slip_alinacak'

