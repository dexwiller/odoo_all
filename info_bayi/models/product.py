# -*- coding: utf-8 -*-
from openerp import fields, models,api
class product_product(models.Model):
	_inherit = 'product.product'

	orj_name               = fields.Char(string = 'Ürün Orjinal Adı')
class product_template(models.Model):
	_inherit = 'product.template'
	
	def get_intercompany_account_settings(self, rec):
		for f in self.env['res.company'].sudo().search([('id','!=',1)]):
			user = f.intercompany_user_id.id
			if user:
				valu = rec.sudo( user )
				if not valu.categ_id.property_account_income_categ_id or not valu.categ_id.property_account_expense_categ_id: 
					
					a    = self.env['account.account'].sudo( user ).search([('internal_type','=','other'),
																			('company_id','=',f.id)])[-1].id
					valu.categ_id.write ({'property_account_income_categ_id'  :  a,
										  'property_account_expense_categ_id' :  a
										})
	@api.model
	def create(self,values):
		val = super(product_template,self ).create( values )
		if not val.company_id:
			self.get_intercompany_account_settings( val )
		return val
	
	@api.multi
	def write(self,values):
		val = super(product_template,self ).write( values )
		for s in self:
			if not s.company_id:
				self.get_intercompany_account_settings( s )
		return val

class product_category( models.Model):
	_inherit = 'product.category'

	