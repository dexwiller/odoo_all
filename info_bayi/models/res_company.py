# -*- coding: utf-8 -*-
from odoo import models, fields, api
class res_company( models.Model):
	_inherit = 'res.company'

	infopos_bayi = fields.Boolean(string ='İnfoPos Bayi')
	bayi 		 = fields.Boolean(string ='ERP Bayi')
	@api.model
	def create( self,vals ):
		res = super( res_company, self).create( vals )
		if not res.partner_id.esnaf and not res.partner_id.kart_musterisi : res.partner_id.dealer = True
		return res