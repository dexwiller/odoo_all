# -*- coding: utf-8 -*-
from openerp import fields, models,api,exceptions,registry
from suds.client import Client as sclient
from datetime import datetime, timedelta, date as bd
import pytz
import base64, traceback
from .util_models import path
from suds.plugin import MessagePlugin
from suds.cache import NoCache
import base64,urllib,ast,copy
import logging

_logger = logging.getLogger(__name__)

ws_odeme_servis = 'service_turkpos_prod'
test_guid       = '0c13d406-873b-403b-9c09-a5766840d98c'
pos_guid        = 'B77267F6-118A-462E-A77B-FBFDFAD7CA68'


def get_utc(time):
  local = pytz.timezone ("Europe/Istanbul")
  naive = datetime.strptime (time, "%Y-%m-%d %H:%M:%S")
  local_dt = local.localize(naive)
  utc_dt = local_dt.astimezone(pytz.utc)
  
  return naive
  
class dummy:
  pass

class LogPlugin(MessagePlugin):
  def sending(self, context):
    _logger.info( 'Sended : ' + str(context.envelope))
  def received(self, context):
    _logger.info( 'Received : ' + str(context.reply))

def get_date_format(date):
    print (type(date))
    if isinstance(date,bd):
        return date.strftime('%d.%m.%Y')
    try:
      s = date.split('-')
      return s[2] + '.' + s[1] + '.' + s[0]
    except:
      return date
    
def fastest_object_to_dict(obj):
      if not hasattr(obj, '__keylist__'):
          return obj
      data = {}
      fields = obj.__keylist__
      for field in fields:
          val = getattr(obj, field)
          if isinstance(val, list):  # tuple not used
              data[field] = []
              for item in val:
                  data[field].append(fastest_object_to_dict(item))
          else:
              data[field] = fastest_object_to_dict(val)
      return data
    
def set_log(obj,requestData,responseData,serviceName,operationName,time_request,proc_id=False): 
    
    time_response = datetime.today()
    logging_obj = obj.env['info_bayi.turkpara_ws_logs']
    
    logging_var={'serviceName':serviceName,'operationName':operationName,
                    'requestTime':time_request,'responseTime':time_response,
                    'requestData': requestData,
                    'responseData':responseData,
                    'proc_id':proc_id
                }
    #bunu ikinci cr da bas, commit et. raise exception da bassın db ye rollback yapmasın.
    with api.Environment.manage():
        with registry(logging_obj.env.cr.dbname).cursor() as new_cr:
            new_env = api.Environment(new_cr, 1, logging_obj.env.context)
            logging_obj.with_env(new_env).create(logging_var)
            new_env.cr.commit()

def guid_check( obj, type_ = 'kart' ):
    if not obj.guid:
        if type_ == 'partner':
            partner_guid = get_GUID( obj, obj.turkpara_e_para_hesap)
            obj.write({'guid':partner_guid})
        elif type_ == 'kart':
            #print obj,  obj.seri_no_computed
            guid = get_GUID( obj, obj.seri_no_computed.replace(" ",""))
            obj.write({'guid':guid})
    if obj.guid:
        return obj
    else:
        raise exceptions.ValidationError('%s için GUID Merkezden alınamadı' %obj.name)

def get_client(obj, service_name = 'service_yemekmatik'):
    ws_parameters = obj.env['info_bayi.turkpara_ws'].sudo().search([('ws_adres','like',service_name)],order = 'id desc', limit=1)
    if not ws_parameters:
        raise exceptions.ValidationError(u'Türk Para Bildirimi İçin Lütfen WebServis Erişim Ayarlarını Yapılandırınız.')
    try:
        client = sclient(ws_parameters.ws_adres, cache=NoCache(),plugins=[LogPlugin()])
        #
    except:
        raise exceptions.ValidationError(u'Türk Para WS Hata: Servis istemcisi yaratılamadı')
    
    g 						    = client.factory.create( 'ST_WS_Guvenlik')
    g.CLIENT_CODE		 	= ws_parameters.ws_cli_code
    g.CLIENT_USERNAME = ws_parameters.ws_cli_user
    g.CLIENT_PASSWORD = ws_parameters.ws_cli_pass
    
    return client,ws_parameters,g

def Cuzdan(obj):
    
    client,ws_parameters,g = get_client( obj)
    time_request = datetime.today()
    response = client.service.Cuzdan(G =   g)
    final_d_s = {}
    if response.Sonuc != 1:
        obj.state = 'error'
        obj.ws_hata_mesaji = response.Sonuc_Str
    else:
      r = fastest_object_to_dict( response )
      final_d = {}
      for dataset in r['DT']['diffgram']:
        for d in dataset['NewDataSet']:
            for final_d_s in d['Temp']:
                final_d[final_d_s['ID'][0]] = final_d_s['Cuzdan'][0].replace('Yemekmatik - ','')

    requestData = dict(CLIENT_CODE    	= ws_parameters.ws_cli_code,
                      CLIENT_USERNAME 	=	ws_parameters.ws_cli_user,
                      CLIENT_PASSWORD	  =	ws_parameters.ws_cli_pass,)
    set_log (obj, requestData, response,'TürkPara','Cüzdan',time_request)
   
    return final_d

def get_TL_oran(obj):
    client,ws_parameters,g = get_client( obj)
    time_request = datetime.today()
    response = client.service.TL_Oran(G =   g)
    t_dict = {}
    if response.Sonuc != 1:
        obj.state = 'error'
        obj.ws_hata_mesaji = response.Sonuc_Str
    else:
      if hasattr(response,'DT') and hasattr(response.DT,'diffgam'):
        for e in response.DT.diffgram:
          if hasattr(e,'NewDataSet'):
            for i in e.NewDataSet:
              if hasattr(i,'Temp') and len(i.Temp) > 0:
                for t in i.Temp:
                  t_dict = { t.kredi_kart_marka[0]:[round(float(t.T1),2),round(float(t.T3),2),round(float(t.T6),2),round(float(t.T9),2),round(float(t.T12),2)]}

    requestData = dict(CLIENT_CODE    	=   ws_parameters.ws_cli_code,
                                    CLIENT_USERNAME 	=	ws_parameters.ws_cli_user,
                                    CLIENT_PASSWORD	    =	ws_parameters.ws_cli_pass,)
    set_log (obj, requestData, response,'TürkPara','TL Oranlari',time_request)
    
    return t_dict
    
def pos_islemleri( pos_obj):
    client,ws_parameters,g = get_client( pos_obj)
    time_request = datetime.today()
    if pos_obj.islem_tipi == "1":
        PID = 0
    else:
        PID = int( pos_obj.pid)
    
    
    if pos_obj.isemri.partner_id.parent_id:
      guid = pos_obj.isemri.partner_id.parent_id.turkpara_e_para_hesap
    else:
      guid = pos_obj.isemri.partner_id.turkpara_e_para_hesap
      
    response = client.service.Pos_Islemleri(G                =   g,
                                            Kart_No          =   guid,
                                            Pos_No		       =   pos_obj.terminal_no,
                                            Isyeri_No        =   pos_obj.uye_isyeri_no,
                                            Islem_Tip        =   pos_obj.islem_tipi,
                                            PID              =   PID)
    
    requestData = dict(CLIENT_CODE    	=   ws_parameters.ws_cli_code,
                                    CLIENT_USERNAME 	  =	ws_parameters.ws_cli_user,
                                    CLIENT_PASSWORD	    =	ws_parameters.ws_cli_pass,
                                    Kart_No             = guid,
                                    Pos_No		          = pos_obj.terminal_no,
                                    Isyeri_No           = pos_obj.uye_isyeri_no,
                                    Islem_Tip           = pos_obj.islem_tipi,
                                    PID                 = PID)
    
    set_log (pos_obj, requestData, response,'TürkPara','Pos İşlemleri',time_request)

    if response.Sonuc != 1:
        pos_obj.state = 'error'
        pos_obj.ws_hata_mesaji = response.Sonuc_Str
    else:
        pos_obj.pid   = response.PID
        if pos_obj.islem_tipi == "1":
            pos_obj.state = 'confirmed'
        elif pos_obj.islem_tipi == "2":
            pos_obj.state = 'guncellendi'
        elif pos_obj.islem_tipi == "3":
            pos_obj.state = 'silindi'
        elif pos_obj.islem_tipi == "4":
            pos_obj.state = 'aktif_edildi'
        try:    
          if pos_obj.state in ( 'confirmed','guncellendi','aktif_edildi'):
            img  = open(path + 'info_bayi/static/img/aktif_cari.jpg','rb')
            byte = img.read()
            b43  = base64.b64encode( byte )
            pos_obj.partner_id.image = b43
        except:
          pass
        if pos_obj.state == 'silindi':
            aktif = False
            for diger in pos_obj.isemri.banka_rels:
                if diger.state != 'silindi':
                    aktif = True
                    break
            try:
              if not aktif:
                img  = open(path + 'info_bayi/static/img/pasif.png','rb')
                byte = img.read()
                b43  = base64.b64encode( byte )
                pos_obj.partner_id.image = b43
            except:
              pass

def pos_islemleri_2(pos_obj, islem_tipi=None, obj_type='cuzdan_rel', ex_cid=None):
    client, ws_parameters, g = get_client(pos_obj)
    time_request = datetime.today()
    BKM_ID = ''
    if not islem_tipi:
        islem_tipi = pos_obj.islem_tipi
    if obj_type == 'cuzdan_rel':

        CID = pos_obj.cid or 0
        TID = 0
        Cuzdan = pos_obj.cuzdan.kod
        Cuzdan_Oncelik = pos_obj.sequence
        Cuzdan_Indirim_Oran = pos_obj.indirim_
        Ozel_Durum = 1
        terminal_obj = pos_obj.terminal
        terminal_no = terminal_obj.terminal_no
        uye_isyeri_no = terminal_obj.uye_isyeri_no

        if pos_obj.islem_tipi == "12" or pos_obj.islem_tipi == "13":#yasakli ve yuksek oncelik.
            prev_islem_tipi = pos_obj.islem_tipi
            pos_obj.islem_tipi = "3"#silme
            pos_islemleri_2(pos_obj, obj_type='cuzdan_rel', ex_cid=CID)

            Ozel_Durum = int(prev_islem_tipi) - 10
            islem_tipi = 1
            TID = 0
            Cuzdan = pos_obj.cuzdan.kod
            Cuzdan_Oncelik = pos_obj.sequence
            Cuzdan_Indirim_Oran = pos_obj.indirim_

            pos_obj.islem_tipi = prev_islem_tipi

        if terminal_obj.isemri.partner_id.parent_id:
            guid = terminal_obj.isemri.partner_id.parent_id.turkpara_e_para_hesap
        else:
            guid = terminal_obj.isemri.partner_id.turkpara_e_para_hesap

    else:

        CID = ex_cid or 0
        TID = pos_obj.tid or 0
        Cuzdan = 1084
        Cuzdan_Oncelik = 999
        Cuzdan_Indirim_Oran = 0
        Ozel_Durum = 1

        terminal_no = pos_obj.terminal_no
        uye_isyeri_no = pos_obj.uye_isyeri_no
        if int(islem_tipi) == 3:
            Cuzdan = 0
            for cuz in pos_obj.cuzdanlar:
                if cuz.cuzdan.mcc_acik:
                    cuz.islem_tipi = "12"
                else:
                    cuz.islem_tipi = "3"

                pos_islemleri_2(cuz)

        if pos_obj.isemri.partner_id.parent_id:
            guid = pos_obj.isemri.partner_id.parent_id.turkpara_e_para_hesap
        else:
            guid = pos_obj.isemri.partner_id.turkpara_e_para_hesap

    if (terminal_no and uye_isyeri_no) or BKM_ID:

        if int(islem_tipi) == 2 and float(Cuzdan_Oncelik) == 0.0 and float(Cuzdan_Indirim_Oran) == 0.0:
            pos_obj.state = 'sended'
        else:
            response = client.service.Pos_Islemleri2(G=g,
                                                     Kart_No=guid or '',
                                                     Pos_No=terminal_no,
                                                     Isyeri_No=uye_isyeri_no,
                                                     Islem_Tip=islem_tipi,
                                                     CID=int(CID),
                                                     TID=int(TID),
                                                     BKM_ID=BKM_ID,
                                                     Cuzdan=Cuzdan,
                                                     Cuzdan_Oncelik=Cuzdan_Oncelik,
                                                     Cuzdan_Indirim_Oran=Cuzdan_Indirim_Oran,
                                                     Ozel_Durum=Ozel_Durum
                                                     )

            requestData = dict(G=g,
                               Kart_No=guid or '',
                               Pos_No=terminal_no,
                               Isyeri_No=uye_isyeri_no,
                               Islem_Tip=islem_tipi,
                               CID=int(CID),
                               TID=int(TID),
                               BKM_ID=BKM_ID,
                               Cuzdan=Cuzdan,
                               Cuzdan_Oncelik=Cuzdan_Oncelik,
                               Cuzdan_Indirim_Oran=Cuzdan_Indirim_Oran,
                               Ozel_Durum=Ozel_Durum)

            set_log(pos_obj, requestData, response, 'TürkPara', 'Pos İşlemleri', time_request)

            if not ex_cid:
                if response.Sonuc != 1 and response.Sonuc != -10001:

                    pos_obj.state = 'error'
                    pos_obj.ws_hata_mesaji = response.Sonuc_Str
                else:
                    if int(pos_obj.islem_tipi) == 1:
                        pos_obj.state = 'confirmed'
                    elif int(pos_obj.islem_tipi) == 2:
                        pos_obj.state = 'guncellendi'
                    elif int(pos_obj.islem_tipi) == 3:
                        pos_obj.state = 'silindi'
                    elif int(pos_obj.islem_tipi) == 12:
                        pos_obj.state = "yasaklandi"
                    elif int(pos_obj.islem_tipi) == 13:
                        pos_obj.state = "yuksek_oncelik"

                    if obj_type != 'cuzdan_rel':
                        pos_obj.tid = response.TID

                    else:
                        pos_obj.cid = response.CID
                        if response.TID:
                            pos_obj.terminal.tid = response.TID

                        aktif_c = False
                        if int(islem_tipi) == 3:
                            for c in pos_obj.terminal.cuzdanlar:
                                if c.state != 'silindi':
                                    aktif_c = True
                                    break
                            if obj_type == 'cuzdan_rel':
                                pos_obj.sudo().unlink()

                        else:
                            aktif_c = True

                        pos_obj = terminal_obj
                        if aktif_c:
                            pos_obj.state = 'guncellendi'
                        else:
                            pos_obj.state = 'silindi'

                    try:
                        if pos_obj.state in ('confirmed', 'guncellendi', 'sended'):
                            img = open(path + 'info_bayi/static/img/aktif_cari.jpg', 'rb')
                            byte = img.read()
                            b43 = base64.b64encode(byte)
                            pos_obj.partner_id.image = b43
                    except:
                        pass
                    if pos_obj.state == 'silindi':
                        aktif = False
                        for diger in pos_obj.isemri.banka_rels:
                            if diger.state != 'silindi':
                                aktif = True
                                break
                        try:
                            if not aktif:
                                img = open(path + 'info_bayi/static/img/pasif.png', 'rb')
                                byte = img.read()
                                b43 = base64.b64encode(byte)
                                pos_obj.partner_id.image = b43
                        except:
                            pass

    else:
        pos_obj.state = 'sended'
        if int(islem_tipi) == 3:
            pos_obj.unlink()

def basvuru_kayit(partner_obj):
    
    client,ws_parameters,g = get_client( partner_obj )
    if partner_obj.tckn:
        TC_VN = partner_obj.tckn
    else:
        TC_VN = partner_obj.taxNumber
    
    tel_no = partner_obj.gsm_no.replace('-','').replace(' ','')
    tip = 0
    if partner_obj.esnaf:
        tip = 1
    elif partner_obj.kart_musterisi:
        tip = 2
    elif partner_obj.bireysel_musteri:
        tip = 3
    
    time_request = datetime.today()
    
    response = client.service.Basvuru_Kayit(G                =   g,
                                            Unvan            =   partner_obj.name,
                                            TC_VN		         =   TC_VN,
                                            Yetkili_GSM      =   tel_no,
                                            Adres_IlKodu     =   int(partner_obj.city_combo.plaka),
                                            Tip              =   tip)
    
    requestData = dict(CLIENT_CODE    	=   ws_parameters.ws_cli_code,
                                   CLIENT_USERNAME 		=	ws_parameters.ws_cli_user,
                                   CLIENT_PASSWORD		=	ws_parameters.ws_cli_pass,
                                   TC_VN				=   TC_VN,
                                   Unvan            	=   partner_obj.name,
                                   Yetkili_GSM      	=   tel_no,
                                   Adres_IlKodu     	=   partner_obj.city_combo.plaka,
                                   Tip                  = tip)
    
    set_log(partner_obj, requestData, response,'TürkPara','Başvuru Kayıt',time_request )
    
    if response.Sonuc != 1:
        partner_obj.state = 'turkpara_ws_error'
        partner_obj.turkpara_ws_error = response.Sonuc_Str
    else:
        partner_obj.turkpara_basvuru_id   = response.Basvuru_ID
        if hasattr(response,'KN'):
            partner_obj.turkpara_e_para_hesap = response.KN
            for c in partner_obj.child_ids:
               c.turkpara_e_para_hesap = response.KN
            partner_obj.state = 'confirmed'
        else:
            if partner_obj.turkpara_e_para_hesap:
               partner_obj.state = 'confirmed'
            else:
              partner_obj.state = 'turkpara_ws_error'

def doc_yolla(partner_obj):
    
      client,ws_parameters,g = get_client( partner_obj)
      time_request           = datetime.today()
      
      basvuru_id              = partner_obj.turkpara_basvuru_id
      res_log                 = {}
      i                       = 0
      '''
      for f in partner_obj.__class__.file_fields:
          i += 1
          if getattr( partner_obj, f ):
              doc_name  =  getattr( partner_obj, f + '_ad' )
              belge_tip = f
              res_by_doc  = {} 
              uzanti      = doc_name.split('.')[-1]
              base64      = getattr( partner_obj, f )
              
              a = dict(G       =   g,
                                              Basvuru_ID       =   basvuru_id,
                                              Belge_Tip		 =   belge_tip,
                                              Belge_Uzanti     =   uzanti,
                                              Belge_Base64     =   base64)
              #print a
              
              response    = client.service.Belge_Yukle(G       =   g,
                                              Basvuru_ID       =   basvuru_id,
                                              Belge_Tip		 =   belge_tip,
                                              Belge_Uzanti     =   uzanti,
                                              Belge_Base64     =   base64)
              time_response = datetime.today()
              logging_obj = partner_obj.env['info_bayi.turkpara_ws_logs']
              
              logging_var={'serviceName':'TürkPara','operationName':'Belge Yükleme',
                              'requestTime':time_request,'responseTime':time_response,
                              'requestData':dict(CLIENT_CODE    	=   ws_parameters.ws_cli_code,
                                              CLIENT_USERNAME 	=	ws_parameters.ws_cli_user,
                                              CLIENT_PASSWORD	    =	ws_parameters.ws_cli_pass,
                                              Basvuru_ID          =   basvuru_id,
                                              Belge_Tip		    =   belge_tip,
                                              Belge_Uzanti        =   uzanti,
                                              Belge_Base64        =   'XXX'),
                              'responseData':response
                          }
          
              logging_obj.sudo().create(logging_var)
              res_log[ belge_tip ] = response
      '''
      for b in partner_obj.uye_is_yeri_evrak:
          belge_tip   = b.evrak_id.name
          belge_send  = b.evrak_id.ws_ile_gonder
          if b.state != 'turkpara_gonderildi' and belge_send and len(b.docs) > 0 and not b.sonradan_gonder:
              for doc in b.docs:
                  res_by_doc  = {} 
                  uzanti      = doc.doc_name.split('.')[-1]
                  base64      = doc.doc
                  
                  a = dict(G       =   g,
                                                  Basvuru_ID       =   basvuru_id,
                                                  Belge_Tip		 =   belge_tip,
                                                  Belge_Uzanti     =   uzanti,
                                                  Belge_Base64     =   base64)
                  #print a
                  
                  response    = client.service.Belge_Yukle(G       =   g,
                                                  Basvuru_ID       =   basvuru_id,
                                                  Belge_Tip		     =   belge_tip,
                                                  Belge_Uzanti     =   uzanti,
                                                  Belge_Base64     =   base64)
                  
                  requestData = dict(CLIENT_CODE    	   =   ws_parameters.ws_cli_code,
                                      CLIENT_USERNAME 	 =	 ws_parameters.ws_cli_user,
                                      CLIENT_PASSWORD	   =	 ws_parameters.ws_cli_pass,
                                      Basvuru_ID         =   basvuru_id,
                                      Belge_Tip		       =   belge_tip,
                                      Belge_Uzanti       =   uzanti,
                                      Belge_Base64       =   base64)
      
                  set_log ( partner_obj,requestData, response,'TürkPara','Belge Yükleme',time_request)
                  
                  res_by_doc[doc.id] = response
                  
              res_log[ belge_tip ] = res_by_doc    
      
      for k,v in res_log.items():
          if partner_obj.esnaf or partner_obj.bireysel_musteri:
              for d_id, res in v.items():
                  doc = partner_obj.sudo().env['info_bayi.uye_is_yeri_docs'].browse([ d_id ])
                  if res.Sonuc != 1:
                      doc.rel_id.state = 'turkpara_hata'
                      partner_obj.sudo().state = 'turkpara_ws_error'
                      partner_obj.sudo().turkpara_ws_error = res.Sonuc_Str
                      break;
                  else:
                      doc.turkpara_doc_id = res.Belge_ID
                      doc.rel_id.state = 'turkpara_gonderildi'
          else:
              #print v
              if v.Sonuc != 1:
                  partner_obj.sudo().state = 'turkpara_ws_error'
                  partner_obj.sudo().turkpara_ws_error = k + ' : ' + v.Sonuc_Str
                  break;
          if partner_obj.state == 'turkpara_ws_error':
              break;
        
def get_GUID(obj,kart_no):
    
    time_request           = datetime.today()
    client,ws_parameters,g = get_client( obj )
    try:
        response = client.service.Get_GUID(G            =   g,
                                       Troy_Kart_No = kart_no )
    except:
        response = traceback.format_exc()
    requestData = dict (CLIENT_CODE             = ws_parameters.ws_cli_code,
                        CLIENT_USERNAME 		= ws_parameters.ws_cli_user,
                        CLIENT_PASSWORD		    = ws_parameters.ws_cli_pass,
                        Troy_Kart_No            = 'XXX')
    
    set_log(obj,requestData,response,'TürkPara','Get_GUID',time_request)
    if not hasattr (response,'GUID'):
        return False
    else:
        return response.GUID
    
def bakiye(obj,type_='kart'):
    time_request           = datetime.today()
    client,ws_parameters,g = get_client( obj )
    client.set_options(timeout=30)
    
    obj = guid_check(obj,type_)
    try:
        response = client.service.Bakiye(G                = g,
                                         GUID             = obj.guid)
        
        #print response.Sonuc
        if hasattr(response,'Sonuc') and int(response.Sonuc) == 1:
            r = fastest_object_to_dict( response )
            for dataset in r['Bakiye']['diffgram']:
              for d in dataset['NewDataSet']:
                  for final_d_s in d['Temp']:
                    try:
                      
                      if int(final_d_s['Kredi'][0]) == 1000:
                        if type_ == 'partner':
                          obj.cuzdanlar[0].bakiye =  final_d_s['Bakiye'][0]
                          
                        else:
                          obj.serbest_limit = final_d_s['Bakiye'][0]
                      elif int(final_d_s['Kredi'][0]) == 1084:
                        obj.available_limit = final_d_s['Bakiye'][0]
                      elif int(final_d_s['Kredi'][0]) == 1102 and type_ == 'kart':
                        obj.gift_limit = final_d_s['Bakiye'][0]
                      
                      c_dict = {}
                      for c in obj.cuzdanlar:
                        c_dict[int(c.cuzdan.kod)]  = c
                      
                      if int(final_d_s['Kredi'][0]) in c_dict.keys():
                          c_dict[int(final_d_s['Kredi'][0])].bakiye = final_d_s['Bakiye'][0]
                      else:
                          cuzdan_ = obj.env['info_kart.cuzdan'].sudo().search([('kod','=',str(final_d_s['Kredi'][0]))])
                          if cuzdan_ : 
                            new_rel = dict( lot_id = obj.id,
                                            cuzdan = cuzdan_.id,
                                            bakiye = final_d_s['Bakiye'][0])
                            obj.env['info_kart.lot_cuzdan_rel'].sudo().create( new_rel )

                    except:
                      continue
        else:
           #print 'SONUC BASARISIZ'
           r = None
    except:
        r = None
        response = traceback.format_exc()
    
    requestData = dict (CLIENT_CODE     = ws_parameters.ws_cli_code,
                    CLIENT_USERNAME 	  = ws_parameters.ws_cli_user,
                    CLIENT_PASSWORD		  = ws_parameters.ws_cli_pass,
                    GUID                = obj.guid)

    set_log(obj,requestData,response,'TürkPara','Bakiye',time_request)
    
    if not r:
        raise exceptions.ValidationError(' %s için Bakiye Alınamadı.'%obj.name)
    return

def tl_iptal_iade(obj,dekont_no,tutar,bloke=0,cuzdan=0):
    time_request           = datetime.today()
    client,ws_parameters,g = get_client( obj )
    client.set_options(timeout=30)
    
    obj = guid_check(obj)
    try:
        response = client.service.TL_Iptal_Iade(G               = g,
                                                GUID            = obj.guid,
                                                Dekont_ID       = dekont_no,
                                                Tutar           = tutar,
                                              )
    except:
        response = traceback.format_exc()
    
    requestData = dict (CLIENT_CODE     = ws_parameters.ws_cli_code,
                    CLIENT_USERNAME 	  = ws_parameters.ws_cli_user,
                    CLIENT_PASSWORD		  = ws_parameters.ws_cli_pass,
                    Troy_Kart_No        = obj.guid,
                    Dekont_ID           = dekont_no,
                    Tutar               = tutar)
    #print response , dir( response)
    set_log(obj,requestData,response,'TürkPara','TL_Iptal_Iade',time_request)
    if not hasattr(response,'Sonuc'):
        raise exceptions.ValidationError(' %s için İptal İade Başarısız.'%obj.name)
    return response
  
def TL_Bloke_Islem(obj,tutar,cuzdan,islem_tipi):
    time_request           = datetime.today()
    client,ws_parameters,g = get_client( obj )
    client.set_options(timeout=30)
    
    obj = guid_check(obj)
    try:
        response = client.service.TL_Bloke_Islem(G              = g,
                                                GUID            = obj.guid,
                                                Cuzdan          = cuzdan,
                                                Tutar           = tutar,
                                                Islem_Tip       = islem_tipi
                                              )
    except:
        response = traceback.format_exc()
    
    requestData = dict (CLIENT_CODE     = ws_parameters.ws_cli_code,
                    CLIENT_USERNAME 	  = ws_parameters.ws_cli_user,
                    CLIENT_PASSWORD		  = ws_parameters.ws_cli_pass,
                    GUID            = obj.guid,
                                                Cuzdan          = cuzdan,
                                                Tutar           = tutar,
                                                Islem_Tip       = islem_tipi)
    #print response , dir( response)
    set_log(obj,requestData,response,'TürkPara','TL_Bloke_Islem',time_request)
    if not hasattr(response,'Sonuc'):
        raise exceptions.ValidationError(' %s için Bloke Başarısız.'%obj.name)
    return response
   
def kart_isle(partner_obj,kart_list):
    time_request           = datetime.today()
    client,ws_parameters,g = get_client( partner_obj )
    partner_obj = guid_check( partner_obj, 'partner')
    g_list = []
    
    for k in kart_list:
        k = guid_check(k)
        i = client.factory.create( 'ST_Kart_Liste')
        i.Troy_Kart_No = k.guid
        g_list.append( i ) 
    
    l      = client.factory.create('ArrayOfST_Kart_Liste')
    l.ST_Kart_Liste = g_list
    try:
        response = client.service.Kart_Isle(G                =   g,
                                        GUID             =   partner_obj.guid,
                                        Troy_KN_Liste    =   l)
    
    except:
        response = traceback.format_exc()
        
    requestData = dict (CLIENT_CODE         = ws_parameters.ws_cli_code,
                        CLIENT_USERNAME 	= ws_parameters.ws_cli_user,
                        CLIENT_PASSWORD		= ws_parameters.ws_cli_pass,
                        GUID                = partner_obj.guid,
                        Troy_KN_Liste       = l)
    
    set_log(obj,requestData,response,'TürkPara','Kart_Isle',time_request)
    if response.Sonuc != 1:
        return False
    else:
        return response.Alt_Firma_ID

def Kisisellestirme(obj, kw):
    #Kisi_TC, Kisi_Adi, Kisi_Soyadi, Kisi_GSM,Kisi_EPosta,Kisi_DogumTarihi
    time_request           = datetime.today()
    client,ws_parameters,g = get_client( obj )
    obj = guid_check( obj, 'partner')

    response = client.service.Kisisellestirme(G           =  g,
                                              GUID        =  obj.guid,
                                              Kisi_TC     =  kw['tckn'],
                                              Kisi_Adi    =  kw['ad'],
                                              Kisi_Soyadi =  kw['soyad'],
                                              Kisi_GSM    =  kw['cep_tel'],
                                              Kisi_EPosta =  kw['email'],
                                              Kisi_DogumTarihi = get_date_format(kw['dogum_tar'])
                                              )
    
    '''
    except:
        response = traceback.format_exc()
    '''    
    requestData = dict (CLIENT_CODE       = ws_parameters.ws_cli_code,
                        CLIENT_USERNAME 	= ws_parameters.ws_cli_user,
                        CLIENT_PASSWORD		= ws_parameters.ws_cli_pass,
                        GUID              = obj.guid,
                        Kisi_TC           = kw['tckn'],
                        Kisi_Adi          = kw['ad'],
                        Kisi_Soyadi       = kw['soyad'],
                        Kisi_GSM          = kw['cep_tel'],
                        Kisi_EPosta       = kw['email'],
                        Kisi_DogumTarihi  = get_date_format(kw['dogum_tar'])
                        )
    
    set_log(obj,requestData,response,'TürkPara','Kisisellestir',time_request)
    return response
    
def get_extre(obj,db_save=True,str_date=False,stp_date=False,sayi=False):
    
    time_request           = datetime.today()
    client,ws_parameters,g = get_client( obj )
    client.set_options(timeout=120)
    
    if db_save:
      lot      = guid_check(obj.lot_id)
      str_date = obj.str_date
      stp_date = obj.stp_date
      sayi     = 300 
    else:
      lot = obj
    try:
      response = client.service.Ekstre(G                 =    g,
                                        GUID             =    lot.guid,
                                        Tarih_Bas        =    get_date_format (str_date),
                                        Tarih_Bit        =    get_date_format (stp_date),
                                        Sayi             =    sayi)
    
    except:
        response = traceback.format_exc()
    
    requestData = dict (CLIENT_CODE     = ws_parameters.ws_cli_code,
                      CLIENT_USERNAME 	= ws_parameters.ws_cli_user,
                      CLIENT_PASSWORD		= ws_parameters.ws_cli_pass,
                      GUID              = lot.guid,
                      Tarih_Bas         = get_date_format (str_date),
                      Tarih_Bit         = get_date_format (stp_date),
                      Sayi              =  sayi)

    set_log(obj,requestData,response,'TürkPara','Extre',time_request)
    l = []
    
    if db_save:
      obj.env['info_kart.extre'].sudo().search( [('get_extre_wizard','=',obj.id)]  ).sudo().unlink()
    if hasattr(response,'Sonuc') and int(response.Sonuc) == 1:
        r = fastest_object_to_dict( response )
        for dataset in r['DT']['diffgram']:
          for d in dataset['NewDataSet']:
              for final_d_s in d['Temp']:
                  new_ds  = {}
                  copy_ds = False
                  for x,y in final_d_s.items():
                      if x == '_rowOrder':
                        final_d_s.update({x:int(y)+1})
                      if type(y) == list:
                         final_d_s.update({x:y[0].replace('Param','Yemekmatik')})
                      
                      if x == 'Durum' and y[0] == 'Silindi':
                         copy_ds = True
                         
                  final_d_s.update({'get_extre_wizard':obj.id})
                  time_f = final_d_s['Tarih'].split('+')[0].replace('T',' ')
                  time_f = time_f.rsplit('.')[0]
                 
                  final_d_s['Tarih'] = get_utc ( time_f )
                  
                  if copy_ds:
                    
                    new_ds =  copy.copy( final_d_s )
                    if new_ds.get('Tip') == u'+':
                       new_ds['Tip'] = u'-'
                    elif new_ds.get('Tip') == u'-':
                       new_ds['Tip'] = u'-'
                       
                    new_ds['Durum']             = 'İptal/İade'
                    final_d_s['Durum']          = 'Aktif***'
                    final_d_s['toplama_dahil']  = False
                    new_ds['toplama_dahil']     = False
                    
                    #print new_ds
                  if db_save:
                    created = obj.env['info_kart.extre'].sudo().create( final_d_s )
                    obj.env['info_kart.extre'].sudo().create( new_ds )
                  else:
                    
                    l.append(final_d_s)
                    if new_ds:
                      l.append( new_ds )
    else:
       #print 'SONUC BASARISIZ'
       r = None
    #print l
    l = {'item':l}
    return l

def tl_transfer(obj,tutar,aciklama,cuzdan,type_='kart'):
    time_request           = datetime.today()
    client,ws_parameters,g = get_client( obj )
    client.set_options(timeout=120)
    
    ym  = obj.env['res.partner'].sudo().browse([1])
    ym  = guid_check(ym,'partner')
    obj = guid_check(obj, type_)
    
    try:
        response = client.service.TL_Transfer(G         = g,
                                          Borclu_GUID   = ym.guid,
                                          Alacakli_GUID = obj.guid,
                                          Tutar         = tutar,
                                          Aciklama      = aciklama,
                                          Cuzdan        = cuzdan)
    except:
        response = traceback.format_exc()
    
    requestData = dict (G            = g,
                                          Borclu_GUID   = ym.guid,
                                          Alacakli_GUID = obj.guid,
                                          Tutar         = tutar,
                                          Aciklama      = aciklama)
    
    set_log(obj,requestData,response,'TürkPara','TL Transfer',time_request)
    
    if not hasattr (response,'YID'):
        return False,False,False
    else:
        if int(response.YID) == 0:
            return False, False,response.Sonuc_Str
        return response.YID, response.HID, response.Sonuc_Str

def tl_transfer_toplu(obj,b64text,islem_tipi):
    
    time_request           = datetime.today()
    client,ws_parameters,g = get_client( obj )
    client.set_options(timeout=120)
    #print client
    #client.options.cache.clear()
    
    try:
        response = client.service.TL_Transfer_Toplu(G            = g,
                                                Dosya_B64    = b64text,
                                                Islem_Tip    = islem_tipi)
    except:
        response = traceback.format_exc()
    
    requestData = dict (G            = g,Dosya_B64="b64txt too long for log.")
    
    set_log(obj,requestData,response,'TürkPara','TL Transfer Toplu',time_request)
    
    if hasattr(response,'Sonuc') and int(response.Sonuc) == 1:
        #print "Yükleme Başarılı"
        obj.never_sended = False
    
        return response.Sonuc, response.Sonuc_Str
    return 0, None

def tl_oranlar_tek(obj):
    
    client,ws_parameters,g = get_client( obj )
    time_request  = datetime.today()
    response      = client.service.TL_Oranlar(G=g)
    final_d       = {}
    if int(response.Sonuc) != 1:
        #print 'SONUC BASARISIZ'
        r = None
    else:
      r = fastest_object_to_dict( response )
      for dataset in r['DT']['diffgram']:
         for d in dataset['NewDataSet']:
             for final_d in d['Temp']:
                
                final_d.update((x, round(float(y[0]),2)) for x, y in final_d.items() if x.startswith('T'))
       
    return final_d

def taksit_info(obj,t,k,g_no,tk):
    
    client,ws_parameters,g = get_client( obj )
    time_request  = datetime.today()
    response      = client.service.Taksit_Info(G=g,
                                               Troy_KN=g_no.replace(' ',''),
                                               KK_No=k.replace(' ',''),
                                               Tutar=t,
                                               Taksit=tk)
    final_d       = {}
    if int(response.Sonuc) != 1:
        #print 'SONUC BASARISIZ'
        r = None
    else:
      r = fastest_object_to_dict( response )
      for dataset in r['DT']['diffgram']:
         for d in dataset['NewDataSet']:
             for final_d in d['Temp']:
                
                final_d.update((x, y[0]) for x, y in final_d.items())
       
      return final_d

def TL_Yukle_KK(obj,Troy_KN,KK_AdSoyad,KK_No,KK_Ay,KK_Yil,KK_CVV,Taksit,Tutar):
    
    client,ws_parameters,g = get_client( obj )
    client.set_options(timeout=60)
    time_request  = datetime.today()
    response      = client.service.TL_Yukle_KK(G          = g,
                                               Troy_KN    = Troy_KN,
                                               KK_AdSoyad = KK_AdSoyad,
                                               KK_No      = KK_No,
                                               KK_Ay      = KK_Ay,
                                               KK_Yil     = KK_Yil,
                                               KK_CVV     = KK_CVV,
                                               Taksit     = Taksit,
                                               Tutar      = Tutar)
    final_d       = {}
    if int(response.Sonuc) != 1:
        #print 'SONUC BASARISIZ'
        r = None
    else:
        r =  response.UCD_URL 
    return r
        
def BIN_SanalPos(obj,bin_number=''):
    
    client,ws_parameters,g = get_client( obj, ws_odeme_servis)
    guid          = pos_guid
    time_request  = datetime.today()
    if ws_odeme_servis.find('test') > 0:
      guid        = test_guid
      
    response      = client.service.BIN_SanalPos(G=g,BIN=bin_number)
    final_d       = {}
    if int(response.Sonuc) != 1:
        #print 'SONUC BASARISIZ'
        r = None
    else:
      r = fastest_object_to_dict( response )
      for dataset in r['DT_Bilgi']['diffgram']:
         for d in dataset['NewDataSet']:
             for final_d in d['Temp']:
                final_d.update((x, y[0]) for x, y in final_d.items())
       
      return final_d

def TP_Ozel_Oran_SK_Liste(obj):
    
    client,ws_parameters,g = get_client( obj, ws_odeme_servis)
    guid          = pos_guid
    time_request = datetime.today()
    if ws_odeme_servis.find('test') > 0:
      guid = test_guid
    response = client.service.TP_Ozel_Oran_SK_Liste(G=g,GUID=guid)

    if int(response.Sonuc) != 1:
        #print 'SONUC BASARISIZ'
        r = None
    else:
      r = fastest_object_to_dict( response )
      for dataset in r['DT_Bilgi']['diffgram']:
         for d in dataset['NewDataSet']:
             for final_d in d['DT_Ozel_Oranlar_SK']:
                final_d.update((x, y[0]) for x, y in final_d.items())
                
                obj.env['info_bayi.tp_ozel_oran_sk_liste'].sudo().create( final_d )
    requestData = dict (G=g,GUID=guid)
    
    set_log(obj,requestData,response,'TürkPara Sanal Pos','TP_Ozel_Oran_SK_Liste',time_request)

def TP_Islem_Odeme(obj):
    
    payload = ast.literal_eval(obj.payload)
    client,ws_parameters,g = get_client( obj, ws_odeme_servis)
    guid          = pos_guid
    time_request = datetime.today()
    if ws_odeme_servis.find('test') > 0:
      guid = test_guid
    
    hashstr = str(g.CLIENT_CODE) + guid + str(payload.get('SanalPOS_ID')) + str(payload.get('Taksit')) + payload.get('Islem_Tutar') + payload.get('Toplam_Tutar') + str(payload.get('Siparis_ID')) + payload.get('Hata_URL') + payload.get('Basarili_URL')  
    b64     = client.service.SHA2B64(Data=hashstr )
    
    payload['Islem_Hash']     = b64
    payload['GUID']           = guid
    payload['G']              = g
    payload['KK_Sahibi_GSM']  = ""
    payload['Islem_ID']       = ""
    payload['IPAdr']          = "94.103.47.27"
    payload['Ref_URL']        = ""
    payload['Data1']          = ""
    payload['Data2']          = ""
    payload['Data3']          = ""
    payload['Data4']          = ""
    payload['Data5']          = ""
    
    response = client.service.TP_Islem_Odeme(**payload)
   
    if int(response.Sonuc) != 1:
        #print 'SONUC BASARISIZ'
        r = None
    else:
        r           = response.UCD_URL
        obj.payload = response.Islem_ID
    return r
    #requestData = dict (G=g,GUID=guid)
    
    #set_log(obj,requestData,response,'TürkPara Sanal Pos','TP_Ozel_Oran_SK_Liste',time_request)

def Kayip_Calinti(obj,new_obj):
    time_request           = datetime.today()
    client,ws_parameters,g = get_client( obj )
    client.set_options(timeout=120)
    guid_check( obj )
    guid_check( new_obj )
    try:
      response      = client.service.Kayip_Calinti(G=g,
                                                 Kayip_Calinti_GUID = obj.guid,
                                                 Yeni_GUID = new_obj.guid)
    except:
      response = traceback.format_exc()
    
    requestData = dict (G=g,
                        Kayip_Calinti_GUID = obj.guid,
                        Yeni_GUID = new_obj.guid)
    
    set_log(obj,requestData,response,'TürkPara','Kayıp Çalıntı',time_request)
    
    if hasattr(response,'Sonuc'):
      return response.Sonuc, response.Sonuc_Str
    else:
      return False,False
    
def RaporByDekont(obj,str_date=False,stp_date=False,tip=False,Dekont_ID=False):
    time_request           = datetime.today()
    client,ws_parameters,g = get_client( obj )
    client.set_options(timeout=30)
    
    ex = False  
    try:
        response = client.service.Rapor(G               = g,
                                    Rapor_Tip           = tip,
                                    Tarih_Bas           = get_date_format (str_date),
                                    Tarih_Bit           = get_date_format (stp_date),
                                    Uye_Isyeri_Kart_No  = "",
                                    POS_Terminal_No     = "",
                                    POS_Isyeri_No       = "",
                                    Dekont_ID           = Dekont_ID)
                                      #Uye_Isyeri_Kart_No  = "")
  
    except:
      response = traceback.format_exc()
      ex = True
    
    requestData = dict (CLIENT_CODE         = ws_parameters.ws_cli_code,
                        CLIENT_USERNAME 	  = ws_parameters.ws_cli_user,
                        CLIENT_PASSWORD		  = ws_parameters.ws_cli_pass,
                        Rapor_Tip           = tip,
                        Tarih_Bas           = get_date_format (str_date),
                        Tarih_Bit           = get_date_format (stp_date),
                        Uye_Isyeri_Kart_No  = "",
                        POS_Terminal_No     = "",
                        POS_Isyeri_No       = "",
                        Dekont_ID           = Dekont_ID)

    set_log(obj,requestData,response,'TürkPara','Rapor',time_request)    
    ids = []
    if hasattr(response,'Sonuc') and int(response.Sonuc) == 1:
        r = fastest_object_to_dict( response )
        for dataset in r['DT']['diffgram']:
          for d in dataset['NewDataSet']:
              for final_d_s in d['Temp']:
                  for x,y in final_d_s.items():
                      if x == '_rowOrder':
                        final_d_s.update({x:int(y)+1})
                      if type(y) == list:
                         final_d_s.update({x:y[0].replace('Param','Yemekmatik')})
                      #if x == "Pan":
                         #pan_masked = "**** **** **** %s"%(y[0][len(y[0]) - 4:])
                         #final_d_s.update({"Pan_masked":pan_masked})
                  
                  time_f = final_d_s['Tarih'].split('+')[0].replace('T',' ')
                  time_f = time_f.rsplit('.')[0]
                  final_d_s['Tarih'] = get_utc ( time_f )
                  #final_d_s['Tarih'] = time_f
                 
                  created = obj.env['info_bayi.rapor'].sudo().create( final_d_s )
                  ids.append( created.id )
    return ids
    
def RaporBundles(obj,bundles):
    time_request           = datetime.today()
    client,ws_parameters,g = get_client( obj )
    client.set_options(timeout=30)
    Uye_Isyeri_Kart_No = ""
    str_date  = obj.str_date
    stp_date  = datetime.strftime (obj.stp_date + timedelta(days = 1),'%Y-%m-%d')
    tip       = obj.rapor_tipi
    
    ex = False
    obj.env['info_bayi.rapor'].sudo().search( [('get_rapor_wizard','=',obj.id)]  ).sudo().unlink()
    
    partner_ids = list(map(lambda x:x.id,obj.partner_id.sudo().child_ids ))
    partner_ids.append( obj.partner_id.sudo().id )
    pos_objt            = obj.env['info_bayi.turkpara_banka_rel'].sudo().search([('partner_id','in',partner_ids),('state','=','confirmed')])
    pos_ids             = list(map(lambda x:['0'* (8 - len(str(x.terminal_no))) + str(x.terminal_no),x.partner_id.id,str(x.uye_isyeri_no).lstrip("0")], pos_objt))
    
    for b in bundles:
      try:
        POS_Isyeri_No   = b[0]
        POS_Terminal_No = b[1]
      
        if POS_Terminal_No and len(POS_Terminal_No) < 8:
            POS_Terminal_No = '0'*(8-len(POS_Terminal_No)) + POS_Terminal_No
        
        response = client.service.Rapor(G                   = g,
                                      Rapor_Tip           = tip,
                                      Tarih_Bas           = get_date_format (str_date),
                                      Tarih_Bit           = get_date_format (stp_date),
                                      Uye_Isyeri_Kart_No  = Uye_Isyeri_Kart_No,
                                      POS_Terminal_No     = POS_Terminal_No or '',
                                      POS_Isyeri_No       = POS_Isyeri_No or '',
                                      Dekont_ID           = '')
                                        #Uye_Isyeri_Kart_No  = "")
    
      except:
        response = traceback.format_exc()
        ex = True
    
      requestData = dict (CLIENT_CODE         = ws_parameters.ws_cli_code,
                        CLIENT_USERNAME 	  = ws_parameters.ws_cli_user,
                        CLIENT_PASSWORD		  = ws_parameters.ws_cli_pass,
                        G                   = g,
                        Rapor_Tip           = tip,
                        Tarih_Bas           = get_date_format (str_date),
                        Tarih_Bit           = get_date_format (stp_date),
                        Uye_Isyeri_Kart_No  = Uye_Isyeri_Kart_No,
                        POS_Terminal_No     = POS_Terminal_No or '',
                        POS_Isyeri_No       = POS_Isyeri_No or '',
                        Dekont_ID           =  '')
    
      set_log(obj,requestData,response,'TürkPara','Rapor',time_request)

      if hasattr(response,'Sonuc') and int(response.Sonuc) == 1:
        r = fastest_object_to_dict( response )
        dekonts = []
        for dataset in r['DT']['diffgram']:
          for d in dataset['NewDataSet']:
              for final_d_s in d['Temp']:
                  db_kontrol = True
                  if int( final_d_s["Dekont_ID"][0] ) not in dekonts:
                    dekonts.append( int(final_d_s["Dekont_ID"][0]))
                  else:
                    #mukerrer
                    #print "mukerrer" , final_d_s["Dekont_ID"]
                    db_kontrol = False
                  if final_d_s.get('Terminal_No') and final_d_s.get('Isyeri_No'):
                    if str(final_d_s.get('Terminal_No')[0]) in [ a[0] for a in pos_ids] and str(final_d_s.get('Isyeri_No')[0]).lstrip("0") in [ a[2] for a in pos_ids]:
                        final_d_s['kapanacak'] = False
                        final_d_s.update({'Sube_iliskili':[a[1] for a in pos_ids if str(final_d_s.get('Terminal_No')[0]) == a[0] and str(final_d_s.get('Isyeri_No')[0]).lstrip("0") == a[2]][0]})
                    else:
                        final_d_s['kapanacak'] = True
                        
                  for x,y in final_d_s.items():
                      if x == '_rowOrder':
                        final_d_s.update({x:int(y)+1})
                      if type(y) == list:
                         final_d_s.update({x:y[0].replace('Param','Yemekmatik')})
                      # if x == "Pan":
                      #    pan_masked = "**** **** **** %s"%(y[0][len(y[0]) - 4:])
                      #    final_d_s.update({"Pan_masked":pan_masked})

                  final_d_s.update({'get_rapor_wizard':obj.id})
                  time_f = final_d_s['Tarih'].split('+')[0].replace('T',' ')
                  time_f = time_f.rsplit('.')[0]
                  final_d_s['Tarih'] = get_utc ( time_f )
                  #final_d_s['Tarih'] = time_f
                  if db_kontrol:
                    created = obj.env['info_bayi.rapor'].sudo().create( final_d_s )

def Rapor(obj,get_by_all=False,db_save=True,str_date=False,
          stp_date=False,tip=False,
          POS_Terminal_No=False,POS_Isyeri_No=False,
          Dekont_ID=False,pan_ids=[]):
    time_request           = datetime.today()
    client,ws_parameters,g = get_client( obj )
    client.set_options(timeout=120)
    Uye_Isyeri_Kart_No = ""
    if db_save:
      str_date  = obj.str_date
      stp_date  = datetime.strftime (obj.stp_date + timedelta(days = 1),'%Y-%m-%d')
      tip       = obj.rapor_tipi
      POS_Terminal_No = obj.terminal_no
      POS_Isyeri_No   = obj.uye_is_yeri_no

    if get_by_all:
      Uye_Isyeri_Kart_No  = obj.partner_id.sudo().turkpara_e_para_hesap or obj.partner_id.sudo().parent_id.turkpara_e_para_hesap or '',
    partner_ids = list(map(lambda x:x.id,obj.partner_id.sudo().child_ids ))
    partner_ids.append( obj.partner_id.sudo().id )
    pos_objt            = obj.env['info_bayi.turkpara_banka_rel'].sudo().search([('partner_id','in',partner_ids),('state','=','confirmed')])
    pos_ids             = list(map(lambda x:['0'* (8 - len(str(x.terminal_no))) + str(x.terminal_no),x.partner_id.id,str(x.uye_isyeri_no).lstrip("0")], pos_objt))
    ex = False  
    try:
      if POS_Terminal_No and len(POS_Terminal_No) < 8:
          POS_Terminal_No = '0'*(8-len(POS_Terminal_No)) + POS_Terminal_No
      response = client.service.Rapor(G                   = g,
                                    Rapor_Tip           = tip,
                                    Tarih_Bas           = get_date_format (str_date),
                                    Tarih_Bit           = get_date_format (stp_date),
                                    Uye_Isyeri_Kart_No  = Uye_Isyeri_Kart_No,
                                    POS_Terminal_No     = POS_Terminal_No or '',
                                    POS_Isyeri_No       = POS_Isyeri_No or '',
                                    Dekont_ID           = Dekont_ID or '')
                                      #Uye_Isyeri_Kart_No  = "")
  
    except:
      response = traceback.format_exc()
      ex = True
    
    requestData = dict (CLIENT_CODE         = ws_parameters.ws_cli_code,
                        CLIENT_USERNAME 	  = ws_parameters.ws_cli_user,
                        CLIENT_PASSWORD		  = ws_parameters.ws_cli_pass,
                        G                   = g,
                        Rapor_Tip           = tip,
                        Tarih_Bas           = get_date_format (str_date),
                        Tarih_Bit           = get_date_format (stp_date),
                        Uye_Isyeri_Kart_No  = Uye_Isyeri_Kart_No,
                        POS_Terminal_No     = POS_Terminal_No or '',
                        POS_Isyeri_No       = POS_Isyeri_No or '',
                        Dekont_ID           = Dekont_ID or '')

    set_log(obj,requestData,response,'TürkPara','Rapor',time_request)
    l = []
    
    if db_save:
      obj.env['info_bayi.rapor'].sudo().search( [('get_rapor_wizard','=',obj.id)]  ).sudo().unlink()
    
    l = {'item':l}
    if hasattr(response,'Sonuc') and int(response.Sonuc) == 1:
        r = fastest_object_to_dict( response )
        dekonts = []
        
        for dataset in r['DT']['diffgram']:
          for d in dataset['NewDataSet']:
              dict_list     = []
              groupped_dict = {}
              for final_d_s in d['Temp']:
                  db_kontrol   = True
                  dict_kontrol = True
                  grupla       = False
                  if int( final_d_s["Dekont_ID"][0] ) not in dekonts:
                    dekonts.append( int(final_d_s["Dekont_ID"][0]))
                  else:
                    #mukerrer
                    #print "mukerrer" , final_d_s["Dekont_ID"]
                    db_kontrol = False
                  if final_d_s.get('Terminal_No') and final_d_s.get('Isyeri_No'):
                    if str(final_d_s.get('Terminal_No')[0]) in [ a[0] for a in pos_ids] and str(final_d_s.get('Isyeri_No')[0]).lstrip("0") in [ a[2] for a in pos_ids]:
                        final_d_s['kapanacak'] = False
                        final_d_s.update({'Sube_iliskili':[a[1] for a in pos_ids if str(final_d_s.get('Terminal_No')[0]) == a[0] and str(final_d_s.get('Isyeri_No')[0]).lstrip("0") == a[2]][0]})
                    else:
                        final_d_s['kapanacak'] = True
                        final_d_s['Sube_iliskili'] = False
                        
                  for x,y in final_d_s.items():
                      if x == '_rowOrder':
                        final_d_s.update({x:int(y)+1})
                      if type(y) == list:
                         final_d_s.update({x:y[0].replace('Param','Yemekmatik')})
                      if x == "Pan":
                         # pan_masked = "**** **** **** %s"%(y[0][len(y[0]) - 4:])
                         # final_d_s.update({"Pan_masked":pan_masked})
                         if pan_ids and str(y[0]) not in pan_ids:
                            dict_kontrol = False
  
                  final_d_s.update({'get_rapor_wizard':obj.id})
                  time_f = final_d_s['Tarih'].split('+')[0].replace('T',' ')
                  time_f = time_f.rsplit('.')[0]
                  final_d_s['Tarih'] = get_utc ( time_f )
                  #final_d_s['Tarih'] = time_f
                  if db_save and db_kontrol:
                    #obj.etxre_lines = [(0,0,final_d_s)]
                    created = obj.env['info_bayi.rapor'].sudo().create( final_d_s )
                  elif dict_kontrol:
                    if pan_ids:
                      if groupped_dict.get( final_d_s.get('Sube')):
                        groupped_dict[ final_d_s.get('Sube') ].append( final_d_s )
                      else:
                        groupped_dict[ final_d_s.get('Sube') ] = [final_d_s]
                    else:   
                      dict_list.append( final_d_s )
              
              if pan_ids:
                dict_list.append( groupped_dict )
                
              l.update({'item':dict_list})
        return True,l
    else:
       #print 'SONUC BASARISIZ'
       r = None
       if ex:
          response = dummy()
          response.Sonuc = -1
          response.Sonuc_Str = 'Sunucu Sonuç Döndürmedi.'
          
       return False,response


def Kd_Harcama_Islem(obj): #Kapalı devre harcama işlemi

    time_request = datetime.today()
    client, ws_parameters, g = get_client(obj, service_name='service_kapalidevre') #info bayi Türk Para Web Servis Özellikleri içinde tanımlı olması gereken parametreler
    client.set_options(timeout=30)
    #obj = guid_check(obj.lot_id)

    try:
        response = client.service.Islem_Harcama(G=g,
                                                 Kart_Data  =   obj.lot_id.seri_no_computed.replace(' ',''),
                                                 Aciklama   =   obj.aciklama,
                                                 # Cuzdan   =   obj.cuzdan_kodu,
                                                 Islem_ID   =   obj.islem_id,
                                                 Tutar      =   obj.tutar
                                                 )
    except:
        response = traceback.format_exc()

    requestData = dict(CLIENT_CODE              =   ws_parameters.ws_cli_code,
                        CLIENT_USERNAME         =   ws_parameters.ws_cli_user,
                        CLIENT_PASSWORD		    =   ws_parameters.ws_cli_pass,
                        Kart_Data               =   obj.lot_id.seri_no_computed.replace(' ',''),
                        Aciklama                =   obj.aciklama,
                        # Cuzdan                =   obj.cuzdan_kodu,
                        Islem_ID                =   obj.islem_id,
                        Tutar                   =   obj.tutar)

    set_log(obj ,requestData ,response ,'TürkParaKapalıDevre' ,'Islem_Harcama' ,time_request)

    if not hasattr(response ,'Sonuc'):
        raise exceptions.ValidationError(' %s için Harcama Başarısız. ' %obj.name)

    return response

def Kd_Harcama_Iptal(obj): #Kapalı devre harcama işlemi iptal

    time_request = datetime.today()
    client, ws_parameters, g = get_client(obj, service_name='service_kapalidevre') #info bayi Türk Para Web Servis Özellikleri içinde tanımlı olması gereken parametreler
    client.set_options(timeout=30)
    #obj = guid_check(obj, "kart")

    try:
        response = client.service.Islem_Iptal(G=g,Islem_ID  =   obj.islem_id,Dekont_ID  =   obj.dekont_id)
    except:
        response = traceback.format_exc()

    requestData = dict(CLIENT_CODE              =   ws_parameters.ws_cli_code,
                        CLIENT_USERNAME         =   ws_parameters.ws_cli_user,
                        CLIENT_PASSWORD		    =   ws_parameters.ws_cli_pass,
                        Islem_ID                =   obj.islem_id,
                        Dekont_ID               =   obj.dekont_id)

    set_log(obj ,requestData ,response ,'TürkParaKapalıDevre' ,'Islem_Harcama_Iptal' ,time_request)

    if not hasattr(response ,'Sonuc'):
        raise exceptions.ValidationError(' %s için Harcama Iptal Başarısız. ' %obj.name)

    return response

def getPartnerFromOkc(termSeriNo,env):
    client = sclient('https://b2b.infoteks.com.tr/okcbanka', cache=NoCache(),plugins=[LogPlugin()])
    response = client.service.cihazSorgulama( {'kurumKodu' :'GRTBNK',
                                    'kurumToken':'5C75FFF49A4C4B94B7C1C0A017915F',
                                    'okcSeriNo':termSeriNo})

    if response.cevapKodu == '000':
        partner_id = env['res.partner'].sudo().search([('taxNumber','=',response.isyeriVergiNo)])
        if partner_id:
            db_name = env['info_restaurant_base.demo'].sudo().search([('partner_id','=',partner_id[-1].id)])
            return db_name
        else:
            return False
        return False


class WSOranlar(models.Model):
  _name = 'info_bayi.tp_ozel_oran_sk_liste'
  Kredi_Karti_Banka_Gorsel = fields.Char()
  MO_01                    = fields.Float()
  MO_02                    = fields.Float()
  MO_03                    = fields.Float()
  MO_04                    = fields.Float()
  MO_05                    = fields.Float()
  MO_06                    = fields.Float()
  MO_07                    = fields.Float()
  MO_08                    = fields.Float()
  MO_09                    = fields.Float()
  MO_10                    = fields.Float()
  MO_11                    = fields.Float()
  MO_12                    = fields.Float()
  
  Ozel_Oran_SK_ID          = fields.Integer()
  SanalPOS_ID              = fields.Integer(index=True)
  Kredi_Karti_Banka        = fields.Char()
  
class WSFirma(models.Model):
    _name = 'info_bayi.harici_firmalar'
    
    name = fields.Char('Firma Adı')
    kurum_kodu = fields.Char('Kurum Kodu')
    kurum_token = fields.Char('Kurum Token')
    
class turkparaopts(models.Model):
    _name = 'info_bayi.turkpara_ws'
    
    ws_adres     = fields.Char(string='Web Servis Adresi')
    ws_cli_code  = fields.Char(string='Client Code')
    ws_cli_user  = fields.Char(string='Client User')
    ws_cli_pass  = fields.Char(string='Client Pass',password="True")

class turkparawslogs (models.Model):
    
    _name = 'info_bayi.turkpara_ws_logs'
    
    serviceName         = fields.Char(string='serviceName')
    operationName       = fields.Char(string='operationName')
    requestTime         = fields.Datetime(string='requestTime')
    responseTime        = fields.Datetime(string='responseTime')
    requestData         = fields.Char(string='requestData')
    responseData        = fields.Char(string='responseData')
    write_date          = fields.Datetime(string="WriteDate", required=False, )
    baslangic_tarihi    = fields.Date( string ='Başlangıç Tarihi', compute = '_tarih_araligi')
    bitis_tarihi        = fields.Date( string ='Bitiş Tarihi', compute = '_tarih_araligi')
    proc_id             = fields.Integer()

    @api.multi
    def _tarih_araligi( self ):
         return lambda *a,**k:{}
    _order = 'id desc'

