# -*- coding: utf-8 -*-
from . import models
from . import bayi_profil
from . import res_partner
from . import util_models
from . import res_company
from . import mail_mail
from . import wizards
from . import account_invoice
from . import call_center
from . import cc_payment
from . import product
from . import account_payment
from . import res_users
from . import webserviceopt
from . import turkpara_isemirleri
from . import ek_modeller
from . import res_users
from . import crm