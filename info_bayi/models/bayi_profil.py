# -*- coding: utf-8 -*-
from openerp import SUPERUSER_ID
from openerp import models, fields, api
bayi_tips      = [(1,'Bayi'),(2,'Bayi Teknik Servis'),(3,'Bayi Teknik Servis Plus')]
fiyat_uyg_tips = [(1,'Yüzde İndirim'),(2,'Değer İndirim'),(3,'Yüzde Artırım'),(4,'Değer Artırım')]

class bayi_ceza_tipleri ( models.Model ):

    _name = 'info_bayi.p_ceza'

    name = fields.Char(string='Bayi Ceza Tipi', required=True)

class bayi_sabit_ucretler (models.Model ):

    _name = 'info_bayi.p_sabit_ucretler'

    name  = fields.Char( string='Sabit Ücret Adı')
    deger = fields.Float( string='Sabit Ücret Değeri')

class bayi_product_rel (models.Model):

    _name = 'info_bayi.product_profil_rel'

    product      = fields.Many2one('product.product', string='Ürün',required=True)
    alis_fiyati  = fields.Float('Hesaplanan Ürün Alış Fiyatı')
    satis_fiyati = fields.Float('Hesaplanan Ürün Satış Fiyatı')
    profil       = fields.Many2one('info_bayi.bayi_profili')

    standard_price = fields.Float(related='product.standard_price')
    list_price     = fields.Float(related='product.list_price')

    def _get_calculate_price( self, base_fiyat, deger, islem_tipi ):

        ###print 'Gelen Base : ' , base_fiyat

        if islem_tipi == 1:
            base_fiyat = base_fiyat - (base_fiyat * (deger / 100) )
        elif islem_tipi == 2:
            base_fiyat = base_fiyat - deger
        elif islem_tipi == 3:
            base_fiyat = base_fiyat + (base_fiyat * (deger / 100) )
        elif islem_tipi == 4:
            base_fiyat = base_fiyat + deger

        return base_fiyat

class bayi_profili ( models.Model):

    _name = 'info_bayi.bayi_profili'

    name            = fields.Char('Profil Adı',help="Örn. Marmara Bölge Bayi",required=True)
    profil_tipi     = fields.Selection(selection = bayi_tips,string='Profil Tipi',required=True)
    city            = fields.Many2many('info_extensions.iller')
    province        = fields.Many2many('info_extensions.ilceler')
    satis_fiyat_uygulama = fields.Selection(string='Satış Fiyat Değişikliği Uygulama Tipi',
                                            selection=fiyat_uyg_tips,
                                            help='Satış Fiyatı Üzerinden Uygulanacak Düzenleme Tipi',
                                            default=1)

    alis_fiyat_uygulama  = fields.Selection(string='Alış Fiyat Değişikliği Uygulama Tipi',
                                            selection=fiyat_uyg_tips,
                                            help='Alış Fiyatı Üzerinden Uygulanacak Düzenleme Tipi',
                                            default=1)

    alis_deger                = fields.Float(string ='Alış Fiyat Değişiklik Değeri')
    satis_deger               = fields.Float(string ='Satış Fiyat Değişiklik Değeri')
    ceza_profili         = fields.Many2one('info_bayi.p_ceza')
    vade_tanimi          = fields.Many2one('account.payment.term')

    sabit_ucretler       = fields.Many2many('info_bayi.p_sabit_ucretler',string ='Sabit Ücretler')
    products_rel         = fields.One2many('info_bayi.product_profil_rel','profil',   string='Ürünler')
    hedef                = fields.Integer('Aylık Satış Hedefi', required=True)
    

    @api.multi
    def fiyat_hesapla(self):
        for s in self:
            for p in s.products_rel:

                ###print 'Alis Hesapla :  ',s.product.standard_price, s.profil.alis_deger, s.profil.alis_fiyat_uygulama
                base_fiyat = p.product.standard_price
                if s.alis_fiyat_uygulama:
                    base_fiyat = p._get_calculate_price (p.product.standard_price, s.alis_deger, s.alis_fiyat_uygulama)
                    ###print 'Alis donen,:',base_fiyat
                p.alis_fiyati = base_fiyat

                base_fiyat_satis = p.product.list_price
                if s.satis_fiyat_uygulama:
                    base_fiyat_satis = p._get_calculate_price (p.product.list_price, s.satis_deger,s.satis_fiyat_uygulama)
                    ###print 'Satis donen,:',base_fiyat_satis
                p.satis_fiyati = base_fiyat_satis

    @api.model
    def create(self,values ):
        res = super(bayi_profili, self).create( values )
        if 'products_rel' in values:
            rel = values['products_rel']
            for r in rel:
                self.env['info_bayi.product_profil_rel'].browse( r[1] ).profil = res.id

        return res


    @api.model
    def default_get(self, fields_list):
        res     = models.Model.default_get(self, fields_list)

        products         = self.env['product.product'].search([('company_id','=',False)])
        relation_product = self.env['info_bayi.product_profil_rel']
        dws = []

        for p in products:
            dw = relation_product.create({
                'product': p.id
            })

            dws.append(dw.id)
        ###print dws
        res['products_rel'] = tuple(dws)
        ###print res
        return res
