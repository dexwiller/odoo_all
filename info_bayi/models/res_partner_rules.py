# -*- coding: utf-8 -*-
from openerp import models, fields,api
from openerp.exceptions import ValidationError
from openerp.tools.translate import _
from datetime import datetime as dt
from datetime import timedelta as td

class res_partner (models.Model):	
	_inherit = 'res.partner'
	
	@api.constrains('name')
	def _check_name(self):

		if self.name and len(self.name.strip()) ==0:
			raise ValidationError(u"Şirket Adı Boştan Faklı Olmalıdır.")

	@api.constrains('tckn')
	def _check_tckn(self):
		if self.country_id.code == 'TR':
			#print '**************'
			#print self.id
			#print self.tckn
			if self.is_company and self.tckn:
	
				if self.sahis_sirketi:
					if not self.tckn:
						raise ValidationError(u" Şahıs Şirketi için TC Kimlik No Girilmesi Zorunludur.")
	
				if self.tckn and (len(self.tckn.strip()) != 11 or not self.tckn.isdigit()):
					raise ValidationError(u"TC Kimlik No 11 Haneli, Boştan Farklı ve Nümerik Olmalıdır.")

	@api.one
	@api.constrains('taxAgent', 'taxNumber')#server side kontrol
	def _check_tax_mersis_sermaye(self):
		###print self.company_id
		if self.country_id.code == 'TR':
			if not self.company_id and self.company_id.id:
				if self.is_company:
					###print self.sermaye_sirketi, not self.parent_id
					if self.sermaye_sirketi and not self.parent_id and self.tckn:
						###print self.taxNumber, self.taxAgent
						if  not self.taxAgent or not self.taxNumber:
							raise ValidationError(u"Sermaye Şirketi için Vergi Dairesi,Vergi No  Girilmesi Zorunludur.")
						else:
							if len(self.taxAgent.name.strip()) == 0 or len(self.taxNumber.strip()) != 10:
								raise ValidationError(u"Sermaye Şirketi için Vergi Dairesi Boştan Farklı,Vergi No Boştan Farklı ve 10 Hane Olmalıdır.")
							if  not self.taxNumber.isdigit():
								raise ValidationError(u"Sermaye Sirketi icin Vergi Dairesi,Vergi No Numerik Olmalidir.".decode('utf-8'))

	@api.one
	@api.constrains('taxNumber')
	def _check_unique_insesitive_taxNumber(self):
		#print self.child_ids
		if self.country_id.code == 'TR':
			
			lst = [x.taxNumber.lower() for x in self.search([]) if x.taxNumber and x.id != self.id]
			#print self.id in lst
			if self.parent_id and self.parent_id.taxNumber:
				lst = list(filter(lambda a: a != self.parent_id.taxNumber.lower(), lst))
	
			if self.taxNumber and not self.child_ids and self.taxNumber.lower() in  lst:
				#print self.taxNumber
				raise ValidationError(u"Vergi No Zaten Eklenmiş!!! Şube tanımı yapmak istiyorsanız, ana firma cari kartı içerisinden \"Şube Ekle\" fonksiyonunu kullanınız.")

	
	@api.one
	@api.constrains('tckn')
	def _check_unique_insesitive_tckn(self):
		if self.country_id.code == 'TR':
			if self.tckn:
				lst = [x.tckn.lower() for x in self.search([]) if x.tckn and x.id != self.id]
				if self.parent_id and self.parent_id.tckn:
					lst = list(filter(lambda a: a != self.parent_id.tckn.lower(), lst))
	
				if not self.child_ids and self.tckn.lower() in  lst:
						raise ValidationError(u"Tc Kimlik No Zaten Eklenmiş!!! Şube tanımı yapmak istiyorsanız, ana firma cari kartı içerisinden \"Şube Ekle\" fonksiyonunu kullanınız.")