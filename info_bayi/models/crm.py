# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions


class crm_team(models.Model):
    _inherit = 'crm.team'

    def get_user_id_unique_dom(self):
        ids = []
        ids.extend(list(map(lambda x: x.user_id.id, self.search([]))))
        final_ids = self.env['res.users'].search([('id', 'not in', ids)]).ids

        try:
            id = self.id
        except:
            id = self[-1].id

        return [('share', '=', False), ('id', 'in', final_ids), '|', ('sale_team_id', '=', False),
                ('sale_team_id', '=', id)]

    saha_ekibi = fields.Boolean()
    ekip_country = fields.Many2one('res.country', string='Ülke',
                                   default=lambda self: self.env['res.country'].search([('id', '=', '224')]))
    ekip_city = fields.Many2one('info_extensions.iller', string='Şehir')
    ekip_ilce = fields.Many2one('info_extensions.ilceler', string='İlçe')
    user_id = fields.Many2one('res.users', string='Team Leader', domain=get_user_id_unique_dom)
    member_ids = fields.One2many('res.users', 'sale_team_id', string='Team Members', domain=get_user_id_unique_dom)

    @api.onchange('ekip_country')
    def on_change_ekip_country(self):
        for s in self:
            if s.ekip_city:
                s.ekip_city = False
            if s.ekip_ilce:
                s.ekip_ilce = False

    @api.onchange('ekip_city')
    def on_change_ekip_city(self):
        for s in self:
            if s.ekip_ilce:
                s.ekip_ilce = False

    @api.model
    def create(self, values):
        res = super(crm_team, self).create(values)
        res.user_id.ekip_uye_id = list(map(lambda x: (4, x.id), res.member_ids))
        res.user_id.sale_team_id = res.id
        res.sudo().company_id = res.user_id.company_id.id
        return res

    @api.multi
    def write(self, values):
        res = super(crm_team, self).write(values)
        if self.user_id and not self.user_id:
            self.user_id.ekip_uye_id = [(5,)]
            self.user_id.ekip_uye_id = list(map(lambda x: (4, x.id), self.member_ids))
            self.user_id.sale_team_id = self.id
        return res
