# -*- coding: utf-8 -*-
from openerp import models,api,SUPERUSER_ID,fields

class crm_sorun_cozumlari( models.Model):

    _name = 'crm.sorun_cozumleri'
    sorun_kategorisi     = fields.Many2one('info_bayi.sorun_kategorileri',string='Sorun Kategorisi',required=True)
    sorun_tipi           = fields.Many2one('info_bayi.sorun_alt_kategorileri',string='Sorun Tipi',domain="[('sorun_kaynagi','=',sorun_kategorisi)]",required=True)
    name                 = fields.Char(string='Çözüm',required=True)

    @api.model
    def create(self, values):
        if 'sorun_tipi' in values:
            sorun_tip_obj = self.env['info_bayi.sorun_alt_kategorileri'].browse( values['sorun_tipi'] )
            values['sorun_kategorisi'] = sorun_tip_obj[0].sorun_kaynagi.id

        return super( crm_sorun_cozumlari, self).create( values)
'''

class crm_phonecall( models.Model):
    _inherit                      = 'crm.phonecall'

    lot_id   = fields.Many2many('stock.production.lot', string='Cihaz Seri Nolar')
    firma_id = fields.Many2one('res.partner', domain="[('customer','=',True)]")
'''