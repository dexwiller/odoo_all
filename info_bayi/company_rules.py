# -*- coding: utf-8 -*-
from openerp import models, fields, api, exceptions, SUPERUSER_ID
class okc_urun_grup_inherit(models.Model):

    _inherit ='info_tsm.okc_urun_grup'
    @api.model
    def _get_default_company_id( self ):
        return self.env.user.company_id
    company_id      = fields.Many2one('res.company',default = _get_default_company_id)
class okc_parametre_grup_inherit(models.Model):

    _inherit ='info_tsm.parametre_grup'
    @api.model
    def _get_default_company_id( self ):
        return self.env.user.company_id
    company_id      = fields.Many2one('res.company',default = _get_default_company_id)

class tsm_ozel_kdv_tablosu_inherit( models.Model ):
    _inherit = 'info_tsm.ozel_kdv_tablosu'
    @api.model
    def _get_default_company_id( self ):
        return self.env.user.company_id
    company_id      = fields.Many2one('res.company',default = _get_default_company_id)

class tsm_ozel_kdv_group_rel_inherit( models.Model ):
    _inherit = 'info_tsm.ozel_kdv_group_rel'
    @api.model
    def _get_default_company_id( self ):
        return self.env.user.company_id
    company_id      = fields.Many2one('res.company',default = _get_default_company_id)

class tsm_ozel_urun_grup_group_rel_inherit( models.Model ):
    _inherit = 'info_tsm.ozel_urun_grup_group_rel'
    @api.model
    def _get_default_company_id( self ):
        return self.env.user.company_id
    company_id      = fields.Many2one('res.company',default = _get_default_company_id)

class tsm_okc_ozel_urun_grubu_tablosu_inherit( models.Model):
    _inherit = 'info_tsm.okc_ozel_urun_grubu_tablosu'
    @api.model
    def _get_default_company_id( self ):
        return self.env.user.company_id
    company_id      = fields.Many2one('res.company',default = _get_default_company_id)

class tsm_urun_grup_group_rel_inherit( models.Model):
    _inherit = 'info_tsm.urun_grup_group_rel'
    @api.model
    def _get_default_company_id( self ):
        return self.env.user.company_id
    company_id      = fields.Many2one('res.company',default = _get_default_company_id)

class tsm_urun_listesi_inherit( models.Model):
    _inherit = 'info_tsm.urun_listesi'
    @api.model
    def _get_default_company_id( self ):
        return self.env.user.company_id
    company_id      = fields.Many2one('res.company',default = _get_default_company_id)

class tsm_ozel_kur_group_rel_inherit( models.Model):
    _inherit = 'info_tsm.ozel_kur_group_rel'
    @api.model
    def _get_default_company_id( self ):
        return self.env.user.company_id
    company_id      = fields.Many2one('res.company',default = _get_default_company_id)

class tsm_ozel_altin_group_rel_inherit( models.Model):
    _inherit = 'info_tsm.ozel_altin_group_rel'
    @api.model
    def _get_default_company_id( self ):
        return self.env.user.company_id
    company_id      = fields.Many2one('res.company',default = _get_default_company_id)

class tsm_tahsilat_tipi_group_rel_inherit( models.Model):
    _inherit = 'info_tsm.tahsilat_tipi_group_rel'
    @api.model
    def _get_default_company_id( self ):
        return self.env.user.company_id
    company_id      = fields.Many2one('res.company',default = _get_default_company_id)

class tsm_satis_tipi_group_rel_inherit( models.Model):
    _inherit = 'info_tsm.satis_tipi_group_rel'
    @api.model
    def _get_default_company_id( self ):
        return self.env.user.company_id
    company_id      = fields.Many2one('res.company',default = _get_default_company_id)

class tsm_fatura_tahsilat_kurum_group_rel_inherit( models.Model):
    _inherit = 'info_tsm.fatura_tahsilat_kurum_group_rel'
    @api.model
    def _get_default_company_id( self ):
        return self.env.user.company_id
    company_id      = fields.Many2one('res.company',default = _get_default_company_id)

class tsm_fatura_tahsilat_kurum_tipleri_inherit( models.Model):
    _inherit = 'info_tsm.fatura_tahsilat_kurum_tipleri'
    @api.model
    def _get_default_company_id( self ):
        return self.env.user.company_id
    company_id      = fields.Many2one('res.company',default = _get_default_company_id)

class tsm_senkron_info_inherit( models.Model):
    _inherit = 'info_tsm.senkron_info'
    @api.model
    def _get_default_company_id( self ):
        return self.env.user.company_id
    company_id      = fields.Many2one('res.company',default = _get_default_company_id)

class tsm_silinen_kisimlar_inherit( models.Model):
    _inherit = 'info_tsm.silinen_kisimlar'
    @api.model
    def _get_default_company_id( self ):
        return self.env.user.company_id
    company_id      = fields.Many2one('res.company',default = _get_default_company_id)

class tsm_uygulama_parametreleri_inherit( models.Model):
    _inherit = 'info_tsm.uygulama_parametreleri'
    @api.model
    def _get_default_company_id( self ):
        return self.env.user.company_id
    company_id      = fields.Many2one('res.company',default = _get_default_company_id)