from pyftpdlib.servers import FTPServer
from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import TLS_FTPHandler
import time
from datetime import datetime
from threading import Thread

def threaded_function(arg):
    try:
        while True:
            try:
                #print '[%s] Still Working... '%datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            
                time.sleep( 10 )
            except:
                #print 'Durdum Ben.'
                break
                raise
    except:
        #print 'Durdum Ben.'
        raise
    

def main():
    authorizer = DummyAuthorizer()
    authorizer.add_user('user', '12345', '.', perm='elradfmw')
    authorizer.add_anonymous('.')
    handler = TLS_FTPHandler
    handler.certfile = '../gib_files/cert/server.crt'
    handler.keyfile = '../gib_files/cert/server.key'
    ##print dir ( handler )
    handler.authorizer = authorizer
    # requires SSL for both control and data channel
    handler.passive_ports = [60001,60002,60003,60004]
self.server.handler.masquerade_address_map = {"127.0.0.1": "128.128.128.128"} 
   handler.tls_control_required = True
    handler.tls_data_required    = True
    server = FTPServer(('', 8021), handler)
    thread = Thread(target = threaded_function, args = (10, ))
    thread.start()
    #thread.join()
    server.serve_forever()
    
        

if __name__ == '__main__':
    main()
