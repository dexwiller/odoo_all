# -*- coding: utf-8 -*-
from pyftpdlib.servers import FTPServer
from pyftpdlib.authorizers import DummyAuthorizer, AuthenticationFailed
from pyftpdlib.handlers import TLS_FTPHandler
from hashlib import md5
import os,sys
reload(sys)
import time
from datetime import datetime
from threading import Thread
import logging
import psycopg2
import base64
sys.setdefaultencoding('utf-8')
makine_adi = 'Prod'
if makine_adi == 'Deniz':
    glob_path    = '../'
    glob_db_name = 'development'
    glob_user    = 'odoo'
    masquerade_address = '192.168.1.62'
elif makine_adi == 'Prod':
    glob_path = '/var/lib/odoo/addons/tsm/'
    glob_db_name = 'production'
    glob_user    = 'odoo'
    masquerade_address = '185.35.23.7'
elif makine_adi == 'Dev':
    glob_path = '/var/lib/odoo/addons/tsm/'
    glob_db_name = 'development_p'
    glob_user    = 'odoo'
    masquerade_address = '185.35.23.4'
elif makine_adi == 'Test':
    glob_path    = '../'
    glob_db_name = 'production'#bu böyle
    glob_user    = 'odoo'
    masquerade_address = '192.168.1.88'
class file_name_keys:
    keys_dict = dict(
        Kayit     = 'info_tsm.kayit_listesi_dosya_satirlari',
        Durum     = 'info_tsm.ynokc_aktivasyon_listesi_dosya_satirlari',
        PublicKey = 'info_tsm.ynokc_public_key_update',
        Yetkili   = 'info_tsm.ynokc_yetkili_servis',
        Uyari     = 'info_tsm.yoknc_gib_uyari_dosya_satirlari'
    )
    OF_list       = [ keys_dict[ 'Kayit' ], keys_dict[ 'Durum' ], keys_dict[ 'PublicKey' ]]
    TC_list       = [ keys_dict[ 'Yetkili' ] ]
    UY_list       = [ keys_dict['Uyari'] ]

now = datetime.now()
year  = str(now.year)
month = str(now.month)
if len(month) == 1:
    month = '0' + month
day   = str(now.day)
if len(day) == 1:
    day = '0' + day

class connect:
    def __init__(self):
        self.conn = psycopg2.connect('postgresql:///%s'%glob_db_name)
        #self.conn = psycopg2.connect(dbname=glob_db_name, user=glob_user, password='manager', host="localhost", port = 5432)
    def select(self,table_name=None, columns=None, where=None,plain=None  ):
        if plain:
            sqls = 'select %s'%plain
        else:
            columns = ','.join( columns )
            sqls = 'select %s from %s where %s' %( columns, table_name, where)
        
        #print sqls
        cur = self.conn.cursor()
        results = None
        try:
            cur.execute( sqls )
            results = cur.fetchall()
        except:
            logging.info("SQL Hatasi : " + sqls )
        cur.close()
        self.conn.close()
        return results
        
    def update(self, table_name, where, column_names, values,sql_implicit=[] ):
        sqls_inside = ""
        for c in column_names:
            sqls_inside += str(c) + "='" + str(values[ column_names.index(c) ]) + "'"
            if sql_implicit and sql_implicit[ column_names.index(c) ] != '':
                    sqls_inside += " || %s " % sql_implicit[column_names.index(c) ]
            sqls_inside += ','
        sqls_inside = sqls_inside[:len(sqls_inside) - 1 ]
        
        sqls = 'update %s set %s where %s returning id '%( table_name, sqls_inside, where)
        #print sqls
        cur = self.conn.cursor()
        #try:
        cur.execute( sqls.decode('utf-8', 'replace') )
        donen = cur.fetchall()
        #except:
        #    logging.info("SQL Hatasi : " + sqls )
        cur.close()
        self.conn.commit()
        self.conn.close()
        return donen
    def insert ( self, table_name, column_names, values ):
         
        sqls = "insert into %s ( %s ) values ( '%s') returning id "% (table_name, ','.join( column_names), "','".join( map( lambda x:str(x) if type( x ) != 'str' else x  , values) ))
        #print sqls
        cur = self.conn.cursor()
        #try:
        cur.execute( sqls.decode('utf-8', 'replace') )
        donen = cur.fetchall()
        #except:
        #    logging.info("SQL Hatasi : " + sqls )
        cur.close()
        self.conn.commit()
        self.conn.close()
        return donen                                                 
        
        
class DummyMD5Authorizer(DummyAuthorizer):

    def validate_authentication(self, username, password, handler):
        if sys.version_info >= (3, 0):
            password = md5(password.encode('latin1'))
        hash_ = md5(password).hexdigest()
        try:
            if self.user_table[username]['pwd'] != hash_:
                raise KeyError
        except KeyError:
            raise AuthenticationFailed

class TSMHandler( TLS_FTPHandler ):

    def on_connect(self):
        #print "%s:%s connected" % (self.remote_ip, self.remote_port)

    def on_disconnect(self):
        # do something when client disconnects
        pass

    def on_login(self, username):
        # do something when user login
        pass

    def on_logout(self, username):
        # do something when user logs out
        pass

    def on_file_sent(self, file_):
        # do something when a file has been sent
        pass

    def on_file_received(self, file_):
        # do something when a file has been received
        file_type = None
        real_file = None
        connection = connect()
        try:
            file_name_= file_.split('/')[-1]
            file_name_splitted_l1 = file_name_.split('.')[0].split('_')
            file_type = file_name_keys.keys_dict[ file_name_splitted_l1[2].strip()]
            tarih     = file_name_splitted_l1[5]
            tarih_y   = tarih[:4]
            tarih_m   = tarih[4:6]
            tarih_d   = tarih[6:]
            tarih_t   = (tarih_y  , tarih_m, tarih_d ) 
            sira      = file_name_splitted_l1[6]
            if file_type not in file_name_keys.UY_list:
                where     = "sira_no = '%s' and date = '%s' and model_name='%s' "%( sira , '%s-%s-%s'%( tarih_y,tarih_m,tarih_d), file_type)
                resultset = connection.select ( 'tsm_gib_files', ['id'] , where)
                if len( resultset ) > 0:
                    file_id = resultset[0][0]
                else:
                    logging.error('Gelen Dosyanın Karşılığı Bulunamadı %s'.decode('latin5') % file_)
                    file_id = -1
                    raise
            if not file_type:
                raise
        except:
            logging.error('Gelen Dosya İsim Formati Yanlis veya Karşılığı Yok %s'.decode('latin5') % file_)
            return
        
        
        file_o = open( file_ , 'r')
        lines = file_o.readlines()
        #print lines
        if file_type in file_name_keys.UY_list:
            encoded     = base64.b64encode(file_o.read())
            file_id     = connect().select(plain = "nextval('tsm_gib_files_id_seq')")[0][0]
            tip         = 'Gib YNOKC Uyarı Dosyası'.decode('utf-8')
            file_insert = connect().insert('tsm_gib_files',
                                           ['id','name','file_','tip','model_name','date'],
                                           [file_id, file_name_, encoded, tip, file_type, year+'-'+month+'-'+day ]) 
            

        if len(lines) >= 2:
            if int(lines[-1][1:]) != 0:
                lines     = lines[1:len(lines) - 1]
                if file_type not in file_name_keys.UY_list:
                    result  =  connect().update('tsm_gib_files','id=%s'%file_id, column_names=['sended'], values=[-1])
                    hata_dict = {}
                    where_f   = None
                    kirmii_id      = []
                    for line in lines:
                        line = line.decode('utf-8')
                        if file_type in file_name_keys.OF_list:
                            hatali_of_kodu = line[9:21]
                            hata_kodu      = line[21:26]
                            hata_aciklama  = line[26:len(line) - 1].strip()
                            lot_id         = connect().select('stock_production_lot',['id'],"name='%s'"%hatali_of_kodu)
                            if len( lot_id ) > 0:
                                lot_id         = lot_id[0][0]      
                                where_f          = "kayit_dosyasi=%s and name=%s"%( file_id, lot_id )
                            
                        elif file_type in file_name_keys.TC_list:
                            tc_kimlik      = line[9:20]
                            hata_kodu      = line[len(line-55):len(line-50)]
                            hata_aciklama  = line[len(line)-50:len(line) - 1].strip()
                            res_part_id    = connect().select('res_partner',['id'],"tckn='%s'"%tc_kimlik)
                            if len( res_part_id ) > 0:
                                res_part_id    = res_part_id[0][0]
                                where_f          = "kayit_dosyasi=%s and yetkili_tckn='%s'"%( file_id, res_part_id )
                        
                        if where_f:
                            column_names   = ['durum', 'hata_kodu', 'hata_aciklama']
                            values         = [-1, hata_kodu, hata_aciklama ]
                            result         = connect().update( file_type.replace('.','_') , where_f , column_names, values )
                            
                            if len( result ) > 0:
                                kirmii_id.append( result[0][0] )
                        else:
                            logging.error(" Dosya Satırları Parse Edilemedi file = %s " .decode('latin5') % file_ )
                    
                    if kirmii_id:
                        
                        result_olumlu   = connect().update( file_type.replace('.','_') ," id not in (%s) and kayit_dosyasi = %s" % ( ','.join (map( lambda x: str(x) , kirmii_id)), file_id ) , ['durum','hata_kodu','hata_aciklama'], [2,'',''],['',"case when hata_kodu is not null and hata_kodu not like '%%---DÜZELTİLMİŞ HATA---%%' then '---DÜZELTİLMİŞ HATA--- '|| hata_kodu else hata_kodu end","case when hata_aciklama is not null and hata_aciklama not like '%%---DÜZELTİLMİŞ HATA---%%' then '---DÜZELTİLMİŞ HATA--- '|| hata_aciklama else hata_aciklama end"] )
                    
                    #print  'sorunlu kayit var'
                else:
                    #uyari dosyasi gelmiş, parse edecez.
                    #print 'Uyari Dosyasi'
                    for line in lines:
                        line = line.decode('utf-8')
                        tsm_kodu      = line[1:5].strip()
                        of_kodu       = line[5:17].strip()
                        tarih         = line[17:25].strip()
                        onem          = line[25:30].strip()
                        kategori      = line[30:35].strip()
                        alt_kategori  = line[35:55].strip()
                        kaynak        = line[55:70].strip()
                        aciklama      = line[70:170].strip()
                        aksiyon       = line[170:220].strip()
                        
                        ##print onem, kategori , alt_kategori, kaynak , aciklama, aksiyon
                        satir_id      = connect().select(plain = "nextval('tsm_yoknc_gib_uyari_dosya_satirlari_id_seq')")[0][0]
                        lot_id         = connect().select('stock_production_lot',['id'],"name='%s'"%of_kodu)
                        if len( lot_id ) > 0:
                                lot_id         = lot_id[0][0]
                                connect().insert( file_type.replace('.','_'),
                                               ['id','name','tarih','onem_derecesi','kategori', 'alt_kategori','kaynak', 'aciklama', 'aksiyon','kayit_dosyasi'],
                                               [ satir_id, lot_id, tarih[:4] + '-' + tarih[4:6] + '-' + tarih[6:], onem,kategori,alt_kategori,kaynak,aciklama,aksiyon,file_id])
                        else:
                            logging.error(" Dosya Satırları Parse Edilemedi file = %s " .decode('latin5') % file_ )

            else:
                if file_type not in file_name_keys.UY_list:
                    result    =  connect().update('tsm_gib_files','id=%s'%file_id, column_names=['sended'], values=[2])
                    res_lines =  connect().update( file_type.replace('.','_'), 'kayit_dosyasi=%s'%file_id, ['durum','hata_kodu','hata_aciklama'], [2,'',''],['',"case when hata_kodu is not null then '---DÜZELTİLMİŞ HATA--- '|| hata_kodu else null end","case when hata_aciklama is not null then '---DÜZELTİLMİŞ HATA--- '|| hata_aciklama else hata_aciklama end"])
                    #print 'tum kayitlar ok'
        else:
            logging.error('Gelen Dosya İçerik Formati Yanlis %s'.decode('latin5') % file_)
            #print 'dosya bozuk'
            
    def on_incomplete_file_sent(self, file_):
        # do something when a file is partially sent
        pass

    def on_incomplete_file_received(self, file_):
        # remove partially uploaded files
        import os
        os.remove( file_ )

def main():
    hash_ = md5('160g118i465b!_').hexdigest()
    authorizer = DummyMD5Authorizer()
    if not os.path.exists(glob_path + '/ftp/gib_receive' ):
        try:
            original_umask = os.umask(0)
            os.makedirs(glob_path+ '/ftp/gib_receive', 0777)
        finally:
            os.umask(original_umask)
            

    authorizer.add_user('gib_ftp_user', hash_, glob_path+ '/ftp/gib_receive', perm='elradfmw')
    authorizer.add_anonymous('.')
    handler = TSMHandler
    handler.certfile = glob_path + '/gib_files/cert/server.crt'
    handler.keyfile  = glob_path + '/gib_files/cert/server.key'
    ##print dir ( handler )
    handler.authorizer = authorizer
    # requires SSL for both control and data channel
    handler.passive_ports = [8022]
    handler.tls_control_required = True
    handler.tls_data_required    = True
    handler.masquerade_address = masquerade_address
    handler.permit_foreign_addresses = True
    server = FTPServer(('', 8021), handler)
    if not  os.path.exists(glob_path+'/gibftplog' ):
        os.mkdir(glob_path+'/gibftplog')
    logging.basicConfig(filename=glob_path+'/gibftplog/gibftp.log', level=logging.INFO)
    server.serve_forever()        


main()
