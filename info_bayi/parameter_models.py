# -*- coding: utf-8 -*-
from pysimplesoap.client import SoapClient, SimpleXMLElement
import xml.etree.ElementTree as ET
from openerp import models, fields, api, exceptions, SUPERUSER_ID
from openerp import http
from check_rules import check
from utils import create_file_chunk, generate_new_pass,f7
from gib_message_formatter import gib_formatter, gibSocket, gib_parameters
import base64,hashlib,os,xlrd
from datetime import datetime
import pytz    # $ pip install pytz
import tzlocal # $ pip install tzlocal

local_timezone = tzlocal.get_localzone() # get pytz tzinfo
batarya_selection = [(2,'2300 MAh'),
                     (5,'5000 Mah')]
renk_selection    = [(1,'Siyah'),
                     (2,'Gri')]

import subprocess
from whatcher import LogWatcher
import zipfile
import io
import string, random, hashlib,copy
file_dir = os.path.dirname(os.path.realpath(__file__))
default_column_map = {'production':'prod_default',
					  'development':'dev_default'}
prog_ids = {'techpos':7,'garanti':21,}

def get_pid(name):
	return check_output(["pidof",name])
import time
import logging
_logger = logging.getLogger( __name__ )
class Dummy:
	pass

fonk_tus1 = [(1,u'PARA ÜSTÜ'),
            (2,u'SATIŞ TİPLERİ'),
            (3,u'KISAYOL EKRANI'),
            (4,u'KAĞIT SÜR'),
            (5,u'X'),
            (6,u'=')]
fonk_tus2 = [(1,u'PLU'),
            (2,u'ÜRÜN GRUPLARI'),
            (3,u'YEMEK KARTI'),
            (4,u'FİŞ İPTAL'),
            (5,u'KREDİ KARTI'),
            (6,u'NAKİT')]
fonk_tsm_sabit = 2021

altin_kod_map = {
	'B' : 601,
	'18' : 602,
	'14' : 603,
	'C' : 604,
	'Y' : 605,
	'T' : 606,
	'G' : 607,
	'A' : 608,
	'R' : 609,
	'H' : 610,
	'GA' : 611,
	'EC' : 612,
	'EY' : 613,
	'ET' : 614,
	'EG' : 615
}
dosya_konumlari = [(1, 'Appfolder'),
				(2, 'Windows'),
				(3, 'ProgramFiles'),
				(4, 'MainFolder')
				]
kdv_index_map = {1:'KDV1 - %0',
				 2:'KDV2 - %1',
				 3:'KDV3 - %8',
				 4:'KDV4 - %18',
				 5:'KDV5 - %5',
				 6:'KDV6 - %10',
				 7:'KDV7 - %16',
				 8:'KDV8 - %20',}
birim_tipleri = [(1, 'Adet'),
				(2, 'Kilogram'),
				(3, 'Gram'),
				(4, 'Litre'),
				(5, 'Paket'),
				(6, 'Kutu'),
				(7, 'Metre'),
				(8, 'Santimetre'),
				(9, 'Metreküp'),
				(10, 'Santimetreküp'),
				(11, 'Metrekare'),
				(12, 'Santimetrekare'),
				(13, 'Rulo'),
				(14, 'Set'),
				(15, 'Top')]
birim_tipleri_map = {1 : 'Adet',
				2: 'Kilogram',
				3: 'Gram',
				4: 'Litre',
				5: 'Paket',
				6: 'Kutu',
				7: 'Metre',
				8: 'Santimetre',
				9: 'Metreküp',
				10: 'Santimetreküp',
				11: 'Metrekare',
				12: 'Santimetrekare',
				13: 'Rulo',
				14: 'Set',
				15: 'Top'
			}
kdv_index = [(1, 'KDV1 - %0'),
			(2, 'KDV2 - %1'),
			(3, 'KDV3 - %8'),
			(4, 'KDV4 - %18'),
			(5, 'KDV5 - %5'),
			(6, 'KDV6 - %10'),
			(7, 'KDV7 - %16'),
			(8, 'KDV8 - %20'),]
bip_tipleri = [ (1, '01'),
				(2, '02'),
				(3, '03'),
				(4, '04'),
				(5, '05'),
				(6, '06'),
				(7, '07'),
				(8, '08'),
				(9, '09'),
				(10,'10'),
				(11,'11'),
				(12,'12'),
				(13,'13'),
				(14,'14'),
				(15,'15'),
				]

default_KDV = {0:1,1:2,8:3,18:4,5:5,10:6,16:7,20:8}

class satis_ekran_tipleri( models.Model):

	_name = 'info_tsm.p_satis_ekran_tipleri'

	name = fields.Char( string = 'Satış Ekran Tip Adı')
	kod  = fields.Float( string = 'Satış Ekran Tip Kodu', size=2, digits=(2,0))

class gun_sonu_tipleri( models.Model):

	_name = 'info_tsm.p_gun_sonu_tipleri'

	name = fields.Char( string = 'Gün Sonu Tip Adı')
	kod  = fields.Float( string = 'Gün Sonu Tip Kodu', size=2, digits=(2,0))

class sleep_suspend( models.Model):

	_name = 'info_tsm.p_sleep_suspend'

	name = fields.Char( string = 'Zaman Ayarı')
	kod  = fields.Float( string = 'Alan Değeri', size=2, digits=(2,0))
	sleep_default = fields.Boolean('Sleep için Default')
	suspend_default = fields.Boolean('Suspend için Default')

class okc_durum_raporlama( models.Model):

	_name = 'info_tsm.p_okc_durum_raporlama'

	name = fields.Char( string = 'Durum Rapor Adı')
	kod  = fields.Float( string = 'Durum Rapor Kodu', size=2, digits=(2,0))

class okc_left_button( models.Model):

	_name = 'info_tsm.p_okc_left_button'

	name = fields.Char( string = 'Sol Tuş Adı')
	kod  = fields.Float( string = 'Sol Tuş Kodu', size=2, digits=(2,0))

class okc_right_button( models.Model):

	_name = 'info_tsm.p_okc_right_button'

	name = fields.Char( string = 'Sağ Tuş Adı')
	kod  = fields.Float( string = 'Sağ Tuş Kodu', size=2, digits=(2,0))

class okc_prm_update_time( models.Model ):

	_name = 'info_tsm.p_prm_update_type'

	name = fields.Char( string = 'Prm Güncelleme Tipi')
	kod  = fields.Float( string = 'Prm Güncelleme Tip Kodu', size=2, digits=(2,0))

class ilk_iletisim_params( models.Model):
	_name = 'info_tsm.ilk_iletisim_params'

	name                = fields.Char ('İlk İletişim Parametre Adı')
	gib_iletisim_durumu = fields.Boolean('Gib İletişim Durumu')
	haberlesme_yontemi  = fields.Many2one('info_tsm.haberlesme_tipleri', string='Haberleşme Tipi')
	katma_deger_mesaj   = fields.Float(string='Katma Değer Mesaj Durumu (0:Disabled, 1:Enabled)', default=1, size=2, digits=(2,0))
	tsm_sunucu_versiyonu = fields.Float(string='TSM Sunucu Versiyonu (1:V1, 2:V2, 3:Tübitak)', default=1, size=2, digits=(2,0))
	grup_id              = fields.One2many('info_tsm.parametre_grup','ilk_iletisim', string="Parametre Grupları" )

	default       = fields.Boolean(string='Varsayılan İlk İletişim.')

class haberlesme( models.Model ):

	_name = 'info_tsm.haberlesme'

	name                                = fields.Char(string="Haberleşme Parametre Adı", required=True)
	aciklama                            = fields.Text(string="Aciklama")
	iletisim_bilgisi_gecerlilik_tarihi  = fields.Datetime(string='İletişim Bilgisi Geçerlilik Tarihi', required=False, )
	eth_birincil_ip_no                  = fields.Char(string='Ethernet Birincil Ip No', required=False, )
	eth_birincil_port_no                = fields.Integer(string='Ethernet Birincil Port No', required=False, )
	eth_ikincil_ip_no                   = fields.Char(string='Ethernet İkincil Ip No', required=False, )
	eth_ikincil_port_no                 = fields.Integer(string='Ethernet İkincil Port No', required=False, )
	dial_up_birincil_tel_no             = fields.Float(string='Dial Up Birincil Tel No',size=10, digits=(10, 0), required=False, )
	dial_up_ikincil_tel_no              = fields.Float(string='Dial Up İkincil  Tel No',size=10, digits=(10, 0), required=False, )
	gprs_birincil_ip_no                 = fields.Char(string='GPRS Birincil Ip No', required=False, )
	gprs_birincil_port_no               = fields.Integer(string='GPRS Birincil Port No', required=False, )
	gprs_ikincil_ip_no                  = fields.Char(string='GPRS İkincil Ip No', required=False, )
	gprs_ikincil_port_no                = fields.Integer(string='GPRS İkincil Port No', required=False, )
	gsm_birincil_tel_no                 = fields.Float(string='Gsm Up Birincil Tel No',size=10, digits=(10, 0), required=False, )
	gsm_ikincil_tel_no                  = fields.Float(string='Gsm Up İkincil  Tel No',size=10, digits=(10, 0), required=False, )
	haberlesme_tipi                     = fields.Many2one('info_tsm.haberlesme_tipleri', string='Haberleşme Tipi', required=True)

	grup_id                             = fields.One2many('info_tsm.parametre_grup','haberlesme', string="Parametre Grupları" )

	default       = fields.Boolean(string='Varsayılan Haberleşme.')


	@api.one
	@api.constrains('eth_birincil_ip_no','eth_ikincil_ip_no','gprs_birincil_ip_no','gprs_ikincil_ip_no' )
	def _check_ip_address(self):

		##print self.haberlesme_tip_adi
		ch_obj = {'eth_birincil_ip_no':'Ethernet Birincil Ip No',
				  'eth_ikincil_ip_no' :'Ethernet İkincil Ip No',
				  'gprs_birincil_ip_no':'GPRS Birincil Ip No',
				  'gprs_ikincil_ip_no':'GPRS İkincil Ip No'}
		foo = check( exceptions, self )
		foo.check_ip_no( ch_obj )

class haberlesme_tipleri( models.Model):
	_name = 'info_tsm.haberlesme_tipleri'

	name            = fields.Char( string='Haberleşme Tip Adı '      , required=True)
	haberlesme_kodu = fields.Char( string='Haberleşme Kodu '         , required=True)
	apn_adi         = fields.Char( string='Apn Adı '         		 , required=True)
	apn_user        = fields.Char( string="Varsa Apn Kullanıcı Adı"  , size=24 )
	apn_pass        = fields.Char( string="Varsa Apn Kullanıcı Şifre", size=24 )
	
	@api.one
	@api.constrains('name')
	def _check_unique_insesitive_record(self):

		##print self.haberlesme_tip_adi
		ch_obj = {'name':'Haberleşme Tip Adı'}
		foo = check( exceptions, self )
		foo.check_unique_bos( ch_obj )

class okc_gib_parametre( models.Model ):
	_name = 'info_tsm.okc_gib_parametre'


	name                                 = fields.Char(string="Ökc Parametre Adı",required=True)
	aciklama                             = fields.Text(string="Aciklama")
	gibe_baglanmadan_acik_kalma_suresi   = fields.Integer(string="Gib'e Bağlanmadan Açık Kalma Süresi", required=True, )
	teminal_status                       = fields.Many2one('info_tsm.terminal_status_tipleri', string='Terminal Statüsü', required=True)
	terminal_id                          = fields.Many2one('stock.production.lot',string="Gib Terminal ID",
														   domain=[('lot_state', '=', 'sold'),
														('terminal_id', '!=', False)])
	param_imza                           = fields.Char(string="Param İmza")

	z_rapor_gonderme_saati               = fields.Char( string="Z Raporu Gönderme Saati",size=4, required=False)
	z_rapor_tekrar_deneme_adedi          = fields.Integer( string="Z Raporu Tekrar Deneme Adedi", required=False)
	z_rapor_tekrar_deneme_araligi        = fields.Integer( string="Z Raporu Tekrar Deneme Aralığı", required=False)

	olay_kaydi_gonderme_saati               = fields.Char( string="Olay Kaydı Gönderme Saati",size=4, required=False)
	olay_kaydi_tekrar_deneme_adedi          = fields.Integer( string="Olay Kaydı Tekrar Deneme Adedi", required=False)
	olay_kaydi_tekrar_deneme_araligi        = fields.Integer( string="Olay Kaydı Tekrar Deneme Aralığı", required=False)

	fis_gonderme_gonderme_saati               = fields.Char( string="Fiş Gönderme Gönderme Saati",size=4, required=False)
	fis_gonderme_tekrar_deneme_adedi          = fields.Integer( string="Fiş Gönderme Tekrar Deneme Adedi", required=False)
	fis_gonderme_tekrar_deneme_araligi        = fields.Integer( string="Fiş Gönderme Tekrar Deneme Aralığı", require=False)

	istatistik_bilgileri_gonderme_saati               = fields.Char( string="İstatistik Bilgileri Gönderme Saati",size=4, required=False)
	istatistik_bilgileri_tekrar_deneme_adedi          = fields.Integer( string="İstatistik Bilgileri Tekrar Deneme Adedi", required=False)
	istatistik_bilgileri_tekrar_deneme_araligi        = fields.Integer( string="İstatistik Bilgileri Tekrar Deneme Aralığı", require=False)

	olay_kritiklik_seviyesi_minimum                   = fields.Integer( string="Olay Kritiklik Seviyesi - Minimum", require=False)
	olay_kritiklik_seviyesi_esik                      = fields.Integer( string="Olay Kritiklik Seviyesi - Eşik", require=False)

	simetrik_anahtar_yenileme_periyodu                = fields.Integer( string="Simetrik Anahtar Yenileme Periyodu", require=False)
	sira_no_maximum_fark_degeri                       = fields.Integer( string="Sıra No Maksimum Fark Değeri", require=False)

	kdv_1_orani                                        = fields.Integer( string="KDV 1 Oranı", require=False)
	kdv_2_orani                                        = fields.Integer( string="KDV 2 Oranı", require=False)
	kdv_3_orani                                        = fields.Integer( string="KDV 3 Oranı", require=False)
	kdv_4_orani                                        = fields.Integer( string="KDV 4 Oranı", require=False)
	kdv_5_orani                                        = fields.Integer( string="KDV 5 Oranı", require=False)
	kdv_6_orani                                        = fields.Integer( string="KDV 6 Oranı", require=False)
	kdv_7_orani                                        = fields.Integer( string="KDV 7 Oranı", require=False)
	kdv_8_orani                                        = fields.Integer( string="KDV 8 Oranı", require=False)

	urun_gruplari                                      = fields.Many2many('info_tsm.urun_gruplari', string="Ürün Grupları" )

class okc_ozel_kdv_tablosu( models.Model ):

	_name = "info_tsm.ozel_kdv_tablosu"

	name      = fields.Char( string= "Kısım Açıklaması",size=19, required = True)
	kdv_orani = fields.Selection (string = "Gib KDV İndeksi ", required = True, selection=kdv_index)
	unit_type = fields.Selection (string = "Birim Tipi", required = True, selection= birim_tipleri )
	#unit type odoo nun zırvvalaması sebebiyle id ler bir artırıldı... (0 id yi odoo yok kabul ediyor.)
	
		
	@api.multi
	def name_get(self):
		res = []
		for record in self:
			total_name = record.name
			if record.name and record.kdv_orani:
				if record.name.find (' - KD') > 0:
					total_name = record.name.split( ' - KD' )[0]
				total_name = total_name + ' - ' + str( kdv_index_map [record.kdv_orani] )
			
			res.append((record['id'],total_name ))
		return res
	
	'''
	@api.onchange('kdv_orani')
	def set_name_by_kdv_orani(self ):
		if self.kdv_orani:
			#print dir ( self.kdv_orani ), self.kdv_orani

			if self.name.find (' - KD') > 0:
				self.name = self.name.split( ' - KD' )[0]
			self.name = self.name + ' - ' + str( kdv_index_map [self.kdv_orani] )
	'''

class okc_ozel_urun_grubu_tablosu ( models.Model):

	_name = 'info_tsm.okc_ozel_urun_grubu_tablosu'

	name           = fields.Char( string = 'Grup İsmi', size=19,required=True )
	#ozel_kdv_orani = fields.('info_tsm.ozel_kdv_tablosu', required=True)
	#kdv_orani      = fields.Selection (string = "Gib KDV İndeksi ", required = True, selection=kdv_index)
	grkodu         = fields.Many2one('info_tsm.urun_gruplari')

class okc_gib_test_parametre ( models.Model ):
	_inherit = 'info_tsm.okc_gib_parametre'
	gib_test  = fields.Integer( string="Gib Test Parametreleri")

	@api.multi
	def send_gib(self):
		self.ensure_one()
		if not self.search([('id', '=', self.id)]):
			raise exceptions.ValidationError( "Parametreleri Gib'e Göndermeden Önce Kaydetmelisiniz." )
		gib_formatter_obj = gib_formatter()

		s     = gibSocket()
		obj_p = gib_parameters( self.id )

		message_header = '''6000010000FF8811'''
		message_part_1 = obj_p.message_part_1

		param_save_date       = obj_p.save_date
		terminal_id           = self.terminal_id[0].terminal_id
		if not terminal_id:
			raise exceptions.ValidationError('GİB Terminal ID Bulunamadı, Seçilen erminalin satışının yapıldığından eminseniz, Terminalden İlk Kayıt Mesajı Gönderiniz.')

		message_part_2_header = 'DF40'
		message_part_2        = 'DFC00108%sDFC00805%s' %( terminal_id , param_save_date )
		parametre_header      = 'DFC00A'

		gibe_baglanmadan_acik_kalma_suresi              = gib_formatter_obj.eksik_byte_tamamlamaca (self.gibe_baglanmadan_acik_kalma_suresi, 4)
		teminal_status                                  = gib_formatter_obj.eksik_byte_tamamlamaca ( gib_formatter_obj.str_to_ascii (self.teminal_status.name), 2, 'ascii')

		z_rapor_gonderme_saati                          = gib_formatter_obj.eksik_byte_tamamlamaca (self.z_rapor_gonderme_saati ,4)
		z_rapor_tekrar_deneme_adedi                     = gib_formatter_obj.eksik_byte_tamamlamaca (self.z_rapor_tekrar_deneme_adedi,2)
		z_rapor_tekrar_deneme_araligi                   = gib_formatter_obj.eksik_byte_tamamlamaca (self.z_rapor_tekrar_deneme_araligi,6)

		olay_kaydi_gonderme_saati                       = gib_formatter_obj.eksik_byte_tamamlamaca (self.olay_kaydi_gonderme_saati,4)
		olay_kaydi_tekrar_deneme_adedi                  = gib_formatter_obj.eksik_byte_tamamlamaca (self.olay_kaydi_tekrar_deneme_adedi,2)
		olay_kaydi_tekrar_deneme_araligi                = gib_formatter_obj.eksik_byte_tamamlamaca (self.olay_kaydi_tekrar_deneme_araligi,6)

		fis_gonderme_gonderme_saati                     = gib_formatter_obj.eksik_byte_tamamlamaca (self.fis_gonderme_gonderme_saati ,4)
		fis_gonderme_tekrar_deneme_adedi                = gib_formatter_obj.eksik_byte_tamamlamaca (self.fis_gonderme_tekrar_deneme_adedi,2)
		fis_gonderme_tekrar_deneme_araligi              = gib_formatter_obj.eksik_byte_tamamlamaca (self.fis_gonderme_tekrar_deneme_araligi,6)

		istatistik_bilgileri_gonderme_saati             = gib_formatter_obj.eksik_byte_tamamlamaca (self.istatistik_bilgileri_gonderme_saati,4)
		istatistik_bilgileri_tekrar_deneme_adedi        = gib_formatter_obj.eksik_byte_tamamlamaca (self.istatistik_bilgileri_tekrar_deneme_adedi,2)
		istatistik_bilgileri_tekrar_deneme_araligi      = gib_formatter_obj.eksik_byte_tamamlamaca (self.istatistik_bilgileri_tekrar_deneme_araligi,6)

		olay_kritiklik_seviyesi_minimum                 = gib_formatter_obj.eksik_byte_tamamlamaca (self.olay_kritiklik_seviyesi_minimum,2)
		olay_kritiklik_seviyesi_esik                    = gib_formatter_obj.eksik_byte_tamamlamaca (self.olay_kritiklik_seviyesi_esik,2)

		simetrik_anahtar_yenileme_periyodu              = gib_formatter_obj.eksik_byte_tamamlamaca (self.simetrik_anahtar_yenileme_periyodu,4)
		sira_no_maximum_fark_degeri                     = gib_formatter_obj.eksik_byte_tamamlamaca (self.sira_no_maximum_fark_degeri , 6)
		kdv_1_orani                                     = gib_formatter_obj.eksik_byte_tamamlamaca (self.kdv_1_orani, 4)
		kdv_2_orani                                     = gib_formatter_obj.eksik_byte_tamamlamaca (self.kdv_2_orani, 4)
		kdv_3_orani                                     = gib_formatter_obj.eksik_byte_tamamlamaca (self.kdv_3_orani, 4)
		kdv_4_orani                                     = gib_formatter_obj.eksik_byte_tamamlamaca (self.kdv_4_orani, 4)
		kdv_5_orani                                     = gib_formatter_obj.eksik_byte_tamamlamaca (self.kdv_5_orani, 4)
		kdv_6_orani                                     = gib_formatter_obj.eksik_byte_tamamlamaca (self.kdv_6_orani, 4)
		kdv_7_orani                                     = gib_formatter_obj.eksik_byte_tamamlamaca (self.kdv_7_orani, 4)
		kdv_8_orani                                     = gib_formatter_obj.eksik_byte_tamamlamaca (self.kdv_8_orani, 4)

		urun_grup_adedi                                 = len( self.urun_gruplari )
		urun_gruplari                                   = []
		if urun_grup_adedi > 0:
			for urun_grup_obj in self.urun_gruplari:
				urun_gruplari.append( gib_formatter_obj.eksik_byte_tamamlamaca (urun_grup_obj.name,4 ))
				urun_gruplari.append( gib_formatter_obj.eksik_byte_tamamlamaca (urun_grup_obj.urun_kdv_indeksi,4 ))

		urun_gruplari_str       = "".join ( urun_gruplari )
		urun_grup_adedi_str     = gib_formatter_obj.eksik_byte_tamamlamaca( urun_grup_adedi , 4 )
		parametre_bilgileri_str = gibe_baglanmadan_acik_kalma_suresi +\
								  teminal_status +\
								  z_rapor_gonderme_saati +\
								  z_rapor_tekrar_deneme_adedi +\
								  z_rapor_tekrar_deneme_araligi +\
								  olay_kaydi_gonderme_saati +\
								  olay_kaydi_tekrar_deneme_adedi +\
								  olay_kaydi_tekrar_deneme_araligi +\
								  fis_gonderme_gonderme_saati +\
								  fis_gonderme_tekrar_deneme_adedi +\
								  fis_gonderme_tekrar_deneme_araligi +\
								  istatistik_bilgileri_gonderme_saati +\
								  istatistik_bilgileri_tekrar_deneme_adedi +\
								  istatistik_bilgileri_tekrar_deneme_araligi +\
								  olay_kritiklik_seviyesi_minimum +\
								  olay_kritiklik_seviyesi_esik +\
								  simetrik_anahtar_yenileme_periyodu +\
								  sira_no_maximum_fark_degeri +\
								  kdv_1_orani +\
								  kdv_2_orani +\
								  kdv_3_orani +\
								  kdv_4_orani +\
								  kdv_5_orani +\
								  kdv_6_orani +\
								  kdv_7_orani +\
								  kdv_8_orani +\
								  urun_grup_adedi_str +\
								  urun_gruplari_str

		uzunluk_parametre_len = gib_formatter_obj.uzunluk_lrc_final_send_data_hesaplamaca( parametre_bilgileri_str , True )

		parametre_final_str    = gib_formatter_obj.uzunluk_prefix_formatlamaca(uzunluk_parametre_len[1],  parametre_header )  + parametre_bilgileri_str
		message_part_2         = message_part_2  + parametre_final_str

		uzunluk_message_part_2 = gib_formatter_obj.uzunluk_lrc_final_send_data_hesaplamaca( message_part_2 , True )
		message_part_2         = gib_formatter_obj.uzunluk_prefix_formatlamaca(uzunluk_message_part_2[1],  message_part_2_header ) + message_part_2

		hash_sha256            = hashlib.sha256()
		hash_sha256.update ( parametre_bilgileri_str )
		hash_params            = hash_sha256.hexdigest()
		param_sign_data        = hash_params + terminal_id + obj_p.sira_no + obj_p.imza_saati
		#print 'start_data  :',param_sign_data
		#param_sign             = obj_p.prvEncrypt ( obj_p.p_key_loc + 'Sgibsign.key',param_sign_data  )
		param_sign             = obj_p.sign_data ( obj_p.p_key_loc + 'Sgibsign.key',param_sign_data  )

		message_footer         = 'DF42820106DFC20B820100' + param_sign
		final_message          = message_part_1 + message_part_2 + message_footer

		uzunluk_message        = gib_formatter_obj.uzunluk_lrc_final_send_data_hesaplamaca ( final_message, True  )

		message                = gib_formatter_obj.uzunluk_prefix_formatlamaca(uzunluk_message[1],  message_header ) + final_message

		send_message           = gib_formatter_obj.uzunluk_lrc_final_send_data_hesaplamaca( message )

		s.connect(gib_formatter.GIB_SERVER_IP, gib_formatter.GIB_SERVER_PORT)
		s.gibsend( send_message )
		donen =  s.gibreceive()

		s.close()

		return {
				'type': 'ir.actions.client',
				'tag': 'reload',
			}

class infoteks_apn( models.Model ):
	_name       = 'info_tsm.infoteks_apn'

	name        = fields.Char( string="Apn Adresi", size=24 )
	apn_user    = fields.Char( string="Apn Kullanıcı Adı", size=24 )
	apn_pass    = fields.Char( string="Apn Kullanıcı Şifre", size=24 )

class infoteks_ntp( models.Model ):
	_name       = 'info_tsm.infoteks_ntp'

	name        = fields.Char( string="NTP Adresi", size=24 )

	@api.one
	@api.constrains('name' )
	def _check_ip_address(self):

		##print self.haberlesme_tip_adi
		ch_obj = {'name':'Ntp Adresi'}
		foo = check( exceptions, self )
		foo.check_ip_no( ch_obj )

class maximum_fis_tutari( models.Model ):
	_name       = 'info_tsm.infoteks_max_fis_tutari'

	name        = fields.Float( string="Maksimum Fiş Tutarı", size=24, digits=(22,2) )

class terminal_status_tipleri( models.Model ):

	_name = 'info_tsm.terminal_status_tipleri'


	name     = fields.Char( string='Terminal Status', required=True )
	aciklama = fields.Char(string='Açıklama')

	@api.one
	@api.constrains('name')
	def _check_unique_insesitive_record(self):

		##print self.haberlesme_tip_adi
		ch_obj = {'name':'Terminal Status'}
		foo = check( exceptions, self )
		foo.check_unique_bos( ch_obj )

class urun_gruplari(models.Model ):

	_name='info_tsm.urun_gruplari'

	name             = fields.Char  ( string='Ürün Grup Kodu', required=True )
	urun_kdv_indeksi = fields.Integer(string='Ürün KDV indeksi')
	aciklama         = fields.Char(string='Açıklama')

	@api.one
	@api.constrains('name')
	def _check_unique_insesitive_record(self):

		##print self.haberlesme_tip_adi
		ch_obj = {'name':'Ürün Grup Kodu'}
		foo = check( exceptions, self )
		foo.check_unique_bos( ch_obj )

class gib_terminal_id ( models.Model ):
	_inherit='stock.production.lot'
	terminal_id         = fields.Char( string= "Terminal ID ",index=True)
	firma_kodu          = fields.Char( string= "Firma Kodu",index=True)
	uygulama_params     = fields.Many2one('info_tsm.parametre_grup', string='Uygulama Parametreleri' )
	urun_params         = fields.Many2one('info_tsm.okc_urun_grup', string='Ürün Grup Parametreleri' )
	kur_params          = fields.Many2one('info_tsm.okc_kur_altin_grup', string='Ürün Kur ve Altın Parametreleri' )
	satis_tahsilat_params = fields.Many2one('info_tsm.okc_tahsilat_satis_grup', string='Ürün Tahsilat Satış Tipleri Parametreleri' )
	urun_tek_update     = fields.Boolean(string='Toplu Update')
	urun_tek_update_kur = fields.Boolean(string='Toplu Update')
	urun_tek_update_tahsilat_satis = fields.Boolean(string='Toplu Update')
	okc_aktivasyon      = fields.Many2one('info_tsm.gib_yoknc_status_codes',string='YNÖKC Son Gib Durum')
	yazilim_hash        = fields.Char(string="Yazılım Hash Kodu")
	eku_no              = fields.Char(string="Ekü No")

	kdv1                = fields.Integer(string='Gib KDV 1')
	kdv2                = fields.Integer(string='Gib KDV 2')
	kdv3                = fields.Integer(string='Gib KDV 3')
	kdv4                = fields.Integer(string='Gib KDV 4')
	kdv5                = fields.Integer(string='Gib KDV 5')
	kdv6                = fields.Integer(string='Gib KDV 6')
	kdv7                = fields.Integer(string='Gib KDV 7')
	kdv8                = fields.Integer(string='Gib KDV 8')

	imei_mtm_set_time    = fields.Datetime(string='Son Mtm Imei Guncelleme Tarihi')
	last_connection      = fields.Char(string="İnternet",compute='get_last_connection')
	modem_tipi           = fields.Selection(related='imei.modem_tipi')
	

	barkod	= fields.Boolean(string='Barkod Okuyucu')
	kamera	= fields.Boolean(string='Kamera')
	parmak	= fields.Boolean(string='Parmak İzi Okuyucu')
	renk	= fields.Selection(selection=renk_selection)
	batarya	= fields.Selection(selection = batarya_selection)

	aktivasyon      = fields.Boolean( string="Aktivasyon", compute="_get_aktivasyon_status")
	aktivasyon_text = fields.Char(string="Durum", compute = "_get_aktivasyon_status_text")
	fabrika_ayar    = fields.Char(string="Fabrika Ayarları", compute = '_get_fabrika')
	program_vers    = fields.Char( string="Program Versiyonları",compute = '_get_progs')
	haberlesme      = fields.Many2one('info_tsm.haberlesme_tipleri',related='uygulama_params.haberlesme.haberlesme_tipi')
	
	
	available_for_use  = fields.Boolean(compute='_get_availability_to_use')
	available_for_sell = fields.Boolean(compute='_get_availability_to_sell')
	iade_alinability   = fields.Boolean(compute='_get_iade_alinability_control')
	
	sube_transfer 	   = fields.Boolean(compute='_get_sube_transfer_control')
	
	foreign_device     = fields.Integer()
	
	@api.multi
	def get_last_connection(self):
		
		a = "select terminal_id,max(create_date::timestamp(0)) from info_tsm_okc_log where terminal_id in (%s)  group by terminal_id;"%','.join(map (lambda x:"'" + str(x) + "'",self.ids))
		self.env.cr.execute( a )
		r = self.env.cr.fetchall()
		
		for s in self:
			last_log = next((x[1] for x in r if s.id == x[0] ), None)
			if last_log:
				date_now   = datetime.now()
				local_time = last_log
				time_zone  = self.env.context['tz']
				tz         = pytz.timezone(time_zone)
				tzoffset   = tz.utcoffset(date_now)
				local_time = datetime.strptime(local_time,'%Y-%m-%d %H:%M:%S') + tzoffset
				local_time = datetime.strftime(local_time,'%Y-%m-%d %H:%M:%S')
				s.last_connection      = local_time
			
	@api.depends('uygulama_params')
	@api.multi
	def okc_grup_iliski_sil( self ):
		self.ensure_one()
		self.uygulama_params = None
		return { }

	@api.multi
	def write(self,vals):
		res = super(gib_terminal_id,self).write( vals )
		if vals.has_key('urun_params'):
			#onceki_senkronu_sil
			onceki = self.env['info_tsm.senkron_info'].search([('name','=',self.id)])
			for o in onceki:
				o.unlink()

			senkron_info_dict = {'name':self.id,
								 'grup_id':vals['urun_params'],
								 'senkron':False,
								 'grup_senkron':False
								 }
			self.env['info_tsm.senkron_info'].create( senkron_info_dict )
		return res
	
	@api.model
	def create(self, vals):
		res = super(gib_terminal_id,self).create( vals )
		if res.urun_params:
			senkron_info_dict = {'name':res.id,
									 'grup_id':res.urun_params.id,
									 'senkron':False,
									 'grup_senkron':False
									 }
			self.env['info_tsm.senkron_info'].create( senkron_info_dict )
		return res
	
	'''
	product_lots        = fields.Many2one(string="Satılan Seri No'lar", store=True,
							compute='_get_lot_by_name')

	@api.one
	@api.depends('name')
	def _get_lot_by_name(self):
		if not (self.name):
			return
		#print self.name
		lot_obj = http.env['stock.production.lot'].sudo().search([('name','=',self.name)])
		#print lot_obj
		self.product_lots =  lot_obj[0].id
	'''

class kur_bilgileri(models.Model ):
	_name='info_tsm.kur_bilgileri'

	name                      = fields.Char(string="Kur Name")
	para_birimi_kodu          = fields.Integer( string = "Para Birimi Kodu")
	para_birimi_kisaltmasi    = fields.Char( string = "Para Birimi Kısaltması")
	para_birimi_isareti       = fields.Char( string = "Para Birimi İşareti")
	isaret_yonu               = fields.Char( string = "İşaret Yönü")
	binlik_ayraci             = fields.Char( string = "Binlik Ayracı")
	kurus_ayraci              = fields.Char( string = "Kuruş Ayracı")
	kurus_basamak_adedi       = fields.Integer( string = "Kuruş Basamak Adedi")
	kur_cevrimi               = fields.Float( string = "Kur Çevrimi", size=12, digits=(12,0))
	aktif                     = fields.Boolean(string ="Aktif")

	terminal_id               = fields.Many2many('stock.production.lot', string='Terminal ID')
	sira_no                   = fields.Float( string="Sıra No", size=15, digits=(15,0))
	gib_test                  = fields.Integer( string="Gib Test Parametreleri")
	dafault       = fields.Boolean(string='Gruplara Varsayılan Olarak Eklensin mi?')
	@api.onchange('para_birimi_kisaltmasi')
	def onchance_parabirimi_kodu( self ):
		self.name = self.para_birimi_kisaltmasi

class uygulama_parametreleri( models.Model ):

	_name='info_tsm.uygulama_parametreleri'

	@api.model
	def _get_default_sleep( self ):
		return self.env['info_tsm.p_sleep_suspend'].search([('sleep_default', '=', True)]).id
	@api.model
	def _get_default_suspend( self ):
		return self.env['info_tsm.p_sleep_suspend'].search([('suspend_default', '=', True)]).id

	name                = fields.Char( string='Parametre Adı', required=True)
	aktif               = fields.Boolean(string='Parametre Durumu',default=True)

	group_id            = fields.One2many('info_tsm.parametre_grup', 'parametre', string='Tanımlı Olduğu Gruplar')


	appissilent         = fields.Boolean(string='Arka Planda Çalış?')
	loginrequire        = fields.Boolean(string='Login Zorunlu mu?')
	canremovesalesline  = fields.Boolean(string='Satış Satırını Kaldır?')
	canfingerlogin      = fields.Boolean(string='Parmak İzi Kullan?')
	gprsopenmode        = fields.Boolean(string='Manuel GPRS Kullan?')
	foreignsales        = fields.Boolean(string='Uluslar Arası Satışlar?')
	flightmode          = fields.Boolean(string='Uçuş Modu?')
	selectedsalesscreen = fields.Many2one('info_tsm.p_satis_ekran_tipleri',string='Seçili Satış Ekranı')
	enddaytype          = fields.Many2many('info_tsm.p_gun_sonu_tipleri',string='Gün Sonu Tipi')
	enddaytime          = fields.Float(string='Gün Sonu Zamanı')
	statereptype        = fields.Many2one('info_tsm.p_okc_durum_raporlama',string='Durum Cevap Tipi')
	staterepperiod      = fields.Integer(string='Durum Cevap Gönderme Periyodu')
	sendsalestsm        = fields.Boolean(string='Satış TSM Gönderme')
	sendsalestsmtime    = fields.Integer(string='Satış TSM Gönderme Zamanı')
	stocksyncmode       = fields.Boolean(string='Stok Eşitleme Modu')
	stocksynctime       = fields.Integer(string='Stok Eşitleme Zamanı')
	left_button          = fields.Many2one('info_tsm.p_okc_left_button' , string='Sol Buton')
	right_button         = fields.Many2one('info_tsm.p_okc_right_button', string='Sağ Buton')
	prmupdatetype       = fields.Many2one('info_tsm.p_prm_update_type', string='Prm Güncelleme Tipi')
	printzslip          = fields.Boolean(string='Z raporu Dijital olarak alınsın mı?')
	acsleep             = fields.Many2one('info_tsm.p_sleep_suspend', string='Ac Güçte Uyku Periyodu',default =_get_default_sleep)
	acssuspend          = fields.Many2one('info_tsm.p_sleep_suspend', string='Ac Güçte Askıya Alma Periyodu',default=_get_default_suspend)
	btsleep             = fields.Many2one('info_tsm.p_sleep_suspend', string='Pilde Uyku Periyodu',default =_get_default_sleep)
	btssuspend          = fields.Many2one('info_tsm.p_sleep_suspend', string='Pilde Askıya Alma Periyodu',default=_get_default_suspend)
	verialmaipadresi    = fields.Char( string="Veri Alma IP Adresi");
	verilmaportno       = fields.Integer( string="Veri Alma Port No");
	resetgibparams      = fields.Boolean(string="Gib Parametrelerini Resetle?")
	gibcurrencymode     = fields.Boolean(string="Döviz Tutarı Gönderilsin mi?")
	rj45boardrev        = fields.Boolean(string="Yeni RJ45 Board?")
	modem3g             = fields.Boolean(string="3G Modem Kullan?")
	keybeep             = fields.Boolean(string="Tuş Sesi Aktif")
	keybeeptype         = fields.Selection(string="Tuş Sesi Tipi", selection=bip_tipleri)
	drawerkey           = fields.Boolean(string="Çekmece Tuşu")
	drawerkeypword      = fields.Boolean(string="Şifreli Çekmece Açılışı?")
	externalcomstatus   = fields.Boolean(string="Harici Cihaz Eşleştirmesi Açık mı?")
	quicksalemode       = fields.Boolean(string="Hızlı Satış Modu Aktif")
	salestypeallowedit  = fields.Selection( selection = [(1, 'TSM + Cihaz'),
				(2, 'Sadece TSM'),
				(3, 'Sadece Cihaz')
				], string="Cihazdan Satış ve Tahsilat Tipleri Değiştirilebilir mi?")

	default       = fields.Boolean(string='Varsayılan Parametreler.')
	partialpaydisabled  = fields.Boolean( string='Parçalı Ödemeyi Engelle',default=True)

	_defaults = {
		'keybeep':True,
		'keybeeptype':1
	}
	seperatedproc    = fields.Boolean(string='Ayrık İşlem İzni')
	@api.one
	@api.constrains('verialmaipadresi')
	def _check_ip_address(self):

		##print self.haberlesme_tip_adi
		ch_obj = {'verialmaipadresi':'Veri Alma IP Adresi'}
		foo = check( exceptions, self )
		foo.check_ip_no( ch_obj )

class terminal_parametre_history( models.Model ):
	_name ='info_tsm.parametre_history'

	terminal_id = fields.Many2one('stock.production.lot', string='YÖKC')
	parametre   = fields.Many2one('info_tsm.uygulama_parametreleri',string='Parametre')
	indirildi_mi  = fields.Boolean(string='Cihaza Yüklendi mi?', default=False)

class terminal_program_history( models.Model ):
	_name ='info_tsm.program_history'

	terminal_id   = fields.Many2one('stock.production.lot', string='YÖKC')
	version       = fields.Many2one('info_tsm.program_version',string='Program Versiyonu')
	indirildi_mi  = fields.Boolean(string='Cihaza Yüklendi mi?', default=False)

class program_tipleri( models.Model ):
	_name = 'info_tsm.program_type'

	name           = fields.Char( string='Program Tipi', required=True )
	kod            = fields.Char( string='Program Kodu', required=True )
	aciklama       = fields.Char( string='Açıklama')
	parent_id      = fields.Many2one('info_tsm.program_type', string='Ana Program Tipi', domain="[('parent_id','=', False)]")
	rqname         = fields.Char(string='RQueName', required=True, default="-")
	wqname         = fields.Char(string='WQueName', required=True, default="-")
	program_id     = fields.Integer(string='Program ID', required = True)
	header_uzunluk = fields.Integer(string='Başlık Uzunluğu (Byte) ')
	oncelik        = fields.Integer(string='InFlight Öncelik(Düşük Sayı Öncelikli)')
	online_oncelik = fields.Integer(string='Online Öncelik(Düşük Sayı Öncelikli)')
	
	_sql_constraints = [('oncelik_uniq', 'unique (oncelik)', 'Bu Program Öncelik numarası mevcut.'),]
	
	@api.multi
	def unlink(self):
		if not self.env.user.has_group('info_tsm.tsm_parameter_vers_delete'):
			raise exceptions.ValidationError('İşlemi Yapmaya Yetkiniz Yok.')
		return super( program_tipleri, self).unlink()
	
	@api.onchange('parent_id')
	def get_kod_by_parent ( self):
		if self.parent_id and self.parent_id.kod:
			self.kod = self.parent_id.kod
		else:
			self.kod = '00'
		if self.parent_id and self.parent_id.rqname != '-':
			self.rqname = self.parent_id.rqname
			self.wqname = self.parent_id.wqname
		else:
			self.rqname = "-"
			self.wqname = "-"

	@api.one
	@api.constrains('name')
	def _check_unique_insesitive_record(self):

		ch_obj = {'name':'Program Tipi'}
		foo = check( exceptions, self )
		foo.check_unique_bos( ch_obj )

	_sql_constraints = [('program_id_uniq', 'unique (program_id)', 'Bu Program ID numarası mevcut.'),]

class programs (models.Model ):
	_name = 'info_tsm.programs'

	name            = fields.Char( string = 'Program Adı', size=50, required=True)
	programfolder   = fields.Char( string = 'Program Klasörü')
	execpath        = fields.Char( string = 'Çalıştırma Yolu')
	programtype      = fields.Many2one( 'info_tsm.program_type', string='Ana Program Tipi')
	programtype_child = fields.Many2one( 'info_tsm.program_type', string='Program Tipi', domain="[('id','=',-1)]", required=True)
	remoteip        = fields.Char(string = 'Uzak Ip')
	remoteport      = fields.Integer(string = 'Uzak Port')
	sortorder       = fields.Integer(string = 'Sıra No', required = True )
	programhostip   = fields.Char( string='Program Host Ip')
	programhostport = fields.Integer( string='Program Host Port')
	status          = fields.Integer( string='Durum')
	isactive        = fields.Boolean(string ='Aktif', default=True )
	versionlar      = fields.One2many('info_tsm.program_version','program_id',string='Versiyonlar')
	
	@api.multi
	def unlink(self):
		if not self.env.user.has_group('info_tsm.tsm_parameter_vers_delete'):
			raise exceptions.ValidationError('İşlemi Yapmaya Yetkiniz Yok.')
		return super( programs, self).unlink()
	
	@api.onchange('programtype')
	def get_child_by_type ( self):
		if self.programtype:
			ch_list = self.env['info_tsm.program_type'].sudo().search([('parent_id','=',self.programtype.id)])

			if len(ch_list) == 0:
				ch_list = [self.programtype.id]
			else:
				ch_list = map( lambda x:x.id, ch_list )
			return {'domain':{'programtype_child': [('id', 'in', ch_list)]}}
	api.onchange('programtype_child')
	def get_prog_name_by_type(self):
		if self.programtype_child:
			self.name = self.programtype_child.name

class ek_versiyon_dosyalari (models.Model ):

	_name = 'info_tsm.ek_versiyon_dosyalari'
	name                = fields.Char('Dosya Adı')
	name_display        = fields.Char('Dosya Adı')
	dosya               = fields.Binary('Dosya Ekle',required=True)
	program_versiyon    = fields.Many2one('info_tsm.program_version', string= 'Ek Dosyalar',ondelete='restrict')
	path                = fields.Char( string='Önizleme',compute= '_get_link', store = True )
	file_location       = fields.Selection( selection=dosya_konumlari,string='Dosya Konumu',required=True )
	file_subfolder      = fields.Char('Dosya Alt Dizini')
	chunk_count         = fields.Integer('Chunk Sayısı')
	file_hash           = fields.Char('File Sha256 Hash')
	file_byte_len       = fields.Float(string = 'Dosya Boyutu')
	file_path           = fields.Char(string='Dosya Konumu')
	user_md5            = fields.Char(string='Kullanıcı Sha256 Hash')

	@api.one
	@api.depends('name')
	def _get_link( self ):
		param      = self.env["ir.config_parameter"]
		url_header = param.get_param('web.base.external_url')
		self.path  = url_header + '/web/binary/download_document?model=info_tsm.ek_versiyon_dosyalari&field=dosya&id=%s&filename=%s'%(self.id,self.name)


	@api.multi
	def prev_file(self):
		return {
			 'type' : 'ir.actions.act_url',
			 'url': '/web/binary/download_document?model=info_tsm.ek_versiyon_dosyalari&field=dosya&id=%s&filename=%s'%(self.id,self.name),
			 'target': 'new'
			 }
	'''
	@api.one
	@api.onchange('dosya','user_md5')
	def _on_change_dosya( self ):
		if self.user_md5 and self.dosya:

			self.name_display = 'Yüklenecek Dosya Seçildi'
			chunks,hexdi    = create_file_chunk( self.dosya,len(self.dosya))
			#print hexdi, self.user_md5
			if hexdi.upper() != self.user_md5.upper():
				self.dosya        = False
				self.name_display = False
				raise exceptions.ValidationError(u" Girilen Hash ile Hesaplanan Hash Eşleşmedi, İlgili Satırı Silerek Dosyayı tekrar yükleyiniz. ")
	'''
	@api.multi
	def unlink( self ):
		for s in self:
			sended    = self.env['info_tsm.sended_version_file_chunks'].search([('versiyon','=',s.id)])
			sended_h  = self.env['info_tsm.sended_version_file_history'].search([('versiyon','=',s.id)])
			chunks    = self.env['info_tsm.ek_dosya_file_chunks'].search([('program_version','=',s.id)])
			for send in sended:
				senf.unlink()
			for sh in sended_h:
				sh.unlink()
			for c in chunks:
				c.unlink()

		return super(ek_versiyon_dosyalari,self).unlink()

	@api.model
	def create( self, vals ):
		res = super(ek_versiyon_dosyalari,self ).create( vals )
		f            = vals['dosya']

		chunks = []
		if f:
			chunks,hexdi    = create_file_chunk( f,2048)
			if hexdi.upper() != res.user_md5.upper():
				res.unlink()
				raise exceptions.ValidationError(u" Girilen Hash ile Hesaplanan Hash Eşleşmedi,İlgili Satırı Silerek Dosyayı tekrar yükleyiniz. ")
			res.chunk_count = len( chunks )
			res.file_hash   = hexdi

			i = 1

			for c in chunks:
				cols = dict(program_version = res.id,
							chunk           = c,
							part_no         = i)
				i+=1

				self.env['info_tsm.ek_dosya_file_chunks'].sudo().create( cols )

		return res

class program_version( models.Model ):

	_name = 'info_tsm.program_version'


	name            = fields.Char( string ='Version Adı',required=True ,size=50)
	program_id      = fields.Many2one('info_tsm.programs', string = 'Program', required=True, size=50,ondelete='restrict')
	activatedate    = fields.Datetime(string='Aktivasyon Tarihi', required=True)
	description     = fields.Text(string='Açıklama')
	musttakezrep    = fields.Boolean(string='Z Raporu Cevabı?')
	mustsyncdata    = fields.Boolean(string='Data Senkronizasyonu?')
	major           = fields.Integer(string='Version No (Major)', required=True)
	minor           = fields.Integer(string='Version No (Minor)', required=True)
	build           = fields.Integer(string='Yapı No')
	revision        = fields.Integer(string='Revizyon No')
	version_file    = fields.Binary(string='Versiyon Dosyası')
	version_file_fname    = fields.Char(string='Orj Dosya Adı')
	version_file_parca_sayisi = fields.Integer( string='Versiyon Dosyası Parça Sayısı')
	isactive        = fields.Boolean(string='Aktif',default=True)
	md5             = fields.Char(string="Dosya Hash")
	dosyalari       = fields.One2many('info_tsm.ek_versiyon_dosyalari','program_versiyon')
	packet_byte_len = fields.Float(string = 'Paket dosya Boyutu')
	packet_path     = fields.Char(sring='Paket Konumu')
	program_ek_ozellikler = fields.One2many('info_tsm.program_ekozellik','name',string='Üçüncü Parti Özellikler')
	version_number_computed = fields.Char(string='Versiyon No', compute='_get_version_no' )
	group_id                = fields.Many2many('info_tsm.parametre_grup',string='Tanımlı Olduğu Gruplar',domain=[('terminal_id','!=',False)])
	extra_pass              = fields.Boolean('Silinmesi için Ekstra Şifre Sor')
	offline_mi              = fields.Boolean('Uygulama Offline mı?')
	listelere_ekle          = fields.Boolean('Onaylı Versiyon (Listelere Otomatik Eklenecek)', default=False)
	
	update_v_f               = fields.Many2one('info_tsm.program_version', string='Güncellenecek Versiyon')
	to_delete                = fields.Boolean()

	@api.one
	@api.depends( 'major', 'minor', 'build', 'revision')
	def _get_version_no( self ):
		self.version_number_computed =  '%s.%s.%s.%s' %( self.major, self.minor, self.build, self.revision)



	@api.multi
	def prog_grup_iliski_sil( self ):
		grup_id = self._context['active_id']
		if self.extra_pass:
			return dict(
					type      = 'ir.actions.act_window',
					name      = "Şifre Giriniz",
					res_model = "info_tsm.delete_v_pass",
					src_model = "info_tsm.parametre_grup",
					view_type = 'form',
					view_mode = 'form',
					target    = "new",
					context   = {'grup_id':grup_id}
				)
		

		self.env.cr.execute("""delete from info_tsm_parametre_grup_info_tsm_program_version_rel
									where info_tsm_parametre_grup_id=%s and info_tsm_program_version_id=%s
							""" % (grup_id, self.id))

		self.env.invalidate_all()

	def getDAtaFormat(self,data,uzunluk, soldan_tamamla = True):
		data = str( data )
		data_eksik = "0"*(uzunluk - len( data ))

		if soldan_tamamla:
			return data_eksik + data
		return data + data_eksik
	def toHexTrimmed(self,str_) :


		hex_ = '';
		if (str_):
			for s in str_:
				kod = hex(ord(s)).replace('0x','')
				if len(kod)%2 != 0: kod = '0' + kod;
				if kod.upper() == '0130': kod = 'DD'#İ
				if kod.upper() == '0131': kod = 'FD'#ı
				if kod.upper() == '011F': kod = 'F0'#ğ
				if kod.upper() == '015F': kod = 'FE'#ş
				if kod.upper() == '015E': kod = 'DE'#Ş
				if kod.upper() == '011E': kod = 'D0'#Ğ
				hex_ += kod

		return hex_;
	def getAppVerPaket( self, version ):

		major   = version.major
		minor   = version.minor
		build   = version.build
		revision = version.revision

		program_id = version.program_id.programtype_child.program_id
		prefix = "packet" #packet ne aq.
		zip_name   =  "%s_%s_%s_%s_%s_%s.zip"%(prefix,program_id,major,minor,build,revision)
		if os.path.exists( file_dir +'/'+zip_name ):
			os.remove ( file_dir +'/'+ zip_name )
		#print os.curdir
		appfileinf       = open(file_dir + '/app_ver.inf',mode='wb')


		program_type = self.getDAtaFormat(hex(int(version.program_id.programtype_child.kod)).replace('0x',''),2)
		rqname       = self.getDAtaFormat(self.toHexTrimmed ( version.program_id.programtype_child.rqname),50,False)
		wqname       = self.getDAtaFormat(self.toHexTrimmed ( version.program_id.programtype_child.wqname),50,False)

		execpath     = self.getDAtaFormat(self.toHexTrimmed (str(version.program_id.programfolder) + '\\' + str(version.program_id.execpath)),150,False)
		program_id_f = self.getDAtaFormat(int(program_id),4)
		progname     = self.getDAtaFormat(self.toHexTrimmed (version.program_id.name), 42,False)
		version_id   = self.getDAtaFormat (version.id,8)
		sortorder    = self.getDAtaFormat (version.program_id.sortorder,2)
		status       = self.getDAtaFormat (version.program_id.status,2)
		major_f      = self.getDAtaFormat( major, 4)
		minor_f      = self.getDAtaFormat( minor, 4)
		build_f      = self.getDAtaFormat( build, 4)
		revision_f   = self.getDAtaFormat( revision, 4)

		data_part1   = program_type + rqname + wqname + execpath + program_id_f + progname + version_id + sortorder + status + major_f + minor_f + build_f + revision_f

		proghost     = version.program_id.programhostip
		proghost_s   = proghost.split(".")
		proghost_f   = "".join( self.getDAtaFormat(x,3) for x in proghost_s ) + "000000000000"
		progport     = self.getDAtaFormat (version.program_id.programhostport,6)
		filecount    = self.getDAtaFormat (len(version.dosyalari),2)

		data_part1 += program_type + rqname + wqname + program_id_f + self.getDAtaFormat(self.toHexTrimmed(str(version.program_id.programfolder)), 150,False) + self.getDAtaFormat(self.toHexTrimmed(str(version.program_id.execpath)), 40,False) + major_f + minor_f + build_f+ revision_f+ proghost_f + progport + filecount

		zippacket   = zipfile.ZipFile( file_dir +'/'+zip_name, 'a')

		for file_obj in version.dosyalari:

			data_part1 +=self.getDAtaFormat (file_obj.id,8);
			data_part1 +=self.getDAtaFormat (file_obj.file_location - 1,2);
			data_part1 +=self.getDAtaFormat (self.toHexTrimmed(file_obj.file_subfolder), 58,False);
			data_part1 +=self.getDAtaFormat ( hex(file_obj.chunk_count).replace('0x',''),4);
			data_part1 +=str(file_obj.file_hash)


			inzipfilename    =  str(file_obj.id) + '.zip'
			inzipfile        =  open( file_dir + '/' + inzipfilename, 'wb' )

			byte = base64.b64decode( file_obj.dosya )
			inzipfile.write( byte )
			inzipfile.close()

			zippacket.write(file_dir +'/'+ inzipfilename, inzipfilename)
			#os.remove ( file_dir +'/'+ inzipfilename )
			file_obj.file_byte_len      = os.path.getsize( file_dir +'/'+inzipfilename )
			file_obj.file_path          = file_dir +'/'+inzipfilename

		appfileinf.write( bytearray.fromhex(data_part1.replace('0x','')))
		appfileinf.close()
		zippacket.write( file_dir +'/'+'app_ver.inf', 'app_ver.inf')

		zippacket.close()
		zipread = open(file_dir +'/'+ zip_name,'rb' )

		version.version_file            = base64.b64encode( zipread.read())
		version.version_file_fname      = zip_name
		zipread.close()
		os.remove( file_dir +'/'+'app_ver.inf' )
		#os.remove( file_dir +'/'+zip_name )

		version.packet_byte_len = os.path.getsize( file_dir +'/'+zip_name )
		version.packet_path     = file_dir +'/'+zip_name

		return True

	@api.multi
	def write( self, vals ):
		version =  super( program_version, self ).write( vals )
		if vals.has_key('dosyalari'):
			paketleme = self.getAppVerPaket( self )

			if   paketleme:
				chunks = []
				if self.version_file:
					chunks,hexdi                      = create_file_chunk( self.version_file,2048)
					self.version_file_parca_sayisi = len( chunks )
					self.md5  = hexdi

				i = 1

				onceki_chunks = self.env['info_tsm.version_file_chunks'].search([('program_version','=',self.id)])
				for o in onceki_chunks:
					o.unlink()
				for c in chunks:
					cols = dict(program_version = self.id,
								chunk           = c,
								part_no         = i)
					i+=1

					self.env['info_tsm.version_file_chunks'].sudo().create( cols )

		return version

	@api.model
	def create( self, vals ):
		version = super ( program_version, self).create( vals )

		#vals['name_display'] = vals['name']

		#loop içerisine al, dosyalar arasında gez.
		#write methodunu da override edeceğiz...
		other_vers = self.search([('program_id','=',version.program_id.id),('id','!=',version.id)],order='id desc')
		for o in other_vers:
			if o.program_ek_ozellikler:
				new_o = o.program_ek_ozellikler[0].copy({'name':version.id})
		
		paketleme = self.getAppVerPaket( version )

		if   paketleme:
			chunks = []
			if version.version_file:
				chunks,hexdi                      = create_file_chunk( version.version_file,2048)
				version.version_file_parca_sayisi = len( chunks )
				version.md5  = hexdi

			i = 1

			for c in chunks:
				cols = dict(program_version = version.id,
							chunk           = c,
							part_no         = i)
				i+=1

				self.env['info_tsm.version_file_chunks'].sudo().create( cols )

		return version
	
	@api.multi
	def update_v(self):
		self.ensure_one()
		prog_id  = self.program_id
		vers_ids = self.program_id.versionlar.ids
		vers_ids.remove( self.id )
		local_context = self.env.context.copy()
		local_context.update( {'vers_ids':vers_ids,'active_model':"info_tsm.vers_okc_wiz"} )
		
		return dict(
					type      = 'ir.actions.act_window',
					name      = "Versiyon Güncelle",
					res_model = "info_tsm.vers_okc_up_wiz",
					src_model = "info_tsm.vers_okc_wiz",
					view_type = 'form',
					view_mode        = 'form',
					target           = "new",
					context          = local_context
				)
	@api.multi
	def rem_delete_v( self ):
		self.ensure_one()
		if self.to_delete:
			self.to_delete = False
			return True
	@api.multi
	def delete_v( self ):
		self.ensure_one()
		if self.to_delete:
			self.to_delete = False
			return True
		if self.extra_pass:
			return dict(
					type      = 'ir.actions.act_window',
					name      = "Şifre Giriniz",
					res_model = "info_tsm.delete_v_pass",
					src_model = "info_tsm.vers_okc_wiz",
					view_type = 'form',
					view_mode = 'form',
					target    = "new",
				)
		else:
			return dict(
					type      = 'ir.actions.act_window',
					name      = "Şifre Giriniz",
					res_model = "info_tsm.delete_v_pass",
					src_model = "info_tsm.vers_okc_wiz",
					view_type = 'form',
					view_mode = 'form',
					target    = "new",
					context   = {'no_pass':True}
				)
			self.to_delete = True
			
	@api.multi
	def unlink(self):
		
		if not self.env.user.has_group('info_tsm.tsm_parameter_vers_delete'):
			raise exceptions.ValidationError('İşlemi Yapmaya Yetkiniz Yok.')
		
		for s in self:
			s.dosyalari.unlink()
			s.program_ek_ozellikler.unlink()
		
		return super(program_version,self ).unlink()    
		
class tsm_delete_v_pass ( models.TransientModel ):
	_name = 'info_tsm.delete_v_pass'

	passwd = fields.Char(string='Password :')
	@api.multi
	def sub(self):
		
		#print self._context
		
		v = self.env['info_tsm.program_version'].browse([ self._context.get('active_id')])
		if not v.extra_pass:
			if self.env.context.get('grup_id'):
				self.env.cr.execute("""delete from info_tsm_parametre_grup_info_tsm_program_version_rel
									where info_tsm_parametre_grup_id=%s and info_tsm_program_version_id=%s
							""" % (self.env.context.get('grup_id'), v.id))

				self.env.invalidate_all()
			else:
				v.to_delete = True
		else:
			if self.passwd == 'manager':
				self.passwd = ""
				if self.env.context.get('grup_id'):
					self.env.cr.execute("""delete from info_tsm_parametre_grup_info_tsm_program_version_rel
									where info_tsm_parametre_grup_id=%s and info_tsm_program_version_id=%s
							""" % (self.env.context.get('grup_id'), v.id))
					self.env.invalidate_all()
				else:
					v.to_delete = True
				
		self.passwd = ""
		return {'return':True, 'type':'ir.actions.act_window_close' }
	
class program_ek_ozellik( models.Model ):
	_name = 'info_tsm.program_ekozellik'

	name        		= fields.Many2one ( 'info_tsm.program_version', string='Program Versiyon Adı', required = True)
	start_date  		= fields.Date( string='Başlangıç Tarihi', required=True)
	dis_ip_iptal        = fields.Char( string='TSM İptal Bağlantı IP')
	dis_port_iptal      = fields.Float( string='TSM İptal Bağlantı Port',size=8,digits=(8,0))
	dis_ip_param        = fields.Char( string='TSM Parametre Bağlantı IP')
	dis_port_param      = fields.Float( string='TSM Parametre Bağlantı Port',size=8,digits=(8,0))
	gprsopenmode   		= fields.Boolean(string='GPRS Açık mı?')
	okcappissilent 		= fields.Boolean(string='Arka Planda Çalış?')
	foreignsales   		= fields.Boolean(string='Dış Satışlar?')
	flightmode     		= fields.Boolean(string='Uçuş Modu?')
	dis_ip_adresi  		= fields.Char( string='TSM Finansal Bağlantı IP ')
	dis_port       		= fields.Float( string='TSM Finansal Bağlantı Port ',size=8,digits=(8,0))
	partialpaydisabled 	= fields.Boolean( string='Parçalı Ödemeyi Engelle')
	@api.one
	@api.constrains('dis_ip_iptal','dis_ip_param','dis_ip_adresi' )
	def _check_ip_address(self):

		##print self.haberlesme_tip_adi
		ch_obj = {'dis_ip_iptal':'TSM İptal Bağlantı IP',
				  'dis_ip_param' :'TSM Parametre Bağlantı IP',
				  'dis_ip_adresi':'TSM Finansal Bağlantı IP'
				  }
		foo = check( exceptions, self )
		foo.check_ip_no( ch_obj )

class ek_dosya_file_chunks (models.Model):
	_name = 'info_tsm.ek_dosya_file_chunks'

	program_version = fields.Many2one('info_tsm.ek_versiyon_dosyalari', string='Program Versiyon',ondelete='cascade')
	chunk           = fields.Char(string='Version Parçası (1024 byte)', size=2048)
	part_no         = fields.Integer(string='Versiyon Parça No')

	_sql_constraints = [
		('chunk_part_unique', 'unique(program_version, part_no)', 'Versiyon Parça Numarası Tek Olmalıdır.'),
		]

class version_file_chunks (models.Model):
	_name = 'info_tsm.version_file_chunks'

	program_version = fields.Many2one('info_tsm.program_version', string='Program Versiyon',ondelete='cascade')
	chunk           = fields.Char(string='Version Parçası (1024 byte)', size=2048)
	part_no         = fields.Integer(string='Versiyon Parça No')

	_sql_constraints = [
		('chunk_part_unique', 'unique(program_version, part_no)', 'Versiyon Parça Numarası Tek Olmalıdır.'),
		]

class sended_version_file_chunks ( models.Model ):
	#transistent bu
	_name       = 'info_tsm.sended_version_file_chunks'
	terminal_id = fields.Many2one('stock.production.lot', string='YÖKC',ondelete='restrict')
	versiyon    = fields.Many2one('info_tsm.ek_versiyon_dosyalari',string='Program Dosyası',ondelete='cascade')
	chunk_id    = fields.Many2one('info_tsm.ek_dosya_file_chunks', string='Versiyon Parçası',ondelete='cascade')
	part_no     = fields.Integer( related = 'chunk_id.part_no', string="Parça No")
	status      = fields.Boolean( string='Gönderildi mi?')

class sended_version_file_history ( models.Model ):

	_name       = 'info_tsm.sended_version_file_history'
	terminal_id = fields.Many2one('stock.production.lot', string='YÖKC')
	versiyon    = fields.Many2one('info_tsm.ek_versiyon_dosyalari',string='Program Versiyonu')
	chunk_id    = fields.Many2one('info_tsm.ek_dosya_file_chunks', string='Versiyon Parçası')
	part_no     = fields.Integer( related = 'chunk_id.part_no', string="Parça No")#hangi partta kaldı?
	status      = fields.Char( string='Gönderildi mi?')

class silinen_kisimlar( models.Model):

	_name = 'info_tsm.silinen_kisimlar'

	grup_id = fields.Many2one('info_tsm.okc_urun_grup', string='Ökc Ürün Grup', required=True)
	ozel_kdv_rel     = fields.Many2one('info_tsm.ozel_kdv_tablosu', string='Özel KDV Grubu', required=True)
	
class parameter_group( models.Model ):

	_name = 'info_tsm.parametre_grup'

	@api.model
	def default_get(self,field_list):

		res              = super( parameter_group, self).default_get( field_list )
		env_col = 'default'
		default_uyg_params    = self.env['info_tsm.uygulama_parametreleri'].search([(env_col,'=',True)])
		res['parametre']      = len(default_uyg_params) > 0 and default_uyg_params[0].id
		default_ilk_iletisim  = self.env['info_tsm.ilk_iletisim_params'].search([(env_col,'=',True)])
		res['ilk_iletisim']   = len(default_ilk_iletisim) > 0 and default_ilk_iletisim[0].id
		default_haberlesme    = self.env['info_tsm.haberlesme'].search([(env_col,'=',True)])
		res['haberlesme']     = len(default_haberlesme) > 0 and default_haberlesme[0].id

		return res

	firma_id            = fields.Many2one( 'res_partner', string = 'Firma', domain=[('active','=',True),
																	   ('is_company','=',True),
																	   ('satilan_lots','!=',False)] )

	name                = fields.Char( string = 'Parametre Grup Adı', required = True)
	terminal_id         = fields.One2many('stock.production.lot', 'uygulama_params', string="Tanımlı Ökc'ler" )
	parametre           = fields.Many2one('info_tsm.uygulama_parametreleri', string="Uygulama Parametreleri")
	prog_version        = fields.Many2many('info_tsm.program_version' , string='Program Versiyonları')
	ilk_iletisim        = fields.Many2one('info_tsm.ilk_iletisim_params' , string='İlk İletişim Bilgileri')
	haberlesme          = fields.Many2one('info_tsm.haberlesme' , string='Haberleşme Bilgileri')
	default_param       = fields.Boolean(string='Prod Varsayılan Grup', default=False)
	test_default_param  = fields.Boolean(string='Test Varsayılan Grup', default=False)
	isactive            = fields.Boolean(string='Aktif',default=True)
	otomatik            = fields.Boolean(string="Otomatik Oluşmuş Grup")
	silinecek_uygulamalar = fields.Many2many('info_tsm.program_version',relation='info_tsm_grup_silinecek_uygulama')
	
	@api.model
	def create(self, values ):
		if values.has_key('default_param'):
			if values['default_param']:
				onceki_aktif = self.search([('default_param','=',True)])
				for o in onceki_aktif:
					o.default_param = False

		if values.has_key('test_default_param'):
			if values['test_default_param']:
				onceki_aktif = self.search([('test_default_param','=',True)])
				for o in onceki_aktif:
					o.test_default_param = False


		return super(parameter_group,self).create(values)
	@api.multi
	def write( self,values):
		if values.has_key('default_param'):
			if values['default_param']:
				onceki_aktif = self.search([('default_param','=',True)])
				for o in onceki_aktif:
					o.default_param = False

		if values.has_key('test_default_param'):
			if values['test_default_param']:
				onceki_aktif = self.search([('test_default_param','=',True)])
				for o in onceki_aktif:
					o.test_default_param = False

		return super( parameter_group, self).write( values )
	
	@api.multi
	def unlink(self):
		if not self.env.user.has_group('info_tsm.tsm_parameter_vers_delete'):
			raise exceptions.ValidationError('İşlemi Yapmaya Yetkiniz Yok.')
		return super( parameter_group, self).unlink()
	
class kur_altin_grup( models.Model ):

	_name = 'info_tsm.okc_kur_altin_grup'

	firma  = fields.Many2one( 'res.partner', string = 'Firma', domain=[('active','=',True),
																	   ('is_company','=',True),
																	   ('customer','=',True),
																	   ('satilan_lots','!=',False)],required=True )

	terminal_id         = fields.Many2one('stock.production.lot', 'Cihaz Seri No')

	#name                = fields.Char( string = 'Kur Bilgileri Grup Adı', required = True)
	terminal_id_disp    = fields.One2many('stock.production.lot', 'kur_params', string="Tanımlı Ökc'ler" )
	kur                 = fields.One2many('info_tsm.ozel_kur_group_rel','grup_id',   string='Özel Kur Oranları')
	altin               = fields.One2many('info_tsm.ozel_altin_group_rel','grup_id', string='Özel Altın Fiyatları')

	_sql_constraints = [
					 ('terminal_id_unique',
					  'unique(terminal_id)',
					  'Bu Seri No Zaten Eklenmiş!!!')
	]

	@api.onchange('firma')
	def _on_change_firma( self ):
		if self.firma:
			return dict ( domain = {'terminal_id': [('sold_to', '=', self.firma.id)]})
		else:
			return dict ( domain = {'terminal_id': []})

	@api.onchange('terminal_id')
	def _on_change_name( self ):
		if self.terminal_id:
			self.firma = self.terminal_id.sold_to.id

	@api.model
	def create( self, values ):
		donen = super(kur_altin_grup,self).create( values )
		#print values
		if not values['terminal_id']:
			firma_cihazlari = self.env['stock.production.lot'].search([('sold_to','=',values['firma'])])
			for cihaz in firma_cihazlari:
				if not cihaz.urun_tek_update_kur:
					cihaz.urun_tek_update_kur = False
					cihaz.kur_params       = donen.id
					#print cihaz.id, donen.id

		else:
			cihaz = self.env['stock.production.lot'].browse(values['terminal_id'])[0]
			cihaz.urun_tek_update_kur = True
			cihaz.kur_params       = donen.id
			#print cihaz.id, donen.id
		return donen


	@api.multi
	def unlink(self):
		for id_ in self:
			firma_cihazlari = self.env['stock.production.lot'].search([('kur_params','=',id_.id)])
			for kur in id_.kur:
				kur.unlink()
			for altin in id_.altin    :
				altin.unlink()

			for f in firma_cihazlari:
				f.kur_params = False
				f.urun_tek_update_kur = False

		return super(kur_altin_grup, self).unlink()

class senkron_info(models.Model):

	_name = 'info_tsm.senkron_info'

	name         = fields.Many2one('stock.production.lot', string="Tanımlı Ökc'ler" )
	grup_id      = fields.Many2one('info_tsm.okc_urun_grup', string='Ökc Ürün Grup')
	senkron      = fields.Boolean(string='Ürünler Senkron Mu')
	grup_senkron = fields.Boolean(string='Ürün Grupları Senkron Mu')

class urun_sablon( models.Model):

	_name = 'info_tsm.urun_sablon'
	name  = fields.Char()
	file_ = fields.Binary(string='Şablon Excel Dosya')
	
	@api.multi
	def get_sablon_indir(self):
		
		res = self.search([],order = 'id desc',limit=1)
		if res:
			return {
				 'type' : 'ir.actions.act_url',
				 'url': '/web/dl/info_tsm.urun_sablon/%s/file_/%s'%(res.id,res.name),
				 'target': 'self'
			}
		else:
			raise exceptions.ValidationError('Henüz Şablon Yüklenmemiş, Lütfen Yöneticinizden Şablon Yüklemesini İsteyiniz.')

class okc_urun_params(models.Model ):

	_name = 'info_tsm.okc_urun_grup'
	
	@api.multi
	def tumunu_sil_urun(self):
		self.ensure_one()
		self.urun.unlink()
	
	@api.multi
	def tumunu_sil_grup(self):
		self.ensure_one()
		self.ozel_urun_gruplari.unlink()
	
	@api.model
	def get_selection(self):
		selection = [('altinkaynak','Altınkaynak')]
		'''
		current_company = self.env.user.company_id
		services        = current_company.services_to_use
		for c in services:
			name = c.service
			for s in c._fields['service'].selection:
				if s[0] == c.service:
					name = s[1]
					break
			selection.append((str(c.id), name))
		'''
		return selection

	@api.model
	def get_cihaz_domain(self):
		#print 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'
		domain = [(1,'=',2)]
		if self._context.has_key('ids'):
			domain = [('id','in',self._context.get('ids'))]
			
		elif self._context.has_key('active_ids'):
			domain = [('id','in',self._context.get( 'active_ids' ))]
			cihaz  = self.env['stock.production.lot'].browse( self._context.get('active_ids') )
			res_id = cihaz.urun_params.sudo()
			if res_id:
				lot_ids = res_id.okc_list
				b = []
				for lot in lot_ids:
					if lot.id != int(self._context.get('active_ids')[0]):
						b.append( (3,lot.id) )
				
				res_id.okc_list = b

		return domain
	
	
	sablon_file     = fields.Binary()
	sablon_name     = fields.Char(default='urun_sablon.xlsx')
	
	servis_firmalar     = fields.Many2many('res.company',string='Servis Firmaları')
	okc_list            = fields.Many2many('stock.production.lot', string='YÖKC Terminaller',domain=get_cihaz_domain)
	firma               = fields.Many2one('res.partner', domain = [('active','=',True),
																	('is_company','=',True),
																	('customer','=',True),
																	('satilan_lots','!=',False)] )
	name                = fields.Char( string='Ürün Tanımlama Grubu Adı', required=True)
	varsayilan          = fields.Boolean(string='Bu Firma İçin Varsayılan Grup')
	terminal_id_disp    = fields.One2many('stock.production.lot', 'urun_params', string="Tanımlı Ökc'ler" )
	terminal_id_disp_name = fields.Char(string="Tanımlı Ökc'ler" ,compute='get_tanimli_okc')
	ozel_kdv            = fields.One2many('info_tsm.ozel_kdv_group_rel','grup_id', string='Özel KDV Oranları')
	ozel_urun_gruplari  = fields.One2many('info_tsm.ozel_urun_grup_group_rel','grup_id', string='Özel Ürün Grupları', domain=[('sil_ekle_disabled','=',False)] )
	urun                = fields.One2many('info_tsm.urun_grup_group_rel','grup_id', string='Cihaz Ürünleri')
	excel_dosya         = fields.Binary(string='Excel Dosyası Yükle')
	excel_dosya2        = fields.Binary(string='Excel Dosyası İndir')
	
	excel_file_name     = fields.Char(string='Excel Dosya Adı')

	kur                 = fields.One2many('info_tsm.ozel_kur_group_rel','grup_id',   string='Özel Kur Oranları')
	altin               = fields.One2many('info_tsm.ozel_altin_group_rel','grup_id', string='Özel Altın Fiyatları')
	tahsilat            = fields.One2many('info_tsm.tahsilat_tipi_group_rel','grup_id',   string='Tahsilat Tipleri')
	satis               = fields.One2many('info_tsm.satis_tipi_group_rel','grup_id', string='Satış Tipleri')
	fatura_tahsilat_kurum 	 = fields.One2many('info_tsm.fatura_tahsilat_kurum_group_rel','grup_id', string='Fatura Tahsilat Kurumları')
	senkron               	 = fields.One2many('info_tsm.senkron_info','grup_id', string='Cihaz Senkron Durumları')
	silinen_kisimlar      	 = fields.One2many('info_tsm.silinen_kisimlar','grup_id', string='Silinen Kısımlar')
	kur_altin_update_service = fields.Selection(selection=get_selection,string='Kullanılacak Güncelleme Servisi')
	
	fonk1                    = fields.One2many('info_tsm.fonksiyon_tuslari1','grup_id',string='Fonsiyon Tuşları')
	fonk2                    = fields.One2many('info_tsm.fonksiyon_tuslari2','grup_id2',string='Fonsiyon Tuşları')
	'''
	execute_fonks            = fields.Boolean(compute='_get_fonks')
	
	@api.multi
	def _comp_fonk1(self):
		print 'Compute fonk 1 aq'
		obj1 = self.env['info_tsm.fonksiyon_tuslari1']
		
		for s in self:
			if not s.fonk1:
				i   = 1
				ids  = []
				for f in fonk_tus1:
					d = {
						'name':f[0],
						'button_no_v':i,
						'button_no':i,
						'grup_id':s.id
					}
					nd = obj1.create( d )
					ids.append((4,nd.id))
					i+=1
				s.fonk1 = ids
				s.write({'fonk1':ids})
				
		
	
	@api.multi
	def _comp_fonk2(self):
		print 'Compute fonk 2 aq'
		obj2 = self.env['info_tsm.fonksiyon_tuslari2']
		for s in self:
			if not s.fonk2:
				i = 1
				ids = []
				for f in fonk_tus2:
					d = {
						'name2':f[0],
						'button_no_v2':i,
						'button_no2':i,
						'grup_id2':s.id
					}
					nd = obj2.create( d )
					ids.append((4,nd.id))
					i+=1
				s.fonk2 = ids
				s.write({'fonk2':ids})
			
	
	@api.multi
	def _get_fonks(self):
		for s in self:
			if s.execute_fonks:
				s.execute_fonks = False#bir onemi yok, yapmış olmak için yapıldı.
			else:
				s.execute_fonks = True
			s._comp_fonk1()
			s._comp_fonk2()
			
	'''		
	
	#info                 = fields.Text(string='İnfo')
	@api.depends( 'terminal_id_disp')
	@api.one
	def get_tanimli_okc(self ):
		hede_str = ""
		for ter in self.terminal_id_disp:
			hede_str += ter.name + " , "
		hede_str = hede_str[:len(hede_str) - 2]
		self.terminal_id_disp_name = hede_str

	@api.model
	def default_get_old(self, fields_list):
		res = super(okc_urun_params,self).default_get( fields_list )
		#print self._context
		
		print 'ALWAYS WORKS?'
		print self.id
		obj1 = self.env['info_tsm.fonksiyon_tuslari1']
		i = 1
		dws = []
		for f in fonk_tus1:
			d = {
				'name':f[0],
				'button_no_v':i,
				'button_no':i,
				'grup_id':self.id
			}
			#dw = obj1.create( d )
			#dws.append((4,dw.id))
			dws.append((0,0,d))
			i+=1
		res['fonk1'] = dws
		
		obj2 = self.env['info_tsm.fonksiyon_tuslari2']
		i = 1
		dws = []
		for f in fonk_tus2:
			d = {
				'name2':f[0],
				'button_no_v2':i,
				'button_no':i,
				'grup_id2':self.id
			}
			#dw = obj2.create( d )
			#dws.append((4,dw.id))
			dws.append((0,0,d))
			i+=1
		res['fonk2'] = dws
		
		device_ids = []
		if self._context.get('active_model') == u'stock.production.lot':
			if self._context.has_key('ids'):
				device_ids = self._context.get ('ids')
			elif self._context.get('active_ids'):
				device_ids = self._context.get('active_ids')

			if device_ids:
				res['okc_list'] = device_ids
				#print 'b',device_ids
				devices = self.env['stock.production.lot'].browse( device_ids )
				#print devices
				if len( device_ids ) == 1:
					#print devices[0].sudo().name
					res['name']     = devices[0].name
				else:
					res['name']     = 'dummy'
				#print res['name']
				res['firma']        = devices[0].sold_to.id
				res['varsayilan']   = True
				#print 'bbbbbbbbbb'

		if len( device_ids ) == 1 or self._context.get('active_model') != u'stock.production.lot':
			#print 'Firts IF'
			kur_objs     = self.env['info_tsm.kur_bilgileri'].search([('dafault','=',True)])
			
			#print kur_objs
			
			relation_kur = self.env['info_tsm.ozel_kur_group_rel']
			dws = []
			button_no = 1
			for kur in kur_objs:
				dw = relation_kur.create({
					'button_no': button_no,
					'ozel_kur_rel': kur.id,
					'grup_id': self.id,
					'kur_cevrimi': 0,
					'webden_guncelle': True,
				})
				dws.append((4,dw.id))
				button_no += 1
			#print 'a ',dws
			res['kur'] = dws

			altin_objs     = self.env['info_tsm.altin_kodlari'].search([('dafault','=',True)])
			relation_altin = self.env['info_tsm.ozel_altin_group_rel']
			dws = []
			button_no = 1
			for altin in altin_objs:
				dw = relation_altin.create({
					'button_no': button_no,
					'ozel_altin_rel': altin.id,
					'grup_id': self.id,
					'alis_fiyati': 0,
					'satis_fiyati': 0,
					'webden_guncelle': True,
				})
				dws.append((4,dw.id))
				button_no += 1
			#print 'b ',dws
			res['altin'] = dws

			satis_objs     = self.env['info_tsm.satis_tipi_islem_kodu_tipleri'].search([('dafault','=',True)])
			relation_satis = self.env['info_tsm.satis_tipi_group_rel']
			dws = []
			button_no = 1
			for sat in satis_objs:
				dw = relation_satis.create({
					'button_no': button_no,
					'satis_tipi_islem_rel': sat.id,
					'grup_id': self.id
				})
				dws.append((4,dw.id))
				button_no += 1
			#print 'c ',dws
			res['satis'] = dws

			tahsilat_objs     = self.env['info_tsm.tahsilat_tipleri'].search([('dafault','=',True)])
			relation_tahsilat = self.env['info_tsm.tahsilat_tipi_group_rel']
			dws = []
			button_no = 1
			for tah in tahsilat_objs:
				dw = relation_tahsilat.create({
					'button_no': button_no,
					'tahsilat_rel': tah.id,
					'grup_id': self.id
				})
				dws.append((4,dw.id))
				button_no += 1
			#print 'd ',dws
			res['tahsilat'] = dws

			fatura_tahsilat_objs     = self.env['info_tsm.fatura_tahsilat_kurum_tipleri'].search([('dafault','=',True)])
			relation_fatura_tahsilat = self.env['info_tsm.fatura_tahsilat_kurum_group_rel']
			dws = []
			button_no = 1
			for tah in fatura_tahsilat_objs:
				dw = relation_fatura_tahsilat.create({
					'button_no': button_no,
					'tahsilat_kurum_rel': tah.id,
					'grup_id': self.id,
					'kurum_kodu':button_no
				})
				dws.append((4,dw.id))
				button_no += 1
			#print 'e ',dws
			res['fatura_tahsilat_kurum'] = dws
		#print 'Default get res : ', res
		return res
	@api.model
	def default_get(self, fields_list):
		res = super(okc_urun_params,self).default_get( fields_list )
		#print self._context
		
		print 'ALWAYS WORKS?'
		print self.id
		obj1 = self.env['info_tsm.fonksiyon_tuslari1']
		i = 1
		dws = []
		for f in fonk_tus1:
			d = {
				'name':f[0],
				'button_no_v':i,
				'button_no':i,
				'grup_id':self.id
			}
			#dw = obj1.create( d )
			#dws.append((4,dw.id))
			dws.append((0,0,d))
			i+=1
		res['fonk1'] = dws
		
		obj2 = self.env['info_tsm.fonksiyon_tuslari2']
		i = 1
		dws = []
		for f in fonk_tus2:
			d = {
				'name2':f[0],
				'button_no_v2':i,
				'button_no':i,
				'grup_id2':self.id
			}
			#dw = obj2.create( d )
			#dws.append((4,dw.id))
			dws.append((0,0,d))
			i+=1
		res['fonk2'] = dws
		
		device_ids = []
		if self._context.get('active_model') == u'stock.production.lot':
			if self._context.has_key('ids'):
				device_ids = self._context.get ('ids')
			elif self._context.get('active_ids'):
				device_ids = self._context.get('active_ids')

			if device_ids:
				res['okc_list'] = device_ids
				#print 'b',device_ids
				devices = self.env['stock.production.lot'].browse( device_ids )
				#print devices
				if len( device_ids ) == 1:
					#print devices[0].sudo().name
					res['name']     = devices[0].name
				else:
					res['name']     = 'dummy'
				#print res['name']
				res['firma']        = devices[0].sold_to.id
				res['varsayilan']   = True
				#print 'bbbbbbbbbb'

		
		return res
	@api.model
	def get_defaults(self):
		res = {}
		kur_objs     = self.env['info_tsm.kur_bilgileri'].search([('dafault','=',True)])
		relation_kur = self.env['info_tsm.ozel_kur_group_rel']
		dws = []
		button_no = 1
		for kur in kur_objs:
			dw  = {
				'button_no': button_no,
				'ozel_kur_rel': kur.id,
				'grup_id': self.id,
				'kur_cevrimi': 0,
				'webden_guncelle': True,
			}
			dws.append([0,0,dw])
			button_no += 1
		#print 'a ',dws
		res['kur'] = dws

		altin_objs     = self.env['info_tsm.altin_kodlari'].search([('dafault','=',True)])
		relation_altin = self.env['info_tsm.ozel_altin_group_rel']
		dws = []
		button_no = 1
		for altin in altin_objs:
			dw = {
				'button_no': button_no,
				'ozel_altin_rel': altin.id,
				'grup_id': self.id,
				'alis_fiyati': 0,
				'satis_fiyati': 0,
				'webden_guncelle': True,
			}
			dws.append([0,0,dw])
			button_no += 1
		#print 'b ',dws
		res['altin'] = dws

		satis_objs     = self.env['info_tsm.satis_tipi_islem_kodu_tipleri'].search([('dafault','=',True)])
		relation_satis = self.env['info_tsm.satis_tipi_group_rel']
		dws = []
		button_no = 1
		for sat in satis_objs:
			dw  = {
				'button_no': button_no,
				'satis_tipi_islem_rel': sat.id,
				'grup_id': self.id
			}
			dws.append([0,0,dw])
			button_no += 1
		#print 'c ',dws
		res['satis'] = dws

		tahsilat_objs     = self.env['info_tsm.tahsilat_tipleri'].search([('dafault','=',True)])
		relation_tahsilat = self.env['info_tsm.tahsilat_tipi_group_rel']
		dws = []
		button_no = 1
		for tah in tahsilat_objs:
			dw = {
				'button_no': button_no,
				'tahsilat_rel': tah.id,
				'grup_id': self.id
			}
			dws.append([0,0,dw])
			button_no += 1
		#print 'd ',dws
		res['tahsilat'] = dws

		fatura_tahsilat_objs     = self.env['info_tsm.fatura_tahsilat_kurum_tipleri'].search([('dafault','=',True)])
		relation_fatura_tahsilat = self.env['info_tsm.fatura_tahsilat_kurum_group_rel']
		dws = []
		button_no = 1
		for tah in fatura_tahsilat_objs:
			dw = {
				'button_no': button_no,
				'tahsilat_kurum_rel': tah.id,
				'grup_id': self.id,
				'kurum_kodu':button_no
			}
			dws.append([0,0,dw])
			button_no += 1
		#print 'e ',dws
		res['fatura_tahsilat_kurum'] = dws
		print 'Defaults Res : ',res
		return res
	
	@api.onchange('excel_file_name')
	def _onchange_excel_dosya(self ):
		res = {}
		if self.excel_file_name:
			suffix = self.excel_file_name.split ('.')
			suffix = suffix[ -1 ]
			if suffix.upper() == 'XLSX' or suffix.upper() == 'XLS':
				pass
			else:
				res['warning'] = {'title': 'Uyarı', 'message': 'Sadece Excel Dosyası Yükleyebilirsiniz. Diğer Dosyalar Formatlanmayacaktır.'}
				res['value'].update({'excel_dosya': False, 'excel_file_name': False})
		return res

	def get_excel_parse_res(self, donen, method='insert'):
		company_id        = self.env.user.company_id.id
		urun_gruplari     = self.env['info_tsm.okc_ozel_urun_grubu_tablosu'].search([('company_id','=',company_id)])
		urun_grup_map     = {}
		urun_grup_mapper  = map( lambda x:urun_grup_map.update ({x.name:x.id}), urun_gruplari )

		birim_tipleri_map    = {}
		birim_tipleri_mapper = map( lambda x:birim_tipleri_map.update({x[1].decode('utf-8'):x[0]}), birim_tipleri)

		kisim_adlari        = self.env['info_tsm.ozel_kdv_tablosu'].search([('company_id','=',company_id)])
		#print kisim_adlari
		kisim_adlari_map = dict(((x.name,x.kdv_orani,x.unit_type), x) for x in kisim_adlari)
		
		print kisim_adlari_map
		##print urun_grup_map
		##print birim_tipleri_map

		excel_file = base64.b64decode( donen.excel_dosya)
		book  = xlrd.open_workbook(file_contents=excel_file ,encoding_override= 'utf-8' )
		deep_sheet  = book.sheets()[0]
		deep_rows   = deep_sheet.get_rows()#generator bu so yield is in action !!! cool.
		gr_sheet    = book.sheets()[1]
		gr_rows     = gr_sheet.get_rows()
		sheet       = book.sheets()[2]
		rows        = sheet.get_rows()#generator bu so yield is in action !!! cool.
		i           = 0
		
		grup_kisim_map = {}
		
		def get_kdv_index_birim_tip ( kdv_adi,kisim_birim ):
			return default_KDV.get( kdv_adi ), birim_tipleri_map.get( kisim_birim )
		
		for dr in deep_rows:
			if i > 0 and i < 13 :
				kisim_adi   = dr[0].value
				kdv_adi     = dr[1].value
				kisim_birim = dr[2].value
				kisim_limit = dr[3].value
				
				if kisim_adi and kisim_birim and (kdv_adi or kdv_adi==0):
					kdv_indexi, birim_tipi = get_kdv_index_birim_tip(kdv_adi,kisim_birim  )

					if kdv_indexi and birim_tipi:
						if not kisim_adlari_map.has_key( (kisim_adi,kdv_indexi,birim_tipi) ):
							new_kisim_dict = dict( name       = kisim_adi,
												   kdv_orani  = kdv_indexi,
												   unit_type  = birim_tipi,
												   company_id = company_id)
							
							new_kisim           = self.env['info_tsm.ozel_kdv_tablosu'].create( new_kisim_dict )
							kisim_adlari_map.update({(kisim_adi,kdv_indexi,birim_tipi):new_kisim})


						kisim_rel_eslesen         = filter(lambda x:x if x.ozel_kdv_rel.name == unicode(kisim_adi)
														   and int(x.ozel_kdv_rel.kdv_orani) == int(kdv_indexi)
														   and x.ozel_kdv_rel.unit_type      == int(birim_tipi)
														   else None,donen.ozel_kdv)

						if len( kisim_rel_eslesen ) > 0:
							kisim_rel_dict      = dict(
													   max_satis_limiti = kisim_limit
													   )
							kisim_rel_eslesen[0].write( kisim_rel_dict )
							kisim_adlari_map.update({(kisim_rel_eslesen[0].ozel_kdv_rel.name,kisim_rel_eslesen[0].ozel_kdv_rel.kdv_orani,kisim_rel_eslesen[0].ozel_kdv_rel.unit_type):kisim_rel_eslesen[0].ozel_kdv_rel})

						else:
							kisim_button_no           = 1 + len( donen.ozel_kdv )
							
							if kisim_button_no > 12:
								raise exceptions.ValidationError('Departman Kaydedilemedi !! En Fazla 12 Adet Farklı Departman Eklenebilir.')
								break
							
							kisim_rel_dict      = dict(ozel_kdv_rel = kisim_adlari_map[(kisim_adi,kdv_indexi,birim_tipi)].id,
													   grup_id      = donen.id,
													   button_no    = kisim_button_no,
													   company_id   = company_id,
													   max_satis_limiti = kisim_limit
													   )
							self.env['info_tsm.ozel_kdv_group_rel'].create( kisim_rel_dict )

					else:
						raise exceptions.ValidationError('Departman Kaydedilemedi !! Verileri Eksiksiz Girdiğinizden Emin Olunuz.')
						break

			i+=1
		i = 0
		for gr in gr_rows:
			if i>0:
				
				gr_dep_name = gr[0].value
				grup_adi    = gr[1].value
				if gr_dep_name and grup_adi:
					
					if i > 18:
						raise exceptions.ValidationError('Ürün Grubu Kaydedilemedi !! En Fazla 18 Adet Farklı Ürün Grubu Eklenebilir.')
						break
					
					gr_dep_name_splited = gr_dep_name.split(',')
					gr_dep_name_splited.reverse()
					con_pars    =  gr_dep_name_splited[0]
					
					gr_dep_name = ','.join(gr_dep_name_splited[1:])
					
					kdv_part    =  con_pars.split(':')
					kdv_name    =  kdv_part[0].replace(' KDV ','')
					birim_name  =  kdv_part[1]
					
					kdv_indexi, birim_tipi = get_kdv_index_birim_tip(int(kdv_name.strip()), birim_name.strip() )
					gr_dep_name_           = unicode(gr_dep_name.strip())
					
					kisim = kisim_adlari_map.get( (gr_dep_name_,kdv_indexi,birim_tipi) )
					
					if kisim:
						grup_kisim_map[grup_adi] = [kisim]
						if urun_grup_map.has_key( grup_adi ):
							urun_grup = urun_grup_map[ grup_adi ]
							##print 'Eşleşti Ürün Grup: ',urun_grup
						else:
							new_urun_grup_dict = dict( name = grup_adi)

							new_urun_grup      = self.env['info_tsm.okc_ozel_urun_grubu_tablosu'].create( new_urun_grup_dict )
							urun_grup          = new_urun_grup.id
							urun_grup_map.update({grup_adi:urun_grup})

						grup_button_no            = 1 + len( donen.ozel_urun_gruplari )
						grup_rel_eslesen          = filter(lambda x:x if x.ozel_urun_rel.id == urun_grup else None,donen.ozel_urun_gruplari)
						if len( grup_rel_eslesen ) > 0:
							grup_rel_dict      = dict(
													   ozel_kdv_rel = kisim.id,
													   change = True
													   )
							grup_rel_eslesen[0].write( grup_rel_dict
													  )

						else:
							grup_rel_dict      = dict( ozel_urun_rel = urun_grup,
													   ozel_kdv_rel = kisim.id,
													   grup_id      = donen.id,
													   button_no    = grup_button_no,
													   company_id   = company_id,
													   change = True
													   )
							self.env['info_tsm.ozel_urun_grup_group_rel'].create( grup_rel_dict )
					else:
						raise exceptions.ValidationError('Ürün Grupları Kaydedilemedi !! Verileri Eksiksiz Girdiğinizden Emin Olunuz.')
						break
			i+=1

		i           = 0
		for r in rows:#her bir urun icin bizde var mi diye check etmek lazım pöff, gruplar için dict yapıp, aynı keyler için db ye gitmeyelim.
			error_res = {}
			#print r

			if i!=0:#first row is header so skip this row. skibbe bıraktı!!!

				grup_adi    = r[0].value
				urun_adi    = r[1].value
				barkod_no   = r[2].value
				plu_no      = r[3].value
				birim_adi   = r[4].value
				urun_tutari = r[5].value

				birim_tipi = None

				if birim_tipleri_map.has_key( birim_adi ):
					birim_tipi = birim_tipleri_map[ birim_adi ]
					##print 'Eşleşti Birim Tipi: ', birim_tipi

				if grup_adi and urun_adi and birim_adi and urun_tutari:
					kisim = grup_kisim_map.get( grup_adi )
					print 'aaa : ',kisim, birim_tipi
					if kisim and birim_tipi:

						##print urun_adi, barkod_no, plu_no, kdv_indexi, birim_tipi, urun_tutari, urun_grup
						if urun_adi and urun_tutari:
							urun_res = self.env['info_tsm.urun_listesi'].search([('name','=ilike',urun_adi),('company_id','=',company_id)])
							##print urun_res, urun_adi

							if len( urun_res ) > 0:
								urun = urun_res[-1]#son eslesen
							else:
								if barkod_no:
									barkod_no = repr( barkod_no ).split(".")[0].strip(' \t\n\r').replace('\'','')
								urun_params = dict( name        = urun_adi,
													barkod_no   = barkod_no,
													company_id   = company_id,)

								urun = self.env['info_tsm.urun_listesi'].sudo().create ( urun_params )

							if urun:
								if plu_no:
									plu_no = repr( plu_no ).split(".")[0].strip(' \t\n\r').replace('\'','')
								else:
									plu_no = None
								urun_grup    = urun_grup_map[ grup_adi ]
								res_urun_rel = dict(urun_rel = urun.id,
												plu_no   = plu_no,
												grup_id  = donen.id,
												urun_grup_id = urun_grup,
												birim_tipi = birim_tipi,
												tutar      = urun_tutari,
												line_number = i,
												company_id   = company_id,
												change = True)
								print method
								if method == 'insert':
									try:
										self.env['info_tsm.urun_grup_group_rel'].sudo().create( res_urun_rel )
									except:

										##print 'insert EXCEPT'
										raise exceptions.ValidationError('Ürün Kaydedilemedi !! Aynı Üründen Mükerrer Girmediğinize Emin Olunuz.')
										break
										error_res = dict(firma = donen.firma.id,
											 name  = donen.name,
											 urun_adi = urun_adi,
											 barkod_no = barkod_no,
											 plu_no = plu_no,
											 kdv    = r[3].value,
											 birim_tipi = r[4].value,
											 urun_grubu = r[6].value,
											 satir_no = i,
											 tutar = urun_tutari)
								else:
									up = self.env['info_tsm.urun_grup_group_rel'].search([('grup_id','=',donen.id),('urun_rel','=',urun.id)])
									if len( up ) > 0:
										try:
											up[0].sudo().write( res_urun_rel )
										except:

											##print 'update EXCEPT'
											raise exceptions.ValidationError('Ürün Kaydedilemedi !! Aynı Üründen Mükerrer Girmediğinize Emin Olunuz.')
											break
											error_res = dict(firma = donen.firma.id,
											 name  = donen.name,
											 urun_adi = urun_adi,
											 barkod_no = barkod_no,
											 plu_no = plu_no,
											 kdv    = r[3].value,
											 birim_tipi = r[4].value,
											 urun_grubu = r[6].value,
											 satir_no = i,
											 tutar = urun_tutari)
									else:
										try:
											self.env['info_tsm.urun_grup_group_rel'].sudo().create( res_urun_rel )
										except:

											##print 'update insert EXCEPT'
											raise exceptions.ValidationError('Ürün Kaydedilemedi !! Aynı Üründen Mükerrer Girmediğinize Emin Olunuz.')
											break
											error_res = dict(firma = donen.firma.id,
											 name  = donen.name,
											 urun_adi = urun_adi,
											 barkod_no = barkod_no,
											 plu_no = plu_no,
											 kdv    = r[3].value,
											 birim_tipi = r[4].value,
											 urun_grubu = r[6].value,
											 satir_no = i,
											 tutar = urun_tutari)
					else:
						pass
					'''
					if error_res:
						self.env['info_tsm.hatali_excel_satirlari'].sudo().create( error_res )
					'''
			i += 1

	@api.model
	def create( self, values ):
		print 'Create New Urun Params'
		if values.has_key('urun') and values['urun']:
			for u in values['urun']:
				u[2].update({'change':True})
		if values.has_key('ozel_urun_gruplari') and values['ozel_urun_gruplari']:
			for u in values['ozel_urun_gruplari']:
				u[2].update({'change':True})
		if values.get('excel_dosya'):
			values['excel_dosya2'] = values.get('excel_dosya')
		donen = super(okc_urun_params,self).create( values )
		
		#print 'Donen executed'
		donen = donen.sudo()
		if donen.varsayilan and donen.firma:
			donen.firma.default_tanim = donen.id
		#print 'Donen executed 2'
		'''
		firma_cihazlari = self.env['stock.production.lot'].search([('sold_to','=',values['firma']),('urun_params','=',False)])
		for cihaz in firma_cihazlari:
			if not cihaz.urun_params:
				cihaz.urun_params       = donen.id
		'''
		#default grup_append
		default_grup = self.env['info_tsm.okc_ozel_urun_grubu_tablosu'].search([('name','=','Diğer'),('company_id','=',self.env.user.sudo().company_id.id)])
		#print 'Donen executed '
		if len( default_grup ) > 0:
			default_group_id = default_grup[0].id
		else:
			#default grup silinmiş, tekrar yarat
			#gr_kodu = self.env['info_tsm.urun_gruplari'].search([('name','=','0000')])[0].id
			default_grup_res  = dict ( name = 'Diğer',
									  kdv_orani = 4,
									  #grkodu = gr_kodu
									  )
			donen_default_grup = self.env['info_tsm.okc_ozel_urun_grubu_tablosu'].sudo().create( default_grup_res )
			default_group_id   = donen_default_grup.id

		new_rel = dict( ozel_urun_rel  = default_group_id,
						grup_id        = donen.id,
						button_no      = 99,
						ozel_kdv_rel   = self.env['info_tsm.ozel_kdv_tablosu'].search([],limit=1).id,
						sil_ekle_disabled = True,
						change         = True
						)

		self.env ['info_tsm.ozel_urun_grup_group_rel'].sudo().create( new_rel )
		
		defaults  = self.get_defaults()
		for k,v in defaults.iteritems():
			for r in v:
				if k == 'satis':
					ss = map( lambda x:x.satis_tipi_islem_rel.id, getattr( donen, k))
					if r[2]['satis_tipi_islem_rel'] not in ss:
						donen.write( {k:v })
				elif k == 'kur':
					ss = map( lambda x:x.ozel_kur_rel.id, getattr( donen, k))
					if r[2]['ozel_kur_rel'] not in ss:
						donen.write( {k:v })
				elif k == 'altin':
					ss = map( lambda x:x.ozel_altin_rel.id, getattr( donen, k))
					if r[2]['ozel_altin_rel'] not in ss:
						donen.write( {k:v })
				elif k == 'tahsilat':
					ss = map( lambda x:x.tahsilat_rel.id, getattr( donen, k))
					if r[2]['tahsilat_rel'] not in ss:
						donen.write( {k:v })
				elif k == 'fatura_tahsilat_kurum':
					ss = map( lambda x:x.tahsilat_kurum_rel.id, getattr( donen, k))
					if r[2]['tahsilat_kurum_rel'] not in ss:
						donen.write( {k:v })

		if donen.excel_file_name and donen.excel_dosya:
			if donen.excel_file_name.split('.')[-1].upper() == 'XLSX' or donen.excel_file_name.split('.')[-1].upper() == 'XLS':
				print 'Özel KDV : ', donen.ozel_kdv.mapped('ozel_kdv_rel')
				print 'Excel Parse Create'
				try:
					self.get_excel_parse_res(donen)
				except:
					_logger.error('Excel Şablon Parse Hatası')
		return donen

	@api.multi
	def write(self,values ):
		print 'Update New Urun Params'
		print values
		if values.get( 'urun' ):
			for u in values['urun']:
				if len(u) > 2:
					if u[2]:
						u[2].update({'change':True})
		if values.get( 'ozel_urun_gruplari' ):
			for u in values['ozel_urun_gruplari']:
				if len(u) > 2:
					if u[2]:
						u[2].update({'change':True})
		
		if values.get('excel_dosya'):
			values['excel_dosya2'] = values.get('excel_dosya')
		
		donen = super( okc_urun_params,self ).write( values )

		if values.has_key('varsayilan') and self.firma:
			self.firma.default_tanim = self.id

		if values.get('excel_dosya') :
			if self.excel_file_name.split('.')[-1].upper() == 'XLSX' or self.excel_file_name.split('.')[-1].upper() == 'XLS':
				print 'Excel Parse Write'
				self.get_excel_parse_res(self,'update')
		return donen

	@api.multi
	def unlink(self):
		for id_ in self:
			firma_cihazlari = self.env['stock.production.lot'].search([('urun_params','=',id_.id)])
			for ozel_kdv in id_.ozel_kdv:
				ozel_kdv.unlink()
			for u in id_.urun:
				u.unlink()
			for ozel_urun_grup in id_.ozel_urun_gruplari :
				ozel_urun_grup.unlink()
			for sat in id_.satis:
				sat.unlink()
			for k in id_.kur:
				k.unlink()
			for t in id_.tahsilat:
				t.unlink()
			for a in id_.altin:
				a.unlink()
			for kurm in id_.fatura_tahsilat_kurum:
				kurm.unlink()

			for f in firma_cihazlari:
				f.urun_params = False
				f.urun_tek_update = False
			for sk in id_.silinen_kisimlar:
				sk.unlink()

		return super(okc_urun_params, self).unlink()

	def get_new_grup_obj(self,lot_id,firma=False):


		#print lot_id.name
		#print self.kur_altin_update_service


		new_obj_dict = dict(okc_list                 = [(4,lot_id.id)],
							firma                    = firma or self.firma.id,
							name                     = lot_id.name,
							varsayilan               = self.varsayilan,
							kur_altin_update_service = self.kur_altin_update_service
							)
		#print new_obj_dict
		
		new_obj     = self.create ( new_obj_dict )
		
		#print 'Yeni Grup : ',new_obj

		for ozel_kdv in self.ozel_kdv:
			ozel_kdv.copy(dict(grup_id=new_obj.id))
		for ozel_urun_gruplari in self.ozel_urun_gruplari:
			ozel_urun_gruplari.copy(dict(grup_id=new_obj.id))
		for urun in self.urun:
			urun.copy(dict(grup_id=new_obj.id))
		for kur in self.kur:
			kur.copy(dict(grup_id=new_obj.id))
		for altin in self.altin:
			altin.copy(dict(grup_id=new_obj.id))
		for tahsilat in self.tahsilat:
			tahsilat.copy(dict(grup_id=new_obj.id))
		for satis in self.satis:
			satis.copy(dict(grup_id=new_obj.id))
		for fatura_tahsilat_kurum in self.fatura_tahsilat_kurum:
			fatura_tahsilat_kurum.copy(dict(grup_id=new_obj.id))
		return new_obj

	@api.multi
	def okc_urun_grup_set( self ):
		self.ensure_one()
		print 'aaaaaaaaaaaaaaaaaa'
		print self._context
		total_errors = {}
		if len(self.okc_list) == 1:
			self.okc_list[0].urun_params = self.id
		else:
			for lot_id in self.okc_list:
				if not lot_id.urun_params:
					#print 'Copying.....'
					new_grup_obj = self.get_new_grup_obj( lot_id )
					lot_id.urun_params  = new_grup_obj.id
				else:
					
					print 'Burdaaaaa'
					
					error_msg = ""
					for ozel_kdv in self.ozel_kdv:
						if ozel_kdv.ozel_kdv_rel.name not in map(lambda x:x.ozel_kdv_rel.name,lot_id.urun_params.ozel_kdv):
							if len(lot_id.urun_params.ozel_kdv) >= 12:
								error_msg += ozel_kdv.ozel_kdv_rel.name + u' Adlı KDV Departmanı Toplam Departman Adet 12\'yi geçtiği için eklenemedi \n'
							else:
								ozel_kdv.copy(dict(button_no   = len(lot_id.urun_params.ozel_kdv)+1,
												   button_no_v = len(lot_id.urun_params.ozel_kdv)+1,
												   grup_id     = lot_id.urun_params.id))

					for ozel_urun_grup in self.ozel_urun_gruplari:
						if ozel_urun_grup.ozel_urun_rel.id not in map(lambda x:x.ozel_urun_rel.id,lot_id.urun_params.ozel_urun_gruplari):
							
							if ozel_urun_grup.ozel_kdv_rel.id not in map(lambda x:x.ozel_kdv_rel.id,lot_id.urun_params.ozel_kdv):
								error_msg += ozel_urun_grup.ozel_urun_rel.name + u' Adlı Ürün Grubu, Departmanı Eklenemediği için Eklenemedi \n'
							elif len(lot_id.urun_params.ozel_urun_gruplari) >= 18:
								error_msg += ozel_urun_grup.ozel_urun_rel.name + u' Adlı Ürün Grubu, Toplam Departman Adet 18\'i geçtiği için eklenemedi \n'
							else:
								ozel_urun_grup.copy(dict(grup_id=lot_id.urun_params.id))

					for u in self.urun:
						if u.urun_rel.id not in map(lambda x:x.urun_rel.id,lot_id.urun_params.urun):
							if u.urun_grup_id.id not in map(lambda x:x.ozel_urun_rel.id,lot_id.urun_params.ozel_urun_gruplari):
								error_msg += u.urun_rel.name + u' Adlı Ürün, Ürün Grubu Eklenemediği için Eklenemedi \n'
							else:
								u.copy(dict(grup_id=lot_id.urun_params.id))

					for k in self.kur:
						if k.ozel_kur_rel.id not in map(lambda x:x.ozel_kur_rel.id,lot_id.urun_params.kur):
							k.copy(dict(grup_id=lot_id.urun_params.id))

					for a in self.altin:
						if a.ozel_altin_rel.id not in map(lambda x:x.ozel_altin_rel.id,lot_id.urun_params.altin):
							a.copy(dict(grup_id=lot_id.urun_params.id))

					for t in self.tahsilat:
						if t.tahsilat_rel.id not in map(lambda x:x.tahsilat_rel.id,lot_id.urun_params.tahsilat):
							if len(lot_id.urun_params.tahsilat) >= 16:
								error_msg += t.tahsilat_rel.name + u' Adlı Tahsilat Tipi Toplam Tip Adet 16\'yi geçtiği için eklenemedi \n'
							else:
								t.copy(dict(button_no   = len(lot_id.urun_params.tahsilat)+1,
											grup_id     = lot_id.urun_params.id))

					for s in self.satis:
						if s.satis_tipi_islem_rel.id not in map(lambda x:x.satis_tipi_islem_rel.id,lot_id.urun_params.satis):
							if len(lot_id.urun_params.tahsilat) >= 20:
								error_msg += t.tahsilat_rel.name + u' Adlı Satış Tipi Toplam Tip Adet 20\'yi geçtiği için eklenemedi \n'
							else:
								s.copy(dict(button_no   = len(lot_id.urun_params.satis)+1,
											grup_id     = lot_id.urun_params.id))

					for f in self.fatura_tahsilat_kurum:
						if f.tahsilat_kurum_rel.id not in map(lambda x:x.tahsilat_kurum_rel.id,lot_id.urun_params.fatura_tahsilat_kurum):
							if len(lot_id.urun_params.fatura_tahsilat_kurum) >= 40:
								error_msg += t.tahsilat_rel.name + u' Adlı Fatura Tahsilat Kurumu Toplam Tip Adet 40\'ı geçtiği için eklenemedi \n'
							else:
								f.copy(dict(grup_id   = lot_id.urun_params.id))


					if len( error_msg ) > 0:
						total_errors [lot_id.name] = error_msg

		if len( total_errors.keys()) > 0:

			str_keys_vals = ""
			for k,v in total_errors.iteritems():
				str_keys_vals +=  k + u' : '
				str_keys_vals +=  v
		
		
		i   = 1
		for row in self.ozel_kdv:
			row.button_no_v = i
			row.button_no   = i
			i+=1

class res_partner_personel( models.Model):
	_inherit = 'res.partner.personel'
	_order    = 'teknik_servis_kullanici_kodu'
	
	@api.model
	def send_mail(self):
		local_context = self.env.context.copy()
		company = self.env['res.users'].browse(SUPERUSER_ID).company_id.name
		local_context.update({
			'dbname': self.env.cr.dbname,
			'base_url': self.env['ir.config_parameter'].get_param('web.base.external_url', default='http://localhost:8069'),
			'datetime' : datetime,
			'company' : company,
			'only_user_sign' : True
		})
		
		temp_obj = self.env['mail.template']
		template_id = self.env['ir.model.data'].sudo().get_object('info_tsm', 'pass_change_template')
		template_obj = temp_obj.with_context(local_context).browse(template_id.id)
		body_html = temp_obj.with_context(local_context).render_template(template_obj.body_html, 'res.partner.personel', self.id)
		
		#print body_html
		
		if template_id:
			values = template_obj.generate_email( res_ids = self.id)
			values['subject'] = 'Şifre Bilgileri'
			values['email_to'] = self.email
			values['body_html'] = body_html
			values['body'] = body_html
			values['res_id'] = False
			values['email_from'] = 'info@infoteks.com.tr'
			mail_mail_obj = self.env['mail.mail'].sudo()
			msg_id = mail_mail_obj.create( values )

			if msg_id:
				#print  msg_id
				msg_id.sudo().send()
		return True
	
	@api.multi
	def generate_pass( self ):
		self.ensure_one()
		new_pass, md5 = generate_new_pass()
		self.teknik_servis_pass           = md5
		self.teknik_servis_first_pass_raw = new_pass

		self.send_mail()

class okc_firma_parametre( models.Model ):

	_inherit = 'res.partner'

	def _get_default_apn( self ):
		apn = self.env['info_tsm.infoteks_apn'].search([],limit=1)
		if len( apn) > 0:
			return apn[0]
		return self.env['info_tsm.infoteks_apn']
	def _get_default_ntp( self ):
		ntp = self.env['info_tsm.infoteks_ntp'].search([],limit=1)
		if len( ntp ) > 0:
			return ntp[0]
		return self.env['info_tsm.infoteks_ntp']

	def _get_default_params( self ):


		params = self.env['info_tsm.parametre_grup'].search([('name','ilike','BANKASIZ YAZARKASA')],limit=1)
		if len( params ) > 0:
			return params[0]
		return self.env['info_tsm.parametre_grup']

	def _get_default_tanims( self ):
		params = self.env['info_tsm.okc_urun_grup'].search([('name','ilike','VARSAYILAN GRUP')],limit=1)
		if len( params ) > 0:
			return params[0]
		return self.env['info_tsm.okc_urun_grup']

	def _get_default_pass( self ):
		  return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(6))

	@api.model
	def create(self,values ):
		md5 = hashlib.md5
		md5 = md5()
		if values.has_key( 'teknik_servis_pass' ):
			values['teknik_servis_first_pass_raw'] = values['teknik_servis_pass']
			md5.update( values['teknik_servis_pass'])
			values['teknik_servis_pass'] = md5.hexdigest()
		res= super(okc_firma_parametre,self).create( values )

		if res.teknik_servis_elemani and not res.gibe_bildirme:
			new_gib_obj      = self.env['info_tsm.ynokc_yetkili_servis']
			new_gib_obj_dict = dict(
				yetkili_ad                    = res.id,
				yetkili_firma                 = res.parent_id.id,
				yetkili_firma_vkn             = res.parent_id.taxNumber,
				yetkili_tckn                  = res.tckn,
				yetkili_firma_il              = res.parent_id.city_combo.id,
				yetkili_firma_il_kodu         = res.parent_id.city_combo.plaka,
				yetki_gecerlilik_baslangic    = res.teknik_servis_bas_tar,
				yetki_gecerlilik_bitis        = res.teknik_servis_bit_tar,
				durum_bilgisi                 = 1,
				durum                         = 0#-1:sorunlu kayit var, 0 yeni, 1 dosyay aeklendi,2 sorunsuz

			)
			new_gib_obj.sudo().create( new_gib_obj_dict )

		return res

	baslik         = fields.Char( string="Fiş Başlığı")
	slip_top       = fields.Text( string="Slip Üst Bilgi" )
	slip_bottom    = fields.Text( string="Slip Alt Bilgi" )
	logo           = fields.Binary( string="Logo")
	default_params = fields.Many2one( 'info_tsm.parametre_grup',string="Varsayılan Parametre Grubu" , default = _get_default_params)
	default_tanim  = fields.Many2one( 'info_tsm.okc_urun_grup',string="Varsayılan Tanım Grubu",  default = _get_default_tanims)
	apn            = fields.Many2one( 'info_tsm.infoteks_apn',String='Apn Server', default=_get_default_apn)
	ntp            = fields.Many2one( 'info_tsm.infoteks_ntp',String='Ntp Server', default=_get_default_ntp)
	teknik_servis_elemani        = fields.Boolean (string='Teknik Servis Personeli')
	teknik_servis_kullanici_kodu = fields.Char (string='Teknik Servis Kullanıcı Kodu')
	teknik_servis_bas_tar        = fields.Date(string='Yetki Başlangıç', size=8)
	teknik_servis_bit_tar        = fields.Date(string='Yetki Bitiş'    , size=8)
	hurdaya_ayirabilir           = fields.Boolean(string='Cihazı Hurdaya Ayırabilir.')
	devir_yapabilir              = fields.Boolean(string='Cihaz Devir İşlemi Yapabilir.')
	teknik_servis_pass           = fields.Char(string='Teknik Servis Pass', default=_get_default_pass)
	teknik_servis_first_pass_raw = fields.Char(string='Teknik Servis Pass Raw')
	teknik_sers                     = fields.One2many('res.partner.personel','parent_id')
	child_ids                       = fields.One2many('res.partner', 'parent_id', string='Contacts',
														domain=[('active', '=', True),('is_company','=',True),('teknik_servis_elemani','=',False)])

	yetki_state                     = fields.Integer(string='Yetki Durumu',compute='_get_yetki_state')
	gibe_bildirme                   = fields.Boolean(string='Gib Bildirimi Yapma') 
	@api.multi
	def _get_yetki_state(self):
		for s in self:
			if s.teknik_servis_bit_tar:
				#print s.teknik_servis_bit_tar
				if datetime.now() > datetime.strptime(s.teknik_servis_bit_tar, '%Y-%m-%d') :
					s.yetki_state = 5
				else:
					s.yetki_state = 2
			else:
				s.yetki_state = 5
	
	_sql_constraints    = [
		('teknik_servis_kod_unique', 'unique(teknik_servis_kullanici_kodu)', 'Bu Teknik Servis Koduna Sahip Başka Bir Kullanıcı Bulunmakta...'),
		]

class uploaded_files ( models.Model ):
	_name = 'info_tsm.uploaded_file'

	terminal_id                 = fields.Many2one('stock.production.lot', string= 'YÖKC')
	major                       = fields.Integer(string='Version No (Major)')
	minor                       = fields.Integer(string='Version No (Minor)')
	build                       = fields.Integer(string='Yapı No')
	revision                    = fields.Integer(string='Revizyon No')
	uploaded_file               = fields.Binary(string='Yüklenen Dosya', required=True)
	uploaded_file_fname         = fields.Char(string='Orj Dosya Adı')
	uploaded_file_parca_sayisi  = fields.Integer( string='Yüklenen Dosya Parça Sayısı')
	isactive                    = fields.Boolean(string='Aktif',default=True)
	md5                         = fields.Char(string="Dosya Md5")

class uploaded_file_chunks ( models.Model ):

	_name = 'info_tsm.uploaded_file_chunks'

	uploaded_file       = fields.Many2one('info_tsm.uploaded_file', string='Ana Dosya',ondelete='restrict')
	chunk               = fields.Char(string='Dosya Parçası (1024 byte)', size=2048)
	part_no             = fields.Integer(string='Dosya Parça No')
	terminal_id         = fields.Many2one('stock.production.lot', string= 'YÖKC')
	_sql_constraints    = [
		('chunk_part_unique', 'unique(uploaded_file, part_no)', 'Dosya Parça Numarası Tek Olmalıdır.'),
		]

class tsm_alert_info( models.Model ):
	_name = 'info_tsm.alert_info'

	cihaz_seri_no = fields.Many2one ('stock.production.lot', string='Cihaz Seri No',required=True)
	mesaj_tipi    = fields.Char(string='Mesaj Tipi', required=True)
	aciklama      = fields.Text(string='Açıklama', required=True)
	state         = fields.Boolean( string='Aktif')

class tsm_sertifika_log( models.Model ):
	_name = 'info_tsm.sertifika_log'

	cihaz_seri_no = fields.Many2one ('stock.production.lot', string='Cihaz Seri No',required=True)
	sertifika     = fields.Many2one('info_tsm.tsm_cert', string='Sertifika', required=True)

	_order = 'id desc'

class tsm_cert_pass ( models.TransientModel ):
	_name = 'info_tsm.server_cert_pass'

	passwd = fields.Char(string='Password :')
	@api.multi
	def sub(self):
		if self.passwd == 'manager':
			self.passwd = ""
			treeview_id = self.env.ref('info_tsm.tsm_tsm_cert_tree').id
			formview_id = self.env.ref('info_tsm.tsm_tsm_cert_form').id
			return dict(
					type      = 'ir.actions.act_window',
					name      = "Sertifika Listesi",
					res_model = "info_tsm.tsm_cert",
					view_type = 'form',
					view_mode        = 'tree,form',
					views            = [(treeview_id, 'tree'),(formview_id, 'form')],
					target           = "main",
					clear_breadcrumb = True,
				)
		self.passwd = ""
		return False

class tsm_cert(models.Model):

	_name = 'info_tsm.tsm_cert'

	name  = fields.Integer( string='Seritifika No')
	filename = fields.Char()
	dosya = fields.Binary( string='Sertifika Dosyası')
	dosya_md5 = fields.Char('Sertifika MD5')
	file_path = fields.Char()
	byte_len  = fields.Float(string = 'Dosya Boyutu')
	def get_file_ops(self, values, one = False):
		if one:
			self.ensure_one()

		if values.get('dosya'):
			md5 = hashlib.md5()
			md5.update( values.get('dosya') )
			h  = md5.hexdigest()
			values['dosya_md5'] = h

			file_path = file_dir + '/' + values.get('filename')
			inzipfile        =  open( file_path, 'wb' )
			byte = base64.b64decode( values.get('dosya') )

			md5 = hashlib.md5()
			md5.update( byte )
			h  = md5.hexdigest()
			values['dosya_md5'] = h

			inzipfile.write( byte )
			inzipfile.close()
			values ['byte_len']  = os.path.getsize( file_path )
			values ['file_path'] = file_path
		return values


	@api.multi
	def write( self, values ):
		values = self.get_file_ops( values, True )
		return super( tsm_cert, self ).write( values )

	@api.model
	def create( self, values ):
		values = self.get_file_ops( values )
		return super( tsm_cert, self ).create( values )

	@api.multi
	def sub(self):

		treeview_id = self.env.ref('info_tsm.tsm_tsm_cert_tree').id
		formview_id = self.env.ref('info_tsm.tsm_tsm_cert_form').id
		return dict(
			type      = 'ir.actions.act_window',
			name      = "Sertifika Listesi",
			res_model = "info_tsm.tsm_cert",
			view_type = 'form',
			view_mode        = 'tree,form',
			views            = [(treeview_id, 'tree'),(formview_id, 'form')],
			target           = "main",
			clear_breadcrumb = True,
		)

class tsm_restart_pass ( models.TransientModel ):
	_name = 'info_tsm.server_restart_pass'

	passwd = fields.Char(string='Password :')
	durum  = fields.Char();
	@api.multi
	def sub(self):
		if self.passwd == 'manager':
			obj  = self.env['info_tsm.server'].browse([self._context['active_id']])
			#print self._context
			#print self.durum
			if str(self._context['durum']) == 'w':
				path = obj.dosya_yolu
				if path[-1] != '/':
					path = path + '/'

				os.chdir( path )
				#print path
				log_name = 'tsm_log.log'

				command_restart = ['forever', 'restart','-o','%s'%log_name, '%s'%obj.file_name, str(int(obj.port_no)),str(obj.gib_ip),str(int(obj.gib_port)),str(obj.gib_parameter_mode),str(obj.tsm_mode) ]
				command_start   = ['forever', 'start','-o','%s'%log_name, '%s'%obj.file_name,str(int(obj.port_no)),str(obj.gib_ip),str(int(obj.gib_port)),str(obj.gib_parameter_mode),str(obj.tsm_mode) ]
				start = False
				proc = 'Hata Olustu !!!\n'
				try:
					#print command_restart
					proc = subprocess.check_output( command_restart)
				except subprocess.CalledProcessError as e:
					proc  = proc + str(e)
					start = True
				if start:
					try:
						proc =  subprocess.check_output( command_start )
					except subprocess.CalledProcessError as e:
						proc  = proc + str(e)

				if not proc.startswith( 'Hata' ):
					onceki_aktif = obj.env['info_tsm.server'].search([('calisiyor','=',True)])
					for a in onceki_aktif:
						a.calisiyor = False
					obj.calisiyor = True
					title = 'Islem Basarili \n'
				else:
					title = u'Hata Olustu \n'
				obj.log_box = title.decode('utf-8') + proc.decode('utf-8')
				self.passwd = ""
				return True

			elif str(self._context['durum']) == 's':
				path = obj.dosya_yolu
				if path[-1] != '/':
					path = path + '/'

				os.chdir( path )
				log_name = 'tsm_log.log'

				command_stop = ['forever', 'stop', '%s'%obj.file_name ]
				proc = 'Hata Olustu !!!\n'
				try:
					proc = subprocess.check_output( command_stop )
				except subprocess.CalledProcessError as e:
					proc  = proc + str(e)

				if proc.startswith( 'Hata' ):
					title = 'Hata Olustu \n'
				else:
					title = 'Islem Basarili \n'
				obj.calisiyor = False

				obj.log_box = title.decode('utf-8') + proc.decode('utf-8')
				self.passwd = ""
				return True

		self.passwd = ""
		return False

class tsm_server_options( models.Model ):

	_name      = 'info_tsm.server'

	name       = fields.Char(string='TSM Versiyonu', required=True)
	major      = fields.Float(string='Major Versiyon', required=True, digits=(2,0), size=2)
	minor      = fields.Float(string='Minor Versiyon', required=True, digits=(2,0), size=2)
	build      = fields.Float(string='Build No', required=True, digits=(2,0), size=2)
	revision   = fields.Float(string='Revizyon No', required=True, digits=(2,0), size=2)
	file_      = fields.Binary(string='Tsm Dosyası', required=True)
	file_name  = fields.Char(string='File Name')
	dosya_yolu = fields.Char(string='Tsm Çalışma Dosya Yolu', required=True)
	port_no    = fields.Float(string='Tsm Server Port', digits=(5,0), size=5, required=True)
	gib_ip     = fields.Char(string='Gib BS IP Adresi', required=True)
	gib_port   = fields.Float(string='Gib BS Port Numarası',digits=(5,0), size=5,required=True)
	gib_mode   = fields.Selection([('Local','Lokal'),
								('Dev','Development'),
								('Prod','Production')],string='Gib Modu',required=True)
	gib_parameter_mode   = fields.Selection([
								('dev', 'Development'),
								('prod', 'Production')
								], string='Gib Parametre Modu',required=True)
	tsm_mode = fields.Selection([('Local','Lokal'),
								('Dev','Development'),
								('Prod','Production')], string='TSM Modu',required=True)

	state = fields.Selection([('hazir','Hazır'),
							  ('aktif','Çalışan Versiyon'),
							  ('iptal','İptal Versiyon')])
	calisiyor = fields.Boolean(string='Çalışıyor')
	log_box   = fields.Text(string='İşlem Log')



	_defaults = {
		'dosya_yolu':'/var/lib/tsm_node/',
		'port_no':8001,
		'gib_ip':'127.0.0.1',
		'gib_port':6969,
		'gib_mode':'Dev',
		'tsm_mode':'Dev',
		'gib_parameter_mode':'dev',
		'state':'hazir'
	}
	_order = 'id desc'

	@api.multi
	def make_ready(self):
		self.ensure_one()
		self.state = 'hazir'
	@api.multi
	def make_it_work(self):
		self.ensure_one()
		file_ = self.file_
		path = self.dosya_yolu
		if path[-1] != '/':
			path = path + '/'

		exec_path = path + self.file_name

		file_opened = open(exec_path,'wb')
		file_opened.write('/* Version:' + self.name + ' */\n')
		file_opened.write( base64.b64decode( file_ ))
		file_opened.close()

		onceki_aktif = self.env['info_tsm.server'].search([('state','=','aktif')])
		for a in onceki_aktif:
			a.state = 'hazir'

		self.state = 'aktif'
		self.log_box = 'Aktif Hale Getirildi'

	@api.multi
	def make_iptal(self):
		self.state='hazir'

	@api.multi
	def calistir(self):
		#once restart edelim, yemese start
		return {
			'name': 'TSM Password',
			'domain': [],
			'res_model': 'info_tsm.server_restart_pass',
			'type': 'ir.actions.act_window',
			'view_mode': 'form',
			'view_type': 'form',
			'context': {'durum':'w'},
			'target': 'new',
		}



	@api.multi
	@api.onchange('calisiyor')
	def durdur(self):
		return {
			'name': 'TSM Password',
			'domain': [],
			'res_model': 'info_tsm.server_restart_pass',
			'type': 'ir.actions.act_window',
			'view_mode': 'form',
			'view_type': 'form',
			'context': {'durum':'s'},
			'target': 'new',
		}


	@api.onchange('major','minor','build','revision')
	def _onchange_versiob_ogeleri(self ):
		self.name = ''
		if self.major:
			self.name += str( int(self.major) )
		else:
			self.name += '0'
		if self.minor:
			self.name += '.' + str( int(self.minor) )
		else:
			self.name += '.0'
		if self.build:
			self.name += '.' + str( int(self.build) )
		else:
			self.name += '.0'
		if self.revision:
			self.name += '.' + str( int(self.revision) )
		else:
			self.name += '.0'
	@api.one
	@api.constrains('gib_ip' )
	def _check_ip_address(self):

		##print self.haberlesme_tip_adi
		ch_obj = {'gib_ip':'Gib BS IP Adresi'}
		foo = check( exceptions, self )
		foo.check_ip_no( ch_obj )

	def get_log(self, cr, uid, context=None):

		active_log        = self.pool.get('info_tsm.server').search(cr, uid,[('state','=','aktif')],limit=1)
		active_log_path   = ''
		if len(active_log) > 0:
			active_log        = self.pool.get('info_tsm.server').browse(cr, uid, active_log[0], context=context)
			active_log_path   = active_log.dosya_yolu
			if active_log_path[-1] != '/':
				active_log_path = active_log_path + '/'
			active_log_path = active_log_path + 'tsm_log.log'

		#print active_log_path

		if active_log_path == '':
			return dict( log = [])


		log_list = []
		try:
			log = LogWatcher.tail(active_log_path, 100)
		except:
			log = ''
		return dict(log=log)

class tsm_katma_deger_sozlesme( models.Model ):

	_name = 'info_tsm.katma_deger_sozlesme'

	name  = fields.Many2one('stock.production.lot', string='Cihaz Seri No', required=True,domain=[('name','ilike','OF')])
	firma = fields.Many2one('res.partner',string='Cihazı Kullanan Firma')
	z_raporu     = fields.Boolean(string='Z Rapor Kayıt Aktivasyonu')
	z_raporu_bas = fields.Date(string = 'Z Rapor Kayıt Başlangıç Tarihi')
	z_raporu_bit = fields.Date(string = 'Z Rapor Kayıt Bitiş Tarihi')

	fis_raporu     = fields.Boolean(string='Fiş Kayıt Aktivasyonu')
	fis_raporu_bas = fields.Date(string = 'Fiş Kayıt Başlangıç Tarihi')
	fis_raporu_bit = fields.Date(string = 'Fiş Kayıt Bitiş Tarihi')

	olay_raporu     = fields.Boolean(string='Olay Kaydı Kayıt Aktivasyonu')
	olay_raporu_bas = fields.Date(string = 'Olay Kaydı Kayıt Başlangıç Tarihi')
	olay_raporu_bit = fields.Date(string = 'Olay Kaydı Kayıt Bitiş Tarihi')

	istat_raporu     = fields.Boolean(string='İstatistik Kayıt Aktivasyonu')
	istat_raporu_bas = fields.Date(string = 'İstatistik Kayıt Başlangıç Tarihi')
	istat_raporu_bit = fields.Date(string = 'İstatistik Kayıt Bitiş Tarihi')

	harici_raporu     = fields.Boolean(string='Harici Cihaz Kayıt Aktivasyonu')
	harici_raporu_bas = fields.Date(string = 'Harici Cihaz Kayıt Başlangıç Tarihi')
	harici_raporu_bit = fields.Date(string = 'Harici Cihaz Kayıt Bitiş Tarihi')

	dosya  = fields.Binary(string = 'Sözleşme Dosyası', required=True)
	file_name = fields.Char(string ='Dosya Adı')
	
	@api.onchange('name')
	def _onchange_name( self ):
		if self.name:
			self.firma = self.name.sold_to.id

	@api.multi
	def prev_file(self):
		return {
			 'type' : 'ir.actions.act_url',
			 'url': '/web/binary/download_document?model=info_tsm.katma_deger_sozlesme&field=dosya&id=%s&filename=%s'%(self.id,self.file_name),
			 'target': 'new'
		}

class tsm_urun(models.Model):
	_name = 'info_tsm.urun_listesi'

	name         = fields.Char(string='Ürün Adı', size=20, required=True)
	#kdv_grup_id  = fields.Selection(selection=kdv_index, string="Gib KDV Indeksi", required=True)
	barkod_no    = fields.Char(string='Barkod No')

	'''
	@api.one
	@api.constrains('name')
	def _check_unique_insesitive_record(self):

		##print self.haberlesme_tip_adi
		ch_obj = {'name':'Ürün Adı','company_id':self.env.user.company_id.id
				  }
		foo = check( exceptions, self )
		foo.check_unique_bos( ch_obj )
	'''
	_sql_constraints = [
	('rel_urun_unique', 'unique(name,company?id)', 'Ürün Zaten Eklenmiş'),
	]

class altin_kodlari(models.Model ):
	_name = 'info_tsm.altin_kodlari'

	name = fields.Char( string ='Altın Çeşiti', required=True)
	kod  = fields.Integer( string ='Altın Kodu', required=True)
	birim_tipi    = fields.Selection( selection=birim_tipleri, string = 'Birim Tipi', required=True)
	dafault       = fields.Boolean(string='Gruplara Varsayılan Olarak Eklensin mi?')

	@api.one
	@api.constrains('name')
	def _check_unique_insesitive_record(self):

		##print self.haberlesme_tip_adi
		ch_obj = {'name':'Altın Çeşiti'
				  }
		foo = check( exceptions, self )
		foo.check_unique_bos( ch_obj )
	_sql_constraints = [
	('rel_unique', 'unique(kod)', 'Ürün Kodu Zaten Eklenmiş'),
	]

class satis_raporu( models.Model ):

	_name = 'info_tsm.satis_raporu'

	name          = fields.Many2one('stock.production.lot', string='Terminal ID')
	firma         = fields.Many2one('res.partner', string='Firma')
	urun          = fields.Many2one('info_tsm.urun_listesi', string='Ürün')
	urun_aciklama = fields.Char(string='Ürün Açıklama')
	rap_bas       = fields.Date (string='Rapor Başlangıç')
	rap_bit       = fields.Date (string='Rapor Bitiş')
	birim_tipi    = fields.Selection( selection=birim_tipleri, string = 'Birim Tipi')
	miktar        = fields.Float(string='Satış Miktarı')
	kdv_dilimi    = fields.Integer(string= 'KDV Oranı')
	total_kdv     = fields.Float(string='Toplam KDV')
	toplam_tutar  = fields.Float(string='Toplam Tutar')
	z_rapor_no    = fields.Integer(string= 'Z Rapor No')

class satis_raporu_eslesmeyen( models.Model ):

	_name = 'info_tsm.satis_raporu_eslesmeyen'

	name          = fields.Many2one('stock.production.lot', string='Terminal ID')
	firma         = fields.Many2one('res.partner', string='Firma')
	urun          = fields.Char(string='Ürün ID')
	urun_aciklama = fields.Char(string='Ürün Açıklama')
	rap_bas       = fields.Date (string='Rapor Başlangıç')
	rap_bit       = fields.Date (string='Rapor Bitiş')
	birim_tipi    = fields.Selection( selection=birim_tipleri, string = 'Birim Tipi')
	miktar        = fields.Float(string='Satış Miktarı')
	kdv_dilimi    = fields.Integer(string= 'KDV Oranı')
	total_kdv     = fields.Float(string='Toplam KDV')
	toplam_tutar  = fields.Float(string='Toplam Tutar')
	z_rapor_no    = fields.Integer(string= 'Z Rapor No')

class parse_edilemeyen_excel_satirlari( models.Model):
	_name = 'info_tsm.hatali_excel_satirlari'

	firma               = fields.Many2one('res.partner', domain = [('active','=',True),
																	('is_company','=',True),
																	('customer','=',True),
																	('satilan_lots','!=',False)] )
	name                = fields.Char( string='Tanımlama Grubu Adı')

	urun_adi            = fields.Char(string='Ürün Adı')
	barkod_no           = fields.Char(string='Barkod No')
	plu_no              = fields.Char(string='Plu No')
	kdv                 = fields.Char(string='KDV Oranı')
	birim_tipi          = fields.Char(string='Birim Tipi')
	urun_grubu          = fields.Char(string='Ürün Grubu')
	tutar               = fields.Char(string='Tutar')
	satir_no            = fields.Char(string='Excel Satır No')

class tsm_okc_urun_cihaz_id_map( models.Model ):

	_name = 'info_tsm.okc_urun_cihaz_id_map'

	name                = fields.Many2one( 'stock.production.lot',string='Cihaz Seri No')
	urun_id             = fields.Many2one('info_tsm.urun_listesi')
	grup_id             = fields.Many2one('info_tsm.okc_ozel_urun_grubu_tablosu')
	cihaz_id            = fields.Integer()
	cihaz_urun_grup_id  = fields.Integer()

class tsm_cihaz_durum_raporu( models.Model ):

	_name = 'info_tsm.cihaz_durum_raporu'

	pos_id                  = fields.Integer(string='Pos Id')
	okc_datetime            = fields.Datetime(string='OKC Tarihi')
	cihaz_seri_no           = fields.Char(string='Terminal Seri No')
	name                    = fields.Many2one('stock.production.lot',string='Terminal')
	gprs_baglantisi         = fields.Char(string='GPRS Bağlantısı')
	ethernet_baglantisi     = fields.Char(string='Ethernet Bağlantısı')
	eku_durumu              = fields.Char(string='Ekü Durumu')
	servis_girisi_zorunlu   = fields.Char(string='Servis Girişi Zorunlu')
	z_raporu_zorunlu        = fields.Char(string='Yetkili Servis Z Raporu Zorunlu')
	aktivasyon_durumu       = fields.Char(string='Aktivasyon Durumu')
	lock_durumu             = fields.Char(string='Lock Durumu')
	mesh_delinme_durumu     = fields.Char(string='MESH Delinme Durumu')
	f_admin_res             = fields.Char(string='Servise Düşme Nedeni')
	next_z                  = fields.Char(string='Bir Sonraki Z No')
	next_x                  = fields.Char(string='Bir Sonraki X No')
	son_fis_no              = fields.Char(string='Son Fiş No')
	son_mali_is_tar         = fields.Datetime(string='Son Mali İşlem Tarihi')
	parametre_vers          = fields.Char(string='Parametre Versiyonu')
	uyg_sayisi              = fields.Char(string='Uygulama Sayısı')
	son_z_gond_tar          = fields.Datetime(string='Son Z Gönderme Tarihi')
	gond_son_z_no           = fields.Char(string='Gönderilen Son Z No')
	son_fis_gond_tar        = fields.Datetime(string='Son Fiş Gönderme Tarihi')
	gond_son_fis_no         = fields.Char(string='Gönderilen Son Fiş No')
	gond_son_z_fis_no       = fields.Char(string='Gönderilen Son Fiş Z No')
	parametre_vers          = fields.Char(string='Parametre Versiyonu')
	son_gib_param_ind_tar   = fields.Datetime(string='Son GİB Param.İndirme Tarihi')
	son_okc_param_ind_tar          = fields.Datetime(string='Son OKC Param.İndirme Tarihi')
	durum_prog_rapor        = fields.One2many('info_tsm.durum_rapor_uyg_durumu','durum_rapor_id',string='Prog. Özellikleri')

class tsm_durum_rapor_uyg_durumu( models.Model ):

	_name = 'info_tsm.durum_rapor_uyg_durumu'
	#durum_rapor_id,tsm_program_type,status,maj_min,bld_rev

	durum_rapor_id   = fields.Many2one('info_tsm.cihaz_durum_raporu',string="Durum Raporu")
	tsm_program_type = fields.Many2one('info_tsm.program_type',string="Program Tipi")
	status           = fields.Char(string='Durum')
	maj_          = fields.Char(string='Major')
	min_          = fields.Char(string='Minor')
	bld_          = fields.Char(string='Build')
	rev_          = fields.Char(string='Rev')

class uygulama_parametreleri_rapor( models.Model ):

	_name='info_tsm.uygulama_parametreleri_raporu'

	name                = fields.Many2one('stock.production.lot',string='Terminal')
	appissilent         = fields.Boolean(string='Arka Planda Çalış?')
	loginrequire        = fields.Boolean(string='Login Zorunlu mu?')
	canremovesalesline  = fields.Boolean(string='Satış Satırını Kaldır?')
	canfingerlogin      = fields.Boolean(string='Parmak İzi Kullan?')
	gprsopenmode        = fields.Boolean(string='Manuel GPRS Kullan?')
	foreignsales        = fields.Boolean(string='Uluslar Arası Satışlar?')
	flightmode          = fields.Boolean(string='Uçuş Modu?')
	selectedsalesscreen = fields.Many2one('info_tsm.p_satis_ekran_tipleri',string='Seçili Satış Ekranı' )
	enddaytype          = fields.Char(string='Gün Sonu Tipi')
	enddaytime          = fields.Integer(string='Gün Sonu Zamanı')
	statereptype        = fields.Many2one('info_tsm.p_okc_durum_raporlama',string='Durum Cevap Tipi')
	staterepperiod      = fields.Many2one('info_tsm.p_sleep_suspend',string='Durum Cevap Gönderme Periyodu')
	sendsalestsm        = fields.Boolean(string='Satış TSM Gönderme')
	sendsalestsmtime    = fields.Integer(string='Satış TSM Gönderme Zamanı')
	stocksyncmode       = fields.Boolean(string='Stok Eşitleme Modu')
	stocksynctime       = fields.Integer(string='Stok Eşitleme Zamanı')
	left_button          = fields.Many2one('info_tsm.p_okc_left_button' , string='Sol Buton')
	right_button         = fields.Many2one('info_tsm.p_okc_right_button', string='Sağ Buton')
	prmupdatetype       = fields.Many2one('info_tsm.p_prm_update_type', string='Prm Güncelleme Tipi')
	printzslip          = fields.Boolean(string='Dijital Z Slip Çıktısı ?')
	acsleep             = fields.Many2one('info_tsm.p_sleep_suspend', string='Ac Güçte Uyku Periyodu')
	acssuspend          = fields.Many2one('info_tsm.p_sleep_suspend', string='Ac Güçte Askıya Alma Periyodu')
	btsleep             = fields.Many2one('info_tsm.p_sleep_suspend', string='Pilde Uyku Periyodu')
	btssuspend          = fields.Many2one('info_tsm.p_sleep_suspend', string='Pilde Askıya Alma Periyodu')
	gibcurrencymode     = fields.Boolean(string="Döviz Tutarı Gönderilsin mi?")
	rj45boardrev        = fields.Boolean(string="Yeni RJ45 Board?")
	modem3g             = fields.Boolean(string="3G Modem Kullan?")
	keybeep             = fields.Boolean(string="Tuş Sesi Aktif")
	keybeeptype         = fields.Selection(string="Tuş Sesi Tipi", selection=bip_tipleri)

class tutanak_no(models.Model):
	_name = 'info_tsm.tutanak_no'

class tahsilat_tipleri( models.Model ):
	_name = "info_tsm.tahsilat_tipleri"

	kod  = fields.Integer(String='Tahsilat Tipi', required=True)
	name = fields.Char( String='Açıklama', required=True )
	dafault       = fields.Boolean(string='Gruplara Varsayılan Olarak Eklensin mi?')

class satis_tipleri( models.Model ):
	_name = "info_tsm.satis_tipleri"

	kod  = fields.Integer(String='Satış Tipi', required=True)
	name = fields.Char( String='Açıklama', required=True )
	dafault       = fields.Boolean(string='Gruplara Varsayılan Olarak Eklensin mi?')

class bilgi_fisi_tipleri( models.Model ):
	_name = "info_tsm.bilgi_fisi_tipleri"

	kod  = fields.Integer(String='Bilgi Fiş Tipi', required=True)
	name = fields.Char( String='Açıklama', required=True )
	dafault       = fields.Boolean(string='Gruplara Varsayılan Olarak Eklensin mi?')

class satis_tipi_islem_kodu_tipleri( models.Model ):
	_name = "info_tsm.satis_tipi_islem_kodu_tipleri"

	kod  = fields.Integer(String='Satış Şekli', required=True)
	name = fields.Char( String='Açıklama', required=True )
	dafault       = fields.Boolean(string='Gruplara Varsayılan Olarak Eklensin mi?')

class fatura_tahislat_kurum_tipleri( models.Model ):
	_name = "info_tsm.fatura_tahsilat_kurum_tipleri"

	kod  = fields.Integer(String='Kurum Tipi', required=True)
	name = fields.Char( String='Açıklama', required=True )
	dafault       = fields.Boolean(string='Gruplara Varsayılan Olarak Eklensin mi?')

class tahsilat_satis_grup( models.Model ):

	_name = 'info_tsm.okc_tahsilat_satis_grup'

	firma  = fields.Many2one( 'res.partner', string = 'Firma', domain=[('active','=',True),
																	   ('is_company','=',True),
																	   ('customer','=',True),
																	   ('satilan_lots','!=',False)],required=True )

	terminal_id         = fields.Many2one('stock.production.lot', 'Cihaz Seri No')

	#name                = fields.Char( string = 'Kur Bilgileri Grup Adı', required = True)
	terminal_id_disp    = fields.One2many('stock.production.lot', 'satis_tahsilat_params', string="Tanımlı Ökc'ler" )
	tahsilat            = fields.One2many('info_tsm.tahsilat_tipi_group_rel','grup_id',   string='Tahsilat Tipleri')
	satis               = fields.One2many('info_tsm.satis_tipi_group_rel','grup_id', string='Satış Tipleri')

	_sql_constraints = [
					 ('terminal_id_unique',
					  'unique(terminal_id)',
					  'Bu Seri No Zaten Eklenmiş!!!')
	]

	@api.onchange('firma')
	def _on_change_firma( self ):
		if self.firma:
			return dict ( domain = {'terminal_id': [('sold_to', '=', self.firma.id)]})
		else:
			return dict ( domain = {'terminal_id': []})

	@api.onchange('terminal_id')
	def _on_change_name( self ):
		if self.terminal_id:
			self.firma = self.terminal_id.sold_to.id

	@api.model
	def create( self, values ):
		donen = super(tahsilat_satis_grup,self).create( values )
		#print values
		if not values['terminal_id']:
			firma_cihazlari = self.env['stock.production.lot'].search([('sold_to','=',values['firma'])])
			for cihaz in firma_cihazlari:
				if not cihaz.urun_tek_update_tahsilat_satis:
					cihaz.urun_tek_update_tahsilat_satis = False
					cihaz.satis_tahsilat_params          = donen.id
					#print cihaz.id, donen.id

		else:
			cihaz = self.env['stock.production.lot'].browse(values['terminal_id'])[0]
			cihaz.urun_tek_update_tahsilat_satis = True
			cihaz.satis_tahsilat_params       = donen.id
			#print cihaz.id, donen.id
		return donen


	@api.multi
	def unlink(self):
		for id_ in self:
			firma_cihazlari = self.env['stock.production.lot'].search([('satis_tahsilat_params','=',id_.id)])
			for tah in id_.tahsilat:
				tahsilat.unlink()
			for sat in id_.satis    :
				sat.unlink()

			for f in firma_cihazlari:
				f.urun_tek_update_tahsilat_satis = False
				f.urun_tek_update_tahsilat_satis = False

		return super(tahsilat_satis_grup, self).unlink()

class cihaz_devir_pending( models.Model ):

	_name = 'info_tsm.cihaz_devir_pending'

	@api.model
	def _get_lot_id_domain(self):
		_logger.info('----------------------- ')
		ready_lot_ids = self.search([('state', '=', 'ready' )])
		_logger.info( ready_lot_ids )
		ids_list      = map(lambda x:x.lot_id.id,ready_lot_ids)
		domain = [('id','not in', ids_list),('sold_to','!=',False)]
		return domain

	@api.model
	def _get_default_company(self):
		return self.env.user.company_id.id

	lot_id                  = fields.Many2one('stock.production.lot', string='Devredilecek Cihaz', domain = _get_lot_id_domain,required=True)
	devreden                = fields.Many2one('res.partner' )
	devralan                = fields.Many2one('res.partner', domain="[('customer','=',True),('id','!=',devreden)]", string='Devralan',required=True)
	devir_islemi_yapan_pers = fields.Many2one('res.partner',string='İşlem Yapan Ad Soyad', size=30,
														domain = ([('is_company','=',False),('teknik_servis_elemani','=',True)]))
	state                   = fields.Selection([('ready','Cihazdan İşlem Onayı Bekliyor'),
							  ('done','Devir Gerçekleşti'),
							  ('cancel','İptal Edildi')], string='İşlem Durumu',default='ready')
	devir_fatura_tarihi     = fields.Date( string='Devir Fatura Tarihi')
	devir_fatura_serisira   = fields.Char( string='Devir Fatura Seri/Sıra No')
	tutanak_no              = fields.Integer(string='Devir Tutanak No')
	mrp                     = fields.Many2one('mrp.repair')
	mrp_close               = fields.Boolean()
	company_id              =  fields.Many2one('res.company', default = _get_default_company)
	
	_sql_constraints = [('tutanak_uniq', 'unique (tutanak_no)', 'Bu Tutanak numarası mevcut.'),]
	
	@api.onchange('lot_id')
	@api.depends('lot_id')
	def lot_id_onchange (self):
		if self.lot_id:
			self.devreden = self.lot_id.sold_to.id
		
		return {'domain':{'lot_id':self._get_lot_id_domain()}}


	@api.model
	def create(self, values ):
		res      = super( cihaz_devir_pending, self).create( values )
		self.repair_control( res )
		res.devreden = res.lot_id.sold_to.id
		
		new_isemri  = self.env['mrp.repair']
		location_id = new_isemri.get_location( self.env.user.company_id.id )
		servis_bayi = new_isemri.determinate_servis_bayi( self.env.user.company_id,  res.lot_id)
		islem_seti_ = self.env['info_extensions.sorun_kategorileri'].sudo().search([('set_tipi','=',60)])
		
		print islem_seti_
		
		if islem_seti_ :
			islem_seti_ =[(4,islem_seti_.id)]

		new_is_emri_dict = dict(
			lot_id_          = res.lot_id.id,
			lot_id           = res.lot_id.id,
			partner_id       = res.lot_id.sold_to.id,
			satis_yapan_bayi = res.lot_id.company_id.id,
			servis_bayi      = servis_bayi,
			company_id       = servis_bayi,
			sorun_anlatimi   = 'Cihaz Devir İş Emridir.',
			islem_seti       = islem_seti_,
			product_uom      = res.lot_id.product_id.uom_id.id,
			location_id      = location_id,
			location_dest_id = location_id,
			state            = 'draft',
			mudehalede         = True,
			step_enabled       = False,
			partner_invoice_id = res.lot_id.sold_to.id
			
		)
		new_job = new_isemri.create( new_is_emri_dict )
		new_job.calc_hakedis()
		new_job.action_repair_confirm()
		new_job.action_repair_start()
		res.mrp = new_job.id
		return res
		

	@api.multi
	def write(self,values ):
		if values.has_key('lot_id'):
			lot_obj = self.env['stock.production.lot'].browse( values['lot_id'])
			values['devreden'] = lot_obj.sold_to.id

		res = super( cihaz_devir_pending, self).write( values )

		return res

	@api.one
	def cancel(self):
		self.state = 'cancel'
	
	@api.one
	def confirm(self):
		self.state = 'done'
		
		
	def repair_control(self,res=False):
		cihaz    = self.lot_id
		devralan = self.devralan
		if res:
			cihaz    = res.lot_id
			devralan = res.devralan
		
		if cihaz.sold_to.taxNumber  != devralan.taxNumber:
			onceki_is_emirleri = self.env['mrp.repair'].search([('lot_id','=',cihaz.id),
				('isEmriKodu','in',['K','TE']),
				('partner_id','=',cihaz.sold_to.id)])
			###print len(onceki_is_emirleri)
			kapama_len = []
			for emir in onceki_is_emirleri:
				kapama = self.env['mrp.repair'].search([('lot_id','=',emir.lot_id.id),
					('isEmriKodu','=','TS'),
					('partner_id','=',emir.partner_id.id),
					('origin','=',emir.origin)])
				if len( kapama ) == 0:
					raise exceptions.ValidationError( u'Değiştirilmek İstenen Mükellefe Ait Silinmemiş Banka Uygulaması Mevcuttur. \n Lütfen İlgili Banka Uygulaması İçin Terminal Silme İş Emri Gönderilmesini Sağlayın')
				for k in kapama:
					kapama_len.append ( k )
				
			if not len( f7( kapama_len )) >= len( onceki_is_emirleri):
				raise exceptions.ValidationError( u'Değiştirilmek İstenen Mükellefe Ait Silinmemiş Banka Uygulaması Mevcuttur. \n Lütfen İlgili Banka Uygulaması İçin Terminal Silme İş Emri Gönderilmesini Sağlayın')
				#raise exceptions.ValidationEr
		
	@api.one
	def return_to_ready(self):
		self.repair_control()
		
		
		self.state = 'ready'

class cihaz_hurda_pending( models.Model):
	_name = 'info_tsm.cihaz_hurda_pending'

	@api.model
	def _get_lot_id_domain(self):

		ready_lot_ids = self.search([('state', '=', 'ready' )])
		ids_list      = map(lambda x:x.lot_id.id,ready_lot_ids)
		domain = [('id','not in', ids_list),('sold_to','!=',False)]
		return domain

	@api.model
	def _get_default_company(self):
		return self.env.user.company_id.id

	lot_id                  = fields.Many2one('stock.production.lot', string='Hurdaya Ayrılacak Cihaz', domain = _get_lot_id_domain, required=True)
	hurda_islemi_yapan_pers = fields.Many2one('res.partner',string='İşlem Yapan Ad Soyad', size=30, domain = ([('is_company','=',False),('teknik_servis_elemani','=',True)]))
	state                   = fields.Selection([('ready','Cihazdan İşlem Onayı Bekliyor'),('done','Hurdaya Ayırma Gerçekleşti'), ('cancel','İptal Edildi')], string='İşlem Durumu',default='ready')
	tutanak_no              = fields.Integer(string='Hurda Tutanak No')
	tutanak_no_tamamla      = fields.Char(string="Hurda Tutanak No", compute="tamamla")
	company_id              = fields.Many2one('res.company', default = _get_default_company)
	mrp                     = fields.Many2one('mrp.repair')
	mrp_close               = fields.Boolean()
	
	_sql_constraints = [('tutanak_uniq', 'unique (tutanak_no)', 'Bu Tutanak numarası mevcut.'),]
	@api.multi
	def get_tutanak_no(self):
		self.ensure_one()
		t = self.env['info_tsm.tutanak_no'].sudo()
		new_t = t.create({})
		self.tutanak_no = new_t.id
			
	@api.one
	def tamamla(self):
		uzunluk = len(str(self.tutanak_no))
		kalan_uzunluk = 10 - uzunluk
		ekle = '0'*kalan_uzunluk
		self.tutanak_no_tamamla = ekle + str(self.tutanak_no)

	@api.one
	def cancel(self):
		#self.state = 'cancel'
		self.search([('id', '=', self.id)]).unlink()

	@api.one
	def return_to_ready(self):
		self.state = 'ready'
	
	@api.multi
	def get_tutanak(self):
		self.ensure_one()
		param = self.env["ir.config_parameter"]
		href_g = param.get_param("web.base.external_url", default=None)

		##print "------------------"
		##print href_g
		return {
			'type': 'ir.actions.act_url',
			'url': '/report/pdf/info_tsm.report_hurda/' + str(self.id),
			'target': 'popup'
		}
	
	@api.one
	def confirm(self):
		if not self.tutanak_no or self.tutanak_no == 0:
			raise exceptions.ValidationError('Tutanak No Hatalı Girilmiş.')
		self.state = 'done'
		self.lot_id.lot_state = 'scrapped'
		
		gib_b = self.env['info_tsm.ynokc_islem_bilgisi'].sudo()
		gib_dict = dict (
			lot_id      = self.lot_id.id,
			islem_id    = self.env['info_tsm.gib_yoknc_status_codes'].sudo().search([('durum_kodu','=',4)]).id,
			islem_yapan = self.hurda_islemi_yapan_pers.id,
			kamera      = self.lot_id.kamera,
			barkod      = self.lot_id.barkod,
			parmak      = self.lot_id.parmak,
			imei        = self.lot_id.imei_seri_no.name,
			m2m         = self.lot_id.mtm_seri_no.name,
			satici_id   = self.lot_id.company_id.partner_id.id
		)
		gib_b.create( gib_dict )
	
	@api.model
	def create(self,values):
		res = super(cihaz_hurda_pending,self).create( values )
		
		new_isemri  = self.env['mrp.repair']
		location_id = new_isemri.get_location( self.env.user.company_id.id )
		servis_bayi = new_isemri.determinate_servis_bayi( self.env.user.company_id,  res.lot_id)
		islem_seti_ = self.env['info_extensions.sorun_kategorileri'].sudo().search([('set_tipi','=',70)])
		if islem_seti_ :
			islem_seti_ =[(4,islem_seti_.id)]

		new_is_emri_dict = dict(
			lot_id_          = res.lot_id.id,
			lot_id           = res.lot_id.id,
			partner_id       = res.lot_id.sold_to.id,
			satis_yapan_bayi = res.lot_id.company_id.id,
			servis_bayi      = servis_bayi,
			company_id       = servis_bayi,
			sorun_anlatimi   = 'Cihaz Hurda İş Emridir.',
			islem_seti       = islem_seti_,
			product_uom      = res.lot_id.product_id.uom_id.id,
			location_id      = location_id,
			location_dest_id = location_id,
			state            = 'draft',
			mudehalede         = True,
			step_enabled       = False,
			partner_invoice_id = res.lot_id.sold_to.id
			
		)
		new_job = new_isemri.create( new_is_emri_dict )
		print 1,new_job.islem_seti
		new_job.action_repair_confirm()
		print 2,new_job.islem_seti
		new_job.action_repair_start()
		res.mrp = new_job.id
		return res
		
class customer_feedback( models.Model ):

	_name = 'info_tsm.customer_feedback'
	_order = 'id desc'
	name      = fields.Many2one('stock.production.lot', string='Cihaz Seri No')
	user_code = fields.Char(string='Kullanıcı Kodu')
	user_name = fields.Char(string='Kullanıcı Kasiyer Adı')
	feedback  = fields.Text(string='Kullanıcı Görüşü')
	tel       = fields.Char(string='Tel No')
	customer_name       = fields.Char(string='Müşteri Adı')

class mrp_repair( models.Model):

	_inherit = 'mrp.repair'
	
	cron_send = fields.Boolean('Send By Cron')
	cihaz_param_grup    = fields.Many2many(related='lot_id.uygulama_params.prog_version')
	ozel_kdv            = fields.One2many('info_tsm.ozel_kdv_tablosu',compute='_get_ozel_kdv', string='Kdv Departmanları')
	devir               = fields.One2many('info_tsm.ynokc_aktivasyon_listesi_dosya_satirlari',compute='_get_devir',string='Devir Tarihçesi')
	satis_yapan_bayi    = fields.Many2one(related='lot_id.company_id',store=True ,string='Satış Yapan Bayi')
	servis_bayi         = fields.Many2one('res.company',string='Servis Veren Bayi',domain=[('partner_id.profil_tipi','>',1)])
	'''
	ozel_urun_gruplari  = fields.One2many(related = 'lot_id.urun_params.ozel_urun_gruplari', string='Ürün Grupları')
	urun                = fields.One2many(related = 'lot_id.urun_params.urun', string='Ürünler')
	
	kur                 = fields.One2many(related = 'lot_id.urun_params.kur',   string='Özel Kur Oranları')
	altin               = fields.One2many(related = 'lot_id.urun_params.altin', string='Özel Altın Fiyatları')
	tahsilat            = fields.One2many(related = 'lot_id.urun_params.tahsilat',   string='Tahsilat Tipleri')
	satis               = fields.One2many(related = 'lot_id.urun_params.satis', string='Satış Tipleri')
	fatura_tahsilat_kurum = fields.One2many(related = 'lot_id.urun_params.fatura_tahsilat_kurum',string='Fatura Tahsilat Kurumları')
	'''
	
	@api.multi
	@api.onchange('servis_bayi')
	def servis_bayi_onchange( self ):
		res = {}
		for s in self:
			if s.servis_bayi:
				res =  s.calc_hakedis()
				servis_ozellik = self.env['info_tsm.okc_servis_ozellik'].search([('lot_id','=',s.lot_id.id)])
				if servis_ozellik:
					servis_ozellik[0].son_kurulum_bayi = s.servis_bayi.id
		return res
	
	def determinate_servis_bayi( self, satis_yapan_bayi, lot_id):
		if lot_id.set_edilen_servis_bayi:
			servis_bayi = lot_id.set_edilen_servis_bayi.id
		elif satis_yapan_bayi:
			if satis_yapan_bayi.partner_id.kurulum_bayi:
				servis_bayi = satis_yapan_bayi.partner_id.kurulum_bayi.company_id.id
			else:
				if satis_yapan_bayi.partner_id.profil_tipi > 1:
					servis_bayi = satis_yapan_bayi.id
				else:
					servis_bayi = 1
		else:
			servis_bayi = False
		
		return servis_bayi

	@api.multi
	@api.onchange('satis_yapan_bayi')
	def _get_servis_bayi(self):
		#print 'servis bayi hesaplaniyore '
		self.ensure_one()
		self.servis_bayi = self.determinate_servis_bayi( self.satis_yapan_bayi.sudo(), self.lot_id.sudo())

	@api.multi
	@api.depends('lot_id')
	def _get_ozel_kdv(self):
		self.ensure_one()
		rel = self.lot_id.sudo().urun_params.ozel_kdv
		self.ozel_kdv = [(5,)]
		rel_list = []
		for r in rel:
			rel_list.append ((4,r.ozel_kdv_rel.id))
		self.ozel_kdv = rel_list
	
	@api.multi
	@api.depends('lot_id')
	def _get_devir( self ):
		devir_kod = self.env['info_tsm.gib_yoknc_status_codes'].search([('durum_kodu','=',2)]) 
		devirler  = self.env['info_tsm.ynokc_aktivasyon_listesi_dosya_satirlari'].search([('name','=',self.lot_id.id),
																						 ('durum_bilgisi','=',devir_kod.id)],order='id desc')
		
		self.devir = [(5,)]
		rel_list = []
		for r in devirler:
			rel_list.append ((4,r.id))
		self.devir = rel_list
	

	def create_new_uyg_group( self, vers_id, current_group, group_name,term_id,set_=True,exclude_ids=[] ):

		vers_id     = list(set(vers_id))
		vers_id.sort()
		exclude_ids = list(set(exclude_ids))
		exclude_ids.sort()

		check_group = []

		check_domain   = [('name','=',current_group.parametre.id),
																('ilk_iletisim','=',current_group.ilk_iletisim.id),
																('haberlesme','=',current_group.haberlesme.id)]
		if set_ :
			check_domain.append(('otomatik','=',True))
			current_uids = map(lambda x:x.id,current_group.prog_version)
			current_uids.sort()
			#print 'First Check:',vers_id , current_uids
			if  vers_id == current_uids:
				check_group = current_group

		if not check_group:
			check_group_sq    = self.env['info_tsm.parametre_grup'].search( check_domain )
			for c in check_group_sq:
				c_prog_ids = map( lambda x:x.id ,c.prog_version)
				c_prog_ids.sort()
				#print 'Second Check:',vers_id,c_prog_ids
				if vers_id == c_prog_ids:
					check_group = c
					break

		if len( check_group ) > 0:
			new_group = check_group[0]
		else:
			new_group      = current_group.copy()
			if set_:
				if group_name:
					new_group.name = group_name
				vs = []
				for v in vers_id:
					vs.append((4,v))
				for v in current_group.prog_version:
					vs.append((4,v.id))

				new_group.prog_version = vs
			new_group.otomatik = True
		current_group.term_id  = [(3,term_id.id)]
		new_group.terminal_id  = [(4,term_id.id)]

		return new_group

	def determinate_uyg_ver( self, key, flightmode=False ):
		new_prog_type = self.env['info_tsm.program_type'].search([('program_id','=',prog_ids[ key ])])
		if len ( new_prog_type ) > 0:
			program       = self.env['info_tsm.programs'].search([('programtype_child','=',new_prog_type[0].id)])
			if len( program )>0:
				uyg_name = ''
				domain = [('program_id','=',program[0].id),('listelere_ekle','=',True)]
				if flightmode:
					domain.append( ('offline_mi','=',True) )
					uyg_name    = u' Offline'
				else:
					domain.append( ('offline_mi','=',False) )
				uyg_id = self.env['info_tsm.program_version'].search(domain,order='major desc, minor desc, build desc, revision desc', limit=1)
				if len( uyg_id ) > 0 :
					return uyg_id[0], uyg_name

		return False,False

	def determinate_banks( self, group_id,exclude_key=False ):
		v_ids  = []
		ex_ids = []
		prog_ids_wo_exclude = group_id.prog_version
		if exclude_key:
			v_ids = map( lambda x:x.id if x.program_id.programtype_child.program_id != prog_ids[ exclude_key ] else None, prog_ids_wo_exclude)
			v_ids = filter(None,v_ids)
			ex_ids = map( lambda x:x.id if x.program_id.programtype_child.program_id == prog_ids[ exclude_key ] else None, prog_ids_wo_exclude)
			ex_ids = filter(None,ex_ids)
		else:
			v_ids  = map(lambda x:x.id,prog_ids_wo_exclude)
			exex   = self.env['info_tsm.program_version'].search([('id','not in', v_ids)])
			ex_ids = map(lambda x:x.id, exex)

		return v_ids,ex_ids

	@api.model
	def create(self, values ):
		print 'TSM Create Başladı'
		res = super( mrp_repair, self).create( values)
		history_obj = self.env['info_tsm.program_version_history'].sudo()
		if res.isWebServiceRequest and (res.isEmriKodu == 'TE' or res.isEmriKodu == 'K' or res.isEmriKodu == '01'):
			res = res.sudo()
			uyg_param = res.lot_id.uygulama_params.parametre

			#uyg_param.flightmode
			if res.ws_caller == '62':#hardcode yazıldı çünkü acquirer_id tutuluyor.
				#garanti
				uyg_id,name_suff = self.determinate_uyg_ver('garanti',uyg_param.flightmode)
				if uyg_id:
					name             = uyg_id.name +  name_suff
					c_uids,ex        = self.determinate_banks( res.lot_id.uygulama_params)
					res.ws_description = res.ws_description + u' Uygulama Kurulum İş Emridir.'

			else:
				#bkm
				uyg_id,name_suff = self.determinate_uyg_ver('techpos',uyg_param.flightmode)
				name             = uyg_id.name +  name_suff
				c_uids,ex        = self.determinate_banks( res.lot_id.uygulama_params)
			if uyg_id:
				#farkli vers varsa elleşme
				uyg_vers       = map ( lambda x:x.id,uyg_id.program_id.versionlar)

				if len(set( c_uids ) & set( uyg_vers )) == 0:
					
					#uyg veya farkli vers grupta yok yani bir takım işlemler yapılacak..
					uyg_id_int       = uyg_id.id
					if len( res.lot_id.sudo().uygulama_params.terminal_id) == 1:
						res.lot_id.sudo().uygulama_params.prog_version = [(4,uyg_id_int)]
						
						history_obj.create({'version_id':uyg_id_int,
										'islem_tipi':'add',
										'lot_id':res.lot_id.id})
					
					#if uyg_id_int in ex:
					#   ex.remove( uyg_id_int )
					#c_uids.append( uyg_id_int )
					#new_group = self.create_new_uyg_group( c_uids, res.lot_id.uygulama_params, res.lot_id.uygulama_params.name + u'-' + name, res.lot_id,exclude_ids=ex)

		
		elif res.isWebServiceRequest and (res.isEmriKodu == 'TS' or res.isEmriKodu == '03'):
			if res.isEmriKodu == 'TS':
				acik_kur = self.search([('lot_id','=',res.lot_id.id),
										('ws_acquirerId','=',res.ws_acquirerId.id),
										 ('isEmriKodu','=','K'),
										 ('state','not in',('done','cancel'))])
				for a in acik_kur:
					#a.action_repair_cancel()
					a.state = 'cancel'
				acik_ekleme = self.search([('lot_id','=',res.lot_id.id),
										('ws_acquirerId','=',res.ws_acquirerId.id),
										 ('isEmriKodu','=','TE'),
										 ('state','not in',('done','cancel'))])
				
				for a in acik_ekleme:
					a.state = 'cancel'                      
				
				
				res.state         = 'done'          
			
			elif res.isEmriKodu == '03':
				acik_kur = self.search([('lot_id','=',res.lot_id.id),
										('ws_caller','=',62),
										 ('isEmriKodu','=','01'),
										 ('state','not in',('done','cancel'))])
				
				for a in acik_kur:
					a.state = 'cancel'
				res.state      = 'done'
				res.ws_description = res.ws_description + u' Uygulama Silme İş Emridir.'

			uyg_ids = []
			ex_ids  = []
			if res.ws_caller == '62':#hardcode yazıldı çünkü acquirer_id tutuluyor.
				#garanti sil
				uyg_ids,ex_ids = self.determinate_banks(res.lot_id.uygulama_params, 'garanti')
			else:
				#bkm sil tüm acquirerlar için ts gelmişse
				
				k_te_ler       = self.search([('isEmriKodu','in',['K','TE']),('lot_id','=',res.lot_id.id)])
				ts_ler         = self.search([('isEmriKodu','in',['TS']),('lot_id','=',res.lot_id.id)])
				
				dict_kte       = {}
				acquirer_ok    = True 
				for kte in k_te_ler:
					if sum( kte.ws_acquirerId.id == p.ws_acquirerId.id for p in k_te_ler ) > sum( kte.ws_acquirerId.id == p.ws_acquirerId.id for p in ts_ler ):
						acquirer_ok = False
						break
				if acquirer_ok:
					uyg_ids,ex_ids = self.determinate_banks(res.lot_id.uygulama_params,'techpos')
			
			if ex_ids:
				if len( res.lot_id.sudo().uygulama_params.terminal_id) == 1:
						
						#res.lot_id.sudo().uygulama_params.prog_version = [(3,ex_ids[-1])]
						res.lot_id.sudo().uygulama_params.silinecek_uygulamalar =  [(4,ex_ids[-1])]
						'''
						history_obj.create({'version_id':ex_ids[-1],
										'islem_tipi':'delete',
										'lot_id':res.lot_id.id})
						'''
		return res

	def send_by_cron(self):
		#print 'CRON  SEND'
		will_send   = self.env['mrp.repair'].search([('cron_send','=',True)])
		
		#print will_send
		for w in will_send:
			try:
				if w.isWebServiceRequest:
					if w.isEmriKapa(state='KB', order_state='done'):
						w.cron_send = False
			except:
				w.cron_send = False
				continue
class program_version_history( models.Model):
	_name = 'info_tsm.program_version_history'

	version_id = fields.Many2one('info_tsm.program_version',string ='İşlem Gören Versiyon')
	islem_tipi = fields.Selection(selection = [ ('add','Cihaza Eklendi'),
									('delete','Cihazdan Çıkarıldı'),
									],string='İşlem Tipi')

	lot_id     = fields.Many2one('stock.production.lot',string='Terminal ID')

class acquirer_history( models.TransientModel):
	_name = 'info_tsm.acquirer_history'

	banka_adi =  fields.Char(string='Banka Adı')
	islem_tipi = fields.Selection(selection = [ ('K','Kurulum'),('01','Kurulum'),('1','Kurulum'),('TE','Terminal Ekleme'),
									('TS','Terminal Silme'),('03','Terminal Silme'),('3','Terminal Silme')],string='İşlem Tipi')

	lot_id     = fields.Many2one('stock.production.lot',string='Terminal ID')
