# -*- coding: utf-8 -*-
from odoo import http
import datetime
from werkzeug.utils import redirect
from openerp.http import request
from collections import OrderedDict
from pysimplesoap.server import SoapDispatcher
import ast
try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET

from openerp.addons.web.controllers.main import serialize_exception,content_disposition
from ..util.util import XmlDictConfig
import time
from ..models.cc_payment import TLSv1_2Adapter
import requests
import json
import functools
from odoo.modules import get_resource_path
import xlwt
import xlsxwriter
import re
from io import StringIO
from io import BytesIO
import base64
VAKIFLARBANKASI = 15
import logging
from odoo import SUPERUSER_ID
_logger = logging.getLogger(__name__)
from odoo.addons.info_bayi.models.webserviceopt import pos_islemleri, set_log,pos_islemleri_2
addons_path = '/opt/odoo12/odoo-custom-addons/info_bayi/views'
#addons_path  = '/var/lib/odoo/addons/10.0/info_bayi/views'
static_path  = '/info_bayi/static/yemekmatik'
pos_img_name = 'POS TALİMATI (KLT.FRM.03-V01-30102018)'
kvkk_name    = 'KVKK AYDINLATMA METNİ (KLT.FRM.02-V01-30102018)'
import odoo
class InfoBayi(http.Controller):
     @http.route('/pos_talimat/', type='http', auth="none", cors="*",csrf=False)
     def troy_sozlesme_sablon(self, **kw):
        imgname = pos_img_name
        imgext = '.pdf'
        placeholder = functools.partial(get_resource_path, 'info_bayi', 'static','sozlesme')
        
        uid = None
        if not uid:
            uid = SUPERUSER_ID
        response = http.send_file(placeholder(imgname + imgext),mimetype='application/pdf',filename='POS TALİMATI (KLT.FRM.03-V01-30102018).pdf')
        
        return response

     @http.route('/sozlesme_image/<s>', type='http', auth="none", cors="*",csrf=False)
     def sozlesme_image(self,s, **kw):
        
        imgname = s
        imgext = '.png'
        placeholder = functools.partial(get_resource_path, 'info_bayi', 'static','sozlesme')
        
        uid = None
        if not uid:
            uid = SUPERUSER_ID
        response = http.send_file(placeholder(imgname + imgext))
        
        return response
    
     @http.route('/kvkk/<s>', type='http', auth="none", cors="*",csrf=False)
     def kvkk(self, s,**kw):
        imgname = kvkk_name
        imgext = '.pdf'
        placeholder = functools.partial(get_resource_path, 'info_bayi', 'static','sozlesme')
        filename = 'KVKK AYDINLATMA METNİ KURUMSAL MÜŞTERİ'
        if s == 'uye':
            filename = 'KVKK AYDINLATMA METNİ ÜYE İŞ YERİ'
        uid = None
        if not uid:
            uid = SUPERUSER_ID
        response = http.send_file(placeholder(imgname + imgext),filename=filename)
        
        return response
    
     @http.route('/belgeler/', type='http', auth="none", cors="*",csrf=False)
     def belgeler(self, **kw):
        imgname = 'ÜYE OLMAK İÇİN İSTENEN EVRAKLAR'
        imgext = '.pdf'
        placeholder = functools.partial(get_resource_path, 'info_bayi', 'static','sozlesme')
        
        uid = None
        if not uid:
            uid = SUPERUSER_ID
        response = http.send_file(placeholder(imgname + imgext),filename='İstenen Belgeler.')
        
        return response
class Binary(http.Controller):
    @http.route('/create_crm_lead',type='http', auth="none", cors="*",csrf=False)
    def create_crm_lead(self,**kw):
        
        kw['date_open'] = datetime.datetime.now()
        new_c = http.request.env['crm.lead'].sudo()
        new_c.create( kw ) 
        
        return "success"
    
    @http.route(['/tesekkurler',
                 '/kampanya/tesekkurler'],type='http', auth="none", cors="*")
    def return_tesekkurler(self,**kw):
        file_ =  open( addons_path + '/tesekkurler.html','r')
        file_in = file_.read()
        file_in = file_in.replace("css/", static_path + "/css/" ).replace("img/", static_path + "/img/" ).replace("js/", static_path + "/js/" ).replace("plugins/", static_path + "/plugins/" )
        
        #print file_in
        
        return  file_in
    @http.route('/kampanya',type='http', auth="none", cors="*")
    def return_landing(self,**kw):
        return redirect( '/' )
    @http.route('/yemekmatik-kampanya',type='http', auth="none", cors="*")
    def return_landing(self,**kw):
        file_ =  open( addons_path + '/yemekmatik_site.html','r')
        file_in = file_.read()
        file_in = file_in.replace("css/", static_path + "/css/" ).replace("img/", static_path + "/img/" ).replace("js/", static_path + "/js/" ).replace("plugins/", static_path + "/plugins/" )
        
        #print file_in
        
        return  file_in
        
        #return redirect( '/' )
    @http.route('/google5e40ffeb076645f7.html',type='http', auth="none", cors="*")
    def return_google(self,**kw):
        return 'google-site-verification: google5e40ffeb076645f7.html'
    @http.route( '/web/binary/company_logo_big', type='http', auth="none", cors="*")
    def company_logo(self, dbname=None, **kw):
        imgname = 'logo_big'
        imgext = '.png'
        placeholder = functools.partial(get_resource_path, 'info_bayi', 'static', 'img')

        '''
        uid = None
        if request.session.db:
            dbname = request.session.db
            uid = request.session.uid
        elif dbname is None:
            dbname = db_monodb()

        if not uid:
            uid = odoo.SUPERUSER_ID
        '''
        #if not dbname:
        #print placeholder(imgname + imgext)
        response = http.send_file(placeholder(imgname + imgext))
        
        return response
    def get_Vakif_payment( self, payload, url,s,payload_obj ):
        ay                          = ["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"]
        MerchantId                  = payload.get("clientid")
        MerchantPassword            = payload.get("key")
        VerifyEnrollmentRequestId   = payload.get("rnd")
        pan                         = payload.get("pan")
        ExpiryDate                  = str(payload.get("Ecom_Payment_Card_ExpDate_Year")) + ay[payload.get("Ecom_Payment_Card_ExpDate_Month")]
        PurchaseAmount              = payload.get("amount")
        Currency                    = payload.get("currency")
        BrandName                   = 100
        SuccessUrl                  = payload.get("okUrl")
        FailureUrl                  = payload.get("failUrl")
        
        if pan.startswith('4')   :
            barnd_name = 100
        elif pan.startswith('5'):
            barnd_name = 200
        else:
            barnd_name = 100
        payloadVakif                    = dict(
            HostMerchantId              = MerchantId,
            HostTerminalId              = 'VP003045',
            MerchantPassword            = MerchantPassword,
            PAN                         = pan,
            ExpireMonth                 = payload.get('Ecom_Payment_Card_ExpDate_Month'),
            ExpireYear                  = '20' + str(payload.get('Ecom_Payment_Card_ExpDate_Year')),
            SuccessUrl                  = SuccessUrl.replace('payresult','vresult'),
            FailUrl                     = FailureUrl.replace('payresult','vresult'),
            AmountCode                  = 949,
            Amount                      = PurchaseAmount,
            OrderID                     = payload.get("oid"),
            TransactionType             = 'Sale',
            IsSecure                    = True,
            AllowNotEnrolledCard        = True,
            CVV                         = payload.get("cv2"),
            CardHoldersName             = payload['holdername'],
            BrandNumber                 = barnd_name
        )
        if payload.get('taksit'):
            payloadVakif ['InstallmentCount'] = payload.get('taksit')
            

        ##print payloadVakif
        resp                = s.post(url, data=payloadVakif)
        
        payload_obj.payload = False

        _logger.info( resp.text )
        response_text = json.loads( resp.text )
        if response_text.get('CommonPaymentUrl') and response_text.get('PaymentToken'):
            payload_obj.payload = response_text.get('PaymentToken')
            return redirect( response_text.get('CommonPaymentUrl') + '?Ptkn='+ response_text.get('PaymentToken') )
            
        else:
            return redirect( '/web/payresult' )
        
    @http.route('/web/odeme_yap/<d>', type='http', auth="user",csrf=False)
    @serialize_exception
    def odeme_yap( self, d=None,**kw):

        if d:
            payload_obj      = http.request.env['info_bayi.odeme_yap'].sudo().browse( int(d) )[0]
            #try:
            url              = payload_obj.url
            url_header       = payload_obj.url_header
            payload          = ast.literal_eval (payload_obj.payload)
        
            #print ">>>> ", payload, payload_obj.bkm_id

            s = requests.Session()
            s.mount('https://', TLSv1_2Adapter())

            if payload_obj.bkm_id == VAKIFLARBANKASI:
                donen = self.get_Vakif_payment( payload,url,s,payload_obj ) 
                return donen

            else:
                #print 'resp  ---> ', url
                resp = s.post(url, data=payload)
                payload_obj.payload = False

                ##print resp.text
                text = resp.text
                text = text.replace('rel="stylesheet" href="','rel="stylesheet" href="%s'%url_header )
                text = text.replace('<img src="','<img src="%s'%url_header )
                #payload_obj.payload = False
                ###print text
                return text

        else:
            return False

    @http.route('/web/pay_test/<d>', type='http', auth="user",csrf=False)
    @serialize_exception
    def pay_test( self,d,**kw):

        ##print "pay_test"

        payload_obj = http.request.env['info_bayi.odeme_yap'].sudo().browse( int(d) )[0]
        kw={}
        kw['Response'] = 'Approved'
        kw['oid']      = payload_obj.name
        param = http.request.env["ir.config_parameter"]
        href_g  = param.get_param("web.base.external_url", default = None)
        
        template='info_bayi.odeme_yap'
        msg         = 'Bir Hata Oluştu'
        vendor_msg  = 'Bankadan Onay Alınamadı, Bankanızı Arayınız.'
        image       = '../../info_bayi/static/src/cros.png'
        makbuz_url  = ''
        state       = 'basarisiz'

        if 'Response' in kw and 'oid' in kw:

            payload_obj = http.request.env['info_bayi.odeme_yap'].sudo().search( [('name','=',kw['oid'])] )[0]

            if kw['Response'] != 'Approved':
                msg = 'Ödeme Başarısız'
                prefix = ''
                if 'mdStatus' in kw:
                    payload_obj.hata_kodu = kw["mdStatus"]
                    prefix = 'Hata Kodu : ' + kw["mdStatus"] +" - "
                payload_obj.hata_msg  = kw['ErrMsg']
                vendor_msg = prefix + 'Hata:' + kw["Response"] +"<br />"+kw['ErrMsg']
                image      = '../../info_bayi/static/src/cros.png'
                href       = ''
                makbuz_url = ''
                state       = 'basarisiz'
            else:
                msg         = 'Ödeme Başarılı'
                vendor_msg  = 'Teşekkür Ederiz'
                image       = '../../info_bayi/static/src/icon.png'
                makbuz_url  = "document.location.href='" + href_g + "/report/pdf/info_bayi.report_makbuz/" + str(payload_obj.id) + "'";
                state       = 'basarili'
                
                ##print 'Cariden Düşülecek : ',payload_obj.cariden_dusulecek
                '''
                if not payload_obj.sudo().temp_payment:
                    if payload_obj.payment:
                        payload_obj.payment[-1].post()
                        payload_obj.sudo().komisyon_mahsuplastir(payload_obj.sudo().payment, payload_obj.komisyon, payload_obj.komisyonfarki )
                else:
                    
                    payload_obj.sudo().temp_payment.payment.kk_odeme = payload_obj.id
                    payload_obj.sudo().temp_payment.mahsup_senaryo = 2
                    payload_obj.sudo().temp_payment.amount = payload_obj.cariden_dusulecek
                    payload_obj.sudo().temp_payment.onayla()
                
                    payload_obj.sudo().komisyon_mahsuplastir(payload_obj.sudo().temp_payment.payment, payload_obj.komisyon, payload_obj.komisyonfarki )
                '''    
            payload_obj.payload = False
            payload_obj.bank_state = state

        return  http.request.render(template, {
                    'msg': msg,
                    'vendor_msg':vendor_msg,
                    'image':image,
                    'makbuz_url':makbuz_url
        })

    @http.route('/web/vresult/', type='http', auth="public",csrf=False)
    @serialize_exception
    def see_pay_v( self,*args,**kw):
        
        # kw, args
        
        ##print "see_pay_v"
        param       = http.request.env["ir.config_parameter"]
        href_g      = param.get_param("web.base.external_url", default = None)
        
        template    = 'info_bayi.odeme_yap'
        msg         = 'Bir Hata Oluştu'
        vendor_msg  = 'Bankadan Onay Alınamadı, Bankanızı Arayınız.'
        image       = '/info_bayi/static/src/cros.png'
        makbuz_url  = ''
        state = 'basarisiz'

        if 'Message' in kw and 'PaymentToken' in kw:

            payload_obj = http.request.env['info_bayi.odeme_yap'].sudo().search( [('payload','=',kw['PaymentToken'])] )
            
            if payload_obj:
                payload_obj = payload_obj[0]

            if kw.get('ErrorCode'):
                msg = 'Ödeme Başarısız'
                state = 'basarisiz'
                prefix = ''
               
                payload_obj.hata_kodu = kw["ErrorCode"]
                prefix = 'Hata Kodu : ' + kw["ErrorCode"] +" - "
                payload_obj.hata_msg  = kw['Message']
                vendor_msg = prefix  +"<br />"+kw['Message']
                image      = '/info_bayi/static/src/cros.png'
            else:
                msg         = 'Ödeme Başarılı'
                vendor_msg  = 'Teşekkür Ederiz <br /> Otorizasyon Kodu: ' + kw['AuthCode']  
                image       = '/info_bayi/static/src/icon.png'
                makbuz_url  = "document.location.href='" + href_g + "/report/pdf/info_bayi.report_makbuz/" + str(payload_obj.id) + "'";
                state = 'basarili'

                try:
                    if not payload_obj.sudo().temp_payment:
                        if payload_obj.payment:
                            payload_obj.payment[-1].post()
                            payload_obj.sudo().komisyon_mahsuplastir(payload_obj.sudo().payment, payload_obj.komisyon, payload_obj.komisyonfarki )
                    else:
                        
                        payload_obj.sudo().temp_payment.payment.kk_odeme = payload_obj.id
                        payload_obj.sudo().temp_payment.mahsup_senaryo = 2
                        payload_obj.sudo().temp_payment.amount = payload_obj.cariden_dusulecek
                        payload_obj.sudo().temp_payment.onayla()
                    
                        payload_obj.sudo().komisyon_mahsuplastir(payload_obj.sudo().temp_payment.payment, payload_obj.komisyon, payload_obj.komisyonfarki )
                except:
                    pass
            payload_obj.payload = False
            payload_obj.bank_state = state
    
        return  http.request.render(template, {
                'msg': msg,
                'vendor_msg':vendor_msg,
                'image':image,
                'makbuz_url':makbuz_url
        })
    
    @http.route('/web/payresult/', type='http', auth="public",csrf=False)
    @serialize_exception
    def see_pay( self,*args,**kw):

        #print "see_pay"
        param       = http.request.env["ir.config_parameter"]
        href_g      = param.get_param("web.base.external_url", default = None)
        
        template    = 'info_bayi.odeme_yap'
        msg         = 'Bir Hata Oluştu'
        vendor_msg  = 'Bankadan Onay Alınamadı, Bankanızı Arayınız.'
        image       = '/info_bayi/static/src/cros.png'
        makbuz_url  = ''
        state = 'basarisiz'

        if 'Response' in kw and 'oid' in kw:
            #print oid
            payload_obj = http.request.env['info_bayi.odeme_yap'].sudo().search( [('name','=',kw['oid'])] )
            #print payload_obj
            if payload_obj:
                payload_obj = payload_obj[0]

                if kw['Response'] != 'Approved':
                    msg = 'Ödeme Başarısız'
                    state = 'basarisiz'
                    prefix = ''
                    if 'mdStatus' in kw:
                        payload_obj.hata_kodu = kw["mdStatus"]
                        prefix = 'Hata Kodu : ' + kw["mdStatus"] +" - "
                    payload_obj.hata_msg  = kw['ErrMsg']
                    vendor_msg = prefix + 'Hata:' + kw["Response"] +"<br />"+kw['ErrMsg']
                    image      = '/info_bayi/static/src/cros.png'
                else:
                    msg         = 'Ödeme Başarılı'
                    vendor_msg  = 'Teşekkür Ederiz'
                    image       = '/info_bayi/static/src/icon.png'
                    makbuz_url  = "document.location.href='" + href_g + "/report/pdf/info_bayi.report_makbuz/" + str(payload_obj.id) + "'";
                    state = 'basarili'
                    '''
                    try:
                        if not payload_obj.sudo().temp_payment:
                            if payload_obj.payment:
                                payload_obj.payment[-1].post()
                                payload_obj.sudo().komisyon_mahsuplastir(payload_obj.sudo().payment, payload_obj.komisyon, payload_obj.komisyonfarki )
                        else:
                            
                            payload_obj.sudo().temp_payment.payment.kk_odeme = payload_obj.id
                            payload_obj.sudo().temp_payment.mahsup_senaryo = 2
                            payload_obj.sudo().temp_payment.amount = payload_obj.cariden_dusulecek
                            payload_obj.sudo().temp_payment.onayla()
                        
                            payload_obj.sudo().komisyon_mahsuplastir(payload_obj.sudo().temp_payment.payment, payload_obj.komisyon, payload_obj.komisyonfarki )
                    except:
                        pass
                    '''
                payload_obj.payload = False
                payload_obj.bank_state = state
        elif 'TURKPOS_RETVAL_Sonuc' in kw and kw.get('TURKPOS_RETVAL_Siparis_ID'):
            
            payload_obj =   http.request.env['info_kart.set_limit'].sudo().search( [('siparis_no','=',kw.get('TURKPOS_RETVAL_Siparis_ID'))] )
            
            if kw.get('TURKPOS_RETVAL_Sonuc') and int(kw.get('TURKPOS_RETVAL_Sonuc')) != 1:
                msg = 'Ödeme Başarısız'
                state = 'basarisiz'
                vendor_msg = kw['TURKPOS_RETVAL_Sonuc_Str']
                image      = '/info_bayi/static/src/cros.png'
                payload_obj.state='cancel'
                payload_obj.kk_state = vendor_msg
            else:
                msg         = 'Ödeme Başarılı'
                vendor_msg  = 'Dekont No : ' + kw['TURKPOS_RETVAL_Dekont_ID']
                image       = '/info_bayi/static/src/icon.png'
                #makbuz_url  = "document.location.href='" + href_g + "/report/pdf/info_bayi.report_makbuz/" + str(payload_obj.id) + "'";
                state = 'basarili'
                payload_obj.state='confirm'
                payload_obj.kk_state = vendor_msg
                payload_obj.confirm()
        
        elif 'inner_message' in kw:
            
            if kw.get('success') and int(kw.get('success')) != 1:
                msg = 'İşlem Başarısız'
                state = 'basarisiz'
                vendor_msg = kw['msj']
                image      = '/info_bayi/static/src/cros.png'
                
            else:
                msg         = 'İşlem Başarılı'
                vendor_msg  = kw['msj']
                image       = '/info_bayi/static/src/icon.png'
                #makbuz_url  = "document.location.href='" + href_g + "/report/pdf/info_bayi.report_makbuz/" + str(payload_obj.id) + "'";
                
            
        return  http.request.render(template, {
                'msg': msg,
                'vendor_msg':vendor_msg,
                'image':image,
                'makbuz_url':makbuz_url
        })

    @http.route('/web/satis_ok/<d>', type='http', auth="user",csrf=False)
    @serialize_exception
    def satis_ok( self, d=None,**kw):

        param       = http.request.env["ir.config_parameter"]
        href_g      = param.get_param("web.base.external_url", default = None)
        href        = "this.window.opener.location.href='"+href_g+""
        template    ='info_bayi.odeme_yap'
        msg         = 'Satış Başarılı'
        vendor_msg  = 'Teşekkür Ederiz'
        image       = '/info_bayi/static/src/icon.png'

        return  http.request.render(template, {
            'msg': msg,
            'vendor_msg':vendor_msg,
            'image':image,
            'href':"javascript:%s/web';this.window.close();"%href,
            'makbuz_url':''
        })
    @http.route('/web/iade_ok/<d>', type='http', auth="user",csrf=False)
    @serialize_exception
    def iade_ok( self, d=None,**kw):

        param       = http.request.env["ir.config_parameter"]
        href_g      = param.get_param("web.base.external_url", default = None)
        href        = "this.window.opener.location.href='"+href_g+""
        template    = 'info_bayi.odeme_yap'
        msg         = 'İade Alımı Başarılı'
        vendor_msg  = 'Muhasebe İade Faturası Oluşturuldu, İade Faturası Onaylanana Kadar, İlgili bakiyede değişiklik OLMAZ!!'
        image       = '/info_bayi/static/src/icon.png'

        return  http.request.render(template, {
            'msg': msg,
            'vendor_msg':vendor_msg,
            'image':image,
            'href':"javascript:%s/web';this.window.close();"%href,
            'makbuz_url':''
        })

class WSLogParser(http.Controller):
    @http.route('/web/logreport',type='http',csrf=False,auth='user')
    @serialize_exception

    def ws_log_parser(self,**kw):
        if http.request.httprequest.method == "POST":
            where = []
            if kw.get('date_start'):
                where.append( ('requestTime','>=',kw['date_start']))
            if kw.get('date_end'):
                where.append(('requestTime','<=',kw['date_end']))
            if kw.get('seri_no'):
                where.append(('requestData','ilike',kw['seri_no']))

            if not where:
                one_month = datetime.timedelta(days = 5)
                date      = datetime.datetime.now() - one_month
                date      = date.strftime('%Y-%m-%d')
                where     = [('requestTime','>=',date)]

        else:
            one_month = datetime.timedelta(days = 5)
            date      = datetime.datetime.now() - one_month
            date      = date.strftime('%Y-%m-%d')
            where     = [('requestTime','>=',date)]

        ##print where


        logs      = http.request.env['info_bayi.turkpara_ws_logs'].sudo().search( where )
        res_dict_t  = {}
        req_dict_t  = {}

        for l in logs:
            level   = 0
            level_r = 0

            l.requestData = l.requestData.replace('<?xml version="1.0" encoding="UTF-8"?><?xml version="1.0" encoding="utf-8"?>','')
            l.responseData = l.responseData.replace('<?xml version="1.0" encoding="UTF-8"?><?xml version="1.0" encoding="utf-8"?>','')
            if l.requestData.find( '<?xml version="1.0"' ) < 0:
                l.requestData = u'<?xml version="1.0" encoding="UTF-8"?>' + l.requestData

            if l.responseData.find( '<?xml version="1.0"' ) < 0:
                l.responseData = u'<?xml version="1.0" encoding="UTF-8"?>' + l.responseData

            try:
                tree_req      = ET.XML(l.requestData.encode('utf-8'))
                req_dict      = XmlDictConfig(tree_req)
                l.req_dict    = req_dict
                level = 1
            except:
                data_tf      = l.requestData.replace('<?xml version="1.0" encoding="utf-8"?>','').replace('<?xml version="1.0" encoding="UTF-8"?>','')
                data_tf_dict = ast.literal_eval( data_tf )
                if isinstance( data_tf_dict, dict):
                    l.req_dict = data_tf_dict
                    req_dict   = data_tf_dict
                    level = 2
                else:
                    l.req_dict   = 'Fortmatı Hatalı İstek!!! Ayrıştırılmamış Loga Bakınız.'
                    req_dict     = None

            try:
                tree_resp     = ET.XML(l.responseData.encode('utf-8'))
                resp_dict     = XmlDictConfig(tree_resp)
                l.resp_dict   = resp_dict
                level_r       = 1
            except:
                data_lf      = l.responseData.replace('<?xml version="1.0" encoding="utf-8"?>','').replace('<?xml version="1.0" encoding="UTF-8"?>','')
                data_lf_dict = ast.literal_eval( data_lf )
                if isinstance( data_lf_dict, dict):
                    l.resp_dict = data_lf_dict
                    resp_dict   = data_lf_dict
                    level_r     = 2
                else:
                    l.resp_dict   = 'Fortmatı Hatalı Cevap!!! Ayrıştırılmamış Loga Bakınız.'
                    resp_dict     = None

            if req_dict:
                if level == 1:
                    l.func_type      = req_dict['Body'].keys()[0]
                    req_dict_t[l.id] = req_dict['Body'][l.func_type]

                    if req_dict['Body'][ l.func_type ]['kurumKodu'] == 'BKM':
                        l.serviceName = 'TechPos'
                    else:
                        l.serviceName = 'InfoPos'
                elif level == 2:
                    l.func_type   = 'IsEmriKapama'
                    l.serviceName = 'BKM'
                    req_dict_t[l.id] = req_dict
                else:
                    req_dict_t[l.id] = {}
            else:
                req_dict_t[l.id] = {}

            if resp_dict:
                if level_r == 1:
                    l.func_resp_type       = resp_dict['Body'].keys()[0]
                    l.func_resp_sub_type   = resp_dict['Body'][l.func_resp_type].keys()[0]
                    if not isinstance ( resp_dict['Body'][l.func_resp_type][l.func_resp_sub_type] ,XmlDictConfig) :
                        res_dict_t[l.id] = resp_dict['Body'][l.func_resp_type]
                    else:
                        res_dict_t[l.id]       = resp_dict['Body'][l.func_resp_type][l.func_resp_sub_type]

                elif level_r == 2:
                    res_dict_t[l.id] = resp_dict [resp_dict.keys()[0]]
                else:
                    res_dict_t[l.id] = {'cevapKodu':'001'}
            else:
                res_dict_t[l.id] = {'cevapKodu':'001'}

            if not res_dict_t[l.id].get('cevapKodu',False):
                res_dict_t[l.id].update({'cevapKodu':'001'})

            l.resp_time_formatted = time.strftime ('%d/%m/%Y %H:%M:%S',time.strptime(l.responseTime,'%Y-%m-%d %H:%M:%S'))
            l.req_time_formatted  = time.strftime ('%d/%m/%Y %H:%M:%S',time.strptime(l.requestTime,'%Y-%m-%d %H:%M:%S'))


        return  http.request.render('bayi.servis_logging', {
                'logs' : logs,
                'resp_dict':res_dict_t,
                'req_dict' :req_dict_t,
            })


        return logs

class Bayi(http.Controller):
    
    @http.route('/yemekmatikbanka', auth='public',csrf=False)
    def service_index_banka(self, **kw):
        param = http.request.env["ir.config_parameter"]
        def get_partner_by_token( kurum_kodu, token ):
            #print kurum_kodu, token
            harici_firma_obj = http.request.env['info_bayi.harici_firmalar'].sudo().search([('kurum_kodu','=',kurum_kodu),('kurum_token','=',token)])
            if len(harici_firma_obj) == 1:
                harici_firma_obj = harici_firma_obj[0].id
            else:
                harici_firma_obj = None
            return harici_firma_obj
        
        def Pos_Iptal(**kw):
            tokenChek = get_partner_by_token(kw.get('kurum_kodu'),kw.get('kurum_token'))
            if not tokenChek:
                return {'Pos_IptalResponse': {
                                            'result':-1,
                                            'result_desc':'Tanımsız Kurum Kodu / Token',
                                            'proc_id':kw.get('proc_id')
                                            }}
            pos_obj = http.request.env['info_bayi.turkpara_banka_rel'].sudo().search([('uye_isyeri_no','=',kw.get('uye_isyeri_no')),
                                                                                      ('terminal_no','=',kw.get('pos_id')),
                                                                                      ('state','in',('confirmed','aktif_edildi'))])
            
            if not pos_obj:
                return {'Pos_IptalResponse': {
                                'result':-102,
                                'result_desc':'Aktif Pos Bilgisi Bulanamadı',
                                'proc_id':kw.get('proc_id')
                                }}
            else:
                p = pos_obj[0]
                p.islem_tipi = '3'
                pos_islemleri_2( p )
                if p.state == 'error':
                    return {'Pos_IptalResponse': {
                            'result':-1,
                            'result_desc':p.ws_hata_mesaji,
                            'proc_id':kw.get('proc_id')
                            }}
                else:
                    return {'Pos_IptalResponse': {
                                'result':1,
                                'result_desc':'İşlem Başarılı',
                                'proc_id':kw.get('proc_id')
                                }}
        
        def Pos_Supurme_Bildirim(**kw):
            tokenChek = get_partner_by_token(kw.get('kurum_kodu'),kw.get('kurum_token'))
            if not tokenChek:
                return {'Pos_Supurme_BildirimResponse': {
                                            'result':-1,
                                            'result_desc':'Tanımsız Kurum Kodu / Token',
                                            'proc_id':kw.get('proc_id')
                                            }}
            pos_obj = http.request.env['info_bayi.turkpara_banka_rel'].sudo().search([('uye_isyeri_no','=',kw.get('uye_isyeri_no')),
                                                                                      ('state','in',('confirmed','aktif_edildi'))])
            
            if not pos_obj:
                return {'Pos_IptalResponse': {
                                'result':-101,
                                'result_desc':'Üye İş Yeri Bulunamadı',
                                'proc_id':kw.get('proc_id')
                                }}
            else:
                p = pos_obj[0]
                new_p = p.copy({'terminal_no':kw.get('pos_id'),
                                'islem_tipi':"1",
                                'pos_markasi':False,
                                'son_gun_sonu':False,
                                'ws_hata_mesaji':False,
                                'supurme_durumu':'v',
                                'pos_goruntu':False,
                                'pos_slip_durumu':False,
                                'red_aciklama':False,
                                'proc_id':False,
                                'red_aciklamasi':False})
                pos_islemleri_2( new_p )
                if new_p.state == 'error':
                    return {'Pos_IptalResponse': {
                            'result':-1,
                            'result_desc':new_p.ws_hata_mesaji,
                            'proc_id':kw.get('proc_id')
                            }}
                else:
                    return {'Pos_IptalResponse': {
                                'result':1,
                                'result_desc':'İşlem Başarılı',
                                'proc_id':kw.get('proc_id')
                                }}        

        dispatcher = SoapDispatcher(
            'soap_service',
            location = param.get_param( "web.base.external_url", default='http://localhost:8069') + '/yemekmatikbanka/',
            action = param.get_param( "web.base.external_url", default='http://localhost:8069') + '/yemekmatikbanka/',
            #namespace = "http://esintegration.infoteks.com.tr/",
            #prefix='tns',
            ns = None)

        # register func
        dispatcher.register_function('Pos_Iptal', Pos_Iptal,
            returns=OrderedDict([('Pos_IptalResponse', OrderedDict(
                                                [('result', int),('result_desc', str),('proc_id',int)])
                                    )]),
            args=OrderedDict([('kurum_kodu',str),('kurum_token',str),('proc_id',int),('uye_isyeri_no',str),('pos_id',str)])
        )
        dispatcher.register_function('Pos_Supurme_Bildirim', Pos_Supurme_Bildirim,
            returns=OrderedDict([('Pos_Supurme_BildirimResponse', OrderedDict(
                                                [('result', int),('result_desc', str),('proc_id',int)])
                                    )]),
            args=OrderedDict([('kurum_kodu',str),('kurum_token',str),('proc_id',int),('uye_isyeri_no',str),('pos_id',str)])
        )
        if http.request.httprequest.method == "POST":

            time_request    = datetime.datetime.today()
            response        = http.request.make_response(dispatcher.dispatch(http.request.httprequest.data))
            time_response   = datetime.datetime.today()
            r_data          = response.data
            requestData     = http.request.httprequest.data
            
            set_log (http.request, requestData, r_data,'Banka','',time_request)

        else:
            response = http.request.make_response(dispatcher.wsdl())

        response.headers['Content-Type'] = 'text/xml'
        return response

    @http.route('/komisyonservice', auth='public',csrf=False)
    def service_index_ym(self, **kw):
        param = http.request.env["ir.config_parameter"]

        dispatcher = SoapDispatcher(
            'soap_service',
            location = param.get_param( "web.base.external_url", default='http://localhost:8069') + '/komisyonservice/',
            action = param.get_param( "web.base.external_url", default='http://localhost:8069') + '/komisyonservice/',
            #namespace = "http://esintegration.infoteks.com.tr/",
            #prefix='tns',
            ns = None)

        # register func
        dispatcher.register_function('KomisyonGuncelle', basvuru_kayit_sorgu,
            returns=OrderedDict([('KomisyonGuncelleResponse', OrderedDict(
                                                [('basvuruId', str),('cevapKodu', str), ('cevapAciklama', str)])
                                    )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str),('basvuruId',str),('basvuruDurum',str),('basvuruStr',str),('kartNo',str)])
        )
        if http.request.httprequest.method == "POST":

            time_request = datetime.datetime.today()
            response = http.request.make_response(dispatcher.dispatch(http.request.httprequest.data))
            time_response = datetime.datetime.today()
            logging_var={'serviceName':'yemekmatikservice','operationName':'',
                         'requestTime':time_request,'responseTime':time_response,
                         'requestData':http.request.httprequest.data,'responseData':response.data
                         }
            logging_obj = http.request.env['info_bayi.turkpara_ws_logs']
            logging_obj.sudo().create(logging_var)

        else:
            response = http.request.make_response(dispatcher.wsdl())

        response.headers['Content-Type'] = 'text/xml'
        return response
    @http.route('/web/fp/<d>/', type='http', auth="public")
    @serialize_exception
    def reciprt_prew(self,d=None,**kw):
        res = None
        res_object  = http.request.env['res.partner'].sudo().browse ( int(d) )
        if len(res_object) > 0:
            res = res_object[0]

            fis_baslik = res.firma_unvani and res.firma_unvani[:39]
            name       = res.name and res.name[:39]

            adres1 = ''
            if res.street:
                adres1 = res.street
            if res.street2:
                adres1 = adres1 +'' +res.street2

            if adres1:
                adres_splited = adres1.split(' ')
                adres1 = ''
                adres2 = ''
                for l in range(0,len(adres_splited)):
                   adres1 += adres_splited[ l ] + ' '
                   if len(adres1) > 37:
                         adres1 = adres1.replace (  adres_splited[ l ] + ' ' , '')
                         adres2 +=  adres_splited[ l ] + ' '
                   if len(adres2) > 37:
                      adres2 = adres2.replace (  adres_splited[ l ] + ' ' , '')


                adres1 = adres1[0:len(adres1)-1]
                if adres2:
                    adres2 = adres2[0:len(adres1)-1]

            adres3 = res.ilce.name + '/' + res.city_combo.name + ' ' + str(res.phone or '')
            ##print adres3
            vergidairesi = ''
            if res.taxAgent:
                vergidairesi = res.taxAgent.name
                splitted_vd  = vergidairesi.upper().split('VERG')
                suffx = ''
                if len(splitted_vd) > 1: suffx  = ' V.D.'
                vergidairesi = splitted_vd[0].lower().capitalize() + suffx
                vergidairesi = vergidairesi[:24] +' '+res.taxNumber 
            slip_top    = res.slip_top and res.slip_top[:39]
            slip_bottom = res.slip_bottom and res.slip_bottom[:39]
            if res.mersisNumber:
                mersis_no   = "Mersis No: " + res.mersisNumber
            else:
                mersis_no  = False
            web         = res.website
        ##print 'Adres 2 : ',len(adres2)
        return  http.request.render('info_bayi.fis_onizleme', {
            'fis_baslik' : fis_baslik,
            'name' : name,
            'adres1' : adres1,
            'adres2' : adres2,
            'adres3' : adres3,
            'vergidairesi':vergidairesi,
            'slip_top':slip_top,
            'slip_bottom':slip_bottom,
            'mersis_no':mersis_no,
            'web':web


        })
    
    @http.route('/web/dl/<s>/<d>/<f>/<n>', type='http', auth="public")
    @serialize_exception
    def dl_f(self,s,d,f,n=None,**kw):
        model_obj = http.request.env[ s ].sudo().browse( int(d))
        if not n:
            filename = '%s_%s' % (s.replace('.', '_'), d)
        if not hasattr (model_obj,f):
            return request.not_found()
        else:
            dosya       = getattr(model_obj,f)
            filecontent = base64.b64decode( dosya )
            return request.make_response(filecontent,
                    [('Content-Type', 'application/octet-stream'),
                     ('Content-Disposition', content_disposition(n))])
    
    @http.route('/web/pos_mail_preview/<d>', type='http', auth="user", csrf=False)
    @serialize_exception
    def pos_mail_preview(self,d=None, **kw):
        text = u'Önizleme Yapılamıyor. Pos Slip Örneklerinin Doğru Eklendiğinden Emin olunuz.'
        if d:
            partner =  http.request.env['res.partner'].sudo().browse([ int(d) ])
            text_donen = partner.bankaya_pos_gonder({},return_only=True)
            if text_donen:
                text = text_donen
        return text
    
    @http.route('/web/payment_terms', type='http', auth="public", csrf=False)
    @serialize_exception
    def payment_terms(self, **kw):
        return u'''<h1>Gizlilik</h1>
            <p>Sitemiz aracılığıyla girdiğiniz bilgiler, sadece size aittir ve tarafımızdan kullanılmaz. Bu verilerin güvenliğinin sağlanması için endüstri standardı önlemler uygulanır. Site ile iletişiminiz SSL protokolü ile şifrelenmektedir.</p>
            <p>Kredi kartı bilgileriniz sadece ödeme işlemlerinde, bir kerelik kullanım için talep edilmekte ve kesinlikle sistemde saklanmamaktadır.</p>
            <p>Site üzerinde yapılan sayfa gezinme işlemleri kayıt altına alınmaktadır, bu bilgiler kişi ayırt edici değildir ve sadece sitenin geliştirilmesi ve daha iyi hizmet vermesi amacıyla toplanmaktadır.</p>
            <p>Olası kötü niyetli kullanımı önlemek için ödeme işlemlerinin yapıldığı tarih, saat ve IP (İnternet) adresi kaydedilmektedir. Kredi kartı kullanırken ortaya çıkabilecek kötü niyetli kullanımda bu bilgiler olası bir hukuki süreçte delil olarak kullanılabilecektir.</p>
            <p>Yemekmatik A.Ş. girilen kullanıcı bilgilerini kesinlikle üçüncü şahıslarla paylaşmayacaktır.</p>
            <h2>Güvenlik</h2>
            <p>Sitemizi kullandığınız sürece girdiğiniz bilgiler TLS v1.2 kullanılarak şifrelenmekte ve bilgilerinizin güvenliği maksimum düzeyde tutulmaktadır.</p>'''

    @http.route('/web/cancellation_return_policy', type='http', auth="public", csrf=False)
    @serialize_exception
    def cancellation_return_policy(self, **kw):
        return u'''
            <h1>İptal İade Koşulları</h1>
            <h3>Sipariş İptali Şartları</h3>
            <p>
                <li>Kullanıcı siparişini, kargo yola çıkmadan önce sipariş numarasını ileterek info@yemekmatik.com.tr e-posta adresine mail atarak veya diğer iletişim bilgilerini kullanarak iptal edebilir.</li>
                <li>Sipariş iptal edildiğinde kullanıcının ödemesi aynı yolla iade edilir.</li>
            </p>
            <h3>İade Şartları</h3>
            <p>Müşteri memnuniyeti politikamız çerçevesinde hizmet vermeye çalıştığımız bu sitede karşılaştığınız herhangi bir problemde sizlere daha iyi ve hızlı bir şekilde yardımcı olabilmemiz aşağıdaki hususlara uyulmasına bağlıdır; </p>
            <p>
                <li>Satılan ürünün iade edilebilmesi için müşterinin siparişi kendisine teslim edildiği anda kargo yetkilisi nezdinde kontrol etmesi gerekir. Kargo paketinde ya da ambalajında herhangi bir bozulma durumu varsa ürünü teslim almadan tutanak tutturarak iade işlemini yapmalıdır. ürünü teslim aldıktan sonra ürüne kargo sırasında zarar verildiği iddiası ile yapılan iadeler kabul edilemez.</li>
                <li>Satılan ve kargo paketi kabul edilerek kullanılmaya başlanan ürünlerin iade edilebilmesi için ürünün üretimden kaynaklanan bir hata nedeniyle kullanılamaz durumda olması gerekmektedir.</li>
                <li>Ürün ile ilgili herhangi bir problem yaşanmamasına karşın, müşteri hizmet kalitesi politikamız çerçevesinde ürün ambalajı açılmadığı ve kullanılmadığı takdirde, ürün teslim tarihinden itibaren 7(yedi)gün içinde teslim aldığınız şekli ile iade edebilirsiniz. İade talebi iletilerek bu talep kabul edildikten sonra ürün iade alınabilir. ürün iade edilirken kargo ile gönderiliyorsa kargo ücreti müşteriye aittir.</li>
            </p>'''

    @http.route('/web/warranty_service_contract', type='http', auth="public", csrf=False)
    @serialize_exception
    def warranty_service_contract(self, **kw):
        return u''' <h1>Garanti Şartları & Hizmet Sözleşmesi</h1>
            <p>
                <li>Sitemizde satışa sunulmuş olan ürünlerin tamamı İTHALATÇI veya ÜRETİCİ FİRMA garantisi altındadır.</li>
                <li>Ürünün Garantisi; ürün üzerinde veya paketi içerisinde yazılı kullanma talimat ve prosüdürüne göre uyulması halinde geçerlidir.</li>
                <b>Arızalı ürün tamir veya değişim süreci aşağıda belirtilen hususlar doğrultusunda uygulanmaktadır.</b></li>
                <li>Satın almış olduğunuz ürün fatura tarihinden itibaren ilk 7 (Yedi) gün içerisinde ürünü hiç kullanmadan arızalanmış veya kutudan arızalı veya fabrikasyon hatalı çıkmış ise; ürün tarafımıza mail veya telefon ile hatasının veya arızasının ne olduğuna dair bir bilgilendirme yapılmalıdır.</li>
                <li>Ürün arızası İTHALATÇI veya ÜRETİCİ firmanın teknik servisine bizim tarafımızdan bildirildikten sonra, size bildirecek yönergeye göre ürünü arıza notu ile birlikte tarafımıza gönderilecektir.</li>
                <li>Tarafımıza ulaşan arızalı ürün teste teste tabi tutulur. Teknik servis arızalı olduğu tespit edilen ürünün; stok durumu müsait olup olmadığı kontrol eder, stokta mevcut ise birebir değişim yapılarak tekrar ürün sahibine gönderilir. Stok durumu müsait olmadığı durumlarda ürün sahibi ile irtibata geçilerek ya ürün iade alınır veya müşterinin onayının ardından muadili veya benzeri bir ürün ile değişim yapılarak ürün sahibine gönderilir. Bu tip arıza durumunda, arızalı ürünün tarafımıza 2 nüsha orjinal faturası, tüm ek, ilave donanımlarıyla beraber ve orjinal ambalajı ile eksiksiz gönderilmesi gerekmektedir. Bu süreç 1 (Bir) ila 3 (üç) iş günü sürebilmektedir.</li>
                <li>Bu süreçteki arızalı ürünün MÜŞTERİDEN-TEKNİK SERVİSE ULAŞIM bedeli tarafımızca karşılanacak olup, TEKNİK SERVİSTEN- MÜŞTERİYE ULAŞIM bedeli müşteri tarafından ödenecektir. Bu gönderinin ulaşım bedeli tarafımızca yapılacağından, gönderinin anlaşmalı olduğumuz KARGO şirketi tarafından ulaştırılması halinde ürün teknik servisimizce kabul edilecektir.  </li>
            </p>
            <p><b>Lütfen dikkat!</b> Anlaşmalı olmayan kargo şirketiyle gönderi yapmayınız. Bu tip kargolar teslim alınmaz ve ürün müşteriye iade edilmek üzere kargo firmasına geri verilir.</p>'''

    
    
    def from_data_custom(self, fields, rows,model):
        fp         = StringIO()
        workbook   = xlsxwriter.Workbook(fp)
        worksheet  = workbook.add_worksheet()
        bold       = workbook.add_format({'bold': True,'text_wrap': True})
        base_style = workbook.add_format({'text_wrap': True})
        format_2 = workbook.add_format({'bold': True,'text_wrap': True})
        format_2.set_align('center')
        format_2.set_align('vcenter')
        datetime_style = workbook.add_format({'text_wrap': True, 'num_format':'DD-MM-YYYY HH:mm:SS'})
        date_style     = workbook.add_format({'text_wrap': True, 'num_format':'DD-MM-YYYY'})
        date_style.set_align('vcenter')
        datetime_style.set_align('vcenter')
        base_style.set_align('vcenter')
        for i, fieldname in enumerate(fields):
            worksheet.write(0, i, fieldname,format_2)
            #worksheet.set_column(0,i,50)
        for row_index, row in enumerate(rows):
            #print row_index + 1
            height_setted = False
            for cell_index, cell_value in enumerate(row):
                cell_style = base_style

                if isinstance(cell_value, unicode ) and cell_value.startswith('http'):
                    try:
                        id_splitted     = cell_value.split("&id=")
                        id_             = id_splitted[1]
                        field_splitted  = id_splitted[0].split("&field=")
                        field_name      = field_splitted[1]

                        base64str = getattr (request.env[model].sudo().browse( int(id_)),field_name)
                        image_string = StringIO(base64.b64decode( base64str ))
                        image = Image.open(image_string)
                        image_data = BytesIO(base64.b64decode( base64str ))


                        width, height = image.size
                        worksheet.set_row(row_index + 1,height)
                        height_setted = True
                        worksheet.set_column(row_index + 1,0,30)

                        worksheet.insert_image(row_index +1,0,'image'+str(row_index+1),
                                   {'image_data': image_data,
                                    'positioning': 1,
                                    'x_offset': 15,
                                    'y_offset': 10} )
                    except:
                        worksheet.write(row_index + 1, cell_index, cell_value, cell_style)
                else:
                    if not height_setted:
                        worksheet.set_row(row_index + 1,64)
                    worksheet.set_column(row_index + 1,0,30)
                    if isinstance(cell_value, basestring):
                        cell_value = re.sub("\r", " ", cell_value)
                    elif isinstance(cell_value, datetime.datetime):
                        cell_style = datetime_style
                    elif isinstance(cell_value, datetime.date):
                        cell_style = date_style

                    worksheet.write(row_index + 1, cell_index, cell_value, cell_style)



        workbook.close()
        fp.seek(0)
        data = fp.read()
        fp.close()
        return data
    
    @http.route('/web/xlsserialnum/<d>/', type='http', auth="public",csrf=False)
    @serialize_exception
    def download_xlsserialnum(self,d=None,**kw):
        #print d
        
        seri_nolar = []
        obj = http.request.env['info_bayi.stock_cihaz_adet'].sudo().browse([int(d)])
        
        for p in obj.products:
            seri_nolar.extend ( p.lots )
        
        rows    = list(map(lambda x:[x.name, x.product_id.name, x.company_id.name, x.barkod or "", x.kamera or "", x.parmak or ""],seri_nolar))
        header  = [u'Seri No',u'Ürün',u'Bayi',u'Barkod',u'Kamera',u'Parmak']
        model   = 'stock.production.lot'
        
        columns_headers = header
        #rows = data.get('rows', [])
        #print rows
        excel = self.from_data_custom(columns_headers, rows, model)
        filename = str('StokSeriNolar')
        if filename.find('xls') > 0:
            filename = filename.replace('xls','xlsx')
        else:
            filename += '.xlsx'

        return request.make_response(
            excel,
            headers=[
                ('Content-Disposition', 'attachment; filename="%s"'
                 %filename),
                ('Content-Type', 'application/vnd.ms-excel')
            ],
        )
