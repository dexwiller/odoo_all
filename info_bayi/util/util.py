
ns = ['http://techpos.bkm.com.tr/',
      'techPosVendorBridgeSIMWS/',
      '{','}','isEmriGirisCevap',
      'http://schemas.xmlsoap.org/soap/envelope/',
      'http://okcbanka.infoteks.com.tr/',
      'seriNoDogrulamaCevap']

batarya_selection = [(2,'2300 MAh'),
                     (5,'5000 Mah')]
renk_selection    = [(1,'Siyah'),
                     (2,'Gri')]

def get_children_domain(object_, level=0):
        result = []

        def _get_rec(object_, level):
            for l in object_:
                l.level = level
                result.append(l)
                if l.child_line_ids:
                    if level<6:
                        level += 1
                    _get_rec(l.child_line_ids,level)
                    if level>0 and level<6:
                        level -= 1
            return result

        children = _get_rec(object_,level)

        ###print children
        return children

class XmlListConfig(list):
    def __init__(self, aList):
        for element in aList:
            if element:
                if len(element) == 1 or element[0].tag != element[1].tag:
                    self.append(XmlDictConfig(element))
                elif element[0].tag == element[1].tag:
                    self.append(XmlListConfig(element))
            elif element.text:
                text = element.text.strip()
                if text:
                    self.append(text)


class XmlDictConfig(dict):
        def tagFormatter(self, tag ):
            for n in ns:
                tag = tag.replace(n,'')
            return tag

        def __init__(self, parent_element):
                if parent_element.items():
                    self.update(dict(parent_element.items()))
                for element in parent_element:
                    if element:
                        if len(element) == 1 or element[0].tag != element[1].tag:
                            aDict = XmlDictConfig(element)
                        else:
                            aDict = {self.tagFormatter (element[0].tag): XmlListConfig(element)}
                        if element.items():
                            aDict.update(dict(element.items()))
                        self.update({self.tagFormatter (element.tag): aDict})
                    elif element.items():
                        self.update({self.tagFormatter (element.tag): dict(element.items())})
                    else:
                        self.update({self.tagFormatter (element.tag): element.text})
