# -*- coding: utf-8 -*-
from openerp import models, fields, api, exceptions, SUPERUSER_ID
from datetime import datetime,timedelta
class garanti_master_pos( models.Model):
    _name = 'info_tsm.garanti_master_pos'
    
    @api.model
    def get_default_tarih(self):
        now     = datetime.now()
        c_month = now.month
        if c_month == 12:
            c_month = 1
            c_year  = now.year + 1
        else:
            c_month = c_month + 1
            c_year  = now.year
            
            
        return datetime.strptime('%s-%s-02'%(c_year,c_month),'%Y-%m-%d')
    
    name        = fields.Char('Kullanıcı Adı 1')
    sifre       = fields.Char('Şifre 1')
    name2        = fields.Char('Kullanıcı Adı 2')
    sifre2       = fields.Char('Şifre 2')
    g_tarih     = fields.Date('Son Geçerlilik Tarihi',default=get_default_tarih)
    aktif       = fields.Boolean('Güncel Kayıt', default=True)
    
    @api.model
    def create(self,v):
        oncekiler = self.search([])
        if oncekiler:
            oncekiler.aktif = False
        return super(garanti_master_pos, self).create( v )
    
    @api.multi
    def send_mail(self):
        local_context = self.env.context.copy()
        company = self.env['res.users'].browse(SUPERUSER_ID).company_id.name
        local_context.update({
            'dbname': self.env.cr.dbname,
            'base_url': self.env['ir.config_parameter'].get_param('web.base.external_url', default='http://localhost:8069'),
            'datetime' : datetime,
            'company' : company,
            'only_user_sign' : True
        })
        
        temp_obj = self.env['mail.template']
        template_id = self.env['ir.model.data'].sudo().get_object('info_tsm', 'garanti_master_pos_template')
        template_obj = temp_obj.with_context(local_context).browse(template_id.id)
        body_html = temp_obj.with_context(local_context).render_template(template_obj.body_html, 'info_tsm.garanti_master_pos', self.id)
        
        #print body_html
        
        if template_id:
            values = template_obj.generate_email( res_ids = self.id)
            values['subject'] = 'Garanti Master Pos Uyarı!!!'
            values['email_to'] = 'yazarkasa@infoteks.com.tr'
            values['body_html'] = body_html
            values['body']      = body_html
            values['res_id'] = False
            values['email_from'] = 'info@infoteks.com.tr'
            mail_mail_obj = self.env['mail.mail'].sudo()
            msg_id = mail_mail_obj.create( values )

            if msg_id:
                #print  msg_id
                msg_id.sudo().send()
        return True
    