var express = require('express');
var app = express();

//Tell express which folder to use to server static files (can be more than one)
app.use(express.static('.'));

//Listen for requests
app.listen(7999);

