$(document).ready(function () {

    var windowWidth = $(window).width();

    if (windowWidth < 768){
        $(".menu").click(function () {
           $(".menu ul").slideToggle();
        });
    }

    $(".menu li").click(function () {

        $(".menu li").removeClass("active");
        $(this).addClass("active");
        var target = $(this).data("target");

        $([document.documentElement, document.body]).stop().animate({
            scrollTop: $(target).offset().top - 50
        }, 1000);

    });

    $(".product .owl-carousel").owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        dots: false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });

    $(".mobile-bar").hide();

    $(document).scroll(function () {
        var location = $(document).scrollTop();
        if (location > 1){
            $(".mobile-bar").fadeIn();
        }else{
            $(".mobile-bar").fadeOut();
        }
    });

    $(".clickAction").click(function () {
        var target = $(this).data("target");
        $([document.documentElement, document.body]).animate({
            scrollTop: $(target).offset().top
        }, 2000);
    });


});