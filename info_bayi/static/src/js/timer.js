odoo.define('info_restaurant_timer.demo_timer', function (require) {

    "use strict";
    var SystrayMenu         = require('web.SystrayMenu');
    var Widget              = require('web.Widget');
    var rpc                 = require('web.rpc');

    var DemoTimerMenu = Widget.extend({

        start: function () {
            var self = this;
            var countDownDate;
            var status;

            rpc.query({
                model: 'info_restaurant_options.config',
                method: 'search_read',
            }).then(function (result) {
                if(result.length > 0){
                    status = result[0].state;
                    if(status === 'demo' ){//demo kullanım ise
                        countDownDate = new Date(result[0].create_date);
                        countDownDate.setDate(countDownDate.getDate()+result[0].demo_duration);
                        var x = setInterval(function() {
                          var now = new Date().getTime();
                          var distance = countDownDate.getTime() - now;
                          var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                          var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                          var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                          var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                          document.getElementById("days").innerHTML = days;
                          document.getElementById("hours").innerHTML = hours;
                          document.getElementById("minutes").innerHTML = minutes;
                          document.getElementById("seconds_last").innerHTML = seconds;
                          if (distance < 0) {
                            clearInterval(x);
                            document.getElementById("days").innerHTML = 0;
                            document.getElementById("hours").innerHTML = 0;
                            document.getElementById("minutes").innerHTML = 0;
                            document.getElementById("seconds_last").innerHTML = 0;
                            document.getElementById("demo_status_text").innerHTML = "Demo Süreniz Dolmuştur!";
                            rpc.query({
                                model: 'info_restaurant_options.config',
                                method: "write",
                                args:[result[0].id,{'state':'expired'}]
                            }).then(function (result) {
                               self._logoutsessions();
                            });
                          }
                        }, 1000);
                    }
                    else if(status === 'product') {//normal kullanım ise
                        document.getElementById("demo_counter").style.display = "none";
                    }
                    else {//expride olmuş ise
                        self._logoutsessions();
                    }
                }
                else {//eğer kayıt yok ise
                   self._logoutsessions();
                }
            });

            return this._super();

        },

        _logoutsessions: function(seats){
            Swal.fire({
                  title: 'Oturumunuz Sonlanacak!',
                  allowOutsideClick: false,
                  text: "Demo süreniz dolmuştur.Lütfen yetkili ile görüşünüz.",
                  type: 'info',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Tamam'
            }).then((result) => {
                  if (result.value) {
                    window.location.href = "/web/session/logout?redirect=/web/login";
                  }
            })
        }
    });

    SystrayMenu.Items.push(DemoTimerMenu);

    return DemoTimerMenu;

});
