//-*- coding: utf-8 -*-
odoo.define('info_tsm.tsm_state', function (require) {
    "use strict";
    var Sidebar = require('web.Sidebar');
    var core = require('web.core');
    var model = require('web.Model');
    var ListView = require('web.ListView');
    var framework = require('web.framework');
    var _t = core._t;
    var QWeb = core.qweb;
    var model_prefix_    = 'info_tsm';
    var model_suffix_    = ['ynokc_aktivasyon_listesi_dosya_satirlari','kayit_listesi_dosya_satirlari','ynokc_public_key_update','ynokc_yetkili_servis','gib_files'];
    var model_suffix_ex_ = ['current_points'];
    var gib_param_model_name;
    var widgets = require('web.form_widgets');
    var GetDefaultsButton = widgets.WidgetButton.extend({
		on_click: function() {
            var self=this;
            framework.blockUI();
            var m       = new model('info_tsm.okc_urun_grup');
            m.call("get_defaults", [], {
			})
			.then(function(result){
                console.log(self);
				console.log(result);
                console.log( self.view.fields.satis.get_value());
                
                self.view.fields.satis.set_value( (5,0) );
                self.view.fields.kur.set_value( (5,0) );
                self.view.fields.altin.set_value( (5,0) );
                self.view.fields.tahsilat.set_value( (5,0) );
                self.view.fields.fatura_tahsilat_kurum.set_value( (5,0) );
                
                self.view.fields.satis.set_value( result.satis );
                self.view.fields.kur.set_value( result.kur );
                self.view.fields.altin.set_value( result.altin );
                self.view.fields.tahsilat.set_value( result.tahsilat );
                self.view.fields.fatura_tahsilat_kurum.set_value( result.fatura_tahsilat_kurum );
                
                
                //self.view.datarecord.satis = [25];
                //
                /*
                var final_satis_cache       = {};
                var satis_last_button_no    = 0;
                self.view.fields.satis.dataset.ids = [];
                for (var s1=0;s1<result.satis.length;s1++){
                    console.log( result.satis[s1] );
                    var new_satis_id = 'one_2_many_v_id_1000' + result.satis[s1].satis_tipi_islem_rel;
                    var values       = result.satis[s1];
                    values.id        = new_satis_id;
                    final_satis_cache[new_satis_id] = {'changes'  :result.satis[s1],
                                                       'from_read':{},
                                                       'id'       :new_satis_id,
                                                       'to_create':true,
                                                       'to_delete':false,
                                                       'values':values};
                    
                self.view.fields.satis.dataset.ids.push( new_satis_id );
                satis_last_button_no = result.satis[s1].button_no;
                }
                self.view.fields.satis.dataset.last_default_get = {'button_no':satis_last_button_no};
                self.view.fields.satis.dataset.cache            = final_satis_cache;
                self.view._dataset_changed = true;
                //this.dataset.x2m._dirty_flag = true;
                self.view.fields.satis._inhibit_on_change_flag = true;
                self.view.fields.satis.dataset.x2m._dirty_flag = true;
                self.view.fields.satis.view._dataset_changed   = true;
                
                self.view._actualize_mode("view");
                self.view._actualize_mode("edit");
                */
                console.log('callback : ', self.view.fields);
                setTimeout(framework.unblockUI(),1000);
                
			});
	    }
    });
	
	core.form_tag_registry.add('get_okc_param_defaults', GetDefaultsButton);

    function stringStartsWith_tsm (string, prefix) {
        if (!string) {
            return false;
        }
    
        return string.slice(0, prefix.length) == prefix;
    }
    function stringEndsWith_tsm( string, suffixList){
        if (!string) {
            return false;
        }
        var return_val = false;
        for ( var l=0;l< suffixList.length;l++) {
            var state = string.slice(string.length - suffixList[l].length, string.length) == suffixList[l];
            if (state) {
                return_val =  true;
                break;
            }
        }
    
        return return_val;
    }
    function inArray(needle,haystack){
        var count=haystack.length;
        for(var i=0;i<count;i++)
        {
            if(haystack[i]===needle){return true;}
        }
        return false;
    }
    Sidebar.include({
        redraw: function () {
            
            var self = this;
            this._super.apply(this, arguments);
            if (self.getParent().ViewManager.active_view.type == 'list') {
                
                self.$el.find('.export_treeview_xls').on('click', self.on_sidebar_export_treeview_xls);
            }
            if (self.getParent() && self.getParent().ViewManager.active_view.type == 'list') {
                var parentNode = self.getParent();
                if ( stringStartsWith_tsm ( parentNode.model, model_prefix_ ) && stringEndsWith_tsm( parentNode.model, model_suffix_)) {

                        if (parentNode.model == 'info_tsm.gib_files') {
                            self.$el.find('.o_dropdown').last().append(QWeb.render('AddHeaderTSMFileButtonsGibFiles', {widget: self}));
                            self.$el.find('.o_dropdown').last().append(QWeb.render('AddHeaderTSMFileButtonsGetList', {widget: self}));
                            self.$el.find('.gibe_gonder').on('click', self.create_file_and_send_ftp);
                            self.$el.find('.gibden_al').on('click', self.get_file_list);
                            gib_param_model_name = parentNode.model;

                            $('.o_cp_sidebar').css({'witdh':'500px'});
                            self.$el.find("div").css({'witdh':'100px'});
                        }else {
                            self.$el.find('.o_dropdown').last().append(QWeb.render('AddHeaderTSMFileButtons', {widget: self}));
                            self.$el.css({'witdh':'500px'});
                            self.$el.find("div").css({'witdh':'100px'});
                            //$("body").on('click','#oe_sidebar_create_file_and_end',self.create_file_and_send_ftp);
                            self.$el.find('.gibe_gonder').on('click', self.create_file_and_send_ftp);
                            
                            gib_param_model_name = parentNode.model;
                        }
                }
               //self.$el.append(QWeb.render('AddExportViewMainExcel', {widget: self}));
               //$("body").on('click','.oe_sidebar_export_view_xls', self.on_sidebar_export_view_xls);
            }
        },
        create_file_and_send_ftp: function () {

            var self = this;
            var conf = window.confirm("Gib'e Gönderimeyen Kayıtlar Gönderilecektir. Bu İşlemin Dönüşü Yoktur. Emin misiniz?");
            if ( conf ) {

                new model("info_tsm.gib_files")
                    .call("create_file_and_send_ftp", [1,gib_param_model_name])
                    .then(function (result) {
                        alert( result[1]);
                    });
            }
        },
        get_file_list: function () {

            var self = this;
            var conf = window.confirm("Gib'den Çekilmeyen Dosyalar Çekilerek, Listeler Güncellenecektir.");
            if ( conf ) {
                new model("info_tsm.gib_files")
                    .call("get_files_list", ["model_name",gib_param_model_name])
                    .then(function (result) {
                        alert( result[1]);
                    });
            }
        },
    });
    
    ListView.include({
        do_delete: function (ids) {
            console.log('Burda');
            var self = this;
            var res  = this._super.apply(this, arguments);
            var to_update_array = [];
            var sff = ['ozel_kdv_group_rel','tahsilat_tipi_group_rel','satis_tipi_group_rel'];
            if (self.dataset ) {
                if ( stringStartsWith_tsm ( self.dataset.model, model_prefix_ ) && stringEndsWith_tsm( self.dataset.model, sff)) {
                    var button_no   = 1;
                    self.records.each(function(record){
                        if ( inArray (record.get('id'), self.dataset.ids)){
                            record.set('button_no', button_no);
                            self.dataset.cache[record.get('id')].values.button_no=button_no;
                            if (self.dataset.cache[record.get('id')].changes ){
                                self.dataset.cache[record.get('id')].changes.button_no=button_no;
                            }
                            else{
                                self.dataset.cache[record.get('id')].changes = {'button_no':button_no};
                            }
                            button_no ++;
                        }
                    });
                }
            }
            self.compute_aggregates();
            return res;
        },
        
    });
    
    ListView.include({

        load_list:function(){
            
            //console.log('Load List');
            
            var self = this;
            var val  = this._super.apply(this, arguments);
            var btnCol = $('#custom_tsm_params_form table thead tr th:nth-child(5)').hasClass("btnCol");
            var table = $('#custom_tsm_params_form table').hasClass("o_list_view");
            //console.log(table);
            //console.log(btnCol);
            //console.log (self.model);
            
            /*if (self.model == 'info_tsm.fonksiyon_tuslari1' || self.model == 'info_tsm.fonksiyon_tuslari2'){
                console.log('Fonsiyon : Dataset',self.dataset);
                
                var m = new model( 'info_tsm.okc_urun_grup' );
                m.call("create_file_and_send_ftp", [])
                    .then(function (result) {
                        alert( result[1]);
                    });
                
                self.reload();
            }*/
            if ( self.model == 'stock.production.lot' || self.model == 'info_extensions.sorun_tipi_rel'){
                try{
                    if(!btnCol && table){
                        var headType = {
                            "primary" : "Cihaz <br/>Versiyonları",
                            "yellow" : "İnfoteks <br/>Ayarları",
                            "green" : "Bayi<br/>Ayarları",
                            "red" : "Cihaz<br/>Ayarları",
                            "info" : "Parametre<br/>Tablosu",
                            "orange" :  "Banka<br/>Durumu",
                            "pink" : "Yemek<br/>Kartları",
                            "purple" : "3.Parti<br/>Uygulamalar",
                            "teal" : "İnternet <br/> Geçmişi",
                            "indigo" : "Modem<br/> Geçmişi",
                            "cyan" : "Cihaz<br/>Geçmiş",
                            "sairs" : "Sertifika<br/>Bitiş",
                            "hotpink":"Servis<br/>Özellikleri",
                            "hotred":"İade<br/>Al",
                            "niceblue":"Şube<br/>Transfer",
                            "lightred":"Değişen <br/> Parça Sil",
                            "lightgreen":"Değişen <br/>Parça Ekle",
                            "lightblue":"İnceleme <br/>Sonucunu Değiştir",
                            "lightpurple":"İşlemi <br/>Uygula",
                            "videoizle":"İşlem <br/>Videosu",
                            "docizle":"Doküman",
                        };
        
                        var removeList = [];
                        
                        var columnCount = 1;
                        //console.log( $('#custom_tsm_params_form table tbody tr:nth-child(1) td'));
                        $('#custom_tsm_params_form table tbody tr:nth-child(1) td').each(function () {
                            if($(this).html().length == 0 && columnCount == 2){
                                $('#custom_tsm_params_form table thead tr th:nth-child(' + columnCount + ')').html( headType.hotred );
                                $('#custom_tsm_params_form table thead tr th:nth-child(' + columnCount + ')').addClass("bg-color-hotred");
                            }
                            if($(this).html().length == 0 && columnCount == 3){
                                $('#custom_tsm_params_form table thead tr th:nth-child(' + columnCount + ')').html( headType.niceblue );
                                $('#custom_tsm_params_form table thead tr th:nth-child(' + columnCount + ')').addClass("bg-color-niceblue");
                            }
                            if($(this).find("button i").length > 0){
                                var attrClass = $(this).find("button i").attr("class").split("text-color-")[1];
                                
                                
                                $('#custom_tsm_params_form table thead tr th:nth-child(' + columnCount + ')').html( headType[attrClass] );
                                $('#custom_tsm_params_form table thead tr th:nth-child(' + columnCount + ')').addClass("bg-color-" + attrClass);
                                
                                
                                if (attrClass.localeCompare('lightpurple') == 0){
                                   
                                    $('#custom_tsm_params_form table thead tr th:nth-child(' + columnCount + ')').attr({"colspan" : "2"});
                                    $('#custom_tsm_params_form table thead tr th:nth-child(' + (columnCount + 1)  + ')').remove();
                                    console.log (columnCount);
                                    columnCount--;
                                }
                                if (attrClass.localeCompare('lightpurple2') == 0){
                                    $('#custom_tsm_params_form table thead tr th:nth-child(' + (columnCount -1) + ')').html( headType.lightpurple);
                                    $('#custom_tsm_params_form table thead tr th:nth-child(' + (columnCount -1) + ')').addClass("bg-color-lightpurple");
                                    $('#custom_tsm_params_form table thead tr th:nth-child(' + (columnCount -1) + ')').attr({"colspan" : "2"});
                                    $('#custom_tsm_params_form table thead tr th:nth-child(' +  columnCount  + ')').remove();
                                    columnCount--;
                                }
                                
                                
                            }
                            
                            columnCount++;
                        });
                        var columnCount2 = 1;
                        $('#custom_tsm_params_form2 table tbody tr:nth-child(1) td').each(function () {
                            if($(this).find("button i").length > 0){
                                var attrClass = $(this).find("button i").attr("class").split("text-color-")[1];
                                $('#custom_tsm_params_form2 table thead tr th:nth-child(' + columnCount2 + ')').html( headType[attrClass] );
                                $('#custom_tsm_params_form2 table thead tr th:nth-child(' + columnCount2 + ')').addClass("bg-color-" + attrClass);
                            }
                            columnCount2++;
                        });
                        
                        if (self.model == 'stock.production.lot'){
                            /*
                            try {
                                    if( $('#custom_tsm_params_form table thead tr th:nth-child(' + (columnCount - 1) + ')').html().trim() == headType[attrClass].trim() ){
                                        removeList.push(columnCount - 1);
                                    }
                                }
                            catch(e){
                                console.log(e);
                            }
                            
                            for(var i = removeList.length - 1; i >= 0; i--){
                                $('#custom_tsm_params_form table thead tr th:nth-child(' + (removeList[i] + 1) + ')').attr({"colspan" : "2"});
                                $('#custom_tsm_params_form table thead tr th:nth-child(' + removeList[i] + ')').remove();
                            }*/
                            $('#custom_tsm_params_form table thead tr th:nth-child(5)').addClass("btnCol");
                            var headerTop    = $(".o_main_content").offset().top + $(".o_control_panel").outerHeight(true); //$(".o_view_manager_content").offset().top;
                            var topOfsetSoft = $(".table-responsive").offset();
                            if (topOfsetSoft){
                                topOfsetSoft = topOfsetSoft.top;    
                            }
                            else{
                                topOfsetSoft = headerTop + 1;
                            }
                            
                            var $orginHeader  = $($(".o_content table thead")[2]);
                            console.log($orginHeader);
                            var $cloneHeader = $($(".o_content table thead")[2]).clone();
                            console.log($cloneHeader);
                            
                            $orginHeader.after($cloneHeader);
                            $orginHeader.css({"position" : 'static', "opacity" : 1});
                            $cloneHeader.css({"display" : 'none', "opacity" : 0.0});
            
                            if(!$orginHeader.hasClass($orginHeader)){
                                $orginHeader.addClass("orginHeader");
                                $cloneHeader.addClass("cloneHeader");
                                responseTableHead();
                                /*
                                if(headerTop > topOfsetSoft){
                                    $cloneHeader.css({
                                         "opacity" : 1,
                                         "display" : "table-header-group"
                                    });
                                    $orginHeader.css({
                                        "position" : "fixed",
                                        "top" : headerTop,
                                         "z-index'" : 2,
                                         "opacity" : 1,
                                         "display" : "table-header-group"
                                    });
                                }
                                else{
                                    $cloneHeader.attr("style", "");
                                    $orginHeader.attr("style", "");
                                    $cloneHeader.css({"display" : 'none', "opacity" : 0.0});
                                }
                                */
                                $(".o_content").scroll(function() {
                                    
                                        var topOfset = $($(".table-responsive")[2]).offset();
                                        
                                        if (topOfset){
                                            topOfset = topOfset.top;
                                            if(headerTop > topOfset){
                                            $cloneHeader.css({
                                                 "opacity" : 1,
                                                 "display" : "table-header-group"
                                            });
                                            $orginHeader.css({
                                                "position" : "fixed",
                                                "top" : headerTop,
                                                 "z-index'" : 2,
                                                 "opacity" : 1,
                                                 "display" : "table-header-group"
                                            });
                                            }
                                            else{
                                                $cloneHeader.attr("style", "");
                                                $orginHeader.attr("style", "");
                                                $cloneHeader.css({"display" : 'none', "opacity" : 0.0});
                                            }
                                        } 
                                });
                            }
                        }
                    }
                }
                catch(e){
                    console.log('catched', e);
                }
            }
            return val;
        }
    });
});

$( window ).resize(function() {
    responseTableHead();
});


function responseTableHead(){
    
    
    
    var th1 =  $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(1)').width();
    var th2 =  $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(2)').width();
    var th3 =  $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(3)').width();
    var th4 =  $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(4)').width();
    var th5 =  $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(5)').width();
    var th6 =  $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(6)').width();
    var th7 =  $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(7)').width();
    var th8 =  $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(8)').width();
    var th9 =  $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(9)').width();
    var th10 = $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(10)').width();
    var th11 = $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(11)').width();
    var th12 = $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(12)').width();
    var th13 = $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(13)').width();
    var th14 = $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(14)').width();
    var th15 = $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(15)').width();
    var th16 = $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(16)').width();

    $(".orginHeader tr th:nth-child(1)").width(th1);
    $(".orginHeader tr th:nth-child(2)").width(th2);
    $(".orginHeader tr th:nth-child(3)").width(th3);
    $(".orginHeader tr th:nth-child(4)").width(th4);
    $(".orginHeader tr th:nth-child(5)").width(th5);
    $(".orginHeader tr th:nth-child(6)").width(th6);
    $(".orginHeader tr th:nth-child(7)").width(th7);
    $(".orginHeader tr th:nth-child(8)").width(th8);
    $(".orginHeader tr th:nth-child(9)").width(th9);
    $(".orginHeader tr th:nth-child(10)").width(th10);
    $(".orginHeader tr th:nth-child(11)").width(th11);
    $(".orginHeader tr th:nth-child(12)").width(th12);
    $(".orginHeader tr th:nth-child(13)").width(th13);
    $(".orginHeader tr th:nth-child(14)").width(th14);
    $(".orginHeader tr th:nth-child(15)").width(th15);
    $(".orginHeader tr th:nth-child(16)").width(th16);
}
