//-*- coding: utf-8 -*-
odoo.define('info_bayi.one2manylistOptions', function (require) {
    "use strict";
    var core        = require('web.core');
    var Sidebar     = require('web.Sidebar');
    var Model       = require('web.Model');
    var ListView    = require('web.ListView');
    var FormView    = require('web.FormView');
    var QWeb        = core.qweb;
	var list_widget_registry = core.list_widget_registry;
    var p  = 0;
    /*Sidebar.include({
        redraw: function () {
            var self = this;
            this._super.apply(this, arguments);
            //console.log('Sidebar Include');
            if (self.getParent() && self.getParent().ViewManager.active_view.type == 'list') {
                var parentNode = self.getParent();

                if (parentNode.model == 'res.partner') {
                    self.$el.parent().prepend(QWeb.render('AddHeaderCuzdanButton', {widget: self}));
                    self.$el.parent().find('.cuzdan_select').on('click', {view:self.getParent()},self.get_cuzdan_select_action);
                }
            }
        },
        get_cuzdan_select_action: function (event) {
            var a = {
                    type: 'ir.actions.act_window',
                    name:'Cüzdan Seç',
                    res_model: 'info_kart.cuzdan',
                    view_mode:"tree",
                    view_type:"form",
                    views: [[false, 'tree']],
                    target:"new",
                    };
            event.data.view.do_action( a );

        },
    });*/
    ListView.include({

        load_list:function(){

            ////console.log('Load List');

            var self = this;
            var val  = this._super.apply(this, arguments);
            var btnCol = $('#custom_tsm_params_form table thead tr th:nth-child(1)').hasClass("btnCol");
            var table = $('#custom_tsm_params_form table').hasClass("o_list_view");
            ////console.log(table);
            ////console.log(btnCol);
            ////console.log (self.model);

            /*if (self.model == 'info_tsm.fonksiyon_tuslari1' || self.model == 'info_tsm.fonksiyon_tuslari2'){
                //console.log('Fonsiyon : Dataset',self.dataset);

                var m = new model( 'info_tsm.okc_urun_grup' );
                m.call("create_file_and_send_ftp", [])
                    .then(function (result) {
                        alert( result[1]);
                    });

                self.reload();
            }*/
            if ( self.model == 'info_bayi.turkpara_banka_rel'){
                try{
                    if(!btnCol && table){
                        var headType = {
                            "primary" : "Gelen <br/>Aramalar",
                            "yellow" : "Giden <br/>Aramalar",
                            "green" : "Diğer Yemek<br/>Kartları",
                            "red" : "En Çok Harcama Yapan<br/>Kart Müşterileri",
                            "info" : "Mesajlaşma<br/>Geçmişi",
                            "orange" :  "Pos<br/>Hareketleri",
                            "pink" : "Yemek<br/>Kartları",
                            "purple" : "3.Parti<br/>Uygulamalar",
                            "teal" : "İnternet <br/> Geçmişi",
                            "indigo" : "Modem<br/> Geçmişi",
                            "cyan" : "Cihaz<br/>Geçmiş",
                            "sairs" : "Cüzdanlar",
                            "hotpink":"Servis<br/>Özellikleri",
                            "hotred":"İade<br/>Al",
                            "lightred":"Değişen <br/> Parça Sil",
                            "lightgreen":"Değişen <br/>Parça Ekle",
                            "lightblue":"İnceleme <br/>Sonucunu Değiştir",
                            "lightpurple":"İşlemi <br/>Uygula",
                            "videoizle":"İşlem <br/>Videosu",
                            "docizle":"Doküman",
                        };

                        var removeList = [];

                        var columnCount = 1;
                        ////console.log( $('#custom_tsm_params_form table tbody tr:nth-child(1) td'));
                        $('#custom_tsm_params_form table tbody tr:nth-child(1) td').each(function () {
                            if($(this).html().length == 0 && columnCount == 2){
                                $('#custom_tsm_params_form table thead tr th:nth-child(' + columnCount + ')').html( headType.hotred );
                                $('#custom_tsm_params_form table thead tr th:nth-child(' + columnCount + ')').addClass("bg-color-hotred");
                            }

                            if($(this).find("button i").length > 0){
                                var attrClass = $(this).find("button i").attr("class").split("text-color-")[1];


                                $('#custom_tsm_params_form table thead tr th:nth-child(' + columnCount + ')').html( headType[attrClass] );
                                $('#custom_tsm_params_form table thead tr th:nth-child(' + columnCount + ')').addClass("bg-color-" + attrClass);


                                if (attrClass.localeCompare('lightpurple') == 0){

                                    $('#custom_tsm_params_form table thead tr th:nth-child(' + columnCount + ')').attr({"colspan" : "2"});
                                    $('#custom_tsm_params_form table thead tr th:nth-child(' + (columnCount + 1)  + ')').remove();
                                    //console.log (columnCount);
                                    columnCount--;
                                }
                                if (attrClass.localeCompare('lightpurple2') == 0){
                                    $('#custom_tsm_params_form table thead tr th:nth-child(' + (columnCount -1) + ')').html( headType.lightpurple);
                                    $('#custom_tsm_params_form table thead tr th:nth-child(' + (columnCount -1) + ')').addClass("bg-color-lightpurple");
                                    $('#custom_tsm_params_form table thead tr th:nth-child(' + (columnCount -1) + ')').attr({"colspan" : "2"});
                                    $('#custom_tsm_params_form table thead tr th:nth-child(' +  columnCount  + ')').remove();
                                    columnCount--;
                                }


                            }

                            columnCount++;
                        });
                        var columnCount2 = 1;
                        $('#custom_tsm_params_form2 table tbody tr:nth-child(1) td').each(function () {
                            if($(this).find("button i").length > 0){
                                var attrClass = $(this).find("button i").attr("class").split("text-color-")[1];
                                $('#custom_tsm_params_form2 table thead tr th:nth-child(' + columnCount2 + ')').html( headType[attrClass] );
                                $('#custom_tsm_params_form2 table thead tr th:nth-child(' + columnCount2 + ')').addClass("bg-color-" + attrClass);
                            }
                            columnCount2++;
                        });

                    }
                }
                catch(e){
                    //console.log('catched', e);
                }
            }
            return val;
        }
    });
    FormView.include({
        load_record:function( object ){
            var self = this;
            var r = this._super.apply(this, arguments);
            if (self.model == 'res.partner') {
                $('.oe_view_manager_buttons').html("");
                //$(".oe_form_sheetbg").css( "border", "1px solid #7e57c2");
                self.$el.prepend(QWeb.render('accordion_view', {widget: self}));
            }
        },
    });
    ListView.List.include({
        row_clicked: function (e, view) {
            if( this.view.is_action_enabled('open') )
                this._super.apply(this, arguments);
        },

        render: function() {
			this._super(this, arguments);
            var self_render = this;
            var setPrices = function() {

                var siparis_no = $("#name_temp span").html();
                var m = new Model('info_bayi.tutar_by_taksit_oranlari');
                $("input[name=sec]").each(function(){

                    if ($(this).is(':checked')){
                        p = $(this).prop('id');
                    }
                });
                //console.log( self_render );
                if (p === 0){
                    self_render.view.do_notify('Lütfen Önce Ödeme Seçeneği Belirleyiniz.');
                }
                else {
                    m.call("setSelected",  [parseInt (p)] ,{'siparis_no':siparis_no,'komisyon_dahil':
                       $('#visualbox').is(':checked')}).then(function(result){
                            var finalTutar = 0;
                            if($('#visualbox').is(':checked')){
                                $("#tutar_cekilecek").html(result.komisyon_dahil + " TL");
                                //$("#komisyon_dahilmi").html("Girilen tutara dahil");
                                finalTutar  = result.komisyon_dahil;
                            }
                            else{
                                $("#tutar_cekilecek").html(result.komisyon_haric + " TL");
                                //$("#komisyon_dahilmi").html("Girilen tutara eklendi");
                                finalTutar  = result.komisyon_haric;
                            }
                            if(result.odeme_yontemi == "_")
                                $("#cekim_yontem").html("Tek Çekim");
                            else
                                $("#cekim_yontem").html(result.odeme_yontemi);

                            $("#musteri_adi").html($("#partner_id input").val());
                            $("#siparis_no").html($("#name_temp span").html());
                            $("#cariden_dusulecek").html(result.cari_dusulecek + " TL");
                            $("#komisyon_tutari").html(result.komisyon + " TL");


                            //$("input[name=sec]").each(function(){
                            //    if ($(this).is(':checked')){
                            //        p = $(this).prop('id');
                            //    }
                            //});

                            //console.log( result);

                            self_render.view.__parentedParent.__parentedParent.__parentedParent.fields.cariden_dusulecek.set_value( result.cari_dusulecek );
                            self_render.view.__parentedParent.__parentedParent.__parentedParent.fields.komisyon.set_value( result.komisyon );
                            self_render.view.__parentedParent.__parentedParent.__parentedParent.fields.komisyonfarki.set_value( result.kom_fark );
                            self_render.view.__parentedParent.__parentedParent.__parentedParent.fields.tutar_cekilecek.set_value( finalTutar );
                        });
                }
			};

            var checked = false;

            if ($("#visualbox").is(':checked')){
                checked = true;
            }

            $("#komisyon_dahil_newc").html('<div class="checkbox"><label style="border:0;!important"><input type="checkbox" name="visualbox" id="visualbox" value=""><span class="cr"><i class="cr-icon fa fa-check"></i></span></label></div>');
            $("#visualbox").prop('checked', checked);

            $("#visualbox").change(function(){
                if ($("#visualbox").is(':checked')){
                    self_render.view.__parentedParent.__parentedParent.__parentedParent.fields.komisyon_dahil.set_value( true );
                }
                else{
                    self_render.view.__parentedParent.__parentedParent.__parentedParent.fields.komisyon_dahil.set_value( false );
                }
            });

			this.$current.delegate('input[name=sec]','click', setPrices);
            $('#visualbox').click( setPrices );


		}
    });

    ListView.Column.include({
		init: function(id, tag, attrs) {
			this._super(id, tag, attrs);

			if (this.widget == 'radio_button_selected') {
				this.radio_list = true;
			}
		},
		_format: function (row_data, options) {
			if ((this.widget == 'radio_button_selected' || this.radio_list) && !!row_data[this.id]) {
				var value    = row_data[this.id].value;
				var name     = value;
                var selected = '';
                if (parseInt (row_data.id.value) === parseInt( p )){
                    selected = 'checked';
                }
				var values   = {
					id: row_data.id.value,
					taksit_oranlari: row_data.taksit_oranlari.value[1],
					tutar_cekilecek: row_data.tutar_cekilecek.value,
                    checked        : selected
				};

				return _.str.sprintf(
					'<input type="radio" name="sec" id="%(id)s"  tutar_cekilecek="%(tutar_cekilecek)s"  taksit_oranlari="%(taksit_oranlari)s" %(checked)s />',values
				);
			}
			else {
			    return this._super(row_data, options);
			}
        },
	});

	list_widget_registry.add('field.radio_button_selected', ListView.Column);

    ListView.include({
        load_list:function(){

            var self = this;
            var val  = this._super.apply(this, arguments);
            var name = $('#name_temp span:nth-child(1)').html();
            var musteri = $('#partner_id span').html();
            var taksit_secimi = $('#taksit_oranlari_').html();
            var tutar_cekilecek = $('#hidden_tutar_cekilecek span').html();
            var kart_no = $('#cart_number span:nth-child(4)').html();
            var banka = $('#card_bank span').html();
            var kart_tipi = $('#card_type_ span').html();
            var kart_style = $('#card_style span').html();
            var kart_no = $('#cart_number span:nth-child(4)').html();
            var odeme_tarihi = $('#write_date span').html();
            var bank_state = $('#bank_state span').html();
            var hata_kodu = $('#hata_kodu span').html();
            var hata_msg = $('#hata_msg span').html();
            var settle_code = $('#settle_code span').html();
            var auth_code = $('#auth_code span').html();
            var host_code = $('#host_code span').html();
            var addViews = '';
            addViews += '<div class="o_edit_form_view"><strong>Ödeme Kodu</strong><b>:</b><span>' + name + '</span></div>';
            addViews += '<div class="o_edit_form_view"><strong>Müşteri</strong><b>:</b><span>' + musteri + '</span></div>';
            addViews += '<div class="o_edit_form_view"><strong>Çekilen Tutar</strong><b>:</b><span>' + tutar_cekilecek +  ' TL</span></div>';
            addViews += '<div class="o_edit_form_view"><strong>Ödeme Alınan Banka</strong><b>:</b><span>' + banka + '</span></div>';
            addViews += '<div class="o_edit_form_view"><strong>Kart Tipi</strong><b>:</b><span>' + kart_tipi + ' / ' + kart_style + '</span></div>';
            addViews += '<div class="o_edit_form_view"><strong>Kart Numarasının Son 4 Hanesi</strong><b>:</b><span>**** **** **** ' + kart_no + '</span></div>';
            addViews += '<div class="o_edit_form_view"><strong>Ödeme Tarihi</strong><b>:</b><span>' + odeme_tarihi + '</span></div>';
            addViews += '<div class="o_edit_form_view"><strong>Ödeme Durumu</strong><b>:</b><span>' + bank_state + '</span></div>';

            if(hata_kodu){
                addViews += '<div class="o_edit_form_view"><strong>Hata Kodu</strong><b>:</b><span>' + hata_kodu + '</span></div>';
                addViews += '<div class="o_edit_form_view"><strong>Hata Mesajı</strong><b>:</b><span>' + hata_msg + '</span></div>';
            }
            else{
                addViews += '<div class="o_edit_form_view"><strong>Banka Gün Sonu No</strong><b>:</b><span>' + settle_code + '</span></div>';
                addViews += '<div class="o_edit_form_view"><strong>Banka Onay Kodu</strong><b>:</b><span>' + auth_code + '</span></div>';
                addViews += '<div class="o_edit_form_view"><strong>Banka Referans Kodu</strong><b>:</b><span>' + host_code + '</span></div>';
            }

            $(".editFormView").html(addViews);


            /*
            var tekCekim = '<tr data-id="9629" class="" style="">';
            tekCekim += '<td data-field="resim"></td>';
            tekCekim += '<td data-field="secim"><input type="radio" name="sec" id="0" tutar_cekilecek="50" taksit_oranlari="Tüm Kartlar Tek Çekim  %0.0"></td>';
            tekCekim += '<td data-field="taksit_oranlari">Tüm Kartlar Tek Çekim  %0.0</td>';
            tekCekim += '<td data-field="tutar" class=" o_list_number   ">50,00</td>';
            tekCekim += '<td data-field="tutar_cekilecek" class=" o_list_number">50,50</td>';
            tekCekim += '</tr>'

            $('#kartListesi table tbody tr:nth-child(1)').before(tekCekim);
            */


            $('#kartListesi table tbody').html();


            var default_img = "iVBORw0KGgoAAAANSUhEUgAAANEAAAB4CAIAAADjS4CeAAAACXBIWXMAAA9hAAAPYQGoP6dpAAAgAElEQVR4nNTc13db95kv/PwtZ73rzGTOmYktAbthV2ADYJNkO5MySRxblkQSve6N3kiw9yZ2AiRB9N5BkGAVRYrqvVi2iuXeJDt538v3gpKsWLbiZJxZzlrfCyxcf9bz/Mrz2z8zzJ2GrMuQdRmyLMPGpynvBTIsfjvGImQsgobC06CmvMBVAdgcyOShbycLsZm/TBpmUntBmBSsT5KWAmbKgroEqEuA+theAF30WwH1Ma42wtVGAM3ThAFNiKsOAqoApAr+tQQAxfy3o5x9NlyFl6PwAAovbYjCSi8on3kaQDYNPhdAOglIJ74VULKXcVA6ymfmAMlxbuPI9wVsGIEaRsD6YbB+8DsDHBsAjvUDx/qAY33gsT7oScCjvU/SAx7tAY507/14NsCRbu5bXc8GONzOlw/CRzq4h1u/nTdbuW+0cN90cw83k8oRATNLMvMUO0exc4TeS+pmSN0MbZirtgYI7TShnSZ0k3uh1BPPhlSNk+qxvRCq0b2QqhFSOUgqB3mSDvy/9D/7bc/af9McYCggpoLAuQQx2b/JHKxPUtYC/hTcT8Gc0gsovJDMSxsjkMLz95kDGseBxnEBOw9KR0Hp6E/KHPBmK18+CB3pAN5qe9Yc8GYL942WfW91oOpxipklnwmh91B6D22Yq7L4H4N7xhxfM/mNtr18lzlKNUSphkhVP/R7688OtK5BlgpkqUDmyt9rrgga8ogxRzuWYCYL/hVzGUSfhvVJWJ8grQXcnIeZ1E/HHFfhBeReUOaF5F6BMQwr/2Zz3MZRUDIuYBdAyTgoGQOlYz8dc9zD7dzDrcDhVr5yEDrSxj3cwjncwnmrBTjsBt9oBt5sw1QTODNPMPMkM0cxs5TeS+m9FOMVsLM1tiCl95Da6b1Q2klSO0lpJ0n1t6m9wBylGuS+7vrZwbZ1yLICWVb+bnOP5bF51JCjHcsQk3uBOZjJIPoMrE+Q1hxuzu0VvJ+OOUDuBb/JjMAUhBR/i7knLRWUjD5pshM/NXPcw63cwy0C5QB8tI3zlpvzlhs43AIebqMUo5R+jmB8JDNL6L2E3kvpPU9bKqmbofQevmb6SSb38nx5+5Y5XHmcUD42x1cNct9s+jHNgWyeZ8jTjiVYn32BOViXpqx53PxNk/1JmpsFFR5APiEwhmCl94ebe9pSf8rmgMMtwGG3QDkAH2kFDrcAb3UTqilKP0fq50n93NOu+rSl7uEjtNM/3NzTIve0zpHKQULex33jR61zAFuAmBxqyAkdy7A+CzJ5kMk9u5KDmDSkT5GWIm7OwUzip23OC8o9kGIGVMzwDSFQPvNXzEnGQekkbQgAkjFQOgZKx3/K5riHW4DDbuCwW6jsh95qo1RTlN5HML4n5ryU3st/pqU+Y25qL99QU03shVCNE6pxQj1GqkYJ1fEn2vYyTCoHCPkAIfuxzT1dyWGGHO1YgpgcyD6ucyCbhtgUrE9Sj8Glns1P1ZwXlHsB2QwonxEYw9Bz7J41B0omhIbgU2fP5qdpjnvYzT3s5h5pPWCdEzCzz5Q3L8l4+HqPiJ2ttgb2wBHaaVI3g2umvrVLpdQTpGpyL5RyjFKOUspRQjWKq0Yx5XFSOUo9zhCh6Cfkw4Rs4B9lDmRyPGOBdizDTPqpOUCfoKx5wvRtcD9xc6Dcs7ee4xtC32L31BwonaTZACSd/GcxBxx2cw+37DvajWmnKMZb54jSBt+z5kTG+VpLYK+2PZvnzRHKCUI5gSvGCdU4oRynNJMEMycw+TDlcVI5smeOVAwTimFCfpyQDwE/8nruG3MFkMmjhqzQsQQzaZBNA0yKsBZwUx55Dtw/izlANi0whCD5DCSbBmVToGxqzxwomxSwAfB7wP3PmOMe6QGO9gLHeoEn/3zLHHB4z1zHHjvOYTfnaDumncFYH8HOU8zsAWdUoJ8hGB/BzAmMvmprkNJ6CM3jXSqpm6Q0k6R2mtJMkpppUj1BaqYFmikhM8vXz1VZoyKjv66pIHYkDrYWRY5YnTtFqEZw5ShfOSLQTPLV4yJ2nmZ8h+whXNr9DzEHfdNk87SjDDJJ3Fp4vqX+c5njyKZB+QxtCMGyaVA2CcomQekEKJnYq3DPnQn/T5iDjg1gKg/Y0Au81Qse7QG+p84Bb7UDb7UBb7UBhzu4b7ZzD7v3H2vDdOOkfo7Q+yj9LMH4KEf6leZUXVul1pn4/dA2ZfAT2hlS6yW1XkLrJRgvpZsRmMIkMy9yZQTmYE17RWhPH+xar7LHD7WXBcbwAXuK0k/UWOKkdlroSIgN3ip3Tmz217YviSzBgy0Z0rBQaw8AR9r+seYgJs8z5qtbK7g5833g/lnMcWXTXNkUJJ2m2SAkn9ozJzQEIekkIHn+HuJ/whxPMiKwJkDpMHikFzjaCx7r4x75xtzTIsc50r7/rXbO0Y79b7VzjnRxjnQTmilSP0vq5yn9vEA3K7QmDgycrnEvvjF+7rXWtLh1CWcCQmMQZ4K1TTm+JXGge13kSh3oXhHZ4wc6K3xzuMaZorSeKnuc0HrFtoSAXTjYWuTbg692nhBZowc6ykLDfFVzmq89TtvDuGoYVQ7zFEOYvJ/7hvMfai4HMnnCWuZb80JHAWGSMJOCmPQ/qzn5NCCbhqTTkGxGYAjylNNCQwB6AguQTkDS8W+ZAyQTgGSK2zjKkYxwG4+DDSPcxuPcxuNA4+BTcE/MDQENA2D9AFg/CNU/+XFsCDw6AB7rB4/1gMd6waN98LE+qL4XPtoLHOuDGgaB+gFEPQdIR0kmCCtnaVMEUc5wj3UDR3vhIz3gWx3gkR7O0X5QOsatH6a0XkAxwbfGhI40zgRI/QLJ+DBmnmLnSNZb5c5WWUK/6tl4rSP/y+5VcdPiK10nBc7cgY4V2poUuQoYM0c5UqTeK3bEaZO/rm25yhp/tWu5yhY71FYWmsO1jgzBeESWKKGdpG1RATMlbskJbMGqzkWhaQ6XDePSYVLaB77h+IeYg5kszGRgJkNaS7i5ADMZzJAROooQmwbZ7yh4/xTmnm4duDIvLJt5zRVHFFOAdBqQToPSKUA6uV82Dci+OZaDJOOwZAxuHAXrh/ZJhoGGMbBxmNM4ijQMgPVjnMbx/ZIRsHF4zxxQP8RpHOQ2DHIahrkNw0D9IFQ/BB0bBo4NceqHOPUD3Pp+bv0gopwFFXMYE8XVC5Qpg2uDhDlBaDy4bgGRj0PqGa5sGJGNI9IxVD/LVc9QbAjXzFOGIKaaInVzqGqm2pGjrAnSkSYtiZqWRX7LYm3PrrB98ZX+7UO9q4c6lkWO2H92lihriG9P8C1h2lGkbKnqtoq4KVfXsSpsyta0LVKOtNCRpdgFyhbhM7NCS0pgCtc2p2hb/EBrXuyIVbdm+Ta/0B4kGY/AOEfopwWsV6DzUJI+4B+0b90zR1qLuLm4hw9m0jxjVuAow0zyn9ocIJ0S6jxzi+eDhZPBygVf4ZSvsDuV2NAMpUHVFFc6/NQcLBk7wE5kT13TD8Y4jaNA4/H9khGOZOR3zfMzxVMLxZNvtITAhuN7pc44lfflT09kTx0yjvKO9bjnK77cibnCtnY0DR7rgut7u6NbvsIJf+WSZWod080gymlANQvLJ/7g9nsKJxcWt2VdMUIfIZkoZvS7fav+5fOe8vlf2gKQdAKSjvFkx1HtvHZitXTpgXpqo6Z7q6Z9paq1InLnjfOng5uX05vXjNNLIkdO1FyqbV76bf/GwdZSa/p8fPPmwvr1P/SXq4wpe2AjvHw2trSzsHwxvH4puHExsnUxuH7p1205kSMmMkVoNkjpvYRmnGTnROYFoTUgdkSqHKlqZ6raERVbQqR04B9iDtTnYCZLWhefAZcB2SzIZlFDWuQoPs/un8gcJJ8WGIKW0dSXj772Zjcae2LHOsP9iRMfffl5Yu0sIJt+ag6RjLb4lr/86vPS9lVuwzi3cQRqGOQ2jkGNI/PF3T89+vz0rXuEfBhuHAfqh2jpwI37H3XN54H6kf0Nx+tU/e999MWFW+8JJN2cYwP7GofVPYFHD78KVc5Tplme0iM0hFFdSGiM87X++OqVR48enrt5n1JOYPJZqPE4rZ+5/uDDvugGT71AsWFcO0tbowJXMX7q/qOHXy6UL5CWrMiVF9hSVZ2r/9lWOvXORw+/eLRz8/1qR1bcVqGsKZEj8Wt39PbHnz969GiyeLrWnqjtKL/anL1599Mb9z9VHi/U96UbB9Oe4vYnX//pV84A7YhjqkFcOUgoj2PK47Q1XOVI8fV+scFHaiZJ7QRPPoxJh/mS3h/ZHNeQBdgMzGZIS5GwFGDm27f7EJvBjBmxPY8wiWcHTP5JzE0B0gmhKQTJp3/bHPrq0VdNvhVAOiEwBCD5dPbElU8fPRJqJ5+u5CDJ4O6tj/Indz/+7Guhemq/ZBRoHNrbOnT6KpffefDo4edd8W2RrbhffhxsGDp38z1tf5TTMMBpmAAahq7eea+4c+Hl+iFUNsq3lJMbZzIn36a1XsqcRGQTmG6O2ziMaL1caW9n9NSNd+5/8ejLnui6wJzB1FO0KXLq+n3tRJ7bMMJTzQDSYX5L5T/dsUvvf7l+6e0b9z+ta87X9J8WOHLV3Vt1Tan0yZu7V+9+8tVnb7QX6jpXxa5UXe8J2/zatfsffv71o6bIDm0MiBxJUufduPL+xXc+rmlOCc2hAx1LBxzh0/e+/E1zrKYlh6nHcOVxTDGCKvoJ9VS1NYqrxgn54OPI+nFpHynt5b7+o+4hAEMWYDOktUQ8qXDPm4OZFGZICR0lRB//JzLHlU6BsimROQTKJkHZ1GNz8yuAdIIrGeczvnDlzIeP/lylGntS56becgc2b717pHnhs6++Noxk9zd+s4FtCRSjyxfLO5fe/+LRK82xlxtHkYauPXOQYlJozcGK2Uvvvl/YPr+vYRiQjPVFdlbO3uKzQa5kCtOHYIWHb03z1PN8e5annelNXsisn1/avX7304e/ti/sb+zFdbOnbtwxTC/zlNNCe17gTtf1XW0N7yZO33b51j5/9P/9sT0mcperbbFfDZ+qdiYKp9/15LcffPHnmcULdR0V3BQWOpOFs3eGkye++Pqr1sgubUvVta+JbInNG+9fePeTWvciX+8VWwK4epLWj6KaEVg5jCqGMcUIphjBlAN87cyh5gyuHcWUQ6hyEFP0/qPMcdkMblvEzQVYn/k+cxCbgZk0bkiKHYWnTfYfbC70l+bmQMUcqJgHFXM/wNw3I3QCY+jxEYls6rfN4UePvmryrXKlk4B0UqSbuvngo+T6eaThm5nNucJuX3AVapi8/eDTyMqFfdIBruSxuS5fJbZ26a32zCef/zm4tsutP86VdJ+7+Z62PwZIRildBGwYv/LO+4WT5/bVD7IjydO3PhZZ/CJzCtYFhaYIrPDgOh/QOIJofHD9SH9wK7Vx/vXu/EeffprZvompp4Sm6Omb7zPjZUAxRjdXCGOsyp1fuvK+O3L5d93lL7542J869+rA7qvtpar2Vb4plNq60ZfazqzfuHL/g4MdFdqRlE6duPHe55rJ9c8eftWXOk8b/SJ7itR6T1y8c/nWXVQ1iirHpN2hdl+ZUI6TqgFcPozJjz82pxhAFeM19jhlmHnVnaXZBVTR/yObgw1Z2JCF2AxuLeLWAsRmnhln+t6ZTcyYftxkmfR/0xxHFeCqg1x1EFA/6ywEqQKQKgiq/YDaw1V5AYUfVPoRfQjWBQnzImpI/pW5ErkHVExB8ilIPrXXUh/vW+Uz/9UU+eJPD/Onbg0kTnpyp67ffW8+t15rneVJJ3mS4/tl05Tq+NUHn75mGIGko7GVC9ff+wSVDj2tc52+cmrjMmnKTRVPP3z0xdGuMKI4fubmfXZqE9UECEsKY0LX3vnw0rV3Z/InP/38U9V4CZSOoupZQDaF6jyIcpY0xVHtHGVJ4BrfQGwnt34e1/omMrufffmwvt3PU0+dvv7A7DtFt1QO9W2J25YbJnauvPf5gY6i2BI4dePB1pV3X+1c4tvTwvYVkSubPXV7JH/aNr/92dd/VntOCU3+ifKViczW0YHSo4d/boqefrWjdLCjLHYmV66/d/+Dj48ntoYTm5dv358v7+5d5z/RNoIpRgj5MKYYq3Gl+eysyOLDtROYtA+X9uHSPkLaC77xY9x9wYYcxGYIWwmzFB/D+gHmYCaJP2myP7o5QO3naAKANgYxCdiQRc0l3LJGOjb5rhNU0ybl2sKca6ijBKrmQNXcC8wB8klYMS007VW4qW+Zmy2cre9N68cKgZVz19//nO1PU4Y5RDLKlU4YR5Nr529wZWNw4zA7lv3q0edHOsKcxsdz6u2+5ezm5f0NM5Ri+Pbd97dvvCeyRXduf8p4T+IaH6L18dSeq++8f/L8Tf1g9N6HH5689VGVIUyYIiQTIZkArvbzNLM8+Tgkn8ZkEx3+9czWNVLrq7FFb7xz//y1u3yTb/vaA0vwFO4qC+1FxFmYyu1EVq/WtpTFbWve8pUvHn711tBadWuZal7k21LxjatDme1qW/ztjz6Lbt15tTVz6c4nbw3k5eNrjx79uSm8KzT5D7VmhCbf+sUb7969p+kL6QdClVOXF0onSdXIt8zh8gFcMYAohlD5SLUtUmOPYNJBXNqHSXoJSc+PYw4yZEl7GbMUvoH1g8ylYTaNG5Iie/6/aW6/KsBVByFd9Kk5rsaPOZaELecJ5ybdehqxVQjXOsRmeeYCl0nyTDnQkKIca5Da90JzM5BymjYFn7bUvzT3ZdvsCiAdB2SjvMbh1ObFDz/+WKCeEbMLkHSsuHPt+rvvl0/fLO++vX7x9pdffzGV2+I2Dj82t7Cc27wE1o/Dx4aMo4VHDx+2B7dOX72rmlwnTHGEiQisicvvvF86fX1fw6hlNPPo4WcDyU1UMsnTeEHlOKn3w7qAwBTi6UO0IdwdWF/cvsqTTaKKaWYk/uWXX/WG1zZvfe4KnqntOVnVtnKo79Sle5+ef/ej5SsfVK59dP72+18//NwZOSd0l0lX8UD7Unzj6uzyObq5Mrd5472Pv+qM7ayeucm3Rd7syT169GdHcKfWnRW5Mr8bXF2/cOPi7fdw5Risnv6ty6sZiX+rzqHyYUw2jMuGCdkQLh8k9FOEbgqX9WOS3h/BHMwWYEMONqRJewm3Fp+h9qI54b94D8GkYCaJGdMiWw7WxQB96rvMhQFdGNRGOfoooA1DmhioiT4xF4Q1fshQIFrO8OxrZPclUBdCVFFYPQ8qwwATwyyrgC6G2pa4bBZzbyHWsqD9FGZfojtOgtYy5d4EVPPf31tnIPmM0PT4VcRfzmxO/bYp9NWjh03z6/vlY/tlXkA+0RPe/OKrL9/oTXPlvj/25t/+7Os/9FX4uhm+doxUjaxfvnPh7fuIdBBoHIFl0+0LlczuLUTlJa0Zvj60fOn+nY8+vXzrPWZsFTXGEdk4qpu/fPuD8ulLkNxDGsPZk1c/+fJhfV+asmUhtZdio6B0jKeeBRpHUM1C7/xiefcmT+Xlm+Kkxrt5+e77nzy6cOtji3eLbili5qhiZuudDz77Zd/Gr9qyf5w884f25LufPCpd/OBA5wZtipDuYmL90mT+HGoI1o9VPv3qT/c/+tQaPlXbdaJ+OPflo4dO/5rIESeZOdoS3rz0ztW37wsYX01riVCN4oqRQ+YpvnoElQ+h8uG97JnDZcO4bICQ9e/lxzPH5kh7kbCV9tZzf6ntB5pLwUwSYxN8WwHSRb/THFcXh4wlgMkRznWYyUBPzEGaEKQOw0yOcJ0EmTTi2tyvDWP2NcCUFLq3YEtB2H6eZ1/md55HzCV+8zqoT2L2CkcVhjQhjnKBq/IDytnvrHOAzAMpPEJTBPruNzh75h61Bnc46nnUvAjpo/7VW189evSbvk3EmD2eu7h15S5tjgDSCUg6ur9h/Hh87bOvPv+ldRpSzwpdpb7IifyZtzG1l2fOcBXew12pjx9+/dWXD5mhEmrNYpp5gSNz7e4nK2ev4Mp5xJioZcff+eDTczfu1xjmuIpJgo3CqlnKmsK083xbuje4Ujj9NtI4gmoXYMX00fG1j798+OjRQ4t3q67/vLi5XDh3P7h+/WD/qdeak0J3GWVDS2dv3f/0y9faC3xrWtx7InXy5kzlNt+Ze7WzdObtO9fvfVSl8/GdicNdsUePHtrnl2vby0Jb/GDn8ubV967c+0xo9Fc5EqhynNZPnb/32e8ck7wn4J41R8gGf3xzpG2JsBVgQ+a/Zy4F6JMoExdaMs+bA7VRkC1U9VyjHNvC7uuIqQRpwnvmuNoIz1kBTCWq9QxmLfO7z6HOirjjAmIrC5pOAkwMt61xdTHMtgQyaaJpAzYV6fZdninPVc7v5QV7CKEpCslnvtMcJJt6qzv96OHDvswFyhZ5taVomll699NHiROXcGOSr52+efeT9NoF2jAHS8cQyeh+yXHXTO7rr74YT21wJaOoIT6e3lm+8DamnMBseYhZoJvKC+Vzjx79iR3IY8YoTzZBGALvvv/FzpXbaKOHNEYhjbclvPnFw6/D69dJnYfPRkDpOKKZ4zYM8XSzY/HVtUvvoIpJsXu5qnX10MDZ+Y3bXz/8zDCzTLdsvtES+PiLh+PZnVd7N/n23KGeE1WuXOLEO18//KIreUFoiAmas5UzN4OVq3zdPN1U1E+WjZFzQkf8YOeGfqL8p0eP2oIbVa40rvNWORK7N+5dfvej3zkXXmnN1VoDffHtjx/9v6+ZxnjyYexJqdszh0mHcOkA/mT38N8yxzUVQWMR2gNnXYLZPGjIg4b83/G+9S/P5+I8Nsm3ZQB9gMvEAX0c1EYhdQLURQBdHLMUQX2Ko0tytE8ba4SrTQo7LyK2TcK5CRtLqHWZy+ZQ6yJozFOuVdi6ImrfxVwbwraziGODcG3BbJ4wZgF17PnzOUgxCylmn7RUj9AUgRTfe99K66eiO2/vXr1/4tZHlasfbN74OHf2TkvqIm2O0c5KcPPm7vV7Jy/d04zlRMwCLB37lWu+fOHOqZsPtq9/cKQv33h8dfPK/a2b77IzRcIQgRQzPF1QpPPs3Hig7Mmhphym9Y3mz525dvfUzfsOT4XSBRDFWH9y68y1e2dufDCS2RHqw7B6lm9OYrqF+qHiyqXb21dus9OLVe4SaUmKmoqHnJnTtz5RTm/+V9+J/MU7F97+aOPyfc3sNunI0k1F1Xhp68Z7527dr9z4+I9dqabI7u6tj7ZvfqQeK9d2rAtsYaErgxt8r7lThXN3zr39fvnyg7eOVwh7pi915vTNB2duPti4/GD16rtrV+/uvv3B5vUHr9i9tG4Olw09W96easOlfbi0F5N0Y5JuQtL995gDjEXIUKRsFcKyBLMFmH3xLNMPN5cAdQmUiVGODGjIIJY8bC/Dri2uLgboolxt5Pk9BKgOQ9oYoA6Trg28+SS/5RTVcop0n0SbtnDHKmJa5BnzoKHAMy4ilgJlr6D2NVA+jyhmX2AOVnhoYxhWeEC55/vMceQzAlueb12kHMu4rUxZCyibgpgUpA2CuhCiDfCYEI+J8U15gTkn0M8hjROEpUQwUR4bR9TzoDYCqbyoMoZpw6gpgbMx0pzGzWnCGke1CUwbBFULkHoOUsygqmmechbVhnE2TJrjOBMVsXGKmcX0IVgxici9kGyKp5iEVaOkOVbVulLVfaq2Y6O25yTdtnOwbYl2FihnWWSPHmoriK1R2lWgW3LCjrXq1sqBjpWq5qUa9zLlLNH2lMAYFJljfEuIckaE9pywpSxuzle1FEVNxRpXlm9LU7YIn50TGhZwZoFvCvFNEZEj+svOxQPuXLU9KmpK0uYYJn9qbuAF5v6euy/EVKKsFb65vAfuxzUHa+OIIVHXvSvsvMZrOkW07O6Z+859K1cbgTV+UO2HLUs8awU0lBBLGTLkCMcyZinx3dukc43fuo05N/DWTdi8yDPnQcUsoJx7gTnaGIEVHkg+833mAOkUKJtAdGFYl4TUQZyJ8aQTkHQUtiRoZxk3L4ntOZzNEKYkpItC+gAk89TaAjytFzZGMTZC2fIUGyFtWdiYoEwRWOtHNT5AOY9o/FyVh9BGCY2fNoZwNkBZ8yiToAwJVDUPaeYh+RiunICkszzNLKbwkmwA14UFxgjO+PhNBZFrRexeoWxF2rEksKWqm5YFzsW6tvUDbfnfDe+I2ivVnZu0qyB2lShzjm9LYOaIyBoXWKK17oK4qVDbXqlqLtS2LwmdKZErTVgSfHMYNwQF5hBtmBfZo3xHvKp1UezMHWzOi62JaluazyzQBl+dIyhgA4RuWmTyo/LBF5tDG7vwxq6/7UwYNhYhY4GwLxH2CswWIMPj/IjmIF0U1EdRY1rkXOZqo5wXnpVwNZG9sxLYVKI6z+LOCr/rEs++THXsQqYS5lzex8R4lmWOOgRoAqBqgav0AYo5rtL31ByomH0cuQeUzwjNUUixh8zzHfet0hlANgXpQqRjCTOmqKZ1zJgRNq1ypPOgZAZUBVBDElAuYEycqw4g1gzChOnmMm5M8ptWxM4caS/BKi9kSHAVHlQfBhQzJBuHtAHaUkCZoMBRRvUBzJID1XOoNgzIJ1FtAJFM4vooppqkzEmY9VG2LKwNUpY0rJhFdfOgZBzVzCG6QHXLstiWOdi9U+2uHOo7Q7srB/vOEo5ybcdabXOmqqmAG4L8pjJtjta0V0SO7IHuHXFzqbpni7Kn6ZZlyhDkO/KE3idwZPjGQFVzUeBI1LWt0LZEXXtFYEkIXXmKnadsSUzr5VtiuG5eZE/xTcHaloLIFv5t77LIHKCdGZ5i7PF2VT6Iy/txae8zK7mev9McZCwQjiXcXnn+4uvHMgfo44A+DuhjqCFE2dJcXeSHnM+BugTs2uRq44hzC9ElSccaaijQ7Wdge0XUfgZm4oDKD6kWnr9v3StvkGIWUnhoUwT8i/L2vLkpQDYFa4OUvcRTLXNcfB8AACAASURBVPBMBa5yATMnubJpQDoBaoLC5hWMTYub11FdnLIVYLUPM6Y4Mg/KxnlKzyF3AdEFBE2LqCHCd5VgfQS3lgDVLKlLcGUTmD6ASEcxNoGoZmlrEdH7+PYipvUTthKomMF0QY5sGNcGgMZxSjePyUb51gSsma9pXxG3rR8YvEA6Fqtb10lTSty8Qhmi1e6y2J7+1fCuqCn/St8W3VSsGjhHOXLi1jWSCQmalgjdHO3M0ex8dVuFtkRruzbFjkx1z0nSGhM1VwhmTmjL8jQzQkcM13vETSnKHK7rWKatkQOtFb4hWuVaJHUekS2KKMbFpvmDlrkaq59Qjj09IsHkA4h0AJU+3q7umdvrrX+bOcKxjNtLf+Ns+t9gjquNcZg0YMju16cQyzJmyZO2JPCE3QvMcbVRftclwl6hei5ijjWq8zxsyJP25X2aJMikOZowoH6ROVDuFZqffhTne83tV3h5hhSgjfGdJYKNCZrXcWO2yr3JU0cAyTRX6YHZKEfmAZSTHOk4ac0gupCwaQllo1TzGsLGKXtRaEugpvh+2Qyq94OyKb4xAannRLZFjFkQOBd5uiBqWwSVXlwX3ycd42nCSOMQzvhR5YzQmuHpZ0S2LKSLkJY0rJjBmQCsXeC7lyljrLZtRWhNHerdrW5efq3/rLi5/Er/6dq2irh9DTMlBc1lnAnQzUsCY7Cuc0Nkz9R274idueqeHdqeFraukIyPtOdwzSzfnqL0MzWuosAaPtC2JrLFD7Wt0sZ4dXMZ188J7AmeeoayhjHttNAZpwzzdS15oTlQ1VKgjP5f95QpzRQmHcKkQ7i0D5EO0GY/oR7/b5kjHBXcXoGMhReYg/R5gMlAbBZk84ChALA5gM3BTBbRZwA2+33mICYN6JJcfYxoO400bQu7LyJN24Kuq3z3SYiNU9YEl4kDTAzUh5/vrVxtcM8cz7HKYTKIbRFi4/s1fkC9AKoCoCoIqgLA4zt+P6iY38s35uQ+SDEntERA5Syo9AKK58E9M5uunKebNnnGEmXLQ9owakyDKj9sSHO1IVB6nCsd48omAekYKB2HpDOkrQBpF3iGKEc5i+hjgNJHGuKIPvJK5xrPkKCdBYyJkJYsrPahuihX7uHpgpB8GmMCsNorsGYQfUBgz/H0ftIcB1U+TBfgyKYQzTxXPk1oZ2HFtLhlucpdOTBwuqp97VDfadq1VNO6SpqzIleZMsd/2blB29K1neu0e+lA7wnaXa7qOcl35sUtFcIYJe05HhPkO/OEMSp2l2l7qq69InLma7rWSHtG3FwmjAu0OcnTz5O2OKpfoF0pyhSucRdpa6yurUxZEqKmHMHO0uYQrJnBVWOEbAiXDtQYZjBpPybrx2V9qHywxhnh6yYISTfR2ENIerDGLqyxi2jsAn6gOcK+htuX97S9eJYJcW2gzlNU+1Wy/SLEFGAmjegziD4LPXno+p11DtYlQH2CdG9j9hXUVgGNJcBQBJkUpInAmrTAfVroPgfonq9zIY4uhDhWENsm6T6NN50StJ6hmjZBlR9U+789XaIMQIr5vXyznpPPCYwxWOn9QW8NFVMIG0MMca58kiOb5TsWcWtB4Chi5jQoHX38gRzZOKINwdoAYs6ghhxpW8JNBb6tgBlShCkNaUO4PshR+RGdH9XM8XR+nAmRljRhjAusBZJN4Wwa0QQR7QKomoU1s6BqBld7eVofYYzgbJSyxDAmQhkipCUtalkV2IqUsyhwZEWuUpWjUNO6Xt26frDjhLh9/UDnCaqpImwqErY0ZU9R5jhtz4rNiSp3WdhcqmlfETcV6trKImdO5MwQpihtS+CGEG3yC8zBGkeSdiRqm4oiV7q2JVdti4mdcYpZ4JsihH6BZhf4jE9kDvItwWpninbEUOngXnnDpP24vB+T9WGyPp60D5YNoNI+XNKzF6yxC23oJBo6f5A5xFTGrCvPgnvBzCbRdp7uuIy4z8HNZwHjMmZdgmwbRPMpgPmO3grpH88y8UyLuGMb0KUhXXQvgDYGamOQNsbVxgB7HrKtAN/VWznaIOZawZ0roLUI2UpcfRLSxiH1AqB+brpJGQDlc98yJ7REYOUcJPf9sHdfM4SlRFoW9966gpogV+PfJ5/kyr55aMiVTNDGAmFLIWyUxwa5Wj+o83PVYUQfR3RRwpzmm+J8e540ZoWmDKpL4vowqAlC6gWOag7RzgHqWZwNokyIMsdQY5RnScH6CKQPQ6o5UOUDVNOwdho3B4XtS8KWtVe6TlZ3bNT0nhK2n6xqO0E7l2hHmbLmRfa8yBKpchVFrlJN+0p1+1JNxwa/Za2qZZVylChbGjMnSUsSM8UF9gxtz9S4i2JXtrq1TDcVaHeZb8tQljDJRglTCGUDAmOINIeq7HGhPVnbnBU6UkJHmrREKIufYHwo68OempN9Aw6V9uCSLkzSy5P0Y7JBXkM32tCJNnTi9Z3gH/+Gs5LS4zxnjmsoQmwBYAsAmwWs22THJcR9StR/C2u/KB64wWs5J+y9yjHkQTYDshmQTcNMCmJSuK0s7roIs0lEn6CatoSdVyB9+uk9BFebArVxyLzEbzsHubbo7qsAk3rOXAzQBhBTlt+8jdjWxR1nAE0MUS2A6gWu+i+mNUFVAFIuUOo5ROEFld+cz4FKD6icBZ8cmnAVXkDhARTToGIOknlg+SRX4eEqnr77muQoPBzFDCCbgGQTkGyCK5vgysYB6QQgneTKRjnyMUg1z5WHEGMK1kb41iWMzdCOJdIQo+05niaAm+KgchZmwvsVHh4bBbTztDFN6GMic5HQRwTWHKr2Y4YYpJyG9QFANoHo/Ih8CmcihMbPN2dRvf9g65a4uVLbscu3Z8XNS7g5IWpe5FsztS0VkWvpYNdOdfPKge4ztLNU1b5NWVK0M4cZQkJnDjOHq5pKtD1b274mduZquzZoZ0bcvkpZYgJnBmd9fGuCx8zT9ixlDNe2FGlr6kBrWWCL1bbkCVOAb00g+lm+KYLJhnH5ECobxOQjIuNCnTuLywZ50iFMOojKBlDZACYZwCR9mKQbk3Rgki6BfuKgPYpJev4+cy+Y2czxmDzEZEE2DTu2hf03IfsG1bLLNa1AlhUOmwfZHMSkYDa5F4RJQfokaimRLRcAQxaxLnOZHIfJw8/MlUCaGKKJIcaioOUkYCwh1gqgjz9vDtQEIWOOcm9zmSxuyYPq0HfNbC5AqgVQ7hHq5nhKD6j6jjPhZ74/5+EqpgDFPKJPg7I5SD79zDv+GUDm4cq8XJmHI/dw5TOAdOpJprlSD6ILVzlXcTYhdJYRbQg3JznySdjg3y+fRYwpUO3H7UsIm6RcKwQTEdoXEY2Pb4xzlOOoPsRpHEf0SUTmQc0Fni6IO5ZxnZ9vy+MqL26K8KTDkN7PYzx0ywrqiNd2bZHulbqhi3TLyqt9O2JXsa5rkzRHRS0lzBgUNJdQY7iqfRm358XdO4Sr/Er3Fu2MHepcp8x+QVMG001SjhSf8Yqa87QxVNO+JbKkDrVvCKyhupYKZfSJXBFYN8k3B2CVR2BPkYy/qmWRsvpfaS2SqglUNoLKRnBZP64exRkvTz7CN/h4ilFEOohIB7HGQaKxF5f0oNIeRNKPKAf4ugVe499Z577D3N4PyJADDBWALcJMlssWuGwRevw5puzeFT7IpLlMlsPkOUyeZ62AxmWBe4dnX6/uuYE3bYgHrqP2JVAXA/Xxp+ZgfZRy7sCmIqCNgtoIqI2CuujzZ8KQOgyow/vVYVAVhFV+RPXtZRyoCgBKH6Vf+H3XIiibAuXeZ/cQ321OOYVoAuLWHZ4+Acmnn963ArJpQDFLWrOgPkY3rUC6GNQ4/sTcJFc+vk8xhTKJ/TIfZswgmjDtWMX0CaFrCWdjAtcirA3yTUlINk3oI4B0BmPTkGpWYMijuojYViF0ftqex1UewpRAZWM8fRBqHIH1UVg6iRvjuHL+QPvputb11wauC9yVA51nKHO8qmUJN0RE7gphSlS3rdO20sGu09VNK4d6z9H2pQNdpylTVOTKo/oFoT3PMy6ImxYF5kRd+wmxJVndvU050oLOLcIU5DfnUN2MwJZEdB6hPU+y/rqWMt8SP9iyRJvDda2LFOunbXGe1sNn5/fAobIRTNaPSgdQ2QCpHqtzJXD1BCId5EkHscZBsrEHbejGpMPVlhifmTzoTJDa43vmiPpO4HXXzw60rcGWJchShs0rkLECmpZB0xJoKu+FayoDxiJsKEPGRdhQggxFyJiDTWWOaZHnPAnY1wXt55Dms1U97xPNFyAmCzFpRJ8D9QWAyYBsFrWuIOYy2boLO3eEPTew1rNVvTewpvPCrms81w7lPg2bijzbCsCkIV0E0MchXQzUJUF9lKNPCjsuI7YV4EX3EGFIHQZVe9Oae1PBT5dxAVi1AKv8kGqB0C38sa9SZY5CynlI4eU8cyb8neY4qlmuYgE2FwFtlKOc5io8gGJyv3yaI58BlR6Rswjr45i5yFGFUeUcR7lAGOOQLiKwlxFTTuhaw9gE31Hi6SOYOQmovTAb4ypmeUwC0vhJWwFmIgLnIm6I07Yiog2RbASUTxHaKEc+DrIhUOHBzCmezk/Z8wQToq1ZVLuAmhK4IShuWcUsKXHTCmlL1nZskk3luv7T4pbywd4dUdNibecJ0poQukuoKUa7ipgxUdW8iNtztZ2bpKt0qGtD6EzVta3yrRGBK4MzPr49TTI+2pEWmKI1rWtCa/pQ24rAFqt1L5LGoNAeRxgv3xyGdbOkNY4yc1WuNM36cFk/JhtCZYM82TAqHcKkg7h0AJP0o7JhVDJQawsI9NNYYz8h6UEbezDJULU5hCuHBLopnqzvaZ0DXnf+7GD7KmheBMwl0FSBTRXYVIRNBdC0uBfIWIAsRciyDjbtQk27RMdFuOOaoO8O1nVD3Hcb7bwg6LzCa7oKt2/BtjXEsQq5VrDWXcK9CzJZkM3T3dd4neeI7nNo8xbZfBa0rfIsJcCQ4rApgE1D+jjMJGEmCerjoC4O6uNcfQzURyBthqNNcnVJri75AnMveA8BqgKg2g+q/aBq/o+9S4fsUVg5+5vWXI0l/GJz+xVz/OZl0laimtcoSwlQTgLyWa5i4mX5NFcbgbQRjM0ghjzhqCCWssC1hJmztKPAMyYIJgFq/ag+DKpDIBtD9DHUWkZMSdKxSJgStDXPYyOYIcTRzvH0C1xNANIGIU0EMaV4TJS0ZjBjhG9N4boorovBSh9PEwCUPlw1hym8r7UuHexYOdR9QtSxWde1Q7uXxW1rQnta1FSgLTHSsUjZCsLmCtW8XN25SbetH+zYEDZV6lqW+bZ0lS2Nm6OULUGaYwJnlm/Pi1rWBa6lmtaK0JmpaioILDGxLUKyC7Q5hLEhvilCWGJCR4l0pGuaiiJ7rNqZo0whyhLDdPOYegLVTPPkfZi0b2+7gMn6UMngXoSmBUIziUn6cEkv2tiDNvagjd28hk60sQtt6ESOtfPqO7BjHdw/OH52bHj1jYGtIyO7/9W/xbOUYUsFsW/Cjm2i9SzRdoHsvYP0vk0MfYD33qb6b/M6rhOtVzHnDu48DTt2YPd5tGmX6L4qbN062h0yDraZBlzGvha2r0XXwarbGFWHXdViUbr0qiaF1Gk/atNLnPJ6h+KoVX3UojpqaTxibnjLVH/YeOxN9tgf2WMHdK1iQ/llox/URUFtDNTGfsg9xPPmAFWQo/JDylmeYuZXLdk3OnP/1Zb9TXMcVnhBxXfctz41x1F6QTYBaKMAk0LZLGIMktYiR+nlyKZJ6xJlX0HNJdyUw5koqotC6gCkj8K6MM+chs1lnrOM2Yq0vYhasrgxiTMxXBeE1V5YG4Q1PpSJYmyCsJQpU4a05XBTCjWkeboAofIiymlQ5eU2Hgfrh7n1o0DDMGUIUpa40BSQTZ6qHznReHyz8fjG73sWjw6tN05vvdG3UtO6IurYru08KW7brGlfp5sqYkeetqYF1gzfkuTbc0JbinKvCpsrdMeG0F2qaSkJm4pV9gzfmBAYYjgbIYwRwhgROLJ8V552l0RN+armgtiRFtoTIkOYb5gXaOcJ1kcwIdocFFlCVY6kyJmocqVB+SAqHXjeHE8ygEn6MUkvLu3bM8dr6OI1dD5Ofcc35v4wskM1n6A7LxzqP9Pg/4jqvMQf+ZToui4auoe2XSLbr0H2Lcx9jmM7ibdf5LnP0H13eW0X+MMP8I4rwoHbaPO5hqF0aenYu9v4uzucd3Zevr3NuXUSfPvEvrdP7L+++YubG7+4tr7/6tq/n84Lyn7u5eVfXF76j8tL/3qh/PMLpf+4UPqP88V/P1/893OFfz+b+7+nsr/wzFQJJQ7MVIQ1MVAb5OoiXH0Y/MHmQFUQUgcg5Typmz/St3ikpyjQ+X7fnmsYKOOK2f1KH/eFewhQNgUoZiH5LG4p0s0boClD2EsvKT2AygdoorAxC+pDqH2JZy5RzWuoKStsqvDYJGHL87QhhI0AijmUTQCaIGFOA4YE5VzlGTNCZxlhwoQlB2u8PCbJVUwDimlQ4SWYOKQLCaw5VJ+gHDmuZJDb0M9pGILq+0H5JM4EDg+v/W7gBN9VrOk5c6h7/c2J84faVut6Nt4cONk4tE4bo3x7iWdKiZrKlKUgbt2mm9dqundETcW67g2+PS1qKlCGMGVLYIaQwJElzTFRS5m0Z6va1wTObG3LEmmNV7nyfMZPGaOYdo4yRXFmXuTMkNa0uLVMW1O17hzfFBM5kyTrExiDuGaSZP2odoa2RFHZk8MR6WNzuKQXl/Rgkl60sZfX0M1r6P5ec7+f/ZDsuMQf/RB17fy679ofx84IOi5y7CfxzpuIe5caeYC0X6ePf4K3XxYN34ebzwq6roKWdaLtImA6gbZf+rVrLhT+TdqPlMJ4JUGuxPFKHFuK4YtRbDGGl2Jkco4oR7ByCEsviINTgmIQLQV5qWlBfgEtBHiFBSQ3D+d8vNw8kpvjZWbxwBAyM/ObGnacw+a4ujBHn9jHJABtHPyOM+FvmwNVQUAdgFU+Ujf3Zn+p1hJ6zRF7vaMIKud+3Zp4vXsRUc1xlQsvMPf0fA6Qz4PKBa58HpTNw0xE3LxDGLOUe4Onj+COFZ56HjWmQMU8YcqCqhBuKyFMknCvooaUqKmCMnHSVgRVPswQA6UTkHJmX+P4fskErPbSzhJH4eE0jvDU80JrDlLOwkwElE8ibITbOAw0DHAahqH6fkrnVc5d+3Xfyqu95yhnRuxeRy0JUesqZo7xW9eElmT91IU3R04e6D8jdJcP9e3Q1kx1c4liQ1WuImaICN1l0pKo6tignIXa7i3aka3u2eab40J3kdLPCe0ZXOsVWHKY0SdqWuKbY9VdJwS2+MG2Cm6er3LmUWZKYIpjygmBOYqpvSJ7ijCFxa2LpClc21Sg2UC1I4PJjmPSQUzWh0oGeI0DvMa9I5JeTNKLNvb8FXOvjrwL2k/yet5Gmi/QQx8dHL7cGPqQbr9QPfIh3HKe6L4B2rYEHVc5lhN4x1XIucMfusNrvUQPfUi0XqjtvxaJ1t89TeVm0ZtniXd2kXd2kHd24Nsnkbe3kFsnkJubVHpGcOkEcH0Nuba+/+r6y5dXuRdW4OgocqnMuVjed6G87/ziy2eLL58rvnyu9NJ2eV9wpm4389LE+Ou/0Pn4LTuIZVXcdQU1L/7AOgerAjyl7/fd5d+2pyH51EFHqr6/VG0N8lSTv2xKYdoFzg8zB8k8sMwDy2a4ci+gmsKMBY7aj1jKiDrIcy7DhjjpPsEzZMXNFYSJEfYCTzGPmhKg3Isb05BygbDkuUySal6FNAGOZJgrGeY0DnNlU6gxwpHN7G8c5jSOcBuOg/JpgW0F1wYFjiJP4eHWD3IbBnHp4Fv9S//ZtowZ43TrScyZq+29RLSs1PZfELUs1vafpa2ZOnfp9e7F+qFVwhAStywS5oi4c53vKNX1nKNdxQO9Jwlruqq5SLIBkSPD0/pIV5lkA4LWCmFJ1HSdFNqTB9o3CGukqqnE180IbElUPS20pnj6KbEzj1oj4pZVvjla11riG4NiZ5bSTQtMQUw5LjCEcOW0yByjVBOoZACV9qKSgb0692JzaEPnN+Z+N/0B1XVDMPoh2nKV6rgD2U7+qv/8H8Yu0W1noeYdwdA9pP0a//jHROcV4dB9pOU8v+sabNvCWs/ts6392u25sQ3ePYuVwsC75166t8u9u7vvzu7Ld3ZevnPypTsnX3p76xdbuZqN/P7bu//37s7+e9v7b2+/tJbYtxx/6frKv12u/Nvl5Z9fXPr5hcV/Pb/4r+dL/3o5+y9Fz76dzP/ezcNCdSfReRmwVlDnKcCQAnWhv2oOVgUxlb/KEOZrvW8OLP2xM/tWb6nGFv1Db+H1njxPPgXJvT+4zs0CsjlQMU86lxFzUuDeQSwFfvMWZsjgtkVYHeSZ0oByDlR6uNJJwpLl6ZNUcwU3pkRNKyibouyLiGYBN8ZB2TTYMLoXqGGEKxkEG4e5kiFu4yDc0A9IBlA2DsjGeGyAK5shtX5C7TnSl3/NFaWYIGqKHug9x3fka5ormCVFN68SliTtXhFYUnU92yJnUeI5+8Zo5VDvOmEv1zZVKGNU6MyibJh2FTFjVNS2Rtqztd1bAntG1LEpsKVEzQWKmaetGUw7JzCnMHZe6CqS5mhV+yrflqhrWyJswSpnDmM8tDmKKScFxjCm8tCWGMEGq5oLfGO4tikvMATF9hRfM4VJh1FZPyrpRyUDqOSv1Lm/MPf70UuQ6zTWcZnXdkXQ/4DXc1M0cPc/j1+V+j+g2s/inVdh1ynIfRZ0bqEtF5HmXarvBq/rlmjwPtVxVTUw+MFZ3v2zUDG8773T++6d5t499Yu93Nn5jzs7/3H31Et3zv2f7eyrO4vI/QvAB5deev8ifH6J/965X9zbfvnm+s+vLv/8cvnfLpf/7Xzhf18o/cuF/L8U5v7PTvbn57I//73ZQLi2Idcq2XEBadkB9SGuJsbV7lH7S3PKIKQMgSo/rvMf7qvUDyz/vj1LaL2H+xd/3ZIAFXMchY/37DLub/l2BGkrIPooziYhTRRhkog2DEmnubI5vqNMWvJcyRhHPkOZ8ogmiuqiHLkX1SYgVRQ3pBA2QTkWUVMaaBgFGka5DaP7G0c50nGOdBxWemClj2B8iCaAWjKoMU7bC7ghKbTGjo2eOORKkIyfYEM4E/p1z3LjxBY7f/7Y+Olf92+LW9cPdJ+knSWxe0lgTtCO6Os9pcP9iwJzrKplXeCq1HWcEDWvHGzfJB15uilHmkKkNYkxAcKSJE1RYdOiwJ6raV0R2DM1LSW+LSV2JEnDgsAU4OnmSIMfZRZE1ghhiYuaUgJroropJzLHhPY4wXgIow/VTZPsHKadoY3zGOuvcWRIcxR9PDnyTBp70Ppu3rEuXv3jlvo0j8058p++Mnab6L2G91zHWi4grbtw01m45cyrvRcOT1xGuy8Tfe8Iet+h+94hum/gnTfwlguI+xzi2uE1n9R0NX18Fn7vPHRmRfDg7C/un+G8d2bf/dMv39t96d7uS/dPv3z/7C/un0PuXYBWYoJbZ4j755GzK7/44Brw4ZWXPryw/8GZfbdP/PvVymN2Fxf/5WLx/zmRAnaL/3qh8L8OO42kaxt3riG2CmipgMY8Yl7mamLPm+OqwpAqSOn9r/dXxOZAFRs4Nrj2qjNCaaff7Fs8ZI9xVAGOOvj3meNZi4RjmbQvk/Yl0l7m24qAzAPIPLDGDyjnIG2AZ0iDUg8omQIkY1zpccpaJC151JjE9CFEF4TVC6ByDtH4MV0QNSZwc4q0ZnBzBjMVecYkrgvA2gVY7QVU87h2vmF857/6N3E2QLIBkln4bVvKsHBO67uk9Z7WzWyrJrdrXTmhIy9uKolaVmraN+iOtbrW9frRk7/vWeQ7cgJzWmRMUZYs35ThOzKUqyhuLgndy9Uti3TzIu3M8y05viFKsAHKGCAMIb45QtsStCsrdGWFTTmhI0U7M7QlTBkWcNZL6GcxvU/I+AXGgMgSEdoSVfaE0BalrRG+MSBg5/jaWUwzg0l6vmUObehGjnY+bw451o4dbQded/7MFL55ZPY23LxF9t1CO24Lh+8TXe8IRu4K2s83+u4jzl245SLiWCdar0DOHX7nTV77ecHgHbz9pnDwgaa39cFl+O4FcCm5/8H5l++d5d47u//e2f33zuy7d/al+6f3v3f2/6ftLrfbzrKE4deNvN+nu6sCtqU/k9CQFFMXBkxiZsliMjMzS7JkWZIl2ZJsSzJTDHGYk0pR93TNPM8FvB+cStIFPd0zz6y1L+G39jlnn3P2Lnh2+OY312i7KdZeruD+Nry5gP9wUvT9deD7E9oPJ+A3h4X3Ns6cLL15PfOno/Qfjxb/lJg4sxl/8yD55mWbDjKmSdceYl9n1Z1g1nWO54AumwP+fm2lS310iQ8STTDkoxztNCGf/LIp87599qu6+EeuCKkYJRWnV/6/8X7uH5kTnH7JGYa1c6g2SpP4Ael0oWCMLhgB+H10YR8g6KMLulF9lDIvAfzeQv5AkaCXLugCReOEeqaQ1wfwuhFtGLMuY9oQyxyHlH5cG6RLJ2FFoEg8iigDgGyM0IVgbZBRE4V14ctdW1+1bnBdWVgTIHQBpmZc3JVSD6ypJm8oR/b107cV48fVA9c4tkWuO4+ZEuyaGKbz49bku/bZ8rYVds1iqWeVZc+UetdY5hTHtUQaZln6EKb2MfRBVO1n10QI/WyJI8UyJcucGWZNhGNPUjo/2xjAlGOk1ocqRpiGGUo9wbFFCZO/xBajDKFSc4zQTLP0k7B8kFSPoJI+SjVKKgZZhgBLEyi1RLCf/3T9s+a+srxRE3ogn/tP3LlHtTwGrdu4Y4dmXCO9h5+3733SfQ92nTDbv0HdJ9yO56j7mNHyGDWvE+5DUJ8jPDuihoZcwvvmqwAAIABJREFUpHhl7sJgG3d1lpWb5WRDrJUZZibITc+Sob6y9Sj8aB9+fA2YH3/r4R76/ABLTEFPrxPPTvBry/RIX8lqtDgb5qR9+MIEnhgjkmPUUCtjdpQ1P4J+brUjpizDsQ5oU7BhqUiVwC3rdHn0NXM+QDYNSX2kfPIzV/wDewSUjbI0vlJjkCaefLsmdKkuTheN/kt9mWjCMZpwlCYcR5UhUDbFNERhTbjYnqdLJmnCnl/Mh6Dx+uj8fkAwAMkmScsCTTwM8HtQeYhpXQYE/QC/BxQPA/JJunAI1YRAySjDEgXVAZZ1AdMEWKYoKPVT2lm6eABThzFN4Iu2jY/dieL6bbY9U9J6fMESkvRkFMMbGt8NycCKauRQPJAVDGwROj/HlSPN8QsNmxz74vv1G7zutU+acwz9DMsSR5UTbEsCU05zbQukfrbElWeYwmW1ebZxju3KYjo/bk5g8nHSGMNlQ6QpSqjHSu1pyhh827lCGX3FjjShGWcZfZC8n6mdRiQDTF2AVIxwLFGGZqrEnmBoJoqtUVI5xjT4EfEASzb8cht3+irz9A36aQX4tfAglR6o3Itf9VyQNLyhmzy80n+C6BOwLvG2O/vnpvXP2/aqfH/9oOsWu/k+aN3EvCeFhhzmvQGYNqiGO6j9gNvylHCdsNu+03R0fXuEP7uBJkPAN4fAk0Pg8T79wW7hk62ix7uFyzP40VrZgo+b9OMnG9STw7PPjs482LuQmWGkA9jRakkuzH12A35+g/b4Gu3G6ptHS386Xjy3MEnfWSg8ShZeNasBaZAuDwDyGVARBPQpdsMhXZek//ynmibzQ7JpptJ3pXHlHcP013XzFy1hmswHSidZOt9XTSlKMQlIAv+SuSKZj+tYw/QJrmsNNsRZzhyoClH6CF04csrr7+dDvLjjByUTmC5RIBouFHYW8gdgXQQUDtD5vXReN53fDUqm2dYMLPMzdLEiwSiiGC+qbqfzOgF+L0sXgdWhi003Wa7sh+3Xqvv2iuu2SGOEdK8xjSHp8Ka4P6+Zvq4Y3jD7H6hHd6UjNyhb8mLtyqfuuSttK392hXnda+874hzHMmWcK6vLsy3zb9dvMEyxUm8e0/opcxJUjpKmCCEfY1gSpNZX6slxtTOl7hzLGOS4V3D1GMcUgWRDbN0sJOthG+dwxUipYwHXBcoci6TGV2xP4spxhnEGkvQReh8i6GRqpkhpH9fsR6VdqKDh5bkBra5Dq+tOjwu/MudFKz14ueuSeehTTcsb2smDi7V5WB//pHm9qm+P178r7Nv9tGGDps9QjQ9g53V223Oi9ha34znmPma2PkFtm5jnqMiQRVy76ta6705o357QMjPID4f0bw+Lnu4WPNo6+3jzzJOdMzk/8mCX/niv4Ml+wZO9c88OCp8dFjw/Ovfs+PzzY/jZCT0XIb69QfvxGPn+EHi4ce7G0h9Olt5MTYPbi386Sv9/FTY5IAsVKWeKFDM0xUyRMlSgnCs6XUxf3rdKJt6rCZdqgpBk5LP6ZKk+QJdOIZLhS3UJSj5Jl06+9qjpnzMnGSX1GZo0gGhjhfIAbkxCygBNOAjwhwD+0O+ZA/h9oKCHzu+l8XtowmGWZRmV+Gn8XpDfifLagOp2Gr+XXtVOq26lVbcXVXUW8doLeZ10QR9hnEdNc2xnDtOH2falqvbcOw07XEvqYvMR154VjZwohg81kyfSgbxy5EjYk/3Ym6J0IUnPtnRgQ+e7oRzZvtS9zahJlrjXSH2QaU0iah+zZh5XTXMtCcoQKnVnGaZQaW2WY4pwPTlSN800xwjJMKGbxaWDlClKqsfKHIsM48xFZ4Y0+YodKVw9zjQGIPkApZlCxP0MrQ+XDXHMc6RqvNgWpVQTxdYoqRpn6gOIuAfh12O8l6fUV0fU3zBX4fpI0/GJph38wvDG1bZ1TB/9c/Mqv3+X37cj6Nv7tG4Z1iUhwyLV9AB17CGuY7p5C3UcgtYd0nsddV9nND/Ga29zmx8bevt+uEH//gaUmcG+Pzr//LDw0dZbDzfOPtw693SrYHGG9min6OluwdPdgqe757/ZL/xmv/D5QdE3R0XPj2jProP5MPn9cdEPJ/TvDoseb5+9lf3TjaU/pCax3dS/XV/8tyqLqkAxA/z+3Rem8n9Wu0jIJmjSAFs9daV5iS6dZGl9b1uCgGTkv9fzkCYeY9pWUWOK5VhDapKkfQnVhF69K/k5Xs7BoQn6ivi9tF/ODO6jV//DXv28LkDuAxVBjneL6cxdbD5geTffqV8XDx1ddGRYNUmONU0Y5kttyStta9qxfenwjsp349Pm3Hu1K9W9q5rxA+ngpnx4+2PHLMMUI3SzHGuSNEUvuFZZtoUyzxJlXSxxpghjkG0MwWofZQgRqmmWaY7SzxY7UgxTrNSVZpkjxfYEqQswdX5EPkZpphD5CEs3hammuDVhQhe8aI2xdL5iU5RUTbC1Pkw6wFCNotIBhnqMkA8h/GaU34jyX145vDKHVNVCFR74tW0cXuF8T92OVTrgcgv6tfGNj2yJT5rWqvqvVffvVvftfVibRzVR1LKFOfdwz12i7jbe+JBqvM9ovE813MVrb+GuQ8y+B9n3Qfumprv1x5vg9zeQpRDx40nBNwcFj7beerB+5sHm2Seb5xdnaA93ik6Psafn2Wd7Bc/2C58dFH5zWPT0GHhh7jrth2P682tF91bP3lx+KzWF7KXOnKT+UP1fmSOU0x+75i/VZwjZRJkpeKkxXaafutS8SCkmIdGv3qb/c+YA4SCqmYdVs7A6gmjmisSjgOjVvy9I0A8K+kBBPyAYBIVDoGQMkPsghR9Xz9B4Pa/Ph6BX/6MZc4XVXaRpvtixwnblmNZFypqmrIkSx/zV7m2OPcNx50u869zajeK6Ta4rV+ZIldnn2TVBlj5Q2ZGXD+ZVg5uK0d3P2tbZrmWuO1vsyDCtKZZxjqUPUboQpZshDGG2KcSsiZXWJLi2FNexyLHFmZYEUx9i6EKUxk+qfYRqgtJMk7oZZk2MaZ7nWKIsc4RdM0vqgyzNJKGcYChGMeUgoZwgNcMM/RRD7+OYImx9iK0LMtQ+hN+E8huRV0nu78zBlV6owgP/HGxh7RXH8Jc1w8QVe7HA/Yaoe53Xv1fdv1fdv/dh3QqijdN1adJzm93ymHTdIr3X6bYD1HlIN20SrgPEvkc03qc8h8y2J4T3jqGv78dbwPc34XyM8eNJ4bODgoebbz5YP3Nv463HG+eSgaIHWwWndZPXzBU8Oyh8dlD45IieD5PfHhV+d1z043Xg20P6g43zt7J/WI0CB8t/Okm/ybNq/rE5SDoFyKbetcxfqktR8tGvGxYrmtIMpZ8uCwC/0XnznzJXKBxB9Gm2M4+Z0yxbniYeA0T9L8xJxhFVEFIEcEMMMcwzbGnEEGfYVmBdgjDGf2Huv5hJIhzATAmOM8dwrZU1bLM862Ut10qcy7LRGxcdi2xbAjeESX0YVflhVYBZEy/25riOuHD0WDV6oJnalwzufN6QInRBwjSPaUNsU4QyzHHtabY1WurJsqyJYs8KZYoyzBFCNU0Ywoh6iqkP4Fo/xxwjjdESR4pZEytxJChjmG0Kk6ppUuNH5aOkZhJXDDEMM5QuwLHMs3SBUvMsQzPD1vtIxTCp7EfEHZigHapqhqtbTsHB1fW/l+deNwdXeJAK56c13Z+Zu7GvDW8YJvb5fTtVfbsf1udh7SKsS0HaFGhaJepOYPs+q/UZ6r7JbnuOek9YHc8g5yGj/hZoXMVcR3RjRt7am49y1xJkfv7tjSg3H2IsBYglPyPtY69Ms3090MON84+3zj/ZLni6U/CiYvfzIvvsAIgNM7JhKhcuXp1j5cPMpQCZnCITY1R8EktO4pe0hkJlAPj5r2GRcpYmD9LlfkLhLzEGuboZtjZEk/pg6dTH5rkrdRmGcpRQTb3WzPW/ZU4yQldMIppAoXScLpkEBKOAcIAu6qcJBnF1mHLkUXUQq1lG5FOQLlYoHkU1sSLh6ZSIHjq/hybop/MHIH4nXNUJVnfTeP1AVefpHBJadSe9qguq7ASEfbjGT9qyFxoOWDVJpnuD0M0xHUuYLvxe3Zqwe/Pd5h1cM4Wpp0jlGKYYxQ0Rrj31Zd2CvH9F2L0kGdn5xDNf7M5T1oULdZuUbeFCwypminFti6TKxzDGEPkEyxTFdH62M02Z5ko8S0TN3AX3Km4IsS3zhHyYMIRh2QBD74PEg5iwi1RNlzrTbIP/oiPN0I1zaqK4YoTUTsPiPoZmEpf2s3R+WNQFCxpQftPplQPKa0X5TS/NwZV1SFU9XFkLV3rhSi9U6YXKPchVD1buLpG1Mvl1JM8LXXVhVx2f17Rf0rS+oZ/Yr+rb+6BhFdbOv3gbrE3B9u3izqeA4xqj/h5g3MLcN2j6FdJ7jFjWmS2PMOcht/0p5bxT0z/wwx3W87v4WpL89hb1+Bp4e+PcrfXCWxtv3t0oTPixB+tFjzbPPdo892T7/EtzLxLeAbQSLH14BD65XvDsGH16CN/bKbyeoy/4zh0s0Y+Wi0QOU6Ey+NIcTRGky/2I0s/SR0HpOCSZJGRTXP0sIPXB4sm3zVFSdfoM/X/Uw7pIPEyTjBSJhwHR0IvOc8IBumgIFI/RRKOAYZ4uG0XNaUwzSzpXYd0cy74KSocB3gCNP0Dn9UG8XqSqDxCMULoFROHj2FKAeAyo6gIqO4CqTnp1CyQYItRBhmUBNSwUuzcZllRx63W2Y6m09YjpWC5pOnrHMlc9fMTWTeHGKKr2ofIRVDFJyIZFbSlpe1zRm7/cuU3pwiWORUTjIw1ziGwIlY9gyrFid5ZhipZ5VzmmuXfdK7ghyLEnMOUEwzgLyobYhhiuHuHaFymdn+vJMgyhMmcKVQwjojZUOkCppyBxL1MXhqX97JowpfJdsCWZmukSR5RQj7ItYUjUBfLqkerXXy69yHNwZR1UUQtVvAAHV3qhKg9U4cIq3GXydvSqgyWo5wq8xcJa6KoTqXB8pu54Qz9x7aP6LKhP0A0vnqTD2kVIly405GiGHLPlKea+yWn/DvFcZ3U8g1yHjPpbgHEVcx0VmZLGPvff7gA/3odzoeIf7hQ+v1Z4f/2tu/kz9/Jn76+dSQTO31s/+3Dj7KPNc4+3zr2+yD7dPf9kn7Y8iz8+PP/tAfjtAfD8Gv3R1pm7+TOpafrNdNHtlT9KbMYCVfDVn2q5H1X62YYQIZ9+2zrHVPsAiQ+XTXL1s3TJFCD1Af9P+6bTheO4do6umiGti4AmxnKsYbp5yrGFqaO4JYPIp0DtfJFoBNPOF0gnceMCKPMDvAGkugfk9dFFQ0xTEpJOoJpZmmAIlEzRRROUdhZRBy+2XKccy6XtJyz3+sWmI0ZNknKt4dpZypklNH62a500xz9o3RT2b7/XtEuY5xHFGKYYI2VDgvYlSV/2I8csphjH5aOofBhR9COyYVQ2gMoGMNkQ17aAKacYNfOIfIxTE0G0kxxnmjTMlnqWCdNcmXMN1wfZlgQuGyaNEVjcjUr6MGEXImqFha0ovx2V9F2wLjE0Aa45jMlHKK0PEvZQmglU0sMy+BFJD8RvQHnNKK8VqW6Bq5rh6ga4uv7U3M/xwhxc5UaqnGWKVqzcWSxppqq96FU3xfdwRF7kqhP8rOaNy527oG4BNKZBfea1/xBpSJtGdBmq9g5k3sVc1yFLjnQdIbYdRsN9xHPEbXmIeU+s/a0/3Ub/cgdZCZP/fhP+7vj8g/U/3cn98c7qHx6svpnw0e+sFd3bOPdg8/yjrYLHW+efbJ9/ulP4dKfw6U7Boz36SpB4du3ctweF314Dnu0WPdw4e3f1j+lp6Hr2zO3sW1KHoUD5yhyoCLANIVA6/pFzntexUt6yDIinAMlYsSEIiSb/K3D/fE14FBAOgMJBUDTNsmVhY4KwLIKqIKqN0uV+SBMBlEHclIDUUaZ1CdXHOdYMpI2RxnlIPgkI+4uEg4B8GpRMYroQqJyhLMuQJkRYU4g2xLClKFua61xDTEncGEU1M5jKhyp93Ppdlmu1pPEa27laWrtFWReL7ZmL5jl+7zpX7+fUzBPGFCEfqWpdvNyxxbLGMfkIKhtGpUOodAiVDr8MTDwEy8YveLIcc+Kia4nSR9nmOKaZYhhmEfkEYQhgqgDTHCF0gWJXiqH3w6IOWNiBiDoQQTvMa0N57aSoGxB0MAwzmDrANcdIzXRxTYTSTLMNM7isFxJ0gvw2pOr0Jearo8Mraq9FmawZL3d+pO0Q1o1fcY0iVx1whatU2oJfdtC/ML/xtjMDGNKgMQMaXoEDtWnUeQB7buEN95kNdxhNDxmND6jGe1jdEek+gmy7iG2nyLJj6q7/j7vYv9/DshHyb7ewH28WPdk5ezf/p9v5P97Pv5WYpt9ZO/9g/dz99XMPN8+cLrKPt8892i14vFf0YL9wZYZ4unfum2uFz/fpT3cK76+fubf2p4wPOs69dSd3VuYyFvxd80M/1zgLSMbLzHOVHatfeOY/qAl8XZ8iFP+ltn8pz43QRYN0ySQkmT4dZYEak7hpgbCk8ZpFwpJGTYuEIQ4rQ4gyBMhnYHkAUgRxdQjRzVHGCG6cJ8wLuC6GGCKoyg8rZwDZJCIbg9VBhiPL8KyX1O0xa7cRtR9TTZHKcUo5wbAv46YkYYmTpjizZolpWeK410qcKx82LFWNHL1Xly21Ji/Xxz+wRxiGOdIwxzCGKXPidW2ngYsHEckooZkhZMO4dAAV9TFr4pQlyrIvcK1Jlj1OGRMMU5BSTVHKcUTWD4k6IFEHIuxA+O0wrw3htaO8VpDfztH7CdUophyH5UO4bBiXDTHUY4RmnGWYYRqmoaoGqKoF+ofmkMraC7IW9KrjfW1Xde3kFecIV+D+vKa3VFhHXHbQv7C88a57GTRmfmEO0qYZtTeopvuI8xrm3KVbtxHHNcC+RdbexDxHrKYnVO0Jp/VeTU/jqblchPzpFv7X2/Tn1wrvr715e/WP93Jvxqdo1/NFq3Nn1iLn7qwVPNw4+3Dj7P3N81uJs5vzBTe36Msz+NO9c8/2C57tFj3ZLniwfvb++psZH3yyeuZu7qzSay5UhV6eW0/zHCSbBKRTuHz8fctMeUuaqfCB4v+XM0kAwUiRaBg3JZn2FaRmiWlZgbUxTBehK4KwKggoApg2jGjCVM0ioY8zrUu4cYE0LyHqOUw7h8h9kCJAk04SygCs8JGGeVQ/yzQvkjWLxY1HLOcay50jazKYdQk1RJnWJKzyo8pphmu1rGGH4VkvrtthOda4zhXCtEDULGL64AVntKIzKxrY+qR+pcyzznEuFXs3ONaFYkcak438whwiGkTFg5h4EJP0nAapCxH6AKScZqh9qGqMpZslDL7imjgqfAEOFLbDgnaY1wbz2mBeO8RvhfntCK8H5nUwdX62ZY7SB1k1YUw9SalHUNkgrhhERa2UdATkNf2eOajCA1d4T83B5W6OuOGitL7KO35R1ghfdqJXvKf/+LO/+muYBnUp0LxBND9E3Mfc9qeY9waj83vYe53R+gixbmGu6zRDHnFd0/d25CPocoQx1EnlZsHlMJGeQeMT9Mj4ubkRWn89nBhn7y1Teyslc0P4nTX6ve1zwUHawQr3ep6bCb492lI2P43M+4riU+D8JBCboMfGwP5GYnYInB+hVZhqilR+4FVrpiAkmyqtmQVl04h08mrLMqWaAmQ+mtT3/9AcTTh4XjxRJPWTmigg95OGBUg9RzmyuD5K2bcxbQK3ZyGlD9HHIekUoomAwhFUG0Pl0wxzGtdGWPZlWBtj2JYAVYDUhemSMVQ/T+miXPc6WZMqbthnOVYvNB+xXfkLjQegag5V+XBdmFkTI3QhtmOZqZ/jelZZ1kRp4w7DliltPGDVzLNdOVITIK2LuDrAMMcRxRgqG3xlTjz0cwyg4n5M0o/+HJTGz3EuMQ2hEmeeMEfZjhSqHmXrpiFhNyTogAXtML/9NMm9CH4rzG9F+O0wvxUT96GKCZDfjPJbcdkw0xBiqiZLasKUerjMNAvyml+V4v6+JnIaWJWrRNEMVjiRcnule+QdeSN01Xl6mP293hFLoGYFrdlhNT8FLduY90aRcQ2rOwZNq8y6m4h9v7jjKeY8LGn9zjLS+ZdHnL/c+2AlVvy322U/3nn7L3eKn58w72+jt9fpN1bP3cq9eTf31p21c6tR5kby3M1lOBt+6+H2Hx9vvvVg98yDfeDBPvZgl/loF3+wg93fRu9uMLNz7JurrFsbiLzWVqSMvMxzdGUIkPsg6XipKVxsCBPqICDz/U9mLwHi8d9YWxVTTFce0ScZrg3QlGDbtyBFCDUuguIJ1JCEhMOYPokqfIQjj2vDDEceUYcJ+zqimiV1UUA0jmsjdMEorg3BslHKvIgb5i+2HlHu3dLWm5Rlubh2CzMkWM5VSD/HcOYQ7QyqnEAUk4hikjIlS1uuMywJTu0apQ0w7Eu4applT1PKAMe5ThnmShq3WObFsrotQhPApAOIZOg0Xpl7TdtpkPIR0hDDJEMsQxiWDbINEVTaBwm7QEEnzO9A+O3Ib5mD+K0wv4WU9ZVaI6i4A61uQiX9LO0EIu4mtVNFvA6Q1wFWNfy68Pt6QOUupMpRpmhi8z0l8ha43AVedUHlbuSq67fNAbpl1LkLWNaJxjuw46C48xnuuU51fod4Txitj1HbDu45oRnWUM+Rbajj/zxg//QIu3fA+s/75/79HvTXu7S/3i367pD2aP3cvdyb93J/vJv7tzurZxanivbSb91Zg5KTtAfrBU83Cx7uvPVk560n27Qn24VPt8882TrzaOtPDzYL12PwnbWie6uFSq+jUO0DXvUCe/GnGpJOQpKp0zv+/4k5SDr5OjhAMnp634qZFgqlM5Q+RZMPk/okrAwyHJuEJkI613HdPO5Yg5QBxJQCpZOwLgYLhxFtDJaMsywpTBVi21cgTZS0LQGqANO+RBgTTM8mpouxnXnSGC9p2GfYVt5uOWI7Vy82npDGBVQ5hSqnMNU0pvSx7cuUxs9xr7FM8dKGfa45XtJ4wDQvFrvWKF2QtKVxuY9U+lDpCCod+i/NIeI+VDLMdS4xtf5iZw4zhdmOZVQ5DAo6/xlzsKCdVPsQQTfKawIFrQivFRN2FVtjuLQPqqqDKxteVuN+w1x5LVzuhcpdaLmN4teC5W6wwgOVu3/DHKRfhjUZWJsC9cvM9r8idQ+I5keofZvhvkl5dnDnEWzaZrivw45DbssDzHuD0/qNZ3Lo/z6C//aIkU9g//kA/Nt9/N/vwn+9A/3tDvzDEe3x1tn7a3+6lT+zuwAujp+/u3rm7vqZXJi1tXD+ydZb9/fOPt49/3j3/OO9M093Cp9sFzzeOv9o61xmBrm1fv7B+lllbU2BYhaQ/8LcDF0WAGT+0/gfzphj6f2AaIQuGDudAEYYF2BtlO3MwYZFrnMDNSUJ+yqsjWLGRZrMD+sigHQS0c6BygBlTaP6KMu+guqTlG0J1IRI/TxNNo2qQ6B4CNXM4LpgccM+15ktazlhunbLmo8J13qxdwM3pihXFjfNMaxLlDFZ5l5jWpKIchJSB8oadinLEtu1TurjDEsKV8+wTAlSGyq1JSlDrMy7yjZHCckgKh2CX6f2cyDiPkTch8sGEXEvLOqBRd2QeIChDyLSYVIbRGXDTL2fUI4j/C5Q0AkJOl8cHV5nx2+F+a0wvw3htSH89hLLHEM9hlY3ILwmpKoFrmolpUNwVStcWQ9XNMKVdXBlLVz54soBqvDAlW5S0ABXuOAKL1zhPUX2Wrigchdy1Un/yvy6uSVImwEMWdCYhez7iG2f9NwgPUfs+gfs5tsX+x7jdbfJhvuo6whz7NPMu7BtS9c3+p/3S356TOXi1P95CP30AP3bPfCn+9Df7gB/uwX+9Qb0bJ92M382OXLu7gZwf/3NBxtvPdh8a8n/9n4O+mbn/NP9wtN4tnd6RXb+6fbZpRDj1vrZe+vnqixWumwGkP8vzjUExUMsXRASjwOiYUA0SpqXEe08posVKaOoehZVBHB1GNVFCUsGr1mkLDlCn2GYMqg2hunm6PIAovRBsjFC6UM1s4QhiZrmmeZFzBQn3OtMzwbLvckwpwlrljQl2ZYUw5pheFeLXZvvt+5fqD9g1+0znOsMZ4aoicLKSVQxiSr9iGKabc8wbZlSb47tznJrN0hHjmVLU4YYUxdC1ZOnezhYPPhrc7CoFxb1UsoRQj4Ai7phUTcs6gEF3ZCwh21fpExRypLEtSGE3wULOl6a+7t4Za4V5rWgilFIMoBUNiJVTXBV84uoaHoRlS8qcy/NMSUtBL8BrnD/vLz+M+YMaUCXIetus1qfog33Wa1PkNq7uOcEtF0D7QeYfbW0cZ+w71D1twjPMbv5PtPzkNe7eX2X9R/P0Hyc838fwf/xCPnpPvTTfeg/7gE/3aH/dBv68TaYmcSeXz//zQH9m/2C59cKvt2nPzoGF3xlzw8KvjmgPbtW9HSv4NlewbO9glNzmRnq7ub5nWUGV9xFU/oAReh/0dxpz0O9HxAPAOKh0yFSuCFBWbNYTYa0rsCmDG5aoKsjdFW4SBlCtLOAyo8bo5g+yrAskqY4aU2hhgRiTIDKWUAdpEsDlCFKWpa5rjzTuVbaeFBSu1XaeMz07LDr9hn21EX3KseR4pgyuHGOY06TlsVidx5RTmHyCUwxhsnHmZY4ZYlhhihhiBHaELNmnmmKFtsymGQUlQ38pjlENAgL+0/NwaJepnqEVAwi4p6X5lDFBEM9TmmmGDVhSNCF8tphXscvwb0y14rwmqDqBoZukmsMIZUtSFXrKTiwovHX5uBKL1juZohbcF4jdNULl9dC5b8G9zvmQP0SoEsD5h3YfRswb1HeE9Sxw2h8jLlvctq+RWtvl3Q95TZfJ+sPgZoN1H2NbsqU1B11DUs3l4oHepjrcfbzBgvBAAAgAElEQVTuIrGTpLYS5HaS2E7g23FifZ7r74XX5xkbMXhtDs2H0bUImo2h/m40E+Tmw3huFsuG0GwIyQaRlRko5Ud6GtD0DGiv+xSWzaL6GVgz979nDjhtYy0e5OgmMfEgqosQ5gygmEX0C0UyH6KfBxR+wryAa+dZtjyhn2fY84gmipszkNIH6aJF0glINwfKRlDjHKKKUrYUaUleaLlBuXe4jQeEJUW6VhFDnGPNkoY425UrcS590HqN7Vp+u2mfYV8q9e5TphTDtgrLp1B9FFb6MMUEopjEFKMsc+xi3RplWyr25ilDmGOOI9IRVDaISocRyRDyMzXkRT//AVjU99IcJOxmqkdwWT8s6oGE3bComzCEuc40ppvl1szCgk6U1wHzfrmNey1aUH4DzGuEBZ2IsBsV9rA0E0h16wtzlQ1wZT1ceXq7WgtX1kLlHpa8m6UaJETdxZpxqKIJuup53RlU7oLLXfBVF3LFiV5x/MLcMqRNE7atko7naN0NVvsPsOuIbLgFmNcx73VQnyW8h6h982LPA8x+vbT9W9xzwuj45svGVCjE/9vDT356+OFfH7337w/f/evDd/768N2/3H/7L/ff/uHuOz/ce+f7u2Xf33nn3sF7q4mS726V/XhS9v3xO98dv/PspOTpde7jI87jw9LHhyWPDksfXeM8vkaMDwnZ0jamOorr/5v9Sv7VWgkgHr5oT9NUs7g1i2hjHNcabkhwnDuEOopbl0B5ADKlQdE4pktikjGiZhFVByjHCq6LsBybpDJMmlfocj9hySC6eY5rAzPMczxrpCVf1nKL7dwsbb3JsGXfa7vOsqVY7lVIG6Gcy6A2xHAtETUJTsM1ti1V1rSDamdQxRiqmCBko6h6hmVNwYoAIpvCpCOweBiRDCCSAVg8+OujAyJ+BQ4W9Z4urEz1CCHrhwSdkKATFfWydNOYqBvlt6OCLoTf+Y/NIfwmqKoFrmqBq5owSR/b4EN5L8398qaLIWplyvuKTX5E3EWphoCKOvjvcxtU7oKvutArLviSA71s/w1zgD6Lu28AhhWG9wZi3uC0fEs4bxW3/4h5bzE7v6V7jpkt1ynvKuY5AnTLuGsfNm5WDBx2Tik30pzHh4xvTzjPr7O/OWY9O2I+O2I+PWB8f5N6dog/2uUerxYvh5j396g7e+SdPfzBEXh3E727hd3ZQG+v4bfWsOtZaDGImZ1fMdUdqC5Jaedebub+t82BCh/XtV7iXiasSzR5CDEsFMrGYWMclI+R9mVUPUM5coQuSTh2YXUEs+YheQA0pAqEY6AuAogHEVMctSy803rIcW6UtN0nHJuclmPMnGB4VwFdmGVbLjbPv1+XJ02JsqZdtmPpQtMRw5IrrT/AjUnSvQVrwqR9BdHMsqyLqDqES0cwTfBC/TpDP4NKhiDJMCIZQSQDyO+USH5hDhH3wKJuSNjFVgxT4j6U1wnIhrnuFKKagPidMK8D/v08B1Y3A9VNdF4zrboNqO6Aq5qgqmaouvnVfu7vzbGkbXh1A1DZSKnHIEEH1zCBC9t+bQ664oQvOeBLDuSSnfaF6XVzK5A2XWRY4nR8i9TfZrZ/hziPGPW3QPMq5j6mm1ZRzwFqyTIanjI8h590PaHc10taniGuI0bDDYY5f6lp+ata39dNoc/qg1da41/V+T+x9nxi7VcMJ75wD3/hHHvf3POuqfMDY9tHuo4Pda2XPF3vaFve1rS8o24oU9YWqxrKVM1szSii8DM08wzdbKEiBMqDhcowoPDRFMGXtZLf6Jsu8dGlAUA6DUgnTzuCQZJpWDwFSaYAiR94MT94/LfMnbIbpomHQeMCqPSV1W8g2jjHvQrr05RzEzXECMsKpAhAunlQMo1oo3TFFGpcwFVRtiWHaWNM2yqmiXBcO5glx3KvY4Y405FDjEmWZ42w5S40HbHtax90nhTXbjEarxOmBaYzD+riTFsc0kUpRwY3JYvrdliW5bL6fbYlU+bZJPUJTDoGySYR/SzyoknjKCweRiRDiGQQkwygkn5EMvALc4i4DxH1IsJeRNgLCzshYRck7IFFnWz5CCHpA4XtkKQbFrbBglaY1wbzOkF+F8prR3ntML/9ZYkE4LcB1S1gdRON11JsmmGopuCqJqSqEausg6uakKp6qLLppTmk0sOStKP8RuiqB6xoKDaMI8JOhmIArW6Cy91guQt8nd3vmYN0K6AuXWRcBOz7QE0WdF1HnNexhkdY3U1GyzNm7W1m8xPcfQOvPQHsm5Rzg1m3iTjWIccO7j3C3DeI5rtk7U1m423ctcdwH6KWdUAdp6uioD5WpIkSrj3MuUdXh2HV7Mv5rbAqCqligCpWpArRlQFU5geUQVwXpnSzgDwAyvyQzAfJJ+kKP13h+3XPw1fzIcSTsGQCkkyDEh8gmQZeNC7xAZJpSDKBSEZgyQQknvy1OZpoiiYep4lHaKIJ0r6BGTMMY6zUnCS0QUg2A2tjsCaMm5K4YZ5hSePGBYZ1GTEmMFMMVAcQ7QxNMQUpw3BNlOVZYznzJY1HbM9OaeMR07PJqttiWjJsa7rYtlDizDCMMYZ9iWlZLvNscJx5TsNusTNf6s4RlgXKHkN0UWbNHGoIU+Z5limMyEZg2TAsG8Uko6ikH5T1wJI+RNIHS/oh6TAsHUClvT+bG4RFvYi4D5X0Y8JeTNiLCnpQYQcm6EIFPbCwm6zuLpF1EdJOSNgOCdsBYTdd0AXx2xFeI8BrA3jtMK/p52zXgvCbwOpGsLoJrGokZH2wqBcR9iCiPrSyAa5qKuK3whWvbrrY4mac3wZWNSL8dljQicn6cEkXVOmFy51IuRu64kauuuHX1tbfy3PLoC4D6Bfp+hSsjVMNt5mN9zDnAeHYB61bqHMXsW9h7gPSdcyofciovcNuefh26w2y7gHkvEbaNtm2RaZtlaZJQep5SBUBVAlQHQfVEVw+XWoMf9i8z/Gs0XTxInXixUwIdQRQBpnKsVLzHNcwg6km6YooqQuThgigDEKqmWJz5IwmWaAIQupgsSVaqJz9/TnVM3TJOCYdIKUjuGyYkA+TikFEPkKT+QHp2HlpEFaOsXVhQDxOE77WCEw0Cov6IVEPLO5CJJ2YcpzUzGKGOGFZK63PM+wrWM0qy5TkmPwfeea5+hCqmqYrZmH1PKKdw/ULpGGRMi8VO65xGw5Jzz7pyiH2VY4pVmad/8Sb5NhTbPc6t3GnrG2f6dlk1e0xXVm2bfFde+R9V5xrijItCwxTgm3Ns+zLpc7tEvvKRUeqzL3BtC8D8jFQMQ6px1HdLEMdL9WEPrTNfVWf4iqHSXkfKB1BRb2IaBAWDsDCgZdL6qk5TNiDCppL5J3vKbs50k5U2I8Ih1jyQUrSg/Lbi2UdlLwP5HcQVU0cSRdTOgBVd56aA3ktSLWb5DlQfi3F9zLEzZSsk6nzcQxBqKoVqqwtUbVi/MafyyKtJK+RUg8Xa0eZ6mGWdpQhH2TIesByD1zuhKubUF4rfMUFXXYAVxzAFQd42fF75pZAXQbSpkBdEtItgJZtqu427D7iND/Gvde57d9S7huMpqeIdYdwnkDGPOQ6RMxLJa2HiD2n6pjbPzkeX70Pq6OgKgap5kFVFFYEpE1Dwdjc4uJsbiN1ePfe1Oo9RB2FVBFIOUfKRt39k7FEeHk5vr2d37v1UDO2x9BHQMUMoAyZOqZThw9g3Rxd4XN0T6cOHsDaudNhc7+xnxP5yiSN/mgosZTO5HLrO9vH9+7VhTboUh8oHYNEU/YBX3L3zstv1YBoFBCNFlX1do4MLawkF5bnF5Zj+a2Vmw8ftSTvw8YUIA+UOpaY4s6GnvFIPLiSXVxfX926+6S8dZlhySNqP66PwPIp3JJCjCnStUZYcqyGw/dt0x1TodhiMLeePrx9pzf/gONIE/Ycpg+zLMufGLr7pmYSizPrWyvHd+41rHxDOZY4jiXEFKIsqXd1PfGV2MGT799r3SdNCyWeVYYtza5b+9w1MBbxxRZn07nk3tFG9sbDP7vCsPBncH9nrgcT9r+raOyfHIvFw8uZ6M619dVbDz80TiD8bpZ8gOS3zC6t1QeyML8Xulqb2Fhr9C8A/HaE14bw2qDLVm97+0JiLp2JLq3E9g62tm7cRwVdQHU7Lu762Nh3//vvOMouuKKWJW4jeI1weS0o7sDFnSC/BRf3YPx2prIfqKiFy52kvIutG6GXu8Er9hfmrjigyw7osgO+/Ctzr97P6RYx+yar/TFg28I8x4BhFfXcoNVs4nV3EMcBu+0p5r7OaX+Cu/bL2u9y3GvcCsP4eM/EYh7QxEDVPKBagNSh8+V9zs4Wucv+sdTyqUjfPTJ0cLzyVUMCUEchZYgrqhue6lBanF+KrBKzbXHRH9s8BhUhQDELKoIKR20wnaEpAjS5T+32zqSykDLw+2eIyffEjqHeBpHFUWUwVuj0PIvzPd0AXRoEpKM08ajSbg9lsq/KcqIRQDh8vqLrYoW8Qm8VGO08o0tisczMTYwt30G866g+zXZtKDoGWhvdPKP5Q7GWZzBt7+fGM4egIQ0ppnHbOsu2XNp+l23Pl7TeZVlWOM6so6GhqbXuc4XtC3Xt7Lz/1tN7H7hSF5pPGK50mT3X3Nvhrndelmq+luuC4amDe7fZxhDHvYXb4qX1e8gVe+tIc3Z99Z3aPKXyk5ZFVDlF1USrzLXO5tovlcYP+TKd055digzENpCX4F4zhwt7UEE/3+xu6GwTaa2fC2Suttp0JuQeTwG8Tri6g6xyjU/0NU2nQF5HwWfWobGODn8a+bmrUuHHyrHxTqu7TqjRV6n1PI2xXOdEKpvBqmZU0vW5xrt7sI5V1LJE7QSvCal4VQrGJZ3Fumlc0svSDNMq6uByJ1RRi4p76FdroSsv8tzLgP6xOZoxQzTfgi2bgDYFm5a5nd+g7mNW21PcvM5wH4C6ZdJ9TNevEt4bpHWJoZ621NpH5pd+NpeAldEiuf/8l+2gZBZUz0Oy2XMf1vhDg00zW4A6RlOFzlV0FH7dAMjHaNoQJuw11FrjSxlIETw1J7W6p+NJmtxPV/gVDvfEfBpU/HIOzmv7uekPJebVrWVU0AaIRwDJCCQZhSRTNKkfkEwUiUYUVltgcel1c6dBFw4Dgn6I3wsIBmlXvfZm78RCHjHGEKEPNcRlHsdMMoWpJtj2NaS6z1LrmUmvMR05WDnDdGyQ+iTTuYXpk6R7nzRmcMPsZHDSMrrCsuY+bD4pFZlXVsPVjfOUY4OmDbKk/XPRMV7HPFWzXFy3U8rXrm3H/+ycLbWv4zo/x5zB5OOfqmz5zaULzmXSGGc2XsMtC+z6HaC6k2PwYYoxTNJXdMlpc1sDC2lU2Ptb5voQYV/hJxroSyNR1QbxO8+8x+vqqxuYy0C8DoTXSfvCMDrWWTceA3mtBZ+bu/sbmycTyOlD8+qWwg9l/vCQtHYAKa+FKhugynqoqhGoaoIqm6DKpg9Eqs3dlQ9qppjSTqiyDqzwvjQHVnjgyibgqgcod4PlbqjchVY3F+umkOoW4NK/YC4N6RYBQ57V/Bi27tD1KwXGZdC8SdMtE94TyH7Ibn2CuY9KWh4SrmNmyxO6bYtURhztDYPzq4A6AqpidHUUViSLNHOgKlaojdBVMVgVPlPe0d3f0R5cpqvmMesmYlimK+dgZRQ3zmOGSJWhNppKAPIAoAiBihmZxeVLLNDkwUJFQOlw+OKLgNJXqAjQ5f6X//hfmiuUBN+XmDf284hoiCadpMsm6BIfJJ4GpROAZLJINKKy2wMLmSLxCF08UiQep4nGIeEIKBqli8cA0QgkGgLEQ/SqHltD/VAiRzhWCU2KsuZq6ryxjaNiexKrSSPCIa3b0h+IQPokZkqwmq/jrs3S1hss53pJy03SlicVg9lcWNiRZ5nmi+s24MrW+OKsvHOxuPka4chcUPVubM5fbsyixhDHuoBXu1eXM5e9IbZjAzElS2qvsSypr2q6s7uZMtcyoQkxzQlcMcaomSeUPqYlT2hDuGQQFvcr7K6JWAQS9/3aHCroRQQ9qKAHFnbB/B6Q3w1fanZ6zb2zCxC/A+a1F31h6BtoaZ9ZJCRdhZ+bu3vrWidjEK/ttPZ2/kNpJDktqR+CX5RIGsDKBnpVI1TZAFc2fSxU7+wtc9SdQHU9UOkpqqylV7jBcg9Y7kHKnchVD3LVhZQ74HInVO4CrrrolfW0q86XaytwxQFcdoCXHfBl2+/nOW0K1qVh3SKgTbObHlF1J4B2ka5dZDTeQp37lOMANq+hjl3Qsok7dxHnHiAL2xq9wwuroDoMnY68UcUAdYSuSkCqMKnysxWBD0wjIyONoo7ZItU8p+EQdm6c00Qu6P2fuyOfmWYsXltfaAZQBABlEFQGVN6OhbVNlmoKlw2r3HWxpSxHM1eoiIGyiddS3UtzM2/LnAc3dqUdSxe1E6XaKabSB8kmi6TTgHScLhzWedw37t61DKUdgwv6vtQn9hAoGns5/oYuHqFJhoCqXluDczy2iunmEPkspp6V2WoP9/O1oa2vO+JXnOOeRoe7fYxbt8dyrHA9a7hliWFbRmqWmPYcZsuTkqlwYjqy95d3G/Lchj2uZiQUmxa3z+H2DGKIM6qbNrYXvmpZRmxZTt0mR96xvBy52postmWYhijTFEc0s59pXatbGa4tTpgWi12rDNtCiXONqolxaqIszSRH3vGOptVT5+yengXFPS/NQcKe00AFLwLit5dKmj/Q932qru3qbrT2zYD8Dpjfev5L49Dk0EAke0HaRV2uaR9oG4msoIKW0/8NZz+SRlORidTm26qOC7JGtrgRrWqjV7VAlfVIZe0lhXl3eymwvOscmrX2zVTVT2H8RvBqA1j+67t8N3jV9Yv0Bl52QF/boa/t8NcW+pe/bw7SpiBtGtCsoLZrSM0WoF0EtIuQ8wCrv0XU3WA2PKAabjMbb5G1x4T3CJTP1Xgdw8lVhiUBq6KQMkXXBiFl5DNTz2AonFnPxNPRoYHG5q5eTDkFqmcBzRyginMNsdnlZC4/N+Uf7Gy1d81ugqcDSZQBU2P74UF2MZdfTCW7etoiwam1u09wtR+QztBkwV+YA8WT74js2czk2tZSZiWR3cge37/bFr9Gk/uLZH66cFjrdkXn/JG58Slfdy4X/O6nv33snAX+3hy9qsdS7xiJb6PmNMOcY1gy5y97J30j+Ww6kUn297eNTfguGidR2zrDvcmo3WU1HLBrd7j11zjebYZ7i9D43N0dz//yILV5OB3Pjk2Mj44NqoayxZ5drneDLepa20l/Xb+Im5O4KUwJazNLwSstMdKcYdmzDHuuxJX/QleXXVsqsUSZ+gilDWM6H6ZOvqMdahocjqYWllfz/pnJrg7vFUs3JOyHBD/H35tD+N3F1eaFTCKTSwyPtwenhr8y9wD8bpjfUfS1bWJq4ODaxurG2nJmob3dvn9wODC/eZrnCj6UTU/1HlzLreXTSyuR1a3sxvVHTEkvWNnKFTZ/KTWMDbcl5qYnp7uCkfFbDw47gkvw1Tqw3AuVu35lzvk/MZcCtOki3QKoWTw1h7kOGLW3IXMete/RTeuYcw+2rDPqb4CKqMnrHJrPoboYy7IIKONF6gRePdHUXe+s8wj11kq140ulB5KMAqoIrIrCqjlSP0/pw+xyy3sCw8dC48hoe2R1G1TOAMogqJxhluvEVhvP6Kg0GAYGm7yNXoF7gC6bA6S/2X/O94fPPB8JdO+JDB+LDZ8LFD7/0OrBESwZAqRTgHBYYbMMjU8CH+rOvqdkX5Jkluf6w1lAPE4Xj74yV9ljqbdPJjYBZRBTJ2iK6ff0vc2NHm+DR6i29Ex23bxz0Jz7luFcL67fIaxLpH0FNi6Q9hxmSnNc6xc82TJJT113d0dfbWOnq3ugfto3yO9JwvokYV5gVjVsbCY+rYuz67dI20qpsmcpHeV1rTCMcbY5iapncWPsE6U5u77INIU5jixVkyx1ZbnGmK61p77WrLDryrXaKwp1WbUWEvZg/N5fm4N53QivG+V1n/vU8P4V/gd82VW1OpEeb5hKQLwemNdZdMn1iVAhMdVI9HqJ3jg+3tra0fm1qfM0z9G/tLM/q/hQKPszX/kRX+hocu0erv9Z345LughB04cC7fzsMOcraUHpVeidz9u63KmtFfSqBSyv/a085/wFOPDSv2Lu13NwaLo4bl0rbn1GuI85bY9h+zWq4VaRKmyqtYzGVgF1AtXEGZYEbNl4xxKYHu/9s3kUkgcAZQhQhumaOUAdg5URwhChTElIHUQVM4gsCImnqvTWeDYFyP2gKgQog6B0nC4eAcSjgHBYYa/xRaKQ9PXTwy/qcz5AMg2JxkDROCQaA0VjX0vNqzvrkLAPkEyAglGZxTg2nwJFk5BoorCqTeUwhVN5UDz2Wp4bpFf2Wepto7F1WO0rdm7jxnltc+fgYNcF9TAi6Sv4wuwL9SW2r8O6CKaeQdQhRB0irEslrcekPfdu6xHbkWXYcjReHyAdo1f3I+XuxKJfO7zBcKyUNR1ypM3rWwufOGYp+wqqCrDFLStL8+UtC5Qhwqnd4FiSJfVbX6gt2fU8R+tnm1OIepRtSgJVbfWdHnv7EM5rQISdoLgfEvfCkj5I9FvmBG2IoA3ht0OCXoTfjlZ1n//M2tRkH42kMV4rymuDq1qhijq43AtXeM//WdHVV98dTuCin/8OVjbBFfVQpQeudEHlteC7VcvLYVn9KM5vQcrrPxBqNnay8GUbVO6Grro/qaxa317Fr9hez3PAFefP8fLE8KIsB//PzQH6JcR1i6bP455j0LRO1d2FdAtGr30otlaoTgCqKK6LcGwr72h7l3KLTM1EgSpGU0VA1RxdPUdXzxH6OGWch1RBuipZqEmA6gBNESq3NEUWYoDc92LGnCJIV4boihAom1E43b54Bnq1pP7q7ks8WSAPFCiDdNk0KB2hS/1X1LWpbAYS9AKScbpwVGK1Lq5uIeKxQtkAIBhWWOzj8TgiGv21uaFIni4bJtTR84I+e71tan6ZsKyh1tzF+hOtu342t1vmzWHqwKk5SB0h9HGOMcQyhgh9vLjpmLRlS5qOSEuOVE3ML/jlfTm6NkZZV6lK187WwtW6CKfhgGVNvaseWsxGLnfkGLow257DlT7CkvpMVbOyluNoJkoca2hNtLR2HahoHBzv9AzFEckwJO0HpX2wpA8S94LCnt9YW3k9dH4/IuiABD2goBcQdNErGlx1jt7gwqk5pLoV5rdDgg5I0HH+C0NXb2PLdJQl7yGEHVBlE1TZAlW0wBUtUEUzWNUMfypaWklUOrrplfVQee0H1dK9/WyZtBm+6gIq6j4SqTPZzL9qDr7kgC9Z/zvm6Lo4oF8qbvsGdx1z2h7jzmuljde51oSt0ZVc33+vPltav0M03CDtO58YunZ2ljSDubKa2WJjkGMMc60xhiVBGeOQMvjZ/0/cXX41lqf7Ap9/6N5zTxfZHgfKWqZ72sugcJIQ3TsJcYFAcHd3CIEAEdzdCocyqtAQwaXm3X0RiqZ1pmfm3LvWd2XtvP+s5/np3kZTef/KXzV2mrDlobwhPjmh2GyFRE3Xn/wSNIHCRkjQBAoauXG65p5+WPC75u7wTN8oi6qGVr5J6KYIq+4TxZrE5MqGOoRTAvKq/GIqQ6WqlbUFVeUgXVjzEM9My4iPL7bc4Zb93FyhJklTYR0hxRTC7EqYU6JJTXm1+Vpl2bof1/E0pTsh1VjU0gWLLfe1dkxsRolmVNzCkLUGxrZQRE1kcWtA3CiGtzDU/VTCSuPk23uaJBWTNG3//ZQVanRm34C1f2X/UfbYp7JaaWJOo6nuvt5OlpruJczcU7T8LW2Qa0gbnRrjlY3/kDaLEvWo1PR/nuhqGoqs0y8fxpY9EJc+lJR/rqz+VFkL/VadQ6OK4KgcdVGTrr47kJtDZef8QGQW5iZoCuqQyCw0MhOL8h3GzESiMv77kSyvID29th2OSGfyCqisHPJzVbFjlJ3eSItO9WcZObrUtrb6vwoNpIgUMDyZ8ihyZLi9dXT5bkyKf6RBZtBXN1STgzW3e+sfmIOD9NBTLfRMBz9TA0//vDlIYiNJelD1LCLto2hnyYI6W5+tf6AtLVVlNpVPzIxOvNwNTN+ANFMP+cax0e7VlfmRkc7ePsvQsM3t3ZJXj4MiC5nXzDXkLK8MTkwN2DttTfXF6Rlx7IRa33gOvHWtFRY2cnVxFa0W8I/W5xq/Fuhn5vvm5ke7+qzN5srMrPhobS7ErfXjV5G4taQf8J6Btsmp3u6etpragqy8tHuCfD9eza2zTBVQRI4+2VDa1guyqkB2CcCuvBfEnZnvGxrp6O2z1NcWZ2QZg+MqYHEbKmm8q2unEk3+sa0BcgtZ1EgR1aOE6X7KHFM/ej91jq4fpPGKLfYaVm43RdpGU3aD7DJZQvz0ZPfwkKOjvTEvNzU2Nd0/vpeusDJl7fHFlUNDloICY11t3vrL+b3D40eJNphX/MmT+OqajMW5saEBe0+Xpb+vY35u1DI8Q44puqH2U1h5fsEJpQ0Vi8uTQ6M97dbm4rLs7NyUBxw9FH19hASOzIQiMqCIjE9+lBaWpCdWNEER6VBkuj+vAPlBMDDePzk5MDzW7+g05xWmpBcXQmEqODQdDkv6r294xryU2ZmBoSF7m7k2KzspXB4Ph8Qjz5OR3zEHBOtuAoYlQCEGalQaEpHB5GT+aXOgxAFKrIC4HcU7KMlbqMjyNU/3vUj3rUDzPU/9I5HyVFdD1U3TE1chTvX34tzouMIficRveYrH4vhIdbo/UQvhbSi/886TxIcRklCJOkosCxHpPuMmQryam+8aAj9d9zIBj8WfE9ng7+9DfMKr/T9BafQfhKEiCUeuCRJr70apYFYBwKsH+ZVgTN2dyML/9TX3e7YoVKJ+IlRQIwwwpwK4NZ6D2LUAq5D6jPdQkF4fo0YAACAASURBVHGHXQVxSiB2jR+rGHwkeMbDQ3HlM1HcvWgDGa+FxBaMMJPFzV/E2wLlzZiwgSxqxER1KFFLUTkQZQ9V7qApu2F26SNJ0ud6u3/C2F3DmH/CKImV/Wmo5KlIGyxWfyGIowvqYEUnVdEZIHf4R2iDxPrHYs0jQvtEpAtTJNHxcoxb6heZHhgU+V0M/i0b/5aNf8sSfc8hvhIYUdbPzEGsfIiVj7AzoeiMO19EfhcRHSWVRcfKHvHFaIgajcxAozJ/YY4UnHD/GSeAkwBFpoERaUh4Gi06Ffo28tuo6BiVLEQs+ixCgD6RQmEGKCQFCjWCIcb/9QXnXlBYhFAQJpDeD4mBnmuA0CT0eSLy0/pIHPg8Dgj+mTYgWEcK0jKF+QxBAZ1XRIkpJEck/3lzUjtCOFCRA5A4QHEXILHCRAeAWwCRBcA7qHFz/qmbZN0kNX4KVo5Q1OMUef9nyRNUqYUmt2NCsx/eA+MWSGQjiZtIeDPIb0L4JlDYRBKaIWHzr819IjLBgmo/kemPzjLxGkj8WphXjbArIFYZEFMDcBthbh3EqybxayFuA8Bp9OPWAuwKEqsC5FQB7CqIXU+6ddcQ5pSTuCV+rDI/TuV/x1RBnBKEMJPFjRShCRW1IKJmKm6h4a2Y2ISImzGxiaE0B8haH6hsmLABEzZgeBOKm+7q+u6lLAboR+4lzdPUff6afoqsk66ykSVWqtqBxdru6gcYqt4HhjGmdsA/cdpf0UHVdSMyCz22FROaaTILRpjpih6UWwryqunsUoBXQI4uR1mFMLsYZheh7GKUXQDF5EGcn8yB0XnX5qLzIXYOHJ2DRWXC4alweCoUnYlGpSLRmXDUL+scEJlNjki+E50FRCT5NhugsFR/XtZDXiY1Mg0OM0JhiVBoGhSSAoUmwSHJSIiRFBaPhiSBQfFoUCIUYkRCDFhI/G1wpCAdKUj3a3NAsA4ON1KiUsgRKTROLiUy5d/8ZrDt9rfRQdwGEXZY3AmLHIC4DdGOBqZv0vSTX+S//MzQB4laYbwVxm++jd4K460QYYGINlBoBkSmO7iZJGyD+M0QYUVEbVTl4McvzZkAYdMfmAM+nsr8t+9UV/ixK4GYKgrehBHm60EbYfYFw01kUSNZ1MiUtQbILRS8CRPW39e0Y6I6TNSA4M2IrI2mG0DFFqZugBzbwUyYpKp776XMMtR995NnKYruu/GDFEkrQ92N4CaaqhMjmgPUPbCi855hmqbsuJ8ww4i1MuKHIG4VJaYU45ah3FLEd36EU4RwisisYphTBHMKYXY+yi5A2QVIdD7CyvUFjsqBonNgVi4clQNH5cDROTArC4rKgKIyoagsKDIbiswCI7PByCwwMhOMzAQj04HIDCA8CQg3AmHJcHhKQEzmQ1EBjZUBhqeQQo2k0CQwJAV8ngKGJIOhiXBIAvI8EQ5NRJ8nICFGKNTgy01LBYL1H3NNDQrSIpFpgeIyGifzLlGNsdOYvGLoeeJ/5DvVt8xdxw4SHYC0C1ONkxW99LQVstzKUDpg3AKJWm/Oz8F4B4JbMKEZxC1kzTAo7aIZlxHVGDN1jaaduJu8QZI47uANfqIGQGj6Z7739R+4xx9TgxHNZLGZLG7BxC034BC8GcNNFLyJGdvKjG314cOE9WS8/r6mnSyqx0QNZJnjs5QVum7wfuo8RdkTaBhGJa10bT8sbGKoe1DC7G8YoSodD1KmGJreu8YpTN55T9ePiE1MRS8sqKQqHBRBDUPbhQnrKB/PoMOcEphTAsWUouzir2NLFKXmrCZbYXNHgdla0GzNN3Vcp7kjr6k9r6k939SR19SeZ/I92/KarPkmW15TR06D5Sa5jW25TZbcptbshpbselNOvSmnvqWgqb20uSO3trmkuSOnvjmnvjm7zhdzTl1zTn2DsaKRY8hnhBngUCMSYvhnzIFBWiTUQI/JAkKNZFaRX1gCnVOAhBj/R8yBuBXCrRBupcZ2Bqj76JlvUFUfJmlnKO0fwVkgsT0gaYWiGIIFbbDE6p+6gSlGmYYZJLabrB0nEXaKdgqMtdENU4i8B/wHde4/Yw7kVqLiFkRiQSQW9Ba4G3PM2OsKd9scGa9/oO4gC2thooGq7ASETXRtDya1MBMmKKrue6lzTHX/g+Q5mqL3nmEUE7fSlN2QqJGmciBEM0MzSJFZHiZMM1XWAOMEXWYJiOsni+pvzEHsYohdTOXkp9RYVjcGtjYHd98PundHPa4Jj2vS45p0H0wdumfczkmXc9LlnPS4pq8fDqYP3QuHrkWva8G1P+XcG/95xpx7I/u7I679MffumGtn4nB/7tA5792fde1MefennNujB7vjezvDzt0R5+7I7vbQ/vbI/tbo5Fz/97Ep8D9hDnyuZ/DzydGZgeIKhFtwF6/G2OkBglLoecL/iDkAt4O4lSa1MeQOELdRdNOIcpyWuOSf+CJQYYNFFkRgRkWttPg5WN4HxtpAiQVVjWOKPlr8C7J2JDBphRY3dde4imknaHHTgKwfFJlBUQdZ2oVIeiDfKyMEjcC/9+6I2/Hj1JB4dZiP1y1qKGHGCBMZb8JEjUyZxTdLvYlvMIcK6jFh/T2VBRXWIoJaRNj+0LjAVHcF6AcwaTtN6YDFZprKisramPpBTGm/lzRG1wx8bpigy/sDtT1UcTNN1oHgDRSFBSEa6CoHRdJKvr5BWAKzikF2SUJF48p6z+aG4/3Lgd13887dJY9z5ci9duReP3SteV1LXtcLr2vBc/DC61o48qwcHMx6j157D98eHe6ceF8f7E0f7E0d7E05d+c8zmXvwYrnYO5gf9K1P+s+eOH1rLoPVj0HG173hse96nWteJ1rRwfrx64N9/6K17nq2V/x7i0f7i+6tqcOdqYWl4a+iNFDIT/NGz6a04HBOihYQwrSkoJ0YEgcg1+ARaVRWFmksGQsOgsIS6FxsqGw/5k6B+FWSmwnXd6F4G0A4WAaVylxC2TtOEU9QiEsTLkdVY2gugksYYZqmGckrVINS2TjCjl+gqodJysHMcUQJOtH5QOIchjTjyL6aUb8NNXwgmqYh/XzAL8Z4jfA/HqQV/+fMgfw6lHCjIlbMHELgjffNkcmmil4k7/M4i/7qcL5ggrqP6YOE9XdVbVhwlpEUEfCm0FRLVU7wNQPBCSM+scNBRqGabp+hrqTLLNS5O1QbAtFYibHttOUdobKEaDvD9D2+esGacoeirKbLGnFOKUop8Rn7guiYOFF/9qqfWOte/PNyObmi62dpf29Fbdrw+t5dXa8dXL42n2w5NpfOnSvHXrWPc4XnoON87Oj87Pjs5M9r3PhYHfStTfp2pt07c97Xctu55LHtXToXjnybBx5Xx16X5+ebJ+fHVycuY8Pt48Ptw5db4/cm2dHW17Xqte15jlYce+98DjnnLvju++Hd7dHchpqoGDdbXDg8zjQN0sNMQChiXBEMhqVinILsJh8mqCYyi+iCYoxbiGVmwuFJ/2PmKNJ7XR5J4hbQcIG4TZQaIdENohog4h2CG+liNseps77J84h2nGydgRSDFAUQ5Csj66eQDUT/oYlevyMf/IKWT9GTVikyEdg1Sggd8DKPkDiQFRDmKKfqR8H+M3//rsjro8Nc6vJ4haypPV3zJl84K6nqMKG6/URYcNtc6iwFhPV3Ve1Y/waQFgN8Wsoik660oFKO6ixNlRsoSs6yQqbf9wETdtzN3EsUNkfqB2lxlqZ8naEMNOkZhQ30SRmLLbdX2W/OZUJs4p5GY2vFuyry71rq73vtlZ3D/b3vE73kevwxHNyfnR0tHdysnt49O7w6L3H+/b4ZMt7sHTi3To9dR4dbx64Zl37Ewe7Ex7ntHt/6ujw9dn5/tnZvsfz9vhk+/TUefXBe3ZxcPX3k6sPp5dXJxdXh5dX3svL48uLo6vL44sLz+Xl4cW5+/xs+/T4jdu1tP9+cm9reGK2GwnW/LzI6aFgjV9wHJmX70+UUrm5TGEhzM6hsLOQyHRydAYcloxFpaORyRR2+n/SHEDYoeuW2gXjVl/Bg3yH0fE2nzkYb4Pxdlhi81e0Y8rhgPTXVN1MYOprTDfBSF5E5f1U3RQo6cJUw6DYStGMwLJBhnEF1Q7RUldh7XiAcRmTOzDdxB2+GRBbQX4DxKv5l835WipZ3IKKW1GJBZVY0F+ZY8o7AuQdZFEzWWT6mKZfmatHBfUIvw4V1N5XtiL8KpRXRZG1ByTN0tVDd40LVHWff+IIRdrO1AzCeAtVbkMEjQyFnSxuYcYN02Tt/okjNKUtwDBKk7QwFe1wTCmZUwBzSmB2kaqgcX3Rsbw48G5z8f3263e77/e9x3tHJ+7TU+/pyfHZ8fHpnte7cXj06ux433uw6j1YPHKveQ+WXM4Zl3PStTfh3p1270+deN9cXrhPTg/OL47Pzo4uL0+vLs8uL08vP5xefji9vPLl5PLq5Orq7PLq9MPfLy4uTy7PPRenO8ee12eHb/d3pp07Y3tbY2sr/eRQzU1L/bgOpweeG+DwZJSTDYamINwcOCqDzi9HOZkBogoyO9dfWIJEptOi/6PmwI8tFcY7PjbZDohovx0Yb4fxdgBvw6RtAfpuetw4JLaTNaMg4aDqpmHFsH/KKlk7HpC2jmnGmSkrcGwPVT/jJ7aR1UMkwkLWTEGybmryClnV75+2DEo6IF4dEPMvmiPx6n217Rrcr8wxFB1MmQ/c7Zh+zxzCr8X4VfcULRiviiIy0zQDsKiBpnIgoiampheVdgQmTtBU3fcSJ6hyxz3DGEXcTNP0ovxahtyGcSvJChtGNAXq+0BRHcIphDklMKdQXVC/tmBfXxlYXxtZ3xjZeDu7f+TZOz71nJ4enp0cnR4fn+wfHr49PHeennmOj9+69+c8zjmPc869P+3en3LtTXr2Z85PXp0e7x4ebZ2c7V9dXVxeXlxenV59OL+8Ovvw9/OrD2fXuTq7urp+/vD3i8ur0/Mz56H75eHBite54twZd+6O7u+MvVwbIIdqf2EOCE8NIMpRTl6ApJoSVcAUV2GR6YyYIiA0jsLK8Qs2kqOzoNAUJif7P2mOKrXTFV0gbr01sPvJHIj7ems7JLKQ8HZqwjRFN/x1zhJVPR6Q/gbVTTFSVhB5P1k3BYi7qJpRiLDStKOQrJdpXMbUQ4y0V6Cmj2FcQxW9aNwMKLbCmpE7Yiuq7geJDvAfs/ulOYhbc9NSf2HOF5qsnS7vIBPmX5jDhE2/A+7aHMqrvCdvpgjqHyRNMeTWwOR5iqrH3ziBxbbQNd0QYWIqbBi/zl9hJRMmRtwwU9Z2N3GCJrcyEsbJYjNTZYMFVaivznEK1QUNqy9sS4u2lUXb+qJ9/fXk7qF75/DYfXLiOTn0HHs83v3zU/fZxdHh0TuvZ9ntnHbvT93EezDjds0eeTdPz9ynZ+7zi8OLy8OzC6/v4eLy+OLy5OrD2cc691OuPpydXxyfne4feV659+bcey/2d0acu6P722Mbq/2/YS4kkc7NI4UkoTFZwPMElJ8FhiUx+BUoO+0uXk5hZQcKi9GINHp0xp825xdrJxHW6NTOxwndJKkVJhyw0BEUX68utOprB7/Ud4Gibljo+FFVLSmwqMr6P4+3wbiVhLeyM6yg2Powtu7b5AE/cXtA4hJFMexvmPLX9pNVQ6jUQdeOIqoxpnGRrBtnpmxQdNN04wqkHKRoxv1ie1HlAERYKeoBKLaXkjCHqYcDkuYxzTDDOIfIekg8M4nbDItaQWErTdaJSRxgTAPErQe5TWBMHcStIfF+Mncnphrg12NiMyaxYBILKm5FPsa3LILgzYzY9oeKGklWZXJNu6HCQZe0oaImCG+5KywSZ1UmVbfEVznoRJOPGsqvhgSmrxWV38e1IIIqjJ+Pp1TkmDrTWycCZB0MuR0kLExJU0xyaVq1NbnB+jRnElH3B8YPUiTtNFUXKmqiylsxYQ1T1kETNftruqnSFoxVCLOLUXahuqBhZcG2smhbWbCuLHW/eb++4z3Z8R47j4+dx559r9Pl8Xo9e56DZe/BvNc54zmY9lyzm3E7JzzOqePjt2fnB0enu6dnB+fn7vML19Hxzsmp5+z88Ozc4+N1eXX64e/nN9p8fy8uDs9Ptg725l17867duYOdKefO+P72+MZqn6+33lof0QPBcUxhMcbJZRKVFE4hA69AozKpMXlgRBKZlQmGGMlRaWBYCoP1581B4m5A2Bim0ooLu/2kHRDRCUUWRRjyn0rTOHJ1ZccwQFgBTmWwMu1bSQpfmVza2oni5v/Nq0wtM0FC83fCbGF+H6LoImumaLoJpnHhXvL8g6wVumGGGr9I0UxgmjFI0Y8pBtDYXopyAFKP0OJm0PgZsnGRrJ+nGibJqiFUPQzJelBFLxprx2RdqLyHoh2lakeZhgmKfoKmn0bVo3d49X68RpKwGRCYQW61H//WnWphIyZtxaTXzmCi5bY5lDDTZO0BsY1EXFKYzEiJ0KQX5MoKLGRRHcqvxHXJYbFxgaHSpKxMRVGbzxwkaMA4xeqMgnvCCkZMdYxCxdLEMZ/jyQW1hrKOh3HD/tqep4pMIjHNP8oQo08qbhumEY10SRNV2sHQ9jJUnQFxwzRVN13bTZO3U2TNGFGLRhfA7GKUVajxmVuwLS/YV1ZG3u++33Kd7LhP94/OdjzePZfb6d7e2p7d3Rp17U16nNPeg5nb5g5dL05OtrxHOyen7pPT/ZPTbe/h5vHp7vnF0cXl8dnFT+auW+qHcx++qw9nx8e7R551j3PmyL14sDtzsDvj3Jnc2/ptc3CQhsLOgiPT0Kh0criRHJmKRqeTuXkYN5cmLMa4hVR+MczO+1f2vu7I2/47ti+AlSEptPpJrSRxJ1Vhp6k6SIT9f7FKkkoaAHELSLTBwqY7Yscn4fkplS2wyAJwC1OKqmDc9LUoDa96QU19hcS/oMdPIMoRSDmOKbsfxA+T5YP0uDmKftrfOEc1TFOSVzH9BCV+EpYPAaoRMLaLEtuHyhwU9TCiGfVPnCbHTdMT5xDtBFk/Bsh6QEUfKLGiUitCWMnKfrJygKmfomj7mXHjAK+WxLu+3Arz6xCx+WNafxncTJO1MxQdfvzalPwyTFBxR9LxMEarLWoGRZZPeFX5VbVkfpGfyPJljEZd1OAzBwvqv4xJlGYVoLwqiFcan5X7UFLzCV75kJOW1tCE4q2YzBGjjQ9KbYT0fTRuTq6pI1DXR9XY/VUOVNqBSSwobqITjRS8niFrAzglMLsQZRXdmFtdtPvMra2Pbrxd3nTuvXMe7Hjcb/e2d/bev3w9svl2YHtzaG9r1LU36T2Y8cW9P+12Th251s7P3Gfnh1cfzs/PvSfHzrPTg+OT/cvLk7Pzw8urQx+484vjm67qe766Ojk723Puzbv3J507E74c7E7tb09srP2GOfCZlhqTyxQUYbwSmqAE5RZRuPlwdAYclQ6HJ2HhSWhoIhad/q/s8UOSTkBs/4KbIS6wkiRWiszBVNjvipvCU/o58UWqzAqAaPcj+uCYxpgce0xiVlpuFUS0+bFrC5tt7DSrMC4/u22MGj8PKQYYiYsUzVhA2mskfu5+xkpg4hTT8AKW96HqQZiwUrRjqNRBM8xg6hH/5CWydpSetAZrxmnx86C0E1IOg7gFU/WCYiuqm0DkvYykOUw7RDcugpohStw8RNhg+QhJ2ArgbSRePYlXdyemFuQ3oIQZEbd8zE15a0GJFhg3M2TtDFkHhjcjMWXphZVkfglZ1PAZx6AvrKfy6kBOUXFlJZVbDIvqvuQZdAX1iKAa5jeh7LLY1Ny7eC7Cq8diyuKzMr9SVvvrBu9yCtNqLF8lDT5ImOBo0sLSu8iEBQnPK2popMraMFENTW2nStsC4gYpcltg3DBFZr2n6oaiSxBWMcIq9pnTFjbemFtZ6V95Ob7xbum9c3vtzcL666lXr8Zerfe93Oh9+2pg6+3Q3taI+yM7j3Pa5Zw9Odq+urq4uDy7/HB2cXl6cXl6eXl2cXl0fn54eXV0fnF4cnpwfLJ9cem+uDy/vDo7v/Rcfjg6vzz58OHk4mzLuTfu3B1z7ox/zMTB7vTGaj85VOPbciAFaX3xC9KAkakwK4cUloSxUsGwZAY3D4tO9xcWYJxcuqgcjs5lcPPhYN2fP7Mp6SZJOz6PySEKHYiik6HqBnnFuszsCKXmWWwqnV8B4nZYZAUjC7+TZAfJtcUNDohohWMan0gSw9RxsnhZXEndg7RVsmaYGb8ExfZQdDMw4aBrx2jy3ocZL8iqwbspq6h+nJG+CmpG6IY5UNpD0U2CQhumnsRwG1k7Q1Z0M1OWMfUwPe0lWTFENSxAhBXWjpJEZrpqCCCaaeoRRGqnG19AohYSr94XQGj6jcL2saVi4haarJ0payeLmjGhCeQUpxVVUfmliKjp00itpqiezKv3YxcVVtTSeGWIqPoLfkJcQR0srEL5DQ84ibGZeaCwDuNVQjGFhpysr1SmAP0Qg5WaXFaPimo+TezlaAyhmZ202HaKoKK00XzPMEyXtjH0/RRhI1nRBQoqKfIOgFcEs66X5ZCPdU5b2Li+3Lm25FhddKws9SyvDK5sjL95v/jq9dzGxvTq2uCbjaHXb8bevp14vzm29eZ2tZv2uF+cn7uuPlyeXZxeXJ1fXJ1dfji7+vv5+eXR6Zn7/MJ7eXl4ePjG413xeF+fX3jPLzxu76uT052Li8Pj4x3X7ov9nTHn7vgtc+POncnXGyP0CD0pSHvbHClYDUcY70rKyaxMJlEMR2f68wvg8FQyK/NOUBwUYiA90YKPlXcexf7r5pQlDqqmBxTbAmLy9BkV/1tiA4kOELeBuI0ktt0R20mEzS8iJ62iAcEtgLgZxZthkenbmFReZisSNwPIBwMSVynq4ftprzDdjH/qK1Q9Hpg0x9ANk7VToLidqhuCJR20+BlUOXg3ZQlTj1JTXsKKIaphAZbaYe0kIGoja0ZQYStFO4VIHczkeVTVGZC0AqkG6IZpWGKFNGOAoNkHDhI2oRLLH5ijyzsYCisZN2NCE1nUDLKLM0pqqPxSFG/6LEqnKqgl8xr82MXFVfU0bqnPnC6vChbVkjkVeGrG5+JCmF+H8qrhmFJjfs4DSTlTN3iXn5Va3uQf63hoGFeklYdndWOEGYjOz6+poKhaEWElRdNFJ8yBhhFGrPmhboIirEdYZVB0ERRdBEcXQqwiJLpQV9S0sdK1vty5tmhfXXAsLXRtrA+vLQ+srY6/2VxaWZ9cXx9cfzX3+u385rvZrc3prc2hva3Rg90Jj3PqyLt0ceG+uDz3mTu/PL38cHb54ez88ujqw/HpmfvIu+nan3U7pw89q+en773udbdr3utZOzl6f3769mB3yr0379qb/IW5l2tDdzlGNDzhF+aA0HgyJ4cUnEhm5ZOeG/2eqf2e6fye6fyCNKSnCviJEn4i93ss+/PzVnHf54omrja/oLHv24ROP0knhZ9dae37IbH/md7yTXw3hLcj/DpN3cg3assTVbEqtwIVWQBxC4g7QNz8pcDIze5iZqxTdRP+ifOwvJeqnwIkXWTVKDnWTjesMPVD32S/wPTTjNQNTDtOj58mybrJ2lGQsGLKfkhspekmUUVvQNILimaQlrwAa8coCXNwrB3VDvqJWzFFr5/vVZgSBzNhkqLs9uPVQ0KTb9X3l9qIVoRoQcVmurydLu9A8OYbc2hMeXJeqSjP8jC2OkqaKEwtQ4QNYEyxIbdYnNfxmbgkQpZKpJaD/AZ/lkGSXQzz6qncCohbi3FL5Glp8eVd3yZ0BStzZFkmFG+gyNrCFQlFnXOfKtqeqgrjc4sf6AcZsjaGro9MNJPlDkhUjkktiLAcjs7zfYETjiqAWEVIdIG+2PRqrWdjpWt9ybG2aF9ZcCwvdK4t9GysDiwv965ujGzuLL/b39o+2H69ufT+7ezOu/Gdd2M7W+MHu5PH3uXTo3cXF97TM9fZxdH55fHFh9PLD2fnVyeXH87Pzr2Hng33/oRrb9Lrmj/yLLr25w72Jr2upRPvS/f+/MHulNe56NqfdO6OO3cmPPuzB7tTzp3xl2uD5BANnZ2KhP3Ezi9Y7fc87i5eRmNl3xOVY5HpYJAaeKoGnqqBZ2rSUyX4VAU9Ufwr5lCi6VsijaNLiVJrI5NrqXgrFlYiNWSGaNKjtLro9AaKwEyKKo/WGuTJOTGqxPuCchhvB8VmCLdiQgs5KukzaS1J4oBFbXDiC9TwgpGyQYufoxs3KPoZatwcouqnaAcDtV1keRcq66JoZhDtjH/8Aho3T0tcAONeUPTTiKIfUvQBYhsi74GkDkw5hCpG/ONnaLqpAMMsRTPJ0I4jsh6y3E4i2kFRs2/t7bdmDC0I0UyVtTAUHb5VEjLefG1O2MwM1xjzSzLyiviqFH9+ISQ0YYI6aog2saAktSA7SpvA4KUh3MYvhClfiAthfjXMr4b5NXBMI+W5Sp6RllJQyIuP/yzOQtd2M+InqLwCTVJqdm2+MsFwX5AGSaywpIkqt1OkFn9NN01pD9AM0mOtYHTRz+tcQVxJ89uX/dfsljvXlhy+aezqon1tybG2NrR5sLvtOdn2HO65d7e3Vt9tzm1vzW1tTe5vT3iccyeeNY97zeNd8Xhenl96zq+OLj9cXHw4v/r7xenp/sH+vG8r1jfVde1NuvamDg+WDg+WDnbmvM6VY8/Gwe7kwd7Ywe6Ee3/GvT/n3B17uT5IDtEAwXo6Ow0Ojfd7prk2F6QjR2ZCIUnAc8OdZ3rgmRZ8qvpZ/kVzii6qygFKLBDhgMQdoLQZlLTCRAskskN4O4jb/MRtENFO5ZvvEBZEaIVFnQBuB8WtvpVhBDchIiuMt2EiM6oZJKsHEfkgQ96Hynr81UNU9RDTMEM2TN5NXXiQOoslLqCaEVA7AMp7YHkvLHUgygFUMUjTjVL1I/TEGThuFomfg9SDqLobbYaGNQAAHNRJREFUkjsguQOM7UCUnYjcQdcMATG1kKjpZr331+ZgkZkma2Mo227WgW/MUUUNoNhE4tdAMaUk3AwKmjB+HcxvRPm1CKccZpVSeFVkbg3Er6Wwy1Fehc8cJKggCcoRbjXKLvJj5yPcfFhqoUlNFJGZFttOja0PkPV+mtjLjOujaDsDVN1kmZUsbYMICyhuhLilECcfZOffNgdH5ceXmt+/GXr7sv/1eq+P3eqi3TerWF10vHn94p3LteU+3vOebrvcO+79d3ubm1vLm28mtt8N7W6NuvZmPQdzHtfUsWfp9Gz/7PLw4ur88ury6srjcS15DmY/UrvJtHvvhWt33utcPHFvHB6sHexOufZmTjwrXueya3f2tjkgWE9jpcCh8aQgLfjMFx34TEt6ogaeasCn6j9lrseX28M4SNxJkXUzVb0Q4YAJB0R0oSIHRDhAsc1P4iBJWu9I2kHcCoscIG6DcesdaTMiakeEDghvBwgrRLSCeAdAWGGRHcStfkQHrB72T1qkxs0Gprwi62YYxg2yvJ+hngClPZBqmBbb9jBxElGMUI2LZM0YM2kDU0/RE1dQRR9FMwKIHRRlDyxuwVQjiLSfqZ/AVMP0hBWacpKasITJuijaQQBvgiRtqLgdlbQh4jYUtyFiCyIxI2ILIm5FJa00WQdd3oEQTRDRQhY1I7gJxVsxYQMmbEbwJkxUiwrrUUETWVAHCWsBYQMiqEH5tRi/DuXXI4IGlFcLCGpRXhXEr4MEDSC/DhRUgrwaUFAI8CsRbg3GrfOX2RkJozSFgxE/TpO2kdUdFGHtZ3EDiKCKLrWR8da7qm6qpP2+fgBml0NRhSCrCGIVQtGFcHQhxCqAowoSys0770bevxl8+7LvdrVbXbSvL3etb0xvOrc3D5z7h2e7h6c7hyc7R64D9+u3GyObr3u33g7sbY2592e9B9PH7uXDw/cXV0dXH86vLs8vz3Zd+7NH7nmPc/rn5qbce7OuvVmPc86zP32wO+V2vjjxvD72vD47eudxLuzvjL1cH7gxBzzT0qNTkJB48KkWeqqBnqmhZ2rgieo3wP0jc9e5MUeSdmHybrq6B5B2AuJeUGy7I+0giXvIIhtMWAFxByBuA3EbiNsh3ArjbRDRDhLWT6St/y21AuIWWNQFis0g7gDEHSTCDhEWmDDB8h5a3AxJYkX143ekDnLcOCgfYqSso9qp+0mvEf20f8YLsrwL00+BEjuqH79DtKL6fj95DzlxBVMPUdI2UNUkNWkRkjuo+ilI1EFXWEG8BtIMA2IblrxBibVh4nZMbEXFbZDYTJK0o0QLRdSGEO2IuIUqb2MorL6WChNmGLeA4hZU2AIKm2CBBRY1wMJGWNiICJpgoQkQNkKCet9SHCRohPkNiLAWFFSDvCZIUAnza2FBARpTReFUYpw6mFsL8WoQbgXCrcD41XR5Fypopsu7QImFbhhBFfb7xtF7SkdAXD+FaKIqOgBhOU1mBdilcHQ+xLq+UwNHF0LsHDiqyFjZvL89tvNu+N3rgdtNdnXRvrJgX9sYf7W5uuV0brmPtz0nO97TnUP365cTr1e736z3vH3Zu/V2cG9r3L0/6XW+OD7ePL84vLw8vTz3nh5tupyLh+5F196ke3/qYHfi5/ImXXuTrt1xz8GL48Pti1Pn2cnOydHmkWvZuTv2cr3fZ44UpIOeaqGnWmZUCvY83vcMPdUCj9Xgkz9X53pgaScs7YQknTfmyPIuqrofkHaSYrtRUfPftLXSgo7HqQ4/aScotvmJO2GhOSrN+ldNF4jbQaINItq+UJjEOW2y/J6nye2IyE4SdwDcZlZKtTS/OzzXRpO2QHgrLGq7K64lGt9g+rm/xo+KyyeoCdOA2IpqR+9LyoisVk5h31epc4huJjBl477GKmncgOUDtLgZmGiFtL0UQbUg08zLa4/M7UeUg8ykdYpy6HlqB17co6ldUJZPfRrXj0raGUQtL6M5trCTnd1BVVh85miK9pudrrt4ZUyhA8absZjKqHTrfVkbLGwCRfVkQfUTdfn3+jZI0IgIaiFB3T1xKSfXRuZXkzlFQUlVX2pbQUEdElkcYagVZjWIc+oVZT1kfgnMLUK4FXBMOVlUx0waQ1V2pnEUE5voagcoqCPLO+CYWkzRgQgbAtQjFKnlQVw/WVjza3NQRGFipcm9P+XcGd95N+xrsjfsVhbtKyu9axtTb96/2vGe7nhP947Od/a3Xy4PvVzufLXW9WajZ/NV3/s3Q3tbI+79uaOjt5dXx1dXZ5cXB27n0pF31eNcuNmZ/Q1zzvmTk+2rD8eXl+cfPpydn7u9B8sHu+OvNgaw52rfpZsbZ9jzePiZ7g/MAU+U4BP5L9ZK+mFJHyTphSXdZJmdLLXB0mtzFHkfXdULShwkaTcp1vENkSVKLgwIkZu7ur/XNUFi2x2JHYzJT85Jiy22kYgeALfBeMsjQo8bswKjDOn1pvAUG0B0gBGFKUUFn7FSnksTTD0zsKgJFTY/YCdn1bUD0s773JLSFluA8QVVPfmF1qLKLPpUUh6iSslrGwVj2xHVcAAvI63WBsoctIQFimokIGmZxs1ILy0J5KSrCyrjy9sQZQ8gMgnVyeEJVfcU1V8rK+7LGiBJKzUiKae47B7HwNal5Lf3oWILTLTARDMsMvnCjEhuMVc+ijeTOCWZRUWfSspAkRnEGzB+iVitS66xgsIGWFgDC2s/Y+urGqt/UFXdZaflFqUF6ytBfg0QkpacX3Q/SsEI0f0Qm4Vyy1FOpc8czK0g480wt5omsyESi79+AI1tuxc35C/puKsboBJNDMUALKjDRA0gpxSOLrhlrgBi5UKRBQnlJl8pcu6M/7zJdq8uOqYnWubnut+8Xd3xnmx7znZc7pXlvrUF69pi58ZK56u1rtfrPZuv+rc3B/e3J93ujfPzw/OLw0PvK49r3utacO/P/765qaOj15eXR+eXRxcXJ1cfTs8vXIeu5YO93zYHPdX+gTnSYwXpsQJ4LPN7JPuFuX5Y0gMSVqas3T/WCou7IXEXIu2hqYZ+arISq8KY+pmqGRJa/dnJNLwOENsBwnZXmKswpuW32jBBOypqA4mO78VJ2iI7OaY+Up4YkdQBES0gpz69sgoV1sOcan1W9l/jLbCoxT86I8c2To8buyeuym6ow7TjsKwzVJ4VndVCVw2jfHNeVd0DfQ/TuPCFtDah2k5W91HUQ5DEgSoG4Ki81DorJu2D2SW1rT3MhNkAw4IguYRbNPo34zhNPwaKrZDEBEdmVrXayXjzVzHZ+ux6VNwCEy0w8dMREmpkQmZmarGlC+OV6HOLqMJyRGBChE0YJ02XmVvRXE8XlJF5VaCw7l60MbskvaChRZyQnl+e95WsAhRWAaz8jLISpjDnc6KCxi+FeJUIt9zXWxFuBcwpgznlTF0PXdbhH9sB4w2Y2IIKmigSK0aYGMouZmwLzC6E2MUgqwiMvr4+CEfngdEFcFSuoczk3Bk/2J3wsdt9P/LuTf/bl72v1/sWZmwz420z07a3b1feHbjfu45ev5r3zTB8s9r15c5Xaz2v13s2X/Vtbw45ndPHx++Pjt+6Dqa9BzO3j5/c5KbJeg7mT062Tk/3jo/3z8/cV5fHx8fvXXtzzt3xVxtDv2nudm8FHqvBJ6rfMPf4F+akPaik49u0cZrMCokdPnOQuAskbppsJyxqSi0qo0qaYLyLRHTC4g5I3AHjlti08r/FZhkKaj5VNAF4D4h3/CBJrjOb0ooq9QVFoNAEES0Auza9vAIT1kN4G1trDEttRwXNgazshuZKVWalJqs0s9pEi5sNjJuNNWQ+zRmi6Kcp2v74rNQvE22gsucBv8hQbkXUg5BmjGaYougnmURtWmMXqh+6w6/NrWqGVWZUPxau0ee3tKbX1AuKBlG802euor4qvqCqorkqEC/ygbttjhYdry+sEcYlirMbE3KKKKIKsqAWEpqey5NZKWVcg5FtrAf5jYCg9m5UoiI1La+yRldUIU5M8JmDWMWZOXkJVY3Fzc33hEUIrxzhlV/XuZhymFMGccpgSRum6KIpe6jaHoamO1DdSVd2khVWsqSFTDSAnBKQVQREF/rMgdF5UHSuz5yuqH73/cj+9thNtdveHHr/ZmB9uevty+GFmY611b63m0tbLufW9qvFxc6blRQfu42Vrpsmu7s9sr83dbA/5Tt74ssvzN3UOe/BrNe96HWteF3rx97X58fvD/bmDnYnDvbG11eGkSDlb5oDn2jAJ5p/aG4Q9pmTDUASxw8ZE9+lDqOSDkDcjdyat/40gRW1KNOyP9e3fyLuxnAzRHSAEjvAqiqrbsisbczNy9fV2iDcBhLWH4gUUXYLHJRcb+9DhM0QYfFj1aWXV6J4HSJokKfm3JPWwcK2gOisuNJqkshCjsrLN1kgZQ9FPRypSBLmWf1kvah6MK2w7DPjCBo3/qm0Rl/VR4sbJ8X2YsoBUGKjcPJS6toQzSQck59T3cwwzj7MeMvRZ0bmDyOybswwheBmQGIGIzLzG0wkTmVyccGP2g7kV+YYUUmGEpNfSLzF3lNbU0MRVmHcGr+YmoSM7KrmltSS0sIaC8qrAfiN96KTFNk1aEgiVZAj1CX8TV4O8qtJrFxjcQHGzUQi8yBeNYlfg/BKfeYgThnMKYM5ZeTY9gC1nYybMVkrJGqgSZpgYQNd1QZxiiB2PsgqBKIKfmUuH47OYxly3r0d3Hs/vLvjWySb3N8Ze7PRu7c1+WptYH256+VG/8tXs5t771eXBpeW2lYWrD5zN2t4t5vs+zcDe1ujrr2pmz3ZX5c6197kwe6Ea2/C7Zxw70959meOXAte54uD3bGDvTHX7lh3Twv07GfmbjnT/JTf7K2PZX/5PH4QlvXDsn5INvCFcfRx+ggi7nio7/8udQK5NYe4tcdv/5Kva+yb+0rWmlBueZJshgjHl7hOk1NM4pux4ESzo5MW64DwjkfieE1RCyhsU6XEKar7QMJM4pRUmVqD400/KrJFqVmooAEQtT2ISYsvqQRFLQHRxozaSlTuuJvyks7KNA8ufRc/EpRQjicXQNp+kGj/Slpc0mL7NqHnsbEvKG8NUY8/EJtbOkeCcka52tTniXX+atuDxBlJYkpS89R3ypbnCY6Hig4MN/mz0krqGmC85dsYXY1jEBE13WjzJZCVktNoAfD67zmykvKCe0QZwjcHxCSmFhainLz/emY0WRq/UdVB/IYvuPHK3FKYW4+xC0WJyY+1ZQi3DojKyq9qCtE3fietDEupI/OK0Ziqn/fWMphTBnFKUKIh0NCHKa2BcT2YpMVf3eUXUwKz8uGoQt/2w425m5CjUrsnxnZe92+9n/A12f3tcffei/evxzY3Rt6/HFlb7n77Zvbt5ovVlYGVRZuvzv1WtevxVbutt4N7W6Pu/Snfwad/2GR99fXj85B7Z4KtTwKCfPe7tD9D9ov8hjmF3yP5Xz41DAHyfii2C421P9T3huTNfm3oC8+apkk74FjHr83BYrsfr/ZbSWGELuMrcRpIWEli272otIeSqk/EViCi5ElcKVnSBuNt/tHZX8WWkMQ2Bsf4rboeFrXeYVU9k6WExhf9QOTCMSYQbwOFLeTw9K8khZDAjIbn/BCbj4pMZE0vzG0LFOQ9jc96nGAix7YzUl+RNVP3pK3ctLJwYwNbl/+9seO/JC1kQXlYUlFwpuVrRV1Exc7zkqWoml1+lklfbtMV1SZUND/LsKK4CYnKfyLLhokmMCqfnVCLikw+ajdzCEpYamhcJSxs9IsoeCJMpAuqSQJzQHj8N8JcWNAEsoq+lRnvSmphXhMtPOk7SRrKrUVZ5Z+xFQ+E+WhMxZ2o/B8liRG69HB1KiuuBuMUknhl1401pvzGHMgpBvjVqLwd4FWgvDI4ughkF4OsIohVAEUX/J45UmTBN/KcqaWB3deDu+8ntt6O7rwbd+3Ou3bnd95ObL8ZebXe/ebV+PrawPpy9/qCffVX5lYX7evLnRsrXa/Wum+zO9iduKl2f9Bkb9i59ycPdieyy0qxx4qbbdY/ZQ58LIMfy//yuaEfjbV9lzYSkj99T2X/a3xvVOHcA7UdlHTBv9VbEaILJjohsR0k7HfEXYDEQpJ0AOL2O4SdJG6FBXYQt6JCG0i0A+J2EmGlCGwQ3goQbYjI9InEDhEmEtGOiGogUQuCt4AiCypqhkRNfngrKDT7iVohYQsoMoHiNkb6BkU+6W+Yg2Q9VO0ASdxOVY3dkdpounFAMcRInke1owHGdShhLjB19Vn+1KPCF2isnSrroWlsX+s6AhXtmLj1O4P9x+ReVGiG8RZM1EIRNUB4E/zRHCRs+phmjN+E8hvJ/HpEUAMImxB+PcavA4UNsLAeFTQCwhqUWwfx60B+LcStJfGrAH4FGlNN4jUgvEqQW+3HrUY5xVBMJZldCsVUQpxiiFPmy0dzpSCnmMStDIjrpcU2guwiKLoIYeUi0UUQqwCMzv+5ufwbc0B0FhCV/7kwv7Spfma6a/PN9PvN6ZfrYzvv5/d3FtaWe9Y3+pZXhhcXuxfnu5ZmuuZnHbMz9tkZmy9zs/a5GfvcrH1+1r4w71ha7FpZ6l5b6X21MfTm1cj7zfGd7emd7entramt95O/zLuJjxnffDs2NGwWxiUhz+TA07g/aU7p91hBeqxAn8geyfL/8pW+88fUsR/SBj+Nc4QXzVMlbZ/GD4XmzTFibajkN+rcP3e/9TfuQ3zMzftKfgoobLnOx3fkgMJmTDkIy/qp2ilQN8hInEfjZvyNLxDNNEM/gch6MdUQLO6k6Scpmv7g4hff5c7ey1ijGZcgoh2WWGBxC0K0IUQbipu/Ndh/TOrCiGaEaEZwE0KYblrqjTlUcB2E3/gx9Qi/DhHUI/x6hF8PcWvAmGqIWwNxayBuNcSruh2YV3nTSX8xjIM513fxIXYxzCoGOeUUsRnlVYCsIpB1vbUKRReBN731V00WiC4AogvByFxKVEaYoeZ7aeYTZWaovvB+pDpcVxyhL7gfqQqIUDPDtMwwHTNc5x+uZYZrGGGagDCNf4g6IFTjH6r2D1X7h6oCwlTMELl/iNw/1BeFf6jCP0zpH6pghsr9w+TMMLl/mNw/TM4MlTFDZcyQWP+QWP8QaWAITnsuB5+pgCCtbxh3kz8ub9crc0+VnzxRQc8UwfLsz6Lj/xKcPRRSME6RtTLUPVEFc18ZB8jS1i/j+u7rhyDJrw+m/z8yd0fYyjDM0+LnUd0spB0hy3tAeT8U2wPLhjDlMFUzStVP3U1dvZ++GlL55vuCVXrcBEU3hmlGYXEHLP7JHIxbYML8TWLPI2MnRViHihoRovkPzPnYoYImhF8P82pv8lHbL82B3MobczfN9Ofl7SdzELsUYpdArBLf39vmfiEPiCwAoq5LHRCVB7Py4YiMR3HNP2qrkfCUB5KysKSGe4I8NCLzB3UdnZWNRGQC4RnXCUsDwlOBsDQgPI0UlnorKaSwFL/QJFJYst91kkhhSUBoEum50e95Iul5gt9zgy93nsf7BceTguNJwQYgyAA+SwCC4kkfX75ECtL5PdP68lsV7pdbXn5PVeAz5RN5zhfsOOB76V++1HU+Sp98lj32vGjub4l9T9JGIvJmGbFWRGIHpb/RW/8f1TlBEyTppmpGUcUALeEFqh1lpCxi2nFG4hKqHKJqRxHlEFM7+ih7/lHOLEU9EpC0TDMu3EtfhYg2mGjx7WXBuAXGW2DcjBDmbw32x0ldFFE9/HHv4ffqHMxrgHkNMK/unzF3u87daPs9cyC7FGEVk6PzYXbRbXNgVCEUXQREFvxmnQOjcuGITDonx5+TEZpkeqyuDk9o/pu0INzQ+FxXi4YlQ+HpQHgGHJr+MSlwaDIcmgKGJYNhyUBoMhCWdP0blkQKTQTCjEBYku8XCjXCIUYwOAEMNoDBBiAo/mPiwKB4KCgOehYPBsWRnmtIwVrgVnn7A3PAr8xhQYpn8qxPWfo7j5Sf/KD8i6BiliKz/pAxHpo9hkmssLiLKumCxV3/3rsj/l1ziKCFouwJSFpB5X0U3ThJ3Ilphu+I2smaIUjWw0h/yYgbC6veeZS3cjfrNUU3EZA4f083+DxvNjh79Ifk3gBJEyIyg0Q7gl8vi8B48zcG+xOjHRL9VOcwoQn5eUtFBU2/a473a2pVCNeXyl+31N+qcz/F11vB6N9tqVB0DsjKIkVnIuyMp4mmiBTz96oKJic7zNjynawUDUtHw9Kp7FQ4PN0XKCztY1JvBwxNuQkUmgI+T4ZCUsDnyR9jBJ4bbkK6rm3XAYOvbzz8op+SgnR+zzS+3Gqpt/a4rndXlXeeasAnqmfy7E9ZOtIjqd8jJfBI+RdJ9ex3yf10WdsPGWM/ZoyCEgco6YL+f5uDBS1+ojZMNwvEDtCNi6h6NCB1DVGNBiSvkHWT/kkvnmROfZ2/Bsts5KQXqHLg+7zFyPKpqKL+qILB6KKhyLyhwNhWiGiD8etrXSDeAgvN3xs6vomz3Exabwrb/2NzIKuI5FuN+/1hnO8BZWc8Npo+JYof8LIjU1o/J4oC2Okhiaa/4vlQaKZfRNafMgd+1AYEJ13neeLvm4v7+A7DP2fOxw54oiQ9VkBPlI/khZ9yEsEfCb9HseCj2M/YyX9hF088Sh8Jzp8mS9vu6vpBogslHP/2O3L+bXN8Myi2BiSvIPIBavwMIOkhaydIeBfTMEvWjHyTv/YkfyEwd4thmA7IekfVjXLqN9mlM6yCIXbBMLtgmJM/9CylDxa1wLhva7UFEplRgZlB1D1Ls0OCRl9LvTVj+B83d13YPgaILvwH5li5UFRWIK/oM1EZJSLjma7psaoqxND4jaKKzs5kxGQgYRlweMY/bw4ISQaeJ/2k7Q/NAc8NfkFxv6bm66p3nmr+uM75VkZIj5WfxyR+w08CHsfeeSzzexSLPZZE6kr+8kVcr7/cEVk0Q5HZfOdKEEnP/3dzvtdrkjUjkKqXljiN6MYDUlZpiasPM19R4ue/y3rxZco4NX4KVQxQ9ZOB2n5B/Vte9Qorf+gmkbnDZGEdQrTBhAXCmyHchPEaHyfYvtS23Azj/qG5nw3j/mlzSEw54tvyYpdC7NJfaPttcx8XR6CoAjgqH4zKIken/U1R9lBaCkWkB/AL/yqtgCOyvsALguMbwfB0MDwdCk+HPoL7PXM/K3KhKcDtCvdTjKQgIyk44afyFqwHnsf5/Q64j/nH5oBH2i/5KfdZer9Hiv87k+ga05hal9Tm9JZ5DCEt2+KmHvPt2mdWvYt6Z4FRnubWmBetcW4/ad902KbhiHnNUfumozZNxxyaD9u1nfPsPJm19L5H/xXn3uuOPdfcOs/mLr6Vt+AcPMHlTjuYPvmwZfEK69KV/nUbwtu3hrZuiOnaEtKxzaJ4OZ40B+9DWOSj9RtISHOwpDaXjDRnmj3NPGuKfebkxNa1IZULElrX+pYvtEifZJXW75Q3LbFzg2vBDLPUCZBkZ447zcHLNiLSXDdmmjNNbCEyzSGmvDDSnElMvXNKU0ztDLOYGkiac0isS2uZx5A167RPx16r6t0WVfsHW5ozr95r03zEuuWk+8Rbzl1XPCbec+q45Nx9w675ZMy08x5tR52aj9nUHXRoPRnWczh7xqHsGQehyW76wYCWbVYla2K6duXOOJoz/WDOjP0RndssitdYFq3Fk+ZghRwJac4id6FF7gKL3PnmOfPMc+ZZYCCCac4sY6ZZ5nSzzGnmWdMtM6dapU0IqFroU77AKm1CVNMq75I5JhnTrDImJ7WtdM6bbpox1RxvmkNJZOSmOZOEZsrLOaO4WvPomrjqmXaJ9SbRNSbRNRZR5TFVUwCl4DE20agGMwAAAABJRU5ErkJggg==";


            var resim1 = "";
            var resim2 = "";
            var ids = 1;
            var rowspan = 1;

            for(var i = 1; i < $('#kartListesi table tr').length; i++){

                if( $('#kartListesi table tbody tr:nth-child(' + i + ') td:nth-child(3)').html() == "_" ){
                    $('#kartListesi table tbody tr:nth-child(' + i + ') td:nth-child(3)').html("Tek Çekim");
                    $('#kartListesi table tbody tr:nth-child(' + i + ') input').attr("taksit_oranlari", "Tek Çekim")
                    $('#kartListesi table tbody tr:nth-child(' + i + ') td:nth-child(1)').html('<img class="oe_image_tree" height="60" src="data:image/png;base64,'+default_img+'"/>');
                }

                $('#kartListesi table tr:nth-child(' + i + ') td:nth-child(1)').addClass("tble" + i);
                resim2 = $('#kartListesi table tr:nth-child(' + i + ') td:nth-child(1)').html();

                if(resim1 == resim2){
                    rowspan++;

                    $('#kartListesi table tr:nth-child(' + i + ') td:nth-child(1)').html("");
                    $('#kartListesi table tr:nth-child(' + i + ') td:nth-child(1)').remove();
                }
                else{
                    resim1 = $('#kartListesi table tr:nth-child(' + i + ') td:nth-child(1)').html();

                    $(".tble" + ids).attr("rowspan" , rowspan);
                    $(".tble" + ids).parent().css("border-top" , "2px solid rgb(187, 187, 187)");
                    ids = i;
                    rowspan = 1;
                }
            }
            return val;
        }
    });
});
$( window ).resize(function() {
    responseTableHead();
});
function responseTableHead(){
    var th1 =  $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(1)').width();
    var th2 =  $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(2)').width();
    var th3 =  $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(3)').width();
    var th4 =  $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(4)').width();
    var th5 =  $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(5)').width();
    var th6 =  $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(6)').width();
    var th7 =  $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(7)').width();
    var th8 =  $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(8)').width();
    var th9 =  $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(9)').width();
    var th10 = $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(10)').width();
    var th11 = $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(11)').width();
    var th12 = $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(12)').width();
    var th13 = $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(13)').width();
    var th14 = $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(14)').width();
    var th15 = $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(15)').width();
    var th16 = $('#custom_tsm_params_form table thead.orginHeader tr th:nth-child(16)').width();

    $(".orginHeader tr th:nth-child(1)").width(th1);
    $(".orginHeader tr th:nth-child(2)").width(th2);
    $(".orginHeader tr th:nth-child(3)").width(th3);
    $(".orginHeader tr th:nth-child(4)").width(th4);
    $(".orginHeader tr th:nth-child(5)").width(th5);
    $(".orginHeader tr th:nth-child(6)").width(th6);
    $(".orginHeader tr th:nth-child(7)").width(th7);
    $(".orginHeader tr th:nth-child(8)").width(th8);
    $(".orginHeader tr th:nth-child(9)").width(th9);
    $(".orginHeader tr th:nth-child(10)").width(th10);
    $(".orginHeader tr th:nth-child(11)").width(th11);
    $(".orginHeader tr th:nth-child(12)").width(th12);
    $(".orginHeader tr th:nth-child(13)").width(th13);
    $(".orginHeader tr th:nth-child(14)").width(th14);
    $(".orginHeader tr th:nth-child(15)").width(th15);
    $(".orginHeader tr th:nth-child(16)").width(th16);
}

$( document ).ready(function() {



    var allPanels = $('.hidingpanel').hide();
    $('body').on('click',".accordion",function() {
        allPanels.slideUp();
        var $target =  $(this).next();
        if(!$target.hasClass('active')){
            $(this).css("border-bottom", "none");
            $target.addClass('active').slideDown('slow');
        }
        else{
            var $hede = $(this);
            $target.removeClass('active').slideUp('slow',function(){
                $hede.css('border-bottom','1px solid #6462b7');
            });
        }
            return false;
    });
/*
    $("body").on('click', "#odeme_secenekleri", function(){
        var siparis_no = $("#name_temp input").val();
        $("#tum_odeme_secenekleri").css("display","block");
        $("#ode").css("display","none");
        $("#ozet").css("display","none");
        $("#odemeinfo").css("display","none");
    });

    $("body").on('click', "#tutar input", function(){
       // $("#tum_odeme_secenekleri").css("display","none");
        $("#ode").css("display","none");
        $("#ozet").css("display","none");
        $("#odemeinfo").css("display","block");
    });

    $("body").on('keydown', "#tutar input", function(){
        //$("#tum_odeme_secenekleri").css("display","none");
        $("#ode").css("display","none");
        $("#ozet").css("display","none");
        $("#odemeinfo").css("display","block");
    });

    */

    $("body").on('keyup', "#cart_number input:nth-child(2)", function(e){
        var cartType = parseInt($(this).val().substring(0,2));

        if((cartType == 34 || cartType == 37) && $(this).val().length > 3){
            $("#cart_number input:nth-child(3)").addClass("o_form_invisible");
            $("#cart_number input:nth-child(4)").addClass("o_form_invisible");
            $("#cart_number input:nth-child(5)").addClass("o_form_invisible");
            $("#cart_number input:nth-child(6)").removeClass("o_form_invisible");
            $("#cart_number input:nth-child(7)").removeClass("o_form_invisible");
            $("#cart_number input:nth-child(6)").focus();
        }
        else if($(this).val().length > 3){
            $("#cart_number input:nth-child(3)").removeClass("o_form_invisible");
            $("#cart_number input:nth-child(4)").removeClass("o_form_invisible");
            $("#cart_number input:nth-child(5)").removeClass("o_form_invisible");
            $("#cart_number input:nth-child(6)").addClass("o_form_invisible");
            $("#cart_number input:nth-child(7)").addClass("o_form_invisible");
            $(this).next().focus();
        }
    });

    $("body").on('keyup', "#cart_number input:nth-child(3), #cart_number input:nth-child(4), #cart_number input:nth-child(5), #cart_number input:nth-child(6)", function(e){
        if($(this).val().length > 3){
            $(this).next().focus();
        }
    });

    $("body").on('keyup', "#cvc input", function(e){
        if($(this).val().length > 2){
             $("#cart_number input").focus();
        }
    });
    $("input[name=sec]").each(function(index){

                if ($(this).is(':checked')){
                    $(this).prop('checked',true);
                    //console.log ('2', self );
                    //console.log ('2',this );
                    //self.view.__parentedParent.__parentedParent.field.views.tree.fields.secim[ index ].set_value( true );
                }
                
            });
       /*
    $("body").on('click', "#geridon1", function(){
        $("#kartListesi").css("display","none");
        $("#ode").css("display","block");
        $("#ozet").css("display","block");
        $("#odemeinfo").css("display","block");
    });
    */
    $("body").on('click', "#geridon2", function(){
        $("#kartListesi").css("display","block");
        $("#ode").css("display","none");
        $("#ozet").css("display","block");
        $("#odemeinfo").css("display","none");
        $("#geridon2").css("display","none");
        $("#go_to_pay").css("display","block");
    });

    
    $("body").on('click', "#go_to_pay", function(){
        if ($('input[name=sec]:checked').length) {
           $("#kartListesi").css("display","none");
            $("#ode").css("display","block");
            $("#ozet").css("display","block");
            $("#odemeinfo").css("display","none");
            $("#geridon2").css("display","block");
            $("#go_to_pay").css("display","none");
        }
        else {
            alert('En Az Bir Ödeme Seçeneği Belirleyiniz.');
        }
        
    });


});

function openTextWindows(href){
    var w = (window.innerWidth  - 800)/2;
    var h = (window.innerHeight - 600)/2;
    window.open(href, 'targetWindow', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=800, height=600, top='+w+', left=' + h);
    return false;
}