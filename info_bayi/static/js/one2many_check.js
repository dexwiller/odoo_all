
odoo.define('web_one2many_selectable.form_widgets', function (require) {
	"use strict";

	var core = require('web.core');
	var form_common = require('web.form_common');
	var _t = core._t;
	var QWeb = core.qweb;
	var Model = require('web.Model');
	var FieldOne2Many = core.form_widget_registry.get('one2many');
	var ListView      = require('web.ListView');
	var list_widget_registry = core.list_widget_registry;
	var widgets = require('web.form_widgets');
	var session = require('web.session');
	var data = require('web.data');
	
	var teknikServisTechDeclineButton =  widgets.WidgetButton.extend({
		on_click: function() {
			var self = this;
			//var parentView = self.view.ViewManager.__parentedParent.__parentedParent;
			self._super.apply(this, arguments);
			while (1){
				console.log('While');
				if (self.view.dataset.ids != []){
					console.log('Break');	
					break;
				}
			}
			var m       = new Model('mrp.repair');
				m.call("tech_decline", self.view.dataset.ids,self.view.dataset.context)
					.then(function(result){
					self.view.do_action( result );
					$('.close').click();
				});
		}
    });
	var ProgVersChangeConfirmButton = widgets.WidgetButton.extend({
		on_click: function() {
			var self = this;
			var res = self._super.apply(this, arguments);
			
			console.log( res );
			
			while (1){
				console.log('While');
				if (self.view.dataset.ids != []){
					console.log('Break');	
					break;
				}
			}
			
			setTimeout(function(){
				var m       = new Model('info_tsm.change_version_confirm');
				
				m.call("sub", self.view.dataset.ids,self.view.dataset.context)
					.then(function(result){
					console.log ( 'Result : ' ,result );
					var a = {
							type: 'ir.actions.act_window',
							name:'Değişen Versiyon',
							res_model: 'info_tsm.program_version',
							view_mode:"form",
							view_type:"form",
							views: [[false, 'form']],
							target:"self",
							res_id:result
							};
					console.log(a);
					console.log( self );
					self.do_action(a);
					$('.close').click();
				});							
				} ,1500);//this.view.do_hide();
		}
    });
	var ProgVersUpSaveButton = widgets.WidgetButton.extend({
		on_click: function() {
			var self = this;
			var res = self._super.apply(this, arguments);
			
			console.log( res );
			
			while (1){
				console.log('While');
				if (self.view.dataset.ids != []){
					console.log('Break');	
					break;
				}
			}
			
			setTimeout(function(){
				console.log( self );
				console.log( self.view );
				var m       = new Model('info_tsm.vers_okc_wiz');
				
				m.call("okc_terminal_grup_set", self.view.dataset.ids,self.view.dataset.context)
					.then(function(result){
					console.log ( 'Result : ' ,result );
					$('.close').click();
				});							
				} ,1500);//this.view.do_hide();
		}
    });
	var ProgVersButton = widgets.WidgetButton.extend({
		on_click: function() {
			var ids =[];
			$('input[name=radiogroup]:checked').closest('tr').each( function(){
				ids.push(parseInt($(this).context.dataset.id));
			});
			var a = {
					type: 'ir.actions.act_window',
					name:'Program Versiyon Ayarları',
					src_model :'res.partner',
					res_model: 'info_tsm.vers_okc_wiz',
					view_mode:"form",
					view_type:"form",
					views: [[false, 'form']],
					key2:'client_action_multi',
					target:"new",
					context:{'ids':ids,'types':['01','03','07','08']},
				};
			if (ids.length === 0)
				{
					this.do_warn(_t("You must choose at least one record."));
					return false;
				}
			this.view.do_action(a);
		}
    });
	var InfoteksAyarButton = widgets.WidgetButton.extend({
		on_click: function() {
			var ids =[];
			$('input[name=radiogroup]:checked').closest('tr').each( function(){
				ids.push(parseInt($(this).context.dataset.id));
			});
			var a = {
					type: 'ir.actions.act_window',
					name:'İnfoteks Ayarları',
					res_model: 'info_tsm.params_okc_wiz',
					view_mode:"form",
					view_type:"form",
					views: [[false, 'form']],
					key2:'client_action_multi',
					target:"new",
					context:{'ids':ids,'hide_senkron_fields':true,'hide_bayi_fields':true,'hide_infoteks_fields':false},
				};
			if (ids.length === 0)
				{
					this.do_warn(_t("You must choose at least one record."));
					return false;
				}
			this.view.do_action(a);
		}
    });
	var BayiAyarButton = widgets.WidgetButton.extend({
		on_click: function() {
			var ids =[];
			$('input[name=radiogroup]:checked').closest('tr').each( function(){
				ids.push(parseInt($(this).context.dataset.id));
			});
			var a = {
					type: 'ir.actions.act_window',
					name:'Bayi Ayarları',
					res_model: 'info_tsm.params_okc_wiz',
					view_mode:"form",
					view_type:"form",
					views: [[false, 'form']],
					key2:'client_action_multi',
					target:"new",
					context:{'ids':ids,'hide_senkron_fields':true,'hide_infoteks_fields':true,'hide_bayi_fields':false},
				};
			if (ids.length === 0)
				{
					this.do_warn(_t("You must choose at least one record."));
					return false;
				}
			this.view.do_action(a);
		}
    });
	var SenkronAyarButton = widgets.WidgetButton.extend({
		on_click: function() {
			var ids =[];
			$('input[name=radiogroup]:checked').closest('tr').each( function(){
				ids.push(parseInt($(this).context.dataset.id));
			});
			var a = {
					type: 'ir.actions.act_window',
					name:'Cihaz Ayarları',
					res_model: 'info_tsm.params_okc_wiz',
					view_mode:"form",
					view_type:"form",
					views: [[false, 'form']],
					key2:'client_action_multi',
					target:"new",
					context:{'ids':ids,'hide_senkron_fields':false,'hide_bayi_fields':true,'hide_infoteks_fields':true},
				};
			if (ids.length === 0)
				{
					this.do_warn(_t("You must choose at least one record."));
					return false;
				}
			this.view.do_action(a);
		}
    });
	var UrunParamsButton = widgets.WidgetButton.extend({

		on_click: function() {
			var self=this;
			var ids =[];
			$('input[name=radiogroup]:checked').closest('tr').each( function(){
				ids.push(parseInt($(this).context.dataset.id));
			});
			var m       = new Model('stock.production.lot');
            m.call("get_urun_params_view_id", [])
			.then(function(result){
				var a = {
					type: 'ir.actions.act_window',
					name:'Parametre Tablosu',
					res_model: 'info_tsm.okc_urun_grup',
					view_mode:"form",
					view_type:"form",
					views: [[ result.view_id, 'form']],
					key2:'client_action_multi',
					target:"new",
					context:{'ids':ids,'active_model':'stock.production.lot'},
				};
				if (ids.length === 0)
					{
						self.do_warn(_t("You must choose at least one record."));
						return false;
					}
				self.view.do_action(a);
				});
		}
    });
	var BankaVersButton = widgets.WidgetButton.extend({
		on_click: function() {
			var ids =[];
			$('input[name=radiogroup]:checked').closest('tr').each( function(){
				ids.push(parseInt($(this).context.dataset.id));
			});
			var a = {
					type: 'ir.actions.act_window',
					name:'Banka Versiyon Ayarları',
					src_model :'res.partner',
					res_model: 'info_tsm.vers_okc_wiz',
					view_mode:"form",
					view_type:"form",
					views: [[false, 'form']],
					key2:'client_action_multi',
					target:"new",
					context:{'ids':ids,'types':['05']},
				};
			if (ids.length === 0)
				{
					this.do_warn(_t("You must choose at least one record."));
					return false;
				}
			this.view.do_action(a);
		}
    });
	var YemekVersButton = widgets.WidgetButton.extend({
		on_click: function() {
			var ids =[];
			$('input[name=radiogroup]:checked').closest('tr').each( function(){
				ids.push(parseInt($(this).context.dataset.id));
			});
			var a = {
					type: 'ir.actions.act_window',
					name:'Yemek Kartı Versiyon Ayarları',
					src_model :'res.partner',
					res_model: 'info_tsm.vers_okc_wiz',
					view_mode:"form",
					view_type:"form",
					views: [[false, 'form']],
					key2:'client_action_multi',
					target:"new",
					context:{'ids':ids,'types':['11']},
				};
			if (ids.length === 0)
				{
					this.do_warn(_t("You must choose at least one record."));
					return false;
				}
			this.view.do_action(a);
		}
    });
	var PartiVersButton = widgets.WidgetButton.extend({
		on_click: function() {
			var ids =[];
			$('input[name=radiogroup]:checked').closest('tr').each( function(){
				ids.push(parseInt($(this).context.dataset.id));
			});
			var a = {
					type: 'ir.actions.act_window',
					name:'3.Parti Versiyon Ayarları',
					src_model :'res.partner',
					res_model: 'info_tsm.vers_okc_wiz',
					view_mode:"form",
					view_type:"form",
					views: [[false, 'form']],
					key2:'client_action_multi',
					target:"new",
					context:{'ids':ids,'types':['09']},
				};
			if (ids.length === 0)
				{
					this.do_warn(_t("You must choose at least one record."));
					return false;
				}
			this.view.do_action(a);
		}
    });
	var musTedDetayEkstraYazdir = widgets.WidgetButton.extend({
		
		on_click: function() {
			var self = this;
			var selectedOption = $("#extre_method").val();
			var	model = new Model('res.partner');
			var dataset = this.view.dataset;
			var active_id = dataset.ids[dataset.index];
			model.call("musTedDetayEkstraYazdir", [active_id], {
				selectedOption:selectedOption
			})
			.then(function(result){
				self.view.do_action( result );
			});
			/*
			var a = {
					type: 'ir.actions.act_window',
					name:'3.Parti Versiyon Ayarları',
					src_model :'res.partner',
					res_model: 'info_tsm.vers_okc_wiz',
					view_mode:"form",
					view_type:"form",
					views: [[false, 'form']],
					key2:'client_action_multi',
					target:"new",
					context:{'ids':ids,'types':['09']},
				};
			if (ids.length === 0)
				{
					this.do_warn(_t("You must choose at least one record."));
					return false;
				}
			this.view.do_action(a);
			*/
		}
    });
	
	core.form_tag_registry.add('prog_vers_button',     ProgVersButton);
	core.form_tag_registry.add('infoteks_ayar_button', InfoteksAyarButton);
	core.form_tag_registry.add('bayi_ayar_button', 	   BayiAyarButton);
	core.form_tag_registry.add('senkron_ayar_button',  SenkronAyarButton);
	core.form_tag_registry.add('urun_params_button',   UrunParamsButton);
	core.form_tag_registry.add('banka_vers_button',    BankaVersButton);
	core.form_tag_registry.add('yemek_vers_button',    YemekVersButton);
	core.form_tag_registry.add('parti_vers_button',    PartiVersButton);
	core.form_tag_registry.add('extre_button',    	   musTedDetayEkstraYazdir);
	core.form_tag_registry.add('prog_vers_up_save_button', ProgVersUpSaveButton);
	core.form_tag_registry.add('tec_decline_button', teknikServisTechDeclineButton);
	core.form_tag_registry.add('prog_vers_change_confirm_button',ProgVersChangeConfirmButton);
    /*
	ListView.Column.include({
		init: function(id, tag, attrs) {
			this._super(id, tag, attrs);
			if (this.widget == 'inline_selection_button') {
				this.use_selection_button = true;
			}
		},
		_format: function (row_data, options) {
			if (this.widget == 'inline_selection_button') {
				var value  = row_data[this.id].value;
				var name   = value;
				var values = {
					row_id: row_data.id.value,
					name: name,
					title:name,
					model:name
				};
				return _.str.sprintf(
					'<button row_id="%(row_id)s" data-wizard-clickable-model="%(model)s" title="%(title)s"><i class="fa fa-search"></i></button>',values
				);
			}
			else {
			    return this._super(row_data, options);
			}
        }
	});

	ListView.List.include({
		render: function() {
			var result = this._super(this, arguments),
            self = this;
			this.$current.delegate('button[data-wizard-clickable-model]',
				'click', function() {
					console.log($(this).data('wizard-clickable-model'));

					var a = {
						type: 'ir.actions.act_window',
						src_model :'res.partner',
						res_model: $(this).data('wizard-clickable-model'),
						res_id:false,
						view_mode:"form",
						view_type:"form",
						views: [[false, 'form']],
						key2:'client_action_multi',
						target:"new",
						context:{'id':this.id},

					};
					console.log( a );
					console.log( self.view );

					self.view.do_action(a);
				});
			return result;
		},

	});

	list_widget_registry.add('field.inline_selection_button', ListView.Column);
	*/
	var One2ManySelectable = FieldOne2Many.extend({

		multi_selection: true,

		start: function()
	    {
	    	this._super.apply(this, arguments);
			var self=this;
		   },
	});
	core.form_widget_registry.add('one2many_selectable', One2ManySelectable);

});