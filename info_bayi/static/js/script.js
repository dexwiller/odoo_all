//-*- coding: utf-8 -*-
odoo.define('info_helpdesk.video_options', function (require) {
    "use strict";
    var core             = require('web.core');
    var rpc              = require('web.rpc');
    require('web.dom_ready');
    //var CrashManager = require('web.CrashManager');
    //var render_last = 1;
    //var render_last_2 = 1;
    var videolistesi = ["MP4","OOG","WEBM","MOV"];
    var doclistesi   = {'JPG':'img','JPEG':'img','XLS':'object','XLSX':'object','DOC':'object','DOCX':'object','PNG':'img','PDF':'object'};
    /*
    core.form_widget_registry.map.binary.include({
        init: function(field_manager, node) {
            var self = this;
            var don = self._super(field_manager, node);
            this.max_upload_size = 45 * 1024 * 1024; // 50Mb
            return don;
        },
    });*/
    var framework = require('web.framework');
    var widgetRegistry = require('web.widget_registry');
    var Widget = require('web.Widget');
    
    var _t = core._t;
    
    var VideoPlayer = Widget.extend({
        template: 'info_helpdesk.video_player',
        events: {
            
        },
        init: function () {
            this._super.apply(this, arguments);
        },
        xmlDependencies: ['/info_helpdesk/static/xml/templates.xml'],
        start: function () {
            rpc.query({
                model: 'info_helpdesk.video',
                method: 'get_url_header',
                args: [[]],
                kwargs: this.__parentedParent.state.context
            }).then(function (result) {
                var url = result.url + "/" + result.path;
                $("video source").attr("src", url);
                $(this).addClass('selected');
                $("video").load();
                $("video").trigger('play');
                $('video').on('loadstart', function (event) {
                    $(this).addClass('video_loading');
                    $(this).attr('poster', '/info_helpdesk/static/img/loading.gif');
                  });
                $('video').on('canplay', function (event) {
                  $(this).removeClass('video_loading');
                  $(this).attr('poster', '');
                });
                $('video').bind('ended', function(){
                    $(this).addClass('video_loading');
                    $(this).attr('poster', '/info_helpdesk/static/img/loading.gif');
                    
                    var next = $('.selected').next();
                    var current = $('.selected');
                    $("video source").attr({
                            "src": $('.selected').next().attr("movieurl"),
                            "poster": "",
                            "autoplay": "autoplay"
                        });
                        $("video").load();
                        $("video").trigger('play');
                        
                        current.css( "color", "white" );
                        next.css("color","yellow");
                        next.addClass('selected');
                        current.removeClass( 'selected' );

                });
                
                for (var p=0;p<result.playlist.length;p++){
                    var p_url   =  result.url + "/" + result.playlist [p];
                    var element  =  '<li movieurl=' + p_url + '>' + result.names[p] + '</li>';
                    if (url.localeCompare(p_url) == 0){
                        element = '<li movieurl=' + p_url + ' style="color:yellow;" class="selected">' + result.names[p] + '</li>'; }
                    $("#playlist").append( element );
                }
                $(function() {
                    $("#playlist").on("click","li", function() {
                        $("video source").attr({
                            "src": $(this).attr("movieurl"),
                            "poster": "",
                            "autoplay": "autoplay"
                        });
                        $("video").load();
                        $("video").trigger('play');
                        $(this).siblings().css( "color", "white" );
                        $(this).siblings().removeClass( 'selected' );
                        $(this).css("color","yellow");
                        $(this).addClass('selected');
                    });
                });
                
            });
            return this._super.apply(this, arguments);
        },
        
        destroy: function () {
            this._super.apply(this, arguments);
        },
    
    
    });
    widgetRegistry.add('video_player', VideoPlayer);
    /*  
        FormController.extend({
            init : function (parent, model, renderer, params) {
                console.log('Burada');
                //try{
                    var self = this;
                    this._super.apply(this, arguments);
                    console.log(self);
                    if(self.model == 'info_helpdesk.video') {
                        console.log(self);
                        
    
                        var m = new Model('info_helpdesk.video');
                        m.call("get_url_header",[],self.dataset.context).then(function(result){
                            var url = result.url + "/" + self.datarecord.path;
                            $("video source").attr("src", url);
                            $(this).addClass('selected');
                            $("video").load();
                            $("video").trigger('play');
                            $('video').on('loadstart', function (event) {
                                $(this).addClass('video_loading');
                                $(this).attr('poster', '/info_helpdesk/static/img/loading.gif');
                              });
                            $('video').on('canplay', function (event) {
                              $(this).removeClass('video_loading');
                              $(this).attr('poster', '');
                            });
                            $('video').bind('ended', function(){
                                $(this).addClass('video_loading');
                                $(this).attr('poster', '/info_helpdesk/static/img/loading.gif');
                                
                                var next = $('.selected').next();
                                var current = $('.selected');
                                $("video source").attr({
                                        "src": $('.selected').next().attr("movieurl"),
                                        "poster": "",
                                        "autoplay": "autoplay"
                                    });
                                    $("video").load();
                                    $("video").trigger('play');
                                    
                                    current.css( "color", "white" );
                                    next.css("color","yellow");
                                    next.addClass('selected');
                                    current.removeClass( 'selected' );
     
                            });
                            
                            for (var p=0;p<result.playlist.length;p++){
                                var p_url   =  result.url + "/" + result.playlist [p];
                                var element  =  '<li movieurl=' + p_url + '>' + result.names[p] + '</li>';
                                if (url.localeCompare(p_url) == 0){
                                    element = '<li movieurl=' + p_url + ' style="color:yellow;" class="selected">' + result.names[p] + '</li>'; }
                                $("#playlist").append( element );
                            }
                            $(function() {
                                $("#playlist").on("click","li", function() {
                                    $("video source").attr({
                                        "src": $(this).attr("movieurl"),
                                        "poster": "",
                                        "autoplay": "autoplay"
                                    });
                                    $("video").load();
                                    $("video").trigger('play');
                                    $(this).siblings().css( "color", "white" );
                                    $(this).siblings().removeClass( 'selected' );
                                    $(this).css("color","yellow");
                                    $(this).addClass('selected');
                                });
                            });
                            //$(".o_cp_left").css("display", "none");
                            //$(".o_cp_sidebar").css("display", "none");
                        });
                    }
                    else if(self.model == 'info_helpdesk.kilavuz' ) {
                        var m2 = new Model(self.model);
                        if (data.id){
                            m2.call("get_fileb64",[ data.id ],{}).then( function(result){
                                var b64         = result.b64;
                                var file_suffix = data.dosyaadi.split(".");
                                file_suffix.reverse();
                                var tag = doclistesi [file_suffix[0].toUpperCase()];
                                
                                if (tag){
                                    if (tag.localeCompare('img') == 0){
                                        $("<img>", {
                                            "src": "data:image/png;base64," + b64,
                                            "width": "100%", "height": "100%"})
                                          .appendTo("#preview");
                                    }
                                    else if ( file_suffix[0].toUpperCase().localeCompare('PDF') == 0 && tag.localeCompare('object') == 0){
                                        var objbuilder = '';
                                            objbuilder += ('<object width="100%" height="100%"      data="data:application/pdf;base64,');
                                            objbuilder += (b64);
                                            objbuilder += ('" type="application/pdf" class="internal">');
                                            objbuilder += ('<embed src="data:application/pdf;base64,');
                                            objbuilder += (b64);
                                            objbuilder += ('" type="application/pdf" />');
                                            objbuilder += ('</object>');
                                            
                                            $( objbuilder ).appendTo('#preview');
                                    }
                                    else self.do_notify("Dikkat","Doküman Görüntülenemiyor", false);
                                }
                                else self.do_notify("Dikkat!", "Dosya Tipi Tanımlanamadı...", false);
                            });
                        }
                    }
                    return don;
                //}
                //catch(e){
                //    console.log('Error : ',e);
                //}
            },
            do_onchange : function (data) {
                var self = this;
                var don = self._super(data);
                if(self.model == 'info_helpdesk.video') {
                    
                    if (data) {
                        if (data.name == "dosya") {
                           
                            var dosyauzantitespit = $('.aaa:last').val();
                            console.log ( data );
                            dosyauzantitespit = dosyauzantitespit.split(".");
                            dosyauzantitespit.reverse();
                            
                            if (videolistesi.indexOf(dosyauzantitespit[0].toUpperCase()) == -1) {
                                self.do_notify("Dikkat!", "Hatalı uzantı. mp4, ogg, webm, mov uzantılarını yükleyebilirsiniz...", false);
                                self.fields.dosya.on_clear();
    
                            }
                        }
                    }
                }
    
                return don;
            }
        });
    /*
    CrashManager.include({
        show_message:function(exception){
            console.log ('Axception Overrided !!!!!');
            if ( !exception.includes('ResizeObserver')){
                this.super( exception );
            }
        }
    });
    */
});


