# -*- coding: utf-8 -*-
from openerp import models, fields, api, exceptions
class tahsil_wizard(models.TransientModel):
    _name    = 'info_check.tahsil_wizard'
    #bank    = fields.Many2one('bkm.acquirer', string='Encash Bank' , required=True)
    bank   = fields.Char(string='Encash Bank', required=True)

    def _default_check(self):
        return self.env['info_check.check'].browse(self._context.get('active_id'))
    def _default_domain(self ):
        company_id =  self.env['res.users'].browse( self._uid )[0].company_id.id
        types      = ['bank','cash']

        return [('type','in',types),('company_id','=',company_id)]

    check_id = fields.Many2one('info_check.check',
        string="Check", required=True, default=_default_check)

    journal_id               = fields.Many2one('account.journal', string='Account Journal', domain = _default_domain,
                                               required=True)

    @api.one
    def save( self ):
        self.check_id.state = 'inthsl'
        self.check_id.t_bank_name = self.bank
        self.check_id.t_journal_id = self.journal_id.id

        return True
class teminat_wizard(models.TransientModel):
    _name    = 'info_check.teminat_wizard'
    #bank    = fields.Many2one('bkm.acquirer', string='Deposit Bank' , required=True)
    bank   = fields.Char(string='Encash Bank', required=True)

    def _default_check(self):
        return self.env['info_check.check'].browse(self._context.get('active_id'))
    def _default_domain(self ):
        company_id =  self.env['res.users'].browse( self._uid )[0].company_id.id
        types      = ['bank','purchase']

        return [('type','in',types),('company_id','=',company_id)]

    check_id = fields.Many2one('info_check.check',
        string="Check", required=True, default=_default_check)

    journal_id               = fields.Many2one('account.journal', string='Account Journal', domain = _default_domain,
                                               required=True)

    @api.one
    def save( self ):
        self.check_id.state = 'teminat'
        self.check_id.t_bank_name = self.bank
        self.check_id.t_journal_id = self.journal_id.id

        return True