# -*- coding: utf-8 -*-
from odoo import models, fields, api,exceptions
import json
import copy
from datetime import datetime
class dummy:
	pass
	#payment like object to create counter payments... niye ingilişce yazdım lan bunu.
class res_partner_bank(models.Model):
	_inherit = 'res.partner.bank'

	bayiden_odeme_alir = fields.Boolean(string='Bayi Ödemelerinde Kullanılabilir.')

	@api.multi
	def name_get(self):
		res = []
		for record in self:
			bank_name = ''
			if record.bank_id.name:
				bank_name = record.bank_id.name
			name = bank_name + u'-' + record.sanitized_acc_number
			res.append((record['id'],name ))
		return res

class res_bank(models.Model):
	_inherit = 'res.bank'

	bkm      = fields.Many2one('info_extensions.acquirer',string='BKM Banka')

	@api.multi
	@api.onchange('bkm')
	def bkm_onchange(self):
		self.ensure_one()
		if self.bkm:
			self.bic  = self.bkm.bkm_id
			self.name = self.bkm.name

class account_move_line(models.Model):
	_inherit = 'account.move.line'
	
	@api.model
	def compute_amount_fields(self, amount, src_currency, company_currency, invoice_currency=False):
		""" Helper function to compute value for fields debit/credit/amount_currency based on an amount and the currencies given in parameter"""
		amount_currency = False
		currency_id = False
		if src_currency:
			amount_currency  = amount
			currency_id      = src_currency.id
			if src_currency != company_currency:
				amount = src_currency.with_context(self._context).compute(amount, company_currency)
				
		debit = amount > 0 and amount or 0.0
		credit = amount < 0 and -amount or 0.0
		if invoice_currency and invoice_currency != company_currency and not amount_currency:
			amount_currency = src_currency.with_context(self._context).compute(amount, invoice_currency)
			currency_id = invoice_currency.id
		
		##print debit, credit, amount_currency, currency_id
		return debit, credit, amount_currency, currency_id
	
class account_journal(models.Model):
	_inherit='account.journal'
	kk      			  = fields.Boolean('Kredi Kartı İle Ödeme Ekranlarını Aç')
	bayiden_odeme_alir    = fields.Boolean(string='Bayi Ödemeleri (KK ve Havale/Eft) için Kullanılabilir.')
	check   		      = fields.Boolean('Çek Ödemeleri Bu Deftere İşlenir.')
	komisyon_defteri 	  = fields.Boolean('KK Banka Komisyonları Bu Deftere İşlenir.')
	komisyon_fark_defteri = fields.Boolean('KK Banka Komisyon Farkları  Bu Deftere İşlenir.')

	@api.model
	def create(self, values ):
		res = super(account_journal, self ).create( values )
		if res.bank_account_id:
			res.bank_account_id.bayiden_odeme_alir = res.bayiden_odeme_alir

		return res

	@api.multi
	def write(self, values ):
		self.ensure_one()
		res = super(account_journal, self ).write( values )
		if 'bayiden_odeme_alir' in values and self.bank_account_id:
			self.bank_account_id.bayiden_odeme_alir = values['bayiden_odeme_alir']

		return res

class info_kk_payment_temp_payment( models.Model):
	
	print (' Loading  --------------------------- Temp Payment')
	
	_name = 'info_kk_payment.temp_payment'
	_order = 'id desc'
	#companyid odeme yapılan şirket... buna bağlı rule olursa, herkeskendi şirketine ait ödemeyi görür.
	partner_id          = fields.Many2one('res.partner','Ödeme Yapan Cari')

	mahsup_senaryo      = fields.Selection(selection=[ (1,'Tüm Tutarı İnfoteks İle Mahsuplaştır'),
													   (2,'Tutarı İnfoteks ve Ana Bayi ile Borç Durumuna Göre Mahsuplaştır'),
													   (3,'Tutarı Sadece Ana Bayi İle Mahsuplaştır.')],string="Senaryo")
	#senaryo seçimi sadece infoteks için gererli, diğer ödeme mahsuplaşması sadece ana bayi ve bayi arasında....1 yani
	payment             = fields.Many2one('account.payment',string='Kaynak Ödeme')
	odeme_parcalari     = fields.One2many('account.payment','temp_odeme',string='Ödeme Parçaları',domain=[('payment_type','=','inbound')])
	odeme_parcalari_v   = fields.Boolean(compute='odeme_parcasi_var_mi')
	journal_id          = fields.Many2one('account.journal',string='Ödeme Methodu')
	company_id          = fields.Many2one('res.company',string='Şirket')
	amount              = fields.Monetary(string='Tutar')
	currency_id         = fields.Many2one('res.currency',string='Para Birimi')
	payment_date        = fields.Date('Ödeme Zamanı')
	state               = fields.Selection(selection=[('draft'     ,'Onay Bekliyor'),
													  ('confirmed' ,'Onaylandı'),
													  ('cancel'    ,'İptal'),
													  ('added'     ,'Ödeme İşlendi')],default='draft')
	current_company_id  = fields.Many2one('res.company',compute='_get_current_company')
	lead_partner_id     = fields.Many2one('res.partner',string="Ödeme Alınan Cari")
	
	source_journal      = fields.Many2one('account.journal',related='payment.journal_id') 



	@api.model
	def _get_current_company( self ):
		self.current_company_id = self.env.user.company_id.id

	@api.multi
	@api.depends('odeme_parcalari')
	def odeme_parcasi_var_mi( self ):
		if len( self.odeme_parcalari ) > 0:
			self.odeme_parcalari_v = True
	'''
	@api.multi
	def unlink(self):
		for s in self:
			if s.state != 'draft':
				raise exceptions.ValidationError( "Onaylanmış Bir Ödeme Silinemez !!!")
			for p in s.odeme_parcalari :
				if p.state == 'draft':
					p.sudo().unlink()
				else:
					raise exceptions.ValidationError( "Onaylanmış Bir Ödeme Silinemez !!!")
		return super( account_payment , self ).unlink()
	'''
	
		
		
		
		
	@api.multi
	def onayla(self):
		self.ensure_one()

		payment          = self.payment
		##print 'Payment. kk_odeme:', payment.kk_odeme
		senaryo = self.mahsup_senaryo
		if self.env.user.company_id.id != 1:
			senaryo = 1
		if not senaryo:
			raise exceptions.ValidationError('Lütfen Mahsuplaştırma Senaryosu Seçiniz.')

		if senaryo   == 1:#self.company_id ye çak bütün ödemeyi.
			karsilik = self.odeme_karsilik_olustur( payment )
			self.state = 'added'
			self.add_payment_to_invoices( self.env.user.company_id )
			for c in self.tedarik_check:
				c.sudo().company_id          = self.env.user.company_id.id
				c.incoming_payment_id = karsilik.id
				c.state               = 'port'

		elif senaryo == 2:
			#uretilecek payment hep customer olacak... once bize borcu varmı? acik fatura residuallerden bak...
			infoteks_borcu        = self.env['account.invoice'].search([('partner_id','=',self.partner_id.id),
																('company_id','=',self.env.user.company_id.id),
																('state','=','open')])
			alt_bayi  =  self.env['res.company'].sudo().search([('partner_id','=',self.partner_id.id)])

			toplam_infoteks_borcu = sum(infoteks_borcu.mapped('residual'))
			#ödeme tüm borcu kapatıyor mu?
			odeme_kalan = self.amount - toplam_infoteks_borcu

			if toplam_infoteks_borcu > 0:
				#biz direk fatura kesmişiz..
				#önce bizim faturaları kapa, sonra varsa ana bayiye olan borcu kapa,
				#sonra bu tutar kadar ana bayinin bize olan borcunu kapa
				if self.amount >= toplam_infoteks_borcu:

					#yeni tutarla odeme dummy...
					info_odeme_like                         = dummy()
					info_odeme_like.amount                  = toplam_infoteks_borcu
					info_odeme_like.payment_date            = payment.payment_date
					info_odeme_like.currency_id             = payment.currency_id
					info_odeme_like.odeme_yapilacak_cari    = payment.odeme_yapilacak_cari
					info_odeme_like.journal_id              = payment.journal_id
					info_odeme_like.payment_method_id       = payment.payment_method_id
					info_odeme_like.partner_id              = self.partner_id
					info_odeme_like.company_id              = self.env.user.company_id
					info_odeme_like.banka                   = payment.banka
					info_odeme_like.outgoing_info_check     = payment.outgoing_info_check
					info_odeme_like.kk_odeme                = payment.kk_odeme
					info_odeme_like.dummy                   = True
					##print 'İnfoteks Borcu Kapanıyor...'
					karsilik                                = self.odeme_karsilik_olustur( info_odeme_like )
					for c in self.tedarik_check:
						c.sudo().company_id          = self.env.user.company_id.id
						c.incoming_payment_id = karsilik.id
						c.state               = 'port'


				else:
					#ödeme yetersiz, tüm parayı biz alalım nihohoahhaha
					#yani senaryo 1...
					##print 'İnfoteks PAranın Tamamını alıyor'
					#komisyon olabilir.
					info_odeme_like                         = dummy()
					info_odeme_like.amount                  = self.amount
					info_odeme_like.payment_date            = payment.payment_date
					info_odeme_like.currency_id             = payment.currency_id
					info_odeme_like.odeme_yapilacak_cari    = payment.odeme_yapilacak_cari
					info_odeme_like.journal_id              = payment.journal_id
					info_odeme_like.payment_method_id       = payment.payment_method_id
					info_odeme_like.partner_id              = payment.partner_id
					info_odeme_like.company_id              = payment.company_id
					info_odeme_like.banka                   = payment.banka
					info_odeme_like.outgoing_info_check     = payment.outgoing_info_check
					info_odeme_like.kk_odeme                = payment.kk_odeme
					
					karsilik = self.odeme_karsilik_olustur( info_odeme_like )
					for c in self.tedarik_check:
						c.sudo().company_id          = self.env.user.company_id.id
						c.incoming_payment_id = karsilik.id
						c.state               = 'port'

			#kalan tutarla ana bayi dummy....ana bayinin bize borcu, varsa elemanın felan...while olur burda...
			if odeme_kalan > 0:
				##print 'While İçinde'

				#bu herifin tedarikçisine olan borcuna bak...
				ana_bayi_borcu        = self.env['account.invoice'].sudo().search([('partner_id','=',self.partner_id.id),
																('company_id','=',alt_bayi.parent_id.id),
																('state','=','open')])
				toplam_ana_bayi_borcu = sum(ana_bayi_borcu.mapped('residual'))
				#ödeme tüm borcu kapatıyor mu?
				alt_odeme_kalan = odeme_kalan - toplam_ana_bayi_borcu

				if not alt_bayi:
					raise exceptions.ValidationError (u'Ödeme Yapacak Cari Bulunamadı...')

				if alt_odeme_kalan >= 0:
					mahsup_tutar = toplam_ana_bayi_borcu
					#yeni tutarla odeme dummy...

					kalan_alacak_comp                            = self.env['res.company'].sudo().search([('partner_id','=',payment.partner_id.id)])
					son_kalan_odeme_like                         = dummy()
					son_kalan_odeme_like.amount                  = alt_odeme_kalan
					son_kalan_odeme_like.payment_date            = payment.payment_date
					son_kalan_odeme_like.currency_id             = payment.currency_id
					son_kalan_odeme_like.odeme_yapilacak_cari    = payment.odeme_yapilacak_cari
					son_kalan_odeme_like.journal_id              = payment.journal_id
					son_kalan_odeme_like.payment_method_id       = payment.payment_method_id
					son_kalan_odeme_like.partner_id              = self.partner_id
					son_kalan_odeme_like.company_id              = kalan_alacak_comp
					son_kalan_odeme_like.banka                   = payment.banka
					son_kalan_odeme_like.outgoing_info_check     = payment.outgoing_info_check
					son_kalan_odeme_like.kk_odeme                = payment.kk_odeme
					son_kalan_odeme_like.dummy                   = True
					##print 'Son Kalan Borcu Kapanıyor...'
					karsilik                                = self.odeme_karsilik_olustur( son_kalan_odeme_like )

				else:
					mahsup_tutar = odeme_kalan
				if mahsup_tutar > 0:
					while alt_bayi.parent_id:
						bayi_odeme_like                         = dummy()
						bayi_odeme_like.amount                  = mahsup_tutar
						bayi_odeme_like.payment_date            = payment.payment_date
						bayi_odeme_like.currency_id             = payment.currency_id
						bayi_odeme_like.odeme_yapilacak_cari    = payment.odeme_yapilacak_cari
						bayi_odeme_like.journal_id              = payment.journal_id
						bayi_odeme_like.payment_method_id       = payment.payment_method_id
						bayi_odeme_like.dummy                   = True
						bayi_odeme_like.partner_id              = alt_bayi.partner_id
						bayi_odeme_like.company_id              = alt_bayi.parent_id
						bayi_odeme_like.banka                   = payment.banka
						bayi_odeme_like.outgoing_info_check     = payment.outgoing_info_check
						bayi_odeme_like.kk_odeme                = payment.kk_odeme

						karsilik                                = self.odeme_karsilik_olustur( bayi_odeme_like )
						if alt_bayi.parent_id.id == 1:
							for c in self.tedarik_check:
								c.sudo().company_id          = self.env.user.company_id.id
								c.incoming_payment_id = karsilik.id
								c.state               = 'port'

						alt_bayi = alt_bayi.parent_id
			alt_bayi  =  self.env['res.company'].sudo().search([('partner_id','=',self.partner_id.id)])
			self.state = 'added'
			while alt_bayi.parent_id:
				self.add_payment_to_invoices( alt_bayi.parent_id )#tum ust bayiler için fatura fasilitesini çalıştır....
				alt_bayi = alt_bayi.parent_id

		elif senaryo == 3:
			#tüm parayı biz de alsak alt cari borcuyla kapa, artanı kimi seçtiyse ona yaz.
			alt_bayi  =  self.env['res.company'].sudo().search([('partner_id','=',self.partner_id.id)])
			#bu herifin tedarikçisine olan borcuna bak...
			ana_bayi_borcu        = self.env['account.invoice'].sudo().search([('partner_id','=',self.partner_id.id),
															('company_id','=',alt_bayi.parent_id.id),
															('state','=','open')])
			toplam_ana_bayi_borcu = sum(ana_bayi_borcu.mapped('residual'))
			#ödeme tüm borcu kapatıyor mu?
			alt_odeme_kalan = self.amount - toplam_ana_bayi_borcu

			if not alt_bayi:
				raise exceptions.ValidationError (u'Ödeme Yapacak Cari Bulunamadı...')

			if alt_odeme_kalan >= 0:
				mahsup_tutar = toplam_ana_bayi_borcu
				#yeni tutarla odeme dummy...

				kalan_alacak_comp                            = self.env['res.company'].sudo().search([('partner_id','=',payment.partner_id.id)])
				son_kalan_odeme_like                         = dummy()
				son_kalan_odeme_like.amount                  = alt_odeme_kalan
				son_kalan_odeme_like.payment_date            = payment.payment_date
				son_kalan_odeme_like.currency_id             = payment.currency_id
				son_kalan_odeme_like.odeme_yapilacak_cari    = payment.odeme_yapilacak_cari
				son_kalan_odeme_like.journal_id              = payment.journal_id
				son_kalan_odeme_like.payment_method_id       = payment.payment_method_id
				son_kalan_odeme_like.partner_id              = self.partner_id
				son_kalan_odeme_like.company_id              = kalan_alacak_comp
				son_kalan_odeme_like.banka                   = payment.banka
				son_kalan_odeme_like.outgoing_info_check     = payment.outgoing_info_check
				son_kalan_odeme_like.kk_odeme               = payment.kk_odeme
				son_kalan_odeme_like.dummy                   = True
				##print 'Son Kalan Borcu Kapanıyor...'
				karsilik                                = self.odeme_karsilik_olustur( son_kalan_odeme_like )

			else:
				mahsup_tutar = self.amount
			if mahsup_tutar > 0:
				while alt_bayi.parent_id:
					bayi_odeme_like                         = dummy()
					bayi_odeme_like.amount                  = mahsup_tutar
					bayi_odeme_like.payment_date            = payment.payment_date
					bayi_odeme_like.currency_id             = payment.currency_id
					bayi_odeme_like.odeme_yapilacak_cari    = payment.odeme_yapilacak_cari
					bayi_odeme_like.journal_id              = payment.journal_id
					bayi_odeme_like.payment_method_id       = payment.payment_method_id
					bayi_odeme_like.dummy                   = True
					bayi_odeme_like.partner_id              = alt_bayi.partner_id
					bayi_odeme_like.company_id              = alt_bayi.parent_id
					bayi_odeme_like.banka                   = payment.banka
					bayi_odeme_like.outgoing_info_check     = payment.outgoing_info_check
					bayi_odeme_like.kk_odeme                = payment.kk_odeme

					karsilik                                = self.odeme_karsilik_olustur( bayi_odeme_like )
					if alt_bayi.parent_id.id == 1:
						for c in self.tedarik_check:
							c.sudo().company_id          = self.env.user.company_id.id
							c.incoming_payment_id = karsilik.id
							c.state               = 'port'

					alt_bayi = alt_bayi.parent_id
			alt_bayi  =  self.env['res.company'].sudo().search([('partner_id','=',self.partner_id.id)])
			self.state = 'added'
			while alt_bayi.parent_id:
				self.add_payment_to_invoices( alt_bayi.parent_id )#tum ust bayiler için fatura fasilitesini çalıştır....
				alt_bayi = alt_bayi.parent_id


		intercompany_uid = payment.company_id.intercompany_user_id and payment.company_id.intercompany_user_id.id or False
		if not intercompany_uid:
			raise exceptions.Warning(_('Provide one user for intercompany relation for % ') % payment.company_id.name)
		if payment.sudo( intercompany_uid).state == 'draft':
			payment.sudo( intercompany_uid).post()
		#infoteks faturalari ve kendi faturaları... her senaryonun altına yapalım.


	def add_payment_to_invoices( self,company_id ):

		open_invoices = self.env['account.invoice'].sudo().search([('partner_id','=',self.partner_id.id),
																		('state','=','open'),
																		('company_id','=',company_id.id)],
																		order = 'id asc')


		for oi in open_invoices:
			aml_ids = json.loads (oi.outstanding_credits_debits_widget)
			if aml_ids:
				for ml in aml_ids.get ('content'):
					if oi.state == 'open':
						if not self.env['account.move.line'].browse([ ml['id']]).reconciled:
							oi.assign_outstanding_credit (ml['id'])
					else:
						break

		self.state = 'added'

	def odeme_karsilik_olustur( self, tedarik_odemesi):

		odeme_yapan          = self.partner_id

		if hasattr( tedarik_odemesi, 'dummy'):
			odeme_yapan         = tedarik_odemesi.partner_id
			company_id          = tedarik_odemesi.company_id
		else:
			company_id = self.env['res.company'].sudo().search([('partner_id','=',int(tedarik_odemesi.partner_id.id))])

		if not company_id:
			raise exceptions.ValidationError('Ödeme Yapılacak Üst Cari Bulunamadı.')

		values = dict(partner_id            =   odeme_yapan.id,
					  payment_type          =   "inbound",
					  partner_type          =   "customer",
					  amount                =   tedarik_odemesi.amount,
					  currency_id           =   tedarik_odemesi.currency_id.id,
					  payment_date          =   tedarik_odemesi.payment_date,
					  odeme_yapilacak_cari  =   'kasa',
					  company_id            =   company_id.id,
					  karsilik              =   True,
					  )

		journal_domain = [('type','=',tedarik_odemesi.journal_id.type),('company_id','=',company_id.id)]
		if tedarik_odemesi.banka:
			journal_domain.append ( ('bank_account_id','=',tedarik_odemesi.banka.id) )
		elif tedarik_odemesi.kk_odeme:

			banka = self.env['res.bank'].sudo().search([('bkm','=',tedarik_odemesi.kk_odeme.banka_id.id)])
			if banka:
				res_partner_bank = self.env['res.partner.bank'].sudo().search([('bank_id','=',banka.id),
																				('bayiden_odeme_alir','=',True)])
				if res_partner_bank:
					journal_domain.append ( ('bank_account_id','=',res_partner_bank.id) )

		elif tedarik_odemesi.outgoing_info_check:
				journal_domain.append(('check','=',True))

		karsilik_journal_id =  self.env['account.journal'].sudo().search( journal_domain )

		##print  'Journal Domain = ' , journal_domain

		karsilik_journal_id =  self.env['account.journal'].sudo().search( journal_domain )

		if len(karsilik_journal_id) == 0:
			#bankasız tekrar dene... her bayinin banka hesabı olmayavbilir...
			bankasiz_domain     =  [('type','=',tedarik_odemesi.journal_id.type),('company_id','=',company_id.id)]
			karsilik_journal_id =  self.env['account.journal'].sudo().search( bankasiz_domain )
			if len(karsilik_journal_id) == 0:
				raise exceptions.ValidationError(u'Seçilen Ödeme Tipine Ait Defter, %s Muhasebesinde Bulunmuyor. %s Muhasebe Birimi İle Görüşünüz.!!!'%(company_id.name,company_id.name))
		payment_method =  self.env['account.payment.method'].sudo().search([('code','=',tedarik_odemesi.payment_method_id.code),
																			('payment_type','=','inbound')])
		if len(payment_method) == 0:
			raise exceptions.ValidationError(u'Seçilen Ödeme Tipine Ait Ödeme Methodu, %s Muhasebesinde Bulunmuyor. %s Muhasebe Birimi İle Görüşünüz.!!!'%(company_id.name,company_id.name))
		values ['journal_id']               = karsilik_journal_id[0].id
		values ['journal_id_domained']      = karsilik_journal_id[0].id
		values ['payment_method_id']        = payment_method[0].id

		#print 'Values : ',values
		c_u = self.env.user.id
		#print 'Curren User : ',c_u
		if values['company_id'] == 1:
			c_u = 1
		else:
			c_u = self.env['res.users'].search([('company_id','=',values['company_id'])])[0].id
		karsilik = self.env['account.payment'].sudo( c_u ).create( values )#aman dikkat recursive olmasın
		self.odeme_parcalari = [(4,karsilik.id)]

		##print 'Payment Currency :',karsilik.currency_id
		if karsilik.state == 'draft':
			karsilik.sudo( c_u ).post()

		return karsilik

class account_payment( models.Model ):
	_inherit = 'account.payment'
	temp_odeme = fields.Many2one('info_kk_payment.temp_payment', string='Onay Kalemi')
	state      = fields.Selection(selection_add=[('kk_odeme','Kredi Kartı Ödemesi'),
												 ('waiting_confirm','Onay Bekliyor.')])
	karsilik   = fields.Boolean()
	@api.model
	def get_journal_domain( self ):
		domain = [('at_least_one_outbound','=',True),
					('type', 'in', ('bank', 'cash')),
					('company_id','=',self.env.user.company_id.id)]
		return domain

	@api.model
	def _get_inv_sended_partner_default( self ):
		domained_supps    = []
		p_id = self.env.user.company_id.sudo()
		while p_id:
			domained_supps.append( p_id.partner_id.id )
			p_id = p_id.parent_id

		return json.dumps([('id','not in',domained_supps)])

	

	@api.model
	def _get_current_user_infoteks_mi_def(self):
		if self.env.user.company_id.id == 1:
			return True
		return False
	@api.model
	def _get_current_payment_partner(self):
		if self.env.user.company_id.id == 1:
			return 'kasa'
		return False

	def _create_payment_entry(self, amount):
		""" Create a journal entry corresponding to a payment, if the payment references invoice(s) they are reconciled.
			Return the journal entry.
		"""
		##print 1
		aml_obj = self.env['account.move.line'].with_context(check_move_validity=False)
		invoice_currency = False
		if self.invoice_ids and all([x.currency_id == self.invoice_ids[0].currency_id for x in self.invoice_ids]):
			#if all the invoices selected share the same currency, record the paiement in that currency too
			invoice_currency = self.invoice_ids[0].currency_id
		##print 2
		##print self.currency_id
		debit, credit, amount_currency, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(amount, self.currency_id, self.company_id.currency_id, invoice_currency)
		##print currency_id
		move = self.env['account.move'].create(self._get_move_vals())
		##print 3
		#Write line corresponding to invoice payment
		counterpart_aml_dict = self._get_shared_move_line_vals(debit, credit, amount_currency, move.id, False)
		##print 3.5
		counterpart_aml_dict.update(self._get_counterpart_move_line_vals(self.invoice_ids))
		##print 3.6
		counterpart_aml_dict.update({'currency_id': currency_id})
		##print 3.7
		##print counterpart_aml_dict
		counterpart_aml = aml_obj.create(counterpart_aml_dict)
		##print 4
		#Reconcile with the invoices
		if self.payment_difference_handling == 'reconcile' and self.payment_difference:
			writeoff_line = self._get_shared_move_line_vals(0, 0, 0, move.id, False)
			amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(self.payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)[2:]
			# the writeoff debit and credit must be computed from the invoice residual in company currency
			# minus the payment amount in company currency, and not from the payment difference in the payment currency
			# to avoid loss of precision during the currency rate computations. See revision 20935462a0cabeb45480ce70114ff2f4e91eaf79 for a detailed example.
			total_residual_company_signed = sum(invoice.residual_company_signed for invoice in self.invoice_ids)
			total_payment_company_signed = self.currency_id.with_context(date=self.payment_date).compute(self.amount, self.company_id.currency_id)
			if self.invoice_ids[0].type in ['in_invoice', 'out_refund']:
				amount_wo = total_payment_company_signed - total_residual_company_signed
			else:
				amount_wo = total_residual_company_signed - total_payment_company_signed
			# Align the sign of the secondary currency writeoff amount with the sign of the writeoff
			# amount in the company currency
			if amount_wo > 0:
				debit_wo = amount_wo
				credit_wo = 0.0
				amount_currency_wo = abs(amount_currency_wo)
			else:
				debit_wo = 0.0
				credit_wo = -amount_wo
				amount_currency_wo = -abs(amount_currency_wo)
			writeoff_line['name'] = _('Counterpart')
			writeoff_line['account_id'] = self.writeoff_account_id.id
			writeoff_line['debit'] = debit_wo
			writeoff_line['credit'] = credit_wo
			writeoff_line['amount_currency'] = amount_currency_wo
			writeoff_line['currency_id'] = currency_id
			writeoff_line = aml_obj.create(writeoff_line)
			if counterpart_aml['debit']:
				counterpart_aml['debit'] += credit_wo - debit_wo
			if counterpart_aml['credit']:
				counterpart_aml['credit'] += debit_wo - credit_wo
			counterpart_aml['amount_currency'] -= amount_currency_wo
		self.invoice_ids.register_payment(counterpart_aml)

		#Write counterpart lines
		'''
		if not self.currency_id != self.company_id.currency_id:
			amount_currency = 0
		'''
		liquidity_aml_dict = self._get_shared_move_line_vals(credit, debit, -amount_currency, move.id, False)
		liquidity_aml_dict.update(self._get_liquidity_move_line_vals(-amount))
		liquidity_aml_dict.update({'currency_id': currency_id})
		##print liquidity_aml_dict
		aml_obj.create(liquidity_aml_dict)

		move.post()
		return move
	
	journal_id_domained = fields.Many2one('account.journal', string='Ödeme Methodu', required=True, domain=get_journal_domain)
	bayi_karsilik       = fields.Many2one('account.payment',string='Üst Cari Karşılığı')
	buttonShow          = fields.Boolean(compute="butonGorunsunMu")
	bankashow           = fields.Boolean(compute="bankaGorunsunMu")
	banka               = fields.Many2one('res.partner.bank',string='Ödeme Yapılan Banka')
	inv_sended_payment  = fields.Many2one('account.invoice',string='Gönderilen Fatura')
	inv_sended_partner  = fields.Many2one('res.partner')
	inv_partner         = fields.Many2one('res.partner')
	inv_partner_domain = fields.Char(compute="_get_inv_sended_partner_domain",default=_get_inv_sended_partner_default)
	
	current_user_inoteks_mi= fields.Boolean(compute="_get_current_user_infoteks_mi", default = _get_current_user_infoteks_mi_def)


	@api.one
	def _get_current_user_infoteks_mi(self):
		self.current_user_inoteks_mi = False
		if self.env.user.company_id.id == 1:
			self.current_user_inoteks_mi =  True
			self.odeme_yapilacak_cari = 'kasa'

	@api.one
	def _get_inv_sended_partner_domain( self ):
		domained_supps    = []
		p_id = self.env.user.company_id.sudo()
		while p_id:
			domained_supps.append( p_id.partner_id.id )
			p_id = p_id.parent_id

		self.inv_partner_domain= json.dumps([('id','not in',domained_supps)])

	@api.one
	@api.depends('journal_id')
	def butonGorunsunMu(self):
		if self.journal_id.kk:
			self.buttonShow = True
		else:
			self.buttonShow = False

	api.one
	def _get_lead_bayi_mi(self):
		lead = False
		if self.env.user.company_id.partner_id.lead_bayi:
			lead = True
		self.lead_bayi_mi = lead

	@api.one
	@api.depends('journal_id','partner_id','odeme_yapilacak_cari')
	def bankaGorunsunMu(self):

		self.bankashow = False
		if self.odeme_yapilacak_cari != 'kasa':
			if self.journal_id.type == 'bank' and int(self.odeme_yapilacak_cari) == 1 and not self.journal_id.kk and not self.journal_id.check:
				self.bankashow = True

	@api.model
	def _get_odeme_parent( self ):

		parent = self.env.user.sudo().company_id.parent_id

		selection = [('kasa','Kendi Kasama Kaydet (Ödeme Tedarikçiye Gönderilmez.)')]

		while parent:
			selection.append((str(parent.partner_id.id),parent.partner_id.name))
			parent = parent.parent_id

		return selection

	odeme_yapilacak_cari = fields.Selection(selection=_get_odeme_parent,string="Ödeme Yapılacak Cari",default=_get_current_payment_partner)
	kk_odeme             = fields.Many2one('info_kk_payment.odeme_yap')
	kk_komisyon_dahil    = fields.Boolean('Banka Komisyonunu Tutara Dahil Et')

	@api.onchange( 'odeme_yapilacak_cari' )
	def odeme_yapilacak_cari_onchange( self ):

		self.journal_id_domained = False
		self.check_odemesi_mi    = False
		self.buttonShow          = False
		domain                   = {}
		
		journal_domain = [('at_least_one_outbound','=',True),
					('type', 'in', ('bank', 'cash')),
					('company_id','=',self.env.user.company_id.id)]
		
		
		if self.odeme_yapilacak_cari and self.odeme_yapilacak_cari != 'kasa':

			if not self.has_invoices:
				self.partner_id         = int(self.odeme_yapilacak_cari)
				self.payment_type ='outbound'
				self.partner_type ='supplier'
			else:
				self.inv_sended_partner = int(self.odeme_yapilacak_cari)
				self.inv_partner        = self.invoice_ids[0].partner_id.id
				##print 'İnv Partner : ' , self.inv_partner
			
			if int(self.odeme_yapilacak_cari) != 1:
				journal_domain = [('at_least_one_outbound','=',True),
									('company_id','=',self.env.user.company_id.id),
									('type', 'in', ('bank', 'cash')),
									('kk','=',False)
								]
			
			domain = {'journal_id_domained':journal_domain,
				'partner_id':[('supplier','=',True)]}
		else:
			if not self.has_invoices:
				self.partner_id   = False

			if self.env.user.company_id.id != 1:
				self.payment_type ='inbound'
				self.partner_type ='customer'
				domained_supps    = []
				p_id = self.env.user.company_id.sudo()
				while p_id:
					domained_supps.append( p_id.partner_id.id )
					p_id = p_id.parent_id

				journal_domain = [('at_least_one_outbound','=',True),('company_id','=',self.env.user.company_id.id),
					'|',('check','=',True),('type', '=', 'cash')]

				domain = {'journal_id_domained':journal_domain,'partner_id':[('customer','=',True),('id','not in',domained_supps)]}
			else:
				if self.partner_type == 'supplier':
					#sadec kendi supps
					partner_dom = [('supplier','=',True),
						('id','!=',self.env.user.company_id.id),
						('company_id','=',self.env.user.company_id.id)]
				elif self.partner_type == 'customer':
					comp_ids    = [self.env.user.company_id.id]
					comp_ids.extend( self.env.user.company_id.child_ids.ids )
					partner_dom = [('customer','=',True),
						('id','!=',self.env.user.company_id.id),
						('company_id','in',comp_ids)]
				
				domain = {'journal_id_domained':journal_domain,
						  'partner_id':partner_dom}

		return {
			'domain':domain
		}

	def _onchange_partner_type(self):

		res = super( account_payment, self)._onchange_partner_type()
		##print res


	@api.onchange( 'journal_id_domained' )
	def journal_id_domained_onchange( self ):
		if self.journal_id_domained:
			self.journal_id = self.journal_id_domained.id

	@api.model
	def default_get(self,field_list):

		res = super( account_payment, self).default_get( field_list )
		if not res.get('invoice_ids'):
			if self.env.user.company_id.id != 1:
				for sec in  self._get_odeme_parent():
					if sec[0] == '1':
						res['odeme_yapilacak_cari'] = '1'
						break

				res['partner_id']   = 1
				res['payment_type'] = 'outbound'
				res['partner_type'] = 'supplier'
			else:
				res['odeme_yapilacak_cari'] = 'kasa'

		##print 'REEES  : ',self._context
		return res
	
	@api.multi
	def kk_odeme_yap( self ):

		self.ensure_one()
		view_id = self.env['ir.ui.view'].search([('name','=','info_kk_payment.odeme.form.wizard')]).id
		if not self.temp_odeme.id:
			pass

		if self.journal_id.kk:
			return {
				'type': 'ir.actions.act_window',
				'name': 'Kredi Kartı Ödeme',
				'res_model': 'info_kk_payment.odeme_yap',
				'src_model': 'account.payment',
				'view_mode': 'form',
				'view_type':'form',
				'views': [[view_id, "form"]],
				'target': 'new',
				'context':{'default_tutar':self.amount,
						   'default_partner_id':self.env['res.company'].sudo().browse([1]).partner_id.id,
						   'default_temp_payment':self.temp_odeme.id,
						   'search_default_group_by_resim':1,
						   'default_payment':[(4,self.id)]
						   },
				}
	@api.multi
	def post(self):
		p = super( account_payment, self).post()
		
		return p

	@api.one
	def odeme_kasa_olustur(self):
		## kullanilmiyor....
		if self.inv_partner and self.has_invoices:
			odeme_yapan          = self.inv_partner
			company_id           = self.env.user.company_id.id
			values = dict(partner_id            =   odeme_yapan.id,
						  payment_type          =   "inbound",
						  partner_type          =   "customer",
						  amount                =   self.amount,
						  currency_id           =   self.currency_id.id,
						  payment_date          =   self.payment_date,
						  odeme_yapilacak_cari  =   'kasa',
						  company_id            =   company_id.id,
						  karsilik              =   True,
						  journal_id            =   self.journal_id,
						  state                 =   'waiting_confirm',
						  payment_method        =  self.env['account.payment.method'].search([('code','=',self.payment_method_id.code),('payment_type','=','inbound')])
						  )

			self.create( values )

	def odeme_temp_olustur( self, tedarik_odemesi):

		if tedarik_odemesi.has_invoices:
			#tedarik_odemesi.post()
			return True

		if tedarik_odemesi.odeme_yapilacak_cari != 'kasa':

			values = dict(partner_id      = self.env.user.company_id.sudo().partner_id.id,
						  amount          = tedarik_odemesi.amount,
						  currency_id     = tedarik_odemesi.currency_id.id,
						  payment_date    = tedarik_odemesi.payment_date,
						  payment         = tedarik_odemesi.id,
						  lead_partner_id = tedarik_odemesi.inv_partner.id
						  )

			company_id = self.env['res.company'].sudo().search([('partner_id','=',int(tedarik_odemesi.odeme_yapilacak_cari))])
			if not company_id:
				raise exceptions.ValidationError('Ödeme Yapılacak Üst Cari Bulunamadı.')



			journal_domain = [('type','=',tedarik_odemesi.journal_id.type),('company_id','=',company_id.id)]
			if tedarik_odemesi.banka:
				journal_domain.append ( ('bank_account_id','=',tedarik_odemesi.banka.id) )
			elif tedarik_odemesi.outgoing_info_check:
				journal_domain.append(('check','=',True))


			karsilik_journal_id =  self.env['account.journal'].sudo().search( journal_domain )


			if len(karsilik_journal_id) == 0:
				raise exceptions.ValidationError(u'Seçilen Ödeme Tipine Ait Defter, %s Muhasebesinde Bulunmuyor. %s Muhasebe Birimi İle Görüşünüz.!!!'%(company_id.name,company_id.name))
			payment_method =  self.env['account.payment.method'].sudo().search([('code','=',tedarik_odemesi.payment_method_id.code),
																				('payment_type','=','inbound')])
			if len(payment_method) == 0:
				raise exceptions.ValidationError(u'Seçilen Ödeme Tipine Ait Ödeme Methodu, %s Muhasebesinde Bulunmuyor. %s Muhasebe Birimi İle Görüşünüz.!!!'%(company_id.name,company_id.name))
			values ['journal_id']               = karsilik_journal_id[0].id
			values ['company_id']               = company_id.id

			temp = self.env['info_kk_payment.temp_payment'].sudo().create( values )#aman dikkat recursive olmasın
			tedarik_odemesi.temp_odeme = temp.id
			for c in tedarik_odemesi.outgoing_info_check:
				c.temp_payment_id   = temp.id
				c.allowed_companies = [(4,company_id.id)]

		else:
			##print 'Last Post'
			intercompany_uid = tedarik_odemesi.company_id.intercompany_user_id and tedarik_odemesi.company_id.intercompany_user_id.id or False
			if not intercompany_uid:
				raise exceptions.Warning(_('Provide one user for intercompany relation for % ') % tedarik_odemesi.company_id.name)

			tedarik_odemesi.sudo( intercompany_uid ).post()
			if not tedarik_odemesi.karsilik:
				tedarik_odemesi.add_payment_to_invoices (self.env.user.company_id )

		if tedarik_odemesi.inv_sended_payment:
			tedarik_odemesi.inv_sended_payment.state = 'send_to_supp'

	@api.multi
	def cancel(self):

		for s in self:
			if s.sudo().temp_odeme:
				if s.sudo().temp_odeme.state=='draft':
					s.sudo().temp_odeme.state='cancel'
				else:
					raise exceptions.ValidationError( "Onaylanmış Bir Ödeme İptal Edilemez. !!!")
		return super( account_payment, self).cancel()

	@api.model
	def create(self,values):
		res = super( account_payment, self).create( values )
		if self.env.user.company_id.id != 1:
			self.odeme_temp_olustur( res )
		return res

	@api.multi
	def unlink(self):
		for s in self:
			if s.sudo().temp_odeme:
				if s.sudo().temp_odeme.state=='draft':
					s.sudo().temp_odeme.unlink()
				else:
					raise exceptions.ValidationError( "Onaylanmış Bir Ödeme Silinemez !!!")
		return super( account_payment , self ).unlink()

	@api.multi
	def write(self,values):
		res = super( account_payment, self).write( values )
		for s in self:
			if s.sudo().temp_odeme:
				if values.get( 'amount'):
					if s.sudo().temp_odeme.state=='draft':
						s.sudo().temp_odeme.amount = values.get('amount')
					else:
						raise exceptions.ValidationError( "Onaylanmış Bir Ödeme Değiştirilemez. !!!")
		return res

	def add_payment_to_invoices( self,company_id ):

		open_invoices = self.env['account.invoice'].sudo().search([('partner_id','=',self.partner_id.id),
																		('state','=','open'),
																		('company_id','=',company_id.id)],
																		order = 'id asc')

		for oi in open_invoices:
			aml_ids = json.loads (oi.outstanding_credits_debits_widget)
			if aml_ids:
				for ml in aml_ids.get ('content'):
					if oi.state == 'open':
						if not self.env['account.move.line'].browse([ ml['id']]).reconciled:
							oi.assign_outstanding_credit (ml['id'])
					else:
						break

	@api.multi
	def post_and_send_payment(self):
		self.ensure_one()

		if self.odeme_yapilacak_cari != 'kasa':
			supplier_payment = self.copy(
				{
					'payment_type':'outbound',
					'partner_type':'supplier',
					'partner_id':self.odeme_yapilacak_cari,
					'invoice_ids':False,
				}
			)

		self.post()
		if self.odeme_yapilacak_cari != 'kasa':

			if self.env['account.invoice'].browse([self._context['active_id']]).state == 'paid':
				self.env['account.invoice'].browse([self._context['active_id']]).state = 'sended_to_supp'
