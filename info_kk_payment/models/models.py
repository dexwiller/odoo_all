# -*- coding: utf-8 -*-
from _ast import Str
from datetime import datetime as dt
from datetime import timedelta as tdel
from dateutil.relativedelta import relativedelta
import time
from hashlib import sha1
import base64
import requests
from difflib import SequenceMatcher as SM
from odoo import models, fields, api, exceptions,registry
from odoo.tools.translate import _
import xml.etree.ElementTree as ET
import random as rd
import logging
import pytz,json
from PIL import Image
#from .webserviceopt import b2b_partner_sync, b2b_payment_sync

URETICI_FIRMA_ADI = 'INFOTEKS BIL. ELK. TELE. MED. REK.  ITH. IHR. SAN.'

from requests.adapters import HTTPAdapter
#from requests.packages.urllib3.poolmanager import PoolManager
from urllib3.poolmanager import PoolManager
from .webservice import b2b_partner_sync, b2b_payment_sync
import ssl
class stock_production_lot(models.Model):
    _inherit = 'stock.production.lot'

class res_partner(models.Model):
    _inherit = 'res.partner'

    b2b_id = fields.Integer()
    okc_names = fields.Char(string='Kullanılan Okc Cihazları')

    @api.multi
    def b2b_senkron(self):
        b2b_partner_sync(self)

class product(models.Model):

    _inherit = 'product.template'

    b2b_id = fields.Integer()

class TLSv1_2Adapter(HTTPAdapter):
    def init_poolmanager(self, connections, maxsize, block=False):
        self.poolmanager = PoolManager(num_pools=connections, maxsize=maxsize, block=block, ssl_version=ssl.PROTOCOL_TLSv1_2)

class taksit_oranlari ( models.Model ):
    _name = 'info_kk_payment.taksit_oranlari'

    kart			= fields.Many2one('info_kk_payment.kart_tipleri',string='Kart Tipi')
    name			= fields.Char( string = 'Ödeme Şekli', store=True, compute = '_get_proper_name')
    taksit_sayisi	= fields.Float(string = 'Taksit Sayısı', size=2,digits=(2,0) )
    taksit_orani	= fields.Float(string = 'Komisyon Oranı', size=4 ,digits=(2,2) )
    komisyon_orani	= fields.Float(string = 'Gerçek Komisyon Oranı', size=4 ,digits=(2,2) )
    name_temp		= fields.Char( string = 'Ödeme Şekli')
    tutar_by_taksit_oranlari = fields.One2many('info_kk_payment.tutar_by_taksit_oranlari', 'taksit_oranlari')
    resim 			= fields.Binary(string='Kart Resmi', related='kart.resim')
    is_active 		= fields.Boolean(string='Taksit Aktif mi', default=True)
    tek_cekim       = fields.Boolean()
    banka_ek_taksit = fields.Integer(string="Banka Ek Taksitleri")

    @api.one
    @api.depends('taksit_sayisi','taksit_orani')
    def _get_proper_name(self, ):
        if self.taksit_sayisi or self.taksit_orani:
            if float(self.taksit_sayisi) == 0.0 or float(self.taksit_sayisi) == 1.0:
                taksit_name ='Tek Çekim'
            else:
                if self.banka_ek_taksit:
                    taksit_name = str(int(self.taksit_sayisi) + self.banka_ek_taksit) +' ( '+str(int(self.taksit_sayisi)) +'+' + str(self.banka_ek_taksit) +' ) ' + ' Taksit '
                else:
                    taksit_name = str(self.taksit_sayisi) + ' Taksit '
            self.name = '%s %s %%%s'%( self.kart.name, taksit_name, self.taksit_orani )
        else:
            self.name = self.name_temp

    @api.onchange('taksit_sayisi','taksit_orani')
    def _get_proper_name_onchange(self, ):
        ###print self.taksit_sayisi, self.taksit_sayisi
        if self.taksit_sayisi or self.taksit_orani:
            if float(self.taksit_sayisi) == 0:
                taksit_name = ' Tek Çekim '.decode('utf-8')
            else:
                if self.banka_ek_taksit:
                    taksit_name = str(int(self.taksit_sayisi) + self.banka_ek_taksit) +' ( '+str(int(self.taksit_sayisi)) +'+' + str(self.banka_ek_taksit) +' ) ' + ' Taksit '
                else:
                    taksit_name = str(self.taksit_sayisi) + ' Taksit '
            self.name = '%s %s %%%s'%( self.kart.name, taksit_name, self.taksit_orani )

class kart_tipleri ( models.Model ):
    _name  = 'info_kk_payment.kart_tipleri'

    name   = fields.Char(string='Kart Tip Adı',required=True)
    banka  = fields.Many2many('info_kk_payment.acquirer',string="Banka Adı",required=True)
    taksit_oranlari = fields.One2many('info_kk_payment.taksit_oranlari','kart',string='Taksit Oranları', domain=[('is_active','=',True)])
    tum_banka_taksit_oranlari = fields.One2many('info_kk_payment.taksit_by_banka','kart',string='Tüm Banka Taksit Oranları')
    resim = fields.Binary(string='Resim', required=True)
    kod   = fields.Integer('Sanal Pos Id')

    @api.model
    def create(self, values ):

        res = super(kart_tipleri,self).create( values )
        self._cr.execute('''select t1.*, t2.banka, t2.komisyon_orani,t2.banka_ek_taksit from
                         (select min(taksit_orani) as min_tax,taksit_sayisi, kart from info_kk_payment_taksit_by_banka group by taksit_sayisi, kart) t1
                         inner join info_kk_payment_taksit_by_banka t2 on t1.min_tax = t2.taksit_orani
                         and t1.taksit_sayisi = t2.taksit_sayisi and t1.kart = t2.kart order by t1.kart desc;''')
        result = self.env.cr.fetchall()
        self._cr.execute('update info_kk_payment_taksit_oranlari set is_active = false')

        new_taksit_sayisi = 0
        new_taksit_orani  = 0
        new_komisyon      = 0
        tek_cekim         = True

        #self._cr.execute('''insert into info_kk_payment_taksit_oranlari (name, taksit_sayisi, taksit_orani, komisyon_orani, tek_cekim) values ('Tek Çekim', 0, 0, 0, TRUE)''')

        self.env['info_kk_payment.taksit_oranlari'].create({'name_temp': '_',
                                                      'taksit_sayisi': new_taksit_sayisi,
                                                      'taksit_orani': new_taksit_orani,
                                                      'komisyon_orani': new_komisyon,
                                                      'tek_cekim': tek_cekim})

        for rslt in result:

            new_kart 			= rslt[2]
            new_taksit_sayisi 	= rslt[1]
            new_taksit_orani 	= rslt[0]
            new_komisyon 		= rslt[4]
            new_banka_taksit 	= rslt[5]
            self.env['info_kk_payment.taksit_oranlari'].create({'kart': new_kart,
                                                          'taksit_sayisi': new_taksit_sayisi,
                                                          'taksit_orani': new_taksit_orani,
                                                          'komisyon_orani': new_komisyon,
                                                          'banka_ek_taksit': new_banka_taksit})

        return res

    @api.multi
    def write(self, values):
        self.ensure_one()
        res = super(kart_tipleri, self).write(values)
        self._cr.execute('''select t1.*, t2.banka, t2.komisyon_orani,t2.banka_ek_taksit from
                         (select min(taksit_orani) as min_tax,taksit_sayisi, kart from info_kk_payment_taksit_by_banka group by taksit_sayisi, kart) t1
                         inner join info_kk_payment_taksit_by_banka t2 on t1.min_tax = t2.taksit_orani
                         and t1.taksit_sayisi = t2.taksit_sayisi and t1.kart = t2.kart order by t1.kart desc;''')
        result = self.env.cr.fetchall()
        self._cr.execute('update info_kk_payment_taksit_oranlari set is_active = false')

        new_taksit_sayisi = 0
        new_taksit_orani = 0
        new_komisyon = 0
        tek_cekim = True

        # self._cr.execute('''insert into info_kk_payment_taksit_oranlari (name, taksit_sayisi, taksit_orani, komisyon_orani, tek_cekim) values ('Tek Çekim', 0, 0, 0, TRUE)''')

        self.env['info_kk_payment.taksit_oranlari'].create({'name_temp': '_',
                                                      'taksit_sayisi': new_taksit_sayisi,
                                                      'taksit_orani': new_taksit_orani,
                                                      'komisyon_orani': new_komisyon,
                                                      'tek_cekim': tek_cekim})

        for rslt in result:

            new_kart = rslt[2]
            new_taksit_sayisi = rslt[1]
            new_taksit_orani = rslt[0]
            new_komisyon = rslt[4]
            new_banka_taksit 	= rslt[5]
            self.env['info_kk_payment.taksit_oranlari'].create({'kart': new_kart,
                                                          'taksit_sayisi': new_taksit_sayisi,
                                                          'taksit_orani': new_taksit_orani,
                                                          'komisyon_orani': new_komisyon,
                                                          'banka_ek_taksit': new_banka_taksit})

        return res

    @api.multi
    def unlink(self):
        for s in self:
            for x in s.tum_banka_taksit_oranlari:
                x.unlink()

            for t in s.taksit_oranlari:
                for to in t.tutar_by_taksit_oranlari:
                    to.unlink()
                t.unlink()
        return super(kart_tipleri, self).unlink()

class taksit_by_siparis ( models.Model ):
    _name = 'info_kk_payment.taksit_by_siparis'

    taksit              = fields.Many2one('info_kk_payment.taksit_oranlari', string='Ödeme Şekli')
    siparis_no          = fields.Char()

class tutar_by_taksit_oranlari ( models.TransientModel ):
    _name 			= 'info_kk_payment.tutar_by_taksit_oranlari'
    secim			= fields.Boolean(string='Seç')
    taksit_oranlari = fields.Many2one('info_kk_payment.taksit_oranlari', string='Ödeme Şekli')
    taksit_mik      = fields.Float(related='taksit_oranlari.taksit_sayisi',store="True")
    tutar           = fields.Float(string='Tutar')
    tutar_cekilecek = fields.Float(string = 'Kredi Kartından Çekilecek Tutar')
    odeme           = fields.Many2one('info_kk_payment.odeme_yap')
    resim 			= fields.Binary(string='Kart Resmi', related='taksit_oranlari.resim')
    name            = fields.Char(related='taksit_oranlari.name', store=True)
    tek_cekim       = fields.Boolean()

    _order = 'taksit_oranlari asc,taksit_mik asc'

    @api.multi
    def setSelected(self,siparis_no,komisyon_dahil):
        siparis_no = str(siparis_no).replace('.','')
        self.ensure_one()

        if komisyon_dahil:
            cari_dusulecek  = self.tutar - round( (self.tutar * self.taksit_oranlari.taksit_orani) / 100 ,2)
            komisyon        = round(self.tutar - cari_dusulecek,2)
            gercek_komisyon = round( (self.tutar * self.taksit_oranlari.komisyon_orani) / 100 ,2)


        else:
            cari_dusulecek  = self.tutar
            komisyon        = round(self.tutar_cekilecek - self.tutar,2)
            gercek_komisyon = round( (self.tutar  / ( 100 - self.taksit_oranlari.komisyon_orani ))*100,2) - self.tutar

        kom_fark            = round(gercek_komisyon- komisyon,2)
        taksit_by_siparis   = self.env['info_kk_payment.taksit_by_siparis'].create(
            {'taksit':self.taksit_oranlari.id,
             'siparis_no':siparis_no,
             })

        return { 'taksit': self.taksit_oranlari.id,
                'kart': self.taksit_oranlari.kart.id,
                'komisyon_dahil': self.tutar,
                'komisyon_haric': self.tutar_cekilecek,
                'odeme_yontemi': self.taksit_oranlari.name,
                'komisyon':komisyon,
                'cari_dusulecek':cari_dusulecek,
                'kom_fark':kom_fark}

class bin_data( models.Model):
    _name = 'info_kk_payment.card_bin_data'

    code         = fields.Integer(string='Bin Code', required=True)
    acquirer_id  = fields.Integer(string='Bank Code', required=True)
    bank_name    = fields.Char(string='Bank Name', required=True)
    card_type    = fields.Char(string='Card Name', required=True)
    card_style   = fields.Char(string='Card Style', required=True)

class taksit_by_banka ( models.Model ):
    _name = 'info_kk_payment.taksit_by_banka'

    kart			= fields.Many2one('info_kk_payment.kart_tipleri',string='Kart Tipi')
    banka			= fields.Many2one('info_kk_payment.acquirer',string='Banka Adı')
    taksit_sayisi	= fields.Float(string = 'Taksit Sayısı', size=2,digits=(2,0) )
    taksit_orani	= fields.Float(string = 'Komisyon Oranı', size=4 ,digits=(2,2) )
    komisyon_orani	= fields.Float(string = 'Gerçek Komisyon Oranı', size=4 ,digits=(2,2) )
    banka_ek_taksit = fields.Integer(string="Banka Ek Taksitleri")

    '''
    @api.onchange('taksit_orani')
    @api.multi
    def onchange_taksit_orani( self ):
        self.ensure_one()
        if self.taksit_orani:
            self.komisyon_orani = self.taksit_orani
            '''

    @api.onchange('banka')
    @api.multi
    def banka_onchange(self):
        self.ensure_one()
        #bankalistesi = self.env['info_kk_payment.kart_tipleri'].search( [('id','=',self.id)] )
        if self._context.get ("banka"):
            liste = self._context["banka"][0][2]
            ##print self._context
            ##print self._context["banka"]
            ##print self._context["banka"][0]
            ##print self._context["banka"][0][2]
        else:
            liste = []

        res = {'domain': {'banka': [('id', 'in', liste )]}}
        ##print "deneme ->>>>>>>>>>>>"
        return res

class odeme_kampanya_rel ( models.Model ):
    _name = 'info_kk_payment.odeme_kampanya_rel'

    odeme  = fields.Many2one('info_kk_payment.odeme_yap')
    taksit = fields.Many2one('info_kk_payment.taksit_oranlari')
    taksit_sayisi = fields.Float(related='taksit.taksit_sayisi')
    taksit_orani  = fields.Float(related='taksit.taksit_orani')
    secili = fields.Selection (selection=[(1,'1'),(2,'2')])

    def setSelected(self,cr,uid,model_name,model_id,context=None):
        res = self.browse( cr,uid,[int(model_id)],context)
        return { 'taksit':res.taksit.id, 'kart':res.taksit.kart.id }

class MakbuzReport(models.AbstractModel):
    _name = 'report.info_kk_payment.report_makbuz'

    @api.model
    def render_html(self, docids,data=None):


        ##print data , docids

        report_obj = self.env['report']
        report = report_obj._get_report_from_name('info_kk_payment.report_makbuz')
        obj    = self.env['info_kk_payment.odeme_yap'].browse( docids )
        if len( obj ) > 1:
            raise exceptions.ValidationError("Bir Seferde Bir Makbuz Basabilirsiniz. Lütfen Makbuzları Tek Tek Basınız")
        '''
        if obj.bank_state and obj.bank_state.startswith('Muhasebe'):
            raise exceptions.ValidationError("Sadece Muhasebeleşmiş İşlemlerin Makbuzu Basılabilir.")
        '''
        company      = self.env.user.company_id
        odeme_yapan  = obj.partner_id.sudo()
        bayi_kodu    = ''
        current_date = time.strftime("%d/%m/%Y")
        obj.makbuz = 'Makbuz Basıldı'
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': obj,
            'company':company,
            'partner_id':odeme_yapan,
            'duzenleyen':self.env.user.partner_id.name,
            'current_date':current_date,
        }
        return report_obj.render('info_kk_payment.report_makbuz', docargs)

class odeme_yap( models.Model ):
    _name = 'info_kk_payment.odeme_yap'

    @api.model
    def b2b_entegration(self):
        payment_to_sync = self.search([('bank_state','=','basarili')])
        for p in payment_to_sync:
            if not p.partner_id.b2b_id:
                b2b_partner_sync( p.partner_id )
            else:
                b2b_payment_sync( p )

    @api.multi
    def kk_odeme_yap( self ):

        self.ensure_one()
        view_id = self.env['ir.ui.view'].search([('name','=','info_kk_payment.odeme.form.wizard')]).id


        return {
            'type': 'ir.actions.act_window',
            'name': 'Kredi Kartı Ödeme',
            'res_model': 'info_kk_payment.odeme_yap',
            'src_model': 'account.payment',
            'view_mode': 'form',
            'view_type':'form',
            'views': [[view_id, "form"]],
            'res_id':self.id,
            'target': 'new',
            'context':{'default_tutar':self.tutar,
                       'default_partner_id':self.env['res.company'].sudo().browse([1]).partner_id.id,
                       'search_default_group_by_resim':1
                       },
            }

    def _get_next_oid ( self ):
        obj_sequence = self.env['ir.sequence'].sudo()
        idd = obj_sequence.next_by_code('kk_odeme_yap.sequence')
        ##print "--------------> sipariş no: ", idd
        return idd

    @api.model
    def _get_partner_id_domain(self ):
        user = self.env.user
        return [('company_id','child_of',user.company_id.id),('customer','=',True),('is_company','=',True)]

    @api.model
    def _get_taksit_oranlari( self ):
        tum_taksit_oranlari = self.env['info_kk_payment.taksit_oranlari'].search([('is_active','=',True)], order='name asc')
        ##print tum_taksit_oranlari
        ##print "---------->", self._context

        tutar = 1
        if self._context.get( 'default_tutar'):
            tutar = self._context.get('default_tutar')

        res = []
        for taksit in tum_taksit_oranlari:
            ##print taksit.id
            tutar_cekilecek = round( (tutar  / ( 100 - taksit.taksit_orani ))*100,2)
            obj = self.env['info_kk_payment.tutar_by_taksit_oranlari'].create({
                'taksit_oranlari':taksit.id,
                'tutar':tutar,
                'tutar_cekilecek':tutar_cekilecek,
                'tek_cekim':taksit.tek_cekim
            })
            res.append (obj)

        ##print 'bura_calisti'

        return list(map( lambda x:x.id, res))

    @api.model
    def _get_default_company_id( self ):
        return self.env.user.company_id.id

    @api.model
    def _get_odeme_alan_banka( self ):
        defalut_banka_id = self.env['info_kk_payment.acquirer'].search([('default_banka', '=', True)]).id
        ##print defalut_banka_id
        return defalut_banka_id

    name				= fields.Char()
    name_temp       	= fields.Char('Sipariş Numarası', default=_get_next_oid)
    tutar				= fields.Float(string = 'Tutar',required=True)
    partner_id			= fields.Many2one('res.partner', string='Ödeme Yapan',required=True)
    diger_tutars		= fields.One2many('info_kk_payment.tutar_by_taksit_oranlari', 'odeme', default = _get_taksit_oranlari )
    tutar_cekilecek		= fields.Float(string='Kredi Kartından Çekilecek Tutar')
    komisyon_dahil  	= fields.Boolean()
    kart_sahibi 		= fields.Char(string='Kart Üzerindeki İsim')
    kart_no_1 			= fields.Char(string='Kart No', size=4)
    kart_no_2 			= fields.Char(string='Kart No', size=4)
    kart_no_3 			= fields.Char(string='Kart No', size=4)
    kart_no_4 			= fields.Char(string='Kart No', size=4)
    amex                = fields.Boolean(string='Kart No',compute='_get_Amex')
    amex_2				= fields.Char(string='Kart No', size=6)
    amex_3				= fields.Char(string='Kart No', size=5)
    cvc 				= fields.Char(string='CVC Kodu', size=4)
    card_bin 			= fields.Many2one('info_kk_payment.card_bin_data')
    card_type_ 			= fields.Char(related='card_bin.card_type',  string='Kart Tipi')
    card_bank 			= fields.Char(related='card_bin.bank_name',   string='Kart Bankası')
    card_style 			= fields.Char(related='card_bin.card_style', string='Banka Kart Tipi')
    sk_tarihi_ay 		= fields.Selection(selection=[(1, '01'),(2, '02'),(3, '03'),(4, '04'),(5, '05'),(6, '06'),(7, '07'),(8, '08'),(9, '09'),(10, '10'), (11, '11'),(12, '12'),], string='Son Kullanma Tarihi Ay')
    sk_tarihi_yil 		= fields.Selection(selection=[(17, '17'),(18, '18'),(19, '19'),(20, '20'),(21, '21'),(22, '22'),(23, '23'),(24, '24'),(25, '25'),(26, '26'),], string='Son Kullanma Tarihi Ay')
    taksit_oranlari_ 	= fields.Many2one('info_kk_payment.taksit_oranlari')
    payload				= fields.Text()
    url					= fields.Char()
    url_header			= fields.Char()
    banka_id			= fields.Many2one('info_kk_payment.acquirer')
    bank_state 			= fields.Selection(selection=[('basarisiz', 'Ödeme Başarısız'),
                                                      ('basarili', 'Ödeme Başarılı'),
                                                      ('muhasebe', 'İnfoteks\e Gönderildi')], string='Ödenme Durumu')
    hata_kodu 			= fields.Char('Hata Kodu')
    hata_msg 			= fields.Char('Hata Msg')
    kart_tipleri_ 		= fields.Many2one('info_kk_payment.kart_tipleri')
    company_id      	= fields.Many2one('res.company',string='Ödeme Yapan',default = _get_default_company_id)
    makbuz 				= fields.Char(string="Makbuz Durumu", company_dependent=True, default='' )
    odeme_tipi			= fields.Selection(selection=[('kk','Kredi Kartı'), ('cash','Nakit'), ('bank','Havale / Eft'), ('bank2','Çek'), ('bank3','Mail Order')], default='kk',  string='Ödeme Şekli')
    settle_code      	= fields.Char(string="Banka Gün Sonu No")
    auth_code       	= fields.Char(string="Banka Onay Kodu")
    host_code        	= fields.Char(string="Banka Referans Kodu")
    amount_total_words  = fields.Char(string='Total', store=False, readonly=True, compute='_compute_amount_total_words', track_visibility='always')
    temp_payment        = fields.Many2one('info_kk_payment.temp_payment')
    payment             = fields.One2many('account.payment','kk_odeme')
    bkm_id              = fields.Integer()
    cariden_dusulecek   = fields.Float(string='Cariden Düşülecek Tutar')
    komisyon            = fields.Float(string='Komisyon Tutarı')
    komisyonfarki       = fields.Float(string='Komisyon Farkı')
    api_url             = fields.Char()
    b2b_sync_info    	= fields.Text()
    product_id          = fields.Many2one('info_kk_payment.yemekmatik_kampanya')
    _order = 'id desc'

    @api.one
    @api.depends('kart_no_1')
    def _get_Amex( self ):
        ##print 'GET AMEX'
        if self.kart_no_1:
            if self.kart_no_1.startswith('37') or self.kart_no_1.startswith('34'):
                self.amex = True
            else:
                self.amex = False

    @api.multi
    @api.onchange('komisyon_dahil')
    def _onchange_komisyon_dahil(self):
        ##print self._context
        self.ensure_one()
        for taksit in self.diger_tutars:

            if self.komisyon_dahil:
                tutar_cekilecek = round(self.tutar, 2)
            else:
                tutar_cekilecek = round( (self.tutar  / ( 100 - taksit.taksit_oranlari.taksit_orani ))*100,2)

            taksit.tutar_cekilecek = tutar_cekilecek

        if self.name_temp == 0:
            obj_sequence = self.env['ir.sequence'].sudo()
            self.name_temp = obj_sequence.next_by_code('odeme_yap.sequence')
        ##print ' ONCHANGE KOM DAHIL'
        self.name = self.name_temp

    @api.multi
    @api.onchange('amex_2')
    def _onchange_amex_2( self ):
        if self.amex_2:
            self.kart_no_2 = self.amex_2[:4]
            self.kart_no_3 = self.amex_2[4:]

    @api.multi
    @api.onchange('amex_3')
    def _onchange_amex_3( self ):
        if self.amex_3:
            self.kart_no_3 += self.amex_2[:4]
            self.kart_no_4 = self.amex_2[4:]

    def getCardType(self, not_raise=False):
        ##print 'GET KART TYPE'
        if self.kart_no_1 and self.kart_no_2:
            str_card_no = str(self.kart_no_1) + str(self.kart_no_2)
            str_card_no = str_card_no[:6]
            self.card_bin = False
            bin_no = self.env['info_kk_payment.card_bin_data'].search([('code', '=', str_card_no)])

            if len(bin_no) > 0:
                self.card_bin = bin_no[0].id
            else:
                self.card_bin = False

    @api.multi
    def odeme_yap(self):
        self.ensure_one()
        test = True

        if self._context.get('trpara'):
            #print self.payload
            pass
        else:

            if test:
                return {
                    'type': 'ir.actions.act_url',
                    'url': '/web/pay_test/' + str(self.id),
                    'target': 'popup'
                }
            else:
                if self.payload:
                    return {
                        'type': 'ir.actions.act_url',
                        'url': '/web/odeme_yap/' + str(self.id),
                        'target': 'popup'
                    }
    @api.multi
    def write(self,values):

        self.ensure_one()
        if  'kart_no_1' in values \
            and 'kart_no_2' in values \
            and 'kart_no_3' in values \
            and 'cvc' in values\
            and 'kart_sahibi' in values \
            and 'sk_tarihi_yil' in values\
            and 'sk_tarihi_ay' in values:
            ##print values
            if not values.get('kart_no_4')              :values['kart_no_4']         = self.kart_no_4
            if not values.get('name')                   :values['name']              = self.name
            if not values.get('tutar_cekilecek')        :values['tutar_cekilecek']   = self.tutar_cekilecek

            secilen_bin = self.env['info_kk_payment.taksit_by_siparis'].search([('siparis_no','=',values['name'])],order='id desc',limit=1)

            if not secilen_bin:
                raise exceptions.ValidationError("Tanımsız Taksit Seçimi")
            else:
                secilen_bin = secilen_bin[0]

            #secilen_acquirer = secilen_bin.taksit.kart.banka.bkm_id

            values['kart_tipleri_']    = secilen_bin.taksit.kart.id
            values['taksit_oranlari_'] = secilen_bin.taksit.id


            self.control_card_set_invalid_log( values )

            payload, url, url_header, api_url, banka_id, bkm_id  = self.get_payload_model( values )

            ##print payload

            values['payload']    = payload
            values['url']        = url
            values['url_header'] = url_header
            values['api_url']    = api_url
            values['banka_id']   = banka_id
            values['bkm_id']     = bkm_id

            values['cvc']            = False
            values['kart_sahibi']    = False
            values['kart_no_1']      = False
            values['kart_no_2']      = False
            values['kart_no_3']      = False
            values['sk_tarihi_ay']   = False
            values['sk_tarihi_yil']  = False


        res = super(odeme_yap, self ).write( values )
        if self.kart_no_1:
            self.kart_no_1 = False
        if self.kart_no_2:
            self.kart_no_2 = False
        if self.kart_no_3:
            self.kart_no_3 = False
        if self.cvc:
            self.cvc       = False
        if self.kart_sahibi:
            self.kart_sahibi = False
        if self.sk_tarihi_yil:
            self.sk_tarihi_yil = False
        if self.sk_tarihi_ay:
            self.sk_tarihi_ay = False
        return res

    @api.model
    def create( self, values ):
        print (values)
        ##print self._context
        ##print 1
        if not values.get('taksit_oranlari_') or not values.get( 'kart_tipleri_' ):
            secilen_bin = self.env['info_kk_payment.taksit_by_siparis'].search([('siparis_no','=',values['name'])],order='id desc',limit=1)
            ##print 2
            if not secilen_bin:
                raise exceptions.ValidationError("Tanımsız Taksit Seçimi")
            else:
                secilen_bin = secilen_bin[0]
            ##print 3
            #secilen_acquirer = secilen_bin.taksit.kart.banka.bkm_id
            ##print 4
            values['kart_tipleri_']    = secilen_bin.taksit.kart.id
            values['taksit_oranlari_'] = secilen_bin.taksit.id
            ##print 5

        payload, url, url_header, api_url, banka_id, bkm_id  = self.get_payload_model( values )
        ##print 6
        values['payload']    = payload
        values['url']        = url
        values['url_header'] = url_header
        values['api_url']    = api_url
        values['banka_id']   = banka_id
        values['bkm_id']     = bkm_id

        values['cvc']            = False
        values['kart_sahibi']    = False
        values['kart_no_1']      = False
        values['kart_no_2']      = False
        values['kart_no_3']      = False
        values['sk_tarihi_ay']   = False
        values['sk_tarihi_yil']  = False

        values['diger_tutars'] = False

        res = super( odeme_yap , self).create( values )
        ##print 7
        return res

    def control_card_set_invalid_log(self,values,secilen_acquirer=False ):
        ##print "-> 2. Çekilecek Tutar", values["tutar_cekilecek"]
        '''
        if values.has_key('card_bin') and values['card_bin']:
            pass
        else:
            self.getCardType( not_raise = True )

            if not self.card_bin:
                raise exceptions.ValidationError("Tanımsız Kart")
            else:
                values['card_bin'] = self.card_bin.id


        bin_ = self.env['info_kk_payment.card_bin_data'].browse([values['card_bin']])[0]
        girilen_acquirer = bin_.acquirer_id


        if int( girilen_acquirer) != int(secilen_acquirer):
            raise exceptions.ValidationError("Seçilen Ödeme Tipine Uygun Kart İle Ödeme Yapınız")
        '''

        if  not values ['kart_no_1'].isdigit() or \
            not values ['kart_no_2'].isdigit() or \
            not values ['kart_no_3'].isdigit() or \
            not values ['kart_no_4'].isdigit() or \
            not values ['cvc'].isdigit():
                raise exceptions.ValidationError("Kart Değerleri Nümerik Olmalıdır.")

    def get_payload_model(self, values):
        kart            = values['kart_tipleri_']
        taksit          = values['taksit_oranlari_']
        ##print 'Kart : ',kart,taksit
        if kart:
            acquirer        = self.env['info_kk_payment.kart_tipleri'].browse( kart )[0].banka
        else:
            acquirer        = self.env['info_kk_payment.acquirer'].search( [('default_banka','=',True)] )
        ##print "acquirer.url", acquirer.url

        if not acquirer.url:
            acquirer = self.env['info_kk_payment.acquirer'].search([('default_banka', '=', True)])

        ##print "acquirer.url", acquirer.url
        ##print "bkm_id ----> ", acquirer.bkm_id

        taksit_oranlari = int(self.env['info_kk_payment.taksit_oranlari'].browse( taksit )[0].taksit_sayisi)

        payload = {}
        card_no = values['kart_no_1'] + values['kart_no_2'] + values['kart_no_3'] + values['kart_no_4']

        rnd = int(round(time.time() * 1000))
        storetype = '3d_pay_hosting'
        param = self.env['ir.config_parameter'].get_param("web.base.url", default = 'www.yemekmatik.com.tr')
        EST_RETURN_URL    = param + "/web/info_kk_payment/payresult"
        EST_FAIL_URL      = param + "/web/info_kk_payment/payresult"

        payload = dict(
            clientid                        = acquirer.EST_MERCHANT_ID,
            oid                             = values['name'],
            amount                          = "{0:.2f}".format (values['tutar_cekilecek']),
            okUrl                           = EST_RETURN_URL,
            failUrl                         = EST_FAIL_URL,
            storetype                       = storetype,
            rnd                             = rnd,
            pan                             = card_no,
            cv2                             = values['cvc'],
            Ecom_Payment_Card_ExpDate_Year  = values['sk_tarihi_yil'],
            Ecom_Payment_Card_ExpDate_Month = values['sk_tarihi_ay'],
            firmaadi                        = URETICI_FIRMA_ADI,
            lang                            = 'tr',
            islemtipi                       = 'Auth',
            currency                        = "949",
            key                             = acquirer.EST_3D_KEY,
            refreshtime                     = 0,
            holdername                      = values['kart_sahibi']
        )

        if taksit_oranlari:
            payload['taksit'] = taksit_oranlari
            hash_ = self.get_sha1_hash_46( payload , True)
        else:
            hash_ = self.get_sha1_hash_46( payload )
        payload['hash'] = hash_
        return payload, acquirer.url, acquirer.url_header, acquirer.api_url, acquirer.id, acquirer.bkm_id

    def get_sha1_hash_46 ( self, payload, taksitli=False ):
        hs = sha1()
        if taksitli:hasg_str = '%(clientid)s%(oid)s%(amount)s%(okUrl)s%(failUrl)s%(islemtipi)s%(taksit)s%(rnd)s%(key)s' % payload
        else:hasg_str='%(clientid)s%(oid)s%(amount)s%(okUrl)s%(failUrl)s%(islemtipi)s%(rnd)s%(key)s' % payload

        ##print hasg_str

        hs.update ( hasg_str.encode('utf-8') )
        sha1_hashed = hs.digest()
        ##print hs.hexdigest()
        base64_enc = base64.b64encode(sha1_hashed)
        return base64_enc

    @api.multi
    def komisyon_mahsuplastir(self,source_payment,komisyon,komisyon_farki):
        move 		= self.env['account.move'].sudo()
        move_line 	= self.env['account.move.line'].sudo()
        currency_id = source_payment.currency_id.id
        journal 	= self.env['account.journal'].sudo()
        journal_komisyon    = journal.search([('komisyon_defteri','=',True)])
        if len(journal_komisyon) > 0:
            journal_komisyon = journal_komisyon[0]
            new_move_komisyon_dict = dict(journal_id = journal_komisyon.id,
                                      date       = dt.now(),
                                      company_id = 1,
                                      ref      	 = source_payment.name)

            komisyon_line_dict_1 = dict(account_id  = journal_komisyon.default_debit_account_id.id,
                                      name        = source_payment.name + u'-KK Komisyon',
                                      debit       = komisyon,
                                      currency_id = currency_id
                                      )
            komisyon_line_dict_2 = dict(account_id  = journal_komisyon.default_credit_account_id.id,
                                      name        = source_payment.name + u'-KK Komisyon',
                                      credit      = komisyon,
                                      currency_id = currency_id
                                      )

            new_move_komisyon_dict['line_ids'] = [[0,False,komisyon_line_dict_1],[0,False,komisyon_line_dict_2]]
            new_move_komisyon      = move.create( new_move_komisyon_dict )
            new_move_komisyon.post()


        journal_kom_fark    = journal.search([('komisyon_fark_defteri','=',True)])
        if len(journal_kom_fark) > 0:

            journal_kom_fark = journal_kom_fark[0]
            new_move_komisyon_fark_dict = dict(journal_id = journal_kom_fark.id,
                                          date       = dt.now(),
                                          company_id = 1,
                                          ref      	 = source_payment.name)

            komisyon_fark_line_dict_1 = dict(account_id  = journal_kom_fark.default_debit_account_id.id,
                                      name        = source_payment.name + u'-KK Komisyon Fark',
                                      debit       = komisyon_farki,
                                      currency_id = currency_id
                                      )
            komisyon_fark_line_dict_2 = dict(account_id  = journal_kom_fark.default_credit_account_id.id,
                                      name        = source_payment.name + u'-KK Komisyon Fark',
                                      credit      = komisyon_farki,
                                      currency_id = currency_id
                                      )

            new_move_komisyon_fark_dict['line_ids'] = [[0,False,komisyon_fark_line_dict_1],[0,False,komisyon_fark_line_dict_2]]

            new_move_komisyon_fark      = move.create( new_move_komisyon_fark_dict )
            new_move_komisyon_fark.post()
    @api.multi
    def makbuz_yazdir(self):
        self.ensure_one()
        param = self.env["ir.config_parameter"]
        href_g = param.get_param("web.base.external_url", default=None)

        ##print "------------------"
        ##print href_g
        return {
            'type': 'ir.actions.act_url',
            'url': '/report/pdf/info_kk_payment.report_makbuz/' + str(self.id),
            'target': 'popup'
        }
    @api.one
    @api.depends('tutar_cekilecek')
    def _compute_amount_total_words(self):
        #-------------------------------------------------------------
        #ENGLISH
        #-------------------------------------------------------------

        to_10 = ( u'SIFIR',  u'BİR',   u'İKİ',  u'ÜÇ', u'DÖRT',   u'BEŞ',   u'ALTI',
                  u'YEDİ', u'SEKİZ', u'DOKUZ' )
        tens  = ( u'ON',u'YİRMİ', u'OTUZ', u'KIRK', u'ELLİ', u'ALTMIŞ', u'YETMİŞ', u'SEKSEN', u'DOKSAN')

        denom = ( '',
                  u'BİN',     u'MİLYON',         u'MİLYAR',       u'TRİLYON',       u'TRİLYAR',
                  u'KATRİLYON',u'KATRİLYAR')


        def _convert_nn(val):
            """convert a value < 100 to turkish.
            """
            if val < 10:
                return to_10[val]
            for (dcap, dval) in ((k, 10 + (10 * v)) for (v, k) in enumerate(tens)):
                if dval + 10 > val:
                    if val % 10:
                        return dcap + '' + to_10[val % 10]
                    return dcap

        def _convert_nnn(val):
            """
                convert a value < 1000 to turkish, special cased because it is the level that kicks
                off the < 100 special case.  The rest are more general.  This also allows you to
                get strings in the form of 'forty-five hundred' if called directly.
            """
            word = ''
            (mod, rem) = (val % 100, val // 100)

            if rem > 0:
                if rem == 1:
                    word= u'YÜZ'
                else:
                    word = to_10[rem] + u'YÜZ'
                if mod > 0:
                    word += ''
            if mod > 0:
                word += _convert_nn(mod)
            return word

        def turkish_number(val):

            if val < 10:
                return to_10[val]
            if val < 100:
                return _convert_nn(val)
            if val < 1000:
                 return _convert_nnn(val)

            for (didx, dval) in ((v - 1, 1000 ** v) for v in range(len(denom))):
                if dval > val:
                    mod = 1000 ** didx
                    l = val // mod
                    r = val - (l * mod)
                    if l == 1:
                        ret = denom[didx]
                    else:
                        ret = _convert_nnn(l) +'' + denom[didx]

                    if r > 0:
                        ret = ret + '' + turkish_number(r)
                    return ret

        def amount_to_text_tr(number, currency = 'TL'):
            number = '%.2f' % number
            units_name = currency
            list = str(number).split('.')
            start_word = turkish_number(int(list[0]))
            end_word = turkish_number(int(list[1]))
            cents_number = int(list[1])
            cents_name =  u'Kr.'

            return ' '.join(list(filter(None, [start_word, units_name, end_word, cents_name])))


        self.amount_total_words = amount_to_text_tr(self.tutar_cekilecek)
    @api.multi
    def get_order_status_from_bank(self,context,from_cron=False ):
        self.ensure_one()
        acquirer = self.banka_id

        xml = """<CC5Request>
                    <Name>%s</Name>
                    <Password>%s</Password>
                    <ClientId>%s</ClientId>
                    <OrderId>%s</OrderId>
                        <Extra>
                            <ORDERSTATUS>QUERY</ORDERSTATUS>
                        </Extra>
                </CC5Request>"""%(acquirer.EST_3D_user, acquirer.EST_3D_KEY, acquirer.EST_MERCHANT_ID, self.name)

        s = requests.Session()
        s.mount('https://', TLSv1_2Adapter())
        resp = s.post(acquirer.api_url, data=xml)
        tree = ET.ElementTree(ET.fromstring(resp.text))
        resp_type = tree.find("Response")

        if resp_type.text == "Approved" :
            extra     = tree.find("Extra")
            auth_code = extra.find("AUTH_CODE")
            self.auth_code = auth_code.text

            host_code = extra.find("HOST_REF_NUM")
            self.host_code = host_code.text

            settle_code = extra.find("SETTLEID")
            self.settle_code = settle_code.text

            if  self.bank_state != 'muhasebe':
                user_company       = self.company_id

                inv = None
                odeme_tipi = 'cash'
                if self.odeme_tipi.startswith('bank') or self.odeme_tipi == 'kk':odeme_tipi = 'bank'

                if self.musteri_fat:
                    inv = self.musteri_fat

                elif self.satinalma:
                    inv = self.env['account.invoice'].search([('origin','=',self.satinalma.name),('state','=','open')])

                vals = dict(
                        amount = self.tutar_cekilecek_hidden,
                        date   = self.create_date
                    )
                try:

                    if self.partner_id.id != user_company.partner_id.id:

                        if inv:
                            journal_id = self.env['account.journal'].sudo().search([('type','=',odeme_tipi),('company_id','=',user_company.id)])

                            first_context =  {
                                'payment_expected_currency': inv.currency_id.id,
                                'default_partner_id': self.env['res.partner']._find_accounting_partner(inv.partner_id).id,
                                'default_amount': inv.type in ('out_refund', 'in_refund') and -inv.residual or inv.residual,
                                'default_reference': inv.name,
                                'close_after_process': True,
                                'invoice_type': inv.type,
                                'invoice_id': inv.id,
                                'default_type': inv.type in ('out_invoice','out_refund') and 'receipt' or 'payment',
                                'type': inv.type in ('out_invoice','out_refund') and 'receipt' or 'payment',
                                'default_journal_id':journal_id.id,
                                'reference':inv.reference,
                                'odeme_tipi':odeme_tipi,
                                'number':self.name,
                                'odeme_secenekleri':self.odeme_secenekleri
                            }
                            ##print first_context
                        else:
                            ##print 4
                            first_context={
                                'type':'payment',
                                'default_type':'payment',
                                'default_amount':self.tutar_cekilecek_hidden,
                                'default_partner_id':self.partner_id.id,
                                'default_partner_id_view':self.partner_id.name,
                                'reference':self.name,
                                'odeme_tipi':odeme_tipi,
                                'number':self.name,
                                'odeme_secenekleri':self.odeme_secenekleri
                        }
                        ##print 5
                        voucher_vals = self.env['account.voucher'].create_voucher_karsilik( first_context,user_company,self.partner_id.id,vals,True )
                        voucher      = self.env['account.voucher'].with_context( first_context ).create ( voucher_vals['value'] )
                        self.voucher = voucher.id
                        voucher.signal_workflow('proforma_voucher')
                        ##print 6
                    counter = 0
                    ##print 7, user_company.parent_id
                    while user_company.parent_id:
                        ##print counter
                        if counter == 0:
                            voucher_set = True
                            if inv:
                                voucher_create_context =  {
                                    'payment_expected_currency': inv.currency_id.id,
                                    'partner_id': self.env['res.partner']._find_accounting_partner(inv.partner_id).id,
                                    'amount': inv.type in ('out_refund', 'in_refund') and -inv.residual or inv.residual,
                                    'reference': inv.name,
                                    'close_after_process': True,
                                    'invoice_type': inv.type,
                                    'invoice_id': inv.id,
                                    'type': inv.type in ('out_invoice','out_refund') and 'receipt' or 'payment',
                                    'journal_id':journal_id.id,
                                    'reference':inv.reference,
                                    'odeme_tipi':odeme_tipi,
                                    'number':self.name,
                                    'new_ent':True,
                                    'odeme_secenekleri':self.odeme_secenekleri
                                }
                            else:
                                voucher_create_context={
                                    'type':'payment',
                                    'amount':self.tutar_cekilecek_hidden,
                                    'partner_id':user_company.sudo().parent_id.partner_id.id,
                                    'partner_id_view':user_company.sudo().parent_id.partner_id.name,
                                    'reference':self.name,
                                    'odeme_tipi':odeme_tipi,
                                    'number':self.name,
                                    'new_ent':True,
                                    'odeme_secenekleri':self.odeme_secenekleri
                            }
                        else:
                            voucher_create_context = {
                                                'type':'payment',
                                                'new_ent':True,
                                                'odeme_tipi':odeme_tipi,
                                                'amount':self.tutar_cekilecek_hidden,
                                                'partner_id':user_company.sudo().parent_id.partner_id.id,
                                                'partner_id_view':user_company.sudo().parent_id.partner_id.name,
                                                'odeme_secenekleri':'acik',
                                                'reference':self.name,
                                                'number':self.name,
                                                }
                        new_voucher  = self.env['account.voucher'].with_context(  voucher_create_context  ).sudo( user_company.intercompany_user_id
                                                                                                                         and user_company.intercompany_user_id.id
                                                                                                                         or False )
                        voucher_vals = new_voucher.create_voucher_karsilik( voucher_create_context,
                                                                            user_company,
                                                                            user_company.sudo().parent_id.partner_id.id,
                                                                            vals,
                                                                            True )
                        ##print 'donen voucher_vals : ', voucher_vals

                        v = new_voucher.create ( voucher_vals['value'] )
                        v.signal_workflow('proforma_voucher')
                        if voucher_set:
                            voucher_set = False
                            self.voucher = v.id
                        user_company = user_company.sudo().parent_id
                        counter += 1
                    self.bank_state = 'muhasebe'

                except:
                    bank_state = "basarili"
                    ##print 'except'
                    ##print self.id

                    self.env.cr.rollback()
                    with api.Environment.manage():
                        with registry(self.env.cr.dbname).cursor() as new_cr:
                            log_q = "update info_kk_payment_odeme_yap set bank_state = 'basarili' where id = %s " % self.id
                            ##print log_q
                            new_cr.execute( log_q )
                            ##print 'here'
                            new_cr.commit()
                    ##print from_cron
                    if not from_cron:
                        raise exceptions.ValidationError("""Ödemeniz Başarılı
                                                     ANCAK MUHASEBELEŞTİRİLEMEDİ. SİSTEM YÖNETİCİSİ İLE GÖRÜŞÜNÜZ""")
        else:
            if not from_cron:
                raise exceptions.ValidationError("BANKA SORGUSU BAŞARISIZ İlgili Ödeme İçin Banka Bilgisi Bulunmamakta ya da Banka Ödemeyi Reddetmiş")

class kampanya(models.Model):
    _name = 'info_kk_payment.yemekmatik_kampanya'

    product_id = fields.Many2one('product.template',string='Ürün', required = True)
    name = fields.Char('Kampanya Adı', required=True)
    aktif = fields.Boolean(string='Aktif',default=True)
    tutar = fields.Float('Kampanya Tutarı', required=True)

    @api.onchange('product_id')
    def onchange_p(self):
        self.name  = self.product_id.name
        self.tutar = self.product_id.list_price

class TeklifWizardKK(models.TransientModel):
    _name          = 'info_kk_payment.urun_sec_wizard_kk'

    partner_id     = fields.Many2one('res.partner',string='Üye İş Yeri / Esnaf',domain=[('is_company','=',True),('esnaf','=',True)],required=True)
    kampanya       = fields.Many2one('info_kk_payment.yemekmatik_kampanya',string='Kampanya', requred=True)
    tutar          = fields.Float(related = 'kampanya.tutar')

    sermaye_sirketi = fields.Boolean(related='partner_id.sermaye_sirketi')
    sahis_sirketi   = fields.Boolean(related='partner_id.sahis_sirketi')
    ortaklik        = fields.Boolean(related='partner_id.ortaklik')

    adres           = fields.Char(related='partner_id.street')
    adres2          = fields.Char(related='partner_id.street2')

    ilce            = fields.Many2one(related='partner_id.ilce')
    il              = fields.Many2one(related='partner_id.city_combo')

    vergi_no        = fields.Char(related='partner_id.taxNumber')
    tckn            = fields.Char(related='partner_id.tckn')



    @api.multi
    def kk_odeme_yap( self ):

        kampanya = self.kampanya.id
        partner_id = self.partner_id.id
        print ( self.partner_id.id)
        return {
            'type': 'ir.actions.act_url',
            'url': '/yemek-karti/kart-ode2/%s/%s'%(str(kampanya),str(partner_id)),
            'target':'self'
        }



        """"
        self.ensure_one()
        view_id = self.env['ir.ui.view'].sudo().search([('name','=','yemekmatik_kampanya.odeme.form.wizard')]).id
        if self.tutar > 0:
            return {
                'type': 'ir.actions.act_window',
                'name': 'Kredi Kartı Taksit Oranları',
                'res_model': 'info_kk_payment.odeme_yap',
                'src_model': 'info_kk_payment.urun_sec_wizard_kk',
                'view_mode': 'form',
                'view_type':'form',
                'views': [[view_id, "form"]],
                'target': 'new',
                'context':{'default_partner_id':self.partner_id.id,
                        'default_tutar':self.tutar,
                       'search_default_group_by_resim':1,
                       },
                }
        else:
            raise exceptions.ValidationError('Lütfen Tutar Giriniz.')
        """

