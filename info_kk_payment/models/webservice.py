
from suds.client import Client as sclient
from datetime import datetime, timedelta, date as bd
from odoo import api,exceptions,registry

def set_log(obj, requestData, responseData, serviceName, operationName, time_request, proc_id=False):
    time_response = datetime.today()
    logging_obj = obj.env['info_bayi.turkpara_ws_logs']

    logging_var = {'serviceName': serviceName, 'operationName': operationName,
                   'requestTime': time_request, 'responseTime': time_response,
                   'requestData': requestData,
                   'responseData': responseData,
                   'proc_id': proc_id
                   }
    # bunu ikinci cr da bas, commit et. raise exception da bassın db ye rollback yapmasın.
    with api.Environment.manage():
        with registry(logging_obj.env.cr.dbname).cursor() as new_cr:
            new_env = api.Environment(new_cr, 1, logging_obj.env.context)
            logging_obj.with_env(new_env).create(logging_var)
            new_env.cr.commit()

def get_b2b_client():
    wsdl        = 'http://localhost:8070/yemekmatikservice'
    kurumKodu   = 'YMK'
    kurumToken  = 'YMK'
    client = sclient(wsdl,cache=None)
    return client, kurumKodu, kurumToken

def b2b_order_sync(obj):

    client, kod, token = get_b2b_client()
    time_request = datetime.today()
    response = client.service.createOrder(kurumKodu   =kod,
                                            kurumToken=token,
                                            partnerId   =obj.partner_id.b2b_id,
                                            pricelistId =1,
                                            productId   = obj.product_id.product_id.b2b_id,
                                            priceUnit   = obj.tutar,
                                            productName = obj.product_id.name
                                          )

    requestData = dict(kurumKodu   =kod,
                                            kurumToken=token,
                                            partnerId   =obj.partner_id.b2b_id,
                                            pricelistId =1,
                                            productId   = obj.product_id.product_id.b2b_id,
                                            priceUnit   = obj.tutar,
                                            productName = obj.product_id.name)

    set_log(obj,requestData, response, 'B2b Entegrasyon', 'Order SYNC', time_request)

    if response.cevapKodu == '000':
        return True
    else:
        return False

def b2b_product_sync(obj):

    client, kod, token = get_b2b_client()
    time_request = datetime.today()
    response = client.service.createProduct(kurumKodu=kod,
                                            kurumToken=token,
                                            name=obj.name,
                                            listPrice=obj.list_price)

    requestData = dict(kurumKodu=kod,
                        kurumToken=token,
                        name=obj.name,
                        listPrice=obj.list_price)

    set_log(obj,requestData, response, 'B2b Entegrasyon', 'Product SYNC', time_request)

    if response.cevapKodu == '000':
        obj.b2b_id = response.productId
        return True
    else:
        return False

def b2b_payment_sync(obj):

    client,kod,token = get_b2b_client()
    time_request = datetime.today()
    response = client.service.createPayment(kurumKodu   = kod,
                                            kurumToken  = token,
                                            partnerId   = obj.partner_id.b2b_id,
                                            amount      = obj.tutar,
                                            paymentDate = obj.create_date)
    
    requestData = dict( kurumKodu   = kod,
                        kurumToken  = token,
                        partnerId   = obj.partner_id.b2b_id,
                        amount      = obj.tutar,
                        paymentDate = obj.create_date)
    
    set_log (obj, requestData, response,'B2b Entegrasyon','Payment SYNC',time_request)
    
    if response.cevapKodu == '000':
        obj.bank_state = 'muhasebe'
        if not obj.product_id.product_id.b2b_id:
            p_sync = b2b_product_sync( obj.product_id.product_id )
            if not p_sync:
                obj.b2b_sync_info = 'Ödeme İşlendi ancek Ürün Senkronu Başarısız, Sipariş Oluşturulamadı.'
                return

        o_sync = b2b_order_sync( obj  )

        if o_sync:
            obj.b2b_sync_info = 'Ödeme İşlendi ve Sipariş Oluşturuldu.'
        else:
            obj.b2b_sync_info = 'Ödeme İşlendi ancek  Sipariş Oluşturulamadı.'
    else:
        obj.b2b_sync_info = response.cevapKodu + ':' + response.cevapAciklama
        
def b2b_partner_sync(obj):
    
    client,kod,token = get_b2b_client()
    time_request = datetime.today()

    response = client.service.createPartner(kurumKodu       = kod,
                                            kurumToken      = token,
                                            name            = obj.name or '',
                                            display_name    = obj.display_name or '',
                                            street          = obj.street or '',
                                            street2         = obj.street2 or '',
                                            ilce            = obj.ilce.ilce_kodu or '',
                                            city_combo      = obj.city_combo.plaka or '',
                                            taxNumber       = obj.taxNumber or '',
                                            tckn            = obj.tckn or '',
                                            email           = obj.email or '',
                                            mersisNumber    = obj.mersisNumber or '',
                                            phone           = obj.phone or '',
                                            firma_unvani    = obj.firma_unvani or '',
                                            sahis_sirketi   = obj.sahis_sirketi,
                                            sermaye_sirketi = obj.sermaye_sirketi,
                                            ortaklik        = obj.ortaklik,
                                            gsm_no          = obj.gsm_no or '1111111111',
                                            gsm_no_name     = obj.gsm_no_name or 'YOK',
                                            taxAgent        = obj.taxAgent.name or '')
    
    requestData = dict(     kurumKodu       = kod,
                                            kurumToken      = token,
                                            name            = obj.name,
                                            display_name    = obj.display_name,
                                            street          = obj.street,
                                            street2         = obj.street2,
                                            ilce            = obj.ilce.ilce_kodu,
                                            city_combo      = obj.city_combo.plaka,
                                            taxNumber       = obj.taxNumber,
                                            tckn            = obj.tckn,
                                            email           = obj.email,
                                            mersisNumber    = obj.mersisNumber,
                                            phone           = obj.phone,
                                            firma_unvani    = obj.firma_unvani,
                                            sahis_sirketi   = obj.sahis_sirketi,
                                            sermaye_sirketi = obj.sermaye_sirketi,
                                            ortaklik        = obj.ortaklik,
                                            gsm_no          = obj.gsm_no,
                                            gsm_no_name     = obj.gsm_no_name)
    
    set_log (obj, requestData, response,'B2b Entegrasyon','Partner Senkron',time_request)
    
    if response.cevapKodu == '000' or response.cevapKodu == '017':
        obj.b2b_id = response.partnerId
        if response.okcNames:
            f = ''
            for o in response.okcNames.okcNames:
                f += o
                f += ' - '
            f = f[:len(f)-3]
            obj.okc_names = f
            print (response.okcNames)

    else:
        obj.b2b_sync_info = response.cevapKodu + ':' + response.cevapAciklama

