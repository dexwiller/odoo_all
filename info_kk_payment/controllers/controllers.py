# -*- coding: utf-8 -*-
from odoo import http
from odoo.addons.web.controllers.main import serialize_exception
from odoo.addons.info_bayi.models.webserviceopt import BIN_SanalPos
import ast, json,requests
from requests.adapters import HTTPAdapter
from urllib3.poolmanager import PoolManager
import ssl
from werkzeug.utils import redirect
VAKIFLARBANKASI = 15
class TLSv1_2Adapter(HTTPAdapter):
    def init_poolmanager(self, connections, maxsize, block=False):
        self.poolmanager = PoolManager(num_pools=connections, maxsize=maxsize, block=block, ssl_version=ssl.PROTOCOL_TLSv1_2)

class KkPayment(http.Controller):
    @http.route("/yemek-karti/kart-ode2/<d>/<p>/", type='http', auth='public', website=True)
    @serialize_exception
    def get_kart_ode(self, d, p, **kw):
        #d urun id
        #p partner_id
        p_id    = p
        kam_id  = http.request.env['info_kk_payment.yemekmatik_kampanya'].sudo().browse([int(d)])

        return_url = kw.get('return_url') or '/'
        #layout     = kw.get('layout') or 'website.layout'
        layout = 'website.layout'
        return http.request.render('info_kk_payment.kart_ode',
                                   {'k_id': d, 'p_id': p_id, 'kampanya': kam_id,'return_url':return_url,'layout':layout})

    @http.route("/yemek-karti/tl_oranlar/local/", type='http', auth='public', website=True)
    @serialize_exception
    def get_taksitler(self, **kw):

        res = http.request.env['info_kk_payment.kart_tipleri'].sudo().search([])
        html = """
                <script>
                function sortTable(table, order) {
                    table.each(function(){
                        console.log($(this))
                        var asc   = order === 'asc';
                        var tbody = $(this).find('tbody');
                        console.log('sorting');
                        tbody.find('tr').sort(function(a, b) {
                            if (asc) {
                                return $('td:first', a).text().localeCompare($('td:first', b).text());
                            } else {
                                return $('td:first', b).text().localeCompare($('td:first', a).text());
                            }
                        }).appendTo(tbody);
                    })
                }
                sortTable($('.taksit_table'),'asc');
                </script>
                <div style="text-align:center;> 
                          <div class="modal-header">
                               <h4>Banka / Ön Ödemeli / Kredi Kartı ile<br> TL Yükleme Komisyon Oranları</h4>
                          </div>  """
        for r in res:
            html += """<div class="modal-body">
                              <div class="col-sm-12" style="text-align:left">
                                  <table class="table taksit_table"><tr><th><img height="120" width="200" src="data:image/png;base64, {}" alt="{}" /><th><th>{}</th><th>Komisyon Oranı</th></tr>
                                   """.format( str(r.resim,'utf-8'), r.resim, r.name )

            for t in r.taksit_oranlari:
                html += """<tr><td>{}<td><td>{}</td><td>%{}</td></tr>""".format(t.name,t.taksit_sayisi,t.taksit_orani)
            html += """</table></div></div>"""
        html += "</div>"
        return html

    @http.route('/yemek-karti/dynamic_tutar2/<t>/<k>/<g>/<tk>', type='http', auth="public")
    @serialize_exception
    def get_dynamic_tutar(self, t,k,g,tk, **kw):

        res = {}
        html = """
                <div class="row" id="ode_f" style="color:red">
                     Kredi Kartı Bilgileri Hatalı veya Desteklenmiyor...
                 </div>
                """
        if float(tk) < 0:
            bin_num = BIN_SanalPos(http.request, k.strip().replace(' ', ''))
            if bin_num:
                banka = http.request.env.get('info_kk_payment.kart_tipleri').sudo().search(
                    [('kod', '=', int(bin_num.get('SanalPOS_ID')))])
                if banka:
                    taksit_oran = http.request.env.get('info_kk_payment.taksit_oranlari').sudo().search([('is_active','=',True),
                                                                                                         ('kart','=',banka.id)],
                                                                                                        order="taksit_sayisi asc")
                    html = "<option value='-1'>Seçiniz</option>"
                    for o in taksit_oran:
                        html  += """"<option value='{}'>{}</option>""".format(o.id, str(o.name))

        else:
            taksit_oran = http.request.env.get('info_kk_payment.taksit_oranlari').sudo().browse([int(tk)])

            res.update({'raw_tutar': round(float(t), 2)})

            res['Oran'] = taksit_oran.taksit_orani
            res['Komisyon_Tutar'] = round(float(t) * taksit_oran.taksit_orani,2) / 100
            gercek_komisyon = round(float(t) * taksit_oran.komisyon_orani,2) / 100
            res['kom_fark'] = round(gercek_komisyon - res['Komisyon_Tutar'], 2)

            if res.get("Oran"):
                res.update({'Oran': float(res['Oran'])})
            else:
                res.update({'Oran': 0.0})
            if res.get('Komisyon_Tutar'):
                total = float(t) + float(res['Komisyon_Tutar'])
                res.update({'Komisyon_Tutar': round(float(res['Komisyon_Tutar']), 2)})
            else:
                total = float(t)
                res.update({'Komisyon_Tutar': 0.0})
            res.update({'total': round(total, 2)})
            # {'Taksit_Oran': 6|8,94|1031|0, 'Taksit': 6, 'Taksit_Str': 6 Taksit [%8,94], '_rowOrder': u'0', 'SanalPOS_ID': 1031, 'Komisyon_Tutar': 4.4700, '_id': u'T', 'Oran': 8.9400}
            html = """
                    <div class="row" id="ode_f">
                         <div class="hff col-md-3 hff_textbox form-group" data-form-type="textbox" data-field-id="31">
                           <label class="control-label" for="ttr">Yükleme Tutarı</label>
                           <input type="text" 
                           class="form-control"  name="ttr" readonly="readonly" value="%(raw_tutar)s TL"/>
                         </div>
                          <div class="hff col-md-3 hff_textbox form-group" data-form-type="textbox" data-field-id="31">
                           <label class="control-label " for="kom_oran">Komisyon Oranı</label>
                           <input type="text" 
                           class="form-control"  name="kom_oran" readonly="readonly" value="%%%(Oran)s"/>
                         </div>
                          <div class="hff col-md-3 hff_textbox form-group" data-form-type="textbox" data-field-id="31">
                           <label class="control-label " for="kom_tut">Komisyon Tutarı</label>
                           <input type="text" 
                           class="form-control"  name="kom_tut" readonly="readonly" value="%(Komisyon_Tutar)s TL"/>
                         </div>
                          <div class="hff col-md-3 hff_textbox form-group" data-form-type="textbox" data-field-id="31">
                           <label class="control-label " for="total_tut">Toplam Tutar</label>
                           <input type="text" 
                           class="form-control"  name="total_tut" readonly="readonly" value="%(total)s TL"/>
                         </div>
                         <input type="hidden" name="kom_fark" value="%(kom_fark)s />
                     </div>
                    """ % (res)

        return html

    def get_Vakif_payment(self, payload, url, s, payload_obj):
        ay = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16",
              "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"]
        MerchantId = payload.get("clientid")
        MerchantPassword = payload.get("key")
        VerifyEnrollmentRequestId = payload.get("rnd")
        pan = payload.get("pan")
        ExpiryDate = str(int(payload.get("Ecom_Payment_Card_ExpDate_Year"))) + ay[int(
            payload.get("Ecom_Payment_Card_ExpDate_Month"))]
        PurchaseAmount = payload.get("amount")
        Currency = payload.get("currency")
        BrandName = 100
        SuccessUrl = payload.get("okUrl")
        FailureUrl = payload.get("failUrl")

        if pan.startswith('4'):
            barnd_name = 100
        elif pan.startswith('5'):
            barnd_name = 200
        else:
            barnd_name = 100
        payloadVakif = dict(
            HostMerchantId=MerchantId,
            HostTerminalId='VP003045',
            MerchantPassword=MerchantPassword,
            PAN=pan,
            ExpireMonth=payload.get('Ecom_Payment_Card_ExpDate_Month'),
            ExpireYear='20' + str(payload.get('Ecom_Payment_Card_ExpDate_Year')),
            SuccessUrl=SuccessUrl.replace('payresult', 'vresult'),
            FailUrl=FailureUrl.replace('payresult', 'vresult'),
            AmountCode=949,
            Amount=PurchaseAmount,
            OrderID=payload.get("oid"),
            TransactionType='Sale',
            IsSecure=True,
            AllowNotEnrolledCard=True,
            CVV=payload.get("cv2"),
            CardHoldersName=payload['holdername'],
            BrandNumber=barnd_name
        )
        if payload.get('taksit'):
            payloadVakif['InstallmentCount'] = payload.get('taksit')

        ##print payloadVakif
        resp = s.post(url, data=payloadVakif)

        payload_obj.payload = False


        response_text = json.loads(resp.text)
        if response_text.get('CommonPaymentUrl') and response_text.get('PaymentToken'):
            payload_obj.payload = response_text.get('PaymentToken')
            return response_text.get('CommonPaymentUrl') + '?Ptkn=' + response_text.get('PaymentToken')

        else:
            return '/web/payresult'

    @http.route('/yemek-karti/kart-ode-3', type='http', auth='none', cors='*')
    @serialize_exception
    def get_kk_payment(self, **kw):
        return_errors = []
        if not kw.get("ad"):
            return_item = {"html_name": "ad", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append(return_item)
        if not kw.get("serial_number"):
            return_item = {"html_name": "serial_number", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append(return_item)
        if not kw.get("k_id"):
            return_item = {"html_name": "k_id", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append(return_item)
        if not kw.get("ay"):
            return_item = {"html_name": "ay", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append(return_item)
        else:
            try:
                if not int(kw.get("ay") ) > 0 and int(kw.get("ay")) > 12:
                    return_item = {"html_name": "ay", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
                    return_errors.append(return_item)
            except:
                return_item = {"html_name": "ay", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
                return_errors.append(return_item)
        if not kw.get("yil"):
            return_item = {"html_name": "yil", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append(return_item)
        else:
            try:
                if not int(kw.get("yil") ) >= 19 and int(kw.get("yil")) > 29:
                    return_item = {"html_name": "yil", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
                    return_errors.append(return_item)
            except:
                return_item = {"html_name": "yil", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
                return_errors.append(return_item)
        if not kw.get("cvc"):
            return_item = {"html_name": "cvc", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append(return_item)
        else:
            try:
                int(kw.get("cvc") )
            except:
                return_item = {"html_name": "cvc", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
                return_errors.append(return_item)
        if not kw.get("tk"):
            return_item = {"html_name": "tk", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append(return_item)
        if not kw.get("tutar"):
            return_item = {"html_name": "tutar", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
            return_errors.append(return_item)

        if return_errors:
            return json.JSONEncoder().encode({'status': 'error', 'errors': return_errors})

        taksit_oran = http.request.env.get('info_kk_payment.taksit_oranlari').sudo().browse([int(kw.get('tk'))])
        kart_tipi   = taksit_oran.kart.id

        ###############################################################################################
        #ekrandaki tutarı alıyor, güvenlik açığı !!!!  kapmanya id ye göre komisyonu tekrar hesapla !!!
        ###############################################################################################

        kart_nos    = kw.get("serial_number").split(" ")
        new_pay     = http.request.env.get('info_kk_payment.odeme_yap').sudo()

        d           = dict(komisyon_dahil=False,
                           tutar=float(kw.get("tutar").replace(",", '.').replace(' TL','')),
                           sk_tarihi_ay=kw.get("ay"),
                           partner_id=int(kw.get("p_id")),
                           tutar_cekilecek=float(kw.get("total_tut").replace(",", '.').replace(' TL','')),
                           kart_no_1 = kart_nos[0],
                           kart_no_2 = kart_nos[1],
                           kart_no_3 = kart_nos[2],
                           kart_no_4 = kart_nos[3],
                           komisyon  = float(kw.get('kom_tut').replace(",", '.').replace(' TL','')),
                           sk_tarihi_yil = kw.get('yil'),
                           komisyonfarki = kw.get('kom_fark'),
                           cariden_dusulecek = float(kw.get("tutar").replace(",", '.').replace(' TL','')),
                           kart_sahibi = kw.get('ad'),
                           cvc         = kw.get('cvc'),
                           taksit_oranlari_ = taksit_oran.id,
                           kart_tipleri_    = kart_tipi,
                           name             = new_pay._get_next_oid(),
                           product_id       =kw.get('k_id')

                           )

        new_pay = new_pay.create( d )
        payload_obj = new_pay
        # try:
        url = payload_obj.url
        url_header = payload_obj.url_header
        payload = ast.literal_eval(payload_obj.payload)

        # print ">>>> ", payload, payload_obj.bkm_id

        s = requests.Session()
        s.mount('https://', TLSv1_2Adapter())

        if payload_obj.bkm_id == VAKIFLARBANKASI:
            donen = self.get_Vakif_payment(payload, url, s, payload_obj)
            return json.JSONEncoder().encode({'status': 'success', 'redirect_url': donen, 'return_type': 'modal'})

        else:
            # print 'resp  ---> ', url
            #print ( url, payload )
            resp = s.post(url, data=payload)
            payload_obj.payload = False

            #print (resp.text)
            text = resp.text
            text = text.replace('rel="stylesheet" href="', 'rel="stylesheet" href="%s' % url_header)
            text = text.replace('<img src="', '<img src="%s' % url_header)
            payload_obj.payload = False
            ###print text

            donen =  text
            return json.JSONEncoder().encode({'status': 'success', 'redirect_url': donen, 'return_type': 'modal_text'})

    @http.route('/web/info_kk_payment/vresult/', type='http', auth="public", csrf=False)
    @serialize_exception
    def see_pay_v(self, *args, **kw):

        # kw, args

        ##print "see_pay_v"
        param = http.request.env["ir.config_parameter"]
        href_g = param.get_param("web.base.url", default=None)

        template = 'info_kk_payment.odeme_yap'
        msg = 'Bir Hata Oluştu'
        vendor_msg = 'Bankadan Onay Alınamadı, Bankanızı Arayınız.'
        image = '/info_kk_payment/static/src/cros.png'
        makbuz_url = ''
        state = 'basarisiz'

        if 'Message' in kw and 'PaymentToken' in kw:

            payload_obj = http.request.env['info_kk_payment.odeme_yap'].sudo().search([('payload', '=', kw['PaymentToken'])])

            if payload_obj:
                payload_obj = payload_obj[0]

            if kw.get('ErrorCode'):
                msg = 'Ödeme Başarısız'
                state = 'basarisiz'
                prefix = ''

                payload_obj.hata_kodu = kw["ErrorCode"]
                prefix = 'Hata Kodu : ' + kw["ErrorCode"] + " - "
                payload_obj.hata_msg = kw['Message']
                vendor_msg = prefix + "<br />" + kw['Message']
                image = '/info_kk_payment/static/src/cros.png'
            else:
                msg = 'Ödeme Başarılı'
                vendor_msg = 'Teşekkür Ederiz <br /> Otorizasyon Kodu: ' + kw['AuthCode']
                image = '/info_kk_payment/static/src/icon.png'
                makbuz_url = "document.location.href='" + href_g + "/report/pdf/info_kk_payment.report_makbuz/" + str(
                    payload_obj.id) + "'";
                state = 'basarili'


            payload_obj.payload = False
            payload_obj.bank_state = state

        return http.request.render(template, {
            'msg': msg,
            'vendor_msg': vendor_msg,
            'image': image,
            'makbuz_url': makbuz_url
        })

    @http.route('/web/info_kk_payment/payresult/', type='http', auth="public", csrf=False)
    @serialize_exception
    def see_pay(self, *args, **kw):

        # print "see_pay"
        param = http.request.env["ir.config_parameter"]
        href_g = param.get_param("web.base.url", default=None)

        template = 'info_kk_payment.odeme_yap'
        msg = 'Bir Hata Oluştu'
        vendor_msg = 'Bankadan Onay Alınamadı, Bankanızı Arayınız.'
        image = '/info_kk_payment/static/src/cros.png'
        makbuz_url = ''
        state = 'basarisiz'

        if 'Response' in kw and 'oid' in kw:
            # print oid
            payload_obj = http.request.env['info_kk_payment.odeme_yap'].sudo().search([('name', '=', kw['oid'])])
            # print payload_obj
            if payload_obj:
                payload_obj = payload_obj[0]

                if kw['Response'] != 'Approved':
                    msg = 'Ödeme Başarısız'
                    state = 'basarisiz'
                    prefix = ''
                    if 'mdStatus' in kw:
                        payload_obj.hata_kodu = kw["mdStatus"]
                        prefix = 'Hata Kodu : ' + kw["mdStatus"] + " - "
                    payload_obj.hata_msg = kw['ErrMsg']
                    vendor_msg = prefix + 'Hata:' + kw["Response"] + "<br />" + kw['ErrMsg']
                    image = '/info_kk_payment/static/src/cros.png'
                else:
                    msg = 'Ödeme Başarılı'
                    vendor_msg = 'Teşekkür Ederiz'
                    image = '/info_kk_payment/static/src/icon.png'
                    makbuz_url = "document.location.href='" + href_g + "/report/pdf/info_bayi.report_makbuz/" + str(
                        payload_obj.id) + "'";
                    state = 'basarili'

                payload_obj.payload = False
                payload_obj.bank_state = state

        return http.request.render(template, {
            'msg': msg,
            'vendor_msg': vendor_msg,
            'image': image,
            'makbuz_url': makbuz_url
        })


