# -*- coding: utf-8 -*-
{
    'name': "info_restaurant_timer",

    'summary': """
        Restoran hizmet sisteminin süresini kontrol eder.
        Kullanıcıları bilgilendirir""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Infoteks Teknology",
    'website': "https://www.infoteks.com.tr",

    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base', 'mail', 'info_restaurant_options'],

    'data': [
        'security/ir.model.access.csv',
        'views/assets.xml',
        'views/automated_action.xml',
        'data/mail_demo_template.xml',
    ],

    "qweb": [
        "static/src/xml/*.xml",
    ],

    'demo': [

    ],
    'application': True,
}
