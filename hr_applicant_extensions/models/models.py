# -*- coding: utf-8 -*-

from odoo import models, fields, api
class Applicant(models.Model):
    _name = "hr.applicant"

    contact_name = fields.Char(related='partner_id.display_name',string='Contact Name')
# class hr_applicant_extensions(models.Model):
#     _name = 'hr_applicant_extensions.hr_applicant_extensions'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100