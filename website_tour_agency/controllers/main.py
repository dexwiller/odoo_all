# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import base64
import werkzeug.utils
import json
import odoo
from odoo import http
from odoo.http import request
from odoo.addons.base_geolocalize.models.res_partner import geo_find, geo_query_address
from odoo.addons.base_geolocalize.models import res_partner as rp
from odoo.addons.website_form.controllers.main import WebsiteForm
from odoo.addons.website_sale.controllers.main import WebsiteSale


class QueryURL(object):

    def __init__(self, path='', **args):
        self.path = path
        self.args = args

    def __call__(self, path=None, **kw):
        if not path:
            path = self.path
        for k, v in self.args.items():
            kw.setdefault(k, v)
        l = []
        for k, v in kw.items():
            if v:
                if isinstance(v, list) or isinstance(v, set):
                    l.append(werkzeug.url_encode([(k, i) for i in v]))
                else:
                    l.append(werkzeug.url_encode([(k, v)]))
        if l:
            path += '?' + '&'.join(l)
        return path

PPG = 20  # Products Per Page
PPR = 4  # Products Per Row


class tours_packages(http.Controller):


    # def package_content_display(self, post):
    #     print('--------CALL javascript---------------------')
    #     print('--------CALL javascript---------------------',post)

    @http.route(['/page/packages'], type='http', auth="public", website=True)
    def packages(self, search='', page=0, **kwargs):
        print('<<-------------->>', search)
        keep = QueryURL('/')
        domain = [('website_published', '=', True)]
        if not search:
            url = '/page/packages'
            packages = request.env['sale.order'].search(
                domain, order='write_date desc')
        if search:
            kwargs["search"] = search
            search = kwargs["search"]
            domain_search = [
                ('name', 'ilike', search),
                ('website_published', '=', True)
            ]
            url = '/page/packages'
            packages = request.env['sale.order'].search(
                domain_search, order='write_date desc')
        print('keep ---', keep)
        print('Search----', search)
        print('packages---->', packages)
        print('packages---->', url)
        return request.render('website_tour_agency.packages', {
            'keep': keep,
            'search': search,
            'packages': packages,
        })

    @http.route(['/', '/page/homepage'], type='http',
                auth='public', website=True)
    def homepage(self, **kwargs):
        domain = [('hotel_ok', '=', True)]
        package_domain = [('website_published', '=', True)]
        hotels = request.env['product.template'].search(
            domain, order='write_date desc')
        packages = request.env['sale.order'].search(
            package_domain, order='write_date desc')
        return request.render('website_tour_agency.tour_agency_homepage', {
            'hotels': hotels,
        })

    @http.route(['/page/hotel'], type='http', auth="public", website=True)
    def hotels(self, search='', page=0, star=0, range=0, **kwargs):
        range = str(range)
        attrib_list = request.httprequest.args.getlist('facilities')
        attrib_values = [map(int, v.split("-")) for v in attrib_list if v]

        attrib_ids = [set(v) for v in attrib_values]

        attrib_set1 = list(attrib_ids)
        attrib_set = [list(i)[0] for i in attrib_set1]

        keep = QueryURL('/')
        domain = [('hotel_ok', '=', True)]
        url = '/page/hotel'

        if not search:
            url = '/page/hotel'
            hotels = request.env['product.template'].search(
                domain, order='write_date desc')
        domain_search = []
        if search:
            kwargs["search"] = search
            search = kwargs["search"]
            domain_search = [('name', 'ilike', search),
                             ]
            url = '/page/hotel'
            hotels = request.env['product.template'].search(
                domain_search, order='write_date desc')

        if star:
            kwargs["star"] = star
            star = kwargs["star"]
            domain += [
                ('rating', 'ilike', star),
                ('hotel_ok', '=', True),
            ]
        if attrib_set:
            for att in attrib_set:
                domain += [('facility_ids', 'in', attrib_set),
                           ('facility_ids', 'in', att)]

        pricelist = request.website.get_current_pricelist()
        if range:
            if range == '1':
                domain += [('converted_amount', '>=', 1),
                           ('converted_amount', '<=', 100)]
            elif range == '2':
                domain += [('converted_amount', '>=', 101),
                           ('converted_amount', '<=', 200)]
            elif range == '3':
                domain += [('converted_amount', '>=', 201),
                           ('converted_amount', '<=', 300)]
            elif range == '4':
                domain += [('converted_amount', '>=', 301),
                           ('converted_amount', '<=', 400)]
            elif range == '5':
                domain += [('converted_amount', '>=', 401)]
            domain += domain_search
            hotels = request.env['product.template'].sudo().search(domain)
            facilities = request.env['product.facility'].search([])
        return request.render('website_tour_agency.hotel', {
            'keep': keep,
            'search': search,
            'hotels': hotels,
            'star': star,
            'facilities': facilities,
            'facilities_selected': attrib_set,
            'range_selected': range,
            'pricelist': pricelist,
        })

    @http.route(['/searchpackage'], type='json', auth="public", website=True)
    def search_packages_from_searchbar(self, **post):
        keep = QueryURL('/')
        domain = [('website_published', '=', True)]
        search = post.get('destination')
        date = post.get('checkin')
        packages = False
        if search == '':
            url = '/page/packages'
            packages = request.env['sale.order'].sudo().search(
                domain, order='write_date desc')
        if search:
            domain_search = [
                ('name', 'ilike', search),
                ('website_published', '=', True)]
        # TODO: NEED TO ADD DATE SEArCH
        # if date:
        #     domain += [
        #         ('rating', 'ilike', star),
        #         ('hotel_ok', '=', True),
        #     ]
            url = '/page/packages'
            packages = request.env['sale.order'].sudo().search(
                domain_search, order='write_date desc')
        return request.env['ir.ui.view'].render_template(
            'website_tour_agency.packages', {
                'keep': keep,
                'search': search,
                'packages': packages,
        })

    @http.route(['/searchhotel'], type='json', auth="public", website=True)
    def search_hotel_from_searchbar(self, **post):
        keep = QueryURL('/')
        domain = [('hotel_ok', '=', True)]
        url = '/page/hotel'
        domain_search = []
        search = post.get('destination')
        if search == ' ':
            url = '/page/hotel'
            hotels = request.env['product.template'].search(
                domain, order='write_date desc')

        if search:
            # kwargs["search"] = search
            # search = kwargs["search"]
            domain_search = [('name', 'ilike', search),
                             ]
            url = '/page/hotel'
            hotels = request.env['product.template'].search(
                domain_search, order='write_date desc')
        if range:
            if range == '1':
                domain += [('converted_amount', '>=', 1),
                           ('converted_amount', '<=', 100)]
            elif range == '2':
                domain += [('converted_amount', '>=', 101),
                           ('converted_amount', '<=', 200)]
            elif range == '3':
                domain += [('converted_amount', '>=', 201),
                           ('converted_amount', '<=', 300)]
            elif range == '4':
                domain += [('converted_amount', '>=', 301),
                           ('converted_amount', '<=', 400)]
            elif range == '5':
                domain += [('converted_amount', '>=', 401)]
            domain += domain_search
        hotels = request.env['product.template'].sudo().search(domain)
        # facilities = request.env['product.facility'].search([])
        return request.env['ir.ui.view'].render_template('website_tour_agency.hotel', {
            'keep': keep,
            'search': search,
            'hotels': hotels,
            # 'star': 0,
            # 'facilities': facilities,
            # 'facilities_selected': facilities,
            # 'range_selected': 0,
            'pricelist': request.website.get_current_pricelist(),
        })






    @http.route(["/hotel/details/<model('product.template'):hotel>"],
                type='http', auth="public",
                website=True)
    def hotel_details_view(self, hotel, **post):
        values = {'template': hotel,
                  'countries': request.env['res.country'].sudo().search([]),
                  'company': request.env['res.company'].sudo().search([])}
        return request.render('website_tour_agency.hotel_details_page', values)

    @http.route(["/hotel/details/<model('product.template'):hotel>/book"],
                type='http', auth="public", website=True)
    def hotel_book_view(self, hotel, **post):
        values = {'hotel': hotel,
                  'countries': request.env['res.country'].sudo().search([]),
                  'company': request.env['res.company'].sudo().search([])}
        return request.render('website_tour_agency.hotel_book_now', values)

    @http.route(["/package/details/<model('sale.order'):order>"],
                type='http', auth="public", website=True)
    def package_details_view(self, order, **post):
        values = {'template': order,
                  'countries': request.env['res.country'].sudo().search([]),
                  'company': request.env['res.company'].sudo().search([])}
        return request.render('website_tour_agency.inherit_so_template', values)

    @http.route(["/package/details/<model('sale.order'):order>/book"],
                type='http', auth="public", website=True)
    def package_book_view(self, order, **post):
        values = {'template': order,
                  'countries': request.env['res.country'].sudo().search([]),
                  'company': request.env['res.company'].sudo().search([])}
        return request.render('website_tour_agency.package_book_now', values)

    @http.route(['/get_all_lat_lang'], type='json', auth="public", website=True)
    def geo_latitude_longitude(self, **kwargs):
        res_hotel = request.env['product.template']
        domain = [('hotel_ok', '=', True)]
        hotels = res_hotel.sudo().search(domain)
        all_company_data = []
        for hotel in hotels:
            addd = rp.geo_query_address(street=hotel.street, zip=hotel.zip,
                                        city=hotel.city,
                                        state=hotel.state_id.name,
                                        country=hotel.country_id.name)
            result = rp.geo_find(addd)
            if result:
                company_info = hotel.name
                if hotel.street:
                    company_info += ',' + hotel.street
                if hotel.street2:
                    company_info += ',' + hotel.street2
                if hotel.city:
                    company_info += ',' + hotel.city
                if hotel.zip:
                    company_info += '-' + str(hotel.zip)
                if hotel.state_id:
                    company_info += ',' + str(hotel.state_id.name)
                if hotel.country_id:
                    company_info += ',' + str(hotel.country_id.name)
                company_data = [company_info, result[0], result[1]]
                all_company_data.append(company_data)
        return all_company_data

    @http.route(['/get_single_lat_lang'], type='json',
                auth="public", website=True)
    def get_single_lat_lang(self, **kwargs):
        company = request.env['product.template'].browse(
            int(kwargs.get('selected_company_id')))
        result = geo_find(geo_query_address(
            street=company.street,
            zip=company.zip,
            city=company.city,
            state=company.state_id.name,
            country=company.country_id.name))
        company_data = []
        if result:
            company_info = company.name
            if company.street:
                company_info += ',' + company.street
            if company.street2:
                company_info += ',' + company.street2
            if company.city:
                company_info += ',' + company.city
            if company.zip:
                company_info += '-' + str(company.zip)
            if company.state_id:
                company_info += ',' + str(company.state_id.name)
            if company.country_id:
                company_info += ',' + str(company.country_id.name)
            company_data = [company_info, result[0], result[1]]
        return company_data

    @http.route(["/page/visa/apply"], type='http', auth="public", website=True)
    def visa_apply(self, **post):
        domain = ['|', ('categ_id.name', '=', 'Visa'),
                  ('categ_id.parent_id.name', '=', 'Visa')]
        visa_list = request.env['product.template'].sudo().search(domain)
        values = {
            'visas': visa_list,
            'countries': request.env['res.country'].sudo().search([]),
            'company': request.env['res.company'].sudo().search([])}
        return request.render('website_tour_agency.visa_apply_now', values)

    @http.route(["/page/contactus_thanks_travel"], type='http',
                auth="public", website=True)
    def contact_us(self, **post):
        return request.render('website_tour_agency.contactus_thanks_travel')

    @http.route(["/page/tours"], type='http', auth="public", website=True)
    def get_tours(self, **post):
        return request.render('website_tour_agency.tours')

    @http.route(["/page/visa"], type='http', auth="public", website=True)
    def get_visa(self, **post):
        return request.render('website_tour_agency.visa')

    @http.route(["/page/transportation"], type='http',
                auth="public", website=True)
    def get_trasportation(self, **post):
        return request.render('website_tour_agency.transportation')


class WebsiteShopRestrict(WebsiteSale):

    @http.route([
        '/shop',
        '/shop/page/<int:page>',
        '/shop/category/<model("product.public.category"):category>',
        '/shop/category/<model("product.public.category"):category>/page/<int:page>'
    ], type='http', auth="public", website=True)
    def shop(self, page=0, category=None, search='', ppg=False, **post):
        return request.render("website.page_404", {})


class WebsiteFormExtended(WebsiteForm):

    @http.route('/website_form/<string:model_name>', type='http',
                auth="public", methods=['POST'], website=True)
    def website_form(self, model_name, **kwargs):
        res = super(WebsiteFormExtended, self).website_form(model_name,
                                                            **kwargs)
        res_id = request.session.get('form_builder_id')
        files = [y for x, y in kwargs.items() if 'file_' in x]
        if files != '':
            attachment_value = {
                'name': 'Attachments',
                'datas': base64.b64encode(bytes(str(files), 'utf-8')),
                'datas_fname': 'File name',
                'res_model': 'crm.lead',
                'res_id': res_id,
            }
            attachment_id = request.env[
                'ir.attachment'].sudo().create(attachment_value)
        return res
