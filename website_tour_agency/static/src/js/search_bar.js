odoo.define('website_tour_agency.tt_website_rpc', function(require) {
    var odoo = require('web.ajax');
    // search from home for package
    $(document).on('click', '#home_search_package', function(e) {
        var $this = $(this);
        odoo.jsonRpc("/searchpackage", 'call', {
            'destination': $("#destination").val(),
            'checkin': $("#checkin").val(),
        }).then(function(data) {
            $('main').fadeOut("slow", function() {
                $('main').html(data).fadeIn("slow");
            });
        });
    });



    // SEArCH Hotels
    $(document).on('click', '#home_search_hotel', function(e) {
        var $this = $(this);
        odoo.jsonRpc("/searchhotel", 'call', {
            'destination': $("#destination").val(),
            'checkin': $("#checkin").val(),
        }).then(function(data) {
            $('main').fadeOut("slow", function() {
                $('main').html(data).fadeIn("slow");
            });
        });
    });
});
