odoo.define('website_tour_agency.website_tour_agency', function (require) {
"use strict";

    var core = require('web.core');
    var website = require('website.website');

    $(document).ready(function(){

        // When choosing an visa, display its required documents
        $("#visa_select").on("change", "select[name='product_id']", function (ev) {
            var payment_id = $(ev.currentTarget).val();
            $('.list_item').addClass("hidden");
            $("#"+payment_id).removeClass("hidden");
        })

    	$('[data-toggle="tooltip"]').tooltip();

    	$('.cd-filter-block h4').on('click', function(){
    		$(this).toggleClass('closed').siblings('.cd-filter-content').slideToggle(300);
    	})

    	var nowDate = new Date();
    	var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);

    	$('#StartdatePicker').datepicker({
    		format:'yyyy-mm-dd'
        })
    	$('#ReturndatePicker').datepicker({
    		format:'yyyy-mm-dd'
        })

    	$('#t_date').datetimepicker({
    	    minDate: today,
    	    format: 'DD/MM/YYYY',
    	    showTodayButton: true,
    	    showClear: true,
    	    useCurrent: false,
    	});
    	$('#r_date').datetimepicker({
    	    minDate: today,
    	    format: 'DD/MM/YYYY',
    	    showTodayButton: true,
    	    showClear: true,
    	    useCurrent: false
    	});

    	$("#t_date").on("dp.change", function (e) {
	         $('#r_date').data("DateTimePicker").setMinDate(e.date);
	     });
	     $("#r_date").on("dp.change", function (e) {
	         $('#t_date').data("DateTimePicker").setMaxDate(e.date);
	     });


//    	$(document).on('click', '.btn-add', function(e)
//		    {
//		        e.preventDefault();
//
//		        var controlForm = $('.controls:first'),
//		            currentEntry = $(this).parents('.entry:first'),
//		            newEntry = $(currentEntry.clone()).appendTo(controlForm);
//
//		        newEntry.find('input').val('');
//		        controlForm.find('.entry:not(:last) .btn-add')
//		            .removeClass('btn-add').addClass('btn-remove')
////		            .removeClass('btn-success').addClass('btn-danger')
//		            .html('<span class="glyphicon glyphicon-minus"></span>');
//		    }).on('click', '.btn-remove', function(e)
//		    {
//		      $(this).parents('.entry:first').remove();
//
//				e.preventDefault();
//				return false;
//			});
    	var counter = 1;
    	$(".btn-add").on("click", function () {
            var newRow = $("<div>");
            var cols = "";

            cols += '<div class="entry input-group col-xs-3 mt8"><input class="btn" type="file" name="attachment_' + counter + '"/><span class="input-group-btn"><button class="btn ibtnDel" type="button"><span class="fa fa-minus"></span></button></span></div>';
            newRow.append(cols);
            if (counter == 50) $('#addrow').attr('disabled', true).prop('value', "You've reached the limit");
            $("div.controls").append(newRow);
            counter++;
        });
    	$("div.controls").on("click", ".ibtnDel", function (event) {
            $(this).closest("div").remove();
            counter -= 1
        });

    	$(".show-more a").each(function() {
    	    var $link = $(this);
    	    var $content = $link.parent().prev("div.text-content");
    	    var visibleHeight = $content[0].clientHeight;
    	    var actualHide = $content[0].scrollHeight - 1;

    	    if (actualHide > visibleHeight) {
    	        $link.show();
    	    } else {
    	        $link.hide();
    	    }
    	});

    	$(".show-more a").on("click", function() {
    	    var $link = $(this);
    	    var $content = $link.parent().prev("div.text-content");
    	    var linkText = $link.text();

    	    $content.toggleClass("short-text, full-text");

    	    $link.text(getShowLinkText(linkText));

    	    return false;
    	});

    	function getShowLinkText(currentText) {
    	    var newText = '';

    	    if (currentText.toUpperCase() === "SHOW MORE") {
    	        newText = "Show less";
    	    } else {
    	        newText = "Show more";
    	    }

    	    return newText;
    	}
    	/*FOR FACILITY ICONS*/

    	$(".show-more-facility a").each(function() {
    	    var $link = $(this);
    	    var $content = $link.parent().prev("div.text-content-facility");
    	    var visibleHeight = $content[0].clientHeight;
    	    var actualHide = $content[0].scrollHeight - 1;

    	    if (actualHide > visibleHeight) {
    	        $link.show();
    	    } else {
    	        $link.hide();
    	    }
    	});

    	$(".show-more-facility a").on("click", function() {
    	    var $link = $(this);
    	    var $content = $link.parent().prev("div.text-content-facility");
    	    var linkText = $link.text();

    	    $content.toggleClass("short-text-facility, full-text-facility");

    	    $link.text(getShowLinkText(linkText));

    	    return false;
    	});

    	function getShowLinkText(currentText) {
    	    var newText = '';

    	    if (currentText.toUpperCase() === "SHOW MORE") {
    	        newText = "Show less";
    	    } else {
    	        newText = "Show more";
    	    }

    	    return newText;
    	}

        $(function(){
            $('#type').change(function(){
                var opt = $(this).val();
                    if(opt == 'hotel'){
                        $('.search_button_package').hide();
                        $('.search_button_hotel').show();
                    }else{
                        $('.search_button_package').show();
                        $('.search_button_hotel').hide();
                    }
            });
        });
    	/*=================end===================*/

    	$('#clear_fil').click(function() {
    	    $('input[name=star]').prop('checked', false);
    	    window.location.href='/page/hotel';
    	});


    	$(".owl-carousel").owlCarousel({
    		navigation : true, // Show next and prev buttons
//    		nav: true,
//    		navText: ['<button id="owl-prv" class="btn btn-primary fa fa-angle-left" data-toggle="tooltip" title="Prev"/>',
//                      '<button id="owl-nxt" class="btn btn-primary fa fa-angle-right" data-toggle="tooltip" title="Next"/>'],
    	      slideSpeed : 300,
    	      paginationSpeed : 400,

    	      items : 1,
    	      itemsDesktop : false,
    	      itemsDesktopSmall : false,
    	      itemsTablet: false,
    	      itemsMobile : false
    	});

    	$(window).scroll(function () {
            $('#back-to-top').tooltip('hide');
                if ($(this).scrollTop() > 50) {
                    $('#back-to-top').fadeIn();
                } else {
                    $('#back-to-top').fadeOut();
                }
        });

        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
//        $('#back-to-top').tooltip({ 'content': 'I should appear beneath "Foo"', 'items': '*' })
    });
});
