# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Website Tour Agency',
    'author': 'Serpent Consulting Services Pvt. Ltd.',
    'summary': 'Website of Tours and Packages',
    'category': 'website',
    'website': 'http://www.serpentcs.com',
    'license': 'LGPL-3',
    'version': '12.0.1.0.0',
    'sequence': 1,
    'depends': ['rating',
                'website',
                'product',
                'tour_travel_package_design',
                'website_crm',
                'website_sale',
                'website_rating'
                ],
    'data': [
        'security/ir.model.access.csv',
        'data/product_category_data.xml',
        'data/website_data.xml',
        'data/product_data.xml',
        'views/assets.xml',
        'views/templates.xml',
        'views/packages.xml',
        'views/transportation.xml',
        'views/hotels.xml',
        'views/hotel_details.xml',
        'views/visa.xml',
        'views/tours.xml',
        'views/packages_details.xml',
        'views/sale_quote_template_inherit.xml',
        'views/snippets.xml',
        'views/package_book.xml',
        'views/hotel_book.xml',
        'views/visa_apply.xml',

    ],
    'images': ['static/src/img/travel-website.jpg'],
    'installable': True,
    'auto_install': False
}
