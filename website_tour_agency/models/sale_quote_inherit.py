# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models
from datetime import datetime


class Packagetags(models.Model):
    _name = "package.tag"
    _description = "Package tags"

    name = fields.Char('Package Tag')


class SaleOrderInheritForWebsite(models.Model):
    _inherit = "sale.order"

    image_medium = fields.Binary("Medium-sized image")
    package_tag = fields.Many2one('package.tag', 'Package Tag',
                                  help="This Tag will be display on package.")
    package_overview = fields.Text('Overview')
    package_images = fields.One2many('package.image', 'package_sale_id',
                                     'Images')
    package_discription = fields.Char(string='Package Discription', )
    video_url = fields.Char(
        string='Video URL',
        help="//www.youtube.com/embed/mwuPTI8AT7M?rel=0")


class HotelInformationLine(models.Model):
    _inherit = 'product.category'

    @api.depends('product_count')
    def get_product_name(self):
        """
        Get the products which fall in defined category.

         ---------------------------------
        @param self : object pointer
        """
        for rec in self:
            products = self.env['product.template'].search([('categ_id', 'in',
                                                             rec.ids)])
            product_name_list = []
            for product in products:
                product_name_list.append(product.name)
            rec.product_list = ", ".join(product_name_list)

    product_list = fields.Char(compute='get_product_name')


class PackageImage(models.Model):
    _name = 'package.image'
    _description = "Package Image"

    name = fields.Char(string='Name')
    image = fields.Binary(string='Image')
    image_small = fields.Binary(string='Small Image')
    package_sale_id = fields.Many2one("sale.order", copy=False)
    doc_name = fields.Char(
        string='Filename')
