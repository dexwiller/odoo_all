# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.
##############################################################################
from odoo import api, fields, models, _
from datetime import datetime,date
from dateutil.relativedelta import relativedelta
 
    
class Res_Partner(models.Model):
    _inherit = 'res.partner'
    
    @api.multi
    def _cron_send_overdue_statement(self):
        partners = self.env['res.partner'].search([('customer','=',True)])
        partners.do_partner_mail()
        return True
    
    @api.multi
    def _cron_send_customer_statement(self):
        partners = self.env['res.partner'].search([('customer','=',True)])
        if self.env.user.company_id.period == 'monthly':
            partners.do_process_monthly_statement_filter()
            partners.customer_monthly_send_mail()
        else:
            partners.customer_send_mail()

        return True
    
    @api.multi
    def _get_amounts_and_date_amount(self):
        user_id = self._uid
        filter_amount_due = 0.0
        filter_amount_overdue = 0.0
        filter_supplier_amount_due = 0.0
        filter_supplier_amount_overdue = 0.0
        
        company = self.env['res.users'].browse(user_id).company_id
        current_date = datetime.now().strftime("%Y-%m-%d")

        
        for partner in self:
            amount_due = amount_overdue = 0.0
            supplier_amount_due = supplier_amount_overdue = 0.0

            for aml in partner.balance_invoice_ids:
#                 if (aml.company_id == company):
                date_maturity = aml.date_due or aml.date
                amount_due += aml.result
                
                if (date_maturity <= datetime.strptime(current_date,"%Y-%m-%d").date()):
                    amount_overdue += aml.result
            
            partner.payment_amount_due_amt = amount_due
            partner.update({'payment_amount_overdue_amt': amount_overdue})
            partner.payment_amount_overdue_amt = amount_overdue
            
            for aml in partner.supplier_invoice_ids:
#                 if (aml.company_id == company):
                date_maturity = aml.date_due or aml.date
                supplier_amount_due += aml.result
                if (date_maturity <= datetime.strptime(current_date,"%Y-%m-%d").date()):
                    supplier_amount_overdue += aml.result
            partner.payment_amount_due_amt_supplier = supplier_amount_due
            partner.payment_amount_overdue_amt_supplier = supplier_amount_overdue
            
            for aml in partner.customer_statement_line_ids:
                if aml.date_due != False:
                    date_maturity = aml.date_due
                    filter_amount_due += aml.result
                    if (date_maturity <= datetime.strptime(current_date,"%Y-%m-%d").date()):
                        filter_amount_overdue += aml.result
            partner.filter_payment_amount_due_amt = filter_amount_due
            partner.filter_payment_amount_overdue_amt = filter_amount_overdue
            
            for aml in partner.vendor_statement_line_ids:
                date_maturity = aml.date_due
                filter_supplier_amount_due += aml.result
                if (date_maturity <= datetime.strptime(current_date,"%Y-%m-%d").date()):
                    filter_supplier_amount_overdue += aml.result
            partner.filter_payment_amount_due_amt_supplier = filter_supplier_amount_due
            partner.filter_payment_amount_overdue_amt_supplier = filter_supplier_amount_overdue
            
            
            monthly_amount_due_amt = monthly_amount_overdue_amt = 0.0
            for aml in partner.monthly_statement_line_ids:
                date_maturity = aml.date_due
                monthly_amount_due_amt += aml.result
                if (date_maturity <= datetime.strptime(current_date,"%Y-%m-%d").date()):
                    monthly_amount_overdue_amt += aml.result
            partner.monthly_payment_amount_due_amt = monthly_amount_due_amt
            partner.monthly_payment_amount_overdue_amt = monthly_amount_overdue_amt
            
            
            

    supplier_invoice_ids = fields.One2many('account.invoice', 'partner_id', 'Customer move lines', domain=[ '&', ('type', 'in', ['in_invoice','in_refund']), '&', ('state', 'in', ['open', 'paid'])]) 
    balance_invoice_ids = fields.One2many('account.invoice', 'partner_id', 'Customer move lines', domain=[ '&', ('type', 'in', ['out_invoice','out_refund']), '&', ('state', 'in', ['open', 'paid'])]) 
    
#     customer_state_ids = fields.One2many('account.move.line', 'partner_id', 'Customer move lines', domain=[ '&', ('reconciled', '=', False), '&', ('account_id.internal_type', '=', 'receivable')])
#     unreconciled_aml_ids = fields.One2many('account.move.line', 'partner_id', 'Move lines')
    
    payment_amount_due_amt = fields.Float(compute='_get_amounts_and_date_amount', string="Balance Due")
    payment_amount_overdue_amt = fields.Float(compute='_get_amounts_and_date_amount',
                                                  string="Total Overdue Amount")
    payment_amount_due_amt_supplier = fields.Float(compute='_get_amounts_and_date_amount', string="Supplier Balance Due")
    payment_amount_overdue_amt_supplier = fields.Float(compute='_get_amounts_and_date_amount',
                                                  string="Total Supplier Overdue Amount")
    
    monthly_statement_line_ids = fields.One2many('monthly.statement.line', 'partner_id', 'Monthly Statement Lines')
    customer_statement_line_ids = fields.One2many('bi.statement.line', 'partner_id', 'Customer Statement Lines')
    vendor_statement_line_ids = fields.One2many('bi.vendor.statement.line', 'partner_id', 'Supplier Statement Lines')
    
    
    filter_payment_amount_due_amt = fields.Float(compute='_get_amounts_and_date_amount', string="Balance Due")
    filter_payment_amount_overdue_amt = fields.Float(compute='_get_amounts_and_date_amount',
                                                  string="Total Overdue Amount")
    
    
    monthly_payment_amount_due_amt = fields.Float(compute='_get_amounts_and_date_amount', string="Balance Due")
    monthly_payment_amount_overdue_amt = fields.Float(compute='_get_amounts_and_date_amount',
                                                  string="Total Overdue Amount")
    
    
    filter_payment_amount_due_amt_supplier = fields.Float(compute='_get_amounts_and_date_amount', string="Supplier Balance Due")
    filter_payment_amount_overdue_amt_supplier = fields.Float(compute='_get_amounts_and_date_amount',
                                                  string="Total Supplier Overdue Amount")
    
    statement_from_date = fields.Date('From Date')
    statement_to_date = fields.Date('To Date')
    
    vendor_statement_from_date = fields.Date('From Date')
    vendor_statement_to_date = fields.Date('To Date')
    first_thirty_day = fields.Float(string="0-30",compute="compute_zero_thirty_days")
    thirty_sixty_days = fields.Float(string="30-60",
        compute="compute_thirty_sixty_days"
        )
    sixty_ninty_days = fields.Float(string="60-90",
        compute="compute_sixty_ninty_days"
        )
    ninty_plus_days = fields.Float(string="90+",
        compute="compute_ninty_plus_days"
        )
    total = fields.Float(string="Total",
        compute="compute_total"
        )



    first_thirty_day_filter = fields.Float(string="0-30",compute="compute_zero_thirty_days_filter")
    thirty_sixty_days_filter = fields.Float(string="30-60",
        compute="compute_thirty_sixty_days_filter"
        )
    sixty_ninty_days_filter = fields.Float(string="60-90",
        compute="compute_sixty_ninty_days_filter"
        )
    ninty_plus_days_filter = fields.Float(string="90+",
        compute="compute_ninty_plus_days_filter"
        )
    total_filter = fields.Float(string="Total",
        compute="compute_total_filter"
        )

    today_date = fields.Date(default=fields.Date.today())
    

    @api.one
    @api.depends('customer_statement_line_ids')
    def compute_zero_thirty_days_filter(self):
        if self.statement_to_date :
            #today = datetime.strptime(self.statement_to_date,'%Y-%m-%d').date()
            
            for line in self.customer_statement_line_ids :
            
                #date1 = datetime.strptime(line.date_invoice,'%Y-%m-%d').date()
                
                diff = self.statement_to_date - line.date_invoice
                
                if diff.days <= 30:
                    print (line.number)
                    self.first_thirty_day_filter = self.first_thirty_day_filter + line.result
        return

    @api.one
    @api.depends('customer_statement_line_ids')
    def compute_thirty_sixty_days_filter(self):
        if self.statement_to_date :
            #today = datetime.strptime(self.statement_to_date,'%Y-%m-%d').date()
            
            for line in self.customer_statement_line_ids :
            
                #date1 = datetime.strptime(line.date_invoice,'%Y-%m-%d').date()
                
                diff = self.statement_to_date - line.date_invoice
               
                if diff.days > 30 and diff.days<=60:
                    print (line.number)
                    self.thirty_sixty_days_filter = self.thirty_sixty_days_filter + line.result
        return

    @api.one
    @api.depends('customer_statement_line_ids')
    def compute_sixty_ninty_days_filter(self):
        if self.statement_to_date :
            #today = datetime.strptime(self.statement_to_date,'%Y-%m-%d').date()
            
            for line in self.customer_statement_line_ids :
                
                #date1 = datetime.strptime(line.date_invoice,'%Y-%m-%d').date()
                
                diff = self.statement_to_date - line.date_invoice
                
                if diff.days > 60 and diff.days<=90:
                    self.sixty_ninty_days_filter = self.sixty_ninty_days_filter + line.result

        return

    @api.one
    @api.depends('customer_statement_line_ids')
    def compute_ninty_plus_days_filter(self):
        if self.statement_to_date :
            #today = datetime.strptime(self.statement_to_date,'%Y-%m-%d').date()
            
            for line in self.customer_statement_line_ids :
                
                #date1 = datetime.strptime(line.date_invoice,'%Y-%m-%d').date()
                
                diff = self.statement_to_date - line.date_invoice
               
                if diff.days > 90  :
                    self.ninty_plus_days_filter = self.ninty_plus_days_filter + line.result

        return

    @api.one
    @api.depends('ninty_plus_days_filter','sixty_ninty_days_filter','thirty_sixty_days_filter','first_thirty_day_filter')
    def compute_total_filter(self):
        self.total_filter = self.ninty_plus_days_filter + self.sixty_ninty_days_filter + self.thirty_sixty_days_filter + self.first_thirty_day_filter
        return


    @api.one
    @api.depends('balance_invoice_ids')
    def compute_zero_thirty_days(self):
        today = fields.date.today()
        
        for line in self.balance_invoice_ids :
        
            #date1 = datetime.strptime(line.date_invoice,'%Y-%m-%d').date()
            
            diff = today - line.date_invoice
           
            if diff.days <= 30:
                print (line.number)
                self.first_thirty_day = self.first_thirty_day + line.result

        return
    @api.one
    @api.depends('balance_invoice_ids')
    def compute_thirty_sixty_days(self):
        today = fields.date.today()
        for line in self.balance_invoice_ids :
           
            #date1 = datetime.strptime(line.date_invoice,'%Y-%m-%d').date()
            
            diff = today - line.date_invoice
            
            if diff.days > 30 and diff.days<=60:
                self.thirty_sixty_days = self.thirty_sixty_days + line.result

        return

    @api.one
    @api.depends('balance_invoice_ids')
    def compute_sixty_ninty_days(self):
        today = fields.date.today()
        for line in self.balance_invoice_ids :
            
            #date1 = datetime.strptime(line.date_invoice,'%Y-%m-%d').date()
            
            diff = today - line.date_invoice
            
            if diff.days > 60 and diff.days<=90:
                self.sixty_ninty_days = self.sixty_ninty_days + line.result

        return

    @api.one
    @api.depends('balance_invoice_ids')
    def compute_ninty_plus_days(self):
        today = fields.date.today()
        for line in self.balance_invoice_ids :
            
            #date1 = datetime.strptime(line.date_invoice,'%Y-%m-%d').date()
            
            diff = today - line.date_invoice
           
            if diff.days > 90  :
                self.ninty_plus_days = self.ninty_plus_days + line.result

        return

    @api.one
    @api.depends('ninty_plus_days','sixty_ninty_days','thirty_sixty_days','first_thirty_day')
    def compute_total(self):
        self.total = self.ninty_plus_days + self.sixty_ninty_days + self.thirty_sixty_days + self.first_thirty_day
        return

        
    @api.multi        
    def do_partner_mail(self):
        unknown_mails = 0
        for partner in self:
            partners_to_email = [child for child in partner.child_ids if child.type == 'invoice' and child.email]
            if not partners_to_email and partner.email:
                partners_to_email = [partner]
            if partners_to_email and partner.payment_amount_overdue_amt != 0:
                for partner_to_email in partners_to_email:
                    mail_template_id = self.env['ir.model.data'].xmlid_to_object('bi_customer_overdue_statement.email_template_account_followup_default_id')
                    mail_template_id.send_mail(partner_to_email.id)
                if partner not in partners_to_email:
                        self.message_post([partner.id], body=_('Overdue email sent to %s' % ', '.join(['%s <%s>' % (partner.name, partner.email) for partner in partners_to_email])))
        return unknown_mails
    
    
    @api.multi
    def customer_monthly_send_mail(self):
        unknown_mails = 0
        for partner in self:
            partners_to_email = [child for child in partner.child_ids if child.type == 'invoice' and child.email]
            if not partners_to_email and partner.email:
                partners_to_email = [partner]
            if partners_to_email:
                for partner_to_email in partners_to_email:
                    mail_template_id = self.env['ir.model.data'].xmlid_to_object('bi_customer_overdue_statement.email_template_customer_monthly_statement')
                    mail_template_id.send_mail(partner_to_email.id)
                if partner not in partner_to_email:
                    self.message_post([partner.id], body=_('Customer Monthly Statement email sent to %s' % ', '.join(['%s <%s>' % (partner.name, partner.email) for partner in partners_to_email])))
        return unknown_mails
    
    @api.multi
    def customer_send_mail(self):
        unknown_mails = 0
        for partner in self:
            partners_to_email = [child for child in partner.child_ids if child.type == 'invoice' and child.email]
            if not partners_to_email and partner.email:
                partners_to_email = [partner]
            if partners_to_email:
                for partner_to_email in partners_to_email:
                    mail_template_id = self.env['ir.model.data'].xmlid_to_object('bi_customer_overdue_statement.email_template_customer_statement')
                    mail_template_id.send_mail(partner_to_email.id)
                if partner not in partner_to_email:
                    self.message_post([partner.id], body=_('Customer Statement email sent to %s' % ', '.join(['%s <%s>' % (partner.name, partner.email) for partner in partners_to_email])))
        return unknown_mails
    
    @api.multi
    def do_process_statement_filter(self):
        account_invoice_obj = self.env['account.invoice'] 
        statement_line_obj = self.env['bi.statement.line']
        account_payment_obj = self.env['account.payment']
        inv_list = []
        for record in self:
            from_date = record.statement_from_date 
            to_date = record.statement_to_date
            domain_payment = [('partner_type', '=', 'customer'), ('state', 'in', ['posted', 'reconciled']), ('partner_id', '=', record.id)]
            domain = [('type', 'in', ['out_invoice','out_refund']), ('state', 'in', ['open', 'paid']), ('partner_id', '=', record.id)]
            if from_date:
                domain.append(('date_invoice', '>=', from_date))
                domain_payment.append(('payment_date', '>=', from_date))
            if to_date:
                domain.append(('date_invoice', '<=', to_date))
                domain_payment.append(('payment_date', '<=', to_date))
                 
                 
            lines_to_be_delete = statement_line_obj.search([('partner_id', '=', record.id)])
            lines_to_be_delete.unlink()
            
            
            
            invoices = account_invoice_obj.search(domain)
            payments = account_payment_obj.search(domain_payment)
            if invoices:
                for invoice in invoices.sorted(key=lambda r: r.number):
                    vals = {
                            'partner_id':invoice.partner_id.id or False,
                            'state':invoice.state or False,
                            'date_invoice':invoice.date_invoice,
                            'date_due':invoice.date_due,
                            'number':invoice.number or '',
                            'result':invoice.result or 0.0,
                            'name':invoice.name or '',
                            'amount_total':invoice.amount_total or 0.0,
                            'credit_amount':invoice.credit_amount or 0.0,
                            'invoice_id' : invoice.id,
                    }
                    test = statement_line_obj.create(vals)
            if payments:
                for payment in payments.sorted(key=lambda r: r.name):
                    credit_amount = 0.0
                    debit_amount = 0.0
                    if not payment.invoice_ids:
                        for move in payment.move_line_ids:
                            if move.account_id.internal_type == 'receivable':
                                if not move.full_reconcile_id:
                                    debit_amount = move.debit
                                    credit_amount = move.credit
                    else:
                        for move in payment.move_line_ids:
                            if move.account_id.internal_type == 'receivable':
                                if move.reconciled:
                                    pass
                                else:
                                    for matched_id in move.matched_debit_ids:
                                        debit_amount = matched_id.amount
                                        credit_amount = move.credit
                    if debit_amount != 0.0 or credit_amount != 0.0:
                        vals = {
                                    'partner_id':payment.partner_id.id or False,
                                    'date_invoice':payment.payment_date,
                                    'date_due':payment.payment_date,
                                    'number':payment.name or '',
                                    'result':debit_amount - credit_amount or 0.0,
                                    'name':payment.name or '',
                                    'amount_total':debit_amount or 0.0,
                                    'credit_amount': credit_amount or 0.0,
                                    'payment_id' : payment.id,
                        }
                        statement_line_obj.create(vals)
                
    
    @api.multi
    def do_process_monthly_statement_filter(self):
        account_invoice_obj = self.env['account.invoice'] 
        statement_line_obj = self.env['monthly.statement.line']
        for record in self:
 
            today = date.today()
            d = today - relativedelta(months=1)

            start_date = date(d.year, d.month,1)
            end_date = date(today.year, today.month,1) - relativedelta(days=1)
            
            from_date = str(start_date)
            to_date = str(end_date)
            
            
            domain = [('type', 'in', ['out_invoice','out_refund']), ('state', 'in', ['open', 'paid']), ('partner_id', '=', record.id)]
            if from_date:
                domain.append(('date_invoice', '>=', from_date))
            if to_date:
                domain.append(('date_invoice', '<=', to_date))
                 
                 
            lines_to_be_delete = statement_line_obj.search([('partner_id', '=', record.id)])
            lines_to_be_delete.unlink()
            
            invoices = account_invoice_obj.search(domain)
            
            for invoice in invoices.sorted(key=lambda r: r.number):
                vals = {
                        'partner_id':invoice.partner_id.id or False,
                        'state':invoice.state or False,
                        'date_invoice':invoice.date_invoice,
                        'date_due':invoice.date_due,
                        'number':invoice.number or '',
                        'result':invoice.result or 0.0,
                        'name':invoice.name or '',
                        'amount_total':invoice.amount_total or 0.0,
                        'credit_amount':invoice.credit_amount or 0.0,
                        'invoice_id' : invoice.id,
                    }
                statement_line_obj.create(vals) 
                
    @api.multi
    def do_process_vendor_statement_filter(self):
        account_invoice_obj = self.env['account.invoice'] 
        vendor_statement_line_obj = self.env['bi.vendor.statement.line']
        account_payment_obj = self.env['account.payment']
        for record in self:
            from_date = record.vendor_statement_from_date 
            to_date = record.vendor_statement_to_date
            domain_payment = [('partner_type', '=', 'supplier'), ('state', 'in', ['posted', 'reconciled']), ('partner_id', '=', record.id)]
            domain = [('type', 'in', ['in_invoice','in_refund']), ('state', 'in', ['open', 'paid']), ('partner_id', '=', record.id)]
            if from_date:
                domain.append(('date_invoice', '>=', from_date))
                domain_payment.append(('payment_date', '>=', from_date))
            if to_date:
                domain.append(('date_invoice', '<=', to_date))
                domain_payment.append(('payment_date', '<=', to_date))
                 
                 
            lines_to_be_delete = vendor_statement_line_obj.search([('partner_id', '=', record.id)])
            lines_to_be_delete.unlink()
            
            invoices = account_invoice_obj.search(domain)
            payments = account_payment_obj.search(domain_payment)
            if invoices:
                for invoice in invoices.sorted(key=lambda r:r.number):
                    vals = {
                        'partner_id':invoice.partner_id.id or False,
                        'state':invoice.state or False,
                        'date_invoice':invoice.date_invoice,
                        'date_due':invoice.date_due,
                        'number':invoice.number or '',
                        'result':invoice.result or 0.0,
                        'name':invoice.name or '',
                        'amount_total':invoice.amount_total or 0.0,
                        'credit_amount':invoice.credit_amount or 0.0,
                        'invoice_id' : invoice.id,
                    }
                    vendor_statement_line_obj.create(vals)
            if payments:
                for payment in payments.sorted(key=lambda r: r.name):
                    credit_amount = 0.0
                    debit_amount = 0.0
                    if not payment.invoice_ids:
                        for move in payment.move_line_ids:
                            if move.account_id.internal_type == 'payable':
                                if not move.full_reconcile_id:
                                    print ("enterrrrrrrrrrrrrrrrr111111111111")
                                    debit_amount = move.debit
                                    credit_amount = move.credit
                    else:
                        for move in payment.move_line_ids:
                            if move.account_id.internal_type == 'payable':
                                if move.reconciled:
                                    pass
                                else:
                                    for matched_id in move.matched_credit_ids:
                                        print ("enterrrrrrrrrrrrrrrrr22222222222222222")
                                        debit_amount = move.debit
                                        credit_amount = matched_id.amount
                    if debit_amount != 0.0 or credit_amount != 0.0:
                        vals = {
                                'partner_id':payment.partner_id.id or False,
                                #'state':payment.state or False,
                                'date_invoice':payment.payment_date,
                                'date_due':payment.payment_date,
                                'number':payment.name or '',
                                'result':debit_amount -  credit_amount or 0.0,
                                'name':payment.name or '',
                                'amount_total':debit_amount or 0.0,
                                'credit_amount':credit_amount or 0.0,
                                'payment_id' : payment.id,
                        }
                        vendor_statement_line_obj.create(vals)
                

    @api.multi
    def do_send_statement_filter(self):
        unknown_mails = 0
        for partner in self:
            partners_to_email = [child for child in partner.child_ids if child.type == 'invoice' and child.email]
            if not partners_to_email and partner.email:
                partners_to_email = [partner]
            if partners_to_email:
                for partner_to_email in partners_to_email:
                    mail_template_id = self.env['ir.model.data'].xmlid_to_object('bi_customer_overdue_statement.email_template_customer_statement_filter')
                    mail_template_id.send_mail(partner_to_email.id)
                if partner not in partner_to_email:
                    self.message_post([partner.id], body=_('Customer Filter Statement email sent to %s' % ', '.join(['%s <%s>' % (partner.name, partner.email) for partner in partners_to_email])))
        return unknown_mails
                
    
    @api.multi
    def do_button_print(self):


        return self.env.ref('bi_customer_overdue_statement.report_customer_overdue_print').report_action(self)
    
    @api.multi
    def do_button_print_statement(self):
        return self.env.ref('bi_customer_overdue_statement.report_customert_print').report_action(self)
    
    @api.multi
    def do_button_print_vendor_statement(self):
        return self.env.ref('bi_customer_overdue_statement.report_supplier_print').report_action(self)
           
                
    @api.multi
    def do_print_statement_filter(self):
        return self.env.ref('bi_customer_overdue_statement.report_customer_statement_filter_print').report_action(self)
    
    
    @api.multi
    def do_print_vendor_statement_filter(self):
        return self.env.ref('bi_customer_overdue_statement.report_supplier_filter_print').report_action(self)
    
