# -*- coding: utf-8 -*-

from openerp import models, fields, api,exceptions

class info_check(models.Model):
    _name = 'info_check.check'

    @api.model
    def _get_default_check_name( self ):
        return self.env['ir.sequence'].sudo().next_by_code('info_check.check_seq')

    @api.model
    def _get_default_amount(self):
        return_amount = 0.0
        total_payment_amount = self._context.get('payment_amount') or 0.0
        if int(total_payment_amount) == 0:
            return 0
        if not self._context.get('checks'):
            return total_payment_amount
        else:
            top_amount = 0.0
            for c in self._context.get('checks'):
                if not c[2]:
                    top_amount += float(self.browse(c[1]).amount)
                else:
                    top_amount += float(c[2]['amount'])
            return float (total_payment_amount - top_amount)


    name  = fields.Char(string='Check Name',default=_get_default_check_name,required=True,readonly=True)
    state = fields.Selection(string='Check State', selection=[('draft','Yeni'),('port','Portföyde'),
         ('ciro','Ciro Edildi'),
         ('inthsl','Tahsilde'),
         ('tahsil','Tahsil Edildi'),
         ('teminat','Teminatta'),
         ('karsiliksiz','Karşılıksız'),
         ('iade','İade Edilen'),
         ('takip','Takipte'),
         ('edilemeyent','Tahsil Edilemeyen'),
         ('protesto','Protesto Edilen'),
         ('kismitahsil','Kısmi Tahsil'),
         ('yazdir','Yazdırıldı'),
         ('odendi','Ödendi'),
         ('kismiodeme','Kısmi Ödeme')],default='draft')
    due_date = fields.Date(string='Due Date',required=True)
    amount   = fields.Float(string='Amount',required=True,default=_get_default_amount)
    currency = fields.Many2one('res.currency',string='Currency' , required=True,default=lambda self:self.env['res.currency'].sudo().search([('name','=','TRY')]).id)
    check_no = fields.Char(string='Check Number',required=True)
    giro     = fields.Boolean(string='Giro?')
    debtor_name = fields.Char(string='Deptors Name', required=True)

    incoming_payment_id = fields.Many2one('account.payment',string='Incoming Voucher')
    outgoing_payment_id = fields.Many2one('account.payment',string='Outgoing Voucher')
    temp_payment_id     = fields.Many2one('info_bayi.temp_payment',string='Temp Voucher')

    payment_city        = fields.Char(string='Payment City',required=True)
    #bank_name           = fields.Many2one('bkm.acquirer', string='Bank Name')
    bank_name           = fields.Char(string='Bank Name',required=True)
    bank_branch         = fields.Char(string='Bank Branch',required=True)
    bank_account_no     = fields.Char(string='Bank Account No',required=True)
    check_image_fr      = fields.Binary(string='Check Image (Front)',required=True)
    check_image_rr      = fields.Binary(string='Check Image (Back)',required=True)

    #t_bank_name            = fields.Many2one('bkm.acquirer', string='Bank Name')
    t_bank_name            = fields.Char(string='Bank Name')
    c_partner              = fields.Many2one('res.partner','Giro Partner')
    t_journal_id           = fields.Many2one('account.journal','Encash-Deposit Journal')
    company_id             = fields.Many2one('res.company',default=lambda self:self.env.user.company_id.id)
    allowed_companies      = fields.Many2many('res.company')

    checkveren             = fields.Many2one('res.company',related='outgoing_payment_id.company_id')
    header_visible         = fields.Many2one('res.company',compute=lambda self:self.company_id.id == self.env.user.company_id.id)

    @api.one
    def to_tahsil_edildi_action(self):
       self.state = 'tahsil'
    @api.one
    def to_tahsil_edildilemedi_action(self):
       self.state = 'edilemeyent'
    @api.one
    def to_karsiliksiz_action(self):
       self.state = 'karsiliksiz'

    @api.model
    def create( self, values ):
        values['allowed_companies'] = [(4,self.env.user.company_id.id)]
        res = super(info_check,self ).create( values)

        return res

class account_payment(models.Model):

    _inherit = 'account.payment'

    check_odemesi_mi = fields.Boolean(compute='check_odeesi_mi')

    @api.one
    @api.depends('journal_id')
    def check_odeesi_mi( self ):
        #print self.journal_id
        if self.journal_id.check:
            self.check_odemesi_mi = True
        else:
            self.check_odemesi_mi = False

    incoming_info_check = fields.One2many('info_check.check','incoming_payment_id',string='Incoming Check')
    outgoing_info_check = fields.One2many('info_check.check','outgoing_payment_id',string='Outgoing Check')

class temp_payment(models.Model):

    _inherit = 'info_bayi.temp_payment'
    tedarik_check    = fields.One2many('info_check.check','temp_payment_id')
    tedarik_check_v  = fields.Boolean(compute='check_var_mi')

    @api.multi
    @api.depends('tedarik_check')
    def check_var_mi( self ):
        if len( self.tedarik_check ) > 0:
            self.tedarik_check_v = True
