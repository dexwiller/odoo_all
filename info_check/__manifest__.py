# -*- coding: utf-8 -*-
{
    'name': "info_check",

    'summary': """
        Check Payment Type extension  for Account Voucher Module""",

    'description': """
        Check Payment Type extension  for Account Voucher Module by Infoteks Technology.
        Developed by Deniz Yıldız.
    """,

    'author': "Infoteks Technology",
    'website': "http://www.infoteks.com.tr",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','account','web','info_extensions','info_bayi'],
    'qweb': [
        'static/src/xml/widget.xml',
    ],
    # always loaded
    'data': [
        'security/groups.xml',
        'security/ir.model.access.csv',
        'assets.xml',
        'account_check_sequence.xml',
        'account_payment.xml',
        'account_check.xml',
        'wizards.xml'
    ],
}
