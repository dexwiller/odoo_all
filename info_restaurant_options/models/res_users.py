# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions
from .utils import  get_new_env, redirect_to_purchase_page

class res_users(models.Model):
    _inherit = 'res.users'

    @api.model
    def default_get(self, f_list):
        config = self.env["info_restaurant_options.config"].sudo().search([])
        total_person = person_size(self, 'yonetici') + person_size(self, 'garson') + person_size(self, 'mutfak')

        total_packet_person = int(config.yonetici_max_size) + int(config.garson_max_size) + int(
            config.mutfak_max_size)
        '''
        if total_packet_person:
            if total_packet_person <= total_person:
                raise exceptions.ValidationError(
                    "Kullanıcı ekleme hakkınız dolmuştur. Lütfen paketinizi yükseltin.")
        else:
            raise exceptions.ValidationError(
                "Sistem ayarlarınız yapılandırılmamış. Lütfen yetkili ile görüşün. Kod:001")
        '''
        res = super(res_users, self).default_get(f_list)
        return res

    user_role = fields.Selection(string='Kullanıcı Tipi', selection=[('yonetici', 'Yönetici'),
                                                                     ('mutfak', 'Mutfak'),
                                                                     ('garson', 'Garson')], default='garson')
    default_pass = fields.Char(string='Oto Pass')

    def manager_count_default(self):
        config = self.env["info_restaurant_options.config"].sudo().search([])
        return config.yonetici_max_size

    def waiter_count_default(self):
        config = self.env["info_restaurant_options.config"].sudo().search([])
        return config.garson_max_size

    def kitchen_count_default(self):
        config = self.env["info_restaurant_options.config"].sudo().search([])
        return config.mutfak_max_size

    manager_status = fields.Integer(compute='status_count', default=manager_count_default)
    waiter_status = fields.Integer(compute='status_count', default=waiter_count_default)
    kitchen_status = fields.Integer(compute='status_count', default=kitchen_count_default)

    def add_manager_count(self):
        return len(self.env['res.users'].search([('user_role', '=', 'yonetici'), ('login', '!=', 'restoran')]))

    def add_waiter_count(self):
        return len(self.env['res.users'].search([('user_role', '=', 'garson'), ('login', '!=', 'restoran')]))

    def add_kitchen_count(self):
        return len(self.env['res.users'].search([('user_role', '=', 'mutfak'), ('login', '!=', 'restoran')]))

    add_manager_status = fields.Integer(compute='status_count', default=add_manager_count)
    add_waiter_status = fields.Integer(compute='status_count', default=add_waiter_count)
    add_kitchen_status = fields.Integer(compute='status_count', default=add_kitchen_count)

    def _get_purchase_url_default(self):
        return "<div class='row' style='text-align:center;'><h1><a href='{}'>Paketinizi Yükseltin !!!</a></h1></div>".format( redirect_to_purchase_page(self.env))

    purchase_url = fields.Html(compute="_get_purchase_url",default=_get_purchase_url_default)

    def _get_purchase_url(self):
        self.purchase_url = "<a href='{}'>Paketinizi Yükseltin !!!</a>".format( redirect_to_purchase_page(self.env))

    @api.multi
    def status_count(self):
        pass

    @api.multi
    def write(self, v):
        if v.get('user_role'):
            v = user_control(self, v)

            #V nin durumuna göre exception raise etmek gerekebilir, res buradaydı dışarı aldım. deniz
        prev_eposta = self.email
        res = super(res_users, self).write(v)

        # base'i bilgilendirelim deniz...
        new_env = get_new_env()
        cre     = new_env.cr

        sq_kontrol = "select count(id) from info_restaurant_base_default_cre where login = '%s' and db_name='%s';"\
                     %(prev_eposta, self.env.cr.dbname)
        cre.execute( sq_kontrol )
        if int(cre.fetchone()[0]) > 0:
            up = ''
            if v.get( 'email'):
                up += "login='%s',"%v.get( 'email')
            if v.get('mobile'):
                up += "phone='%s',"%v.get('mobile')
            if v.get('password'):
                up += "raw_pass='%s'," %v.get('password')
            if up:
                up = up[:len(up)-1]
                sq_user_info = "update info_restaurant_base_default_cre set %s where login = '%s' and db_name='%s'; "%(up,prev_eposta,
                                                                                                                       self.env.cr.dbname)
                cre.execute ( sq_user_info )
                cre.commit()
                cre.close()
        else:
            sq_user_info = "insert into info_restaurant_base_default_cre " \
                           "(create_uid, create_date, write_uid, write_date,login,phone,basvuru_id,db_name,user_role) select 2,now() \
                    at time zone 'UTC',2,now() at time zone 'UTC', '%s','%s', id, '%s', '%s' from info_restaurant_base_demo where db_name = '%s';" % (
            self.email,self.mobile,self.env.cr.dbname,self.user_role, self.env.cr.dbname)
            cre.execute(sq_user_info)
            cre.commit()
            cre.close()

        return res

    @api.model
    def create(self, v):
        v = user_control(self, v)

        res = super(res_users, self).create(v)
        #base'i bilgilendirelim deniz...
        new_env      = get_new_env()
        cre          = new_env.cr
        sq_user_info = "insert into info_restaurant_base_default_cre (create_uid, create_date, write_uid, write_date,login,phone,db_name,user_role,basvuru_id) select 2,now() \
                at time zone 'UTC',2,now() at time zone 'UTC', '%s','%s','%s','%s', id from info_restaurant_base_demo where db_name = '%s'"\
                       %( res.email,res.mobile,self.env.cr.dbname,res.user_role, self.env.cr.dbname )

        cre.execute( sq_user_info )
        cre.commit()
        cre.close()
        return res


def user_control(self, v):
    if v.get('user_role'):
        if change_person_control(self, v.get('user_role')):
            g_name = 'info_restaurant_options.' + v.get('user_role')
            v['groups_id'] = [(4, self.env.ref(g_name).id), ]
        else:
            raise exceptions.ValidationError(
                "'" + v.get('user_role').upper() +
                "' kullanıcısı ekleme hakkınız kalmamıştır. Lütfen paketinizi yükseltin.")
    return v


def change_person_control(self, user_role):
    config = self.env["info_restaurant_options.config"].sudo().search([])
    if person_size(self, user_role) < int(getattr(config, user_role + "_max_size")):
        return True
    else:
        return False


def person_size(self, name):
    return len(self.env['res.users'].search([('user_role', '=', name), ('login', '!=', 'restoran')]))
