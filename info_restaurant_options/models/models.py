# -*- coding: utf-8 -*-

from odoo import models, fields, api
from .utils import get_new_env,redirect_to_purchase_page
from datetime import datetime as dt, timedelta as td

class account_journal(models.Model):
    _inherit = 'account.journal'

    tsm_code = fields.Integer(default=-1)


class uom_uom(models.Model):
    _inherit = 'uom.uom'

    tsm_code = fields.Integer()

class account_invoice(models.Model):
    _inherit = 'account.invoice'

    belge_no = fields.Char('Fatura Belge No')

class mobil_odeme(models.Model):
    _name = 'info_restaurant_options.mobil_odeme'

    order_id = fields.Char()

class info_restaurant_options_config(models.Model):
    _name = 'info_restaurant_options.config'

    db_name     = fields.Char(string = 'Veritabanı Adı')
    state       = fields.Selection(string='Durum',selection=[('demo','Demo Sürüm'),
                                                             ('expired','Süresi Dolan Sürüm'),
                                                             ('product','Ücretli Sürüm')], required=True,default='demo')
    users           = fields.Many2many('res.users',string='Kullanıcılar',required=True)
    demo_duration   = fields.Integer(string='Demo Geçerlilik Süresi (Gün)' ,required = True)
    demo_after_time = fields.Integer(string="Demo Bitiminden Sonra Verilerin Saklanacağı Süre (Gün )")
    email = fields.Char(string='Mail Adresi')
    phone = fields.Char(string='Telefon Numarası')

    yonetici_max_size = fields.Char(string='En Fazla Yönetici Sayısı')
    garson_max_size = fields.Char(string='En Fazla Garson Personel Sayısı')
    mutfak_max_size = fields.Char(string='En Fazla Mutfak Personel Sayısı')

    paket_adi       = fields.Char('paket_adi')
    base_partner_id = fields.Integer()

    def _get_purchase_url_default(self):
        return redirect_to_purchase_page(self.env)

    purchase_url = fields.Char(compute="_get_purchase_url",default=_get_purchase_url_default)

    def _get_purchase_url(self):
        self.purchase_url = redirect_to_purchase_page(self.env)

    def get_to_drop_db(self):
        config = self.sudo().search([])
        if config:
            config = config[-1]
        demo_after_time =  config.demo_after_time
        if not demo_after_time:
            demo_after_time = 0

        if config.state == 'expired' and dt.now() > config.create_date + td(days=config.demo_duration) + td( days = demo_after_time):
            new_env = get_new_env()
            new_env.cr.execute("insert into info_restaurant_base_db_to_drop (create_date,write_date,create_uid,write_uid,name,is_dropped) \
                                values (now() at time zone 'UTC', now() at time zone 'UTC',2,2,{},false)").format(self.env.cr.dbname)

            new_env.cr.commit()
            new_env.cr.close()




