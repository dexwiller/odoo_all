master_db_name = 'yemek12'
protocol       = 'http'
purchase_url_suffix = 'yemek-karti/urunler/restoran'
from odoo import modules, SUPERUSER_ID, api, exceptions
def get_new_env():
    reg = modules.registry.Registry.new(master_db_name)
    new_cr = reg.cursor()
    # if it's a copy of a database, force generation of a new dbuuid
    new_env = api.Environment(new_cr, SUPERUSER_ID, {})
    #new_env['ir.config_parameter'].init(force=True)

    return new_env

def redirect_to_purchase_page(env):
    #databasename gidecek,
    config = env.get('ir.config_parameter').sudo()
    href_g = config.get_param("web.base.url", default=None)
    href_g = 'http://' + env.cr.dbname + '.dev'
    db_config  = env.get ('info_restaurant_options.config').search([])
    if not db_config:
        raise exceptions.ValidationError("Sistem Konfigürasyonu Bulunamadı.")

    db_config = db_config[-1]
    url_params = '?'
    if db_config.db_name:
        url_params += 'database={}'.format(db_config.db_name)
        url_params+='&'
    if db_config.base_partner_id:
        url_params += 'partner={}'.format(db_config.base_partner_id)
        url_params += '&'
    if db_config.paket_adi:
        url_params += 'paket={}'.format(db_config.paket_adi)
        url_params += '&'

    url_params = url_params[:len(url_params)-1]

    red_url = href_g.replace(env.cr.dbname,master_db_name).replace('http',protocol) + '/' + purchase_url_suffix + url_params
    return red_url

