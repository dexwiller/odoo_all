# -*- coding: utf-8 -*-
from odoo import http, _
import odoo,json
from odoo.http import request
from odoo.addons.web.controllers.main import Home, ensure_db
from datetime import datetime as dt, timedelta as td
import werkzeug

class PosController(http.Controller):
    @http.route('/pos/web', type='http', auth='user')
    def pos_web(self, debug=False, **k):
        # if user not logged in, log him in
        pos_sessions = request.env['pos.session'].search([
            ('state', '=', 'opened'),
            ('user_id', '=', request.session.uid),
            ('rescue', '=', False)])
        if not pos_sessions:
            #bu kullanıcının son kapanan session configi bul...
            config = request.env['pos.config'].search([('user_id','=',request.session.uid)])
            if config:
                config = config[-1]
            else:
                prev_session = request.env['pos.session'].search([
                                ('state', 'in', ('closed','closing_control')),
                                ('user_id', '=', request.session.uid),
                                ('rescue', '=', False)],order='id desc',limit=1)

                if not  prev_session:
                    return werkzeug.utils.redirect('/web_pos_close#action=point_of_sale.action_client_pos_menu&no_open=true')

                config          = prev_session.config_id
            pos_sesion_dict = config.open_session_cb()
            if pos_sesion_dict.get ('type') == 'ir.actions.act_url':
                pos_sessions = request.env['pos.session'].search([
                    ('state', '=', 'opened'),
                    ('user_id', '=', request.session.uid),
                    ('rescue', '=', False)])
            else:
                pos_sessions     = request.env['pos.session'].browse( pos_sesion_dict.get('res_id'))
        pos_sessions.login()
        context = {
            'session_info': json.dumps(request.env['ir.http'].session_info())
        }
        return request.render('point_of_sale.index', qcontext=context)


class InfoRestaurantOptions(Home):
     @http.route('/web', type='http', auth="none")
     def web_client(self, s_action=None, **kw):

        res =  super(InfoRestaurantOptions, self).web_client(s_action)
        ensure_db()
        if not http.request.session.uid:
            return werkzeug.utils.redirect('/web/login', 303)
        user_id = http.request.session.uid
        user = http.request.env.get('res.users').browse([user_id])
        config = request.env.get('info_restaurant_options.config').sudo().search([])
        if config:
            config = config[-1]
        if config.state == 'expired':
            return werkzeug.utils.redirect('/web/login', 303)
        if user and  hasattr(user,'user_role') and user.id > 5:#5ten öncekiler defaultlar

            if config.state == 'product' and user.default_pass and http.request.env.cr.dbname != 'restoran':
                return werkzeug.utils.redirect('/web/reset_password?')

            if user.user_role in ('garson','mutfak'):
                return  werkzeug.utils.redirect('/pos/web', 303)
        return res

     @http.route('/web_pos_close', type='http', auth="none")
     def web_client_close(self, s_action=None, **kw):

         res = super(InfoRestaurantOptions, self).web_client(s_action)
         ensure_db()
         if not http.request.session.uid:
             return werkzeug.utils.redirect('/web/login', 303)
         user_id = http.request.session.uid
         user = http.request.env.get('res.users').browse([user_id])
         if user.user_role in ('garson', 'mutfak'):
             #sessionı kapat
             ssesions = http.request.env.get('pos.session').sudo().search([('state','=','opened'),('user_id','=',user_id)])
             ssesions.action_pos_session_closing_control()
             return werkzeug.utils.redirect('/web/login', 303)

         return res


     @http.route('/web/login', type='http', auth="none", sitemap=False)
     def web_login(self, redirect=None, **kw):
         ensure_db()
         request.params['login_success'] = False
         if request.httprequest.method == 'GET' and redirect and request.session.uid:
             return http.redirect_with_hash(redirect)

         if not request.uid:
             request.uid = odoo.SUPERUSER_ID

         values = request.params.copy()
         try:
             values['databases'] = http.db_list()
         except odoo.exceptions.AccessDenied:
             values['databases'] = None

         if request.httprequest.method == 'POST':
             old_uid = request.uid
             try:
                 uid = request.session.authenticate(request.session.db, request.params['login'],
                                                    request.params['password'])
                 request.params['login_success'] = True
                 return http.redirect_with_hash(self._login_redirect(uid, redirect=redirect))
             except odoo.exceptions.AccessDenied as e:
                 request.uid = old_uid
                 if e.args == odoo.exceptions.AccessDenied().args:
                     values['error'] = _("Wrong login/password")
                 else:
                     values['error'] = e.args[0]
         else:
             if 'error' in request.params and request.params.get('error') == 'access':
                 values['error'] = _('Only employee can access this database. Please contact the administrator.')

         if 'login' not in values and request.session.get('auth_login'):
             values['login'] = request.session.get('auth_login')

         if not odoo.tools.config['list_db']:
             values['disable_database_manager'] = True

         # otherwise no real way to test debug mode in template as ?debug =>
         # values['debug'] = '' but that's also the fallback value when
         # missing variables in qweb
         if 'debug' in values:
             values['debug'] = True

         config = request.env.get('info_restaurant_options.config').sudo().search([])
         if config:
             config = config[-1]
         demo_after_time = config.demo_after_time
         if not demo_after_time:
             demo_after_time = 0

         if config.state == 'expired':
             if dt.now() > config.create_date + td(days=config.demo_duration) + td(
                 days=demo_after_time):
                values['error'] = 'Deneme Süreniz Dolmuş ve Verileriniz 1 Günden Kısa Süre İçinde Silinecek. \
                                '.format(config.purchase_url)
             else:
                kalan_zaman = config.create_date + td(days=config.demo_duration) + td(
                 days=demo_after_time) - dt.now()
                kalan_zaman = '{} Gün {} Saat '.format( kalan_zaman.days , kalan_zaman.seconds//3600)
                values['error'] = 'Deneme Süreniz Dolmuş ve Verileriniz {} İçinde Silinecek.\
                                                '.format(kalan_zaman,config.purchase_url)
             values['purchase_link'] = config.purchase_url

         response = request.render('web.login', values)
         response.headers['X-Frame-Options'] = 'DENY'
         return response




