odoo.define('info_restaurant_options.graph_buttons', function (require) {
"use strict";

    var core = require('web.core');
    var GraphController = require('web.GraphController');
    var ListController = require('web.ListController');
    var qweb = core.qweb;
    var _t = core._t;

    ListController.include({
        renderButtons: function($node) {
            this._super.apply(this, arguments);
                if (this.$buttons) {
                    let filter_button = this.$buttons.find('.oe_odendi_button');
                    filter_button && filter_button.click(this.proxy('odendi_button')) ;

                    let filter_button2 = this.$buttons.find('.oe_islendi_button');
                    filter_button2 && filter_button2.click(this.proxy('islendi_button')) ;
                }
            },
            odendi_button: function () {
                return this.do_action({
                    name: _t('Pos Orders'),
                    type: 'ir.actions.act_window',
                    res_model: 'pos.order',
                    views: [[false, 'list'], [false, 'kanban'], [false, 'form']],
                    view_mode: 'tree,kanban,form',
                    target: 'current',
                    context:{'search_default_paid':1}
                },{clear_breadcrumbs: true});
            },
            islendi_button: function () {
                return this.do_action({
                    name: _t('Pos Orders'),
                    type: 'ir.actions.act_window',
                    res_model: 'pos.order',
                    views: [[false, 'list'], [false, 'kanban'], [false, 'form']],
                    view_mode: 'tree,kanban,form',
                    target: 'current',
                    context:{'search_default_posted':1}
                },{clear_breadcrumbs: true});
            }

    });
    GraphController.include({

        renderButtons: function($node) {
        //this._super.apply(this, arguments);
            if ($node) {
                var context = {
                    measures: _.sortBy(_.pairs(_.omit(this.measures, '__count__')), function (x) { return x[1].string.toLowerCase(); }),
                    model:this.modelName
                };
                this.$buttons = $(qweb.render('GraphView.buttons', context));
                this.$measureList = this.$buttons.find('.o_graph_measures_list');
                this.$buttons.find('button').tooltip();
                this.$buttons.click(this._onButtonClick.bind(this));
                this._updateButtons();
                this.$buttons.appendTo($node);
                if (this.isEmbedded) {
                    this._addGroupByMenu($node, this.groupableFields);
                }
            }
            if (this.$buttons) {
                let self = this;
                let gunluk_button = this.$buttons.find('.oe_gun_button');
                gunluk_button && gunluk_button.click(this.proxy('gun_button')) ;

                let haftalik_button = this.$buttons.find('.oe_hafta_button');
                haftalik_button && haftalik_button.click(this.proxy('hafta_button')) ;

                let aylik_button = this.$buttons.find('.oe_ay_button');
                aylik_button && aylik_button.click(this.proxy('ay_button')) ;
            }
        },
        gun_button: function () {
            return this.do_action({
                name: _t('Orders Analysis'),
                type: 'ir.actions.act_window',
                res_model: 'report.pos.order',
                views: [[false, 'graph'], [false, 'pivot']],
                view_mode: 'graph,pivot',
                target: 'current',
                context:{'group_by_no_leaf':1,'group_by':'date:day'}
            },{clear_breadcrumbs: true});
        },
        hafta_button: function () {
            return this.do_action({
                name: _t('Orders Analysis'),
                type: 'ir.actions.act_window',
                res_model: 'report.pos.order',
                views: [[false, 'graph'], [false, 'pivot']],
                view_mode: 'graph,pivot',
                target: 'current',
                context:{'group_by_no_leaf':1,'group_by':'date:week'}
            },{clear_breadcrumbs: true});

        },
        ay_button: function () {
            return this.do_action({
                name: _t('Orders Analysis'),
                type: 'ir.actions.act_window',
                res_model: 'report.pos.order',
                views: [[false, 'graph'], [false, 'pivot']],
                view_mode: 'graph,pivot',
                target: 'current',
                context:{'group_by_no_leaf':1,'group_by':'date:month'}
            },{clear_breadcrumbs: true});

        }

    });

})