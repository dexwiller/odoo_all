from Crypto.Cipher import AES
import base64
import os
import xlsxwriter, re
from io import BytesIO
BLOCK_SIZE = 16
PADDING = '{'
pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING
EncodeAES = lambda c, s: base64.b64encode(c.encrypt(pad(s)))
DecodeAES = lambda c, e: str(c.decrypt(base64.b64decode(e)), 'utf-8').rstrip(PADDING)
#secret = os.urandom(BLOCK_SIZE)
a = 'PTHK%GT&+Y386^0*'
cipher = AES.new(a)
from copy import copy
from datetime import datetime,date
#encoded = EncodeAES(cipher, 'password')
#print 'Encrypted string:', encoded

# decode the encoded string
#decoded = DecodeAES(cipher, encoded)
#print 'Decrypted string:', decoded

import pytz

def copy_format(book, fmt):
    properties = [f[4:] for f in dir(fmt) if f[0:4] == 'set_']
    dft_fmt = book.add_format()
    return book.add_format({k : v for k, v in fmt.__dict__.items() if k in properties and dft_fmt.__dict__[k] != v})

def get_naive_from_aware( dt ):
    dt = dt.replace(tzinfo=None)
    local = pytz.timezone ("Europe/Istanbul")
    tzoffset = local.utcoffset(dt)
    dt       = dt + tzoffset
    
    return dt
def from_data_custom(fields, rows, float_colored = False, damali = False):
    fp         = BytesIO()
    workbook   = xlsxwriter.Workbook(fp)
    worksheet  = workbook.add_worksheet()
    bold       = workbook.add_format({'bold': True,'text_wrap': True})
    base_style = workbook.add_format({'text_wrap': True})
    format_2 = workbook.add_format({'bold': True,'text_wrap': True})
    format_2.set_align('center')
    format_2.set_align('vcenter')
    format_2.set_bg_color("#EB560A")
    format_2.set_font_color("white")
    
    datetime_style = workbook.add_format({'text_wrap': True, 'num_format':'DD-MM-YYYY HH:mm:SS'})
    date_style     = workbook.add_format({'text_wrap': True, 'num_format':'DD-MM-YYYY'})
    date_style.set_align('vcenter')
    datetime_style.set_align('vcenter')
    base_style.set_align('vcenter')
    
    int_stye = workbook.add_format({'text_wrap': False})
    int_stye.set_align('vcenter')
    if float_colored:
        int_stye.set_bg_color(float_colored[0])
        int_stye.set_font_color(float_colored[1])
    for i, fieldname in enumerate(fields):
        worksheet.write(0, i, fieldname,format_2)
        #worksheet.set_column(0,i,50)
    c = '#A4DE63'
    for row_index, row in enumerate(rows):
        height_setted = False
        if damali:
            if len(str(row[0]).strip()) != 0:#satirda kayit var style deistir.
                if c != '#FA9A50':
                    c = '#FA9A50'
                else:
                    c = '#A4DE63'
                            
        for cell_index, cell_value in enumerate(row):
            cell_style = base_style
            if not height_setted:
                worksheet.set_row(row_index + 1,64)
            worksheet.set_column(row_index + 1,0,30)
            if isinstance(cell_value, str):
                cell_value = re.sub("\r", " ", cell_value)
            elif isinstance(cell_value, datetime):
                cell_style = datetime_style
            elif isinstance(cell_value, date):
                cell_style = date_style
            elif isinstance(cell_value,float):
                cell_style = int_stye
            
            if damali:
                if c == '#A4DE63':
                    new_c  = copy_format(workbook, cell_style)
                    new_c.set_bg_color(c)
                    worksheet.write(row_index + 1, cell_index, cell_value, new_c)
                else:
                    cell_style.set_bg_color(c)
                    worksheet.write(row_index + 1, cell_index, cell_value, cell_style)
            else:
                worksheet.write(row_index + 1, cell_index, cell_value, cell_style)
    workbook.close()
    fp.seek(0)
    data = fp.read()
    fp.close()
    return data

def get_popup_w(obj,name, msj ):
    view    = obj.env.ref('sh_message.sh_message_wizard')
    view_id = view and view.id or False
    context = dict(obj._context or {})
    context['message'] = msj
    return dict(
        name=name,
        type='ir.actions.act_window',
        view_type = 'form',
        view_mode = 'form',
        res_model = 'sh.message.wizard',
        views     = [(view_id,'form')],
        view_id   = view_id,
        target    = 'new',
        context   = context
    )
    
