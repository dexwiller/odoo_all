# -*- coding: utf-8 -*-

from . import res_partner
from . import stock_production_lot
from . import product
from . import kargo
from . import check
from . import wizards
from . import account
from . import mobil_odeme