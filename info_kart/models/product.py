# -*- coding: utf-8 -*-

from odoo import models, fields, api,exceptions

class product(models.Model):
    _inherit = 'product.template'
    
    kart     = fields.Boolean("Ürün Kart mı?")
    
    @api.onchange("kart")
    def kart_onchange(self):
        if self.kart:
            self.tracking = 'serial'