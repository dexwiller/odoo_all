# -*- coding: utf-8 -*-
from odoo import models, fields, api,exceptions
from odoo.addons.info_bayi.models import webserviceopt
import logging
_logger = logging.getLogger(__name__)
class kargo_firmalari(models.Model):
    _name='info_kart.kargo_firmalari'
    
    name = fields.Char('Kargo Firması Adı', required = True)
    
class kargo_istisna_rel(models.Model):
    _name='info_kart.kargo_istisna_rel'
    sira  = fields.Integer('Sıra No')
    kargo = fields.Many2one('info_kart.kargo',string='Kargo')
    kart  = fields.Many2one('stock.production.lot',string='Kart No')
    
class kargo(models.Model):
    _name   = 'info_kart.kargo'
    _order  = 'id desc'
    
    seri_no    = fields.Char('Kargo Seri No',
        default=lambda self: self.env['ir.sequence'].sudo().next_by_code('info_kart.kargo_sq'),
        copy=False)
    
    partner_id = fields.Many2one('res.partner', domain=['|',('dealer','=',True),('kart_musterisi','=',True),
                                                        ('is_company','=',True),
                                                        ('customer','=',True),
                                                        ('state','=','confirmed')],string='Müşteri')
    
    street      = fields.Char(related='partner_id.street')
    street2     = fields.Char(related='partner_id.street2')
    
    ilce        = fields.Many2one(related='partner_id.ilce')
    city_combo  = fields.Many2one(related='partner_id.city_combo')
    
    street3      = fields.Char()
    street4      = fields.Char()
    tel          = fields.Char(related='partner_id.phone')
    
    ilce2        = fields.Many2one('info_extensions.ilceler')
    city_combo2  = fields.Many2one('info_extensions.iller')
    
    
    partner_shipping_id = fields.Many2one('res.partner',
                                          string='Farklı Teslimat Adresi', readonly=True,
                                          states={'draft': [('readonly', False)],
                                                  'sent': [('readonly', False)]},
                                          help="Teslimat Adresi Girmek İçin Tıklayınız.")     
    
    teslim_alan     = fields.Char(string='Teslim Alacak Kişi')
    teslim_alan_tel = fields.Char(string='Telefon')
    kartlar         = fields.Many2many('stock.production.lot',string='Kargolanacak Kartlar')
    talep_adeti     = fields.Integer(string='Talep Edilen Kart Adeti')
    kart_adetleri   = fields.Integer(compute='_get_card_count',string='Kargolanacak Kart Adeti')
    
    state           = fields.Selection(selection=[('talep','Talep'),
                                                  ('draft','Yeni'),
                                                  ('waiting_firm','Kargolama Bekliyor'),
                                                  ('sent','Gönderildi'),
                                                  ('marked','Teslim Alma Teyidi Bekleniyor'),
                                                  ('done','Teslim Edildi'),
                                                  ('cancel','Süreci İptal Edildi')
                                                  ],string='Durum',default='draft')
    
    kargo_firmasi  = fields.Many2one('info_kart.kargo_firmalari',string='Kargo Firması')
    takip_numarasi = fields.Char(string='Kargo Takip No')
    
    teslim_alan_n  = fields.Char(string='Teslim Alan Kişi')
    teslim_tarihi  = fields.Date(string='Teslim Tarihi')
    teslim_adeti   = fields.Integer(string='Teslim Adeti')
    
    stock            = fields.Selection(selection=(('true',"Yedek Kart Stoğuna Gönder"),('false','Kullanım İçin Gönder')),string="Kargo Tipi")
    
    teslim_exception = fields.One2many('info_kart.kargo_istisna_rel','kargo',string='Kargo İstisnası')
    
    is_editable      = fields.Boolean(compute='_is_editable',default=True)
    
    def _is_editable(self):
        if self.env.user.has_group('info_kart.group_kart_kargo_editor') or self.state == 'talep':
            self.is_editable = True
        else:
            self.is_editable = False
    
    @api.multi
    def confirm_and_proceed(self):
        
        return {'type': 'ir.actions.act_window_close'}
    
    @api.onchange('teslim_adeti')
    def teslim_istisnasi(self):
        if self.teslim_adeti and self.teslim_adeti != self.kart_adetleri:
            fark = self.kart_adetleri - self.teslim_adeti
            if fark < 0 : fark=fark*-1
            self.teslim_exception = [(5,)]
            exes = []
            for i in range(fark):
                exes.append ((0,0,{'kargo':self.id,'sira':i+1}))
            self.teslim_exception = exes

    @api.onchange('kartlar')
    def onchange_karts(self):
        res = {}
        self.ensure_one()
        for kart in self.kartlar:
            
            domain = [('kartlar','=',kart.id),
                      ('state','!=','done')]
            if self.id:
                domain.append(('id','!=',self.id))
        
            others = self.search( domain )
        
            if others:
              res['warning'] = {'title': 'Seri No Başka Kargoya Eklenmiş.', 'message': 'Seri No Başka Kargoya Eklenmiş, Bu Seri No\'yu seçerek devam etmeniz halinde, eklenmiş olduğu kargodan çıkarılarak bu kargoda teslim edilecektir.'}
        
            for o in others:
                o.write({'kartlar':[(3,kart.id)]})
                o.kartlar = [(3,kart.id)]
        return res
        
    @api.onchange('teslim_alan_tel')
    def onchange_mobile(self):
        
        res = {}
        if self.teslim_alan_tel and len( self.teslim_alan_tel) > 0:
            phone = [str(s) for s in self.teslim_alan_tel.split() if s.isdigit()]
            if len( phone )> 0:
                phone = "".join( phone )
                if phone[0] == "0":
                    phone = phone[1:]
                if len( phone) != 10:
                    self.teslim_alan_tel = False
                    res = {'warning': {
                    'title': 'Telefon Numarası',
                    'message': u'Lütfen Geçerli Bir Telefon No Giriniz. '}
                    }
                else:
                    phone = phone[0:3] + '-' + phone[3:6] + ' ' + phone[6:8] + ' ' + phone[8:10]
                    self.teslim_alan_tel = phone
            else:
                self.teslim_alan_tel = False
                res = {'warning': {
                    'title': 'Telefon Numarası',
                    'message': u'Lütfen Geçerli Bir Telefon No Giriniz. '}
                    }
        return res
    
    @api.model
    def create(self,values):
        if values.get('state') == 'draft':
            values['state'] = 'waiting_firm'
        
        res = super( kargo,self).create( values )
        res.partner_id.mail_kargo_gonderildi_mi = 2
        if res.partner_id.parent_id:
            res.partner_id.parent_id.mail_kargo_gonderildi_mi =2
            
        email_template = self.env.ref('info_kart.kargo_email_template')
        mail_id   = email_template.send_mail(res.id, raise_exception="Mail gönderilemedi Parametreleri ve Mail adresini Kontrol Ediniz.", force_send=True)
        return res
    
    @api.multi
    def on_send(self):
        for s in self:
            s.state = 'sent'
    @api.multi
    def on_to_draft(self):
        for s in self:
            s.state = 'waiting_firm'
    
    @api.multi
    def on_takip_no_gir(self):
        for s in self:
            s.state = 'marked'
    
    @api.multi
    def on_yedege_gonder(self):
        self.ensure_one()
        self.stock = 'true'
        for k in self.kartlar:
            if k.lot_state in ('draft','draft2','sold'):
                k.lot_state = 'draft2'
    
    @api.one
    def on_done(self):
        self = self.sudo()
        except_list = list(map(lambda x:x.kart.id, self.teslim_exception))
        _logger.info( except_list )
        dom = [('partner_id','=',self.partner_id.id)]
        if self.partner_id.parent_id:
            dom = [('partner_id','=',self.partner_id.parent_id.id)]
        
        comp = self.env['res.company'].sudo().search(dom)
        if not comp:
            raise exceptions.ValidationError("Kargo Teslimi Yapılacak firma Onaylanmamış, Lütfen Önce Firmayı Onaylayınız.")
        
        for lot in self.kartlar:
            update_dict = {}
            _logger.info( lot.name )
            if lot.id not in except_list:
                _logger.info( 1 )
                state = 'sold'
                if self.stock == 'true':
                   state = 'draft2'
                _logger.info( 2 )
                partner = self.partner_id
                if partner.parent_id:
                    update_dict ['sube_id'] = partner.id
                    partner = partner.parent_id
                _logger.info( 3 )
                update_dict ['sold_to']    = partner.id
                
                if lot.lot_state not in ('sold2','sold3','scrapped'):
                    
                    update_dict['lot_state']  = state
                    #update_dict['ad_soyad' ]  = False
                    #update_dict['tckn'     ]  = False
                    #update_dict['cep_tel'  ]  = False
                _logger.info( 4 )
                update_dict['company_id'] = comp.id
                lot.write( update_dict )
            '''
            alt_firma_id = webserviceopt.kart_isle(s.partner_id,s.kartlar)
            if alt_firma_id:    
                s.partner_id.alt_firma_id = alt_firma_id
            else:
                raise exceptions.ValidationError("Merkez Stok bilgisi Gönderilemedi, Daha Sonra Tekrar Deneyiniz.")
            '''
            _logger.info( 5 )
        self.state = 'done'
        partner.mail_kargo_gonderildi_mi      = 3
        self.partner_id.mail_kargo_gonderildi_mi = 3
    @api.multi
    def on_reset(self):
        for s in self:
            s.kargo_firmasi   = False
            s.takip_numarasi  = False 
            s.state = 'waiting_firm'
    
    @api.multi
    def on_cancel(self):
        for s in self:
            s.state = 'cancel'        

    @api.multi
    @api.depends('kartlar')
    def _get_card_count(self):
        for s in self:
            s.kart_adetleri = len(s.kartlar)
            
    
    @api.multi
    def unlink(self):
        for s in self:
            if s.state == 'done':
                raise exceptions.ValidationError('Sadece Gönderilmemiş (Kargolama Bekliyor veya Yeni) Kargo Takibini Silebilirsiniz.')
            
        return super(kargo,self).unlink()
    
    @api.multi
    def get_kart_list_excel(self):
        self.ensure_one()
        f_list = [["seri_no_computed","Kart No"]]
        r = "/excel_c/info_kart.kargo/%s/kartlar/%s"%(self.id,f_list)
        
        return {
                'type': 'ir.actions.act_url',
                'url': r,
                'target': 'new'
                }
            