# -*- coding: utf-8 -*-
from odoo import models, fields, api,exceptions,registry
import base64
import xlrd
from copy import copy 
import datetime,calendar
from .util import cipher, EncodeAES,get_popup_w,from_data_custom,get_naive_from_aware
from odoo.addons.info_bayi.models.webserviceopt import bakiye,TP_Ozel_Oran_SK_Liste,BIN_SanalPos,TP_Islem_Odeme,tl_transfer,get_extre,tl_iptal_iade,Cuzdan,TL_Bloke_Islem,RaporByDekont,Kayip_Calinti,Rapor

class dummy:
    pass

class cuzdan(models.Model):
    inherit = 'info_kart.cuzdan'

class getYuklemeExtre(models.TransientModel):
    _name = 'info_kart.yukleme_harcama_ekstre'
    
    @api.model
    def default_get(self,f_list):
        def_ = super(getYuklemeExtre,self).default_get(f_list)
        default_yuklemeler = self.env['info_kart.set_limit'].search([('partner','=',self._context.get('active_id')),
                                                                     ('create_date','>=',def_.get('str_date')),
                                                                     ('create_date','<',def_.get('stp_date')),
                                                                     ('lot','=',False),
                                                                     ('state','=','confirm')
                                                                     ])
        def_['yuklemeler'] = list(map(lambda x:(4,x.id),default_yuklemeler))
        default_harcamalar = self.env['info_kart.set_limit'].search([('partner','=',self._context.get('active_id')),
                                                                     ('create_date','>=',def_.get('str_date')),
                                                                     ('create_date','<',def_.get('stp_date')),
                                                                     ('lot','!=',False),
                                                                     ('state','=','confirm')
                                                                     ])
        def_['harcamalar'] = list(map(lambda x:(4,x.id),default_harcamalar))
        default_iadeler    = self.env['info_kart.set_limit'].search([('partner','=',self._context.get('active_id')),
                                                                     ('create_date','>=',def_.get('str_date')),
                                                                     ('create_date','<',def_.get('stp_date')),
                                                                     ('lot','!=',False),
                                                                     ('state','=','iade')
                                                                     ])
        def_['iadeler'] = list(map(lambda x:(4,x.id),default_iadeler))
        return def_
    def _get_default_st_date(self):
         now = datetime.datetime.now()
         m   = str(now.month)
         if len(m) == 1:
            m = '0' + m
         return str(now.year) + '-' + m + '-' + '01'
    def _get_default_sp_date(self):
         now = datetime.datetime.now()
         m   = now.month
         d   = calendar.monthrange(now.year,m)[1]
         m   = str(m)
         if len(m) == 1:
            m = '0' + m
         return str(now.year) + '-' + m + '-' + str(d)
        
    str_date    = fields.Date(string='Başlangıç Tarihi',default=_get_default_st_date)
    stp_date    = fields.Date(string='Bitiş Tarihi',default=_get_default_sp_date)
    partner_id = fields.Many2one('res.partner',string='Kart Müşterisi')

    yuklemeler = fields.Many2many('info_kart.set_limit',string='Yüklemeler',domain=[('odeme_yontemi','!=',False),
                                                                                    ('partner','!=',False),
                                                                                    ('lot','=',False),
                                                                                    ('state','=','confirm')])
    harcamalar = fields.Many2many('info_kart.set_limit',string='Harcamalar',domain=[
                                                                                    ('partner','!=',False),
                                                                                    ('lot','!=',False),
                                                                                    ('state','=','confirm')])
    iadeler = fields.Many2many('info_kart.set_limit',string='İadeler',domain=[
                                                                                    ('partner','!=',False),
                                                                                    ('lot','!=',False),
                                                                                    ('state','=','iade')])
    
    @api.multi
    def get_kurumsal_extre(self):
        def_ = {}
        self.ensure_one()
        default_yuklemeler = self.env['info_kart.set_limit'].search([('partner','=',self.partner_id.id),
                                                                     ('create_date','>=',self.str_date),
                                                                     ('create_date','<',self.stp_date),
                                                                     ('lot','=',False),
                                                                     ('odeme_yontemi','!=',False),
                                                                     ('state','=','confirm')
                                                                     ])
        def_['yuklemeler'] = list(map(lambda x:(4,x.id),default_yuklemeler))
        
        default_harcamalar = self.env['info_kart.set_limit'].search([('partner','=',self.partner_id.id),
                                                                     ('create_date','>=',self.str_date),
                                                                     ('create_date','<',self.stp_date),
                                                                     ('lot','!=',False),
                                                                     ('state','=','confirm')
                                                                     ])
        
        def_['harcamalar'] = list(map(lambda x:(4,x.id),default_harcamalar))
        
        default_iadeler    = self.env['info_kart.set_limit'].search([('partner','=',self.partner_id.id),
                                                                     ('create_date','>=',self.str_date),
                                                                     ('create_date','<',self.stp_date),
                                                                     ('lot','!=',False),
                                                                     ('state','=','iade')
                                                                     ])
        
        def_['iadeler'] = list(map(lambda x:(4,x.id),default_iadeler))
        
        self.write( def_ )

        return {
            'type': 'ir.actions.act_window',
            'name': 'Kurumsal Ekstre',
            'res_id': self.id,
            'view_mode': 'form',
            'view_type':'form',
            'res_model': 'info_kart.yukleme_harcama_ekstre',
            'src_model':'res.partner',
            'context': {'default_partner_id':self.id},
            'target':"new"
       }

class getExtreParams(models.Model):
    _name='info_kart.get_extre_wizard'
    
    def _get_default_st_date(self):
         now = datetime.datetime.now()
         m   = str(now.month)
         if len(m) == 1:
            m = '0' + m
         return str(now.year) + '-' + m + '-' + '01'
    def _get_default_sp_date(self):
         now = datetime.datetime.now()
         m   = now.month
         d   = calendar.monthrange(now.year,m)[1]
         m   = str(m)
         if len(m) == 1:
            m = '0' + m
         return str(now.year) + '-' + m + '-' + str(d)
    def _get_default_extre( self ):
        extre_dict = get_extre(self,False)
        return extre_dict
    
    str_date    = fields.Date(string='Başlangıç Tarihi',default=_get_default_st_date)
    stp_date    = fields.Date(string='Bitiş Tarihi',default=_get_default_sp_date)
    lot_id      = fields.Many2one('stock.production.lot', string='Kart No',default=lambda self:self._context.get('active_id'))
    etxre_lines =  fields.One2many('info_kart.extre','get_extre_wizard',string="Extre Satırları",)
    total       = fields.Float(string="Ekstre Toplamı", compute="_get_extre_total")
    curry       = fields.Many2one('res.currency',default=lambda self: self.env['res.currency'].search([('name','=','TRY')], limit=1))
    
    @api.multi
    @api.depends('etxre_lines')
    def _get_extre_total(self):
        for s in self:
            t = 0.0
            for i in s.etxre_lines:
                if i.Tip == '+' and i.toplama_dahil:
                    t+= i.Tutar
                elif i.Tip == '-' and i.toplama_dahil:
                    t -= i.Tutar
            s.total = t
    
    @api.multi
    def get_card_extre(self):
        get_extre(self)
        return {
            'type': 'ir.actions.act_window',
            'name': 'Ekstre',
            'res_id': self.id,
            'view_mode': 'form',
            'view_type': 'form',
            'res_model': 'info_kart.get_extre_wizard',
            'src_model': 'stock.production.lot',
            'target': "new"
        }

    @api.multi
    def get_card_extre_pdf(self):
        extre_dict = get_extre(self)
        return {
            'type': 'ir.actions.act_url',
            'url': '/report/pdf/info_kart.report_kart_extre/' + str(self.id),
            'target': 'new'
        }

class extre(models.Model):
    _name = 'info_kart.extre'
    _order = 'Tarih desc'
    Terminal_Aciklama = fields.Char("Terminal Açıklama")
    Tip               = fields.Char("İşlem Tipi")
    _rowOrder         = fields.Integer()
    Tutar             = fields.Float("Tutar")
    ID                = fields.Char("İşlem Id")
    Aciklama          = fields.Char("Açıklama")
    Cuzdan            = fields.Char("Cüzdan No")
    _id               = fields.Char()
    Tarih             = fields.Datetime("İşlem Tarihi")
    get_extre_wizard  = fields.Many2one("info_kart.get_extre_wizard")
    curr              = fields.Many2one('res.currency',default=lambda self: self.env['res.currency'].search([('name','=','TRY')], limit=1))
    Durum             = fields.Char("Durum")
    PB                = fields.Char()
    toplama_dahil     = fields.Boolean(default = True)
    
    @api.multi
    def pos_islem(self):
        self.ensure_one()
        # tarih_s = self.Tarih.strip().split(" ")[0]
        tarih_s = self.Tarih.strftime("%Y-%m-%d")
        tarih_e = datetime.datetime.strftime (self.Tarih + datetime.timedelta(days = 1),'%Y-%m-%d')
        
        tip   = 1 
        lines = RaporByDekont(self,tarih_s,tarih_e,tip,self.ID)
        
        action={
            'name':'Pos Hareketleri',
            'type':'ir.actions.act_window',
            'res_model':'info_bayi.rapor',
            'src_model':'info_kart.extre',
            'view_type':'tree',
            'view_mode':'tree',
            'target':'new',
            'domain':[('id','in',lines)]
        }
        return action

class setLimitGeriCek(models.TransientModel):
    _name = 'info_kart.get_limit'
    
    def check_cuzdan(self):
        return False
        c_obj  =  self.env['info_kart.cuzdan']
        cuzdan =  c_obj.search([])
        if cuzdan:
            return cuzdan[0].id
        else:
            c_dict = Cuzdan( self )
            f      = 0
            for k,v in c_dict.items():
                new_cr = {'name':v,
                          'kod' :k}
                o = c_obj.create( new_cr )
                f = o.id
            return f
    
    def get_res(self,kart,kalan,res={}):
        if kart:
            res['kart'] = kart.id
            ava_bakiye = []
            if kart.odemeler:
                for o in kart.odemeler:
                    if o.cuzdan.id == res['cuzdan']:
                        if o.state == 'confirm' and o.final_bakiye > 0:
                            if o.final_bakiye >= kalan:
                                ava_bakiye.append((4,o.id))
                                break
                            else:
                                ava_bakiye.append((4,o.id))
                                kalan -= o.final_bakiye
                
            res['ava_bakiye']  = ava_bakiye
            res['total_tutar'] = kart.available_limit
            '''
            else:
                raise exceptions.ValidationError('Geri Alınabilecek Ödeme Bulunamadı.')
            '''
        else:
            raise exceptions.ValidationError('Kart No Alınamadı. Sayfayı Yenileyip Tekrar Deneyiniz.')
        return res
    
    @api.model
    def _get_default_kart_id(self):
        return self._context.get('active_id')
    
    @api.model
    def default_get(self,fields_list):
        res = super(setLimitGeriCek, self).default_get( fields_list )
        kart = self.env['stock.production.lot'].sudo().browse( self._get_default_kart_id())
        if res.get('cuzdan'):
            bakiye( kart )
            res = self.get_res (kart,kart.available_limit,res)
        return res
    
    @api.model
    def _get_cuzdan_default(self):
        kart = self.env['info_kart.lot_cuzdan_rel'].sudo().search([('lot_id','=',self._get_default_kart_id())])
        ids  = [x.cuzdan.id for x in kart]
        return [('id','in',ids),('serbest','=',False)]
    
    cuzdan      = fields.Many2one('info_kart.cuzdan',string='Cüzdan',default=check_cuzdan,domain=_get_cuzdan_default)
    cuzdan_kodu = fields.Char(related='cuzdan.kod')
    kart        = fields.Many2one('stock.production.lot',string='Bakiyesi geri Çekilecek Kart No',default=_get_default_kart_id)
    bakiye      = fields.Float()
    gift        = fields.Float(related='kart.gift_limit')
    partner     = fields.Many2one(related='kart.sold_to')
    ava_bakiye  = fields.Many2many('info_kart.set_limit',string='İade Alınabilecek Ödemeler')
    total_tutar = fields.Float('İade Tutarı', required=True)
    curr        = fields.Many2one('res.currency',default=lambda self: self.env['res.currency'].search([('name','=','TRY')], limit=1))
    
    islem_tipi  = fields.Selection([('-1','Tutar İptal/İade'),
                                    ('1','Tutar Bloke'),
                                    ('2','Tutar Bloke Kaldır'),
                                    ('3','Kayıp Kart(Tüm Cüzdanlara Bloke Koyar)'),
                                    ('4','Tüm Blokeleri Kaldır ve Kartı Kullanıma Al'),
                                    #('5','Kayıp Kart Bildir ve Yeni Kart Ata')
                                    ],string='İşlem Tipi',required=True)
    #islem_tipi  = fields.Selection([('1','Tutar Bloke'),('2','Tutar Bloke Kaldır'),('3','Kayıp Kart'),('4','Kayıp Bildirimi İptali')],string='İşlem Tipi',required=True)
    yeni_kart        = fields.Many2one('stock.production.lot',string='Atanacak Yeni Kart(Bakiye Transferi Otomatik Gerçekleşir.)' )
    label_yeni_kart  = fields.Char()
    @api.onchange('islem_tipi')
    def islem_tipi_onchange(self):
        domain = {}
        if self.islem_tipi == '5':
            domain = {'yeni_kart':[('company_id','=',self.kart.company_id.id),
                                   ('lot_state','=','draft2')]}
        return{'domain':domain}

    @api.onchange('cuzdan')
    def onchange_cuzdan(self):
        self.ava_bakiye  = [(5,)]
        res              =  self.get_res(self.kart,self.total_tutar,res={'cuzdan':self.cuzdan.id})
        self.ava_bakiye  = res['ava_bakiye']
        cuzdan_rel       = self.env[('info_kart.lot_cuzdan_rel')].sudo().search([('lot_id','=',self.kart.id),('cuzdan','=',self.cuzdan.id)])
        self.total_tutar = cuzdan_rel.bakiye
        self.bakiye      = cuzdan_rel.bakiye
            
    @api.onchange('total_tutar')
    def onchange_total_tutar( self ):
        res = {}
        if self.total_tutar:
            cuzdan_rel       = self.env[('info_kart.lot_cuzdan_rel')].sudo().search([('lot_id','=',self.kart.id),('cuzdan','=',self.cuzdan.id)])
            total_tut        = cuzdan_rel.bakiye
            if self.total_tutar > total_tut:
               self.total_tutar = total_tut
               res['warning']= {'title': 'Hatalı Tutar','message': 'Geri Alınacak Tutar Cüzdandaki Bakiyeden Fazla Olamaz.'}
            else:
                self.ava_bakiye = [(5,)]
                res             =  self.get_res(self.kart,self.total_tutar,res={'cuzdan':self.cuzdan.id})
                self.ava_bakiye = res['ava_bakiye']
        return res
    
    def total_bloke_koy_kaldir(self, islem_tipi):
        msj = ''
        cuzdans = self.env['info_kart.lot_cuzdan_rel'].sudo().search([('lot_id','=',self.kart.id)])
        s = False
        for k in cuzdans:
            tutar = k.bakiye
            att = {}
            if int(k.cuzdan.kod) == 1000:
                tutar = self.kart.serbest_limit
                if islem_tipi == 1:
                    att   = {'serbest_limit_b':tutar}
                else:
                    att   = {'serbest_limit_b':0.0}
                s = True
            elif int(k.cuzdan.kod) == 1084:
                tutar = self.kart.available_limit
                if islem_tipi == 1:
                    att   = {'available_limit_b':tutar}
                else:
                    att   = {'available_limit_b':0.0}
            elif int(k.cuzdan.kod) == 1102:
                tutar = self.kart.gift_limit
                if islem_tipi == 1:
                    att   = {'gift_limit_b':tutar}
                else:
                    att   = {'gift_limit_b':0.0}
            donen = TL_Bloke_Islem(self.kart,tutar,k.cuzdan.kod,islem_tipi)
            if donen.Sonuc != 1:
                msj = 'İşlem Başarısız : Cüzdan : %s  %s - %s'%(k.cuzdan.name,donen.Sonuc,donen.Sonuc_Str)
                r = "/web/payresult?inner_message=1&success=%s&msj=%s"%(-1,msj)
                return {
                            'type': 'ir.actions.act_url',
                            'url': r,
                            'target': 'popup'
                        }
            else:
                if att:
                    self.kart.write(att)
                if islem_tipi == 1:
                    k.bakiye_b = tutar
                else:
                    k.bakiye_b = 0.0
                msj += 'Cüzdan : %s  %s \n' %(k.cuzdan.name,donen.Sonuc_Str )
        
        if not s:        
            donen = TL_Bloke_Islem(self.kart,self.kart.serbest_limit,1000,islem_tipi)#serbest cüzdan
            if donen.Sonuc != 1:
                msj = 'İşlem Başarısız : Cüzdan : %s  %s - %s'%('Serbest TL',donen.Sonuc,donen.Sonuc_Str)
                r = "/web/payresult?inner_message=1&success=%s&msj=%s"%(-1,msj)
                return {
                            'type': 'ir.actions.act_url',
                            'url': r,
                            'target': 'popup'
                        }
            else:
                if islem_tipi == 1:
                    self.kart.serbest_limit_b = self.kart.serbest_limit
                else:
                    self.kart.serbest_limit_b  = 0.0
                msj += 'Cüzdan : %s  %s \n' %('Serbest TL ',donen.Sonuc_Str )
        
        bakiye(self.kart)
        if islem_tipi == 1:
            self.kart.prev_state = self.kart.lot_state
            self.kart.lot_state  = 'scrapped'
        else:
            if self.kart.lot_state == 'scrapped':
                self.kart.lot_state  = self.kart.prev_state or 'sold'
            
    @api.multi
    def confirm_and_proceed( self ):
        msj=''
        r = ''
        tutar = self.total_tutar
        if int(self.islem_tipi) == -1:
            self.ava_bakiye = [(5,)]
            res             =  self.get_res(self.kart,tutar,res={'cuzdan':self.cuzdan.id})
            self.ava_bakiye = res['ava_bakiye']
            final_refund    = 0
            refund_ids      = []     
            if not self.ava_bakiye:
                raise exceptions.ValidationError('İade / İptal Edilebilecek Bir Ödeme Bulunamadı')
            
            for i in self.ava_bakiye:
                new_pay = None
                if i.tutar >= tutar:
                    #final step
                    now    = datetime.datetime.now()
                    y_date = i.create_date
                    flag   = False
                    if y_date + datetime.timedelta(days=1) < now:
                        #24 saat geçmiş, parçalı iade yapabilir.
                        i_tutar = tutar
                    else:
                        #24 saat geçmemiş, parçalı iade olamaz.ama son tutar i.tutara eşit olabilir. bu durumda ödeme oluşturmayacağız.
                        i_tutar = i.tutar
                        flag    = True
                    donen   = tl_iptal_iade( self.kart, i.yid, i_tutar)      
                    
                    if donen.Sonuc == -3 or donen.Sonuc == -13:
                        refund_ids.append( [i,i_tutar] )
                        final_refund += i_tutar #bu durum, iade sistem dışı yapılınca oluyor, o halde bakiyeyi artıralım.
                        if final_refund > 0:
                            msj = 'Girilen Tutarın Bir Kısmı veya Tamamı Geri Alındı veya Önceden Geri Alınmış : %s TL \n\
                               Son Servis Mesajı : %s-%s'%(final_refund,donen.Sonuc, donen.Sonuc_Str)
                        else:
                            msj = ' %s için İptal İade Başarısız. Servis Mesajı : %s-%s'%(self.kart.name,donen.Sonuc, donen.Sonuc_Str)
                        r = "/web/payresult?inner_message=1&success=%s&msj=%s"%(-1,msj)
                    elif donen.Sonuc == 1 :
                        refund_ids.append([i,i_tutar])
                        final_refund += i_tutar
                        if flag:
                            fark    = i_tutar - tutar
                            if fark > 0: 
                                new_pay = self.env['info_kart.set_limit'].sudo().with_context({'active_model':'stock.production.lot',
                                                                                                    'active_id':self.kart.id,
                                                                                                    'kart':True}).create(
                                                                  {'partner':self.kart.sold_to.id,
                                                                   'lot':self.kart.id,
                                                                   'tutar':fark,
                                                                   'cuzdan':self.cuzdan.id,
                                                                   'state':'draft'})
                    else:
                        if final_refund > 0:
                            msj = 'Girilen Tutarın Bir Kısmı veya Tamamı Geri Alındı veya Önceden Geri Alınmış, Geri Alınan Tutar : %s TL \n\
                                   Son Servis Mesajı : %s-%s'%(final_refund,donen.Sonuc, donen.Sonuc_Str)
                        else:
                            msj = '%s için İptal İade Başarısız. Son Servis Mesajı : %s-%s'%(self.kart.name,donen.Sonuc, donen.Sonuc_Str)
                        r = "/web/payresult?inner_message=1&success=%s&msj=%s"%(-1,msj)
                    break #loop bitmeli ama tutarı azaltmıyoruz, o halde braek koyalım nolur noolmaz :D
                else:
                    donen = tl_iptal_iade( self.kart, i.yid, i.tutar)
                    if donen.Sonuc == 1:
                        final_refund += i.tutar
                        refund_ids.append([i,i.tutar])
                    elif donen.Sonuc == -3 or donen.Sonuc == -13:
                        final_refund += i.tutar #bu durum, iade sistem dışı yapılınca oluyor, o halde bakiyeyi artıralım.
                        refund_ids.append([i,i.tutar])
                    tutar -= i.tutar

            cuzdan = False
            for c in self.partner.cuzdanlar:
                if c.cuzdan.kod == self.cuzdan.kod:
                    c.bakiye += final_refund
           
            for ref in refund_ids:     
                
                li  = ref[0]
                tut = ref[1]
                
                li.iade_tutar    += tut
                new_iade = li.copy({'tutar':tut,'state':'iade','iade_tutar':0,'main_set_limit':li.id})
                    
            if new_pay:
                new_pay.confirm()
        elif int(self.islem_tipi) == 1:
            #print self.cuzdan.kod, ' - ', self.islem_tipi
            donen = TL_Bloke_Islem(self.kart,tutar,self.cuzdan.kod,self.islem_tipi)
            if donen.Sonuc != 1:
                msj = 'İşlem Başarısız : %s - %s'%(donen.Sonuc,donen.Sonuc_Str)
                r = "/web/payresult?inner_message=1&success=%s&msj=%s"%(-1,msj)
                
                return {
                            'type': 'ir.actions.act_url',
                            'url': r,
                            'target': 'popup'
                        }
            
            
            att = {}
            if int(self.cuzdan.kod) == 1000:
               
                att   = {'serbest_limit_b':tutar + self.kart.serbest_limit_b}
            elif int(self.cuzdan.kod) == 1084:
                 att   = {'available_limit_b':tutar + self.kart.available_limit_b}
            elif int(self.cuzdan.kod) == 1102:
                att   = {'gift_limit_b':tutar + self.kart.gift_limit_b}
            
            cuzdan_rel = self.env[('info_kart.lot_cuzdan_rel')].sudo().search([('lot_id','=',self.kart.id),('cuzdan','=',self.cuzdan.id)])
            cuzdan_rel.bakiye_b += tutar

            bakiye (self.kart)

            if att:
                self.kart.write( att )
            msj = donen.Sonuc_Str
        elif int(self.islem_tipi) == 2:
            #print self.cuzdan.kod, ' - ', self.islem_tipi
            donen = TL_Bloke_Islem(self.kart,tutar,self.cuzdan.kod,self.islem_tipi)
            if donen.Sonuc != 1:
                msj = 'İşlem Başarısız : %s - %s'%(donen.Sonuc,donen.Sonuc_Str)
                r = "/web/payresult?inner_message=1&success=%s&msj=%s"%(-1,msj)
                return {
                            'type': 'ir.actions.act_url',
                            'url': r,
                            'target': 'popup'
                        }
            att = {}
            if int(self.cuzdan.kod) == 1000:
                att   = {'serbest_limit_b':0.0}
            elif int(self.cuzdan.kod) == 1084:
                 att   = {'available_limit_b':0.0}
            elif int(self.cuzdan.kod) == 1102:
                att   = {'gift_limit_b':0.0}
            
            cuzdan_rel = self.env[('info_kart.lot_cuzdan_rel')].sudo().search([('lot_id','=',self.kart.id),('cuzdan','=',self.cuzdan.id)])
            cuzdan_rel.bakiye_b = 0.0
            
                
            bakiye(self.kart)
            if att:
                self.kart.write( att )
            msj = donen.Sonuc_Str
        elif int(self.islem_tipi) == 3:
            return self.total_bloke_koy_kaldir( 1 )
        elif int(self.islem_tipi) == 4:
            return self.total_bloke_koy_kaldir( 2 )
        elif int(self.islem_tipi) == 5:
            
            if not self.kart or not self.yeni_kart:
                raise exceptions.ValidationError("Lütfen Atanacak Kartı Seçiniz, Yedek Kart Stoğunuzda Kart Yoksa Talop Ediniz.")
            else:
                res,res_t = Kayip_Calinti(self.kart, self.yeni_kart)
                if res:
                    if int(res) != 1:
                        msj = res_t
                        r   = "/web/payresult?inner_message=1&success=%s&msj=%s"%(-1,msj)
                    else:
                        
                        self.yeni_kart.write({'lot_state':self.kart.lot_state,
                                              'ad_soyad':self.kart.ad_soyad,
                                              'cep_tel':self.kart.cep_tel,
                                              'dogum_tar':self.kart.dogum_tar,
                                              'sube_id':self.kart.sube_id,
                                              'tckn':self.kart.tckn,
                                              'email':self.kart.email,
                                              'adres':self.kart.adres,
                                              'il':self.kart.il,
                                              'ilce':self.kart.ilce,
                                              'sicil_no':self.kart.sicil_no,
                                              'iliski_status':self.kart.iliski_status})
                        self.kart.prev_state = self.kart.lot_state
                        self.kart.lot_state  = 'scrapped'
                        
                else:
                    msj = 'Sistem Hatası'
                    r   = "/web/payresult?inner_message=1&success=%s&msj=%s"%(-1,msj)
                
        if not msj:
            msj = 'İşlem Başarıyla Gerçekleştirildi'
        if not r:
            r = "/web/payresult?inner_message=1&success=%s&msj=%s"%(1,msj)
        return {
                    'type': 'ir.actions.act_url',
                    'url': r,
                    'target': 'popup'
                }

class setLimitWizard(models.Model):
    _name = 'info_kart.set_limit'
    _order = "id desc"
    def _get_default_partner(self):
        if self._context.get('active_model') and self._context.get('active_model') == 'res.partner':
            return self._context.get('active_id')
        elif self._context.get('active_model') and self._context.get('active_model') == 'stock.production.lot':
            return self.env['stock.production.lot'].browse([self._context.get('active_id')]).sold_to.id
    
    @api.model
    def _get_cuzdan_domain(self):
        poslar    = self.env['res.partner'].sudo().browse( self._get_default_partner() ).cuzdanlar
        cuzdanlar = [x.cuzdan.id for x in poslar ]
        
        return [('id','in',cuzdanlar),('serbest','=',False)]
    
    def _get_default_lot(self):
        if self._context.get('active_model') and self._context.get('active_model') == 'stock.production.lot':
            return self._context.get('active_id')
    
    def _get_p_limit_default(self):
        limit = 0
        if self._context.get('active_model') and self._context.get('active_model') == 'res.partner':
            limit = self.env['res.partner'].browse([ int(self._context.get('active_id')) ]).available_limit
        elif self._context.get('active_model') and self._context.get('active_model') == 'stock.production.lot':
            limit = self.env['stock.production.lot'].browse([ int(self._context.get('active_id')) ]).sold_to.available_limit
        
        return limit
    
    def _get_k_limit_default(self):
        limit = self._get_p_limit_default()
        if self._context.get('active_model') and self._context.get('active_model') == 'res.partner':
            limit += self.env['res.partner'].browse([ int(self._context.get('active_id')) ]).credit_limit
        elif self._context.get('active_model') and self._context.get('active_model') == 'stock.production.lot':
            limit += self.env['stock.production.lot'].browse([ int(self._context.get('active_id')) ]).sold_to.credit_limit
        return limit

    def check_cuzdan(self):
        return False
        c_obj  =  self.env['info_kart.cuzdan']
        cuzdan =  c_obj.search([])
        if cuzdan:
            return cuzdan[0].id
        else:
            c_dict = Cuzdan( self )
            f      = 0
            for k,v in c_dict.items():
                new_cr = {'name':v,
                          'kod' :k}
                o = c_obj.create( new_cr )
                f = o.id
            return f

    partner         = fields.Many2one('res.partner', default         = _get_default_partner)
    cuzdan          = fields.Many2one('info_kart.cuzdan',string='Cüzdan',default=check_cuzdan,domain=_get_cuzdan_domain)
    kurumsal_cuzdan = fields.Many2one('info_kart.cuzdan',string='Cüzdan',domain=_get_cuzdan_domain)
    p_limit         = fields.Float(related='partner.available_limit',default=_get_p_limit_default)
    kr_tutar        = fields.Float(related='partner.credit_limit')
    
    k_limit         = fields.Float(compute='_get_k_limit', string='Kredili Kullanılabilir Limit', default=_get_k_limit_default)
    lot             = fields.Many2one('stock.production.lot',default = _get_default_lot)
    l_limit         = fields.Float(related='lot.available_limit') 
    
    tutar           = fields.Float(string='Yüklenecek Bakiye')
    iade_tutar      = fields.Float(string='İade Bakiye')
    
    final_bakiye    = fields.Float(compute='get_final_bakiye')
    
    tutar_c         = fields.Many2one('res.currency',default=lambda self: self.env['res.currency'].search([('name','=','TRY')], limit=1))
    odeme_yontemi   = fields.Selection(selection=[('kk','Kredi Kartı'),('hv','Havale/EFT'),('ck','Çek')])
    hesap_no        = fields.Many2one('account.journal',domain=[('type','=','bank'),('company_id','=',1),('bayiden_odeme_alir','=',True)],string='Havale / Eft Yapılacak Hesap')
    
    
    tutar_cekilecek     = fields.Float(string='Kredi Kartından Çekilecek Tutar')
    komisyon_dahil      = fields.Boolean()
    kart_sahibi         = fields.Char(string='Kart Üzerindeki İsim')
    kart_no_1           = fields.Char(string='Kart No', size=4)
    kart_no_2           = fields.Char(string='Kart No', size=4)
    kart_no_3           = fields.Char(string='Kart No', size=4)
    kart_no_4           = fields.Char(string='Kart No', size=4)
    amex                = fields.Boolean(string='Kart No',size=3)
    amex_2              = fields.Char(string='Kart No', size=6)
    amex_3              = fields.Char(string='Kart No', size=5)
    
    kart_no             = fields.Char(string='Kart No')
    cvc                 = fields.Char(string='CVC Kodu', size=4)
    card_image          = fields.Html()
    
    sk_tarihi_ay        = fields.Selection(selection=[(1, '01'),(2, '02'),(3, '03'),(4, '04'),(5, '05'),(6, '06'),(7, '07'),(8, '08'),(9, '09'),(10, '10'), (11, '11'),(12, '12'),], string='Son Kullanma Tarihi Ay')
    sk_tarihi_yil       = fields.Selection(selection=[(17, '17'),(18, '18'),(19, '19'),(20, '20'),(21, '21'),(22, '22'),(23, '23'),(24, '24'),(25, '25'),(26, '26'),(27, '27'),(28, '28'),(29, '29'),(30, '30'),(31, '31'),(32, '32')], string='Son Kullanma Tarihi Ay')

    checks               = fields.One2many('info_check.check','wizard_id',string="Ödeme İçin Verilen Çekler")
    state                = fields.Selection(selection=[('draft','Yeni Ödeme'),
                                                        ('cancel','Ödeme İptal Edildi'),
                                                        ('confirm','Ödeme Onaylandı'),
                                                        ('iade','İptal/İade Yapıldı')],
                                            default='draft')
    
    kk_state             = fields.Char("Kredi Kartı Onay Durumu")
    
    kart_tipi_ok         = fields.Boolean()
    sanal_pos            = fields.Char()
    payload              = fields.Char()
    siparis_no           = fields.Char('Kredi Kartı Sipariş No')
    yid                  = fields.Char('Yükleme Dekont Id')
    hid                  = fields.Char('Harcama Dekont Id')
    cuzdan_rel_id        = fields.Many2one('info_kart.cuzdan_partner_rel')
    all_cuzdans          = fields.One2many(related='partner.cuzdanlar')
    
    main_set_limit       = fields.Many2one('info_kart.set_limit') 
    
    @api.onchange('cuzdan')
    def onchange_cuzdan(self):
        if self.cuzdan:
            cuzdan_rel = self.env[('info_kart.lot_cuzdan_rel')].sudo().search([('lot_id','=',self.lot.id),('cuzdan','=',self.cuzdan.id)])
            if cuzdan_rel:
                self.l_limit = cuzdan_rel.bakiye

    @api.multi
    def get_final_bakiye(self):
        for s in self:
            s.final_bakiye = s.tutar - s.iade_tutar
    
    @api.onchange('odeme_yontemi')
    def onchange_odeme(self):
        if self.odeme_yontemi == 'ck':
            self.tutar = 0.0
    
    def get_bin(self,bin_number):
        if len(self.env['info_bayi.tp_ozel_oran_sk_liste'].sudo().search([])) == 0:
            TP_Ozel_Oran_SK_Liste( self )
        bin_pos = BIN_SanalPos(self,bin_number)
        if bin_pos.get('SanalPOS_ID'):
            image = self.env['info_bayi.tp_ozel_oran_sk_liste'].sudo().search([('SanalPOS_ID','=',bin_pos['SanalPOS_ID'])])
            if image:
                self.sanal_pos      = image[0].SanalPOS_ID
                image               = image[0].Kredi_Karti_Banka_Gorsel
                tag                 = "<img src=%s />" %image
                self.card_image     = tag
                self.kart_tipi_ok   = True
        else:
            self.card_image   =  ""
            self.kart_tipi_ok = False
            
    @api.onchange('kart_no')
    def onchange_kart_no(self):
        if self.kart_no:
            kart_no_to_bin = self.kart_no.replace(" - ","")[:6]
            self.get_bin( kart_no_to_bin )
        else:
            self.card_image   =  ""
            self.kart_tipi_ok = False

    @api.onchange('checks')
    def onchange_checks(self):
        self.ensure_one()
        t = 0.0
        for c in self.checks:
            t+= c.amount
            
        self.tutar = t
    
    @api.multi
    @api.depends('p_limit','partner')
    def _get_k_limit( self ):
        for s in self:
            s.k_limit = s.partner.available_limit + s.partner.credit_limit
    
    @api.multi
    def get_limit_p(self):
        self.ensure_one()
        bakiye(self.partner, 'partner')
        return {
            "type": "ir.actions.do_nothing",
        }
    
    @api.multi
    def get_limit_k(self):
        self.ensure_one()
        bakiye(self.lot)
        return {
            "type": "ir.actions.do_nothing",
        }
    
    def get_kurumsal_cuzdan(self,partner_id = False, cuzdan_id = False):
        
        cuzdan_obj = self.cuzdan_rel_id
        if not cuzdan_obj:
            p = self.partner.id or partner_id
            c = self.kurumsal_cuzdan.id or cuzdan_id
            cuzdan_obj = self.env['info_kart.cuzdan_partner_rel'].sudo().search([('partner_id','=',p),('cuzdan','=',c)])
            if len( cuzdan_obj ) > 0:
                cuzdan_obj = cuzdan_obj[-1]
        if not cuzdan_obj:
            raise exceptions.ValidationError("Yükleme Yapılan Cüzdan Bulunamadı. Lütfen Sistem Yöneticinizle İletişime Geçiniz.")
        return cuzdan_obj
    
    @api.multi
    def confirm(self):
        self.ensure_one()
        self.state = 'confirm'
        if self._context.get('kart'):
            self.partner      = self.lot.sold_to.id
            cuzdan_obj        = self.get_kurumsal_cuzdan(partner_id = self.partner.id, cuzdan_id = self.cuzdan.id)
            prev_limit        = cuzdan_obj.bakiye
            if self.tutar > cuzdan_obj.total_bakiye:
               raise exceptions.ValidationError("Yüklenecek tutar kullanılabilir bakiyenizin üzerinde, Lütfen önce Kurumsal Müşteri Bakiyenizi Artırınız.")
            next_limit        = prev_limit - self.tutar
            cuzdan_obj.bakiye = next_limit
            y_dekont, h_dekont, sonuc_str = tl_transfer(self.lot,self.tutar,'Yemekmatik A.Ş. Bakiye Yükleme Bildirimidir.',self.cuzdan.kod)
            success = 1
            srv     = ""
            if not y_dekont:
                cuzdan_obj.bakiye = prev_limit
                self.state = 'cancel'
                success    = 0
                srv        = u'<li style="color:red"><b>Hata :</b> %s</li></ul>'% sonuc_str 
            else:
                bakiye( self.lot )
            self.yid = y_dekont
            self.hid = h_dekont
            self.kk_state = sonuc_str
            msj           = u"""<ul style="list-style-type: none;padding-right: 40px;"><li><b>Kart No :</b> %s</li><li><b>Tutar   :</b> %s TL</li><li><b>Yükleme Dekont no :</b> %s</li> """ %(self.lot.name,
                                                self.tutar,
                                                y_dekont or ''
                                                )
            msj           += srv
            #return get_popup_w(self,header,msj)
            r = "/web/payresult?inner_message=1&success=%s&msj=%s"%(success,msj)
            return {
                        'type': 'ir.actions.act_url',
                        'url': r,
                        'target': 'popup'
                    }
        else:
            cuzdan_obj        = self.get_kurumsal_cuzdan()
            prev_limit        = cuzdan_obj.bakiye
            next_limit        = prev_limit + self.tutar
            cuzdan_obj.bakiye = next_limit
        
    @api.multi
    def cancel(self):
        self.ensure_one()
        self.state = 'cancel'
        
    @api.multi
    def confirm_and_proceed(self):
        
        if self.tutar > 0:
            if self._context.get('active_model') and self._context.get('active_model') == 'stock.production.lot':
                if not self.cuzdan:
                    raise exceptions.ValidationError("Yükleme Yapılacak Cüzdanı Seçiniz.")
                return self.confirm()
            if self._context.get('active_model') and self._context.get('active_model') == 'res.partner':
                if self.odeme_yontemi == 'kk':
                    self.env['info_bayi.tp_ozel_oran_sk_liste'].sudo().search([]).unlink()
                    TP_Ozel_Oran_SK_Liste( self )
                    r = TP_Islem_Odeme(self )
                    if r:
                        return {
                            'type': 'ir.actions.act_url',
                            'url': r,
                            'target': 'popup'
                        }
                else:
                    email_template = self.env.ref('info_kart.muhasebe_odeme_email_template')
                    mail_id   = email_template.send_mail(self.id, raise_exception="Mail gönderilemedi Parametreleri ve Mail adresini Kontrol Ediniz.", force_send=True)
                    #mail_obj  = self.env['mail.mail'].sudo().browse( mail_id )
                    
                    msj = u"""Ödeme Kaydınız Alındı, Muhasebe Onayından Sonra Kullanılabilir Bakiyenize Yansıtılacaktır. Teşekkür Ederiz.""" 
                    r = "/web/payresult?inner_message=1&success=%s&msj=%s"%(1,msj)
                    return {
                                'type': 'ir.actions.act_url',
                                'url': r,
                                'target': 'popup'
                            }
        else:
            raise exceptions.ValidationError("Yüklenecek tutar 0'dan büyük olmalıdır.")
        
    def _get_next_oid ( self ):
            obj_sequence = self.env['ir.sequence'].sudo()
            idd = obj_sequence.next_by_code('odeme_yap.sequence')
            ##print "--------------> sipariş no: ", idd
            return idd
    
    @api.model
    def create(self,values):
        if self._context.get('partner') and not values.get('odeme_yontemi'):
            raise exceptions.ValidationError("Lütfen Ödeme Yöntemi Seçiniz")
        if not values.get('tutar'):
            raise exceptions.ValidationError("Yüklenecek tutar 0'dan büyük olmalıdır.")
        if values.get("odeme_yontemi") == 'kk':

            param = self.env['ir.config_parameter'].get_param("web.base.external_url", default='http://localhost:8071')
            EST_RETURN_URL = param + "/web/payresult"
            EST_FAIL_URL = param + "/web/payresult"

            m = str(values['sk_tarihi_ay'])
            if len(m) == 1:
                m = '0' + m
            # print values['tutar'] , ' : ' ,    "{0:.2f}".format (values['tutar']).replace('.',','),
            payload = dict(
                Siparis_ID=self._get_next_oid(),
                Islem_Tutar="{0:.2f}".format(values['tutar']).replace('.', ','),
                Toplam_Tutar="{0:.2f}".format(values['tutar']).replace('.', ','),
                SanalPOS_ID=values['sanal_pos'],
                Hata_URL=EST_RETURN_URL,
                Basarili_URL=EST_FAIL_URL,
                KK_No=values['kart_no'].replace(" - ", ""),
                KK_CVC=values['cvc'],
                KK_SK_Yil='20' + str(values['sk_tarihi_yil']),
                KK_SK_Ay=m,
                Taksit="1",
                KK_Sahibi=values['kart_sahibi']
            )
            values['payload'] = payload
            values['cvc'] = False

            values['kart_no'] = values['kart_no'].replace(" - ", "")
            values['kart_no'] = values['kart_no'][0:6] + "******" + values['kart_no'][len(values['kart_no']) - 4:]
            values['sk_tarihi_ay'] = False
            values['sk_tarihi_yil'] = False
            values['siparis_no'] = payload['Siparis_ID']
            values['diger_tutars'] = False

        elif values.get('odeme_yontemi') == 'hv':
            if len(self.search([('partner', '=', self._context.get('active_id')),
                                ('state', '=', 'draft'),
                                ('odeme_yontemi', '=', 'hv')])) > 0:
                raise exceptions.ValidationError(u'Hali Hazırda Onayda Bekleyen Havaleniz Bulunmaktadır. \n\
                                                 Bu Havale İşlemi Onaylanmadan Başka Havale Yapamazsınız. \n\
                                                 Diğer Ödeme Yöntemlerini Kullanabilirsiniz.')
        res = super(setLimitWizard, self).create(values)

        return res

class kart_ata_wizard(models.TransientModel):
    _name = 'info_kart.kart_ata_wizard'
    @api.model
    def get_kart_domain(self):
        domain =[('company_id','=',self.env.user.company_id.id ),('lot_state','=','draft')]
        return domain
    @api.model
    def _get_default_partner(self):
        return self._context.get('active_id')
    kurumsal_kart = fields.Many2many('stock.production.lot' ,string='Kartları Seçiniz', domain= get_kart_domain)
    
    bireysel_kart = fields.Many2one('stock.production.lot',string='Kart Seçiniz',  domain= get_kart_domain)
    
    partner_id    = fields.Many2one('res.partner',default=_get_default_partner)
    stock         = fields.Selection(selection=(('true',"Yedek Kart Stoğuna Gönder"),('false','Kullanım İçin Gönder')),string="Atama Tipi")
    
    @api.multi
    def confirm_and_proceed(self):
        self.ensure_one()
        if self.bireysel_kart:
            #self.bireysel_kart.sold_to              = self.partner_id.id
            self.bireysel_kart.bireysel_partner_id  = self.partner_id.id
            self.bireysel_kart.lot_state = 'sold2'
            self.bireysel_kart.ad_soyad  = self.partner_id.name
            self.bireysel_kart.tckn      = self.partner_id.tckn
            self.bireysel_kart.cep_tel   = self.partner_id.gsm_no
        elif self.kurumsal_kart:
            for i in self.kurumsal_kart:
                state       = 'sold'
                if self.stock == 'true':
                   state    = 'draft2'
                partner     = self.partner_id
                if partner.parent_id:
                    i.sube_id   = partner.id
                    partner     = partner.parent_id
                i.sold_to       = self.partner_id.id
                i.company_id    = self.partner_id.company_id.id
                if i.lot_state not in ('sold2','sold3','scrapped'):
                    i.lot_state     = state
                    i.ad_soyad      = False
                    i.tckn          = False
                    i.cep_tel       = False                

class upload_card_excel(models.TransientModel):
    _name = 'info_kart.upload_kart_excel_wizard'
    
    excel_file      = fields.Binary(string='Excel Dosyası',required=True)
    excel_file_name = fields.Char('Excel Dosya Adı')
    
    @api.multi
    def confirm_and_proceed(self):
        self.ensure_one()
        
        excel_file = base64.b64decode( self.excel_file )
        book  = xlrd.open_workbook(file_contents=excel_file ,encoding_override= 'utf-8')
        deep_sheet  = book.sheets()[0]
        deep_rows     = deep_sheet.get_rows()#generator bu so yield is in action !!! cool.
        already_added = []
        c = 1
        #f = open('/var/lib/odoo/yedek/finansbank_parse_res.txt','w' )
        for dr in deep_rows:
            c = c+1
            counter     = str(dr[0].value)
            kart_no     = dr[1].value.replace(" ","")
            name = kart_no
            #print name
            if counter.startswith('000'):
                #finansbank fail safe
                kart_no     = dr[2].value
                kart_no     = '97925700' + kart_no
                kart_no     = kart_no[0:4] + ' ' + kart_no[4:8] + ' ' + kart_no[8:12] + ' '  + kart_no[12:16]
                seri_no_enc = EncodeAES(cipher,  kart_no)
                
                db_kart          = self.env['stock.production.lot'].sudo().search([('seri_no','=',seri_no_enc)])
                #print kart_no , db_kart
                db_kart.sold_to  = 221
                db_kart.ad_soyad = dr[1].value
                db_kart.sicil_no = dr[0].value
                db_kart.lot_state = 'sold'
                
                sube = self.env['res.partner'].search([('display_name','ilike',dr[4].value.replace('I','ı').capitalize()),('parent_id','=',221),('city_combo','=',35)])
                #print sube
                if len(sube) == 1:
                    kargo = self.env['info_kart.kargo'].search([('partner_id','=',sube.id)])
                    #print kargo
                    if kargo:
                        kargo[0].kartlar = [(4,db_kart.id)]
                    
                    db_kart.sube_id = sube.id
                
                else:
                    sube = self.env['res.partner'].search([('display_name','ilike',dr[4].value),('parent_id','=',221),('city_combo','=',35)])
                    if len(sube) == 1:
                        kargo = self.env['info_kart.kargo'].search([('partner_id','=',sube.id)])
                        #print kargo
                        if kargo:
                            kargo[0].kartlar = [(4,db_kart.id)]
                    
                        db_kart.sube_id = sube.id
                    else:
                        f.write(str(c) + ' ' + dr[4].value + '\n')
                
            else:
                #print dr
                if len( kart_no) == 16 and kart_no.startswith('979257'):
                    
                    #print dr
                    
                    skt         = str(dr[2].value).replace(" ","") or False
                    kart_type   = str(dr[3].value).replace(" ","")
                    
                    ad          = str(dr[4].value).replace(" ","") or False
                    soyad       = str(dr[5].value).replace(" ","") or False
                    
                    tc          = str(dr[6].value).replace(" ","") or False
                    sicil_no    = str(dr[7].value).replace(" ","") or False
                    if ad and soyad :
                        ad_soyad = ad +' '+soyad
                    else:
                        ad_soyad = False
                    
                    if skt:
                        skt   = skt.split('/')
                        day   = '01'
                        month = skt[0]
                        year  = '20' + skt[1]
                        fskt   = year + '-' + month + '-' + day
                    else:
                        fskt  =False
                
                    name = [str(s) for s in kart_no.split() if s.isdigit()]
                    name = name[0]
                    name = name[0:4] + ' ' + name[4:6] + '** **** '  + name[12:16]
                    kart_no = kart_no[0:4] + ' ' + kart_no[4:8] + ' ' + kart_no[8:12] + ' '  + kart_no[12:16]
                
                    kart_type =self.env['info_kart.kart_tipi'].sudo().search([('name','ilike',kart_type)])
                    if kart_type:
                        kart_type = kart_type[-1].id
                    
                    dict_ = dict(name=name,
                                 company_id = 1,
                                 lot_state = 'draft',
                                 due_date  = fskt,
                                 seri_no   = kart_no,
                                 kart_type = kart_type,
                                 ad_soyad  = ad_soyad,
                                 tckn      = tc,
                                 sicil_no  = sicil_no,
                                 sold_to   = 1)
                    seri_no_enc = EncodeAES(cipher, kart_no)
                    alreadey_added_card = self.env['stock.production.lot'].sudo().search([('seri_no','=',seri_no_enc)])
                    if not alreadey_added_card:
                        self.env['stock.production.lot'].sudo().create( dict_ )
                    else:
                        if alreadey_added_card.lot_state in ('draft','sold'):
                            keys    = ['ad_soyad','tckn','sicil_no']
                            dict_up = {x:dict_[x] for x in keys}
                            alreadey_added_card.write( dict_up )
                        else:
                            keys    = ['sicil_no']
                            dict_up = {x:dict_[x] for x in keys}
                            alreadey_added_card.write( dict_up )
                elif len( kart_no) == 10 and not kart_no.startswith('979257'):
                    guid        = dr[2].value
                    
                    kart_no     = '979257' + kart_no
                    kart_no     = kart_no[0:4] + ' ' + kart_no[4:8] + ' ' + kart_no[8:12] + ' '  + kart_no[12:16]
                    seri_no_enc = EncodeAES(cipher,  kart_no)
                    
                    db_kart     = self.env['stock.production.lot'].sudo().search([('seri_no','=',seri_no_enc)])
                    if db_kart :
                        db_kart.write({'guid':guid})
        #f.close()
        act =  {
                    'type': 'ir.actions.act_window',
                    'name':'Kart No',
                    'res_model': 'stock.production.lot',
                    'view_mode':"tree,form",
                    'view_type':"form",
                    'target':"self",
                    };    
        return act

class upload_bakiye_excel(models.TransientModel):
    
    _name = 'info_kart.upload_bakiye_excel'
    
    excel_file      = fields.Binary(string='Excel Dosyası',required=True)
    excel_file_name = fields.Char('Excel Dosya Adı')
    cuzdan_id       = fields.Many2one('info_kart.cuzdan_partner_rel',default=lambda self:self._context.get('active_id'))
    
    @api.multi
    def confirm_and_proceed(self):
        
        self.ensure_one()
        p = self.cuzdan_id.partner_id
        p.excel_dosya     = self.excel_file
        p.excel_file_name = self.excel_file_name
        p.cuzdan          = self.cuzdan_id.cuzdan.id
        c                 = {'cuzdan_rel':self.cuzdan_id.id}
        return p.with_context(c).excel_isle()

class kart_change_sube(models.TransientModel):
    _name = 'info_kart.change_sube_wizard'
    
    @api.model
    def get_current_sube(self):
        return self.env['stock.production.lot'].browse( self._context.get('active_id')).sube_id.id
    
    @api.model
    def get_sube_domain(self):
        domain=[('parent_id','=',self.env['stock.production.lot'].browse( self._context.get('active_id')).sold_to.id)]
        #print domain
        
        return domain

    sube_ids = fields.Many2one('res.partner', string='Yeni Şube', domain=get_sube_domain)
    m_sube   = fields.Many2one('res.partner', string="Mevcut Şube",default=get_current_sube)
    sold_to  = fields.Many2one(related='m_sube.parent_id')
    lot_id   = fields.Many2one('stock.production.lot', string='Kart No', default=lambda self:self._context.get('active_id'))
    
    @api.multi
    def confirm(self):
        if self.sube_ids:
            
            self.lot_id.sube_id = self.sube_ids.id
        else:
            raise exceptions.ValidationError("Yeni Şubeyi Seçiniz.")
        msj = 'Şube Değiştirildi.'
        r = "/web/payresult?inner_message=1&success=%s&msj=%s"%(1,msj)
        return {
                'type': 'ir.actions.act_url',
                'url': r,
                'target': 'popup'
                }
    
    @api.multi
    def sube_olustur( self ):
        self.ensure_one()
        new_cr = self._context.copy()
        new_cr.update({'active_id':self.sold_to.id})
        return self.sold_to.with_context(new_cr).bir_sube_ekle_action()

class kart_ops_wizard(models.TransientModel):
    _name = 'info_kart.kart_confirm_wizard'

    parayi_al = fields.Boolean("İşlemden Sonra Bakiyeyi Geri Alma Ekranlarını Aç")
    aciklama = fields.Text("İşlem Açıklaması")
    message = fields.Char(default=lambda self: self._context.get('message'))
    lot_id = fields.Many2one('stock.production.lot', default=lambda self: self._context.get('active_id'))

    @api.multi
    def confirm(self):
        self.ensure_one()
        new_c = self._context.copy()
        if self.parayi_al:
            new_c.update({'parayi_al': True})

        if self.aciklama:
            self.lot_id.son_islem_aciklama = self.aciklama
        self.lot_id.statu_degisim_tarihi = datetime.datetime.now()
        metod = getattr(self.lot_id.with_context(new_c), new_c.get('method_name'))
        return metod()

class getYukHarParams(models.TransientModel):
    _name='info_kart.yuk_har'
    
    def _get_default_st_date(self):
         now = datetime.datetime.now()
         m   = str(now.month)
         if len(m) == 1:
            m = '0' + m
         return str(now.year) + '-' + m + '-' + '01'

    def _get_default_sp_date(self):

         now = datetime.datetime.now()
         m   = now.month
         d   = calendar.monthrange(now.year,m)[1]
         m   = str(m)
         if len(m) == 1:
            m = '0' + m
         return str(now.year) + '-' + m + '-' + str(d)
    
    
    str_date            = fields.Date(string='Başlangıç Tarihi',default=_get_default_st_date)
    stp_date            = fields.Date(string='Bitiş Tarihi',default=_get_default_sp_date)
    partner_id          = fields.Many2one('res.partner', string='Kurumsal Müşteri',default=lambda self:self._context.get('active_id'))
    yuk_rapor           = fields.Binary()
    har_rapor           = fields.Binary()

    @api.multi
    def get_harcama_rapor(self):
        pan_ids = [str(x.seri_no_computed.replace('-','').replace(' ','')[6:]) for x in self.partner_id.kartlar]
        sonuc,rapor = Rapor(self,db_save=False,tip=1,str_date=self.str_date,stp_date = self.stp_date,pan_ids=pan_ids)
        if sonuc:
            header = ['Üye İş Yeri','Kart No','Banka','MCC','BKM ID','Tarih','Tutar']
            
            for r in rapor['item']:
                lines  = []
                for k,v in r.items():
                    cell = []
                    cell.append( k )
                    i = 0
                    for vi in v:
                        #print v
                        if i == 0:
                            cell.append( '979257' + str(vi['Pan']) )
                            cell.append(vi.get('Kart_Banka') or '')
                            cell.append(vi['MCC_Kodu'])
                            cell.append(vi['BKM_ID'])
                            cell.append( get_naive_from_aware(vi['Tarih']))
                            cell.append( float(vi['Tutar']))
                            lines.append( cell )
                        else:
                            cell = []
                            cell.append( ' ' )
                            cell.append( '979257' + str(vi['Pan']) )
                            cell.append(vi.get('Kart_Banka') or '')
                            cell.append(vi['MCC_Kodu'])
                            cell.append(vi['BKM_ID'])
                            cell.append( get_naive_from_aware(vi['Tarih']))
                            cell.append( float(vi['Tutar']))
                            lines.append( cell )
                        i+= 1

            fil = from_data_custom(header, lines,damali=True )
            excel_data = base64.b64encode (fil)
            self.har_rapor  = excel_data
            r = "/excel_yuk_har/%s/%s"%(self.id,'har')
            return {
                    'type': 'ir.actions.act_url',
                    'url': r,
                    'target': 'new'
                    }
        else:
            raise exceptions.ValidationError('Veri Alınamadı')

    @api.multi
    def get_yukleme_rapor(self):
        lines = []
        header = ['Şube', 'Kart No', 'Ad - Soyad', 'Cep Telefonu', 'Sicil No']
        max_odeme = 0
        for k in self.partner_id.kartlar:
            line = [self.partner_id.display_name, k.seri_no_computed or ' ', k.ad_soyad or ' ', k.cep_tel or ' ',
                    k.sicil_no or ' ']
            if len(k.odemeler) > 0:
                odemeler = self.env['info_kart.set_limit'].sudo().search([('lot', '=', k.id),
                                                                          ('create_date', '>=', self.str_date),
                                                                          ('create_date', '<=', self.stp_date),
                                                                          ('state', '=', 'confirm')], order='id asc')

                if len(odemeler) > max_odeme:
                    max_odeme = len(odemeler)

                line.extend([x.create_date.strftime('%d/%m/%Y, %H:%M:%S') + '- ' + str(x.tutar) + ' TL' for x in odemeler])
            lines.append(line)

        for i in range(1, max_odeme + 1):
            header.append(str(i) + '. Yükleme')

        excel_data = base64.b64encode(from_data_custom(header, lines))
        self.yuk_rapor = excel_data
        r = "/excel_yuk_har/%s/%s" % (self.id, 'yuk')

        return {
            'type': 'ir.actions.act_url',
            'url': r,
            'target': 'new'
        }

class cuzdan_uyari_view(models.TransientModel):
    _name = 'info_kart.cuzdan_uyari_view'
    uyari = fields.Text('')
    cuzdan_partner_rel = fields.Many2one('info_kart.cuzdan_partner_rel',default=lambda self:self._context.get('active_id'))
    @api.multi
    def onsave(self):
        return self.cuzdan_partner_rel.set_limit()