# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions
from odoo.addons.info_bayi.models.webserviceopt import bakiye, tl_transfer_toplu
from openerp.modules.registry import Registry
import io
from io import StringIO, BytesIO
import base64, xlrd
from datetime import datetime
from threading import Thread


class BakiyeThread(Thread):
    def __init__(self, obj):
        Thread.__init__(self)
        self.obj = obj

    def run(self):
        with api.Environment.manage():
            with Registry(self.obj.env.cr.dbname).cursor() as new_cr:
                new_env = api.Environment(new_cr, 1, {})
                self.obj = self.obj.with_env(new_env)
                try:
                    for k in self.obj.kartlar:
                        bakiye(k)
                except:
                    pass
                self.obj.toplu_guncelle = False
                new_env.cr.commit()


MERKEZ = 1
import heapq

path = '/var/lib/odoo/addons/10.0/'


def merge_no_duplicates(*iterables):
    last = object()

    for val in heapq.merge(*iterables):
        if val != last:
            last = val
            yield val


class cuzdan_partner_rel(models.Model):
    _name = 'info_kart.cuzdan_partner_rel'

    partner_id = fields.Many2one('res.partner', 'Kurumsal Müşteri', domain=[('state', '=', 'confirmed'),
                                                                            ('customer', '=', True),
                                                                            ('is_company', '=', True),
                                                                            ('parent_id', '=', False),
                                                                            ('kart_musterisi', '=', True)
                                                                            ], required=True)
    kredili_musteri = fields.Boolean(string='Kredili Cüzdan')
    cuzdan = fields.Many2one('info_kart.cuzdan', string='Cüzdan', required=True, domain=[('serbest', '=', False)])
    kredi = fields.Float(string='Kredi Limiti')
    bakiye = fields.Float(string='Cüzdan Bakiyesi')
    total_bakiye = fields.Float(string='Kullanılabilir Bakiye', compute='_get_total')
    bakiye_c = fields.Many2one('res.currency',
                               default=lambda self: self.env['res.currency'].search([('name', '=', 'TRY')], limit=1))

    excel_file_name = fields.Char()
    excel_dosya = fields.Binary('Tutar Excel')
    excel_time = fields.Datetime()
    excel_time_gizle = fields.Boolean(compute='_get_time_exceld', default=True)
    text_dosya = fields.Binary()
    iade_text = fields.Binary()

    kul_kredi = fields.Float(compute="_get_money_credit")
    rel_para = fields.Float(compute="_get_money_credit")
    tot_kredi = fields.Float(compute="_get_money_credit")
    tot_bakiye = fields.Float(compute="_get_money_credit")
    fark = fields.Float(compute="_get_money_credit")

    yukleme_gizle = fields.Boolean(compute="_get_yukleme_gizle", default=True)

    _sql_constraints = [('partner_cuzdan_unique', 'unique (partner_id,cuzdan)',
                         'Bir cüzdan sadece bir kez eklenebilir. Listeyi kontrol ediniz !!!')]

    @api.multi
    def _get_yukleme_gizle(self):
        y = self.env.user.has_group("info_kart.group_kart_bakiye_verir")
        y2 = self.env.user.has_group("info_kart.group_kart_bakiye_verir_kendine")
        p = self.env.user.company_id.partner_id.id
        for s in self:
            s.yukleme_gizle = True
            if y:
                s.yukleme_gizle = False
            else:
                if s.partner_id.id == p and y2:
                    s.yukleme_gizle = False

    @api.multi
    @api.depends('bakiye', 'kredi')
    def _get_money_credit(self):
        sq = 'select sum(bakiye) as s from info_kart_cuzdan_partner_rel where partner_id != 1 and bakiye > 0';
        sq_2 = 'select sum(kredi) as s from info_kart_cuzdan_partner_rel where partner_id != 1 and kredi > 0'
        sq_3 = 'select sum(bakiye + kredi ) as s from info_kart_cuzdan_partner_rel where partner_id != 1'
        self.env.cr.execute(sq)
        fetched = self.env.cr.dictfetchall()
        self.env.cr.execute(sq_2)
        fetched2 = self.env.cr.dictfetchall()
        self.env.cr.execute(sq_3)
        fetched3 = self.env.cr.dictfetchall()

        total_bakiye = float(fetched3[0].get('s'))
        total_para = self.sudo().search([('partner_id', '=', 1), ('cuzdan', '=', 1)]).bakiye
        rel_para = float(fetched[0].get('s'))
        tot_kredi = float(fetched2[0].get('s'))
        kul_kredi = total_para - rel_para
        fark = total_para - total_bakiye
        for s in self:
            s.kul_kredi = kul_kredi
            s.rel_para = rel_para
            s.tot_kredi = tot_kredi
            s.tot_bakiye = total_bakiye
            s.fark = fark

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            cuzdan_name = ''
            if record.cuzdan.name:
                cuzdan_name = record.cuzdan.name
            name = cuzdan_name + u' - Kullanılabilir Bakiye : ' + str(record.total_bakiye) + ' TL'
            res.append((record['id'], name))
        return res

    @api.multi
    def _get_time_exceld(self):
        for s in self:
            excel_time_gizle = True
            if s.excel_time:
                diff = datetime.utcnow() - s.excel_time

                if diff.total_seconds() <= 24 * 60 * 60 and s.iade_text:
                    excel_time_gizle = False

            s.excel_time_gizle = excel_time_gizle

    @api.multi
    def _get_total(self):
        for s in self:
            s.total_bakiye = s.bakiye + s.kredi

    @api.multi
    def write(self, v):
        super(cuzdan_partner_rel, self).write(v)
        if self.kredili_musteri:
            self.partner_id.kredili_musteri = True
        else:
            up = False
            for c in self.partner_id.cuzdanlar:
                if c.kredili_musteri:
                    up = True
                    break
            if not up:
                self.partner_id.kredili_musteri = False

    @api.model
    def create(self, v):
        res = super(cuzdan_partner_rel, self).create(v)
        if res.kredili_musteri:
            res.partner_id.kredili_musteri = True

        return res

    @api.multi
    def set_limit_prepare(self):
        self.ensure_one()
        if self.cuzdan.uyari:
            return {
                'type': 'ir.actions.act_window',
                'name': 'Kullanılabilir Bakiye Yükle',
                'view_mode': 'form',
                'view_type': 'form',
                'res_model': 'info_kart.cuzdan_uyari_view',
                'src_model': 'res.partner',
                'context': {'active_id': self.id,
                            'default_uyari': self.cuzdan.uyari, },
                'target': "new"
            }
        else:
            return self.set_limit()

    @api.multi
    def set_limit(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'name': 'Kullanılabilir Bakiye Yükle',
            'view_mode': 'form',
            'view_type': 'form',
            'res_model': 'info_kart.set_limit',
            'src_model': 'res.partner',
            'context': {'partner': True, 'edit': True,
                        'active_id': self.partner_id.id,
                        'active_model': 'res.partner',
                        'default_kurumsal_cuzdan': self.cuzdan.id,
                        'default_cuzdan_rel_id': self.id,
                        },
            'flags': {'terminate_parent': True},
            'target': "new"
        }

    @api.multi
    def excel_isle(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'name': 'Excel İle Kartlara Toplu Bakiye Aktarımı',
            'view_mode': 'form',
            'view_type': 'form',
            'res_model': 'info_kart.upload_bakiye_excel',
            'src_model': 'info_kart.cuzdan_partner_rel',
            'target': "new"
        }

    @api.multi
    def excel_isle_geri_al(self):
        self.ensure_one()
        if self.text_dosya:
            self.partner_id.text_dosya = self.text_dosya
            c = {'cuzdan_rel': self.id}
            return self.partner_id.with_context(c).parse_excel_create_txt(tip=2)
        else:
            raise exceptions.ValidationError('Geri Alınacak Yükleme dosyası Bulunamadı.')

    @api.onchange('cuzdan')
    def cuzdan_onchange(self):
        cuzdan_ids = []
        for c in self._context.get('ekli_cuzdanlar'):
            print(c)
            if c[1] and not str(c[1]).startswith('virtual'):
                cuzdan_ids.append(self.env['info_kart.cuzdan_partner_rel'].sudo().browse(c[1]).cuzdan.id)
            else:
                cuzdan_ids.append(c[2]['cuzdan'])

        final_ids = list(set(cuzdan_ids))

        return {'domain': {'cuzdan': [('id', 'not in', final_ids), ('serbest', '=', False)]}}

    @api.multi
    def unlink(self):
        for s in self:
            if s.bakiye:
                raise exceptions.ValidationError('İçerisinde Para veya Borç bulunan bir Cüzdan Silinemez')
            else:
                return super(cuzdan_partner_rel, s).unlink()


class res_partner(models.Model):
    _inherit = 'res.partner'

    def get_cuzdan_selection(self):
        r = []
        for c in self.cuzdanlar:
            r.append((c.cuzdan.id, c.cuzdan.name))

        return r

    kart_musterisi = fields.Boolean('Kart Müşterisi')
    bireysel_musteri = fields.Boolean('Bireysel Müşteri')

    # file_fields = ['sicil_gazetesi', 'esnaf_sanatkar', 'ikamet', 'adli', 'imza', 'faaliyet', 'nufus', 'vergi_levhasi',
    #                'sozlesme']

    kartlar = fields.One2many('stock.production.lot', 'sold_to', string='Kartlar', domain=[('name', 'ilike', '9792 57'),
                                                                                           ('lot_state', 'not in', (
                                                                                           'draft2', 'scrapped',
                                                                                           'isten_ayrildi', 'sorunlu',
                                                                                           'izinli'))])

    kartlar2 = fields.One2many('stock.production.lot', 'sold_to', string='Kayıp Kartlar',
                               domain=[('name', 'ilike', '9792 57'),
                                       ('lot_state', '=', 'scrapped')])
    ayrilan_kartlar = fields.One2many('stock.production.lot', 'sold_to', string='İşten Ayrılan',
                                      domain=[('name', 'ilike', '9792 57'),
                                              ('lot_state', '=', 'isten_ayrildi')])

    kartlar4 = fields.One2many('stock.production.lot', 'sold_to', string='İzinli olanlar',
                               domain=[('name', 'ilike', '9792 57'),
                                       ('lot_state', '=', 'izinli')])

    kartlar5 = fields.One2many('stock.production.lot', 'sold_to', string='Yedek Kartlar',
                               domain=[('name', 'ilike', '9792 57'),
                                       ('lot_state', '=', 'draft2')])

    kartlar6 = fields.One2many('stock.production.lot', 'sold_to', string='Sorunlu Kartlar',
                               domain=[('name', 'ilike', '9792 57'),
                                       ('lot_state', '=', 'sorunlu')])

    available_limit = fields.Float(string="Toplam Limit")
    kart_adeti = fields.Float(string="Kart Adeti", compute='_get_kart_adeti', store=True)
    tutar_c = fields.Many2one('res.currency',
                              default=lambda self: self.env['res.currency'].search([('name', '=', 'TRY')], limit=1))
    excel_file_name = fields.Char()
    excel_dosya = fields.Binary('Tutar Excel')
    excel_time = fields.Datetime()
    excel_time_gizle = fields.Boolean(compute='_get_time_exceld', default=True)
    text_dosya = fields.Binary()
    never_sended = fields.Boolean(default=False)
    bakiye_yukleme = fields.One2many('info_kart.set_limit', 'partner')
    iade_text = fields.Binary()
    cuzdan = fields.Many2one('info_kart.cuzdan', string='Excel Yükleme Yapılacak Cüzdan',
                             domain=[('serbest', '=', False)])
    cuzdanlar = fields.One2many('info_kart.cuzdan_partner_rel', 'partner_id', string='Kurumsal Cüzdanlar ve Bakiyeler')
    ava_cuzdan = fields.Selection(selection=get_cuzdan_selection, string='Yükleme Yapılacak Cüzdan')
    pers_count = fields.Integer(string='Kart Kullanacak Personel Sayısı')
    money = fields.Float(string="Toplam Aylık yüklenecek Tutar")
    caney = fields.Float(string="Kart Başına Aylık Yüklenecek Tutar")
    onceki_kart = fields.Selection(selection=(('var', 'Var'), ('yok', 'Yok')), string='Firmanın Kullandığı Yemek Kartı')
    onceki_kart_tipi = fields.Selection(
        selection=(('multinet', 'Multinet'), ('sodekso', 'Sodexo'), ('ticket', 'Ticket'), ('setkart', 'SetCart'),),
        string='Kullandığı Yemek Kartı')

    kredili_musteri = fields.Boolean("Kredili Müşteri")
    curr = fields.Many2one('res.currency',
                           default=lambda self: self.env['res.currency'].search([('name', '=', 'TRY')], limit=1))

    bireysel_kartlar = fields.One2many('stock.production.lot', 'bireysel_partner_id')
    dogum_tar = fields.Date("Doğum Tarihi")
    # child_ids_sec           = fields.Selection(selection=_get_child_ids, stirng='Şube Seç')

    toplu_guncelle = fields.Boolean(default=False)

    top_limit = fields.Float(compute="_get_top_limit")

    kargolar = fields.One2many('info_kart.kargo', 'partner_id')
    kargo_count = fields.Integer(compute='_get_cargo_count')

    @api.multi
    def _get_cargo_count(self):
        for k in self:
            k.kargo_count = len(k.kargolar)

    @api.multi
    def kargo_olustur(self):
        self.ensure_one()

        r_dict = {
            'type': 'ir.actions.act_window',
            'name': 'Kargo Talebi',
            'view_mode': 'tree,form',
            'view_type': 'form',
            'res_model': 'info_kart.kargo',
            'src_model': 'res.partner',
            'context': {'default_state': 'talep', 'default_partner_id': self.id, 'odoo_bug': True},
            'target': "self",
            'domain': [('partner_id', '=', self.id)],
           #'flags':{'action_buttons':True}


        }
        print (r_dict)
        return r_dict

    @api.multi
    def _get_top_limit(self):
        for s in self:
            s.top_limit = s.available_limit + s.credit_limit

    @api.multi
    def _get_time_exceld(self):
        for s in self:
            excel_time_gizle = True
            if s.excel_time:
                diff = datetime.utcnow() - datetime.strptime(s.excel_time, '%Y-%m-%d %H:%M:%S')
                if diff.total_seconds() <= 24 * 60 * 60 and s.iade_text:
                    excel_time_gizle = False

            s.excel_time_gizle = excel_time_gizle

    @api.multi
    def excel_isle(self):
        self.ensure_one()
        if self.excel_dosya and self.cuzdan:
            return self.parse_excel_create_txt()
        else:
            raise exceptions.ValidationError("Excel Dosyanızı ve Cüzdan Seçiminizi Kontrol Ediniz.")

    @api.multi
    def excel_isle_geri_al(self):
        self.ensure_one()
        if self.text_dosya:
            self.parse_excel_create_txt(tip=2)

    @api.one
    @api.constrains('pers_count')
    def _check_pers_count(self):
        # print self.child_ids
        if self.pers_count and self.pers_count <= 0:
            raise exceptions.ValidationError("Kart Kullanacak Kişi Sayısı 0'dan büyük olmalıdır.")

    @api.one
    @api.constrains('excel_file_name')
    def _check_pexcel_file_name(self):
        # print self.child_ids
        if self.excel_file_name:
            suffix = self.excel_file_name.split('.')
            suffix = suffix[-1]
            if suffix.upper() == 'XLSX' or suffix.upper() == 'XLS':
                pass
            else:
                raise exceptions.ValidationError(
                    'Sadece Excel Dosyası Yükleyebilirsiniz. Diğer Dosyalar Formatlanmayacaktır.')

    @api.one
    @api.constrains('money')
    def _check_money(self):
        # print self.child_ids
        if self.money and self.money < 250:
            raise exceptions.ValidationError("Toplam Aylık yüklenecek Tutar minimum 250 TL Olmalıdır.")

    @api.one
    @api.constrains('caney')
    def _check_caney(self):
        # print self.child_ids
        if self.caney and self.caney < 50:
            raise exceptions.ValidationError("Kart Başına Aylık Yüklenecek Tutar minimum 50 TL Olmalıdır.")

    @api.multi
    def get_all_bakiye(self):
        self.ensure_one()
        self.toplu_guncelle = True
        t = BakiyeThread(self)
        t.start()

    @api.multi
    def get_child_opts(self):

        d = {}
        try:
            for c in self.child_ids:
                d[c.id] = c.display_name
        except:
            pass
        return d

    @api.multi
    def sozlesme_yazdir(self):
        soz = super(res_partner, self).sozlesme_yazdir()
        if self.kart_musterisi:
            return {
                'type': 'ir.actions.act_url',
                'url': '/report/pdf/info_kart.report_kurumsal_sozlesme/' + str(self.id),
                'target': 'new'
            }
        else:
            return soz

    @api.multi
    def get_limit(self):

        self.ensure_one()
        if self.id == 1:
            bakiye(self, 'partner', )
        else:
            return True

    @api.multi
    def set_limit(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'name': 'Kullanılabilir Bakiye Yükle',
            'view_mode': 'form',
            'view_type': 'form',
            'res_model': 'info_kart.set_limit',
            'src_model': 'res.partner',
            'context': {'partner': True, 'edit': True},
            'target': "new"
        }

    @api.multi
    def get_har_yuk(self):
        self.ensure_one()

        return {
            'type': 'ir.actions.act_window',
            'name': 'Harcama Yükleme Raporu',
            'view_mode': 'form',
            'view_type': 'form',
            'res_model': 'info_kart.yuk_har',
            'src_model': 'res.partner',
            'context':self._context,
            'target': "new"
        }

    @api.multi
    def kurumsal_extre(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'name': 'Kurumsal Ekstre',
            'view_mode': 'form',
            'view_type': 'form',
            'res_model': 'info_kart.yukleme_harcama_ekstre',
            'src_model': 'res.partner',
            'context': {'default_partner_id': self.id},
            'target': "new"
        }

    @api.depends('kartlar')
    def _get_kart_adeti(self):
        for s in self:
            s.kart_adeti = len(s.kartlar)

    @api.multi
    def kart_ata(self):
        self.ensure_one()
        if self.bireysel_musteri:
            context = {'bireysel': True}
        elif self.kart_musterisi or self.dealer:
            context = {'kurumsal': True}
        else:
            context = {}
        return {
            'type': 'ir.actions.act_window',
            'name': 'Kart Ata',
            'view_mode': 'form',
            'view_type': 'form',
            'source_model': 'res.partner',
            'res_model': 'info_kart.kart_ata_wizard',
            'context': context,
            'target': "new"
        }

    @api.multi
    def write(self, vals):
        if "parent_id" in vals and not vals.get("parent_id"):
            res = True
        else:
            res = super(res_partner, self).write(vals)
        for s in self:

            if vals.get('credit_limit'):
                obj = self.browse([1])
                bakiye(obj, 'partner')
                if vals.get('credit_limit') > obj.available_limit:
                    raise exceptions.ValidationError('Yemekmatik A.Ş. Kurumsal Bakiyesi Bu Krediyi Vermeye Müsait Değil,\
                                                      Lütfen Önce Yemekmatik A.Ş. Kurumsal Bakiyesi İçin Para Yatırınız.\n\
                                                      Yemekmatik A.Ş. Güncel Bakiye : %s TL.' % round(
                        obj.available_limit, 2))
        return res

    def get_kurumsal_cuzdan(self, partner_id=False, cuzdan_id=False):

        cuzdan_obj = self.cuzdan_rel_id
        if not cuzdan_obj:
            p = self.partner.id or partner_id
            c = self.kurumsal_cuzdan.id or cuzdan_id
            cuzdan_obj = self.env['info_kart.cuzdan_partner_rel'].sudo().search(
                [('partner_id', '=', p), ('cuzdan', '=', c)])
        if not cuzdan_obj:
            raise exceptions.ValidationError(
                "Yükleme Yapılan Cüzdan Bulunamadı. Lütfen Sistem Yöneticinizle İletişime Geçiniz.")
        return cuzdan_obj

    @api.multi
    def get_yukleme_excel(self):
        self.ensure_one()
        f_list = [["seri_no_computed", "Kart No"], ["empty", "Yuklenecek Tutar"], ["ad_soyad", "Kart Sahibi"],
                  ["sicil_no", "Sicil No"], ["cep_tel", "Cep Tel."]]
        r = "/excel_c/res.partner/%s/kartlar/%s" % (self.id, f_list)

        return {
            'type': 'ir.actions.act_url',
            'url': r,
            'target': 'new'
        }

    @api.multi
    def parse_excel_create_txt(self, c=None, tip=1):
        self.ensure_one()
        if tip == 1:
            if self._context.get('cuzdan_rel'):
                cuzdan_rel = self.env['info_kart.cuzdan_partner_rel'].sudo().browse(self._context.get('cuzdan_rel'))
                excel_file = base64.b64decode(self.excel_dosya)
                book = xlrd.open_workbook(file_contents=excel_file, encoding_override='utf-8')
                deep_sheet = book.sheets()[0]
                deep_rows = deep_sheet.get_rows()
                i = 0
                kart_nums = list(map(lambda x: str(x.seri_no_computed.replace(" ", '')), self.kartlar))
                nf = BytesIO()
                total = 0
                success = 0
                for dr in deep_rows:
                    k_no = str(dr[0].value).replace(" ", '').replace('.0', '')
                    if len(k_no) == 10:
                        k_no = '979257' + k_no
                    elif len(k_no) == 8:
                        k_no = '97925700' + k_no
                    if k_no in kart_nums:
                        nf.write((k_no + '|' + str(dr[1].value) + '|' + self.cuzdan.kod + '\n').encode())
                        total += float(dr[1].value)

                if cuzdan_rel.total_bakiye > 0 and cuzdan_rel.total_bakiye >= total:
                    if nf.tell() == 0:
                        raise exceptions.ValidationError(
                            "Exceldeki  Kart Numaralarının Okunmasında Bir Sorun Oluştu, Kart Noları Doğru Yazdığınızdan ve Yazdığınız Tüm Kart No Ların Size Ait Olduğundan Emin Olunuz.")

                    print(nf.getvalue())
                    b64text = base64.b64encode(nf.getvalue())
                    cuzdan_rel.text_dosya = b64text
                    sonuc, sonuc_str = tl_transfer_toplu(self, b64text, tip)
                    if int(sonuc) == 1:
                        prev_limit = cuzdan_rel.bakiye
                        next_limit = prev_limit - total
                        cuzdan_rel.bakiye = next_limit

                        self.excel_dosya = False
                        self.cuzdan = False
                        cuzdan_rel.excel_time = datetime.utcnow()
                else:
                    raise exceptions.ValidationError(
                        "Exceldeki Toplam tutar Cüzdan Kullanılabilir Limitinizin Üzerindedir. Lütfen Limit Artırımı Yapınız veya Exceli Düzenleyiniz.")
            else:
                raise exceptions.ValidationError("Bakiyenin Alınacağı Kurumsal Cüzdan Bulunamadı")
        elif tip == 2:
            if self._context.get('cuzdan_rel'):
                cuzdan_rel = self.env['info_kart.cuzdan_partner_rel'].sudo().browse(self._context.get('cuzdan_rel'))
                b64text = cuzdan_rel.iade_text
                cuzdan_rel.iade_text = False

                sonuc, sonuc_str = tl_transfer_toplu(self, b64text, tip)
            else:
                raise exceptions.ValidationError("Bakiyenin İade Edileceği Kurumsal Cüzdan Bulunamadı")
        if sonuc_str:
            label = 'Yükleme'

            if tip == 2: label = "Geri Alma"
            if int(sonuc) == 1:
                msj = u"""Toplu %s Talimatınız Oluşturuldu. Bakiyelerin Kartlara Yansıması 4-5 dakika sürebilir.""" % label
                success = 1

            else:
                msj = u"""Toplu %s Talimatınız Sırasında Bir Hata Oluştu.
                                    Hata Detayı : %s""" % (label, sonuc_str)
        else:
            msj = "Servis Hatası -- İstemci Oluşturulamadı."

        r = "/web/payresult?inner_message=1&success=%s&msj=%s" % (success, msj)

        return {
            'type': 'ir.actions.act_url',
            'url': r,
            'target': 'popup'
        }

    @api.model
    def create(self, vals):
        res = super(res_partner, self).create(vals)
        # infoteks iliskiyi burada bas....
        if vals.get('kart_musterisi'):
            if self.env.user.company_id.id != MERKEZ:
                if not res.main_dealer:
                    res.main_dealer = self.env.user.sudo().company_id.id
            # res.bayi_kodu = self.get_bayi_kodu(res.city_combo.plaka, res.bolge_sira_no)
            if res.parent_id:
                res.state = 'confirmed'
            else:
                res.state = 'waiting_confirm'
            res.write({'iade_text':"1"})# state belirlemek için yazıldı
        return res

    @api.model
    def get_kart_action(self):
        print(1, datetime.now())
        action = {
            'name': 'Kurumsal Kart Müşterileri',
            'type': 'ir.actions.act_window',
            'res_model': 'res.partner',
            'view_type': 'form',
            'view_mode': 'kanban,tree,form',
            'views': [[self.env.ref('info_extensions.new_partner_kanban_view').id, 'kanban'],
                      [self.env.ref('base.view_partner_tree').id, 'tree'],
                      [self.env.ref('info_kart.kart_res_partner_form_view').id, 'form']
                      ],
            'filter': True,
            'context': "{'disable_create':1}",

        }

        p_ids = "select distinct(sold_to) from stock_production_lot";
        self.env.cr.execute(p_ids)
        p_ids = [x[0] for x in self.env.cr.fetchall()]

        '''
        partners = self.search([('state','=','confirmed'),
                                  ('customer','=',True),
                                  ('is_company','=',True),
                                  ('parent_id','=',False),
                                  ('kart_musterisi','=',True)
                                  ])
        
        print 2,datetime.now()
        p_ids = []
        for p in partners:
            if len(p.kartlar) != 0 and p.id != 1:
                 p_ids.append( p.id )
        if self.env.user.company_id.id == 1:
            p_ids.append( 1 )
        p_ids.append( self.env.user.company_id.partner_id.id)
        '''

        domain = [('id', 'in', p_ids),
                  ('customer', '=', True),
                  ('is_company', '=', True),
                  ('parent_id', '=', False),
                  ('kart_musterisi', '=', True)]
        print(3, datetime.now())
        print(domain)

        action['domain'] = domain
        return action
