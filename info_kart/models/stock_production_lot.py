# -*- coding: utf-8 -*-
from dateutil.relativedelta import relativedelta

from odoo import models, fields, api,exceptions
from .util import cipher, EncodeAES, DecodeAES
import xml.etree.ElementTree as etree
from difflib import SequenceMatcher
from odoo.addons.info_bayi.models.webserviceopt import bakiye, Kisisellestirme
class kart_tipi(models.Model):
    _name = 'info_kart.kart_tipi'
    
    name = fields.Char('Kart Tip Adı', required= True)

class stock_production_lot_cuzdan_rel(models.Model):
    _inherit = 'info_kart.lot_cuzdan_rel'
    
class stock_production_lots(models.Model):
    
    _inherit = 'stock.production.lot'
    
    @api.multi
    def name_get(self):
        res = []
        for record in self:
            _name = record.seri_no_computed
            if not _name:
                _name = record.name
            res.append((record['id'],_name ))
        return res
    
    name = fields.Char(
        'Kart No', default=False,
        required=True, help="Kart Numarası")
    
    sube_id   = fields.Many2one('res.partner')
    due_date  = fields.Date('Son Kullanma Tarihi',required=True)
    ad_soyad  = fields.Char('Kartı Kullanan Kişinin Ad-Soyad')
    tckn      = fields.Char('Tc Kimlik')
    cep_tel   = fields.Char('Cep Telefonu')
    email     = fields.Char('E-posta')
    adres     = fields.Char('Adres')
    il        = fields.Many2one('info_extensions.iller')
    ilce      = fields.Many2one('info_extensions.ilceler')
    dogum_tar = fields.Date('Doğum Tarihi')
    sicil_no  = fields.Char('Sicil No')
    lot_state = fields.Selection(selection = [  ('draft','İsimsiz (Stokta)'),
                                                ('draft2','İsimsiz (Yedek)'),
                                                ('sold','İsimsiz (Müşteri Kartı)'),
                                                ('sold2','Aktif'),
                                                ('sold3','Bireysel'),
                                                ('scrapped','Kayıp Kart'),
                                                ('sorunlu','Stoktan Çıkarıldı'),
                                                ('izinli','İzinli'),
                                                ('isten_ayrildi','Kart Sahibi İşten Ayrıldı')],string='lot_state',default='draft')
    prev_state = fields.Char()
    
    kart_type  = fields.Many2one('info_kart.kart_tipi',string='Kart Türü')
    seri_no    = fields.Char(string='Açık Kart No',index=True)
    seri_no_computed  = fields.Char(string='Açık Kart No', compute='_get_seri_no_compute',search='_get_seri_no_search' )
    available_limit   = fields.Float(string="Kullanılabilir Limit")
    available_limit_b = fields.Float(string="Yemek Bloke Tutar") 
    gift_limit        = fields.Float(string="Gift Limiti")
    gift_limit_b      = fields.Float(string="Gift Bloke Tutar")
    serbest_limit     = fields.Float(string="Serbest Limiti")
    serbest_limit_b   = fields.Float(string="Serbest Bloke Tutar")
    guid     =  fields.Char()
    
    gelen_phone_cals       = fields.One2many('info_bayi.phone_calls','kart_id',string='Gelen Aramalar',domain=[('type_','=','in')] )
    giden_phone_cals       = fields.One2many('info_bayi.phone_calls','kart_id',string='Giden Aramalar',domain=[('type_','=','out')] )
    bireysel_partner_id    = fields.Many2one('res.partner')
    ws_last_message        = fields.Char("Son Merkez Mesajı")
    son_bakiye_yukleme_mesaj = fields.Char("Son Merkez Mesajı")
    
    odemeler                 = fields.One2many('info_kart.set_limit','lot')
    kapali_gizle             = fields.Boolean( compute = '_get_kapali_goster')
    
    i_tarihi                 = fields.Datetime(related='bireysel_partner_id.create_date',string='İlişkinedirilme Tarihi')
    
    yukleme_gizle            = fields.Boolean(compute="_get_yukleme_gizle", default=True)
    
    iliski_status            = fields.Selection(selection=[ ('yok','Yok'),
                                                            ('sold2','İlişkili'),
                                                            ('sold3','Kişisel')],string="İlişki Durumu", default="yok")
    
    son_islem_aciklama       = fields.Text("Son İşlem Açıklama") 
    statu_degisim_tarihi     = fields.Datetime()
    
    cuzdanlar                = fields.One2many('info_kart.lot_cuzdan_rel','lot_id')
    bakiyeler                = fields.Html(compute='get_dynamic_bayike_by_kart')


    p_cuzdanlar             = fields.One2many(related='sold_to.cuzdanlar')
    p_image                 = fields.Binary(related='sold_to.image')
    p_firma_unvani          = fields.Char(related='sold_to.firma_unvani')
    p_name                  = fields.Char(related='sold_to.name')
    p_contacts              = fields.One2many(related='sold_to.contacts')
    p_contacts_visible      = fields.Integer(related='sold_to.contacts_visible')
    p_sermaye_sirketi       = fields.Boolean(related = 'sold_to.sermaye_sirketi')
    p_sahis_sirketi         = fields.Boolean(related='sold_to.sahis_sirketi')
    p_ortaklik              = fields.Boolean(related='sold_to.ortaklik')

    p_street                = fields.Char(related='sold_to.street')
    p_street2               = fields.Char(related='sold_to.street2')

    p_city_combo            = fields.Many2one(related='sold_to.city_combo')
    p_ilce                  = fields.Many2one(related='sold_to.ilce')
    p_tax_number            = fields.Char(related='sold_to.taxNumber')
    p_tckn                  = fields.Char(related='sold_to.tckn')


    @api.multi
    def get_k_limit(self):
        self.ensure_one()
        self.get_limit()
        return self.sold_to.get_limit()

    @api.multi
    def kurumsal_extre(self):
        self.ensure_one()
        return self.sold_to.kurumsal_extre()

    @api.multi
    def get_yukleme_excel(self):
        self.ensure_one()
        return self.sold_to.get_yukleme_excel()

    @api.multi
    def get_har_yuk(self):
        self.ensure_one()
        context_ = self._context.copy()
        context_['active_model'] = 'res.partner'
        context_['active_id'] = self.sold_to.id
        return self.sold_to.with_context( context_ ).get_har_yuk()

    @api.multi
    def get_dynamic_bayike_by_kart(self):
        
        for s in self:
            html = '''<div class="divTable">
                        <div class="divTableBody">
                            <div class="divTableRow">
                                <div class="divTableHead">Cüzdan</div>
                                <div class="divTableHead">Bakiye</div>
                                <div class="divTableHead">Bloke</div>
                            </div>'''
            for c in s.cuzdanlar:
                if c.cuzdan.kod != '1000':
                    html += '<div class="divTableRow">'
                    html += '<div class="divTableCell">' + c.cuzdan.name.split(' ')[0].split('-')[0].split('(')[0] + '</div>'
                    html += '<div class="divTableCell">' + str(c.bakiye)  + ' TL</div>'
                    html += '<div class="divTableCell">' + str(c.bakiye_b)  + ' TL</div>'
                    html += '</div>'
            html += '</div></div>'
            s.bakiyeler = html
            
    @api.multi
    def get_other_limits(self):
        self.ensure_one()
        bakiye( self )
        return {
            'type': 'ir.actions.act_window', 
            'name': 'Diğer Bakiyeler',
            'view_mode': 'tree',
            'view_type':'form',
            'res_model': 'info_kart.lot_cuzdan_rel',
            'target':"new",
            'domain':[('lot_id','=',self.id)]
        }    
    
    @api.multi
    def get_process(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window', 
            'name': 'Onayla ve Devam Et',
            'view_mode': 'form',
            'view_type':'form',
            'res_model': 'info_kart.kart_confirm_wizard',
            'context':self._context,
            'target':"new"
        }
    @api.multi
    def sube_degistir(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window', 
            'name': 'Şube Değiştir',
            'view_mode': 'form',
            'view_type':'form',
            'res_model': 'info_kart.change_sube_wizard',
            'context':self._context,
            'target':"new"
        }    

    @api.multi
    def _get_yukleme_gizle(self):
        y = self.env.user.has_group("info_kart.group_kart_bakiye_verir")
        y2 = self.env.user.has_group("info_kart.group_kart_bakiye_verir_kendine")
        p = self.env.user.company_id.partner_id.id
        for s in self:
            s.yukleme_gizle = True
            if y:
                s.yukleme_gizle = False
            else: 
                if s.sold_to.id == p and y2:
                    s.yukleme_gizle = False
                
    
    @api.multi
    def _get_kapali_goster(self ):
        res_user = self.env.user
        for s in self:
            if res_user.has_group('info_kart.group_kart_stok_editor') or res_user.has_group('info_kart.group_kart_stok_reader_plus'):
                s.kapali_gizle = True
            else:
                s.kapali_gizle = False
    
    @api.multi
    def limiti_geri_al(self):
        self.ensure_one()
        
        c = self._context.copy()
        c.update({'active_id':self.id})
        
        return {
            'type': 'ir.actions.act_window', 
            'name': 'Limiti Geri Al',
            'view_mode': 'form',
            'view_type':'form',
            'res_model': 'info_kart.get_limit',
            'context':c,
            'target':"new"
       }
        
    def _get_seri_no_search(self,operator,value):
        if operator == 'like':
            operator = 'ilike'
        if len( value ) != 10:
            raise exceptions.ValidationError("Lütfen Kartın Son 10 Hanesini Giriniz.")
        name = '979257' + value
        name = [str(s) for s in name.split() if s.isdigit()]
        name = name[0]
        seri_no = name[0:4] + ' ' + name[4:8] + ' ' + name[8:12] + ' '  + name[12:16]
        enc     = EncodeAES( cipher, seri_no ) 
        
        return [('seri_no',operator,enc)]
    
    @api.multi
    def get_limit(self):
        self.ensure_one()
        gb = bakiye(self,'kart')
        
    @api.multi
    def get_card_extre(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window', 
            'name': 'Ekstre',
            'view_mode': 'form',
            'view_type':'form',
            'res_model': 'info_kart.get_extre_wizard',
            'src_model':'stock.production.lot',
            'target':"new"
       }
    
    @api.multi
    def re_personal(self):
        self.ensure_one()
        kw = {
            'tckn':self.tckn.strip(),
            'ad':self.ad_soyad.strip().rsplit(' ',1)[0],
            'soyad':self.ad_soyad.strip().split(' ')[-1],
            'cep_tel':self.cep_tel.strip(),
            'email':self.email.strip(),
            'dogum_tar':self.dogum_tar
        }
        resp = Kisisellestirme(self, kw)
        if resp.Sonuc != 1:
            msj = 'İşlem Başarısız : %s - %s'%(resp.Sonuc,resp.Sonuc_Str)
            r = "/web/payresult?inner_message=1&success=%s&msj=%s"%(-1,msj)
            return {
                        'type': 'ir.actions.act_url',
                        'url': r,
                        'target': 'popup'
                    }
        msj = 'İşlem Başarılı : %s - %s'%(resp.Sonuc,resp.Sonuc_Str)
        r = "/web/payresult?inner_message=1&success=%s&msj=%s"%(1,msj)
        return {
                    'type': 'ir.actions.act_url',
                    'url': r,
                    'target': 'popup'
                }

    @api.multi
    def set_limit(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window', 
            'name': 'Karta Bakiye Yükle',
            'view_mode': 'form',
            'view_type':'form',
            'res_model': 'info_kart.set_limit',
            'src_model':'stock.production.lot',
            'context': {'kart':True,'edit':True}, 
            'target':"new"
       }
        
    @api.multi
    def gelen_aramalar(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window', 
            'name': 'Gelen Aramalar',
            'view_mode': 'tree,form',
            'view_type':'form',
            'res_model': 'info_bayi.phone_calls', 
            'context': {'default_type_':'in','default_kart_id':self.id}, 
            'flags': {'action_buttons': True},
            'domain':"[('type_','=','in'),('kart_id','=',%s)]"%self.id,
            'target':"new"
       }
    @api.multi
    def giden_aramalar(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window', 
            'name': 'Giden Aramalar',
            'view_mode': 'tree,form',
            'view_type':'form',
            'res_model': 'info_bayi.phone_calls', 
            'context': {'default_type_':'out','default_kart_id':self.id}, 
            'flags': {'action_buttons': True},
            'domain':"[('type_','=','out'),('kart_id','=',%s)]"%self.id,
            'target':"new"
       }
    _sql_constraints = [
        ('name_ref_uniq', 'unique (seri_no)', 'Kart Zaten Eklenmiş !'),
    ]
    
    @api.multi
    def _get_seri_no_compute(self):
        for s in self:
            if s.seri_no:
                s.seri_no_computed = DecodeAES(cipher, s.seri_no)
        
    @api.onchange('name')
    def stock_no_onchange(self):
        res = {}
        if self.name and self.name != '979257':
            if len( self.name) == 16:
                name = [str(s) for s in self.name.split() if s.isdigit()]
                name = name[0]
                self.name = name[0:4] + ' ' + name[4:6] + '** **** '  + name[12:16]
                self.seri_no = name[0:4] + ' ' + name[4:8] + ' ' + name[8:12] + ' '  + name[12:16]
                self.seri_no_computed = self.seri_no
            else:
                self.name = False
                res = {'warning': {
                    'title': 'Geçersiz Kart No',
                    'message': u'Lütfen Geçerli Bir Kart No Giriniz. '}
                }
        return res
        
    @api.model
    def default_get( self, fields_list):
        
        res         = super(stock_production_lots,self).default_get( fields_list)
        p_id        = self.env['product.product'].search([('kart','=',True)],order = 'id asc')
        kart_tipi   = self.env['info_kart.kart_tipi'].search([])
        
        res['name'] = '979257'
        
        if kart_tipi:
            res['kart_type'] = kart_tipi[0].id
        
        if p_id:
            res['product_id'] = p_id[0].id
        return res
    
    @api.multi
    def write(self,vals):
        
        if vals.get("sold_to"):
            vals['company_id'] = self.env['res.partner'].sudo().browse([ vals.get('sold_to')]).company_id.id
        
        if vals.get("lot_state") == 'sold2' or vals.get("lot_state") == 'sold3':
            vals.update({'iliski_status':vals.get("lot_state")})

        if "sold_to" in vals  and not vals.get("sold_to"):
            pass
        else:
            return super(stock_production_lots, self).write(vals)
    
    @api.model
    def create(self,vals):
        if vals.get('seri_no'):
            enc = EncodeAES(cipher, vals.get('seri_no'))
            vals['seri_no'] = enc
        
        return super(stock_production_lots,self).create( vals )
    @api.multi
    def yedege_al(self):
        self.ensure_one
        self.lot_state = 'draft2'

    @api.multi
    def kullanima_al(self):
        self.ensure_one
        self.lot_state = 'sold'
    
    @api.multi
    def kayba_al(self):
        self.ensure_one
        self.lot_state = 'scrapped'
        return self.sudo().limiti_geri_al()
    
    @api.multi
    def soruna_al(self):
        self.ensure_one
        self.lot_state = 'sorunlu'
        if self._context.get('parayi_al'):
            return self.sudo().limiti_geri_al()
    
    @api.multi
    def isten_ayrildi(self):
        self.ensure_one
        self.lot_state = 'isten_ayrildi'
        if self._context.get('parayi_al'):
            return self.sudo().limiti_geri_al()
    @api.multi
    def izne_ayrildi(self):
        self.ensure_one
        self.lot_state = 'izinli'
        if self._context.get('parayi_al'):
            return self.sudo().limiti_geri_al()

        