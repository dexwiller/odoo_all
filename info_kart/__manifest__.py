# -*- coding: utf-8 -*-
{
    'name': "Kurumsal&Bireysel Müşteriler",

    'summary': """
        Kurumsal&Bireysel Müşteri Modülü""",

    'description': """
        İnfoteks Kurumsal&Bireysel Müşterileri kullanımı için hazırlanmış bayi modülüdür.
    """,

    'author': "İnfoteks adına Deniz Yıldız",
    'website': "http://www.infoteks.com.tr",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','info_bayi','stock','account','info_check'],

    # always loaded
    'data': [
        'security/groups.xml',
        'security/ir.model.access.csv',
        'views/forms.xml',
        'views/actions.xml',
        'views/tree.xml',
        'views/menu.xml',
        'views/assets.xml',
        'views/bireysel_forms.xml',
        'views/bireysel_actions.xml',
        'views/bireysel_menu.xml',
        'views/search.xml',
        'views/report_sozlesme.xml',
        'views/website_templates.xml',
        #'views/robots.xml',
        'views/mail_sablon.xml',
        'views/sequence.xml',
        'views/stock_production_lot_forms.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    "qweb": ['views/templates.xml',],
    
    'installable': True,
    'application': True,
    'auto_install': False,
}