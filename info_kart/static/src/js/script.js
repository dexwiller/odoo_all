//-*- coding: utf-8 -*-
odoo.define('info_kart.kart_options', function (require) {
    "use strict";
    var Sidebar = require('web.Sidebar');
    var core = require('web.core');
    var model = require('web.Model');
    var FormView = require("web.FormView");
    var _t = core._t;
    var QWeb = core.qweb;
    var model_    = 'stock.production.lot';
    var ListView = require('web.ListView');
    
    FormView.include({
        load_record:function(record){
            var self = this;
            var val  = this._super.apply(this, arguments);
            
            console.log(record);
            console.log (this);
            $('#search_combobox').find('option').remove().end();
            $('#search_combobox').append('<option value="-1">Şube Seçiniz...</option>').val('-1');
            var m = new model( 'res.partner' );
                    m.call("get_child_opts", [record.id])
                        .then(function (result) {
                            $('#search_combobox').append('<option value="-1">Şube Seçiniz...</option>').val('-1');
                            for (var key in result) {
                                $('#search_combobox').append('<option value=' + key + '>' + result[key] + '</option>');
                            }
                        });
            
            return val;
        }
    }),
    /*
    var action_manager = require('web.ActionManager'); 
    action_manager.include({ 
      ir_actions_act_close_wizard_and_reload_view: function (action, options) { 
        if (!this.dialog) { 
          options.on_close(); 
        } 
        this.dialog_stop();
        console.log( this );
        this.inner_widget.active_view.controller.ViewManager.action_manager.webclient.do_reload(); 
        return $.when(); 
      }, 
    });*/
    Sidebar.include({
        redraw: function () {
            var self = this;
            this._super.apply(this, arguments);
            if (self.getParent() && self.getParent().ViewManager.active_view.type == 'list') {
                var parentNode = self.getParent();
               
                if (parentNode.model == 'stock.production.lot') {
                    self.$el.parent().prepend(QWeb.render('AddHeaderExcelButtons', {widget: self}));
                    self.$el.parent().find('.excel_upload').on('click', {view:self.getParent()},self.get_file_upload_action);
                }
                if (parentNode.model == 'info_kart.cuzdan') {
                    self.$el.parent().prepend(QWeb.render('AddHeaderCuzdanButtons', {widget: self}));
                    self.$el.parent().find('.cuzdan_guncelle').on('click', {view:self.getParent()},self.get_cuzdan_guncelle_action);
                }
            }
        },
        get_file_upload_action: function (event) {
            var a = {
                    type: 'ir.actions.act_window',
                    name:'Kart No Excel Yükleme',
                    res_model: 'info_kart.upload_kart_excel_wizard',
                    view_mode:"form",
                    view_type:"form",
                    views: [[false, 'form']],
                    target:"new",
                    };
            event.data.view.do_action( a );
            
        },
        get_cuzdan_guncelle_action: function (event) {
            var m = new model('info_kart.cuzdan');
            m.call("get_cuzdan_full",[],{}).then(function(result){
                
                var a = {
                        type: 'ir.actions.act_window',
                        name:'Cüzdanlar',
                        res_model: 'info_kart.cuzdan',
                        view_mode:"tree,form",
                        view_type:"form",
                        views: [[false, 'tree'],[false,'form']],
                        target:"self",
                        };
                event.data.view.do_action( a );
                
            });
        },
    });
    
    ListView.include({

        load_list:function(){
            var self = this;
            var val  = this._super.apply(this, arguments);
            var btnCol = $('#custom_kart_params_form table thead tr th:nth-child(5)').hasClass("btnCol");
            var table = $('#custom_kart_params_form table').hasClass("o_list_view");
            //console.log (1);
            if ( self.model == 'stock.production.lot'){
                    if(!btnCol && table){
                        //console.log("buraya mı girmiyore");
                        var headType = {
                            "primary" : "Kartı <br/>Kişiselleştir",
                            "yellow" : "Karta Limit <br/>Yükle",
                            "green" : "Kart<br/>Limiti",
                            "red" : "Kart<br/>Extresi",
                            "info" : "Kart<br/>İşlemleri",
                            "orange" :  "Bloke<br /> İşlemleri",
                            "pink" : "Yemek<br/>Kartları",
                            "purple" : "3.Parti<br/>Uygulamalar",
                            "teal" : "İnternet <br/> Geçmişi",
                            "indigo" : "Giden<br/>Aramalar",
                            "cyan" : "Cihaz<br/>Geçmiş",
                            "sairs" : "Tüm<br/>Cüzdanlar",
                            "hotpink":"Servis<br/>Özellikleri",
                            "hotred":"Gelen<br/>Aramalar",
                            "niceblue":"Şube<br/>Transfer",
                            "lightred":"Değişen <br/> Parça Sil",
                            "lightgreen":"Giden <br/>Aramalar",
                            "lightblue":"Gelen <br/>Aramalar",
                            "lightpurple":"İşlemi <br/>Uygula",
                            "videoizle":"İşlem <br/>Videosu",
                            "docizle":"Doküman",
                        };
        
                        var removeList = [];
                        
                        var columnCount = 1;
                        //console.log( $('#custom_kart_params_form table tbody tr:nth-child(1) td'));
                        $('#custom_kart_params_form table tbody tr:nth-child(1) td').each(function () {
                            //console.log("buraya mı girmiyore 2");
                            if($(this).html().length == 0 && columnCount == 2){
                                $('#custom_kart_params_form table thead tr th:nth-child(' + columnCount + ')').html( headType.hotred );
                                $('#custom_kart_params_form table thead tr th:nth-child(' + columnCount + ')').addClass("bg-color-hotred");
                            }
                            if($(this).html().length == 0 && columnCount == 3){
                                $('#custom_kart_params_form table thead tr th:nth-child(' + columnCount + ')').html( headType.niceblue );
                                $('#custom_kart_params_form table thead tr th:nth-child(' + columnCount + ')').addClass("bg-color-niceblue");
                            }
                            if($(this).find("button i").length > 0){
                                var attrClass = $(this).find("button i").attr("class").split("text-color-")[1];
                                
                                
                                $('#custom_kart_params_form table thead tr th:nth-child(' + columnCount + ')').html( headType[attrClass] );
                                $('#custom_kart_params_form table thead tr th:nth-child(' + columnCount + ')').addClass("bg-color-" + attrClass);
                                
                                
                                if (attrClass.localeCompare('lightpurple') == 0){
                                   
                                    $('#custom_kart_params_form table thead tr th:nth-child(' + columnCount + ')').attr({"colspan" : "2"});
                                    $('#custom_kart_params_form table thead tr th:nth-child(' + (columnCount + 1)  + ')').remove();
                                    console.log (columnCount);
                                    columnCount--;
                                }
                                if (attrClass.localeCompare('lightpurple2') == 0){
                                    $('#custom_kart_params_form table thead tr th:nth-child(' + (columnCount -1) + ')').html( headType.lightpurple);
                                    $('#custom_kart_params_form table thead tr th:nth-child(' + (columnCount -1) + ')').addClass("bg-color-lightpurple");
                                    $('#custom_kart_params_form table thead tr th:nth-child(' + (columnCount -1) + ')').attr({"colspan" : "2"});
                                    $('#custom_kart_params_form table thead tr th:nth-child(' +  columnCount  + ')').remove();
                                    columnCount--;
                                }
                                
                                
                            }
                            
                            columnCount++;
                        });
                        var columnCount2 = 1;
                        $('#custom_kart_params_form2 table tbody tr:nth-child(1) td').each(function () {
                            if($(this).find("button i").length > 0){
                                var attrClass = $(this).find("button i").attr("class").split("text-color-")[1];
                                $('#custom_kart_params_form2 table thead tr th:nth-child(' + columnCount2 + ')').html( headType[attrClass] );
                                $('#custom_kart_params_form2 table thead tr th:nth-child(' + columnCount2 + ')').addClass("bg-color-" + attrClass);
                            }
                            columnCount2++;
                        });
                        
                        
                        $('#custom_kart_params_form table thead tr th:nth-child(5)').addClass("btnCol");
                        
                        var headerTop    = $(".o_main_content").offset().top + $(".o_control_panel").outerHeight(true); //$(".o_view_manager_content").offset().top;
                        var topOfsetSoft = $(".table-responsive").offset();
                        if (topOfsetSoft){
                            topOfsetSoft = topOfsetSoft.top;    
                        }
                        else{
                            topOfsetSoft = headerTop + 1;
                        }
                        
                        var $orginHeader  = $($('#custom_kart_params_form table thead'));
                        //console.log($orginHeader);
                        var $cloneHeader = $orginHeader.clone();
                        //console.log($cloneHeader);
                        
                        $orginHeader.after($cloneHeader);
                        $orginHeader.css({"position" : 'static', "opacity" : 1});
                        $cloneHeader.css({"display" : 'none', "opacity" : 0.0});
        
                        if(!$orginHeader.hasClass($orginHeader)){
                            $orginHeader.addClass("orginHeader");
                            $cloneHeader.addClass("cloneHeader");
                            //
                            
                            $(".o_content").scroll(function() {
                                
                                    var topOfset = $($("#custom_kart_params_form table")).offset();
                                    //console.log(topOfset);
                                    //console.log(headerTop);
                                    if (topOfset){
                                        topOfset = topOfset.top;
                                        if(headerTop > topOfset){
                                            responseTableHead_2();
                                            $cloneHeader.css({
                                                 "opacity" : 1,
                                                 "display" : "table-header-group"
                                            });
                                            $orginHeader.css({
                                                "position" : "fixed",
                                                "top" : headerTop,
                                                 "z-index'" : 2,
                                                 "opacity" : 1,
                                                 "display" : "table-header-group"
                                            });
                                        }
                                        else{
                                            $cloneHeader.attr("style", "");
                                            $orginHeader.attr("style", "");
                                            $cloneHeader.css({"display" : 'none', "opacity" : 0.0});
                                        }
                                    } 
                            });
                        }
                        
                    }
                /*}
                catch(e){
                    console.log('catched', e);
                }*/
            }
            //console.log (1);
            //console.log($("#o_button_import"));
            //$(".o_button_import").css({'display':'none'});
            //$("#o_button_import").css({"display":'none'});
            //console.log($("#o_button_import").css());
            return val;
        }
    });
});
$( window ).resize(function() {
    responseTableHead_2();
});

function responseTableHead_2(){

    var th1 =  $('#custom_kart_params_form table thead.orginHeader tr th:nth-child(1)').width();
    var th2 =  $('#custom_kart_params_form table thead.orginHeader tr th:nth-child(2)').width();
    var th3 =  $('#custom_kart_params_form table thead.orginHeader tr th:nth-child(3)').width();
    var th4 =  $('#custom_kart_params_form table thead.orginHeader tr th:nth-child(4)').width();
    var th5 =  $('#custom_kart_params_form table thead.orginHeader tr th:nth-child(5)').width();
    var th6 =  $('#custom_kart_params_form table thead.orginHeader tr th:nth-child(6)').width();
    var th7 =  $('#custom_kart_params_form table thead.orginHeader tr th:nth-child(7)').width();
    var th8 =  $('#custom_kart_params_form table thead.orginHeader tr th:nth-child(8)').width();
    var th9 =  $('#custom_kart_params_form table thead.orginHeader tr th:nth-child(9)').width();
    var th10 = $('#custom_kart_params_form table thead.orginHeader tr th:nth-child(10)').width();
    var th11 = $('#custom_kart_params_form table thead.orginHeader tr th:nth-child(11)').width();
    var th12 = $('#custom_kart_params_form table thead.orginHeader tr th:nth-child(12)').width();
    var th13 = $('#custom_kart_params_form table thead.orginHeader tr th:nth-child(13)').width();
    var th14 = $('#custom_kart_params_form table thead.orginHeader tr th:nth-child(14)').width();
    var th15 = $('#custom_kart_params_form table thead.orginHeader tr th:nth-child(15)').width();
    var th16 = $('#custom_kart_params_form table thead.orginHeader tr th:nth-child(16)').width();
    var th17 = $('#custom_kart_params_form table thead.orginHeader tr th:nth-child(17)').width();
    var th18 = $('#custom_kart_params_form table thead.orginHeader tr th:nth-child(18)').width();

    $(".orginHeader tr th:nth-child(1)").width(th1);
    $(".orginHeader tr th:nth-child(2)").width(th2);
    $(".orginHeader tr th:nth-child(3)").width(th3);
    $(".orginHeader tr th:nth-child(4)").width(th4);
    $(".orginHeader tr th:nth-child(5)").width(th5);
    $(".orginHeader tr th:nth-child(6)").width(th6);
    $(".orginHeader tr th:nth-child(7)").width(th7);
    $(".orginHeader tr th:nth-child(8)").width(th8);
    $(".orginHeader tr th:nth-child(9)").width(th9);
    $(".orginHeader tr th:nth-child(10)").width(th10);
    $(".orginHeader tr th:nth-child(11)").width(th11);
    $(".orginHeader tr th:nth-child(12)").width(th12);
    $(".orginHeader tr th:nth-child(13)").width(th13);
    $(".orginHeader tr th:nth-child(14)").width(th14);
    $(".orginHeader tr th:nth-child(15)").width(th15);
    $(".orginHeader tr th:nth-child(16)").width(th16);
    $(".orginHeader tr th:nth-child(17)").width(th17);
    $(".orginHeader tr th:nth-child(18)").width(th18);
}

    $('.change_odeme_yontemi').on('change', function (e) {
        alert(this.value);
    });


