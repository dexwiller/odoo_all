# -*- coding: utf-8 -*-

from odoo import models, fields, api,SUPERUSER_ID,exceptions,modules
from datetime import datetime
from io import StringIO
import base64
import qrcode
def generate_campaign_barcode( barcode_obj ):
    p_name      = barcode_obj.partner_id.name
    c_name      = barcode_obj.campaing_message
    c_id        = barcode_obj.id
    
    qr_text = p_name + u';' + c_name + u';' + str(c_id)
    img = qrcode.make( qr_text )
    buff = StringIO()
    img.save( buff )
    img_str = base64.b64encode( buff.getvalue() )
    
    return qr_text, img_str
class User(models.Model):
    _inherit = 'res.users'
    
    user_card_no   = fields.Char('Yemekmatik Kart No')
    user_card_type = fields.Integer('Yemekmatik Kart Tipi', default=1)
    tckn           = fields.Char()
    bdate          = fields.Date(string='Doğum Tarihi')

    def _check_credentials_by_email_pass(self,db_name,email, password):
        print (1)
        """ Validates the current user's password.

        Override this method to plug additional authentication methods.

        Overrides should:

        * call `super` to delegate to parents for credentials-checking
        * catch AccessDenied and perform their own checking
        * (re)raise AccessDenied if the credentials are still invalid
          according to their own validation method

        When trying to check for credentials validity, call _check_credentials
        instead.
        """
        """ Override this method to plug additional authentication methods"""
        assert password
        reg = modules.registry.Registry.new(db_name)
        print(2)
        new_cr = reg.cursor()
        # if it's a copy of a database, force generation of a new dbuuid
        new_env = api.Environment(new_cr, SUPERUSER_ID, {})
        print(3)
        new_env['ir.config_parameter'].init(force=True)
        print(4)
        user_obj = new_env["res.users"].sudo()
        new_cr.execute(
            "SELECT COALESCE(password, '') FROM res_users WHERE login=%s",
            [email]
        )
        print(5)
        [hashed] = new_cr.fetchone()
        valid = self._crypt_context()\
            .verify(password, hashed)
        print ( valid  )
        if not valid:
            new_cr.close()
            return False
        user = user_obj.search([('login','=',email)])

        return user

    @api.model
    def send_mail(self,raw_pass):
        local_context = self.env.context.copy()
        company = self.env['res.users'].browse(SUPERUSER_ID).company_id.name
        local_context.update({
            'dbname': self.env.cr.dbname,
            'base_url': self.env['ir.config_parameter'].get_param('web.base.external_url', default='http://localhost:8069'),
            'datetime' : datetime,
            'company' : company,
            'only_user_sign' : True,
            'raw_pass':raw_pass
        })
        temp_obj = self.env['mail.template']
        template_id = self.env['ir.model.data'].sudo().get_object('yemekmatikxmlws', 'pass_change_template')
        template_obj = temp_obj.with_context(local_context).browse(template_id.id)
        body_html = temp_obj.with_context(local_context).render_template(template_obj.body_html, 'res.users', self.id)
        
        # print body_html
        
        if template_id:
            values = template_obj.generate_email( res_ids = self.id)
            values['subject'] = 'Şifre Bilgileri'
            values['email_to'] = self.login
            values['body_html'] = body_html
            values['body'] = body_html
            values['res_id'] = False
            values['email_from'] = 'info@yemekmatik.com.tr'
            mail_mail_obj = self.env['mail.mail'].sudo()
            msg_id = mail_mail_obj.create( values )

            if msg_id:
                # print  msg_id
                msg_id.sudo().send()
        return True
'''
class YmCampaign( models.Model):
    
    _name        = 'yemekmatik.campaign'
    
    campaing_message           = fields.Html('Kampanya Mesajı')
    campaing_discount_type     = fields.Selection(selection=[('p','Yüzde'),('a','Tutar')],string='İndirim Methodu',required=True)
    campaing_discount          = fields.Float('Kampanya İndirimi',required=True)
    campaign_barcode           = fields.Char('Barcode Data')
    campaign_barcode_img       = fields.Binary('Barcode Image')
    campaign_img_name          = fields.Char('Kampanya Resim Adı')
    campaign_img               = fields.Binary('Kampanya Resmi')
    vitrin                     = fields.Boolean('Vitrin Kampanyası')
    partner_id                 = fields.Many2one('res.partner','Kampanya Üye İş Yeri')
    
    @api.onchange('vitrin')
    def vitrin_onchange(self):
        if self.vitrin:
            c = self.search([('partner_id','=',self.partner_id.id)])
            for k in c:
                k.sudo().write({'vitrin','=',False})
    
    @api.multi
    def generate_barcode(self):
        self.ensure_one()
        return {
            "type": "ir.actions.act_url",
            "url": "/campaign_barcode/"+str(self.id),
            "target": "popup",
        } 
    @api.model
    def create(self,values):
        res = super( YmCampaign, self ).create( values )
        qr_text, img = generate_campaign_barcode( res )
        res.campaign_barcode        = qr_text
        res.campaign_barcode_img    = img
        
        return res
    @api.multi
    def write(self,values):
        self.ensure_one()
        if values.get('partner_id') or values.get('campaing_message'):
            qr_text, img = generate_campaign_barcode( self )
            values['campaign_barcode_img'] = img
            values['campaign_barcode']     = qr_text
        return super( YmCampaign, self ).write( values )
        
    
class Partner(models.Model):
    _inherit = 'res.partner'
    
    #campaign = fields.One2many('yemekmatik.campaign','partner_id',string='Kampanyalar')
'''
# class addons/yemekmatikxmlws(models.Model):
#     _name = 'addons/yemekmatikxmlws.addons/yemekmatikxmlws'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100