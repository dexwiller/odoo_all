# -*- coding: utf-8 -*-
from odoo import http,api,modules
import datetime
from collections import OrderedDict
from pysimplesoap.server import SoapDispatcher
try:
    from urllib.request import urlopen
except ImportError:
    from urllib2 import urlopen
try:
    import json
except ImportError:
    import simplejson as json
import sys
import  time
import random
from io import StringIO
import base64, string
from odoo.addons.web.controllers.main import serialize_exception
from odoo.addons.info_kart.models.util import cipher, EncodeAES
from odoo.addons.info_bayi.models.webserviceopt import get_extre,bakiye,TL_Bloke_Islem,Kisisellestirme,Kd_Harcama_Islem,Kd_Harcama_Iptal,getPartnerFromOkc
from odoo import modules, SUPERUSER_ID
master_db_name = 'yemek12'

class YemekWebService(http.Controller):

    @http.route('/campaign_barcode/<d>', type='http', auth="user",csrf=False)
    @serialize_exception
    def campaign_barcode( self, d=None,**kw):
        barcode_obj = http.request.env['yemekmatik.campaign'].sudo().browse([ d ])

        return  http.request.render(template, {
            'msg': msg,
            'vendor_msg':vendor_msg,
            'image':image,
            'href':"javascript:%s/web';this.window.close();"%href,
            'makbuz_url':''
        })
    
    @http.route('/yemekmatikservice', auth='public',csrf=False)
    def service_index_ym(self, **kw):
        
        def get_partner_by_token( kurum_kodu, token ):
            # print kurum_kodu, token
            harici_firma_obj = http.request.env['info_bayi.harici_firmalar'].sudo().search([('kurum_kodu','=',kurum_kodu),('kurum_token','=',token)])
            if len(harici_firma_obj) == 1:
                harici_firma_obj = harici_firma_obj[0].id
            else:
                harici_firma_obj = None
            return harici_firma_obj
        
        def basvuru_kayit_sorgu(kurumKodu,kurumToken,basvuruId,basvuruDurum,basvuruStr,kartNo):
            t = get_partner_by_token( kurumKodu, kurumToken)
            if not t:
                return {'BasvuruDurumResponse': {'basvuruId':basvuruId,
                                                 'cevapKodu':'002',
                                                 'cevapAciklama':u'KurumKodu/ Token Hatalı'
                                                 }} 
            
            partner = http.request.env['res.partner'].sudo().search([('turkpara_basvuru_id','=',basvuruId)])
            if partner:
                try:
                    if int(basvuruDurum) == 1:  
                        if kartNo:
                            partner.turkpara_e_para_hesap = kartNo
                            partner.state='islak_bekleniyor'
                            for k in partner.bireysel_kartlar:
                                k.state = 'sold3'
                        else:
                            return {'BasvuruDurumResponse': {'basvuruId':basvuruId,
                                                 'cevapKodu':'004',
                                                 'cevapAciklama':u'Kart No Bulunamadı'
                                                 }}
                    else:
                        partner.state             = 'turkpara_ws_error'
                        partner.turkpara_ws_error = basvuruStr
                except:
                    return {'BasvuruDurumResponse': {'basvuruId':basvuruId,
                                                 'cevapKodu':'099',
                                                 'cevapAciklama':u'Input Validasyon Basarisiz Gönderilen Alanları Kontrol Ediniz.'
                                                 }}
            else:
                return {'BasvuruDurumResponse': {'basvuruId':basvuruId,
                                                 'cevapKodu':'003',
                                                 'cevapAciklama':u'Başvuru Bulunamadı'
                                                 }}
            
            
            return {'BasvuruDurumResponse': {'basvuruId':basvuruId,
                                                 'cevapKodu':'001',
                                                 'cevapAciklama':u'Kayıt Başarılı'
                                                 }} 
        
        def toplu_yukleme_iptal_result(kurumKodu,kurumToken,base64file,islemTipi):
            t = get_partner_by_token( kurumKodu, kurumToken)
            if not t:
                return {'TopluYuklemeIptalResponse': {
                                                 'cevapKodu':'002',
                                                 'cevapAciklama':u'KurumKodu/ Token Hatalı'
                                                 }}
            file_text = base64.b64decode( base64file)
            lines     = file_text.split('\n')
            bakiye    = {}
            nf        = StringIO()
            b         = open('/var/lib/odoo/servis_donen.txt','w') 
            partner   = None
            cuzdan_rel = None
            for l in lines:
                kart    = None
                ls      = l.split('|')
                # print ls
                try:
                    if len(ls) > 4:
                        kart_no     = ls[0][0:4] + ' ' + ls[0][4:8] + ' ' + ls[0][8:12] + ' '  + ls[0][12:16] 
                        kart_no     = EncodeAES (cipher,kart_no)
                        
                        tutar       = float(ls[1])
                        cuzdan      = ls[2]
                        cuzdan = http.request.env['info_kart.cuzdan'].sudo().search([('kod','=',cuzdan)])
                        if cuzdan:
                            cuzdan      = cuzdan.id
                        else:
                            cuzdan = False
                        state       = ls[3]
                        state_str   = ls[4]
                        
                        # print tutar, cuzdan, state, state_str
                        
                        kart        = http.request.env['stock.production.lot'].sudo().search([('seri_no','=',kart_no)])
    
                        if int(state) > 0:
                            partner     = kart.sold_to
                            cuzdan_rel  = http.request.env['info_kart.cuzdan_partner_rel'].sudo().search([('cuzdan','=',cuzdan),
                                                                                                          ('partner_id','=',partner.id)])
                            dekont      = http.request.env['info_kart.set_limit'].sudo().search([('yid','=',state)])
                            ## print dekont
                            if not dekont:
                                http.request.env['info_kart.set_limit'].sudo().with_context({'active_model':'res.partner',
                                                                                             'active_id':kart.sold_to.id}).create(
                                                                  {'partner':kart.sold_to.id,
                                                                   'lot':kart.id,
                                                                   'tutar':tutar,
                                                                   'yid':state,
                                                                   'cuzdan':cuzdan,
                                                                   'cuzdan_rel_id':cuzdan_rel.id,
                                                                   'state':'confirm'})


                            if int(islemTipi) == 1:
                                nf.write( ls[0] + '|' + str(tutar) + '|' + state +  '\n' )
                                b.write( ls[0] + '|' + str(tutar) + '|' + state +  '\n' )
                            
                            if int(islemTipi) == 2:#iade basarili
                                if bakiye.get(cuzdan_rel):
                                    bakiye[cuzdan_rel] += tutar
                                else:
                                    bakiye[cuzdan_rel] = tutar
                        else:
                            if int(islemTipi) == 1:#bakiye zaten düşmüş, burada artırıyoruz.
                                if bakiye.get(cuzdan_rel):
                                    bakiye[cuzdan_rel] += tutar
                                else:
                                    bakiye[cuzdan_rel] = tutar
                    elif len(ls) == 4: #eski dosya
                        kart_no     = ls[0][0:4] + ' ' + ls[0][4:8] + ' ' + ls[0][8:12] + ' '  + ls[0][12:16] 
                        kart_no     = EncodeAES (cipher,kart_no)
                        
                        tutar       = float(ls[1])
                        cuzdan      = 1
                        
                        state       = ls[2]
                        state_str   = ls[3]
                        
                        # print tutar, cuzdan, state, state_str
                        
                        kart        = http.request.env['stock.production.lot'].sudo().search([('seri_no','=',kart_no)])
    
                        if int(state) > 0:
                            partner = kart.sold_to
                            dekont = http.request.env['info_kart.set_limit'].sudo().search([('yid','=',state)])
                            ## print dekont
                            if not dekont:
                                http.request.env['info_kart.set_limit'].sudo().with_context({'active_model':'res.partner',
                                                                                             'active_id':kart.sold_to.id}).create(
                                                                  {'partner':kart.sold_to.id,
                                                                   'lot':kart.id,
                                                                   'tutar':tutar,
                                                                   'yid':state,
                                                                   'cuzdan':cuzdan,
                                                                   'state':'confirm'})

                except:
                    # print 'except'
                    continue
                if kart:
                    kart.sudo().write({'son_bakiye_yukleme_mesaj' : state})
                else:
                    http.request.env['stock.production.lot'].sudo().search([('seri_no','=',kart_no)]).write({'son_bakiye_yukleme_mesaj' : state})
            
            b64text = base64.b64encode( nf.getvalue())
            nf.close()
            b.close()
            
            for c,t in bakiye.items():
                prev_limit        = c.bakiye
                next_limit        = prev_limit + t
                c.bakiye          = next_limit
            
            if cuzdan_rel:
                 
                cuzdan_rel.write ({'iade_text':b64text})
                ## print 'Limit : ', next_limit
            
            return  {'TopluYuklemeIptalResponse': {
                                                 'cevapKodu':'000',
                                                 'islemTipi':islemTipi,
                                                 'cevapAciklama':u'Sonuç Başarılı'
                                                 }}
            
            
                
            
        param = http.request.env["ir.config_parameter"]

        dispatcher = SoapDispatcher(
            'soap_service',
            location = param.get_param( "web.base.external_url", default='http://localhost:8069') + '/yemekmatikservice/',
            action = param.get_param( "web.base.external_url", default='http://localhost:8069') + '/yemekmatikservice/',
            #namespace = "http://esintegration.infoteks.com.tr/",
            #prefix='tns',
            ns = None)

        # register func

        basvuru_durum_returns = dict(basvuruId= str,cevapKodu =str, cevapAciklama= str)

        print ( basvuru_durum_returns )

        dispatcher.register_function('BasvuruDurum', basvuru_kayit_sorgu,
            returns=basvuru_durum_returns,
            args=dict(OrderedDict([('kurumKodu',str),('kurumToken',str),('basvuruId',str),('basvuruDurum',str),('basvuruStr',str),('kartNo',str)]))
        )
        dispatcher.register_function('TopluYuklemeIptalResult', toplu_yukleme_iptal_result,
             returns=OrderedDict([('TopluYuklemeIptalResponse', OrderedDict(
                                                 [('islemTipi', str),('cevapKodu', str), ('cevapAciklama', str)])
                                     )]),
             args=OrderedDict([('kurumKodu',str),('kurumToken',str),('base64file',str),('islemTipi',str)])
         )
        if http.request.httprequest.method == "POST":

            time_request = datetime.datetime.today()
            response = http.request.make_response(dispatcher.dispatch(http.request.httprequest.data))
            time_response = datetime.datetime.today()
            logging_var={'serviceName':'yemekmatikservice','operationName':'',
                         'requestTime':time_request,'responseTime':time_response,
                         'requestData':http.request.httprequest.data,'responseData':response.data
                         }
            logging_obj = http.request.env['info_bayi.turkpara_ws_logs']
            logging_obj.sudo().create(logging_var)

        else:
            response = http.request.make_response(dispatcher.wsdl())

        response.headers['Content-Type'] = 'text/xml'
        return response

    @http.route('/ymkdintegration', auth='public',csrf=False)
    def service_index_kd(self, **kw):

        def get_partner_by_token( kurum_kodu, token ):
            harici_firma_obj = http.request.env['info_tsm.harici_firmalar'].sudo().search([('kurum_kodu','=',kurum_kodu),('kurum_token','=',token)])
            if len(harici_firma_obj) == 1:
                harici_firma_obj = harici_firma_obj[0].musteri.id
            else:
                harici_firma_obj = None
            return harici_firma_obj

        def get_cihaz_kontrol ( partner, seri_no):
            cihaz_seri_no_kontrol = http.request.env['stock.production.lot'].sudo().search([('name','=',seri_no),('sold_to','=',partner)])
            if len( cihaz_seri_no_kontrol ) == 1:
                return cihaz_seri_no_kontrol[0].id
            return None

        def sozlesme_kontrol( partner,check_list,cihaz_id ):
            search_list = [('firma','=', partner),('name','=',cihaz_id)]
            for l in check_list:
                search_list.append ( ( l,'=', True ) )
                search_list.append ( ( str(l)+'_bas', '<=', time.strftime('%Y-%m-%d') ))
                search_list.append ( ( str(l)+'_bit', '>=', time.strftime('%Y-%m-%d') ))
            # print search_list
            guncel_sozlesme = http.request.env['info_tsm.katma_deger_sozlesme'].sudo().search(search_list,order='id desc')
            sozlesme = None
            if len(guncel_sozlesme) > 0 :
                sozlesme = guncel_sozlesme[0]
            return sozlesme

        def kdSalesReportData (kurumKodu,kurumToken,OKCSeriNo,reportDateStart,reportDateEnd, reportZNo ):
            # print kurumKodu,kurumToken

            partner = get_partner_by_token (kurumKodu,kurumToken)

            if not partner:
                return {'kdSalesReportDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'001',
                                                 'cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'
                                                 }}
            cihaz_id = get_cihaz_kontrol( partner, OKCSeriNo )
            if not cihaz_id:
                return {'kdSalesReportDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'004',
                                                 'cevapAciklama':'Tanimsiz YNOKC'
                                                 }}
            sozlesme = sozlesme_kontrol( partner, ['z_raporu','fis_raporu'],cihaz_id)
            if not sozlesme:
                return {'kdSalesReportDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'003',
                                                 'cevapAciklama':'Tanimsiz Harici Sistem'
                                                 }}


            search_list = [('name','=', cihaz_id)]
            if reportDateStart:
                reportDateStart = datetime.datetime.strptime(reportDateStart, '%Y%m%d').strftime('%Y-%m-%d')
                search_list.append( ('rap_bas','>=', reportDateStart ))
            if reportDateEnd  :
                reportDateEnd   = datetime.datetime.strptime(reportDateEnd, '%Y%m%d').strftime('%Y-%m-%d')
                search_list.append( ('rap_bas','<=', reportDateEnd ))
            if reportZNo:
                search_list.append (('z_rapor_no','=',reportZNo))

            search_list = []
            satis_raporlari   = http.request.env['info_tsm.satis_raporu'].sudo().search( search_list )
            satis_raporu_list = []

            for s in satis_raporlari:
                satis = {}

                satis['salesLine'] = dict(prdName       = s.urun_aciklama,
                     prdUnitType    = birim_tipleri_map[ s.birim_tipi ].decode('utf-8'),
                     prdSaleAmount  = ('%f' % s.miktar).rstrip('0').rstrip('.'),
                     prdVatRate     = s.kdv_dilimi,
                     salesTotalVAT  = ('%f' % s.total_kdv).rstrip('0').rstrip('.'),
                     salesTotalCost = ('%f' % s.toplam_tutar).rstrip('0').rstrip('.'),
                     salesReportZNo = s.z_rapor_no
                )
                satis_raporu_list.append ( satis )

            satis_raporlari   = http.request.env['info_tsm.satis_raporu_eslesmeyen'].sudo().search( search_list )

            for s in satis_raporlari:
                satis = {}

                satis['salesLine'] = dict(prdName       = s.urun_aciklama,
                     prdUnitType    = birim_tipleri_map[ s.birim_tipi ].decode('utf-8'),
                     prdSaleAmount  = ('%f' % s.miktar).rstrip('0').rstrip('.'),
                     prdVatRate     = s.kdv_dilimi ,
                     salesTotalVAT  = ('%f' % s.total_kdv).rstrip('0').rstrip('.'),
                     salesTotalCost = ('%f' % s.toplam_tutar).rstrip('0').rstrip('.'),
                     salesReportZNo = s.z_rapor_no
                )
                satis_raporu_list.append ( satis )

            # print satis_raporu_list

            return {'kdSalesReportDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'000',
                                                 'cevapAciklama':'islem Basarili',
                                                 'reportData': satis_raporu_list }}

        def kdContractInfo (kurumKodu,kurumToken,OKCSeriNo ):
            # print kurumKodu,kurumToken
            partner = get_partner_by_token (kurumKodu,kurumToken)
            if not partner:
                return {'kdContractInfoCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'001',
                                                 'cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'
                                                 }}
            cihaz_id = get_cihaz_kontrol( partner, OKCSeriNo )
            if not cihaz_id:
                return {'kdContractInfoCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'004',
                                                 'cevapAciklama':'Tanimsiz YNOKC'
                                                 }}

            sozlesme = sozlesme_kontrol( partner, [],cihaz_id)
            if not sozlesme:
                return {'kdSalesReportDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'003',
                                                 'cevapAciklama':'Tanimsiz Harici Sistem'
                                                 }}
            z_raporu_bas = ''
            if sozlesme.z_raporu_bas:
                z_raporu_bas = datetime.datetime.strptime(sozlesme.z_raporu_bas, '%Y-%m-%d').strftime('%Y%m%d')

            z_raporu_bit = ''
            if sozlesme.z_raporu_bit:
                z_raporu_bit = datetime.datetime.strptime(sozlesme.z_raporu_bit, '%Y-%m-%d').strftime('%Y%m%d')

            fis_raporu_bas = ''
            if sozlesme.fis_raporu_bas:
                fis_raporu_bas = datetime.datetime.strptime(sozlesme.fis_raporu_bas, '%Y-%m-%d').strftime('%Y%m%d')

            fis_raporu_bit = ''
            if sozlesme.fis_raporu_bit:
                fis_raporu_bit = datetime.datetime.strptime(sozlesme.fis_raporu_bit, '%Y-%m-%d').strftime('%Y%m%d')

            olay_raporu_bas = ''
            if sozlesme.olay_raporu_bas:
                olay_raporu_bas = datetime.datetime.strptime(sozlesme.olay_raporu_bas, '%Y-%m-%d').strftime('%Y%m%d')

            olay_raporu_bit = ''
            if sozlesme.olay_raporu_bit:
                olay_raporu_bit = datetime.datetime.strptime(sozlesme.olay_raporu_bit, '%Y-%m-%d').strftime('%Y%m%d')

            istat_raporu_bas = ''
            if sozlesme.istat_raporu_bas:
                istat_raporu_bas = datetime.datetime.strptime(sozlesme.istat_raporu_bas, '%Y-%m-%d').strftime('%Y%m%d')

            istat_raporu_bit = ''
            if sozlesme.istat_raporu_bit:
                istat_raporu_bit = datetime.datetime.strptime(sozlesme.istat_raporu_bit, '%Y-%m-%d').strftime('%Y%m%d')

            sozlesme_dict = dict(zReportActive = sozlesme.z_raporu,

                                 zReportStart  = z_raporu_bas,
                                 zReportEnd    = z_raporu_bit,

                                 salesActive = sozlesme.fis_raporu,
                                 salesStart  = fis_raporu_bas,
                                 salesEnd    = fis_raporu_bit,

                                 eventsActive = sozlesme.olay_raporu,
                                 eventsStart  = olay_raporu_bas,
                                 eventsEnd    = olay_raporu_bit,

                                 statisticActive = sozlesme.istat_raporu,
                                 statisticStart  = istat_raporu_bas,
                                 statisticEnd    = istat_raporu_bit,
                                 )
            return  {'kdContractInfoCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'000',
                                                 'cevapAciklama':'Islem Basarili',
                                                 'contractInfo' : sozlesme_dict
                                                 }}

        def kdSalesReceiptData ( kurumKodu,kurumToken,OKCSeriNo,reportDateStart,reportDateEnd, reportZNo ):
            # print kurumKodu,kurumToken
            partner = get_partner_by_token (kurumKodu,kurumToken)
            if not partner:
                return {'kdSalesReceiptDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'001',
                                                 'cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'
                                                 }}
            cihaz_id = get_cihaz_kontrol( partner, OKCSeriNo )
            if not cihaz_id:
                return {'kdSalesReceiptDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'004',
                                                 'cevapAciklama':'Tanimsiz YNOKC'
                                                 }}
            sozlesme = sozlesme_kontrol( partner, ['fis_raporu'],cihaz_id)
            if not sozlesme:
                return {'kdSalesReceiptDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'003',
                                                 'cevapAciklama':'Tanimsiz Harici Sistem'
                                                 }}


            search_list = [('terminal_serial','=', OKCSeriNo)]
            if reportDateStart:
                reportDateStart = datetime.datetime.strptime(reportDateStart, '%Y%m%d').strftime('%Y-%m-%d')
                search_list.append( ('islem_tarihi','>=', reportDateStart ))
            if reportDateEnd  :
                reportDateEnd   = datetime.datetime.strptime(reportDateEnd, '%Y%m%d').strftime('%Y-%m-%d')
                search_list.append( ('islem_tarihi','<=', reportDateEnd ))
            if reportZNo:
                search_list.append (('z_sayac_no','=',reportZNo))

            fis_raporlari   = http.request.env['info_tsm.fis_data'].sudo().search( search_list )
            fis_raporu_list = []
            for s in fis_raporlari:
                # print s.id
                fis_dict = {}
                kalems = []

                fis_kalems              =  http.request.env['info_tsm.fis_kalem_data'].sudo().search([('fis_id','=',s.id)] )

                for f in fis_kalems:
                    kalem_dict = {}
                    kalem_dict['salesLine'] = dict( salesLineName      = f.isim,
                                                    salesLineAmount    = ('%f' % f.miktar).rstrip('0').rstrip('.'),
                                                    salesLineIncAmount = ('%f' % f.artirim_tutari).rstrip('0').rstrip('.'),
                                                    salesLineDecAmount = ('%f' % f.indirim_tutari).rstrip('0').rstrip('.'),
                                                    salesLineVAT       = ('%f' % f.kdv_orani).rstrip('0').rstrip('.'),
                                                    salesLineTotCost   = ('%f' % f.tutar).rstrip('0').rstrip('.'),
                                                )
                    kalems.append  ( kalem_dict )
                islem_saati  = s.islem_saati
                islem_tarihi = datetime.datetime.strptime(islem_saati, '%Y-%m-%d %H:%M:%S').strftime('%Y%m%d%H%M%S')

                fis_dict['receiptLine'] = dict(salesTime       = islem_tarihi,
                     salesReceiptNo     = s.fis_sira_no,
                     salesZNo           = s.z_sayac_no,
                     salesEKUNo         = s.eku_no ,
                     salesReceiptType   = s.status,
                     salesTotalVAT      = ('%f' % s.toplam_kdv_tutari).rstrip('0').rstrip('.'),
                     salesTotalCost     = ('%f' % s.toplam_tutar).rstrip('0').rstrip('.'),
                     salesCashAmount    = ('%f' % s.nakit_odenen_tutar).rstrip('0').rstrip('.'),
                     salesKKAmount      = ('%f' % s.kredi_karti_odenen_tutar).rstrip('0').rstrip('.'),
                     salesOtherAmount   = ('%f' % s.diger_odenen_tutar).rstrip('0').rstrip('.'),
                     salesCurrAmount    = ('%f' % s.doviz_odenen_tutar).rstrip('0').rstrip('.'),
                     salesLines         = kalems
                )

                fis_raporu_list.append ( fis_dict )


            return {'kdSalesReceiptDataCevap': { 'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'000',
                                                 'cevapAciklama':'islem Basarili',
                                                 'salesData': fis_raporu_list }}

        def kdZReportData ( kurumKodu,kurumToken,OKCSeriNo,reportDateStart,reportDateEnd, reportZNo ):
            # print kurumKodu,kurumToken
            partner = get_partner_by_token (kurumKodu,kurumToken)
            if not partner:
                return {'kdZReportDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'001',
                                                 'cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'
                                                 }}
            cihaz_id = get_cihaz_kontrol( partner, OKCSeriNo )
            if not cihaz_id:
                return {'kdZReportDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'004',
                                                 'cevapAciklama':'Tanimsiz YNOKC'
                                                 }}
            sozlesme = sozlesme_kontrol( partner, ['z_raporu'],cihaz_id)
            if not sozlesme:
                return {'kdZReportDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'003',
                                                 'cevapAciklama':'Tanimsiz Harici Sistem'
                                                 }}


            search_list = [('terminal_serial','=', OKCSeriNo)]
            if reportDateStart:
                reportDateStart = datetime.datetime.strptime(reportDateStart, '%Y%m%d').strftime('%Y-%m-%d')
                search_list.append( ('islem_tarihi','>=', reportDateStart ))
            if reportDateEnd  :
                reportDateEnd   = datetime.datetime.strptime(reportDateEnd, '%Y%m%d').strftime('%Y-%m-%d')
                search_list.append( ('islem_tarihi','<=', reportDateEnd ))
            if reportZNo:
                search_list.append (('z_sayac_no','=',reportZNo))

            z_raporlari   = http.request.env['info_tsm.z_raporu_data'].sudo().search( search_list )

            z_raporu_list = []

            for z in z_raporlari:

                islem_saati  = z.islem_saati
                islem_tarihi = datetime.datetime.strptime(islem_saati, '%Y-%m-%d %H:%M:%S').strftime('%Y%m%d%H%M%S')
                z_dict = {}
                z_kisim_tutars = dict(  kisim_toplam_tutari1 = z.kisim_toplam_tutari1,
                                        kisim_toplam_tutari2 = z.kisim_toplam_tutari2,
                                        kisim_toplam_tutari3 = z.kisim_toplam_tutari3,
                                        kisim_toplam_tutari4 = z.kisim_toplam_tutari4,
                                        kisim_toplam_tutari5 = z.kisim_toplam_tutari5,
                                        kisim_toplam_tutari6 = z.kisim_toplam_tutari6,
                                        kisim_toplam_tutari7 = z.kisim_toplam_tutari7,
                                        kisim_toplam_tutari8 = z.kisim_toplam_tutari8,
                                        kisim_toplam_tutari9 = z.kisim_toplam_tutari9,
                                        kisim_toplam_tutari10 = z.kisim_toplam_tutari10,
                                        kisim_toplam_tutari11 = z.kisim_toplam_tutari11,
                                        kisim_toplam_tutari12 = z.kisim_toplam_tutari12
                                      )
                z_kisim_names  = dict(  kisim_adi1 = z.kisim_adi1,
                                        kisim_adi2 = z.kisim_adi2,
                                        kisim_adi3 = z.kisim_adi3,
                                        kisim_adi4 = z.kisim_adi4,
                                        kisim_adi5 = z.kisim_adi5,
                                        kisim_adi6 = z.kisim_adi6,
                                        kisim_adi7 = z.kisim_adi7,
                                        kisim_adi8 = z.kisim_adi8,
                                        kisim_adi9 = z.kisim_adi9,
                                        kisim_adi10 = z.kisim_adi10,
                                        kisim_adi11 = z.kisim_adi11,
                                        kisim_adi12 = z.kisim_adi12
                                      )
                z_kisim_adets  = dict(  kisim_toplam_adedi1 = z.kisim_toplam_adedi1,
                                        kisim_toplam_adedi2 = z.kisim_toplam_adedi2,
                                        kisim_toplam_adedi3 = z.kisim_toplam_adedi3,
                                        kisim_toplam_adedi4 = z.kisim_toplam_adedi4,
                                        kisim_toplam_adedi5 = z.kisim_toplam_adedi5,
                                        kisim_toplam_adedi6 = z.kisim_toplam_adedi6,
                                        kisim_toplam_adedi7 = z.kisim_toplam_adedi7,
                                        kisim_toplam_adedi8 = z.kisim_toplam_adedi8,
                                        kisim_toplam_adedi9 = z.kisim_toplam_adedi9,
                                        kisim_toplam_adedi10 = z.kisim_toplam_adedi10,
                                        kisim_toplam_adedi11 = z.kisim_toplam_adedi11,
                                        kisim_toplam_adedi12 = z.kisim_toplam_adedi12
                                      )
                z_cur_tutar    = dict(  doviz_toplam_tutar1 = z.doviz_toplam_tutar1,
                                    doviz_toplam_tutar2 = z.doviz_toplam_tutar2,
                                    doviz_toplam_tutar3 = z.doviz_toplam_tutar3,
                                    doviz_toplam_tutar4 = z.doviz_toplam_tutar4,
                                    doviz_toplam_tutar5 = z.doviz_toplam_tutar5,
                                    doviz_toplam_tutar6 = z.doviz_toplam_tutar6,

                                      )
                z_cur_names    = dict(  doviz_adi1 = z.doviz_adi1,
                                        doviz_adi2 = z.doviz_adi2,
                                        doviz_adi3 = z.doviz_adi3,
                                        doviz_adi4 = z.doviz_adi4,
                                        doviz_adi5 = z.doviz_adi5,
                                        doviz_adi6 = z.doviz_adi6,

                                      )
                z_cur_cevrim   = dict(  doviz_tl_karsiligi1 = z.doviz_tl_karsiligi1,
                                        doviz_tl_karsiligi2 = z.doviz_tl_karsiligi2,
                                        doviz_tl_karsiligi3 = z.doviz_tl_karsiligi3,
                                        doviz_tl_karsiligi4 = z.doviz_tl_karsiligi4,
                                        doviz_tl_karsiligi5 = z.doviz_tl_karsiligi5,
                                        doviz_tl_karsiligi6 = z.doviz_tl_karsiligi6,

                                      )
                z_ver_tutar    = dict(  vergi_toplam_tutari1 = z.vergi_toplam_tutari1,
                                    vergi_toplam_tutari2 = z.vergi_toplam_tutari2,
                                    vergi_toplam_tutari3 = z.vergi_toplam_tutari3,
                                    vergi_toplam_tutari4 = z.vergi_toplam_tutari4,
                                    vergi_toplam_tutari5 = z.vergi_toplam_tutari5,
                                    vergi_toplam_tutari6 = z.vergi_toplam_tutari6,
                                    vergi_toplam_tutari7 = z.vergi_toplam_tutari7,
                                    vergi_toplam_tutari8 = z.vergi_toplam_tutari8,

                                      )
                z_ver_names    = dict(  vergi_orani1 = z.vergi_orani1,
                                        vergi_orani2 = z.vergi_orani2,
                                        vergi_orani3 = z.vergi_orani3,
                                        vergi_orani4 = z.vergi_orani4,
                                        vergi_orani5 = z.vergi_orani5,
                                        vergi_orani6 = z.vergi_orani6,
                                        vergi_orani7 = z.vergi_orani7,
                                        vergi_orani8 = z.vergi_orani8,

                                      )
                z_ver_kdv   = dict(     vergi_toplam_kdv1 = z.vergi_toplam_kdv1,
                                        vergi_toplam_kdv2 = z.vergi_toplam_kdv2,
                                        vergi_toplam_kdv3 = z.vergi_toplam_kdv3,
                                        vergi_toplam_kdv4 = z.vergi_toplam_kdv4,
                                        vergi_toplam_kdv5 = z.vergi_toplam_kdv5,
                                        vergi_toplam_kdv6 = z.vergi_toplam_kdv6,
                                        vergi_toplam_kdv7 = z.vergi_toplam_kdv7,
                                        vergi_toplam_kdv8 = z.vergi_toplam_kdv8,

                                      )

                z_dict['zReportLine'] = dict(zReportTime                = islem_tarihi,
                                             zReportReceiptNo           = z.fis_sira_no,
                                             zReportNo                  = z.z_sayac_no,
                                             zReportEKUNo               = z.eku_no,
                                             zReportIncNum              = z.artirim_toplam_adedi,
                                             zReportDecNum              = z.indirim_toplam_adedi,
                                             zReportFixNum              = z.hata_duzelt_toplam_adedi,
                                             zReportFiscalRecNum        = z.mali_fis_adedi,
                                             zReportUnfiscalRecNum      = z.mali_olmayan_fis_adedi,
                                             zReportCustRecNum          = z.musteri_fis_adedi,
                                             zReportCancelRecNum        = z.satis_fisi_iptal_adedi,
                                             zReportBillRecNum          = z.faturali_satis_adedi,
                                             zReportRestTrnNum          = z.yemek_islem_sayisi,
                                             zReportParkTrnNum          = z.otopark_giris_islem_sayisi,
                                             zReportKisimCost           = z_kisim_tutars,
                                             zReportKisimName           = z_kisim_names,
                                             zReportKisimNum            = z_kisim_adets,
                                             zReportIncCost             = z.artirim_toplam_tutari,
                                             zReportDecCost             = z.indirim_toplam_tutari,
                                             zReportCashAmount          = z.nakit_toplam_tutar,
                                             zReportOtherAmount         = z.diger_odeme_toplam_tutar,
                                             zReportKKAmount            = z.kredi_karti_toplam_tutar,
                                             zReportCurrAmount          = z_cur_tutar,
                                             zReportCurrName            = z_cur_names,
                                             zReportCurrToTL            = z_cur_cevrim,
                                             zReportCancelAmount        = z.satis_iptal_toplam_tutar,
                                             zReportFixAmount           = z.hata_duzelt_toplam_tutar,
                                             zReportBillAmount          = z.faturali_satis_toplam_tutar,
                                             zReportRestAmount          = z.yemek_islem_toplam_tutari,
                                             zReportTechNum             = z.servis_mudehale_sayisi,
                                             zReportVATAmount           = z_ver_tutar,
                                             zReportVATRate             = z_ver_names,
                                             zReportVATCost             = z_ver_kdv,
                                             zReportDailyTotAmount      = z.gunluk_toplam_tutar,
                                             zReportDailyVATAmount      = z.gunluk_toplam_kdv,
                                             zReportAccTotAmount        = z.kumule_toplam_tutar,
                                             zReportAccVATAmount        = z.kumule_toplam_kdv
                                             )

                z_raporu_list.append( z_dict )
            # print len(z_raporu_list)
            return {'kdZReportDataCevap': { 'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'000',
                                                 'cevapAciklama':'islem Basarili',
                                                 'zRepData': z_raporu_list }}

        def kdEventsData (kurumKodu,kurumToken,OKCSeriNo,reportDateStart,reportDateEnd ):
            # print kurumKodu,kurumToken
            partner = get_partner_by_token (kurumKodu,kurumToken)
            if not partner:
                return {'kdEventsDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'001',
                                                 'cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'
                                                 }}
            cihaz_id = get_cihaz_kontrol( partner, OKCSeriNo )
            if not cihaz_id:
                return {'kdEventsDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'004',
                                                 'cevapAciklama':'Tanimsiz YNOKC'
                                                 }}
            sozlesme = sozlesme_kontrol( partner, ['olay_raporu'],cihaz_id)
            if not sozlesme:
                return {'kdEventsDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'003',
                                                 'cevapAciklama':'Tanimsiz Harici Sistem'
                                                 }}


            search_list = [('terminal_serial','=', OKCSeriNo)]
            if reportDateStart:
                reportDateStart = datetime.datetime.strptime(reportDateStart, '%Y%m%d').strftime('%Y-%m-%d')
                search_list.append( ('olay_tarihi','>=', reportDateStart ))
            if reportDateEnd  :
                reportDateEnd   = datetime.datetime.strptime(reportDateEnd, '%Y%m%d').strftime('%Y-%m-%d')
                search_list.append( ('olay_tarihi','<=', reportDateEnd ))

            olay_raporlari   = http.request.env['info_tsm.olay_kaydi_data'].sudo().search( search_list )

            olay_raporu_list = []

            for o in olay_raporlari:
                islem_saati  = o.olay_saati
                islem_tarihi = datetime.datetime.strptime(islem_saati, '%Y-%m-%d %H:%M:%S').strftime('%Y%m%d%H%M%S')
                olay_dict = {}
                olay_dict['eventsLine'] = dict(
                    eventsSource   = o.olay_kaynagi,
                    eventsCrtLevel = o.olay_kritiklik_seviyesi,
                    eventsType     = o.olay_tipi,
                    eventsTime     = islem_tarihi,
                    eventsUserName = o.kullanici_adi,
                    eventsTermState= o.yazar_kasa_konumu,
                )

                olay_raporu_list.append( olay_dict )

            return {'kdEventsDataCevap': { 'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'000',
                                                 'cevapAciklama':'islem Basarili',
                                                 'eventsData': olay_raporu_list }}
        def kdStatisticsData (kurumKodu,kurumToken,OKCSeriNo,reportDateStart,reportDateEnd ):
            # print kurumKodu,kurumToken
            partner = get_partner_by_token (kurumKodu,kurumToken)
            if not partner:
                return {'kdStatisticsDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'001',
                                                 'cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'
                                                 }}
            cihaz_id = get_cihaz_kontrol( partner, OKCSeriNo )
            if not cihaz_id:
                return {'kdStatisticsDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'004',
                                                 'cevapAciklama':'Tanimsiz YNOKC'
                                                 }}
            sozlesme = sozlesme_kontrol( partner, ['istat_raporu'],cihaz_id)
            if not sozlesme:
                return {'kdStatisticsDataCevap': {'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'003',
                                                 'cevapAciklama':'Tanimsiz Harici Sistem'
                                                 }}


            search_list = [('terminal_serial','=', OKCSeriNo)]
            if reportDateStart:
                reportDateStart = datetime.datetime.strptime(reportDateStart, '%Y%m%d').strftime('%Y-%m-%d')
                search_list.append( ('save_date','>=', reportDateStart ))
            if reportDateEnd  :
                reportDateEnd   = datetime.datetime.strptime(reportDateEnd, '%Y%m%d').strftime('%Y-%m-%d')
                search_list.append( ('save_date','<=', reportDateEnd ))

            istat_raporlari   = http.request.env['info_tsm.istatistik_data'].sudo().search( search_list )

            istat_raporu_list = []

            for i in istat_raporlari:

                stat_dict = {}
                stat_dict['statLine'] = dict(
                    statRespNum  = i.toplam_yanit_alan_islem,
                    statFailNum  = i.toplam_olumsuz_yanit_alan_islem,
                    statEthConn  = i.toplam_ethernet_baglanti,
                    statDialConn = i.toplam_dialup_baglanti,
                    statGPRSConn = i.toplam_gprs_baglanti,
                    statGSMConn  = i.toplam_gsm_baglanti,
                    statEthFail  = i.toplam_ethernet_basarisiz_baglanti,
                    statDialFail = i.toplam_dialup_basarisiz_baglanti,
                    statGPRSFail = i.toplam_gprs_basarisiz_baglanti,
                    statGSMFail  = i.toplam_gsm_basarisiz_baglanti,

                )

                istat_raporu_list.append( stat_dict )

            return {'kdStatisticsDataCevap': { 'OKCSeriNo':OKCSeriNo,
                                                 'cevapKodu':'000',
                                                 'cevapAciklama':'islem Basarili',
                                                 'statData': istat_raporu_list }}

        param = http.request.env["ir.config_parameter"]

        dispatcher = SoapDispatcher(
            'soap_service',
            location = param.get_param( "bkm.ws_location", default='http://localhost:8069') + '/kdintegration/',
            action = param.get_param( "bkm.ws_location", default='http://localhost:8069') + '/kdintegration/',
            #namespace = "http://esintegration.infoteks.com.tr/",
            #prefix='tns',
            ns = None)

        # register func
        dispatcher.register_function('kdSalesReportData', kdSalesReportData,
            returns=OrderedDict([('kdSalesReportDataCevap', OrderedDict(
                                                [('OKCSeriNo', str),('cevapKodu', str), ('cevapAciklama', str),('reportData',
                                                                      OrderedDict([('prdName',str),
                                                                                    ('prdUnitType',str),
                                                                                    ('prdSaleAmount',str),
                                                                                    ('prdVatRate',str),
                                                                                    ('salesTotalVAT',str),
                                                                                    ('salesTotalCost',str),
                                                                                    ('salesReportZNo',str)]))])
                                    )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str),('OKCSeriNo',str),('reportDateStart',str),('reportDateEnd',str),('reportZNo',str)])
        )

        dispatcher.register_function('kdContractInfo', kdContractInfo,
            returns=OrderedDict([('kdContractInfoCevap', OrderedDict(
                    [('OKCSeriNo', str),('cevapKodu', str), ('cevapAciklama', str),
                        ('contractInfo',
                            OrderedDict([('zReportActive',str),
                                        ('zReportStart',str),
                                        ('zReportEnd',str),
                                        ('salesActive',str),
                                        ('salesStart',str),
                                        ('salesEnd',str),
                                        ('eventsActive',str),
                                        ('eventsStart',str),
                                        ('eventsEnd',str),
                                        ('statisticActive',str),
                                        ('statisticStart',str),
                                        ('statisticEnd',str)
                                        ]
                            )
                        )
                    ]
                )
            )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str),('OKCSeriNo',str)])
        )

        dispatcher.register_function('kdSalesReceiptData', kdSalesReceiptData,
            returns=OrderedDict([('kdSalesReceiptDataCevap', OrderedDict(
                                                [('OKCSeriNo', str),('cevapKodu', str), ('cevapAciklama', str),('salesData',
                                                                      OrderedDict([('salesTime',str),
                                                                                    ('salesReceiptType',str),
                                                                                    ('salesReceiptNo',str),
                                                                                    ('salesZNo',str),
                                                                                    ('salesEKUNo',str),
                                                                                    ('salesTotalVAT',str),
                                                                                    ('salesTotalCost',str),
                                                                                    ('salesCashAmount',str),
                                                                                    ('salesKKAmount',str),
                                                                                    ('salesOtherAmount',str),
                                                                                    ('salesCurrAmount',str)]))])
                                    )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str),('OKCSeriNo',str),('reportDateStart',str),('reportDateEnd',str),('reportZNo',str)])
        )

        dispatcher.register_function('kdZReportData', kdZReportData,
            returns=OrderedDict([('kdZReportDataCevap', OrderedDict(
                                                [('OKCSeriNo', str),('cevapKodu', str), ('cevapAciklama', str),('zRepData',
                                                                      OrderedDict([('zReportTime',str),
                                                                                    ('zReportReceiptNo',str),
                                                                                    ('zReportNo',str),
                                                                                    ('zReportEKUNo',str),
                                                                                    ('zReportIncNum',str),
                                                                                    ('zReportDecNum',str),
                                                                                    ('zReportFixNum',str),
                                                                                    ('zReportFiscalRecNum',str),
                                                                                    ('zReportUnfiscalRecNum',str),
                                                                                    ('zReportCustRecNum',str),
                                                                                    ('zReportCancelRecNum',str),

                                                                                    ('zReportBillRecNum',str),
                                                                                    ('zReportRestTrnNum',str),
                                                                                    ('zReportParkTrnNum',str),
                                                                                    ('zReportKisimCost',OrderedDict([
                                                                                        ('kisim_toplam_tutari1',str),
                                                                                        ('kisim_toplam_tutari2',str),
                                                                                        ('kisim_toplam_tutari3',str),
                                                                                        ('kisim_toplam_tutari4',str),
                                                                                        ('kisim_toplam_tutari5',str),
                                                                                        ('kisim_toplam_tutari6',str),
                                                                                        ('kisim_toplam_tutari7',str),
                                                                                        ('kisim_toplam_tutari8',str),
                                                                                        ('kisim_toplam_tutari9',str),
                                                                                        ('kisim_toplam_tutari10',str),
                                                                                        ('kisim_toplam_tutari11',str),
                                                                                        ('kisim_toplam_tutari12',str),
                                                                                        ])),
                                                                                    ('zReportKisimName',OrderedDict([
                                                                                        ('kisim_adi1',str),
                                                                                        ('kisim_adi2',str),
                                                                                        ('kisim_adi3',str),
                                                                                        ('kisim_adi4',str),
                                                                                        ('kisim_adi5',str),
                                                                                        ('kisim_adi6',str),
                                                                                        ('kisim_adi7',str),
                                                                                        ('kisim_adi8',str),
                                                                                        ('kisim_adi9',str),
                                                                                        ('kisim_adi10',str),
                                                                                        ('kisim_adi11',str),
                                                                                        ('kisim_adi12',str),
                                                                                        ])),
                                                                                    ('zReportKisimNum',OrderedDict([
                                                                                        ('kisim_toplam_adedi1',str),
                                                                                        ('kisim_toplam_adedi2',str),
                                                                                        ('kisim_toplam_adedi3',str),
                                                                                        ('kisim_toplam_adedi4',str),
                                                                                        ('kisim_toplam_adedi5',str),
                                                                                        ('kisim_toplam_adedi6',str),
                                                                                        ('kisim_toplam_adedi7',str),
                                                                                        ('kisim_toplam_adedi8',str),
                                                                                        ('kisim_toplam_adedi9',str),
                                                                                        ('kisim_toplam_adedi10',str),
                                                                                        ('kisim_toplam_adedi11',str),
                                                                                        ('kisim_toplam_adedi12',str),
                                                                                        ])),
                                                                                    ('zReportIncCost',str),
                                                                                    ('zReportDecCost',str),
                                                                                    ('zReportCashAmount',str),
                                                                                    ('zReportOtherAmount',str),
                                                                                    ('zReportKKAmount',str),

                                                                                    ('zReportCurrAmount',OrderedDict([
                                                                                        ('doviz_toplam_tutar1',str),
                                                                                        ('doviz_toplam_tutar2',str),
                                                                                        ('doviz_toplam_tutar3',str),
                                                                                        ('doviz_toplam_tutar4',str),
                                                                                        ('doviz_toplam_tutar5',str),
                                                                                        ('doviz_toplam_tutar6',str)
                                                                                        ])),
                                                                                    ('zReportCurrName',OrderedDict([
                                                                                        ('doviz_adi1',str),
                                                                                        ('doviz_adi2',str),
                                                                                        ('doviz_adi3',str),
                                                                                        ('doviz_adi4',str),
                                                                                        ('doviz_adi5',str),
                                                                                        ('doviz_adi6',str)
                                                                                        ])),
                                                                                    ('zReportCurrToTL',OrderedDict([
                                                                                        ('doviz_tl_karsiligi1',str),
                                                                                        ('doviz_tl_karsiligi2',str),
                                                                                        ('doviz_tl_karsiligi3',str),
                                                                                        ('doviz_tl_karsiligi4',str),
                                                                                        ('doviz_tl_karsiligi5',str),
                                                                                        ('doviz_tl_karsiligi6',str)
                                                                                        ])),
                                                                                    ('zReportCancelAmount',str),
                                                                                    ('zReportFixAmount',str),
                                                                                    ('zReportBillAmount',str),
                                                                                    ('zReportRestAmount',str),
                                                                                    ('zReportTechNum',str),
                                                                                    ('zReportVATAmount',OrderedDict([
                                                                                        ('vergi_toplam_tutari1',str),
                                                                                        ('vergi_toplam_tutari2',str),
                                                                                        ('vergi_toplam_tutari3',str),
                                                                                        ('vergi_toplam_tutari4',str),
                                                                                        ('vergi_toplam_tutari5',str),
                                                                                        ('vergi_toplam_tutari6',str),
                                                                                        ('vergi_toplam_tutari7',str),
                                                                                        ('vergi_toplam_tutari8',str)
                                                                                        ])),
                                                                                    ('zReportVATRate',OrderedDict([
                                                                                        ('vergi_orani1',str),
                                                                                        ('vergi_orani2',str),
                                                                                        ('vergi_orani3',str),
                                                                                        ('vergi_orani4',str),
                                                                                        ('vergi_orani5',str),
                                                                                        ('vergi_orani6',str),
                                                                                        ('vergi_orani7',str),
                                                                                        ('vergi_orani8',str)
                                                                                        ])),
                                                                                    ('zReportVATCost',OrderedDict([
                                                                                        ('vergi_toplam_kdv1',str),
                                                                                        ('vergi_toplam_kdv2',str),
                                                                                        ('vergi_toplam_kdv3',str),
                                                                                        ('vergi_toplam_kdv4',str),
                                                                                        ('vergi_toplam_kdv5',str),
                                                                                        ('vergi_toplam_kdv6',str),
                                                                                        ('vergi_toplam_kdv7',str),
                                                                                        ('vergi_toplam_kdv8',str)
                                                                                        ])),

                                                                                    ('zReportDailyTotAmount',str),
                                                                                    ('zReportDailyVATAmount',str),
                                                                                    ('zReportAccTotAmount',str),
                                                                                    ('zReportAccVATAmount',str)
                                                                                    ]))])
                                    )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str),('OKCSeriNo',str),('reportDateStart',str),('reportDateEnd',str),('reportZNo',str)])
        )

        dispatcher.register_function('kdEventsData', kdEventsData,
            returns=OrderedDict([('kdEventsDataCevap', OrderedDict(
                                                [('OKCSeriNo', str),('cevapKodu', str), ('cevapAciklama', str),('eventsData',
                                                                      OrderedDict([('eventsSource',str),
                                                                                    ('eventsCrtLevel',str),
                                                                                    ('eventsType',str),
                                                                                    ('eventsTime',str),
                                                                                    ('eventsUserName',str),
                                                                                    ('eventsTermState ',str)]))])
                                    )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str),('OKCSeriNo',str),('reportDateStart',str),('reportDateEnd',str)])
        )

        dispatcher.register_function('kdStatisticsData', kdStatisticsData,
            returns=OrderedDict([('kdStatisticsDataCevap', OrderedDict(
                                                [('OKCSeriNo', str),('cevapKodu', str), ('cevapAciklama', str),('statData',
                                                                      OrderedDict([('statRespNum',str),
                                                                                    ('statFailNum',str),
                                                                                    ('statEthConn',str),
                                                                                    ('statDialConn',str),
                                                                                    ('statGPRSConn',str),
                                                                                    ('statGSMConn ',str),
                                                                                    ('statEthFail',str),
                                                                                    ('statDialFail',str),
                                                                                    ('statGPRSFail',str),
                                                                                    ('statGSMFail',str)]))])
                                    )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str),('OKCSeriNo',str),('reportDateStart',str),('reportDateEnd',str)])
        )

        if http.request.httprequest.method == "POST":


            time_request = datetime.datetime.today()
            response = http.request.make_response(dispatcher.dispatch(http.request.httprequest.data))
            time_response = datetime.datetime.today()
            logging_var={'serviceName':'katma_deger_entegrasyonu','operationName':'',
                         'requestTime':time_request,'responseTime':time_response,
                         'requestData':http.request.httprequest.data,'responseData':response.data
                         }
            logging_obj = http.request.env['info_tsm.es_logs']
            logging_obj.sudo().create(logging_var)

        else:
            response = http.request.make_response(dispatcher.wsdl())

        response.headers['Content-Type'] = 'text/xml'
        return response

    @http.route('/ymesintegration', auth='public',csrf=False)
    def service_index(self, **kw):

        def get_partner( isyeriTCKN, isyeriVergiNo ):
            partner_obj=http.request.env['res.partner']
            partner_sahis = []
            partner_ser   = []

            if isyeriVergiNo != None:
                partner_ser=partner_obj.sudo().search([('taxNumber','=',isyeriVergiNo)])
            if isyeriTCKN != None:
                partner_sahis=partner_obj.sudo().search([('tckn','=',isyeriTCKN)])

            if len(partner_sahis)>0:
                partner =  map( lambda x:x.id, partner_sahis )
            elif len(partner_ser)>0:
                partner =  map( lambda x:x.id, partner_ser )
            else:
                partner = None

            return partner

        def get_NextUniqueIslemIdByCihaz ( cihaz_obj, esislemid ):
            term_kodu = cihaz_obj.name
            term_id   = cihaz_obj.id
            params    = gib_parameters( term_id )
            gib_formatter_obj         = gib_formatter()
            s                         = gibSocket(tls=True)
            parameters         = dict (term_kodu     = gib_formatter_obj.str_to_ascii(term_kodu , hex_=True ),
                                       islem_sira_no = '000001',
                                       term_date     = params.date,
                                       term_time     = params.time_,
                                       esislemid     = gib_formatter_obj.eksik_byte_tamamlamaca ( esislemid, 20 ))
            header             = '''6000010000%(term_kodu)sFF8B7639DF0225DF82040C%(term_kodu)sDF820803%(islem_sira_no)sDF820903%(term_date)sDF820A03%(term_time)sDF550FDFf0180A%(esislemid)s''' %( parameters )
            # print header
            final_message      =  gib_formatter_obj.uzunluk_lrc_final_send_data_hesaplamaca( header )
            s.connect(gib_formatter.TSM_SERVER_IP, gib_formatter.TSM_SERVER_PORT )
            donen_es_islem_no = None
            unique_id         = None
            try:
                s.gibsend( final_message.decode( 'hex' ) )
                #time.sleep(7)
                donen             =  s.gibreceive()
                donen_es_islem_no = donen[:20]
                unique_id         = donen[20:100]
                # print donen_es_islem_no , unique_id
                if str(donen_es_islem_no) == str(parameters[ 'esislemid' ]) :
                    # print 'Eşleştieaaaaa'
                    return unique_id
                else: raise 'Control Error Es Islem Id'
            except:
                e = sys.exc_info()
                # print e
            return False

        def get_cihaz_byES( esCihazId ):
            cihaz_obj = http.request.env["stock.production.lot"]
            cihaz     = cihaz_obj.sudo().search([('harici_cihaz_no','=',esCihazId)])
            if len(cihaz) == 1:
                return cihaz[0]
            return False
        def check_auth(kurumKodu,kurumToken):

            firma_obj = http.request.env["info_tsm.harici_firmalar"]
            firma = firma_obj.sudo().search([('kurum_kodu','=',kurumKodu),('kurum_token','=',kurumToken)])
            if len(firma) == 1:
                return firma[0]
            return False

        def okcESIntegrate(kurumKodu,kurumToken,esCihazSeriNo,masterOKCSeriNo,isyeriTCKN,isyeriVergiNo):
            # print kurumKodu, kurumToken,esCihazSeriNo,masterOKCSeriNo,isyeriTCKN,isyeriVergiNo
            if not check_auth(kurumKodu,kurumToken):
                return {'okcESIntegrateResult': {'okcESIntegrateCevap:masterOKCSeriNo':masterOKCSeriNo,
                                                 'okcESIntegrateCevap:cevapKodu':'001',
                                                 'okcESIntegrateCevap:cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'}}

            partner_id_list= get_partner(isyeriTCKN, isyeriVergiNo )
            # print partner_id_list
            if not partner_id_list:
                return {'okcESIntegrateResult': {'okcESIntegrateCevap:masterOKCSeriNo':masterOKCSeriNo,
                                                 'okcESIntegrateCevap:cevapKodu':'001',
                                                 'okcESIntegrateCevap:cevapAciklama':'TCKN/VKN Hatali'}}


            lot_ids=http.request.env['stock.production.lot'].sudo().search([('name','=',masterOKCSeriNo)])
            if len(lot_ids) == 0:
                return {'okcESIntegrateResult':  {'okcESIntegrateCevap:masterOKCSeriNo':masterOKCSeriNo,
                                                 'okcESIntegrateCevap:cevapKodu':'001',
                                                 'okcESIntegrateCevap:cevapAciklama':'Cihaz Tanimli Degil'}}

            lot_id = lot_ids[0]

            if lot_id.lot_state == 'sold' and lot_id.sold_to.id in partner_id_list:
                lot_id.harici_cihaz_no = esCihazSeriNo
                return {'okcESIntegrateResult': {'okcESIntegrateCevap:masterOKCSeriNo':masterOKCSeriNo,
                                                 'okcESIntegrateCevap:cevapKodu':'000',
                                                 'seriNoDogrulamaCevap:cevapAciklama':'Cihaz Firmaya Ait, Eslesme Kaydi Basarili'}}
            else:
                return {'okcESIntegrateResult': {'okcESIntegrateCevap:masterOKCSeriNo':masterOKCSeriNo,
                                                 'okcESIntegrateCevap:cevapKodu':'001',
                                                 'okcESIntegrateCevap:cevapAciklama':'TCKN/VKN Hatali'}}

        def okcIslemNo(kurumKodu,kurumToken,esCihazSeriNo,esIslemID):
            # print kurumKodu,kurumToken,esCihazSeriNo,esIslemID
            if not check_auth(kurumKodu,kurumToken):
                return {'okcIslemNoResult': {'okcIslemNoCevap:esCihazSeriNo':esCihazSeriNo,
                                                 'okcIslemNoCevap:cevapKodu':'001',
                                                 'okcIslemNoCevap:cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'}}

            cihaz_control = get_cihaz_byES(esCihazSeriNo )
            # print cihaz_control
            if not cihaz_control:
                return {'okcIslemNoResult': {'okcIslemNoCevap:esCihazSeriNo':esCihazSeriNo,
                                                 'okcIslemNoCevap:cevapKodu':'003',
                                                 'okcIslemNoCevap:cevapAciklama':'Eslesme Yapilmamis!!!'}}


            unique_islem_id = get_NextUniqueIslemIdByCihaz( cihaz_control, esIslemID )
            if  not unique_islem_id:
                return {'okcIslemNoResult':  {'okcIslemNoCevap:esCihazSeriNo':esCihazSeriNo,
                                                 'okcIslemNoCevap:cevapKodu':'001',
                                                 'okcIslemNoCevap:cevapAciklama':'Unique Key Bulunamadi'}}


            return {'okcIslemNoResult': {'okcIslemNoCevap:esCihazSeriNo':esCihazSeriNo,
                                                 'okcIslemNoCevap:cevapKodu':'000',
                                                 'okcIslemNoCevap:cevapAciklama':'islem Basarili',
                                                 'okcIslemNoCevap:esIslemID':esIslemID,
                                                 'okcIslemNoCevap:okcUniqueID':str(unique_islem_id)}}

        def okcFisBilgisi(kurumKodu,kurumToken,esCihazSeriNo,satisTipi,esIslemID,kalemVerisi):
            # print kurumKodu,kurumToken,esCihazSeriNo,esIslemID
            if not check_auth(kurumKodu,kurumToken):
                return {'okcFisBilgisiResult': {'okcFisBilgisiCevap:esCihazSeriNo':esCihazSeriNo,
                                                 'okcFisBilgisiCevap:cevapKodu':'001',
                                                 'okcFisBilgisiCevap:cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'}}

            cihaz_control = get_cihaz_byES(esCihazSeriNo )
            # print cihaz_control
            if not cihaz_control:
                return {'okcFisBilgisiResult': {'okcFisBilgisiCevap:esCihazSeriNo':esCihazSeriNo,
                                                 'okcFisBilgisiCevap:cevapKodu':'003',
                                                 'okcFisBilgisiCevap:cevapAciklama':'Eslesme Yapilmamis!!!'}}


            data={'kurum_kodu':kurumKodu,
                  'kurum_token':kurumToken,
                  'es_cihaz_seri_no':esCihazSeriNo,
                  'satis_tipi':satisTipi,
                  'es_islem_id':esIslemID,
                  'kalem_verisi':kalemVerisi
                  }
            new_fis = http.request.env['info_tsm.harici_sistemler_okc_fis_bilgisi'].sudo().create(data)

            return {'okcFisBilgisiResult': {'okcFisBilgisiCevap:esCihazSeriNo':esCihazSeriNo,
                                                 'okcFisBilgisiCevap:cevapKodu':'000',
                                                 'okcFisBilgisiCevap:cevapAciklama':'islem Basarili',
                                                 'okcFisBilgisiCevap:esIslemID':esIslemID
                                                 }}

        def okcFisOdemeDurumSorgulama ( kurumKodu,kurumToken,esIslemID):
            # print kurumKodu,kurumToken,esIslemID
            esCihazSeriNo = 1914
            if not check_auth(kurumKodu,kurumToken):
                return {'okcFisOdemeDurumSorgulamaResult': {'okcFisOdemeDurumSorgulamaCevap:esCihazSeriNo':esCihazSeriNo,
                                                 'okcFisOdemeDurumSorgulamaCevap:cevapKodu':'001',
                                                 'okcFisOdemeDurumSorgulamaCevap:cevapAciklama':'Gecersiz Kurum Kodu ve/veya Token'}}


            return {'okcFisOdemeDurumSorgulamaResult': {'okcFisOdemeDurumSorgulamaCevap:esCihazSeriNo':esCihazSeriNo,
                                                 'okcFisOdemeDurumSorgulamaCevap:cevapKodu':'000',
                                                 'okcFisOdemeDurumSorgulamaCevap:cevapAciklama':'islem basarili (Sabit Demo Veri)',
                                                 'okcFisOdemeDurumSorgulamaCevap:fisOdemeDurumu':'00',
                                                 'okcFisOdemeDurumSorgulamaCevap:satisTipi':'00',
                                                 'okcFisOdemeDurumSorgulamaCevap:nakitOdemeMiktar':'35.25',
                                                 'okcFisOdemeDurumSorgulamaCevap:kkOdemeMiktar':'00',
                                                 'okcFisOdemeDurumSorgulamaCevap:cariOdemeMiktar':'00'
                                                 }}


        param = http.request.env["ir.config_parameter"]

        dispatcher = SoapDispatcher(
            'soap_service',
            location = param.get_param( "bkm.ws_location", default='http://localhost:8069') + '/esintegration/',
            action = param.get_param( "bkm.ws_location", default='http://localhost:8069') + '/esintegration/',
            #namespace = "http://esintegration.infoteks.com.tr/",
            #prefix='tns',
            ns = False)

        # register func

        dispatcher.register_function('okcESIntegrate', okcESIntegrate,
            returns=OrderedDict([('okcESIntegrateResult', OrderedDict(
                                                [('masterOKCSeriNo', str),('cevapKodu', str), ('cevapAciklama', str)])
                                    )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str),('esCihazSeriNo',str),('masterOKCSeriNo',str),('isyeriTCKN',str),('isyeriVergiNo',str)])
                                     )

        dispatcher.register_function('okcIslemNo', okcIslemNo,
            returns=OrderedDict([('okcIslemNoResult', OrderedDict(
                                                [('esCihazSeriNo', str),('cevapKodu', str), ('cevapAciklama', str), ('esIslemID', str), ('okcUniqueID', str)])
                                    )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str),('esCihazSeriNo',str),('esIslemID',str)])
                                     )

        dispatcher.register_function('okcFisBilgisi', okcFisBilgisi,
            returns=OrderedDict([('okcFisBilgisiResult', OrderedDict(
                                                [('esCihazSeriNo', str),('cevapKodu', str), ('cevapAciklama', str), ('esIslemID', str)])
                                    )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str),('esCihazSeriNo',str),('satisTipi',str),('esIslemID',str),('kalemVerisi',str)])
                                     )

        dispatcher.register_function('okcFisOdemeDurumSorgulama', okcFisOdemeDurumSorgulama,
            returns=OrderedDict([('okcFisOdemeDurumSorgulamaResult', OrderedDict(
                                                [('esCihazSeriNo', str),('cevapKodu', str), ('cevapAciklama', str), ('fisOdemeDurumu', str), ('satisTipi', str),
                                                    ('nakitOdemeMiktar', str),('kkOdemeMiktar', str), ('cariOdemeMiktar', str)])
                                    )]),
            args=OrderedDict([('kurumKodu',str),('kurumToken',str),('esIslemID',str)])
                                     )

        if http.request.httprequest.method == "POST":


            time_request = datetime.datetime.today()
            response = http.request.make_response(dispatcher.dispatch(http.request.httprequest.data))
            time_response = datetime.datetime.today()
            logging_var={'serviceName':'es_harici_sistemler','operationName':'',
                         'requestTime':time_request,'responseTime':time_response,
                         'requestData':http.request.httprequest.data,'responseData':response.data
                         }
            logging_obj = http.request.env['info_tsm.es_logs']
            logging_obj.sudo().create(logging_var)

        else:
            response = http.request.make_response(dispatcher.wsdl())

        response.headers['Content-Type'] = 'text/xml'
        return response

    @http.route('/mobileapp', auth='public',csrf=False)
    def service_index_service_user_info(self, **kw):
        def generate_new_pass():
            raw_pass = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(6))
            return raw_pass

        def get_card_by_no(kart_no):
            kart_no     = EncodeAES (cipher,kart_no)
            kart        = http.request.env['stock.production.lot'].sudo().search([('seri_no', '=', kart_no)])
            if len(kart) > 0:
                return kart[0]
            else:
                return False

        def check_card( guid ):
            card_obj = http.request.env["stock.production.lot"].sudo().search([('guid','=',guid)])

            if len( card_obj ) > 0:
                return card_obj[0]
            else:
                #guid =  guid[0:4] + ' ' + guid[4:8] + ' ' + guid[8:12] + ' '  + guid[12:16]
                seri_no_comp = get_card_by_no( guid )
                if seri_no_comp:
                    return seri_no_comp
                return False

        def check_token(kurumKodu,kurumToken):
            param = http.request.env["ir.config_parameter"].sudo()
            return param.get_param( "mobileapp.kurum_kodu", default=None) == kurumKodu and param.get_param( "mobileapp.kurum_token", default=None) == kurumToken

        def close_cr( cr ):

            if cr and not cr.closed:
                cr.close()

        def check_auth(user,passwd,db_name=False):
            new_cr =False
            if not db_name:
                db_name  = http.request.env.cr.dbname
                user_obj = http.request.env["res.users"].sudo()

            else:
                reg = modules.registry.Registry.new(db_name)
                new_cr =  reg.cursor()
                # if it's a copy of a database, force generation of a new dbuuid
                new_env = api.Environment(new_cr, SUPERUSER_ID, {})
                new_env['ir.config_parameter'].init(force=True)
                user_obj = new_env["res.users"].sudo()

            if db_name and user and passwd:
                try:
                    user_id = user_obj._login(db_name, user, passwd)
                except:
                    close_cr( new_cr )
                    user_id = False

                if user_id:
                    return user_obj.browse([user_id]), new_cr
            close_cr( new_cr )
            return False,False

        def check_auth_for_reset(user):

            user_obj = http.request.env["res.users"].sudo()
            user_db  = user_obj.search([('login','=',user)])
            
            if len(user_db) > 0:
                return user_db[-1]
            else:
                return False
            
        def getCityDistirctId(plaka = None,ilce_kod = None):
            res = {}
            if plaka:
                il_obj = http.request.env["info_extensions.iller"].sudo().search([('plaka','=',plaka)])
                if len(il_obj) > 0:
                    res['il'] = il_obj[0].id
                    if ilce_kod:
                        ilce_obj = http.request.env["info_extensions.ilceler"].sudo().search([('ilce_kodu','=',ilce_kod),
                                                                                              ('il_id','=',il_obj[0].id)])
                        if len(ilce_obj) > 0:
                            res['ilce'] = ilce_obj[0].id
            return res

        def userUnique(userMail):
            usr  = http.request.env['res.users'].sudo().search([('login','=',userMail)])
            if len( usr) > 0:
                return False
            return True

        def send_auth_login_sms(telNum,message):

            sms    = http.request.env.get('info_restaurant_base.sms').sudo()
            result = sms.create({'metin':message,'telefon':telNum}).send_sms()
            return result

        def get_cursor_by_okc(okcSerino):

            ###### Test için Restoran Db set edildi, prodda aşağıdaki satır açılacak

            #db_name = getPartnerFromOkc( okcSerino, http.request.env)
            db_name='restoran'
            reg     = modules.registry.Registry.new(db_name)
            new_cr  = reg.cursor()

            return new_cr

        def adisyonDetay(serviceCode,serviceToken,okcSeriNo,adisyonNo):
            checkToken = check_token(serviceCode, serviceToken)
            if not checkToken:
                return {
                    'adisyonDetayResult': {'resultCode': '-1', 'resultDef': 'Gecersiz Servis Kodu ve/veya Token'}}

            cr = get_cursor_by_okc( okcSeriNo )

            like_q = " where order_ref = '{}'".format(adisyonNo)
            sqls = 'select * from pos_sync_order {} '.format(like_q)
            cr.execute(sqls)

            orders = cr.dictfetchall()
            return_list = []
            cnt = 0
            for o in orders:
                pos_order = json.loads( o.get('pos_order') ).get('order')
                for line in pos_order.get('lines'):
                    cnt += 1
                    detail  = line[2]
                    cr.execute( """select t1.name,(case when t2.tsm_code is null then 0 else t2.tsm_code end) as 
                                tsm_code from product_template t1 inner join uom_uom t2 on t1.uom_id = t2.id where t1.id = %s"""%detail.get('product_id') )
                    product = cr.fetchone()
                    tax = 0
                    if detail.get('tax_ids')[0][2]:
                        cr.execute("select (case when amount = 18::numeric then 3 else 2 end) as tax from account_tax where id = %s"%detail.get('tax_ids')[0][2][0])
                        tax = cr.fetchone()
                        tax = tax[0]
                    return_list.append({
                            'urun_adi':product[0],
                            'birim_tipi':product[1],
                            'miktar':detail.get('qty'),
                            'indirim':0,
                            'artirim':0,
                            'kdv_id':tax,
                            'tutar':str('%.2f'% detail.get('price_subtotal_incl'))
                        }
                    )
            cr.close()
            return {
                'adisyonDetayResult': {'resultCode': [cnt], 'resultDef': return_list}}

        def adisyonListesi(serviceCode,serviceToken,okcSeriNo,katNo=0,masaNo=0,limit=40,offset=0):
            checkToken = check_token(serviceCode, serviceToken)
            if not checkToken:
                return {
                    'adisyonListesiResult': {'resultCode': '-1', 'resultDef': 'Gecersiz Servis Kodu ve/veya Token'}}

            cr = get_cursor_by_okc(okcSeriNo)
            like_q = ' where 1 = 1 '
            if katNo and katNo != 'KAT0':
                like_q += " AND pos_order ilike '%{}%'".format(katNo)
            if masaNo and masaNo != 'Masa 0':
                like_q += " AND pos_order ilike '%{}%'".format(masaNo)

            sqls = 'select * from pos_sync_order {} order by id desc limit {} offset {} '.format(like_q, limit, offset)
            cr.execute(sqls)
            orders = cr.dictfetchall()

            return_list = []
            for o in orders:
                pos_order = json.loads(o.get('pos_order')).get('order')
                return_list.append({
                    'id':o.get('id'),
                    'kat': pos_order.get('floor') and pos_order.get('floor').replace('KAT','') or 1,
                    'masa': pos_order.get('table') and pos_order.get('table').replace('Masa ','') or 0,
                    'adisyon': pos_order.get('sequence_number'),
                    'tutar': str(round(pos_order.get('amount_total'),2)),
                    'adisyon_id': o.get('order_ref')}
                )
            cr.execute('select count(*) from pos_sync_order {}'.format(like_q))
            kayitSayisi = cr.fetchone()[0]
            return {
                'adisyonListesiResult': {'resultCode': [str(kayitSayisi)], 'resultDef': return_list}}

        def adisyonOdeme(serviceCode,serviceToken,okcSeriNo,adisyonNo,odemeLines,faturaTc=False,faturaTarihi=False,faturaNo=False):
            checkToken = check_token(serviceCode, serviceToken)
            if not checkToken:
                return {
                    'adisyonOdemeResult': {'resultCode': '-1', 'resultDef': 'Gecersiz Servis Kodu ve/veya Token'}}

            cr  = get_cursor_by_okc(okcSeriNo)
            new_env = api.Environment(cr, SUPERUSER_ID, {})
            stids = odemeLines.split(";")
            statement_ids = []

            like_q = " where order_ref = '{}'".format(adisyonNo)
            sqls = 'select * from pos_sync_order {} '.format(like_q)
            cr.execute(sqls)

            data = {'id': adisyonNo}

            order = cr.dictfetchall()[0]
            order_json = json.loads(order.get('pos_order'))
            pos_order = order_json.get('order')
            action =  'remove_order'
            for s in stids:
                if s:
                    odeme     = s.split('|')
                    odemeTipi = odeme[0]
                    journal_id = new_env['account.journal'].sudo().search([('tsm_code','=',int(odemeTipi))]) or new_env['account.journal'].sudo().search([('tsm_code','=',0)])
                    cr.execute("select id from account_bank_statement where journal_id = {} and state = 'open' and pos_session_id = {}".format( journal_id[-1].id,pos_order.get('pos_session_id')))
                    statement_id = cr.fetchone()[0]
                    acquirer  = odeme[1]
                    tutar     = odeme[2]

                    statement = {'name':datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                 'statement_id':statement_id,
                                 'journal_id':journal_id[-1].id,
                                 'account_id':journal_id[-1].default_credit_account_id.id,
                                 'amount':float(tutar),
                                 }
                    statement_ids.append([0,0,statement])

            pos_order['statement_ids'] = statement_ids


            data['to_invoice'] = False
            if faturaNo:
                data['to_invoice'] = True
                if faturaTc:
                    cr.execute("select id from res_partner where vat = '{}'".format( faturaTc ))
                    p = cr.fetchone()
                    if p:
                        pos_order['partner_id'] = p[0]
                if not pos_order.get('partner_id'):
                    cr.execute ("insert into res_partner (name,display_name,vat) values ('{}','{}','{}') returning id".format((faturaTc or faturaNo or faturaTarihi),(faturaTc or faturaNo or faturaTarihi),faturaTc))
                    partner_id = cr.fetchone()[0]
                    pos_order['partner_id'] = partner_id
                    cr.commit()

            data['data'] = pos_order

            dataset = [data]
            order_ids = new_env.get('pos.order').create_from_ui( dataset )
            cr.commit()
            if data['to_invoice']:
                fatura = new_env.get('pos.order').sudo().browse(order_ids).invoice_id
                fatura.date_invoice = faturaTarihi or datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                if faturaNo:
                    fatura.name = faturaNo

                cr.commit()

            cr.execute("delete from pos_sync_order where order_ref = '{}'".format( adisyonNo))


            '''
            if order_ids:

                sync      = new_env.get('pos.sync.session').sudo().browse([int(pos_order.get('pos_session_id'))])

                order_json.update({'action': action})

                print ( order_json )

                sync.order_operations(order_json)
            '''
            cr.commit()
            return {
                    'adisyonOdeme': {'resultCode': ['1'], 'resultDef': order_ids}}


        def ymmSendSms(serviceCode,serviceToken,phone,message):
            checkToken = check_token(serviceCode, serviceToken)
            if not checkToken:
                return {
                    'ymmSendSmsResult': {'resultCode': '001', 'resultDef': 'Gecersiz Servis Kodu ve/veya Token'}}

            result = send_auth_login_sms(phone,message )
            if result > 0:

                return {
                    'ymmSendSmsResult': {'resultCode': str(result),
                                           'resultDef': 'MutluCell Sms Hatası'}}
            else:
                return {
                    'ymmSendSmsResult': {'resultCode':'000',
                                           'resultDef': 'Sms Gönderimi Başarılı'}}

        def ymmBlokeHarcamaIslem(serviceCode,serviceToken,guid,cuzdanKodu,tutar,islem_id):

            checkToken = check_token(serviceCode, serviceToken)
            if not checkToken:
                return {
                    'TL_Bloke_IslemResponse': {'resultCode': '001', 'resultDef': 'Gecersiz Servis Kodu ve/veya Token'}}

            kart = check_card(guid)
            if not kart:
                return {'resultCode': '004', 'resultDef': u'Gecersiz Guid / Kart Bulunamadı'}

            islem = TL_Bloke_Islem(obj=kart, tutar=tutar, cuzdan=cuzdanKodu, islem_id=islem_id)

            if islem.Sonuc > 0:
                http.request.env['info_kart.mobil_odeme'].sudo().create({
                    'lot_id': kart.id,
                    'tutar': tutar,
                    'cuzdan_kodu': cuzdanKodu,
                    'islem_tipi': int(islem_id),
                    'status': 'done',
                    'status_str': islem.Sonuc_Str
                })
                return {'resultCode': '000', 'resultDef': islem.Sonuc_Str}
            else:
                http.request.env['info_kart.mobil_odeme'].sudo().create({
                    'lot_id': kart.id,
                    'tutar': tutar,
                    'cuzdan_kodu': cuzdanKodu,
                    'islem_tipi': int(islem_id),
                    'status': 'fail',
                    'status_str': islem.Sonuc_Str
                })
                return {'resultCode': '003', 'resultDef': islem.Sonuc_Str}

        def kart_params_up(kart, kw, part):

            kart.sudo().write({'bireysel_partner_id': part.id,
                               'ad_soyad': kw.get('ad') + ' ' + kw.get('soyad'),
                               'tckn': kw.get('tckn'),
                               'cep_tel': kw.get('cep_tel'),
                               'email': kw.get('email'),
                               'adres': kw.get('adres'),
                               'il': kw.get('il'),
                               'ilce': kw.get('ilce'),
                               'dogum_tar': kw.get('dogum_tar'),
                               'lot_state': 'sold2'})

        def create_bireysel_musteri(kw, kart,response):

            p = http.request.env['res.partner'].sudo()
            chack_p_by_tckn = p.search([('tckn', '=', kw.get('tckn'))], order='id desc', limit=1)
            if chack_p_by_tckn:
                chack_p_by_tckn.turkpara_basvuru_id = response.Basvuru_ID
                kart_params_up(kart, kw, chack_p_by_tckn)
                return 1, chack_p_by_tckn.id

            d = dict(name=kw.get('ad') + ' ' + kw.get('soyad'),
                     street=kw.get('adres'),
                     city_combo=kw.get('il'),
                     ilce=kw.get('ilce'),
                     tckn=kw.get('tckn'),
                     email=kw.get('email'),
                     gsm_no=kw.get('cep_tel'),
                     dogum_tar=kw.get('dogum_tar'),
                     turkpara_basvuru_id=response.Basvuru_ID
                     )

            context = {'default_customer': 1,
                       'default_dealer': 0,
                       'default_esnaf': 0,
                       'default_bireysel_musteri': 1,
                       'default_is_company': 0,
                       'default_state': 'predraft',
                       'default_order': 'id asc',
                       }
            try:
                part = p.with_context(context).create(d)
                p1   = 1, part.id
                self.kart_params_up(kart, kw, part)
                return p1
            except:
                return 0, False

        def ymmKisisellestir(serviceCode,serviceToken,userName,
                              userSurName,
                              userMail,
                              userPhone,
                              userCardNo,
                              userTckn,
                              userBdate,
                              userAddressText,
                              userAddressDistrictCode,
                              userAddresscityCode,
                             ):

            checkToken = check_token(serviceCode, serviceToken)
            if not checkToken:
                return {
                    'ymmKisisellestirResult': {'resultCode': '001', 'resultDef': 'Gecersiz Servis Kodu ve/veya Token'}}
            res = getCityDistirctId(userAddresscityCode, userAddressDistrictCode)
            if not res.get('il'):
                return {'ymmKisisellestirResult': {'resultCode': '010',
                                                  'resultDef': 'userAddresscityCode (Sehir Kodu) hatali'}}
            if not res.get('ilce'):
                return {'ymmKisisellestirResult': {'resultCode': '011',
                                                  'resultDef': 'userAddressDistrictCode (Ilce Kodu) hatali'}}
            kart = get_card_by_no( userCardNo )
            if not kart:
                return {'resultCode':'002','resultDef':u'Gecersiz Guid / Kart Bulunamadı'}

            kw = {'tckn':userTckn,
                  'ad':userName,
                  'soyad':userSurName,
                  'cep_tel':userPhone,
                  'email':userMail,
                  'dogum_tar':userBdate,
                  'adres':userAddressText,
                  'il':res.get('il'),
                  'ilce':res.get('ilce')}

            response = Kisisellestirme( kart,kw )
            if int(response.Sonuc) != 1 and not hasattr(response, 'Basvuru_ID'):
                kart.sudo().write({'ws_last_message': response.Sonuc_Str.replace('_', ' ')})
                return {'resultCode': '003', 'resultDef': response.Sonuc_Str}
            else:
                partner_id = create_bireysel_musteri(kw, kart, response)
                if partner_id[0] != 1:
                    kart.ws_last_message = partner_id[1]
                    return {'resultCode': '003', 'resultDef': 'Kişiselleştirme Başarılı Ancak İşlem Sunucuya İşlenemedi !!!'}
                else:
                    return {'resultCode': '000',
                            'resultDef': response.Sonuc_Str}

        def ymmRegisterUser(serviceCode,serviceToken,userName,
                              userSurName,
                              userMail,
                              userPhone,
                              userAddressText,
                              userAddressDistrictCode,
                              userAddresscityCode,
                              userCardNo,
                              userPass,
                              userTckn,
                              userBdate,):
            
            checkToken = check_token( serviceCode,serviceToken)
            if not checkToken:
                return {'ymmRegisterUserResult': {'resultCode':'001','resultDef':'Gecersiz Servis Kodu ve/veya Token'}}
            
            userObj = http.request.env[('res.users')]
            res = getCityDistirctId( userAddresscityCode,userAddressDistrictCode)
            if not res.get('il'):
                return {'ymmRegisterUserResult': {'resultCode':'010','resultDef':'userAddresscityCode (Sehir Kodu) hatali'}}
            if not res.get('ilce'):
                return {'ymmRegisterUserResult': {'resultCode':'011','resultDef':'userAddressDistrictCode (Ilce Kodu) hatali'}}
            cr_dict = dict(login        = userMail,
                           mobile       = userPhone,
                           name         = userName + ' ' + userSurName,
                           street       = userAddressText,
                           city_combo   = res['il'],
                           ilce         = res['ilce'],
                           password     = userPass,
                           user_card_no = userCardNo,
                           is_company   = False,
                           customer     = False,
                           email        = userMail,
                           tckn         = userTckn,
                           bdate        = userBdate)
            
            if not userUnique( userMail ):
                return {'ymmRegisterUserResult': {'resultCode':'012','resultDef':'userMail (e-posta adresi) zaten kayitli'}}
            
            userObj.sudo().create( cr_dict )
            
            return {'ymmRegisterUserResult': {'resultCode':'000','resultDef':'Kullanici Kaydedildi'}}
        def ymmGetUserInfo(serviceCode,serviceToken,userMail,userPass):
            checkToken = check_token( serviceCode,serviceToken)
            if not checkToken:
                return {'resultCode':'001','resultDef':'Gecersiz Servis Kodu ve/veya Token'}
            user,cr  = check_auth(userMail,userPass)
            if not user:
                return  {'userStatus':False,'resultCode':'002','resultDef':'Kullanici Bulunamadi'}
            else:
                user       = user.sudo()
                p          = user.partner_id
                kartlar    = p.bireysel_kartlar
                ks         = []
                for k in kartlar:
                    bakiye(k)
                    card_data = {}
                    card_data['guid']         = k.guid
                    card_data['cardNo']       = k.name
                    d = {}
                    d['detail']   = [{'credit':k.serbest_limit,'creditName':'Serbest Bakiye'},
                                                 {'credit':k.available_limit,'creditName':'Yemek Bakiye'},
                                                 {'credit':k.gift_limit,'creditName':'Hediye Bakiye'},
                                                ]
                    
                    card_data['cardDetail'] = d
                    ks.append( card_data )
                if not ks:
                    ks = 'false'
                else:
                    ks = {'cards':ks}
                return{'userStatus':True,
                        'resultCode':'000',
                        'resultDef':'Sorgu Basarili',
                        'userDispName':user.name,
                        'userAddressText':user.street,
                        'userAddressDistrictCode':user.ilce.ilce_kodu,
                        'userAddresscityCode':user.city_combo.plaka,
                        'userMail':user.login,
                        'userPhone':user.mobile,
                        'userTckn':user.tckn,
                        'userBdate':user.bdate,
                        'userCard':ks
                        }

        def ymmUserInfoChange(userMail,
                              userPass,
                              serviceCode,
                              serviceToken,
                              newPass=None,
                              newAddressText=None,
                              newAddressDistrictCode=None,
                              newAddresscityCode=None,
                              newPhone=None,
                              newMail=None,
                              newTckn=None,
                              newBdate=None):
            checkToken = check_token( serviceCode,serviceToken)
            if not checkToken:
                return  {'resultCode':'001','resultDef':'Gecersiz Servis Kodu ve/veya Token'}
            user,cr  = check_auth(userMail,userPass)
            if not user:
                return {'userStatus':False,'resultCode':'002','resultDef':'Kullanici Bulunamadi'}
            user = user.sudo()
            if newMail:
                if not userUnique( newMail ):
                    return  {'resultCode':'012','resultDef':'newMail (e-posta adresi) zaten kayitli'}
                user.login = newMail
            
            res = getCityDistirctId( newAddresscityCode,newAddressDistrictCode)

            if newPass:
                user.sudo( user.id ).change_password(userPass,newPass )
            if newAddressText:
                user.use_parent_address = False
                user.street = newAddressText
            if newAddressDistrictCode:
                if res.has_key('ilce'):
                    user.ilce = res['ilce']
            if newAddresscityCode:
                if res.has_key('il'):
                    user.city_combo = res['il']
            if newPhone:
                user.mobile = newPhone
            if newTckn:
                user.tckn = newTckn
            if newBdate:
                user.bdate = newBdate
            return { 'userStatus':True,
                    'resultCode':'000',
                    'resultDef':'Kayit Basarili',
                    'userDispName':user.name,
                    'userAddressDistrictCode':user.ilce.ilce_kodu,
                    'userAddresscityCode':user.city_combo.plaka,
                    'userMail':user.login,
                    'userPhone':user.mobile,
                    'userTckn':user.tckn,
                    'userBdate':user.bdate
                    }

        def ymmUserPassReset(serviceCode,serviceToken,userMail):
            checkToken = check_token( serviceCode,serviceToken)
            if not checkToken:
                return {'ymmUserPassResetResult': {'resultCode':'001','resultDef':'Gecersiz Servis Kodu ve/veya Token'}}
            user = check_auth_for_reset( userMail )
            if not user:
                return {'ymmUserPassResetResult': {'userStatus':False,'resultCode':'002','resultDef':'Kullanici Bulunamadi'}}

            new_pass                 = generate_new_pass()
            user.sudo().password     = new_pass
            user.send_mail( new_pass )
            return  {'ymmUserPassResetResult': {'userStatus':True,
                                                 'resultCode':'000',
                                                 'resultDef':'PassWord Reset Basarili'
                                                 }}

        def ymmCampaignInfo(serviceCode,serviceToken,campCityCode,campDistrictCode):
            checkToken = check_token( serviceCode,serviceToken)
            if not checkToken:
                return {'ymmCampaignInfoResult': {'resultCode':'001','resultDef':'Gecersiz Servis Kodu ve/veya Token'}}
            camp_do = [('campaign','!=',False)]
            
            if campDistrictCode and not campCityCode:
                return {'ymmRegisterUserResult': {'resultCode':'012','resultDef':'Sadece Ilce Kodu ile Sorgu Yapılamaz. Il kodu da gonderiniz.'}}
            
            if campCityCode:
                res = getCityDistirctId( plaka = campCityCode )
                
                if not res.get('il'):
                    return {'ymmRegisterUserResult': {'resultCode':'010','resultDef':'userAddresscityCode (Sehir Kodu) hatali'}}
                
                camp_do.extend([('city_combo','=',res.get('il'))])
                if campDistrictCode:
                    res = getCityDistirctId( campCityCode, campDistrictCode )
                    
                    if not res.get('ilce'):
                        return {'ymmRegisterUserResult': {'resultCode':'011','resultDef':'userAddressDistrictCode (Ilce Kodu) hatali'}}
                    camp_do.extend([('ilce','=',res.get('ilce'))])
                    
            camp_partners    = http.request.env['res.partner'].sudo().search(camp_do)
            
            data = []
            for p in camp_partners:
                if len(p.campaign) == 0:
                    continue
                firma = {}
                firma['FirmItem'] =  dict(campCityCode       = p.city_combo.plaka,
                                 campDistrictCode   = p.ilce.ilce_kodu,
                                 campFirmName       = p.firma_unvani,
                                 campFirmAddress    = u'%s %s %s %s'%(p.street or '', p.street2 or '', p.ilce.name, p.city_combo.name),
                                 )
                p_campaigns = []
                path   = "/web/binary/image?model=yemekmatik.campaign&field=campaign_img&id=%s"
                for c in p.campaign:
                    camp_data = {}
                    camp_data['CItem'] = dict(
                        campMessage  = c.campaing_message,
                        campImage    = path  % c.id,
                        campShowcase = c.vitrin
                        )
                    if c.campaing_discount_type == 'p':
                        camp_data['CItem']['campDiscountRate']   = c.campaing_discount
                        camp_data['CItem']['campDiscountAmount'] = 0.0

                    elif c.campaing_discount_type == 'a':
                        camp_data['CItem']['campDiscountAmount'] = c.campaing_discount
                        camp_data['CItem']['campDiscountRate']   = 0.0
                    p_campaigns.append( camp_data )
                    
                firma['FirmItem']['CampaignItem'] = p_campaigns
                data.append( firma )
            
            return  {'ymmCampaignInfoResult': {'ymmCampaignFirmItem':data,
                                                'resultCode':'000',
                                                'resultDef':'Sorgu Basarili'
                                                 }}

        def ymmDiscountInfo(serviceCode,serviceToken,campBarcodeData,userCardNo):
            checkToken = check_token( serviceCode,serviceToken)
            if not checkToken:
                return {'ymmDiscountInfoResult': {'resultCode':'001','resultDef':'Gecersiz Servis Kodu ve/veya Token'}}
            camp_obj = http.request.env['yemekmatik.campaign'].sudo().search([('campaign_barcode','=',campBarcodeData)])
            
            camp_data ={}
            if len( camp_obj ) > 0:
                camp_obj = camp_obj[-1]
                camp_data = dict(
                    campFirmName=camp_obj.partner_id.name,
                    resultCode  = '000',
                    resultDef   = 'Sorgu Basarili',
                    
                    )
                if camp_obj.campaing_discount_type == 'p':
                    camp_data['campDiscountRate']  = camp_obj.campaing_discount
                elif camp_obj.campaing_discount_type == 'a':
                    camp_data['campDiscountAmount'] = camp_obj.campaing_discount
                return {'ymmDiscountInfoResult': camp_data }

        def ymmGetCityInfo(serviceCode,serviceToken):
            checkToken = check_token( serviceCode,serviceToken)
            if not checkToken:
                return {'ymmGetCityInfoResult': {'resultCode':'001','resultDef':'Gecersiz Servis Kodu ve/veya Token'}}
            city_obj = http.request.env['info_extensions.iller'].sudo().search([])
            data     = []
            for c in city_obj:
                f = {}
                f['City'] =  dict(cityServerId       = c.id,
                                 cityName               = c.name,
                                 cityCode               = c.plaka,
                                 )
                data.append(f)
            
            return  {'ymmGetCityInfoResult': {'ymmCity':data,
                                                'resultCode':'000',
                                                'resultDef':'Sorgu Basarili'
                                                 }}

        def ymmGetDistrictInfo(serviceCode,serviceToken):
            checkToken = check_token( serviceCode,serviceToken)
            if not checkToken:
                return {'ymmGetDistrictInfoResult': {'resultCode':'001','resultDef':'Gecersiz Servis Kodu ve/veya Token'}}
            dict_obj = http.request.env['info_extensions.ilceler'].sudo().search([])
            data     = []
            for c in dict_obj:
                f = {}
                f['District'] =  dict(districtServerId       = c.id,
                                 districtName                   = c.name,
                                 districtCode                   = c.ilce_kodu,
                                 districtCityCode               = c.il_id.id
                                 )
                data.append(f)
            
            return  {'ymmGetDistrictInfoResult': {'ymmDistrict':data,
                                                'resultCode':'000',
                                                'resultDef':'Sorgu Basarili'
                                                 }}

        def ymmGetEkstre(serviceCode,serviceToken,guid,str_date, stp_date, sayi):
            checkToken = check_token( serviceCode,serviceToken)
            if not checkToken:
                return {'resultCode':'001','resultDef':'Gecersiz Servis Kodu ve/veya Token'}
            
            kart = check_card( guid )
            if not kart:
                return {'resultCode':'002','resultDef':u'Gecersiz Guid / Kart Bulunamadı'}
            
            ekstre = get_extre( kart, db_save=False,str_date=str_date, stp_date=stp_date, sayi=sayi)
            # print ekstre
            
            return ekstre

        def ymmLoginDeterminate(serviceCode,serviceToken,userMail,userPass,userGsm):
            checkToken = check_token(serviceCode, serviceToken)
            if not checkToken:
                return {
                    'ymmLoginDeterminateResult': {'resultCode': '001', 'resultDef': 'Gecersiz Servis Kodu ve/veya Token'}}

            #once restoran mı, uye is yerimi ona bak

            rest = http.request.env.get('info_restaurant_base.default_cre').sudo().search([('login','=',userMail)])
            if userGsm.startswith('0'):
                userGsm = userGsm[1:]

            if len(rest) > 0:
                auth,cr = check_auth(userMail,userPass,rest.db_name)


                if not auth:
                    close_cr(cr)
                    return {'ymmLoginDeterminateResult': {'resultCode':'002','resultDef':'Hatalı Giriş ya da Şifre'}}
                else:

                    mob      = auth.mobile.replace(' ','').replace('-','')
                    if not mob.isalnum:
                        close_cr(cr)
                        return {
                            'ymmLoginDeterminateResult': {'resultCode': '008',
                                                          'resultDef': 'Cep Telefonunuzu Hatalı Yazdınız.'}}
                    loginUrl  = 'https://%s.yemekmatik.com.tr/pos/web' % rest.db_name
                    cariId    = rest.db_name
                    partnerId = rest.basvuru_id.partner_id or 1
                    if float(mob) != float(userGsm):
                        close_cr(cr)
                        return {
                            'ymmLoginDeterminateResult': {'resultCode': '007',
                                                          'resultDef': 'Sistemde Kaydınız Mevcut Ancak Cep Telefonunuz Sistemde Kayıtlı Değil, Yöneticinize Danışınız.'}}
            else:
                #bak bakalım bu herif kullanıcı mı , kullanıcı ise telefon nosu kayıtlı mı.
                auth,cr = check_auth( userMail, userPass)
                if not auth:
                    close_cr(cr)
                    return {'ymmLoginDeterminateResult': {'resultCode': '002', 'resultDef': 'Hatalı Giriş ya da Şifre'}}

                userGsmformatted = userGsm[:3] + '-' + userGsm[3:6] + ' ' + userGsm[6:8] + ' ' +userGsm[8:10]


                p = http.request.env.get('res.partner').sudo().search(['&','|','|',
                                                                   ('gsm_no','=',userGsmformatted),
                                                                   ('phone','=',userGsmformatted),
                                                                   ('gorusulen_kisi_tel', '=',userGsmformatted),
                                                                   ('company_id','=',auth.company_id.id)])
                if p:
                    auth.mobile = userGsm
                    mob = userGsm
                    cariId = p.tckn or p.taxNumber
                    partnerId = p.id
                else:
                    close_cr(cr)
                    return {
                        'ymmLoginDeterminateResult': {'resultCode': '007', 'resultDef': 'Cep Telefonu Sistemde Kayıtlı Değil'}}
                loginUrl = 'https://%s.yemekmatik.com.tr/web' % master_db_name

            if mob and loginUrl:
                code = random.randint(100000, 999999)
                metin = '%s Kodu İle Yemekmatik Sistemlerine Giriş Yapabilirsiniz.' % code
                sms_result = send_auth_login_sms(mob, metin )
                if sms_result > 0:
                    close_cr(cr)
                    return {
                        'ymmLoginDeterminateResult': {'resultCode': str(sms_result),
                                                      'resultDef': 'MutluCell Sms Hatası'}}
                else:
                    close_cr(cr)
                    return {
                        'ymmLoginDeterminateResult': {'resultCode': '000',
                                                      'resultDef': 'Giriş Başarılı',
                                                      'loginUrl': loginUrl,
                                                      'authCode': code,
                                                      'cariId'  : cariId,
                                                      'partnerId':partnerId
                                                      }}
            else:
                close_cr(cr)
                return {
                    'ymmLoginDeterminateResult': {'resultCode': '099',
                                                  'resultDef': 'Sistem Hatası'}}

        def kdHarcamaIslem(serviceCode, serviceToken, guid, cuzdanKodu, tutar, islemId, aciklama, cariId,partnerId,orderId,restoranOdeme):

            checkToken = check_token(serviceCode, serviceToken)
            if not checkToken:
                return {'resultCode': '001', 'resultDef': 'Gecersiz Servis Kodu ve/veya Token'}

            kart = check_card(guid)
            if not kart:
                return {'resultCode': '004', 'resultDef': u'Gecersiz Guid / Kart Bulunamadı'}

            objSaveObj = http.request.env['info_kart.mobil_odeme'].sudo()
            objSave = objSaveObj.search([('islem_id', '=', int(islemId))])
            if objSave:
                objSave.write({
                    'lot_id': kart.id,
                    'aciklama': aciklama,
                    'tutar': tutar,
                    'cuzdan_kodu': cuzdanKodu,
                    'islem_id': int(islemId),
                    'cari_id': cariId,
                    'partner_id': int(partnerId),
                    'status': 'fail',
                    'status_str': ''
                })
            else:
                objSave = objSaveObj.create({
                    'lot_id': kart.id,
                    'aciklama': aciklama,
                    'tutar': tutar,
                    'cuzdan_kodu': cuzdanKodu,
                    'islem_id': int(islemId),
                    'cari_id': cariId,
                    'partner_id': int(partnerId),
                    'status': 'fail',
                    'status_str': ''
                })

            islem = Kd_Harcama_Islem(objSave)
            objSave.status_str = islem.Sonuc_Str

            if int(islem.Sonuc) > 0:
                objSave.status = "done"
                objSave.dekont_id = islem.Dekont_ID
                if restoranOdeme:
                    try:
                        new_cr = modules.registry.Registry.new(cariId).cursor()
                        new_cr.execute("insert into info_restaurant_options_mobil_odeme (order_id) values ('{}')".format(orderId))
                        new_cr.commit()
                        new_cr.close()
                    except:
                        return {'resultCode': '098', 'resultDef': 'Ödeme Alındı ancak Sipariş Kapatılamadı. db_name: %s'%cariId}
                return {'resultCode': '000', 'resultDef': islem.Sonuc_Str}
            else:
                objSave.status = "fail"
                return {'resultCode': '003', 'resultDef': islem.Sonuc_Str}

        def kdHarcamaIptal(serviceCode, serviceToken, guid, islemId):

            checkToken = check_token(serviceCode, serviceToken)
            if not checkToken:
                return {'resultCode': '001', 'resultDef': 'Gecersiz Servis Kodu ve/veya Token'}

            kart = check_card(guid)
            if not kart:
                return {'resultCode': '004', 'resultDef': u'Gecersiz Guid / Kart Bulunamadı'}

            obj = http.request.env['info_kart.mobil_odeme'].sudo().search([('islem_id','=',int(islemId))])

            islem = Kd_Harcama_Iptal(obj)
            obj.status_str = islem.Sonuc_Str

            if int(islem.Sonuc) > 0:
                obj.status = "cancel"
                return {'resultCode': '000', 'resultDef': islem.Sonuc_Str}
            else:
                obj.status = "cancelFail"
                return {'resultCode': '003', 'resultDef': islem.Sonuc_Str}

        def kdUniqKey(serviceCode, serviceToken):

            checkToken = check_token(serviceCode, serviceToken)
            if not checkToken:
                return {'resultCode': '001', 'resultDef': 'Gecersiz Servis Kodu ve/veya Token'}

            seq = http.request.env['ir.sequence'].sudo().next_by_code('info_kart.mobil_odeme')
            return {'resultCode': '000', 'resultDef': seq}

        param = http.request.env["ir.config_parameter"].sudo()

        dispatcher = SoapDispatcher(
            'soap_service',
            location = param.get_param( "web.base.external_url", default='http://localhost:8069') + '/mobileapp/',
            action   = param.get_param( "web.base.external_url", default='http://localhost:8069') + '/mobileapp/',
            namespace = 'ymminfo',
            ns = 'ymminfo')


        # register func
        dispatcher.register_function('ymmLoginDeterminate', ymmLoginDeterminate,
                                     returns=OrderedDict([('ymmLoginDeterminateResult', OrderedDict(
                                         [('resultCode', str),
                                          ('resultDef', str),
                                          ('loginUrl',str),
                                          ('authCode',str),
                                          ('cariId', str),
                                          ('partnerId',str)
                                          ])
                                                           )]),
                                     args=OrderedDict([('serviceCode', str),
                                                       ('serviceToken', str),
                                                       ('userMail', str),
                                                       ('userPass', str),
                                                       ('userGsm', str),
                                                       ])
                                     )

        dispatcher.register_function('ymmRegisterUser', ymmRegisterUser,
            returns=OrderedDict([('ymmRegisterUserResult', OrderedDict(
                                                [('resultCode', str),
                                                 ('resultDef', str)
                                                ])
                                    )]),
            args=OrderedDict([('serviceCode',str),
                              ('serviceToken',str),
                              ('userName',str),
                              ('userSurName',str),
                              ('userMail',str),
                              ('userPhone',str),
                              ('userAddressText',str),
                              ('userAddressDistrictCode',str),
                              ('userAddresscityCode',str),
                              ('userCardNo',str),
                              ('userPass',str),
                              ('userBdate',str),
                              ('userTckn',str)])
                                     )

        dispatcher.register_function('ymmKisisellestir', ymmKisisellestir,
                                     returns=OrderedDict([('ymmKisisellestirResult', OrderedDict(
                                         [('resultCode', str),
                                          ('resultDef', str)
                                          ])
                                                           )]),
                                     args=OrderedDict([('serviceCode', str),
                                                       ('serviceToken', str),
                                                       ('userName', str),
                                                       ('userSurName', str),
                                                       ('userMail', str),
                                                       ('userPhone', str),
                                                       ('userCardNo', str),
                                                       ('userBdate', str),
                                                       ('userTckn', str),
                                                       ('userAddressText', str),
                                                       ('userAddressDistrictCode', str),
                                                       ('userAddresscityCode', str),
                                                       ])
                                     )

        dispatcher.register_function('ymmBlokeHarcamaIslem', ymmBlokeHarcamaIslem,
            returns=OrderedDict([('ymmBlokeHarcamaIslemResult ', OrderedDict(
                                                [('userStatus', bool),
                                                 ('resultCode', str)
                                                    ])
                                    )]),
            args=OrderedDict([('serviceCode',str),
                              ('serviceToken',str),
                              ('guid',str),
                              ('cuzdanKodu',str),
                              ('tutar',str),
                              ('islemTipi',str)])
                                     )

        dispatcher.register_function('ymmGetEkstre', ymmGetEkstre,
                                     returns=OrderedDict([('ymmGetEkstreResult ', OrderedDict(
                                         [('userStatus', bool),
                                          ('resultCode', str),
                                          ('resultDef', str),
                                          ('userDispName', str),
                                          ('userAddressText', str),
                                          ('userAddressDistrictCode', str),
                                          ('userAddresscityCode', str),
                                          ('userMail', str),
                                          ('userPhone', str),
                                          ('userCardNo', str),
                                          ('userCardType', str),
                                          ('userBdate', str),
                                          ('userTckn', str)
                                          ])
                                                           )]),
                                     args=OrderedDict([('serviceCode', str),
                                                       ('serviceToken', str),
                                                       ('guid', str),
                                                       ('str_date', str),
                                                       ('stp_date', str),
                                                       ('sayi', str)])
                                     )

        dispatcher.register_function('ymmGetUserInfo', ymmGetUserInfo,
            returns=OrderedDict([('ymmGetUserInfoResult', OrderedDict(
                                                [('userStatus', bool),
                                                    ('resultCode', str),
                                                    ('resultDef', str),
                                                    ('userDispName', str),
                                                    ('userAddressText',str),
                                                    ('userAddressDistrictCode',str),
                                                    ('userAddresscityCode',str),
                                                    ('userMail',str),
                                                    ('userPhone',str),
                                                    ('userCardNo',str),
                                                    ('userCardType',str),
                                                    ('userBdate',str),
                                                    ('userTckn',str),
                                                    ('userCard',OrderedDict([('guid',str),
                                                                             ('cardNo',str),
                                                                             ('cardDetail',OrderedDict([('credit',float),
                                                                                                        ('creditName',str)])
                                                                              )
                                                                            ]))
                                                    ])
                                    )]),
            args=OrderedDict([('serviceCode',str),
                              ('serviceToken',str),
                              ('userMail',str),
                              ('userPass',str)])
                                     )

        dispatcher.register_function('ymmGetCityInfo', ymmGetCityInfo,
            returns=OrderedDict([('ymmGetCityInfoResult', OrderedDict(
                                                [('ymmCity', OrderedDict(
                                                        [
                                                            ('cityServerId',str),
                                                            ('cityName', str),
                                                            ('cityCode',str)
                                                        ]
                                                    )),
                                                    ('resultCode', str),
                                                    ('resultDef', str),
                                                ])
                                    )]),
            args=OrderedDict([('serviceCode',str),
                              ('serviceToken',str)
                              ])
                                     )

        dispatcher.register_function('ymmGetDistrictInfo', ymmGetDistrictInfo,
            returns=OrderedDict([('ymmGetDistrictInfoResult', OrderedDict(
                                                [('ymmDistrict', OrderedDict([
                                                        ('districtServerId', int),
                                                        ('districtName', str),
                                                        ('districtCode',str),
                                                        ('districtCityCode',int)
                                                        ])),
                                                    ('resultCode', str),
                                                    ('resultDef', str),
                                                    ])
                                    )]),
            args=OrderedDict([('serviceCode',str),
                              ('serviceToken',str)
                              ])
                                     )

        dispatcher.register_function('ymmUserInfoChange', ymmUserInfoChange,
            returns=OrderedDict([('ymmUserInfoChangeResult', OrderedDict(
                                                [('userStatus', bool),
                                                    ('userDispName', str),
                                                    ('userAddressDistrictCode',str),
                                                    ('userAddresscityCode',str),
                                                    ('userMail',str),
                                                    ('userPhone',str),
                                                    ('userBdate',str),
                                                    ('userTckn',str),
                                                    ('resultCode', str),
                                                    ('resultDef', str),])
                                    )]),
            args=OrderedDict([
                                ('serviceCode',str),
                                ('serviceToken',str),
                                ('userMail',str),
                                ('userPass',str),
                                ('newPass',str),
                                ('newAddressText',str),
                                ('newAddressDistrictCode',str),
                                ('newAddresscityCode',str),
                                ('newPhone',str),
                                ('newMail',str),
                                ('newBdate',str),
                                ('newTckn',str),])
                                     )

        dispatcher.register_function('ymmUserPassReset', ymmUserPassReset,
            returns=OrderedDict([('ymmUserPassResetResult', OrderedDict(
                                                [('resultCode', str),
                                                 ('resultDef', str)
                                                ])
                                    )]),
            args=OrderedDict([
                                ('serviceCode',str),
                                ('serviceToken',str),
                                ('userMail',str)])
                                     )

        dispatcher.register_function('ymmCampaignInfo', ymmCampaignInfo,
            returns=OrderedDict([('ymmCampaignInfoResult', OrderedDict(
                                                [('ymmCampaignFirmItem',
                                                    OrderedDict([('campCityCode',str),
                                                                  ('campDistrictCode',str),
                                                                  ('campFirmName',str),
                                                                  ('campFirmAddress',str),
                                                                  ('ymmCampaignListItem',
                                                                   OrderedDict([('campMessage',str),
                                                                                ('campDiscountRate',str),
                                                                                ('campDiscountAmount',str),
                                                                                ('campImage',str),
                                                                                ('campShowcase',bool)
                                                                               ]
                                                                    )
                                                                   )
                                                                  ]
                                                        )
                                                    ),
                                                    ('resultCode',str),
                                                    ('resultDef',str),
                                                ]
                                                )
                                    )]),
            args=OrderedDict([('serviceCode',str),
                                ('serviceToken',str),
                                ('campCityCode',str),
                                ('campDistrictCode',str)
                            ])
                                    )

        dispatcher.register_function('ymmDiscountInfo', ymmDiscountInfo,
            returns=OrderedDict([('ymmDiscountInfoResult', OrderedDict([
                                                                        ('campFirmName',str),
                                                                        ('campDiscountRate',str),
                                                                        ('campDiscountAmount',str),
                                                                        ('resultCode',str),
                                                                        ('resultDef',str),
                                                                    ])
                                )]),
            args=OrderedDict([('serviceCode',str),
                                ('serviceToken',str),
                                ('campBarcodeData',str),
                                ('userCardNo',str)
                            ])
                                    )

        dispatcher.register_function('ymmSendSms', ymmSendSms,
                                     returns=OrderedDict([('ymmSendSmsResult', OrderedDict([
                                         ('resultCode', str),
                                         ('resultDef', str),
                                     ])
                                                           )]),
                                     args=OrderedDict([('serviceCode', str),
                                                       ('serviceToken', str),
                                                       ('phone', str),
                                                       ('message', str)
                                                       ])
                                     )

        dispatcher.register_function('kdHarcamaIslem', kdHarcamaIslem,
            returns=OrderedDict([('kdHarcamaIslemResult ', OrderedDict(
                                                [('userStatus', bool),
                                                 ('resultCode', str)
                                                    ])
                                    )]),
            args=OrderedDict([('serviceCode',str),
                              ('serviceToken',str),
                              ('guid',str),
                              ('cuzdanKodu',str),
                              ('tutar',str),
                              ('islemId',str),
                              ('cariId', str),
                              ('partnerId', str),
                              ('aciklama',str),
                              ('orderId',str),
                              ('restoranOdeme',bool)
                              ])
                                     )

        dispatcher.register_function('kdHarcamaIptal', kdHarcamaIptal,
            returns=OrderedDict([('kdHarcamaIptalResult ', OrderedDict(
                                                [('userStatus', bool),
                                                 ('resultCode', str)
                                                    ])
                                    )]),
            args=OrderedDict([('serviceCode',str),
                              ('serviceToken',str),
                              ('guid',str),
                              ('islemId',str)])
                                     )

        dispatcher.register_function('kdUniqKey', kdUniqKey,
            returns=OrderedDict([('kdUniqKeyResult ', OrderedDict(
                                                [('userStatus', bool),
                                                 ('resultCode', str)
                                                    ])
                                    )]),
            args=OrderedDict([('serviceCode',str),
                              ('serviceToken',str)])
                                     )
        dispatcher.register_function('adisyonListesi', adisyonListesi,
                                     returns=OrderedDict([('adisyonListesiResult', OrderedDict(
                                         [('resultCode', bool),
                                          ('resultDef', str)
                                          ])
                                                           )]),
                                     args=OrderedDict([('serviceCode', str),
                                                       ('serviceToken', str),
                                                       ('okcSeriNo',str),
                                                       ('limit',str),
                                                       ('offset',str),
                                                       ('katNo',str),
                                                       ('masaNo',str)])
                                     )
        dispatcher.register_function('adisyonDetay', adisyonDetay,
                                     returns=OrderedDict([('adisyonDetayResult', OrderedDict(
                                         [('resultCode', bool),
                                          ('resultDef', str)
                                          ])
                                                           )]),
                                     args=OrderedDict([('serviceCode', str),
                                                       ('serviceToken', str),
                                                       ('okcSeriNo', str),
                                                       ('adisyonNo', str)])
                                     )
        dispatcher.register_function('adisyonOdeme', adisyonOdeme,
                                     returns=OrderedDict([('adisyonOdemeResult', OrderedDict(
                                         [('resultCode', bool),
                                          ('resultDef', str)
                                          ])
                                                           )]),
                                     args=OrderedDict([('serviceCode', str),
                                                       ('serviceToken', str),
                                                       ('okcSeriNo', str),
                                                       ('adisyonNo', str),
                                                       ('odemeLines', str),
                                                       ('faturaTc', str),
                                                       ('faturaTarihi', str),
                                                       ('faturaNo', str)])
                                     )

        if http.request.httprequest.method == "POST":

            time_request = datetime.datetime.today()
            response = http.request.make_response(dispatcher.dispatch(http.request.httprequest.data))
            time_response = datetime.datetime.today()
            logging_var={'serviceName':'es_harici_sistemler','operationName':'',
                         'requestTime':time_request,'responseTime':time_response,
                         'requestData':http.request.httprequest.data,'responseData':response.data
                         }
            #logging_obj = http.request.env['info_tsm.es_logs']
            #logging_obj.sudo().create(logging_var)

        else:
            response = http.request.make_response(dispatcher.wsdl())

        response.headers['Content-Type'] = 'text/xml'
        return response