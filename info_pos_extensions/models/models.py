# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class pos_config(models.Model):
    _inherit = 'pos.config' 

    pos_qr_duration = fields.Integer("Pos Qr Code Duration",default=120)
    user_id         = fields.Many2one('res.users',string='Kullanıcı')

    @api.onchange('user_id')
    def user_id_on_change(self):
        if self.user_id:
            self.pos_kitchen_view = False
            if self.user_id.user_role == 'mutfak':
                self.pos_kitchen_view = True

            self.module_pos_restauran = True
            self.longpolling_max_silence_timeout = 60
            self.longpolling_pong_timeout = 10
            self.autostart_longpolling = True
            self.is_table_management = True
            self.iface_orderline_notes = True
            self.iface_display_categ_images = True
            self.start_category = True
            self.barcode_scanner = False
            self.show_number_guests = False
            self.iface_vkeyboard = True
            self.auto_mobile = True
            self.iface_big_scrollbars = True
            self.is_order_printer = True
            self.tax_regime_selection = True
            self.tax_regime = True
            self.default_fiscal_position_id = 1
            self.fiscal_position_ids = [(3,1)]
            self.iface_tax_included = 'total'
            self.iface_printbill = True
            self.iface_splitbill = True
            self.picking_type_id = 2
            self.module_account = True
            self.invoice_journal_id = 1
            self.journal_id = 1
            self.wv_session_id = [(0,0,{'name':self.env['info_restaurant_options.config'].sudo().search([])[-1].db_name})]

            if self.search_count([('user_id','=',self.user_id.id)]) > 0:
                self.user_id = False

                return {'warning': {'title': _('Uyarı...'), 'message': _('Aynı Kullanıcının Zaten Konfigürasyonu Mevcut...')}}





# class info_pos_extensions(models.Model):
#     _name = 'info_pos_extensions.info_pos_extensions'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100