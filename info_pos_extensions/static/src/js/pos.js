odoo.define('info_pos_extensions', function(require){
    var pos_sync_order = require('pos_sync_order');
    var session = require('web.session');
    // var Backbone = window.Backbone;
    var core = require('web.core');
    var screens = require('point_of_sale.screens');
    var PosBaseWidget = require('point_of_sale.BaseWidget');
    var models = require('point_of_sale.models');
    // var bus = require('bus.bus');
    var gui = require('point_of_sale.gui');
    var chrome = require('point_of_sale.chrome');
    var PosPopWidget = require('point_of_sale.popups');
    var QWeb = core.qweb;
    var rpc = require('web.rpc')
    var _t = core._t;
    console.log( navigator.userAgent );
    var isMobile = {

        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
    };
    function close_mobile (){
            console.log( navigator.userAgent );
            if(isMobile.iOS()){
                console.log('IOS Close Session');
                webkit.messageHandlers.logOutSession.postMessage(false);
            }
            else if(isMobile.Android()){
                console.log('Android Close Session');
                AndroidInterface.logOutSession(false);
            }


    }
    chrome.UsernameWidget.include({

        click_username: function(){
            console.log('Click Veto');
        },

    })
    //console.log( gui );
    gui.Gui.include({

        _close:function(){

            var self = this;
            this.chrome.loading_show();
            this.chrome.loading_message(_t('Closing ...'));

            this.pos.push_order().then(function(){
                var url = "/web_pos_close#action=point_of_sale.action_client_pos_menu";
                close_mobile();
                //console.log( url );
                window.location = session.debug ? $.param.querystring(url, {debug: session.debug}) : url;
            });
        }
    })
    screens.ReceiptScreenWidget.include({
        click_back: function() {
            this.gui.show_screen('products');
            // Placeholder method for ReceiptScreen extensions that
            // can go back ...
        },

    });
    screens.PaymentScreenWidget.include({
        renderqrButton: function() {
            var self = this;


            var methodqr = $(QWeb.render('QrPaymentButton', { widget:this }));
            methodqr.on('click',function(){
                var cashregister = null;
                for ( var i = 0; i < self.pos.cashregisters.length; i++ ) {
                    if ( self.pos.cashregisters[i].journal_id[1].includes('Kasa') ){
                        cashregister = self.pos.cashregisters[i];
                        break;
                    }
                }
                var ord    = self.pos.get_order();
                console.log( ord.uid );
                var partnerId = 1;
                var pres = rpc.query({
                        model: 'info_restaurant_options.config',
                        method: 'search_read',
                    }).then(function (result) {

                            console.log( result );

                            if (result[0].base_partner_id > 0){
                                partnerId = result[0].base_partner_id;
                            }
                            console.log( partnerId );
                            var q_text = ord.uid + '|' + session.db + '|'+ partnerId +'|' + self.pos.config.pos_qr_duration + '|' + ord.get_total_with_tax() +'|';
                            var lines  = ord.orderlines.models;

                            for (var l in lines){
                                q_text += lines[l].product.display_name + '#';
                                q_text += lines[l].quantity + '#';
                                q_text += lines[l].price + ';';
                            }
                            self.pos.qr_text = q_text;
                            $('.qrcodeImg').empty();
                            jQuery('.qrcodeImg').qrcode({
                                 text	: self.pos.qr_text
                            });

                            ord.add_paymentline( cashregister );
                            self.reset_input();
                            self.render_paymentlines();
                            var domain = [['order_id','=',ord.uid]];

                            $('.qrcode-container').css({'display':'block'});
                            var counter = 0;
                            var checkPayment = setInterval(function(){
                                counter ++;
                                var res = rpc.query({
                                    model: 'info_restaurant_options.mobil_odeme',
                                    method: 'search_count',
                                    args:[domain]
                                }).then(function (count_result) {

                                        console.log('AAAAAAAAAAAAA');
                                        if ( count_result > 0){
                                            clearInterval( checkPayment );
                                            self.$(".next").click();
                                        }
                                    });

                                if (counter >= 12){
                                    clearInterval( checkPayment );
                                }
                            },10000)
                        });
            });


            return methodqr;
        },
        render_qr_code: function(){

            var self = this;
            $('.qrcode-container').css({'display':'none'});

            var Infoqrcode = $(QWeb.render('PaymentScreen-QrCode', { widget:this }));
            //Infoqrcode.on('click',

            return Infoqrcode;
        },
        renderElement: function() {
            var self = this;
            this._super();
            var Infoqrcode = this.render_qr_code();
            Infoqrcode.appendTo(this.$('.qrcode-container'));

            var methodsqr = this.renderqrButton();
            methodsqr.appendTo(this.$('.paymentmethods'));

        }

    });
});