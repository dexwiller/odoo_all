# -*- coding: utf-8 -*-
{
    'name': "Yemekmatik Kapmanya",

    'summary': """Yemekmatik Kampanya Yönetimi""",

    'description': """
        Yemekmatik Kampanya Yönetim Modülüdür.
    """,

    'author': "Yemekmatik adına Deniz Yıldız",
    'website': "http://www.yemekmatik.com.tr",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Plugins',
    'version': '10.1',

    # any module necessary for this one to work correctly
    'depends': ['base','info_kart','info_bayi'],

    # always loaded
    'data': [
        'security/groups.xml',
        'security/ir.model.access.csv',
        'views/trees.xml',
        'views/actions.xml',
        'views/forms.xml',
        'views/menu.xml',
        'views/search.xml',
        'views/kanbans.xml',
        'views/sequences.xml',
        'views/wizards.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}