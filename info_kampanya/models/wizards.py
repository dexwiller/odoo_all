# -*- coding: utf-8 -*-

from odoo import models, fields, api

class KampanyaOnayWizard(models.TransientModel):
    _name='info_kampanya.kampanya_onay'
    
    kampanya_id      = fields.Many2one('info_kampanya.kampanya',default = lambda self:self._context.get('active_id'))
    kurallar         = fields.Many2many('info_kampanya.kural',string='Avantaj Cüzdanına Katılım Kurallarını Giriniz')
    avantaj_ismi     = fields.Char(string="Avantaj Cüzdanı Sloganı")
    
    @api.multi
    def confirm(self):
        self.ensure_one()
        self.kampanya_id.kurallar       = self.kurallar
        self.kampanya_id.avantaj_ismi   = self.avantaj_ismi
        self.kampanya_id.kod            = self.env['ir.sequence'].sudo().next_by_code('avantaj.cuzdan')
        self.kampanya_id.state          = 'confirmed'
        
        
