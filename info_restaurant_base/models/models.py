# -*- coding: utf-8 -*-
import shutil, os
from odoo import models, fields, api, modules, tools, SUPERUSER_ID
from odoo.service import db
from subprocess import Popen, PIPE
from datetime import datetime
import random, string, requests


class sms_kontrol(models.Model):
    _name = 'info_restaurant_base.sms'

    sms_tarih = fields.Datetime(string="SMS Tarihi", default=datetime.now())
    metin = fields.Char(string="SMS Metin")
    telefon = fields.Char(string='Personel Telefon')
    key = fields.Char(string='Gönderilen SMS Key')
    demo_id = fields.Many2one('info_restaurant_base.demo')

    @api.multi
    def send_sms(self):
        return -1
        for s in self:
            apiurl = 'https://smsgw.mutlucell.com/smsgw-ws/sndblkex'
            xml2 = '<?xml version="1.0" encoding="UTF-8"?>'
            xml2 += '<smspack ka="infoteks" pwd="in20fo17teks!_" org="Yemekmatik" charset="turkish">'
            xml2 += '<mesaj>'
            xml2 += u'<metin>%s</metin>' % s.metin
            xml2 += '<nums>%s</nums>' % s.telefon
            xml2 += '</mesaj>'
            xml2 += '</smspack>'
            xml2 = xml2.encode("utf-8")

            resp = requests.post(apiurl, data=xml2)
            s.key = resp.text

            if resp.text.startswith('$'):
                return -1
            else:
                return int(resp.text)


class demo_basvuru(models.Model):
    _name = 'info_restaurant_base.demo'

    status = fields.Char(string='Son Durum')
    status_t = fields.Datetime(string='Durum Tarihi')
    email = fields.Char(string='E-posta')
    phone = fields.Char(string='Tel No')
    db_name = fields.Char(string='Veritabanı Adı')
    cres = fields.One2many('info_restaurant_base.default_cre', 'basvuru_id')
    partner_id = fields.Many2one('res.partner', string='Satınalan Cari')


class default_cre(models.Model):
    _name = 'info_restaurant_base.default_cre'

    basvuru_id = fields.Many2one('info_restaurant_base.demo')
    login = fields.Char()
    raw_pass = fields.Char()
    phone = fields.Char()
    db_name = fields.Char(related='basvuru_id.db_name', store=True)
    user_role = fields.Selection(string='Kullanıcı Tipi', selection=[('yonetici', 'Yönetici'),
                                                                     ('mutfak', 'Mutfak'),
                                                                     ('garson', 'Garson')], default='garson')


class info_restaurant_db_to_drop(models.Model):
    _name = 'info_restaurant_base.db_to_drop'
    name = fields.Char(string='Silinecek Veritabanı')
    is_dropped = fields.Boolean(string="Silindi mi?", default=False)
    drop_date = fields.Datetime(string='Silinme Tarihi')


class info_restaurant_base_config(models.Model):
    _name = 'info_restaurant_base.config'

    state = fields.Selection(string='Durum', selection=[('demo', 'Demo Sürüm'),
                                                        ('expired', 'Süresi Dolan Sürüm'),
                                                        ('product', 'Ücretli Sürüm')], required=True, default='demo')
    # users           = fields.Many2many('res.users',string='Kullanıcılar',required=True)
    demo_duration = fields.Integer(string='Demo Geçerlilik Süresi (Gün)', required=True)
    demo_after_time = fields.Integer(string="Demo Bitiminden Sonra Verilerin Saklanacağı Süre (Gün )")
    product_id = fields.Many2one('product.template', domain="[('restoran','!=',False)]", required=True)
    default_conf = fields.Boolean(string="Demo için Varsayılan Konfigürasyon")
    master_db_name = fields.Char(string="Kopyalanacak Demo Veritabanı")

    yonetici_max_size = fields.Integer(string='Maximum Yönetici Sayısı')
    garson_max_size = fields.Integer(string='Maximum Garson Sayısı')
    mutfak_max_size = fields.Integer(string='Maximum Mutfak Sayısı')

    def set_status(self, obj, status):

        obj.write(status)
        self.env.cr.commit()

    def after_create(self, new_b, opt):
        reg = modules.registry.Registry.new(new_b.db_name)
        with reg.cursor() as new_cr:
            # if it's a copy of a database, force generation of a new dbuuid
            new_env = api.Environment(new_cr, SUPERUSER_ID, {})
            new_env['ir.config_parameter'].init(force=True)

            yonetici = opt.yonetici_max_size
            if not yonetici > 0:
                return 0, "Ürün Kullanımı İçin Sistem Şu An Müsait Değil, Daha Sonra Tekrar Deneyiniz. (Varsayılan Konfigürasyon En Az Bir Yönetici İçermelidir.)"
            new_user_obj = new_env.get('res.users').sudo()
            partner_id = 'null'
            partner = self.env['res.partner'].sudo().search(
                [('email', '=', new_b.email), ('is_company', '=', True), ('customer', '=',
                                                                          True)])
            try:
                if partner:
                    partner_id = partner.id
                    # new_b.partner_id = partner_id partner id demek satınalınmış demek ödemeden sonra basacağız.

                new_cr.execute("select count(*) from public.info_restaurant_options_config")
                if int(new_cr.fetchone()[0]) > 0:
                    new_cr.execute("""update public.info_restaurant_options_config set db_name = '%s', state ='demo', demo_duration = %s, email = '%s', phone = '%s', 
                                                yonetici_max_size = %s, garson_max_size=%s, mutfak_max_size=%s,demo_after_time=%s,paket_adi='%s',base_partner_id=%s,
                                                create_uid = 2, create_date = now() at time zone 'UTC', write_uid = 2, write_date=now() at time zone 'UTC';"""
                                   % (new_b.db_name, opt.demo_duration, new_b.email, new_b.phone, opt.yonetici_max_size,
                                      opt.garson_max_size, opt.mutfak_max_size, opt.demo_after_time, 'demo',
                                      partner_id))
                else:
                    new_cr.execute("""INSERT INTO public.info_restaurant_options_config(
                                db_name, state, demo_duration, email, phone, 
                                yonetici_max_size, garson_max_size, mutfak_max_size,demo_after_time,paket_adi,base_partner_id, 
                                create_uid, create_date, write_uid, write_date)
                                VALUES (%s, 'demo', %s,%s,%s, %s, %s, %s,%s,'%s',%s, 2, now() at time zone 'UTC', 2, now() at time zone 'UTC');"""
                                   % (new_b.db_name, opt.demo_duration, new_b.email, new_b.phone, opt.yonetici_max_size,
                                      opt.garson_max_size, opt.mutfak_max_size, opt.demo_after_time, 'demo',
                                      partner_id))

            except:
                return 0, "Ürün Kullanım İçin Sistem Şu An Müsait Değil, Daha Sonra Tekrar Deneyiniz. (Hedef Konfigürasyon Oluşturulamadı.)"

            # creating users here....
            try:
                new_users = new_user_obj.search([])
                letters = string.ascii_uppercase
                users_dict_list = []
                for u in new_users:
                    if u.id > 5:
                        new_p = ''.join(random.choice(letters) for i in range(6))
                        email = new_b.db_name + '-' + u.login
                        phone = False

                        if u.user_role == 'yonetici':
                            email = new_b.email
                            phone = new_b.phone

                        u.write({'password': new_p,
                                 'login': email,
                                 'mobile': phone,
                                 'default_pass': new_p})

                        users_dict_list.append((0, 0, {'login': email,
                                                       'raw_pass': new_p,
                                                       'phone': new_b.phone,
                                                       'user_role': u.user_role}))  # varsayılan olarak ilk telefonu bastık

                new_b.cres = users_dict_list
                new_env.cr.commit()
            except Exception as e:
                return 0, "Ürün Kullanım İçin Sistem Şu An Müsait Değil, Daha Sonra Tekrar Deneyiniz. ( %s ) " % e

        return 1, users_dict_list

    def copy_database(self, kw):

        old_mail = self.env['info_restaurant_base.demo'].sudo().search_count([('email', '=', kw.get('email'))])
        if old_mail > 0:
            return 0, "Daha Önce Demo Oluşturmuşsunuz."
        opt = self.search([('default_conf', '=', True)], order='id desc')

        if not opt:
            return 0, "Ürün Kullanımı İçin Sistem Şu An Müsait Değil, Daha Sonra Tekrar Deneyiniz. (Konfigürasyon Bulunamadı)"
        opt = opt[-1]
        master_db = opt.master_db_name
        if not master_db:
            return 0, "Ürün Kullanımı İçin Sistem Şu An Müsait Değil, Daha Sonra Tekrar Deneyiniz. (Varsayılan Veritabanı Bulunamadı)"

        new_b = self.env.get('info_restaurant_base.demo').sudo().browse([int(kw.get('demo_id'))])

        if not new_b:
            return 0, "Ürün Kullanımı İçin Sistem Şu An Müsait Değil, Daha Sonra Tekrar Deneyiniz. (Başvuru Bulunamadı)"

        self.set_status(new_b, {'status': 'Demo Sistem Oluşturma İşlemi Başladı',
                                'status_t': datetime.now(),
                                'db_name': kw.get('subdomain'),
                                'email': kw.get('email'),
                                'phone': kw.get('phone'),
                                'partner_id': kw.get('partner_id')
                                })

        self.set_status(new_b, {'status': 'Demo Veritabanı Yedekleniyor.....',
                                'status_t': datetime.now(), })

        p1 = Popen(['pg_dump', '-f', '/tmp/temp.dump', master_db], stdout=PIPE, stderr=PIPE)
        stdout, stderr = p1.communicate()
        if stderr:
            return 0, "Ürün Kullanımı İçin Sistem Şu An Müsait Değil, Daha Sonra Tekrar Deneyiniz. (%s)" % stderr

        self.set_status(new_b, {'status': 'Veritabanı Oluşturuluyor.....',
                                'status_t': datetime.now(), })

        try:
            db._create_empty_database(kw.get('subdomain'))
        except Exception as e:
            return 0, "Veritabanı Zaten Oluşturulmuş, Lütfen Farklı Bir Alt alan Adı Seçiniz."

        self.set_status(new_b, {'status': 'Veritabanı Kopyalanıyor.....',
                                'status_t': datetime.now(), })

        p2 = Popen(['psql', '-f', '/tmp/temp.dump', kw.get('subdomain')])
        stdout2, stderr2 = p2.communicate()
        if stderr2:
            db.exp_drop(kw.get('subdomain'))
            return 0, "Ürün Kullanımı İçin Sistem Şu An Müsait Değil, Daha Sonra Tekrar Deneyiniz. (%s)" % stderr2

        self.set_status(new_b, {'status': 'Dosya Sistemi Oluşturuluyor...',
                                'status_t': datetime.now(), })
        try:
            fs = tools.config.filestore(master_db)
            fs2 = tools.config.filestore(kw.get('subdomain'))
            shutil.copytree(fs, fs2)
        except Exception as e:
            db.exp_drop(kw.get('subdomain'))

            return 0, 'Dosya Sistemi Oluşturulamadı....  %s' % e

        self.set_status(new_b, {'status': 'Veritabanı Kullanıcılar ve Profil Düzenleniyor...',
                                'status_t': datetime.now(), })
        email = kw.get('email')
        s, sa = self.after_create(new_b, opt)
        if not s:
            db.exp_drop(kw.get('subdomain'))
            return s, sa

        self.set_status(new_b, {'status': 'İşlemler Tamamlandı',
                                'status_t': datetime.now(), })

        return new_b.id, sa

    def drop_db(self):

        domain = [('is_dropped', '=', False)]
        base_db_name = self.search([('default_conf', '=', True)])
        if base_db_name:
            base_db_name = base_db_name.master_db_name
            domain.append(('name', '!=', base_db_name))

        dbs_to_drop = self.env['info_restaurant_base.db_to_drop'].sudo().search(domain)
        for d in dbs_to_drop:
            r = db.exp_drop(d.name)
            if r:
                d.is_dropped = True
                d.drop_date = datetime.now()


class product_template(models.Model):
    _inherit = 'product.template'
    restoran = fields.Char('Restoran Modül Anahtarı.')


class res_users(models.Model):
    _inherit = 'res.users'

    user_role = fields.Selection(string='Kullanıcı Tipi', selection=[('yonetici', 'Yönetici'),
                                                                     ('mutfak', 'Mutfak'),
                                                                     ('garson', 'Garson')], default='garson')

    @api.multi
    def write(self, v):
        if v.get('user_role'):
            g_name = 'info_restaurant_base.' + v.get('user_role')

            v['groups_id'] = [(4, self.env.ref(g_name).id), ]

        res = super(res_users, self).write(v)
        return res

    @api.model
    def create(self, v):
        if v.get('user_role'):
            g_name = 'info_restaurant_base.' + v.get('user_role')
            print(self.env.ref(g_name).id)
            v['groups_id'] = [(4, self.env.ref(g_name).id), ]

        res = super(res_users, self).create(v)
        return res
