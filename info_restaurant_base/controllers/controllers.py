# -*- coding: utf-8 -*-
from odoo import http
from odoo.addons.web.controllers.main import serialize_exception
import re,json,random
from datetime import datetime,date
import base64
import werkzeug
import urllib.parse as url_parser
from Crypto.Cipher import AES
BLOCK_SIZE = 16
PADDING = '{'.encode('utf-8')
pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING
EncodeAES = lambda c, s: base64.b64encode(c.encrypt(pad(s)))
DecodeAES = lambda c, e: str(c.decrypt(base64.b64decode(e)), 'utf-8').rstrip(PADDING.decode('utf-8'))
#secret = os.urandom(BLOCK_SIZE)
a = 'PTHK%GT&+Y386^0*'
cipher = AES.new(a)

class InfoRestaurantBase(http.Controller):


    def check_email(self, email):
        if not re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', email):
            return False
        return True

    def calculate_age(self, born):
        born = datetime.strptime(born, '%Y-%m-%d')
        today = date.today()
        return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

    def tel_conf(self, tel):
        phone = [str(s) for s in tel.split() if s.isdigit()]

        if phone:
            if phone[0][0] == "0":
                phone = [phone[0][1:]]
            if len(phone) > 0:
                phone = "".join(phone)
                if len(phone) != 10:
                    return 1
                else:
                    if phone[0] != '5':
                        return 2
                    if not phone.isdigit():
                        return 3
            else:
                return 4
        else:
            return 1
        return 5

    @http.route('/demo/get_status/<d>',type='http', auth='public',cors='*',website=True)
    @serialize_exception
    def get_status(self,d,**kw):
        demo_status = http.request.env.get('info_restaurant_base.demo').sudo().browse([int(d)]).status
        if not demo_status:
            demo_status = ''
        return demo_status

    @http.route('/yemek-karti/urunler/restoran/demo/', type='http', auth='public',cors='*',website=True)
    @serialize_exception
    def index(self, **kw):
        if http.request.httprequest.method == "POST":
            kw['paket']='demo'
            return_errors = []
            values = kw
            if not kw.get('phone'):
                return_item = {"html_name": "phone", "error_messsage": "Yetkili GSM Giriniz."}
                return_errors.append(return_item)
            else:
                if kw.get("phone")[0] == "0":
                    values["phone"] = kw.get("phone")[1:]
                # print kw["cep_tel"]
                c = self.tel_conf(values["phone"])

                if c == 1:
                    return_item = {"html_name": "phone",
                                   "error_messsage": "Cep Telefonu Numarası 10 Hane Olmalıdır ve Başında 0 Olmamalıdır."}
                    return_errors.append(return_item)
                elif c == 2:
                    return_item = {"html_name": "phone", "error_messsage": "Cep Telefonu Numarası 5 ile Başlamalıdır."}
                    return_errors.append(return_item)
                elif c == 3:
                    return_item = {"html_name": "phone",
                                   "error_messsage": "Cep Telefonu Numarası Sadece Rakamlardan Oluşmalıdır."}
                    return_errors.append(return_item)
                elif c == 4:
                    return_item = {"html_name": "phone",
                                   "error_messsage": "Lütfen Geçerli bir Cep Telefonu Numarası Giriniz."}
                    return_errors.append(return_item)

            if not kw.get("email"):
                return_item = {"html_name": "email", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
                return_errors.append(return_item)
            else:
                if not self.check_email(kw.get("email")):
                    return_item = {"html_name": "email", "error_messsage": "Lütfen Geçerli E-Posta Adresi Giriniz."}
                    return_errors.append(return_item)

            if not kw.get('subdomain'):
                return_item = {"html_name": "subdomain", "error_messsage": "Alt Alan Adı Giriniz."}
                return_errors.append(return_item)
            else:
                if len(kw.get('subdomain').strip()) > 25 or len(kw.get('subdomain').strip()) < 2:
                    return_item = {"html_name": "subdomain",
                                   "error_messsage": "Geçersiz Alt Alan Adı. Alt Alan Adınız Minimum 2, Maksimum 25 Karakter Uzunluğunda Olmalıdır ve Boşluk, Türkçe Karakter veya '%&*' gibi Özel Karakterler İçermemelidir."}
                    return_errors.append(return_item)
                if not all(ord(c) < 128 for c in kw.get('subdomain')):
                    return_item = {"html_name": "subdomain", "error_messsage": "Geçersiz Alt Alan Adı. Alt Alan Adınız Boşluk, Türkçe Karakter veya '%&*' gibi Özel Karakterler İçermemelidir."}
                    return_errors.append(return_item)
            if return_errors:
                return json.JSONEncoder().encode({'status': 'error', 'errors': return_errors})
            result,result_str = http.request.env['info_restaurant_base.config'].sudo().copy_database(kw)
            if not result:
                return_item = {"html_name": "status", "error_messsage": result_str}
                return json.JSONEncoder().encode({'status': 'error', 'errors': [return_item]})
            return json.JSONEncoder().encode({'status': 'success','redirect_url':"/yemek-karti/urunler/restoran/demo/result/%s"%result})
        else:
            page  = 'info_restaurant_base.get_demo_opts'
            new_b = http.request.env.get('info_restaurant_base.demo').sudo().create({})
            return http.request.render(page,{'demo_id':new_b.id})


    @http.route('/yemek-karti/urunler/restoran/demo/result/<d>', type='http', auth='public', cors='*', website=True)
    @serialize_exception
    def demo_result(self,d, **kw):

        new_b = http.request.env.get('info_restaurant_base.demo').sudo().browse([int(d)])
        for c in new_b.cres:
            if not c.raw_pass:
                c.unlink()
        return http.request.render('info_restaurant_base.get_demo_result',{'logins':new_b.cres})

    @http.route('/yemek-karti/urunler/restoran/fatura/result/<d>/<k>/<p>', type='http', auth='public', cors='*', website=True)
    @serialize_exception
    def fatura_result(self, d,k,p, **kw):

        new_b = http.request.env.get('info_restaurant_base.demo').sudo().browse([int(d)])
        for c in new_b.cres:
            if not c.raw_pass:
                c.unlink()
        return_url = "/yemek-karti/urunler/restoran/fatura/result/{}/{}/{}"%(d,k,p)
        layout = 'info_restaurant_base.sa_cari_form'
        return http.request.render('info_restaurant_base.get_paket_result', {'logins': new_b.cres,
                                                                            'k_id':k,
                                                                            'p_id':p,
                                                                            'return_url':return_url,
                                                                            'layout':layout
                                                                            })

    @http.route('/form/get_partner_by_tckn_vkn/<d>', type='http', auth="public")
    def get_partner_by_tckn_vkn(self, d=None, **kw):

        if d:
            partner = http.request.env['res.partner'].sudo().search_read(['|',('taxNumber', '=', d),
                                                                     ('tckn','=',d)],fields=['id','email','gsm_no','gsm_no_name','city_combo','ilce','street','display_name'])
            if not partner:
                return ""
            else:
                print (partner)
                return json.JSONEncoder().encode( partner[0] )
    '''
    def create_copy_sms(self,basvuru_id, veri_sahibi):
        code = random.randint(100000, 999999)
        metin = '%s Kodu İle Restoran Uygulama Verilerinizin Kopyalanmasına İzin Verebilirsiniz...' % code
        phone = veri_sahibi.telefon
        sms  = http.request.env('info_restaurant_base.sms').sudo().create({
            'key':code,
            'metin':metin,
            'telefon':phone,
            'demo_id':basvuru_id
        })
    '''
    @http.route('/yemek-karti/urunler/restoran/fatura/<d>/<p>/<s>', type='http', auth='public', cors='*', website=True)
    @serialize_exception
    def index_result(self, d,p=False,s=False, **kw):
        if http.request.httprequest.method == "POST":
            values = kw
            partner_obj = http.request.env['res.partner'].sudo()
            if values.get("esnaf"):
                values['esnaf'] = True

            values.update({'customer': True, 'is_company': True})
            partner = False
            if values.get('partner_id'):
                partner = partner_obj.browse([int( kw.get('partner_id'))])

            return_errors = []
            if not partner and  not kw.get("sirket_tipi_selection"):
                return_item = {"html_name": "sirket_tipi_selection", "error_messsage": "Şirket Tipi Seçiniz."}
                return_errors.append(return_item)

            if not kw.get("vkn_tckn") :
                return_item = {"html_name": "vkn_tckn", "error_messsage": "Firma VKN/TCKN Giriniz."}
                return_errors.append(return_item)

            if not kw.get("subdomain") :
                return_item = {"html_name": "subdomain", "error_messsage": "Talep Edilen Erişim Adresi Öneki Giriniz."}
                return_errors.append(return_item)

            if not kw.get("kampanya") :
                return_item = {"html_name": "kampanya", "error_messsage": "Kampanya Seçiniz"}
                return_errors.append(return_item)

            if not kw.get("name") or kw.get("name").strip() == "":
                return_item = {"html_name": "name", "error_messsage": "Firma İsmi Giriniz"}
                return_errors.append(return_item)

            if not kw.get("street") or kw.get("street").strip() == "":
                return_item = {"html_name": "street", "error_messsage": "Adres Giriniz"}
                return_errors.append(return_item)

            if not kw.get("city_combo"):
                return_item = {"html_name": "city_combo", "error_messsage": "Şehir Seçiniz"}
                return_errors.append(return_item)

            if not kw.get("ilce"):
                return_item = {"html_name": "ilce", "error_messsage": "İlçe Seçiniz"}
                return_errors.append(return_item)

            if not kw.get("gsm_no_name") or kw.get("gsm_no_name").strip() == "":
                return_item = {"html_name": "gsm_no_name", "error_messsage": "Firma Yetkilisi Giriniz."}
                return_errors.append(return_item)

            if not kw.get('gsm_no'):
                return_item = {"html_name": "gsm_no", "error_messsage": "Yetkili GSM Giriniz."}
                return_errors.append(return_item)
            else:
                if kw.get("gsm_no")[0] == "0":
                    values["gsm_no"] = kw.get("gsm_no")[1:]
                # print kw["cep_tel"]
                c = self.tel_conf(values["gsm_no"].replace('-','').replace(' ',''))

                if c == 1:
                    return_item = {"html_name": "gsm_no",
                                   "error_messsage": "Cep Telefonu Numarası 10 Hane Olmalıdır ve Başında 0 Olmamalıdır."}
                    return_errors.append(return_item)
                elif c == 2:
                    return_item = {"html_name": "gsm_no", "error_messsage": "Cep Telefonu Numarası 5 ile Başlamalıdır."}
                    return_errors.append(return_item)
                elif c == 3:
                    return_item = {"html_name": "gsm_no",
                                   "error_messsage": "Cep Telefonu Numarası Sadece Rakamlardan Oluşmalıdır."}
                    return_errors.append(return_item)
                elif c == 4:
                    return_item = {"html_name": "gsm_no",
                                   "error_messsage": "Lütfen Geçerli bir Cep Telefonu Numarası Giriniz."}
                    return_errors.append(return_item)

            if not kw.get("email"):
                return_item = {"html_name": "email", "error_messsage": "Girilen Değeri Kontrol Ediniz."}
                return_errors.append(return_item)
            else:
                if not self.check_email(kw.get("email")):
                    return_item = {"html_name": "email", "error_messsage": "Lütfen Geçerli E-Posta Adresi Giriniz."}
                    return_errors.append(return_item)

            if not return_errors:
                if partner:
                    partner.write(values)
                else:
                    partner = partner_obj.create(values)
                subd = http.request.env.get('info_restaurant_base.demo').sudo()
                saved_subd = subd.search([('db_name','=',kw.get('subdomain'))])
                if saved_subd:
                    if saved_subd[-1].email != kw.get('email').strip():
                        return_item = {"html_name": "status", "error_messsage": 'Talep Edilen Erişim Adresi Öneki Başka Bir Müşterimiz Tarafından Kullanılmaktadır. '}
                        return json.JSONEncoder().encode({'status': 'error', 'errors': [return_item]})
                    else:
                         saved_subd.partner_id = partner.id
                         return_url = "/yemek-karti/urunler/restoran/fatura/{}/{}/{}".format(d,p,s)
                         layout     = 'info_restaurant_base.sa_cari_form'

                         return json.JSONEncoder().encode({'status': 'success',
                                                           'redirect_url': '/yemek-karti/kart-ode2/{}/{}/?return_url={}&layout={}'
                                                         .format(kw.get('kampanya'),partner.id,return_url,layout)})

                else:
                    new_b = subd.create({'partner_id':partner.id})
                    kw['phone'] = kw.get('gsm_no')
                    kw['partner_id'] = partner.id
                    kw['demo_id'] = new_b.id
                    result, result_str = http.request.env['info_restaurant_base.config'].sudo().copy_database(kw)
                    if not result:
                        return_item = {"html_name": "status", "error_messsage": result_str}
                        return json.JSONEncoder().encode({'status': 'error', 'errors': [return_item]})
                    return json.JSONEncoder().encode(
                        {'status': 'success', 'redirect_url': "/yemek-karti/urunler/restoran/fatura/result/%s/%s/%s" % (result,kw.get('kampanya'),partner.id)})

            else:
                return json.JSONEncoder().encode({'status': 'error', 'errors': return_errors})

        else:
            #old_dbs   = http.request.env['info_restaurant_base.demo'].sudo().search([('partner_id','!=',False)])
            #db_uyari  = "Mevcut verilerden kopyalama seçeneğini kullanırsanız, MEVCUT TÜM VERİLERİNİZ SİLİNECEK ve seçtiğiniz sistem yöneticisinin SMS ONAYI sonrası satınalma işleminiz tamamlanacaktır."

            product = http.request.env['product.template'].sudo().search([('restoran','=',d)])
            kams    = http.request.env['info_kk_payment.yemekmatik_kampanya'].sudo().search([
                ('product_id','=',product.id),
                ('aktif','=',True)])

            if not product or  not kams:
                return werkzeug.utils.redirect('Product Bulunamadı', 404)
            vkn_tckn = False
            partner = False
            subdomain_uyari = ""
            subdomain = False
            if s and s != "-1":
                subdomain  = s
                if subdomain:
                    subdomain_uyari = 'Erişim Adresi Önekinin Değişmesi Halinde Verileriniz Kaybolacaktır.'

            if p and p != '-1':
                p_id_d = p
                try:
                    partner = http.request.env['res.partner'].sudo().browse([int(p_id_d)])
                    vkn_tckn = partner.taxNumber or partner.tckn or ''
                except:
                    partner = False
            iller   = http.request.env['info_extensions.iller'].sudo().search([])
            ilceler = []
            if partner:
                ilceler = http.request.env['info_extensions.ilceler'].sudo().search([('il_id','=',partner.city_combo.id)])

            values={#'db_copy_uyari':db_uyari,
                    #'old_database':old_dbs,
                    'kams':kams,
                    'product':product,
                    'partner':partner,
                    'iller':iller,
                    'ilceler':ilceler,
                    'firma_unvani':partner and partner.display_name or False,
                    'sirket_tipi_selection':partner and True or False,
                    'street':partner and partner.street or False,
                    'city_combo':partner and partner.city_combo or False,
                    'ilce':partner and partner.ilce or False,
                    'gsm_no_name':partner and partner.gorusulen_kisi or False,
                    'gsm_no':partner and partner.gorusulen_kisi_tel or False,
                    'email':partner and partner.email or False,
                    'vkn_tckn':vkn_tckn,
                    'subdomain':subdomain,
                    'subdomain_uyari':subdomain_uyari}

            page = 'info_restaurant_base.sa_cari_form'
            return http.request.render(page, values)

    @http.route('/yemek-karti/urunler/restoran', type='http', auth='public', cors='*', website=True)
    @serialize_exception
    def urunler_restoran(self,  **kw):

        standart_price = http.request.env.get('product.template').search([('restoran','=','standart')])
        gold_price     = http.request.env.get('product.template').search([('restoran', '=', 'gold')])

        standart_price = standart_price and  standart_price[0].list_price or '65'
        gold_price     = gold_price and gold_price[0].list_price or '125'

        values={'encoded_partner':kw.get('partner') or '-1',
                'encoded_database':kw.get('database') or '-1',
                'standart_price':standart_price,
                'gold_price':gold_price}
        page = 'info_restaurant_base.restoran-sistemi'
        return http.request.render(page, values)


