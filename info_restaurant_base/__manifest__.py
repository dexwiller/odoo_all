# -*- coding: utf-8 -*-
{
    'name': "info_restaurant_base",

    'summary': """
        Info Restaurant Base""",

    'description': """
        :)
    """,

    'author': "İnfoteks Technology - Deniz Yıldız",
    'website': "http://www.infoteks.com.tr",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Plugins',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','product','website'],

    # always loaded
    'data': [
        'security/groups.xml',
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/ir_cron.xml'
        
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}