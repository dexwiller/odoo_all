odoo.define('web_widget_date_validation', function (require) {
    "use strict";

    var core = require('web.core');
    var time = require('web.time');
    var DateWidget = require('web.datepicker').DateWidget;

    DateWidget.include({
        _onDateTimePickerShow: function () {
            var value;
            var self = this;
            var date_comparision = false;
            var parent = this.getParent();
            if (parent.nodeOptions.disable_past_date){
          	  var d = new Date();
          	  d.setHours(0,0,0,0);
          	  this.$el.datetimepicker('minDate', moment(d));
            }
            if(parent.nodeOptions.to_date){
            	var to_date = parent.nodeOptions.to_date;
                var to_date_val = $('input[name='+to_date+']').val()
                if (to_date_val){
                	var max_date = this._parseClient(to_date_val)
                	if (max_date.length !== 0 && this.isValid()) {
		          	  	var d = new Date();
                    	d.setHours(0,0,0,0);
						if(max_date < d){
							 this.$el.datetimepicker('maxDate', moment(d) || null)
						}else{
							this.options['maxDate'] = moment(max_date);
							this.$el.datetimepicker('maxDate', moment(max_date) || null);
						}
                	}
                }else{
                    this.$el.datetimepicker('maxDate', moment().add(100, 'y') || null);
                }
            }
            if(parent.nodeOptions.from_date){
            	var from_date = parent.nodeOptions.from_date;
                if(from_date){
                    var from_date_value = $('input[name='+from_date+']').val()
                    if(from_date_value){
                    	var min_date = this._parseClient(from_date_value)
                    	if (min_date.length !== 0 && this.isValid()) {
			          	  	var d = new Date();
	                    	d.setHours(0,0,0,0);
							if(min_date < d){
								 this.$el.datetimepicker('minDate', moment(d) || null)
							}else{
								this.options['minDate'] = moment(min_date);
								this.$el.datetimepicker('minDate', moment(min_date) || null);
							}
	                	}
                    }else{
                    	var d = new Date();
                    	d.setHours(0,0,0,0);
                    	this.options['minDate'] = moment(d);
                    	this.$el.datetimepicker('minDate', moment(d));
                    }
                }
            }
            if (this.$input.val().length !== 0 && this.isValid()) {
                this.$input.select();
            }
        },
    });
});