# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.
{
    'sequence': 1,
    'name': 'Website Date Validations',
    'version': '12.0.1.0.0',
    'author': 'Serpent Consulting Services Pvt. Ltd.',
    'maintainer': 'Serpent Consulting Services Pvt. Ltd.',
    'website': 'http://www.serpentcs.com',
    'description': """Web Widget Date Validations.""",
    'summary': """Web Widget Date Validations.""",
    'category': 'Extra Tools',
    'license': 'LGPL-3',
    'depends': ['base'],
    'data': ['view/qweb.xml'],
     'images': ['static/src/img/Date-validation.jpg'],
    'installable': True,
    'application': True,
    'price': 49,
    'currency': 'EUR'
}
