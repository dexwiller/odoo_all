Datepicker Widget Options
=========================

This module allows passing options to the jquery datepicker for fields that use
the optionds. The option are passed as-is and are not validated.

Usage
=====

You must pass options through the "disable_past_date" field in the options:
it will disable all the past dates.

for vise versa disable dates from start date and end date you must have to pass 
parameter in options to_date in start date field and from_date in end date field

    ...
    <field name="start_date" options="{'disable_past_date' : True,'to_date':'end_date'}"/>
    <field name="end_date" options="{'disable_past_date' : True,'from_date':'start_date'}"/>
    ...
